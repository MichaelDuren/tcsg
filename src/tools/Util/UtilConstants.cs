using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Util
{
    public static class UtilConstants
    {
        public static class AppConfigNames
        {
            public static readonly string ApplicationName = "ApplicationName";
            public static readonly string BaseDatabaseConnectionStringName = "BaseDatabaseConnectionStringName";
        }
    }
}
