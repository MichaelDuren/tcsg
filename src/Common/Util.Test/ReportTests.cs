﻿using System;
using System.Text;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Util.Reports;

namespace Util.Test
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class ReportTests
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        [TestMethod]
        public void CsvReportTest()
        {
            log.Info("Running CsvReportTest()");

            CsvReportWriter rw = new CsvReportWriter();

            rw.CreateColumnHeading("Name");
            rw.CreateColumnHeading("Birthday");
            rw.CreateColumnHeading("Age");
            rw.CreateColumnHeading("Married");
            rw.CreateColumnHeading("Height");

            rw.NewRow();
            rw.SetCellValue("Jim");
            rw.SetCellDateValue(DateTime.Parse("1/16/1959"));
            rw.SetCellIntegerValue(53);
            rw.SetCellBooleanValue(true);
            rw.SetCellDoubleValue(5.11);

            rw.NewRow();
            rw.SetCellValue("Martha");
            rw.SetCellDateValue(DateTime.Parse("9/24/1960"));
            rw.SetCellIntegerValue(51);
            rw.SetCellBooleanValue(true);
            rw.SetCellDoubleValue(5.45);

            rw.SaveToFile(@"\Logs\Util.Test\", "CSV Test Report.csv");
        }

        [TestMethod]
        public void HtmlReportTest()
        {
            log.Info("Running HtmlReportTest()");

            ReportWriterBase rw = new HtmlReportWriter();
            //ReportWriterBase rw = new CsvReportWriter();

            rw.WriteUnstructuredContent("Report Heading");

            rw.StartGrid();
            rw.CreateColumnHeading("Name");
            rw.CreateColumnHeading("Birthday", ReportStyleOption.AlignLeft);
            rw.CreateColumnHeading("Age");
            rw.CreateColumnHeading("Married");
            rw.CreateColumnHeading("Height");
            rw.CreateColumnHeading("Link");

            rw.NewRow();
            rw.SetCellValue("Jim");
            rw.SetCellDateValue(DateTime.Parse("1/16/1959"), "{0:d}");
            rw.SetCellIntegerValue(53, ReportStyleOption.FontColor_Blue, ReportStyleOption.BackgroundColor_Yellow, ReportStyleOption.FontSize_15);
            rw.SetCellBooleanValue(true);
            rw.SetCellDoubleValue(5.11, 3);
            rw.SetCellTextValue("Jim1 Link", hyperLinkFormat: @"http:\\jim.com", styleOptions: ReportStyleOption.BackgroundColor_Green);

            rw.NewRow();
            rw.SetCellValue("Martha");
            rw.SetCellDateValue(DateTime.Parse("9/24/1960"));
            rw.SetCellIntegerValue(51);
            rw.SetCellBooleanValue(true);
            rw.SetCellDoubleValue(5.045, 3);
            rw.SetCellTextValue("Martha Link", hyperLinkFormat: @"http:\\martha.com");

            rw.EndGrid();
            rw.WriteUnstructuredContent("Report Footer");

            rw.SaveToFile(@"\Logs\Util.Test\", "HTML Test Report.html");
        }

        [TestMethod]
        public void AdHocReportTest()
        {
            log.Info("Running AdHocReportTest()");

            SqlConnection connection = new SqlConnection("Data Source=homeserver;Initial Catalog=espDb;Integrated Security=true");
            connection.Open();

            //AdHocReport report = new AdHocReport(ReportWriterType.Html, connection, "Select * from App_ActivityLog");
            //AdHocReport report = new AdHocReport(ReportWriterType.Csv, connection, "Select [Name] ,[Category] ,[SubCategory] ,[Description] ,[DataTypeId] ,[BooleanValue] from App_SystemSettings");

            string sql = "Select * from App_SystemSettings";

            //AdHocReport_OLD report;
            //using (report = new AdHocReport_OLD(ReportWriterType.Csv, "Test Report", connection, sql))
            //{
            //    report.ReportFooter = "Report Footer";
            //    report.InitializeAndRun();
            //    //report.ReportWriter.SaveToFile(@"\Logs\Util.Test\Ad Hoc Test Report.csv");
            //    report.Save(@"\Logs\Util.Test\", reportSaveType: ReportSaveType.Csv);
            //}

            //using (report = new AdHocReport_OLD(ReportWriterType.Html, "Test Report", connection, sql))
            //{
            //    report.ReportFooter = "Report Footer";
            //    report.InitializeAndRun();
            //    //report.ReportWriter.SaveToFile(@"\Logs\Util.Test\Ad Hoc Test Report.html");
            //    report.Save(@"\Logs\Util.Test\", reportSaveType: ReportSaveType.Html);
            //}

            //using (report = new AdHocReport_OLD(ReportWriterType.Html, "Test Report", connection, sql))
            //{
            //    report.ReportFooter = "Report Footer";
            //    report.InitializeAndRun();
            //    //report.ReportWriter.SaveToFile(@"\Logs\Util.Test\Ad Hoc Test Report.html");
            //    report.Save(@"\Logs\Util.Test\", reportSaveType: ReportSaveType.Excel);
            //}
        }

        [TestMethod]
        public void AdHocReport_WithColumnDefinitions_Test()
        {
            log.Info("Running AdHocReport_WithColumnDefinitions_Test()");

            SqlConnection connection = new SqlConnection("Data Source=homeserver;Initial Catalog=espDb;Integrated Security=true");
            connection.Open();

            string sql;
            sql = "Select Id, [Name] ,[Category] ,[SubCategory] ,[Description] ,[DataTypeId] ,[BooleanValue] from App_SystemSettings";
            sql = "Select Id, [Name] ,[Category] ,[SubCategory] ,[DataTypeId] ,[BooleanValue] from App_SystemSettings";
            //sql = "Select * from App_SystemSettings";
            //sql = "Select * from App_ActivityLog";


            AdHocReportDefinition reportDef = new AdHocReportDefinition("Test Ad Hoc Report - with Definitions", sql);
            reportDef.ReportColumnDefinitions.Add(
                new AdHocReportColumnDefinition("ID") { HyperLinkFormat = "http://myTestLink.com?Id={0}" }
            );

            AdHocReport report;

            using (report = new AdHocReport(reportDef, ReportWriterType.Html))
            {
                report.SetDataSource(connection);

                report.ReportFooter = "Report Footer";
                report.InitializeAndRun();
                //report.ReportWriter.SaveToFile(@"\Logs\Util.Test\Ad Hoc Test Report.html");
                report.Save(@"\Logs\Util.Test\", reportSaveType: ReportSaveType.Html);
            }
        }

        // ********************************************************************************
        #region Setup Stuff
        // ********************************************************************************

        public ReportTests() { }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        #endregion

    }
}
