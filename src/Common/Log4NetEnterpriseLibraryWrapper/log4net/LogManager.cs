//Current as of 09.05.2012
using System;
using System.Collections.Generic;

namespace ESPLogWrapper
{
    public static class LogManager
    {
        private static Dictionary<string, ILog> loggers = new Dictionary<string, ILog>();

        public static ILog GetLogger(Type type)
        {
            return GetLogger(type.FullName);
        }

        public static ILog GetLogger(string loggerName)
        {
            if (loggers.ContainsKey(loggerName))
            {
                return loggers[loggerName];
            }
            else
            {
                ILog logger = new LogImpl(loggerName);
                return logger;
            }
        }
    }
}
