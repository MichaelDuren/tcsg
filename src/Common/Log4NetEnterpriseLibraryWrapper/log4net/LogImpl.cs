//Current as of 09.05.2012

using System;
using System.Globalization;
using Log4NetEnterpriseLibraryWrapper;

namespace ESPLogWrapper
{
    internal class LogImpl : ILog
    {
        private readonly static Type ThisDeclaringType = typeof(LogImpl);

        private ILogger Logger;

        public LogImpl(string name)
        {
            this.Logger = new LoggerWrapper(name);
        }

        // ********************************************************************************
        #region Debug
        // ********************************************************************************

        virtual public void Debug(object message, int eventId = 0)
        {
            Logger.Log(ThisDeclaringType, LoggingLevel.DEBUG, message, null, eventId);
        }

        virtual public void Debug(Type callerStackBoundaryDeclaringType, object message, int eventId = 0)
        {
            Logger.Log(callerStackBoundaryDeclaringType, LoggingLevel.DEBUG, message, null, eventId);
        }

        virtual public void Debug(object message, Exception exception, int eventId = 0)
        {
            Logger.Log(ThisDeclaringType, LoggingLevel.DEBUG, message, exception, eventId);
        }

        virtual public void DebugFormat(string format, params object[] args)
        {
            if (IsDebugEnabled)
            {
                Logger.Log(ThisDeclaringType, LoggingLevel.DEBUG, new SystemStringFormat(CultureInfo.InvariantCulture, format, args), null, 0);
            }
        }

        virtual public void DebugFormat(Type callerStackBoundaryDeclaringType, string format, params object[] args)
        {
            if (IsDebugEnabled)
            {
                Logger.Log(callerStackBoundaryDeclaringType, LoggingLevel.DEBUG, new SystemStringFormat(CultureInfo.InvariantCulture, format, args), null, 0);
            }
        }

        virtual public void DebugFormat(IFormatProvider provider, string format, params object[] args)
        {
            if (IsDebugEnabled)
            {
                Logger.Log(ThisDeclaringType, LoggingLevel.DEBUG, new SystemStringFormat(provider, format, args), null, 0);
            }
        }

        #endregion

        // ********************************************************************************
        #region Ingo
        // ********************************************************************************

        virtual public void Info(object message, int eventId = 0)
        {
            Logger.Log(ThisDeclaringType, LoggingLevel.INFO, message, null, eventId);
        }

        virtual public void Info(Type callerStackBoundaryDeclaringType, object message, int eventId = 0)
        {
            Logger.Log(callerStackBoundaryDeclaringType, LoggingLevel.INFO, message, null, eventId);
        }

        virtual public void Info(object message, Exception exception, int eventId = 0)
        {
            Logger.Log(ThisDeclaringType, LoggingLevel.INFO, message, exception, eventId);
        }

        virtual public void InfoFormat(string format, params object[] args)
        {
            if (IsInfoEnabled)
            {
                Logger.Log(ThisDeclaringType, LoggingLevel.INFO, new SystemStringFormat(CultureInfo.InvariantCulture, format, args), null, 0);
            }
        }

        virtual public void InfoFormat(Type callerStackBoundaryDeclaringType, string format, params object[] args)
        {
            if (IsInfoEnabled)
            {
                Logger.Log(callerStackBoundaryDeclaringType, LoggingLevel.INFO, new SystemStringFormat(CultureInfo.InvariantCulture, format, args), null, 0);
            }
        }

        virtual public void InfoFormat(IFormatProvider provider, string format, params object[] args)
        {
            if (IsInfoEnabled)
            {
                Logger.Log(ThisDeclaringType, LoggingLevel.INFO, new SystemStringFormat(provider, format, args), null, 0);
            }
        }

        #endregion

        // ********************************************************************************
        #region Warn
        // ********************************************************************************

        virtual public void Warn(object message, int eventId = 0)
        {
            Logger.Log(ThisDeclaringType, LoggingLevel.WARN, message, null, eventId);
        }

        virtual public void Warn(Type callerStackBoundaryDeclaringType, object message, int eventId = 0)
        {
            Logger.Log(callerStackBoundaryDeclaringType, LoggingLevel.WARN, message, null, eventId);
        }

        virtual public void Warn(object message, Exception exception, int eventId = 0)
        {
            Logger.Log(ThisDeclaringType, LoggingLevel.WARN, message, exception, eventId);
        }

        virtual public void WarnFormat(string format, params object[] args)
        {
            if (IsWarnEnabled)
            {
                Logger.Log(ThisDeclaringType, LoggingLevel.WARN, new SystemStringFormat(CultureInfo.InvariantCulture, format, args), null, 0);
            }
        }

        virtual public void WarnFormat(Type callerStackBoundaryDeclaringType, string format, params object[] args)
        {
            if (IsWarnEnabled)
            {
                Logger.Log(callerStackBoundaryDeclaringType, LoggingLevel.WARN, new SystemStringFormat(CultureInfo.InvariantCulture, format, args), null, 0);
            }
        }

        virtual public void WarnFormat(IFormatProvider provider, string format, params object[] args)
        {
            if (IsWarnEnabled)
            {
                Logger.Log(ThisDeclaringType, LoggingLevel.WARN, new SystemStringFormat(provider, format, args), null, 0);
            }
        }

        #endregion

        // ********************************************************************************
        #region Error
        // ********************************************************************************

        virtual public void Error(object message, int eventId = 0)
        {
            Logger.Log(ThisDeclaringType, LoggingLevel.ERROR, message, null, eventId);
        }

        virtual public void Error(Type callerStackBoundaryDeclaringType, object message, int eventId = 0)
        {
            Logger.Log(callerStackBoundaryDeclaringType, LoggingLevel.ERROR, message, null, eventId);
        }

        virtual public void Error(object message, Exception exception, int eventId = 0)
        {
            Logger.Log(ThisDeclaringType, LoggingLevel.ERROR, message, exception, eventId);
        }

        virtual public void Error(Type callerStackBoundaryDeclaringType, object message, Exception exception, int eventId = 0)
        {
            Logger.Log(callerStackBoundaryDeclaringType, LoggingLevel.ERROR, message, exception, eventId);
        }

        virtual public void ErrorFormat(string format, params object[] args)
        {
            if (IsErrorEnabled)
            {
                Logger.Log(ThisDeclaringType, LoggingLevel.ERROR, new SystemStringFormat(CultureInfo.InvariantCulture, format, args), null, 0);
            }
        }

        virtual public void ErrorFormat(Type callerStackBoundaryDeclaringType, string format, params object[] args)
        {
            if (IsErrorEnabled)
            {
                Logger.Log(callerStackBoundaryDeclaringType, LoggingLevel.ERROR, new SystemStringFormat(CultureInfo.InvariantCulture, format, args), null, 0);
            }
        }

        virtual public void ErrorFormat(IFormatProvider provider, string format, params object[] args)
        {
            if (IsErrorEnabled)
            {
                Logger.Log(ThisDeclaringType, LoggingLevel.ERROR, new SystemStringFormat(provider, format, args), null, 0);
            }
        }

        #endregion

        // ********************************************************************************
        #region Fatal
        // ********************************************************************************

        virtual public void Fatal(object message, int eventId=0)
        {
            Logger.Log(ThisDeclaringType, LoggingLevel.FATAL, message, null, eventId);
        }

        virtual public void Fatal(Type callerStackBoundaryDeclaringType, object message, int eventId = 0)
        {
            Logger.Log(callerStackBoundaryDeclaringType, LoggingLevel.FATAL, message, null, eventId);
        }

        virtual public void Fatal(object message, Exception exception, int eventId = 0)
        {
            Logger.Log(ThisDeclaringType, LoggingLevel.FATAL, message, exception, eventId);
        }

        virtual public void Fatal(Type callerStackBoundaryDeclaringType, object message, Exception exception, int eventId = 0)
        {
            Logger.Log(callerStackBoundaryDeclaringType, LoggingLevel.FATAL, message, exception, eventId);
        }

        virtual public void FatalFormat(string format, params object[] args)
        {
            if (IsFatalEnabled)
            {
                Logger.Log(ThisDeclaringType, LoggingLevel.FATAL, new SystemStringFormat(CultureInfo.InvariantCulture, format, args), null, 0);
            }
        }

        virtual public void FatalFormat(Type callerStackBoundaryDeclaringType, string format, params object[] args)
        {
            if (IsFatalEnabled)
            {
                Logger.Log(callerStackBoundaryDeclaringType, LoggingLevel.FATAL, new SystemStringFormat(CultureInfo.InvariantCulture, format, args), null, 0);
            }
        }

        virtual public void FatalFormat(IFormatProvider provider, string format, params object[] args)
        {
            if (IsFatalEnabled)
            {
                Logger.Log(ThisDeclaringType, LoggingLevel.FATAL, new SystemStringFormat(provider, format, args), null, 0);
            }
        }

        #endregion

        // ********************************************************************************
        #region Is Enabled
        // ********************************************************************************

        virtual public bool IsDebugEnabled
        {
            get { return Logger.IsEnabledFor(LoggingLevel.DEBUG); }
        }

        virtual public bool IsInfoEnabled
        {
            get { return Logger.IsEnabledFor(LoggingLevel.INFO); }
        }

        virtual public bool IsWarnEnabled
        {
            get { return Logger.IsEnabledFor(LoggingLevel.WARN); }
        }

        virtual public bool IsErrorEnabled
        {
            get { return Logger.IsEnabledFor(LoggingLevel.ERROR); }
        }

        virtual public bool IsFatalEnabled
        {
            get { return Logger.IsEnabledFor(LoggingLevel.FATAL); }
        }

        #endregion

    }
}
