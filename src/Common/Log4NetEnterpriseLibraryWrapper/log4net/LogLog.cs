using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESPLogWrapper
{
    public static class LogLog
    {
        private const string PREFIX = "Log4NetEnterpriseLibraryWrapper: ";
        private const string ERR_PREFIX = "Log4NetEnterpriseLibraryWrapper:ERROR ";
        private const string WARN_PREFIX = "Log4NetEnterpriseLibraryWrapper:WARN ";

        public static void Debug(string message)
        {
            Debug(message, null);
        }

        public static void Debug(string message, Exception exception)
        {
            EmitOutLine(PREFIX + message);
            if (exception != null)
            {
                EmitOutLine(exception.ToString());
            }
        }

        public static void Warn(string message)
        {
            Warn(message, null);
        }

        public static void Warn(string message, Exception exception)
        {
            EmitErrorLine(WARN_PREFIX + message);
            if (exception != null)
            {
                EmitErrorLine(exception.ToString());
            }
        }

        public static void Error(string message)
        {
            Error(message, null);
        }

        public static void Error(string message, Exception exception)
        {
            EmitErrorLine(ERR_PREFIX + message);
            if (exception != null)
            {
                EmitErrorLine(exception.ToString());
            }
        }

        private static void EmitOutLine(string message)
        {
            try
            {
                Console.Out.WriteLine(message);
                System.Diagnostics.Debug.WriteLine(message);
                System.Diagnostics.Trace.WriteLine(message);
            }
            catch
            {
                //Ignore exception by design
            }
        }

        private static void EmitErrorLine(string message)
        {
            try
            {
                Console.Error.WriteLine(message);
                System.Diagnostics.Debug.WriteLine(message);
                System.Diagnostics.Trace.WriteLine(message);
            }
            catch
            {
                //Ignore exception by design
            }
        }
    }
}
