using System;

namespace ESPLogWrapper
{
    public interface ILog
    {
        void Debug(object message, int eventId = 0);
        void Debug(Type callerStackBoundaryDeclaringType, object message, int eventId = 0);
        void Debug(object message, Exception exception, int eventId = 0);

        void DebugFormat(string format, params object[] args);
        void DebugFormat(Type callerStackBoundaryDeclaringType, string format, params object[] args);
        void DebugFormat(IFormatProvider provider, string format, params object[] args);


        void Info(object message, int eventId = 0);
        void Info(Type callerStackBoundaryDeclaringType, object message, int eventId = 0);
        void Info(object message, Exception exception, int eventId = 0);

        void InfoFormat(string format, params object[] args);
        void InfoFormat(Type callerStackBoundaryDeclaringType, string format, params object[] args);
        void InfoFormat(IFormatProvider provider, string format, params object[] args);


        void Warn(object message, int eventId = 0);
        void Warn(Type callerStackBoundaryDeclaringType, object message, int eventId = 0);
        void Warn(object message, Exception exception, int eventId = 0);

        void WarnFormat(string format, params object[] args);
        void WarnFormat(Type callerStackBoundaryDeclaringType, string format, params object[] args);
        void WarnFormat(IFormatProvider provider, string format, params object[] args);


        void Error(object message, int eventId = 0);
        void Error(Type callerStackBoundaryDeclaringType, object message, int eventId = 0);
        void Error(object message, Exception exception, int eventId = 0);
        void Error(Type callerStackBoundaryDeclaringType, object message, Exception exception, int eventId = 0);

        void ErrorFormat(string format, params object[] args);
        void ErrorFormat(Type callerStackBoundaryDeclaringType, string format, params object[] args);
        void ErrorFormat(IFormatProvider provider, string format, params object[] args);


        void Fatal(object message, int eventId = 0);
        void Fatal(Type callerStackBoundaryDeclaringType, object message, int eventId = 0);
        void Fatal(object message, Exception exception, int eventId = 0);
        void Fatal(Type callerStackBoundaryDeclaringType, object message, Exception exception, int eventId = 0);

        void FatalFormat(string format, params object[] args);
        void FatalFormat(Type callerStackBoundaryDeclaringType, string format, params object[] args);
        void FatalFormat(IFormatProvider provider, string format, params object[] args);


        bool IsDebugEnabled { get; }
        bool IsInfoEnabled { get; }
        bool IsWarnEnabled { get; }
        bool IsErrorEnabled { get; }
        bool IsFatalEnabled { get; }
    }
}
