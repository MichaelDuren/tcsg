using Log4NetEnterpriseLibraryWrapper;
using System;

namespace ESPLogWrapper
{
    public interface ILogger
    {
        string Name { get; }

        void Log(Type callerStackBoundaryDeclaringType, LoggingLevel level, object message, Exception exception, int eventId);
        void Log(LocationInfo locationInfo, LoggingLevel level, object message, Exception exception, int eventId);

        bool IsEnabledFor(LoggingLevel level);
    }
}
