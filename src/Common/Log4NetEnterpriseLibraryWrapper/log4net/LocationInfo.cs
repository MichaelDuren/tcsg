using System;
using System.Diagnostics;

namespace ESPLogWrapper
{
    public class LocationInfo
    {
        private const string NA = "?";

        private readonly string m_className;

        public string ClassName
        {
            get { return m_className; }
        }

        private readonly string m_fileName;

        public string FileName
        {
            get { return m_fileName; }
        }

        private readonly string m_lineNumber;

        public string LineNumber
        {
            get { return m_lineNumber; }
        }

        private readonly string m_methodName;

        public string MethodName
        {
            get { return m_methodName; }
        }

        public string FullInfo
        {
            get
            {
                return string.Format("[ {0,-45} ][ {1,-40} ]",
                    GetRightmost(this.ClassName, 45),
                    GetRightmost(string.Format("{0}():{1}", this.MethodName, LineNumber), 40)
                    );
            }
        }


        public LocationInfo(string className, string methodName, string fileName, string lineNumber)
        {
            m_className = className;
            m_fileName = fileName;
            m_lineNumber = lineNumber;
            m_methodName = methodName;
        }

        public LocationInfo(Type callerStackBoundaryDeclaringType)
        {
            m_className = NA;
            m_fileName = NA;
            m_lineNumber = NA;
            m_methodName = NA;

            if (callerStackBoundaryDeclaringType != null)
            {
                try
                {
                    StackTrace st = new StackTrace(true);
                    int frameIndex = 0;

                    while (frameIndex < st.FrameCount)
                    {
                        StackFrame frame = st.GetFrame(frameIndex);
                        if (frame != null && frame.GetMethod().DeclaringType == callerStackBoundaryDeclaringType)
                        {
                            break;
                        }
                        frameIndex++;
                    }

                    while (frameIndex < st.FrameCount)
                    {
                        StackFrame frame = st.GetFrame(frameIndex);
                        if (frame != null && frame.GetMethod().DeclaringType != callerStackBoundaryDeclaringType)
                        {
                            break;
                        }
                        frameIndex++;
                    }

                    if (frameIndex < st.FrameCount)
                    {
                        StackFrame locationFrame = st.GetFrame(frameIndex);

                        if (locationFrame != null)
                        {
                            System.Reflection.MethodBase method = locationFrame.GetMethod();

                            if (method != null)
                            {
                                m_methodName = method.Name;
                                if (method.DeclaringType != null)
                                {
                                    m_className = method.DeclaringType.FullName;
                                }
                            }
                            m_fileName = locationFrame.GetFileName();
                            m_lineNumber = locationFrame.GetFileLineNumber().ToString(System.Globalization.NumberFormatInfo.InvariantInfo);
                        }
                    }
                }
                catch (System.Security.SecurityException)
                {
                    LogLog.Debug("LocationInfo: Security exception while trying to get caller stack frame.  Error ignored.  Location information not available.");
                }
            }
        }

        public static string GetRightmost(string s, int length)
        {
            if (s == null) { return null; }

            int stringLength = s.Length;

            if (length >= stringLength)
            {
                return s;
            }

            return s.Substring(stringLength - length, length);
        }
    }
}
