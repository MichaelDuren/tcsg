using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ESPLogWrapper;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace Log4NetEnterpriseLibraryWrapper
{
    public enum LoggingLevel
    {
        DEBUG, INFO, WARN, ERROR, FATAL
    }

    public class LoggerWrapper : ILogger
    {
        private const string CLASS_NAME = "ClassName";
        private const string METHOD_NAME = "MethodName";
        private const string LINE_NUMBER = "LineNumber";

        private const string ALL_CAPS_SEVERITY = "AllCapsSeverity";
        private const string CODE_LOCATION_INFO = "CodeLocationInfo";

        private const string DEBUG = "DEBUG";
        private const string INFO = "INFO ";
        private const string WARN = "WARN ";
        private const string ERROR = "ERROR";
        private const string FATAL = "FATAL";

        public string Name { get; set; }

        public LoggerWrapper(string name)
        {
            this.Name = name;
        }

        public void Log(Type callerStackBoundaryDeclaringType, LoggingLevel level, object message, Exception exception, int eventId)
        {
            try
            {
                LogEntry logEntry = InitializeLogEntry(level);

                if (logEntry == null) { return; }

                logEntry.TimeStamp = DateTime.Now;

                logEntry.Message = string.Empty;
                if (message != null)
                {
                    logEntry.Message = message.ToString();
                }

                if (exception != null)
                {
                    logEntry.Message += string.Format("\n{0}", exception.ToString());
                }

                LocationInfo locationInfo = new LocationInfo(callerStackBoundaryDeclaringType);
                logEntry.ExtendedProperties.Add(new KeyValuePair<string, object>(CLASS_NAME, locationInfo.ClassName));
                logEntry.ExtendedProperties.Add(new KeyValuePair<string, object>(METHOD_NAME, locationInfo.MethodName));
                logEntry.ExtendedProperties.Add(new KeyValuePair<string, object>(LINE_NUMBER, locationInfo.LineNumber));
                logEntry.ExtendedProperties.Add(new KeyValuePair<string, object>(CODE_LOCATION_INFO, locationInfo.FullInfo));
                logEntry.EventId = eventId;

                Logger.Write(logEntry);
            }
            catch (Exception e)
            {
                LogLog.Error(string.Format("Exception writing log entry to log.  Message: {0}", message), e);
            }
        }

        public void Log(LocationInfo locationInfo, LoggingLevel level, object message, Exception exception, int eventId)
        {
            try
            {
                LogEntry logEntry = InitializeLogEntry(level);

                if (logEntry == null) { return; }

                logEntry.TimeStamp = DateTime.Now;

                logEntry.Message = string.Empty;
                if (message != null)
                {
                    logEntry.Message = message.ToString();
                }

                if (exception != null)
                {
                    logEntry.Message += string.Format("\n{0}", exception.ToString());
                }

                logEntry.ExtendedProperties.Add(new KeyValuePair<string, object>(CLASS_NAME, locationInfo.ClassName));
                logEntry.ExtendedProperties.Add(new KeyValuePair<string, object>(METHOD_NAME, locationInfo.MethodName));
                logEntry.ExtendedProperties.Add(new KeyValuePair<string, object>(LINE_NUMBER, locationInfo.LineNumber));
                logEntry.ExtendedProperties.Add(new KeyValuePair<string, object>(CODE_LOCATION_INFO, locationInfo.FullInfo));

                Logger.Write(logEntry);
            }
            catch (Exception e)
            {
                LogLog.Error(string.Format("Exception writing log entry to log.  Message: {0}", message), e);
            }
        }

        private void SetSeverityAndPriority(LogEntry logEntry, LoggingLevel level)
        {
            string allCapsSeverity;

            switch (level)
            {
                case LoggingLevel.DEBUG:
                    logEntry.Severity = System.Diagnostics.TraceEventType.Verbose;
                    allCapsSeverity = DEBUG;
                    break;

                case LoggingLevel.INFO:
                    logEntry.Severity = System.Diagnostics.TraceEventType.Information;
                    allCapsSeverity = INFO;
                    break;

                case LoggingLevel.WARN:
                    logEntry.Severity = System.Diagnostics.TraceEventType.Warning;
                    allCapsSeverity = WARN;
                    break;

                case LoggingLevel.ERROR:
                    logEntry.Severity = System.Diagnostics.TraceEventType.Error;
                    allCapsSeverity = ERROR;
                    break;

                case LoggingLevel.FATAL:
                    logEntry.Severity = System.Diagnostics.TraceEventType.Critical;
                    allCapsSeverity = FATAL;
                    break;

                default:
                    throw new ArgumentOutOfRangeException(string.Format("Invalid Logging Level: {0}", level));
            }

            logEntry.Priority = (int)level;
            logEntry.ExtendedProperties.Add(new KeyValuePair<string, object>(ALL_CAPS_SEVERITY, allCapsSeverity));
        }


        public bool IsEnabledFor(LoggingLevel level)
        {
            LogEntry logEntry = InitializeLogEntry(level);
            if (logEntry == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public LogEntry InitializeLogEntry(LoggingLevel level)
        {
            if (!Logger.IsLoggingEnabled()) { return null; }

            LogEntry logEntry = new LogEntry();

            SetSeverityAndPriority(logEntry, level);
            logEntry.Categories = new List<string>(1);
            logEntry.Categories.Add(this.Name);

            if (!Logger.ShouldLog(logEntry)) { return null; }

            return logEntry;
        }
    }
}
