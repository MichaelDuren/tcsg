﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Util.Reports
{
    /// <summary>
    /// Assumptions about usage:
    ///     This class supports a serial creation of the report (ie. report output is created serially as you progress through the report)
    ///     First create all column headings by calling CreateColumnHeading() for each column
    ///     Call NewRow() before EVERY new row - including the first row
    ///     Call one of the cell methods for every cell that you want to create - even empty ones (by calling CreateEmptyCell())
    /// </summary>
    public abstract class ReportWriterBase : IDisposable
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected bool GridStarted { get; set; }

        // Default (empty) implementation of Dispose.
        public virtual void Dispose() { }

        // ********************************************************************************
        #region Grid Methods
        // ********************************************************************************

        public abstract void StartGrid();

        public abstract void EndGrid();

        protected void ConfirmInGrid()
        {
            if (!GridStarted)
            {
                throw new InvalidOperationException("Cannot do Grid Operations before calling StartGrid()");
            }
        }

        protected void ConfirmNotInGrid()
        {
            if (GridStarted)
            {
                throw new InvalidOperationException("Only Grid Operations are allowed while in a Grid.  Call EndGrid() to end the grid before calling non-grid operations.");
            }
        }

        /// <summary>
        /// Indicates the beginning of a new row (must be called before writing columns to a new row)
        /// </summary>
        public abstract void NewRow();

        // ****************************************
        #region Column Heading methods
        // ****************************************

        public void CreateColumnHeading(string columnName, params ReportStyleOption[] styleOptions)
        {
            SetCellTextValue(columnName, MergeStyleOptions(styleOptions, ReportStyleOption.Style_Heading));
        }

        #endregion

        // ****************************************
        #region Set Empty Cell methods
        // ****************************************

        /// <summary>
        /// Creates an empty cell.  Needed in case a cell needs to be skipped
        /// </summary>
        public void SetEmptyCell(params ReportStyleOption[] styleOptions)
        {
            SetCellValue(string.Empty, styleOptions);
        }

        #endregion

        // ****************************************
        #region Set Cell Text methods
        // ****************************************

        public void SetCellTextValue(string value, params ReportStyleOption[] styleOptions)
        {
            SetCellTextValue(value, null, styleOptions);
        }

        public void SetCellTextValue(string value, string format = null, params ReportStyleOption[] styleOptions)
        {
            SetCellTextValue(value, format, null, styleOptions);
        }

        public void SetCellTextValue(string value, string format = null, string hyperLinkFormat = null, params ReportStyleOption[] styleOptions)
        {
            SetCellValue(CellFormatType.Text, value, format, hyperLinkFormat, styleOptions);
        }

        #endregion

        // ****************************************
        #region Set Cell Date methods
        // ****************************************

        public void SetCellDateValue(DateTime? value, params ReportStyleOption[] styleOptions)
        {
            SetCellDateValue(value, null, styleOptions);
        }

        public void SetCellDateValue(DateTime? value, string format = null, params ReportStyleOption[] styleOptions)
        {
            SetCellDateValue(value, format, null, styleOptions);
        }

        public void SetCellDateValue(DateTime? value, string format = null, string hyperLinkFormat = null, params ReportStyleOption[] styleOptions)
        {
            SetCellValue(CellFormatType.Date, value, format, hyperLinkFormat, MergeStyleOptions(styleOptions, ReportStyleOption.AlignLeft));
        }

        #endregion

        // ****************************************
        #region Set Cell Boolean methods
        // ****************************************

        public void SetCellBooleanValue(bool? value, params ReportStyleOption[] styleOptions)
        {
            SetCellBooleanValue(value, null, styleOptions);
        }

        public void SetCellBooleanValue(bool? value, string format = null, params ReportStyleOption[] styleOptions)
        {
            SetCellBooleanValue(value, format, null, styleOptions);
        }

        public void SetCellBooleanValue(bool? value, string format = null, string hyperLinkFormat = null, params ReportStyleOption[] styleOptions)
        {
            string displayValue;
            if (value == null) { displayValue = string.Empty; }
            else if (value == true) { displayValue = "Yes"; }
            else { displayValue = "No"; }

            SetCellValue(CellFormatType.Boolean, displayValue, format, hyperLinkFormat, MergeStyleOptions(styleOptions, ReportStyleOption.AlignCenter));
        }

        #endregion

        // ****************************************
        #region Set Cell Integer methods
        // ****************************************

        public void SetCellIntegerValue(int? value, params ReportStyleOption[] styleOptions)
        {
            string defaultFormat = "{0:#,###,##0}";
            SetCellIntegerValue(value, defaultFormat, styleOptions);
        }

        public void SetCellIntegerValue(int? value, string format = null, params ReportStyleOption[] styleOptions)
        {
            SetCellIntegerValue(value, format, null, styleOptions);
        }

        public void SetCellIntegerValue(int? value, string format = null, string hyperLinkFormat = null, params ReportStyleOption[] styleOptions)
        {
            SetCellValue(CellFormatType.Integer, value, format, hyperLinkFormat, MergeStyleOptions(styleOptions, ReportStyleOption.AlignRight));
        }

        #endregion

        // ****************************************
        #region Set Cell Double methods
        // ****************************************

        /// <summary>
        /// Outputs a Double Value. Defaults to 2 decimal places
        /// </summary>
        public void SetCellDoubleValue(double? value, params ReportStyleOption[] styleOptions)
        {
            string defaultFormat = "{0:#,###,##0.00}";
            SetCellDoubleValue(value, defaultFormat, styleOptions);
        }

        public void SetCellDoubleValue(double? value, int decimals, params ReportStyleOption[] styleOptions)
        {
            SetCellDoubleValue(value, decimals, null, styleOptions);
        }

        public void SetCellDoubleValue(double? value, int decimals, string hyperLinkFormat = null, params ReportStyleOption[] styleOptions)
        {
            string defaultFormat = string.Format("{{0:#,###,##0.{0}}}", StringTools.PadFill("", "0", decimals));
            SetCellDoubleValue(value, defaultFormat, hyperLinkFormat, styleOptions);
        }

        public void SetCellDoubleValue(double? value, string format = null, params ReportStyleOption[] styleOptions)
        {
            SetCellDoubleValue(value, format, null, styleOptions);
        }

        public void SetCellDoubleValue(double? value, string format = null, string hyperLinkFormat = null, params ReportStyleOption[] styleOptions)
        {
            SetCellValue(CellFormatType.Double, value, format, hyperLinkFormat, MergeStyleOptions(styleOptions, ReportStyleOption.AlignRight));
        }

        #endregion

        // ****************************************
        #region Set Cell Value methods
        // ****************************************

        public void SetCellValue(object value, params ReportStyleOption[] styleOptions)
        {
            SetCellValue(CellFormatType.Value, value, format: null, styleOptions: styleOptions);
        }

        public void SetCellValue(object value, string format = null, params ReportStyleOption[] styleOptions)
        {
            SetCellValue(value, format, null, styleOptions);
        }

        public void SetCellValue(object value, string format = null, string hyperLinkFormat = null, params ReportStyleOption[] styleOptions)
        {
            SetCellValue(CellFormatType.Value, value, format, hyperLinkFormat, styleOptions);
        }

        protected abstract void SetCellValue(CellFormatType cellFormatType, object value, string format = null, string hyperLinkFormat = null, params ReportStyleOption[] styleOptions);

        #endregion
        #endregion

        // ********************************************************************************
        #region Style Methods
        // ********************************************************************************

        public ReportStyleOption[] MergeStyleOptions(ReportStyleOption[] styleOptions_1, params ReportStyleOption[] styleOptions_2)
        {
            return MergeStyleOptions(styleOptions_1, false, styleOptions_2);
        }

        public ReportStyleOption[] MergeStyleOptions(ReportStyleOption[] styleOptions_1, bool paramsHavePriority, params ReportStyleOption[] styleOptions_2)
        {
            // If Params have priority - then load params AFTER the initial set of Style Options - so that they will have priority when they are applied
            if (paramsHavePriority)
            {
                List<ReportStyleOption> list = new List<ReportStyleOption>(styleOptions_1);
                list.AddRange(styleOptions_2);
                return list.ToArray();
            }
            else
            {
                List<ReportStyleOption> list = new List<ReportStyleOption>(styleOptions_2);
                list.AddRange(styleOptions_1);
                return list.ToArray();
            }
        }

        #endregion

        // ********************************************************************************
        #region Output Methods
        // ********************************************************************************

        public abstract string GetReportContent();

        public virtual void SaveToFile(string directory, string fileName, bool force = true)
        {
            string fileNameAndPath = fileName;

            if (!StringTools.IsBlankOrNull(directory))
            {
                fileNameAndPath = Path.Combine(directory, fileName);
            }

            FileTools.SaveStringToFile(this.GetReportContent(), fileNameAndPath, force);
        }

        #endregion

        // ********************************************************************************
        #region Write Unstructured Content Methods
        // ********************************************************************************

        public virtual void WriteUnstructuredContent(object value, params ReportStyleOption[] styleOptions)
        {
            WriteUnstructuredContent(value, null, styleOptions);
        }

        /// <summary>
        /// Supports allowing unstructured content to be added to the report.
        /// This can NOT be part of a "Grid" - it must be created either before or after the creation of a grid
        /// </summary>
        public abstract void WriteUnstructuredContent(object value, string format = null, params ReportStyleOption[] styleOptions);

        #endregion

    }
}
