﻿using System;

namespace Util.Reports
{
    public class CsvReportWriter : ReportWriterBase
    {
        // ********************************************************************************
        #region Properties and Constructors
        // ********************************************************************************

        private string rowDelimiter = ",";

        private StringDelimiter row;
        private StringDelimiter rows = new StringDelimiter(Environment.NewLine);

        public string RowDelimiter
        {
            get { return rowDelimiter; }
            set
            {
                rowDelimiter = value;
                row = new StringDelimiter(rowDelimiter);
            }
        }

        public CsvReportWriter()
        {
            row = new StringDelimiter(rowDelimiter);
        }

        public CsvReportWriter(string rowDelimiter)
            : this()
        {
            this.RowDelimiter = RowDelimiter;
        }

        #endregion

        // ********************************************************************************
        #region Initialize / Finalize report sections methods
        // ********************************************************************************

        public override void StartGrid()
        {
            Guard.FailIfTrue(GridStarted, "Cannot Start Grid - a grid has already been started.  First call EndGrid()");
            GridStarted = true;
        }

        public override void EndGrid()
        {
            Guard.FailIfTrue(!GridStarted, "Cannot End Grid - a grid has not been started.  First call StartGrid()");
            GridStarted = false;
        }

        #endregion

        // ********************************************************************************
        #region Row / Cell Methods
        // ********************************************************************************

        /// <summary>
        /// Indicates the beginning of a new row (must be called before writing columns to a new row
        /// </summary>
        public override void NewRow()
        {
            rows.Add(row.ToString());
            row = new StringDelimiter(rowDelimiter);
        }

        protected override void SetCellValue(CellFormatType cellFormatType, object value, string format = null, string hyperLinkFormat = null, params ReportStyleOption[] styleOptions)
        {
            // Note: CellFormatType, Style Options and hyperLinkFormat are ignored for CSV output

            // Apply formatting if applicable
            if (!StringTools.IsBlankOrNull(format))
            {
                value = string.Format(format, value);
            }

            row.Add(value);
        }

        #endregion

        /// <summary>
        /// Supports allowing unstructured content to be added to the report.
        /// This can NOT be part of a "Grid" - it must be created either before or after the creation of a grid
        /// </summary>
        public override void WriteUnstructuredContent(object value, string format = null, params ReportStyleOption[] styleOptions)
        {
            // Note: Style Options are ignored for CSV output

            ConfirmNotInGrid();

            // Apply formatting if applicable
            if (!StringTools.IsBlankOrNull(format))
            {
                value = string.Format(format, value);
            }

            rows.Add(value);
        }

        // ********************************************************************************
        #region General Methods
        // ********************************************************************************

        /// <summary>
        /// Returns the contents of the Report
        /// </summary>
        public override string GetReportContent()
        {
            if (GridStarted)
            {
                EndGrid();
            }

            return rows.ToString();
        }

        #endregion
    }
}
