﻿using System;
using System.Collections.Generic;
using Util.Sql;

namespace Util.Reports
{
    public class AdHocReportDefinition
    {
        public string ReportTitle { get; set; }

        /// <summary>
        /// If true (default) it will parse the column heading names from the supplied field names
        /// </summary>
        public bool ParseColumnHeadings { get; set; }

        public List<AdHocReportColumnDefinition> ReportColumnDefinitions { get; set; }

        // ***********************************************
        // For Data Source Type = SQL
        // ***********************************************

        /// <summary>
        /// SQL for getting the data for the report
        /// </summary>
        public string Sql { get; set; }

        /// <summary>
        /// Data Parameters to use with the SQL to specifiy paramters to pass to the DB
        /// Only used if SQL Specified.
        /// </summary>
        public List<DataParameter> DataParameters { get; set; }

        /// <summary>
        /// If specified, this determines the comand timeout to use for the Select
        /// Only used if SQL Specified.
        /// </summary>
        public int? CommandTimeout { get; set; }

        // ********************************************************************************
        #region Constructors
        // ********************************************************************************

        public AdHocReportDefinition()
        {
            this.DataParameters = new List<DataParameter>();
            this.ReportColumnDefinitions = new List<AdHocReportColumnDefinition>();
            this.ParseColumnHeadings = true;
        }

        public AdHocReportDefinition(string reportTitle)
            : this()
        {
            Guard.ArgumentNotNull(reportTitle, "reportTitle");

            this.ReportTitle = reportTitle;
        }

        public AdHocReportDefinition(string reportTitle, string sql, List<DataParameter> dataParameters = null)
            : this(reportTitle)
        {
            Guard.ArgumentNotNull(sql, "sql");

            //this.dataSourceType = AdHocReportDataSourceType.Sql;

            this.Sql = sql;
            this.DataParameters = dataParameters;
        }

        #endregion
    }

    public class AdHocReportColumnDefinition
    {
        /// <summary>
        /// This is the name of the column as defined in the SQL query.
        /// </summary>
        public string ColumnName { get; internal set; }

        /// <summary>
        /// This is the text to be used as the column heading.  If not specified - the column heading will be derived from parsing the Column Name
        /// </summary>
        public string ColumnHeading { get; set; }

        /// <summary>
        /// If specified - these style options will be applied to the Column Heading for this column
        /// </summary>
        public List<ReportStyleOption> HeadingStyleOptions { get; set; }

        /// <summary>
        /// If specified - these style options will be applied to each Cell for this column
        /// </summary>
        public List<ReportStyleOption> CellStyleOptions { get; set; }

        /// <summary>
        /// If specified - this Format value will be used for formatting the Cell
        /// </summary>
        public string CellFormat { get; set; }

        /// <summary>
        /// If specified - then the cell will be formatted as a HyperLink - with this value as the link - using standard string format syntax to format the link
        /// </summary>
        public string HyperLinkFormat { get; set; }

        internal AdHocReportColumnDefinition()
        {
            this.HeadingStyleOptions = new List<ReportStyleOption>();
            this.CellStyleOptions = new List<ReportStyleOption>();
        }

        public AdHocReportColumnDefinition(string columnName)
            : this()
        {
            Guard.ArgumentNotNull(columnName, "columnName");
            this.ColumnName = columnName;
        }
    }
}
