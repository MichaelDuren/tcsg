﻿using System.Collections.Generic;

namespace Util.Reports
{
    public class StandardValidators
    {
        // ********************************************************************************
        #region Properties, etc.
        // ********************************************************************************

        public List<PropertyError> PropertyErrors { get; private set; }

        /// <summary>
        /// Return a bool based on if there are any errors
        /// </summary>
        public bool IsValid
        {
            get
            {
                if (PropertyErrors.Count == 0)
                {
                    return true;
                }
                return false;
            }
        }

        #endregion

        public StandardValidators()
        {
            PropertyErrors = new List<PropertyError>();
        }

        // ********************************************************************************
        #region Methods
        // ********************************************************************************

        public void AddPropertyError(string propertyName, string message)
        {
            Guard.ArgumentNotBlankOrNull(propertyName, "propertyName");

            PropertyErrors.Add(new PropertyError
            {
                PropertyName = propertyName,
                Message = message
            });
        }

        public string GetPropertyErrorMessages()
        {
            StringDelimiter sd = new StringDelimiter("\n");

            foreach (var pe in PropertyErrors)
            {
                sd.Add("Error in the {0} field. {1}", pe.PropertyName, pe.Message);
            }

            return sd.ToString();
        }

        #endregion

        // ********************************************************************************
        #region Validate Messages
        // ********************************************************************************

        /// <summary>
        /// Checks the value to ensure it isn't blank or null
        /// </summary>
        public void ValidateNotBlankOrNull(object value, string propertyName, string errorMessage)
        {
            if (StringTools.IsBlankOrNull(value))
            {
                AddPropertyError(propertyName, errorMessage);
            }
        }

        /// <summary>
        /// Checks the value to ensure it isn't null
        /// </summary>
        public void ValidateNotNull(object value, string propertyName, string errorMessage)
        {
            if (value == null)
            {
                AddPropertyError(propertyName, errorMessage);
            }
        }

        /// <summary>
        /// Checks the condition to ensure that it is false
        /// </summary>
        public void ValidateFailIfTrue(bool condition, string propertyName, string errorMessage)
        {
            if (condition)
            {
                AddPropertyError(propertyName, errorMessage);
            }
        }

        /// <summary>
        /// Checks the condition to ensure that it is true
        /// </summary>
        public void ValidateFailIfFalse(bool condition, string propertyName, string errorMessage)
        {
            if (!condition)
            {
                AddPropertyError(propertyName, errorMessage);
            }
        }

        #endregion
    }

    public class PropertyError
    {
        public string PropertyName { get; set; }
        public string Message { get; set; }
    }
}
