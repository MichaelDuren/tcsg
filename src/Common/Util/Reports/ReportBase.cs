﻿using System;
using System.Web;
using Util.Config;

namespace Util.Reports
{
    public abstract class ReportBase : IReport, IDisposable
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // Flag indicating that this object has been disposed
        protected bool disposed;

        // ********************************************************************************
        #region Properties, etc.
        // ********************************************************************************

        protected bool ReportIsRunning = false;

        private string _reportTitle;
        public string ReportTitle
        {
            get { return _reportTitle; }
            set
            {
                if (ReportIsRunning)
                {
                    throw new InvalidOperationException("Cannot change ReportTitle while the report is running");
                }
                _reportTitle = value;
            }
        }

        private ReportWriterType _reportWriterType;
        public ReportWriterType ReportWriterType
        {
            get { return _reportWriterType; }
            set
            {
                if (ReportIsRunning)
                {
                    throw new InvalidOperationException("Cannot change ReportWriterType while the report is running");
                }
                _reportWriterType = value;
            }
        }

        public ReportWriterBase ReportWriter { get; private set; }

        public string ReportFooter { get; set; }

        public bool SuppressHeaderAndFooter { get; set; }

        #endregion

        // ********************************************************************************
        #region Constructors, etc.
        // ********************************************************************************

        public ReportBase() { }

        public ReportBase(ReportWriterType reportWriterType)
        {
            this.ReportWriterType = reportWriterType;
        }

        public ReportBase(ReportWriterType reportWriterType, string reportTitle)
            : this(reportWriterType)
        {
            this.ReportTitle = reportTitle;
        }

        // Finalizer
        ~ReportBase()
        {
            this.Dispose();
        }

        public void Dispose()
        {
            if (!this.disposed)
            {
                this.disposed = true;

                if (ReportWriter != null)
                {
                    ReportWriter.Dispose();
                    ReportWriter = null;
                }

                GC.SuppressFinalize(this);
            }
        }

        #endregion

        // ********************************************************************************
        #region Core Methods
        // ********************************************************************************

        /// <summary>
        /// Calls the Initialize and then the Run methods in the implementing class.
        /// Additionally - tracks and populates the reportRunStatus.ExecutionTime
        /// </summary>
        public virtual ReportRunStatus InitializeAndRun()
        {
            try
            {
                // Validate
                StandardValidators validators = ValidateCriteria();
                Guard.ArgumentNotNull(validators, "Something is wrong.  StandardValidators should never return null.");
                if (!validators.IsValid)
                {
                    return ReportRunStatus.FailValidation(validators);
                }

                DateTime startTime = DateTime.Now;

                Initialize();

                ValidateRunState();

                ReportIsRunning = true;

                InitializeReportWriter();

                // Create Header
                if (!SuppressHeaderAndFooter && ReportWriterType != Reports.ReportWriterType.Csv)
                {
                    CreateReportHeader();
                }

                ReportWriter.StartGrid();

                // Run the report
                ReportRunStatus runStatus = Run();

                ReportWriter.EndGrid();

                // Create Footer
                if (!SuppressHeaderAndFooter)
                {
                    CreateReportFooter();
                }

                // Calculate Elaped Time
                DateTime endTime = DateTime.Now;
                runStatus.ExecutionTime = endTime - startTime;

                log.InfoFormat("Report: {0} ran in: ({1})", ReportTitle, runStatus.ExecutionTime);

                return runStatus;
            }
            catch (Exception e)
            {
                log.Error("Exception occurred running report", e);
                throw new Exception("Exception occurred running report", e);
            }
            finally
            {
                ReportIsRunning = false;
            }
        }

        /// <summary>
        /// Validates that the report is in the correct state to be run
        /// </summary>
        protected virtual void ValidateRunState()
        {
            Guard.ArgumentNotNull(ReportWriterType, "ReportWriterType");
            Guard.ArgumentNotNull(ReportTitle, "ReportTitle");
        }

        private void InitializeReportWriter()
        {
            switch (ReportWriterType)
            {
                case ReportWriterType.Html:
                    ReportWriter = new HtmlReportWriter();
                    break;

                case ReportWriterType.Csv:
                    ReportWriter = new CsvReportWriter();
                    break;

                default:
                    throw new InvalidOperationException("Invalid ReportWriterType: " + ReportWriterType);
                //break;
            }
        }

        #endregion

        // ********************************************************************************
        #region Save / Output related methods
        // ********************************************************************************

        /// <summary>
        /// Saves the report output as a file - the file uses an extension of .xls - so that it opens in Excel.
        /// This should work for both the "csv" and the "html" versions of a report
        /// </summary>
        public virtual void Save(string directory = null, string fileName = null, ReportSaveType reportSaveType = ReportSaveType.Excel)
        {
            fileName = AdjustFileName(fileName, reportSaveType);

            // Do the actual save of the file
            this.ReportWriter.SaveToFile(directory, fileName, force: true);
        }

        /// <summary>
        /// Saves the ReportWriter results to the HttpResponse to be opened in Excel.
        /// The baseFileName should be just a simple file name - NOT a full path.
        /// </summary>
        public virtual void Save(HttpResponseBase response, string fileName = null, ReportSaveType reportSaveType = ReportSaveType.Excel)
        {
            Guard.ArgumentNotNull(response, "response");

            fileName = AdjustFileName(fileName, reportSaveType);
            response.AddHeader("Content-Disposition", string.Format("attachment;filename=/{0}", fileName));

            switch (reportSaveType)
            {
                case ReportSaveType.Csv:
                    response.ContentType = "text/csv";
                    break;

                case ReportSaveType.Html:
                    // No need to set anything
                    break;

                case ReportSaveType.Excel:
                    //response.ContentType = "application/excel";
                    response.ContentType = "application/vnd.ms-excel";
                    break;

                case ReportSaveType.Word:
                    response.ContentType = "application/msword";
                    break;

                default:
                    throw new ArgumentOutOfRangeException("Should never get here.");
            }

            response.Write(this.ReportWriter.GetReportContent());
            response.End();
        }

        private string AdjustFileName(string fileName, ReportSaveType reportSaveType)
        {
            // If the file name is not specified - then create a file name based on convention
            if (StringTools.IsBlankOrNull(fileName))
            {
                string environmentPrefix = string.Empty;

                if (!DynamicConfigurationManager.IsProduction)
                {
                    environmentPrefix = string.Format("({0}) ", DynamicConfigurationManager.Environment);
                }

                fileName = string.Format("{0}{1} {2:yyyy-MM-dd HH.mm.ss}", environmentPrefix, this.ReportTitle, DateTime.Now);
            }

            fileName = fileName.Trim();

            switch (reportSaveType)
            {
                case ReportSaveType.Excel:
                    // NO GUARD - Either CSV or HTML can save to Excel

                    if (!fileName.ToLower().EndsWith(".xls"))
                    {
                        fileName += ".xls";
                    }
                    break;

                case ReportSaveType.Word:
                    Guard.FailIfTrue(this.ReportWriterType != Reports.ReportWriterType.Html, "Report Writer Type must be Html to save as Word");

                    if (!fileName.ToLower().EndsWith(".doc"))
                    {
                        fileName += ".doc";
                    }
                    break;

                case ReportSaveType.Csv:
                    Guard.FailIfTrue(this.ReportWriterType != Reports.ReportWriterType.Csv, "Report Writer Type must be CSV to save as CSV");

                    if (!fileName.ToLower().EndsWith(".csv"))
                    {
                        fileName += ".csv";
                    }
                    break;

                case ReportSaveType.Html:
                    Guard.FailIfTrue(this.ReportWriterType != Reports.ReportWriterType.Html, "Report Writer Type must be Html to save as HTML");

                    if (!fileName.ToLower().EndsWith(".html") && !fileName.ToLower().EndsWith(".htm"))
                    {
                        fileName += ".html";
                    }
                    break;

                default:
                    throw new ArgumentOutOfRangeException("Should never get here.");
            }

            return fileName;
        }

        /// <summary>
        /// Returns the Html for the report
        /// </summary>
        public virtual string GetHtml()
        {
            Guard.FailIfFalse(ReportWriterType == Util.Reports.ReportWriterType.Html, "Report writer type must be Html to Get Html");
            return this.ReportWriter.GetReportContent();
        }

        #endregion

        // ********************************************************************************
        #region Methods to override in the implementing classes
        // ********************************************************************************

        /// <summary>
        /// Validates the report criteria and returns a Standard Validators object which includes any erors, etc.
        /// This method should be overridden in an implementing class if there are any validations.
        /// The default implementation just return an initialized StandardValidators object.
        /// </summary>
        public virtual StandardValidators ValidateCriteria()
        {
            return new StandardValidators();
        }

        /// <summary>
        /// Does any initialization required to run the report.
        /// </summary>
        protected virtual void Initialize() { }

        /// <summary>
        /// Creates the actual report output
        /// </summary>
        protected abstract ReportRunStatus Run();

        protected virtual void CreateReportHeader()
        {
            ReportWriter.WriteUnstructuredContent(GetDefaultReportHeader(), ReportStyleOption.FontSize_10, ReportStyleOption.FontColor_Gray);
        }

        protected virtual string GetDefaultReportHeader()
        {
            string environmentPrefix = string.Empty;

            if (!DynamicConfigurationManager.IsProduction)
            {
                environmentPrefix = string.Format("({0}) ", DynamicConfigurationManager.Environment);
            }

            return string.Format("{0}{1} {2:yyyy-MM-dd HH:mm:ss}", environmentPrefix, this.ReportTitle, DateTime.Now);
        }

        protected virtual void CreateReportFooter()
        {
            if (!StringTools.IsBlankOrNull(this.ReportFooter))
            {
                ReportWriter.WriteUnstructuredContent(this.ReportFooter, ReportStyleOption.FontSize_10, ReportStyleOption.FontColor_Gray);
            }
        }

        #endregion
    }
}
