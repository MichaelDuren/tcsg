﻿
namespace Util.Reports
{
    public enum ReportCompletionState
    {
        Success,
        SuccessSaved,
        FailNoData,
        FailValidation,
        FailRun
    }

    public enum ReportWriterType
    {
        Html,
        Csv
        //Text
    }

    public enum ReportSaveType
    {
        Excel,
        Word,
        Csv,
        Html
        //Text
    }

    public enum CellFormatType
    {
        Value,
        Text,
        Date,
        Boolean,
        Integer,
        Double
    }

    public enum ReportStyleOption
    {
        /// <summary>
        /// Not normally used - but used instead of a "null"
        /// </summary>
        None,

        Style_Heading,

        //AlignTop,

        AlignLeft,
        AlignCenter,
        AlignRight,

        FontWeight_Normal,
        FontWeight_Bold,

        FontStyle_Normal,
        FontStyle_Italic,

        FontSize_8,
        FontSize_9,
        FontSize_10,
        FontSize_11,
        FontSize_12,
        FontSize_13,
        FontSize_14,
        FontSize_15,

        FontColor_Red,
        FontColor_Blue,
        FontColor_Green,
        FontColor_Gray,

        BackgroundColor_Yellow,
        BackgroundColor_Green,
        BackgroundColor_Red
    }

}
