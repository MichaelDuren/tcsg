﻿using System;

namespace Util.Reports
{
    public class ReportRunStatus
    {
        public bool RanSuccessfully { get; set; }
        public int? NumberOfRows { get; set; }
        public string Message { get; set; }
        public TimeSpan ExecutionTime { get; set; }
        public ReportCompletionState? ReportCompletionState { get; set; }
        public StandardValidators StandardValidators { get; set; }

        public ReportRunStatus() { }

        // ********************************************************************************
        #region Methods
        // ********************************************************************************

        public static ReportRunStatus Success(int? numberOfRows = null)
        {
            return new ReportRunStatus
            {
                RanSuccessfully = true,
                NumberOfRows = numberOfRows,
                ReportCompletionState = Util.Reports.ReportCompletionState.Success
            };
        }

        public static ReportRunStatus SuccessSaved(int? numberOfRows = null)
        {
            return new ReportRunStatus
            {
                RanSuccessfully = true,
                NumberOfRows = numberOfRows,
                ReportCompletionState = Util.Reports.ReportCompletionState.SuccessSaved
            };
        }

        public static ReportRunStatus NoData()
        {
            return new ReportRunStatus
            {
                RanSuccessfully = false,
                Message = "No results found.  Please modify and re-try your search.",
                ReportCompletionState = Util.Reports.ReportCompletionState.FailNoData
            };
        }

        public static ReportRunStatus FailRun(string message)
        {
            return new ReportRunStatus
            {
                RanSuccessfully = false,
                Message = message,
                ReportCompletionState = Util.Reports.ReportCompletionState.FailRun
            };
        }

        public static ReportRunStatus FailValidation(StandardValidators validators)
        {
            return new ReportRunStatus
            {
                RanSuccessfully = false,
                Message = "Failed Validation",
                StandardValidators = validators,
                ReportCompletionState = Util.Reports.ReportCompletionState.FailValidation
            };
        }

        public override string ToString()
        {
            return string.Format("RanSuccessfully: {0} NumberOfRows: {1} Message: {2} ExecutionTime: {3}",
                this.RanSuccessfully, this.NumberOfRows, this.Message, this.ExecutionTime);
        }

        #endregion
    }
}
