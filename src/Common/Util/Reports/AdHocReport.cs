﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Util.Sql;
using Util.Sql.Query;

namespace Util.Reports
{
    public class AdHocReport : ReportBase, IDisposable
    {
        private enum DataSourceType
        {
            Sql,
            DataReader
        }

        // ********************************************************************************
        #region Properties, etc.
        // ********************************************************************************

        public AdHocReportDefinition ReportDefinition { get; set; }

        private DataSourceType? dataSourceType = null;

        private IDbConnection dbConnection;
        private IDataReader dataReader;

        public int ColumnCount { get; private set; }
        public int RowCount { get; private set; }

        private List<AdHocReportColumnDefinition> actualReportColumnDefinitions;

        #endregion

        // ********************************************************************************
        #region Constructors, etc.
        // ********************************************************************************

        public AdHocReport() { }

        public AdHocReport(AdHocReportDefinition reportDefinition)
        {
            this.ReportDefinition = reportDefinition;
        }

        public AdHocReport(AdHocReportDefinition reportDefinition, ReportWriterType reportWriterType)
            : base(reportWriterType)
        {
            this.ReportDefinition = reportDefinition;
        }

        public new void Dispose()
        {
            if (this.dataReader != null)
            {
                this.dataReader.Dispose();
                this.dataReader = null;
            }

            if (this.dbConnection != null)
            {
                this.dbConnection = null;
            }

            base.Dispose();
        }

        /// <summary>
        /// Sets the Data Source as Sql using the provided Data Connection.  The Sql is provided in the Report Definition
        /// The assumption is that if this method is used, then the Report Definiton has specified 
        /// Sql and optionally DataParamaters and a CommandTimeout
        /// </summary>
        public void SetDataSource(IDbConnection dbConnection)
        {
            Guard.ArgumentNotNull(dbConnection, "dbConnection");
            Guard.ValueMustBeNull(dataSourceType, "Cannot call SetDataSource() after it has already been called.");

            this.dbConnection = dbConnection;
            dataSourceType = DataSourceType.Sql;
        }

        /// <summary>
        /// Sets the Data Source as DataReader
        /// The assumption is that if this method is used, then the Report Definiton DOES NOT specify Sql, DataParameters or CommandTimeout
        /// </summary>
        public void SetDataSource(IDataReader dataReader)
        {
            Guard.ArgumentNotNull(dataReader, "dataReader");
            Guard.ValueMustBeNull(dataSourceType, "Cannot call SetDataSource() after it has already been called.");

            this.dataReader = dataReader;
            dataSourceType = DataSourceType.DataReader;
        }

        #endregion

        // ********************************************************************************
        #region Core Methods
        // ********************************************************************************

        protected override void Initialize() { }

        protected override ReportRunStatus Run()
        {
            if (dataSourceType == DataSourceType.Sql)
            {
                GetDataReader();
            }

            List<DbColumnDefinition> dbColumnDefinitions = DataReaderTools.GetColumnDefinitions(this.dataReader);

            InitializeActualReportColumnDefinitions(dbColumnDefinitions);

            // Create the column headings and get the column count - for later use
            this.ColumnCount = CreateColumnHeadings();

            // Create the Body - capture the number of rows created
            this.RowCount = CreateBody();

            return ReportRunStatus.Success(RowCount);
        }

        /// <summary>
        /// Validates that the report is in the correct state to be run
        /// </summary>
        protected override void ValidateRunState()
        {
            Guard.ArgumentNotNull(ReportDefinition, "ReportDefinition");
            Guard.ArgumentNotNull(ReportDefinition.ReportTitle, "ReportDefinition.ReportTitle");
            Guard.ValueNotNull(dataSourceType, "Must Set a DataSource");

            if (StringTools.IsBlankOrNull(ReportDefinition.Sql) && dbConnection != null)
            {
                throw new InvalidOperationException("Invalid Report Run State: Must provide a Sql in the Report Definition if using a DbConnection to create the report.");
            }

            if (!StringTools.IsBlankOrNull(ReportDefinition.Sql) && dbConnection == null)
            {
                throw new InvalidOperationException("Invalid Report Run State: Must provide a DbConnection if using a DbConnection and Sql to create the report.");
            }

            // Sync Properties after we pass validation
            this.ReportTitle = ReportDefinition.ReportTitle;

            base.ValidateRunState();
        }

        #endregion

        // ********************************************************************************
        #region Column Heading Methods
        // ********************************************************************************

        protected virtual int CreateColumnHeadings()
        {
            // TODO: It seems that I should set col = 0 instead of col = 1 - TEST THIS
            int col = 1;

            foreach (AdHocReportColumnDefinition rcd in actualReportColumnDefinitions)
            {
                ReportWriter.CreateColumnHeading(rcd.ColumnHeading, rcd.HeadingStyleOptions.ToArray());
                col++;
            }

            return col;
        }

        #endregion

        // ********************************************************************************
        #region Body Methods
        // ********************************************************************************

        protected virtual int CreateBody()
        {
            // TODO: It seems that I should set row = 0 instead of row = 1 - TEST THIS
            int row = 1;

            while (this.dataReader.Read())
            {
                int col = 1;

                ReportWriter.NewRow();


                foreach (AdHocReportColumnDefinition rcd in actualReportColumnDefinitions)
                {
                    object value = this.dataReader[rcd.ColumnName];
                    WriteBodyCell(rcd, value);
                    col++;
                }

                row++;
            }

            return row;
        }

        private void WriteBodyCell(AdHocReportColumnDefinition rcd, object value)
        {
            ReportStyleOption[] styleOptions = new ReportStyleOption[] { };

            if (rcd.CellStyleOptions.Count > 0)
            {
                styleOptions = rcd.CellStyleOptions.ToArray();
            }

            if (value == null)
            {
                ReportWriter.SetEmptyCell(styleOptions);
            }
            else if (value is string)
            {
                ReportWriter.SetCellTextValue(value as string, rcd.CellFormat, rcd.HyperLinkFormat, styleOptions);
            }
            else if (value is int || value is int? || value is Int16 || value is Int16?)
            {
                ReportWriter.SetCellIntegerValue(value as int?, rcd.CellFormat, rcd.HyperLinkFormat, styleOptions);
            }
            else if (value is DateTime || value is DateTime?)
            {
                ReportWriter.SetCellDateValue(value as DateTime?, rcd.CellFormat, rcd.HyperLinkFormat, styleOptions);
            }
            else if (value is bool || value is bool?)
            {
                ReportWriter.SetCellBooleanValue(value as bool?, rcd.CellFormat, rcd.HyperLinkFormat, styleOptions);
            }
            else if (value is double || value is double?)
            {
                ReportWriter.SetCellDoubleValue(value as double?, rcd.CellFormat, rcd.HyperLinkFormat, styleOptions);
            }
            else
            {
                ReportWriter.SetCellValue(value, rcd.CellFormat, rcd.HyperLinkFormat, styleOptions);
            }
        }

        #endregion

        // ********************************************************************************
        #region Support Methods
        // ********************************************************************************

        private void GetDataReader()
        {
            QueryDef qd = new QueryDef(ReportDefinition.Sql);
            if (ReportDefinition.CommandTimeout != null)
            {
                qd.CommandTimeout = (int)ReportDefinition.CommandTimeout;
            }

            // Make sure that the connection is open
            if (this.dbConnection.State != ConnectionState.Open)
            {
                dbConnection.Open();
            }

            DataParameters dataParameters = null;
            if (ReportDefinition.DataParameters != null)
            {
                dataParameters = new DataParameters(ReportDefinition.DataParameters);
            }

            DataReaderAndCommand drac = QueryEngine.GetDataReader(this.dbConnection, qd, dataParameters);

            Guard.ArgumentNotNull(drac, "DataReaderAndCommand");

            this.dataReader = drac.DataReader;
        }

        private void InitializeActualReportColumnDefinitions(List<DbColumnDefinition> dbColumnDefinitions)
        {
            actualReportColumnDefinitions = new List<AdHocReportColumnDefinition>();

            foreach (DbColumnDefinition columnDef in dbColumnDefinitions)
            {
                // Get the Report Column Definition if it exists
                AdHocReportColumnDefinition rcd = ReportDefinition.ReportColumnDefinitions
                    .Where(cd => cd.ColumnName.ToUpper() == columnDef.ColumnName.ToUpper())
                    .FirstOrDefault();

                AdHocReportColumnDefinition actualRcd;

                if (rcd != null)
                {
                    actualRcd = new AdHocReportColumnDefinition
                    {
                        ColumnName = rcd.ColumnName,
                        ColumnHeading = rcd.ColumnHeading,
                        HeadingStyleOptions = rcd.HeadingStyleOptions,
                        CellStyleOptions = rcd.CellStyleOptions,
                        CellFormat = rcd.CellFormat,
                        HyperLinkFormat = rcd.HyperLinkFormat
                    };
                }
                else
                {
                    actualRcd = new AdHocReportColumnDefinition
                    {
                        ColumnName = columnDef.ColumnName
                    };
                }

                // Set the Column Heading
                if (StringTools.IsBlankOrNull(actualRcd.ColumnHeading))
                {
                    actualRcd.ColumnHeading = columnDef.ColumnName;
                    if (ReportDefinition.ParseColumnHeadings)
                    {
                        actualRcd.ColumnHeading = StringTools.GetHeadingFromFieldName(actualRcd.ColumnHeading);
                    }
                }

                actualReportColumnDefinitions.Add(actualRcd);
            }
        }

        #endregion
    }
}
