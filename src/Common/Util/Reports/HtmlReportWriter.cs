﻿using System;
using System.IO;
using System.Text;
using System.Web.UI;

namespace Util.Reports
{
    public class HtmlReportWriter : ReportWriterBase, IDisposable
    {
        // ********************************************************************************
        #region Properties, Constructors and Related
        // ********************************************************************************

        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private StringWriter stringWriter = new StringWriter();
        private HtmlTextWriter htmlWriter;

        private bool rowInitialized;
        private bool rowFinalized;

        public HtmlReportWriter()
        {
            htmlWriter = new HtmlTextWriter(stringWriter, "    ");
        }

        #endregion

        // ********************************************************************************
        #region Initialize / Finalize report sections methods
        // ********************************************************************************

        public override void StartGrid()
        {
            Guard.FailIfTrue(GridStarted, "Cannot Start Grid - a grid has already been started.  First call EndGrid()");
            GridStarted = true;

            htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "reportGrid");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);

            //htmlWriter.AddAttribute(HtmlTextWriterAttribute.Border, "1");
            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Table);
        }

        public override void EndGrid()
        {
            Guard.FailIfTrue(!GridStarted, "Cannot End Grid - a grid has not been started.  First call StartGrid()");
            GridStarted = false;

            if (rowInitialized && !rowFinalized)
            {
                FinalizeRow();
            }

            htmlWriter.RenderEndTag(); // Table
            htmlWriter.RenderEndTag(); // Div
        }

        private void InitializeRow()
        {
            rowInitialized = true;
            rowFinalized = false;

            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Tr);
        }

        private void FinalizeRow()
        {
            rowInitialized = false;
            rowFinalized = true;

            htmlWriter.RenderEndTag(); // tr
        }

        #endregion

        // ********************************************************************************
        #region Row Methods
        // ********************************************************************************

        /// <summary>
        /// Indicates the beginning of a new row (must be called before writing columns to a new row)
        /// </summary>
        public override void NewRow()
        {
            ConfirmInGrid();

            if (rowInitialized && !rowFinalized)
            {
                FinalizeRow();
            }

            InitializeRow();
        }

        #endregion

        // ********************************************************************************
        #region Cell Methods
        // ********************************************************************************

        protected override void SetCellValue(CellFormatType cellFormatType, object value, string format = null, string hyperLinkFormat = null, params ReportStyleOption[] styleOptions)
        {
            // Note: cellFormatType is not used

            ConfirmInGrid();

            // Make sure that the FIRST row is initialized
            if (!rowInitialized)
            {
                InitializeRow();
            }

            // Set any style related stuff as attributes before writing this tag
            ApplyStyleOptions(styleOptions);

            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Td);

            // ***************************************************
            // Write the Cell Value
            // ***************************************************

            bool isHyperLink = !StringTools.IsBlankOrNull(hyperLinkFormat);

            if (isHyperLink)
            {
                // Write Opening HREF tag
                //  This is a sample of what we want to output: <a href="http://www.w3schools.com">Visit W3Schools.com!</a>
                htmlWriter.AddAttribute(HtmlTextWriterAttribute.Href, string.Format(hyperLinkFormat, value));
                htmlWriter.RenderBeginTag(HtmlTextWriterTag.A);
            }

            // Apply formatting if applicable
            if (!StringTools.IsBlankOrNull(format))
            {
                value = string.Format(format, value);
            }

            htmlWriter.Write(value);

            if (isHyperLink)
            {
                // Write Closing HREF tag
                htmlWriter.RenderEndTag(); // a
            }

            // Write the END Tag
            htmlWriter.RenderEndTag(); // td
        }

        #endregion

        // ********************************************************************************
        #region Write Unstructured Content
        // ********************************************************************************

        /// <summary>
        /// Supports allowing unstructured content to be added to the report.
        /// This can NOT be part of a "Grid" - it must be created either before or after the creation of a grid
        /// </summary>
        public override void WriteUnstructuredContent(object value, string format = null, params ReportStyleOption[] styleOptions)
        {
            ConfirmNotInGrid();

            // Set any style related stuff as attributes before writing this tag
            ApplyStyleOptions(styleOptions);

            htmlWriter.RenderBeginTag(HtmlTextWriterTag.Div);

            // Apply formatting if applicable
            if (!StringTools.IsBlankOrNull(format))
            {
                value = string.Format(format, value);
            }

            htmlWriter.Write(value);

            // Write the END Tag
            htmlWriter.RenderEndTag(); // div
        }

        #endregion

        // ********************************************************************************
        #region Style Related Methods
        // ********************************************************************************

        private void ApplyStyleOptions(params ReportStyleOption[] styleOptions)
        {
            ReportStyleOption? textAlignmentOption = null;

            ReportStyleOption? fontWeightOption = null;
            ReportStyleOption? fontStyleOption = null;
            ReportStyleOption? fontSizeOption = null;
            ReportStyleOption? fontColorOption = null;
            ReportStyleOption? backgroundColorOption = null;

            // NOTE: Must deal with the possibility of an option - or option class being specified more than once.
            //          We are using the convention that the LAST one specified wins.

            foreach (ReportStyleOption styleOption in styleOptions)
            {
                switch (styleOption)
                {
                    case ReportStyleOption.None:
                        // Do nothing
                        break;

                    case ReportStyleOption.Style_Heading:
                        // Make headings use standard styles except Add BOLD and Center
                        fontWeightOption = ReportStyleOption.FontWeight_Bold;
                        textAlignmentOption = ReportStyleOption.AlignCenter;
                        htmlWriter.AddAttribute(HtmlTextWriterAttribute.Class, "reportHeading");
                        break;

                    case ReportStyleOption.AlignLeft:
                    case ReportStyleOption.AlignCenter:
                    case ReportStyleOption.AlignRight:
                        textAlignmentOption = styleOption;
                        break;

                    case ReportStyleOption.FontWeight_Normal:
                    case ReportStyleOption.FontWeight_Bold:
                        fontWeightOption = styleOption;
                        break;

                    case ReportStyleOption.FontStyle_Normal:
                    case ReportStyleOption.FontStyle_Italic:
                        fontStyleOption = styleOption;
                        break;

                    case ReportStyleOption.FontSize_8:
                    case ReportStyleOption.FontSize_9:
                    case ReportStyleOption.FontSize_10:
                    case ReportStyleOption.FontSize_11:
                    case ReportStyleOption.FontSize_12:
                    case ReportStyleOption.FontSize_13:
                    case ReportStyleOption.FontSize_14:
                    case ReportStyleOption.FontSize_15:
                        fontSizeOption = styleOption;
                        break;

                    case ReportStyleOption.FontColor_Red:
                    case ReportStyleOption.FontColor_Blue:
                    case ReportStyleOption.FontColor_Green:
                    case ReportStyleOption.FontColor_Gray:
                        fontColorOption = styleOption;
                        break;

                    case ReportStyleOption.BackgroundColor_Yellow:
                    case ReportStyleOption.BackgroundColor_Green:
                    case ReportStyleOption.BackgroundColor_Red:
                        backgroundColorOption = styleOption;
                        break;

                    default:
                        throw new NotImplementedException(string.Format("Should not get here.  StyleOption: {0} is not supported", styleOption));
                }
            }

            StringBuilder styles = new StringBuilder();

            ApplyTextAlignmentOption(styles, textAlignmentOption);
            ApplyFontWeightOption(styles, fontWeightOption);
            ApplyFontStyleOption(styles, fontStyleOption);
            ApplyFontSizeOption(styles, fontSizeOption);
            ApplyFontColorOption(styles, fontColorOption);
            ApplyBackgroundColorOption(styles, backgroundColorOption);

            if (styles.Length > 0)
            {
                htmlWriter.AddAttribute(HtmlTextWriterAttribute.Style, styles.ToString());
            }
        }

        private void ApplyTextAlignmentOption(StringBuilder styles, ReportStyleOption? styleOption)
        {
            if (styleOption == null) { return; }

            switch (styleOption)
            {
                case ReportStyleOption.AlignLeft:
                    styles.Append("text-align:left;");
                    break;

                case ReportStyleOption.AlignCenter:
                    styles.Append("text-align:center;");

                    break;
                case ReportStyleOption.AlignRight:
                    styles.Append("text-align:right;");
                    break;

                default:
                    throw new NotImplementedException(string.Format("Should not get here.  Text Alignment StyleOption: {0} is not supported", styleOption));
            }
        }

        private void ApplyFontWeightOption(StringBuilder styles, ReportStyleOption? styleOption)
        {
            if (styleOption == null) { return; }

            switch (styleOption)
            {
                case ReportStyleOption.FontWeight_Normal:
                    styles.Append("font-weight:normal;");
                    break;

                case ReportStyleOption.FontWeight_Bold:
                    styles.Append("font-weight:bold;");
                    break;

                default:
                    throw new NotImplementedException(string.Format("Should not get here.  Font Weight StyleOption: {0} is not supported", styleOption));
            }
        }

        private void ApplyFontStyleOption(StringBuilder styles, ReportStyleOption? styleOption)
        {
            if (styleOption == null) { return; }

            switch (styleOption)
            {
                case ReportStyleOption.FontStyle_Normal:
                    styles.Append("font-style:normal;");
                    break;

                case ReportStyleOption.FontStyle_Italic:
                    styles.Append("font-style:italic;");
                    break;

                default:
                    throw new NotImplementedException(string.Format("Should not get here.  Font Style StyleOption: {0} is not supported", styleOption));
            }
        }

        private void ApplyFontSizeOption(StringBuilder styles, ReportStyleOption? styleOption)
        {
            if (styleOption == null) { return; }

            switch (styleOption)
            {
                case ReportStyleOption.FontSize_8:
                    styles.Append("font-size:8pt;");
                    break;

                case ReportStyleOption.FontSize_9:
                    styles.Append("font-size:9pt;");
                    break;

                case ReportStyleOption.FontSize_10:
                    styles.Append("font-size:10pt;");
                    break;

                case ReportStyleOption.FontSize_11:
                    styles.Append("font-size:11pt;");
                    break;

                case ReportStyleOption.FontSize_12:
                    styles.Append("font-size:12pt;");
                    break;

                case ReportStyleOption.FontSize_13:
                    styles.Append("font-size:13pt;");
                    break;

                case ReportStyleOption.FontSize_14:
                    styles.Append("font-size:14pt;");
                    break;

                case ReportStyleOption.FontSize_15:
                    styles.Append("font-size:15pt;");
                    break;

                default:
                    throw new NotImplementedException(string.Format("Should not get here.  Font Size StyleOption: {0} is not supported", styleOption));
            }
        }

        private void ApplyFontColorOption(StringBuilder styles, ReportStyleOption? styleOption)
        {
            if (styleOption == null) { return; }

            switch (styleOption)
            {
                case ReportStyleOption.FontColor_Red:
                    styles.Append("color: Red;");
                    break;

                case ReportStyleOption.FontColor_Blue:
                    styles.Append("color: Blue;");
                    break;

                case ReportStyleOption.FontColor_Green:
                    styles.Append("color: Green;");
                    break;

                case ReportStyleOption.FontColor_Gray:
                    styles.Append("color: Gray;");
                    break;

                default:
                    throw new NotImplementedException(string.Format("Should not get here.  Font Color StyleOption: {0} is not supported", styleOption));
            }
        }

        private void ApplyBackgroundColorOption(StringBuilder styles, ReportStyleOption? styleOption)
        {
            if (styleOption == null) { return; }

            switch (styleOption)
            {
                case ReportStyleOption.BackgroundColor_Yellow:
                    styles.Append("background-color:Yellow;");
                    break;

                case ReportStyleOption.BackgroundColor_Green:
                    styles.Append("background-color:Green;");
                    break;

                case ReportStyleOption.BackgroundColor_Red:
                    styles.Append("background-color:Red;");
                    break;

                default:
                    throw new NotImplementedException(string.Format("Should not get here.  Background Color StyleOption: {0} is not supported", styleOption));
            }
        }

        #endregion

        // ********************************************************************************
        #region Miscellaneous Methods
        // ********************************************************************************

        /// <summary>
        /// Returns the contents of the Report
        /// </summary>
        public override string GetReportContent()
        {
            if (GridStarted)
            {
                EndGrid();
            }

            return stringWriter.ToString();
        }

        #endregion
    }
}
