﻿using System;

namespace Util.Reports
{
    public interface IReport : IDisposable
    {
        string ReportTitle { get; }
      
        ReportWriterBase ReportWriter { get;  }
        ReportWriterType ReportWriterType { get; }

        StandardValidators ValidateCriteria();

        ReportRunStatus InitializeAndRun();
    }
}
