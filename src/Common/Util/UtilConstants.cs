
namespace Util
{
    public static class UtilConstants
    {
        public static class AppConfigNames
        {
            public static readonly string ApplicationName = "ApplicationName";
            public static readonly string BaseDatabaseConnectionStringName = "BaseDatabaseConnectionStringName";

            public static readonly string ShortCacheTimeout = "ShortCacheTimeout";
            public static readonly string LongCacheTimeout = "LongCacheTimeout";
            public static readonly string QueryEngineIncludeSqlInLog = "QueryEngineIncludeSqlInLog";
        }
    }
}
