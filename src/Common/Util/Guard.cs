using System;
using System.Collections.Generic;

namespace Util
{
    /// <summary>
    /// Common guard clauses
    /// </summary>
    public static class Guard
    {
        // ********************************************************************************
        #region Null Checks
        // ********************************************************************************

        /// <summary>
        /// Checks a string argument to ensure it isn't blank or null
        /// </summary>
        public static void ArgumentNotBlankOrNull(string argumentValue, string argumentName)
        {
            if (StringTools.IsBlankOrNull(argumentValue))
            {
                throw new ArgumentException(string.Format("Argument {0} cannot be null or blank", argumentName));
            }
        }

        /// <summary>
        /// Checks an argument to ensure it isn't blank or null
        /// </summary>
        public static void ValueNotBlankOrNull(object value, string exceptionMessage)
        {
            if (StringTools.IsBlankOrNull(value))
            {
                throw new InvalidOperationException(exceptionMessage);
            }
        }

        /// <summary>
        /// Checks an argument to ensure it isn't null
        /// </summary>
        public static void ArgumentNotNull(object argumentValue, string argumentName)
        {
            if (argumentValue == null)
            {
                throw new ArgumentNullException(argumentName);
            }
        }

        /// <summary>
        /// Checks an argument to ensure it isn't null
        /// </summary>
        public static void ValueNotNull(object value, string exceptionMessage)
        {
            if (value == null)
            {
                throw new InvalidOperationException(exceptionMessage);
            }
        }

        /// <summary>
        /// Checks an argument to ensure it IS null
        /// </summary>
        public static void ArgumentMustBeNull(object argumentValue, string argumentName)
        {
            if (argumentValue != null)
            {
                throw new InvalidOperationException(string.Format("Argument: {0} with value: {1} MUST BE NULL.", argumentName, argumentValue));
            }
        }

        /// <summary>
        /// Checks an argument to ensure it IS null
        /// </summary>
        public static void ValueMustBeNull(object value, string exceptionMessage)
        {
            if (value != null)
            {
                throw new InvalidOperationException(exceptionMessage);
            }
        }

        #endregion

        // ********************************************************************************
        #region Greater / Less than checks
        // ********************************************************************************

        public static void ArgumentGreaterThan(int argumentValue, int targetValue, string argumentName)
        {
            if (argumentValue <= targetValue)
            {
                throw new ArgumentException(string.Format("Argument {0} with value: {1} must be greater than: {2}", argumentName, argumentValue, targetValue));
            }
        }

        public static void ArgumentGreaterThanOrEqual(int argumentValue, int targetValue, string argumentName)
        {
            if (argumentValue < targetValue)
            {
                throw new ArgumentException(string.Format("Argument {0} with value: {1} must be greater than or equal to: {2}", argumentName, argumentValue, targetValue));
            }
        }

        public static void ArgumentLessThan(int argumentValue, int targetValue, string argumentName)
        {
            if (argumentValue >= targetValue)
            {
                throw new ArgumentException(string.Format("Argument {0} with value: {1} must be less than: {2}", argumentName, argumentValue, targetValue));
            }
        }

        public static void ArgumentLessThanOrEqual(int argumentValue, int targetValue, string argumentName)
        {
            if (argumentValue > targetValue)
            {
                throw new ArgumentException(string.Format("Argument {0} with value: {1} must be less than or equal to: {2}", argumentName, argumentValue, targetValue));
            }
        }

        #endregion

        // ********************************************************************************
        #region General True / False checks
        // ********************************************************************************

        public static void FailIfTrue(bool condition, string exceptionMessage)
        {
            if (condition)
            {
                throw new Exception(exceptionMessage);
            }
        }

        public static void FailIfFalse(bool condition, string exceptionMessage)
        {
            if (!condition)
            {
                throw new Exception(exceptionMessage);
            }
        }

        #endregion

        // ********************************************************************************
        #region Argument Length tests
        // ********************************************************************************

        public static void ArgumentMaxLength(string argumentValue, string argumentName, int maxLength)
        {
            if (argumentValue != null && argumentValue.Length > maxLength)
            {
                throw new InvalidOperationException(string.Format("Argument: {0} with value: {1} Cannot be longer than {2}.", argumentName, argumentValue, maxLength));
            }
        }

        public static void ArgumentMaxLength<T>(ICollection<T> argumentValue, string argumentName, int maxLength)
        {
            if (argumentValue != null && argumentValue.Count > maxLength)
            {
                throw new InvalidOperationException(string.Format("Collection: {0} Cannot have more than {1} elements.", argumentName, maxLength));
            }
        }

        public static void ArgumentMinLength(string argumentValue, string argumentName, int minLength)
        {
            if (argumentValue == null || argumentValue.Length < minLength)
            {
                throw new InvalidOperationException(string.Format("Argument: {0} with value: {1} MUST be at least {2} characters long.", argumentName, argumentValue, minLength));
            }
        }

        public static void ArgumentMinLength<T>(ICollection<T> argumentValue, string argumentName, int minLength)
        {
            if (argumentValue != null && argumentValue.Count < minLength)
            {
                throw new InvalidOperationException(string.Format("Collection: {0} MUST have at least {1} elements.", argumentName, minLength));
            }
        }

        #endregion
    }
}
