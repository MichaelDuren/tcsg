using System;
using System.Configuration;
using System.Diagnostics;
using Util.Collections.Generic;

namespace Util.Config
{
    /// <summary>
    /// This class will use the DynamicConfigurationSection from the .config file for the application so that appSettings
    /// and connectionStrings can be retrieved that are specific to the environment that the machine is configured to run in
    /// </summary>
    public static class DynamicConfigurationManager
    {
        #region Members and Properties
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static object lockObject = new object();

        static CaseInsensitiveDictionary<MachineEnvironmentElement> _machines;
        /// <summary>
        /// The set of machines configured in the dynamicConfiguration section of the .config file for the current application
        /// </summary>
        static CaseInsensitiveDictionary<MachineEnvironmentElement> Machines
        {
            get
            {
                if (_machines == null)
                {
                    lock (lockObject)
                    {
                        _machines = new CaseInsensitiveDictionary<MachineEnvironmentElement>();

                        // Get the DynamicConfiguration section.
                        DynamicConfigurationSection dynamicConfigSection = ConfigurationManager.GetSection("dynamicConfiguration") as DynamicConfigurationSection;

                        if (dynamicConfigSection == null)
                        {
                            string message = "The dynamicConfiguration section is missing from the .config file.  The Dynamic Configuration Manager cannot function without that config section.";
                            throw new InvalidOperationException(message);
                        }
                        else
                        {
                            for (int i = 0; i < dynamicConfigSection.Machines.Count; i++)
                            {
                                _machines.Add(dynamicConfigSection.Machines[i].Name, dynamicConfigSection.Machines[i]);
                            }
                        }
                    }
                }

                return _machines;
            }
        }

        /// <summary>
        /// Returns the MachineEnvironmentElement for the machine executing the application
        /// </summary>
        public static MachineEnvironmentElement Element
        {
            get
            {
                if (!Machines.ContainsKey(System.Environment.MachineName))
                {
                    string message = String.Format("The element for the current environment cannot be retrieved.  The Machine Name: {0} was not found in the list of machines.  Please verify that {0} has been added to the dynamicConfiguration section of the config file.", System.Environment.MachineName);
                    throw new InvalidOperationException(message);
                }
                return Machines[System.Environment.MachineName];
            }
        }

        /// <summary>
        /// Returns the environment configured in the .config file for the machine executing the application
        /// </summary>
        public static string Environment
        {
            get { return DynamicConfigurationManager.Element.Environment; }
        }

        /// <summary>
        /// Returns true if the environment is Production
        /// </summary>
        public static bool IsProduction
        {
            get
            {
                if (Environment.ToUpper() == "PRD" ||
                    Environment.ToUpper() == "PROD" ||
                    Environment.ToUpper() == "PRODUCTION")
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        private static AppSettingsCollection _appSettings = new AppSettingsCollection();
        /// <summary>
        /// Provides indexer access to app settings, returns the proper value for the current environment
        /// </summary>
        public static AppSettingsCollection AppSettings
        {
            get { return _appSettings; }
        }

        private static ConnectionStringsCollection _connectionStrings = new ConnectionStringsCollection();
        /// <summary>
        /// Provides indexer access to connection strings, returns the proper value for the current environment
        /// </summary>
        public static ConnectionStringsCollection ConnectionStrings
        {
            get { return _connectionStrings; }
        }

        #endregion

        #region  Methods
        /// <summary>
        /// Returns the value of the app setting for the setting name provided.
        /// 
        /// First, the environment specific version will be searched for.  If that is found, it is returned.
        /// 
        /// If the environment specific version is not found, then the default version will be searched for and returned.
        /// </summary>
        /// <param name="settingName">The settings name (ie key) of the setting to be retrieved</param>
        /// <param name="throwIfNotExists">If true, an exception is thrown if the setting name is not found.  Default value is false.</param>
        public static string GetAppSetting(string settingName, bool throwIfNotExists = false)
        {
            string returnValue = AppSettings[settingName];

            if (returnValue == null && throwIfNotExists)
            {
                throw new Exception(String.Format("The App Setting {0} was not found in the .config file.", settingName));
            }

            return returnValue;
        }

        /// <summary>
        /// Returns the value of the connection string for the name provided.
        /// 
        /// First, the environment specific version will be searched for.  If that is found, it is returned.
        /// 
        /// If the environment specific version is not found, then the default version will be searched for and returned.
        /// </summary>
        /// <param name="connectionStringName">The connection string name of the connection string to be retrieved</param>
        /// <param name="throwIfNotExists">If true, an exception is thrown if the connection string is not found.  Default value is false.</param>
        public static ConnectionStringSettings GetConnectionString(string connectionStringName, bool throwIfNotExists = false)
        {
            ConnectionStringSettings returnValue = ConnectionStrings[connectionStringName];

            if (returnValue == null && throwIfNotExists)
            {
                throw new Exception(String.Format("The Connection String {0} was not found in the .config file.", connectionStringName));
            }

            return returnValue;
        }

        /// <summary>
        /// Get the connection string name for the current environment
        /// </summary>
        public static string GetConnectionStringName()
        {
            string baseDatabaseConnectionStringName = ConfigurationManager.AppSettings[UtilConstants.AppConfigNames.BaseDatabaseConnectionStringName];
            return GetConnectionStringName(baseDatabaseConnectionStringName);
        }

        /// <summary>
        /// Get the connection string name for the base connection string name provided
        /// </summary>
        public static string GetConnectionStringName(string baseDatabaseConnectionStringName)
        {
            string environmentConnectionStringName = String.Format("{0}-{1}", baseDatabaseConnectionStringName, Environment);

            if (ConfigurationManager.ConnectionStrings[environmentConnectionStringName] != null)
            {
                return environmentConnectionStringName;
            }
            else if (ConfigurationManager.ConnectionStrings[baseDatabaseConnectionStringName] != null)
            {
                return baseDatabaseConnectionStringName;
            }
            else
            {
                string message = string.Format("Connection String for BaseDatabaseConnectionStringName: {0} / Environment: {1} does not exist in .config file", baseDatabaseConnectionStringName, Environment);
                Debug.WriteLine(message);
                log.Error(message);
                throw new Exception(message);
            }
        }

        #endregion

        public class AppSettingsCollection
        {
            public string this[string settingName]
            {
                get
                {
                    return Get(settingName);
                }
            }

            public virtual string Get(string settingName)
            {
                string returnValue = null;
                string environmentSettingName = String.Format("{0}-{1}", settingName, Environment);

                //Try to get the environment specific value first
                returnValue = ConfigurationManager.AppSettings[environmentSettingName];

                //If no value found, try the default version (ie setting with no environment name)
                if (returnValue == null)
                {
                    returnValue = ConfigurationManager.AppSettings[settingName];
                }

                return returnValue;
            }
        }

        public class ConnectionStringsCollection
        {
            public ConnectionStringSettings this[string connectionStringName]
            {
                get
                {
                    return Get(connectionStringName);
                }
            }

            public virtual ConnectionStringSettings Get(string connectionStringName)
            {
                ConnectionStringSettings returnValue = null;
                string environmentConnectionStringName = String.Format("{0}-{1}", connectionStringName, Environment);

                //Try to get the environment specific value first
                returnValue = ConfigurationManager.ConnectionStrings[environmentConnectionStringName];

                //If no value found, try the default version (ie setting with no environment name)
                if (returnValue == null)
                {
                    returnValue = ConfigurationManager.ConnectionStrings[connectionStringName];
                }

                return returnValue;
            }
        }
    }
}
