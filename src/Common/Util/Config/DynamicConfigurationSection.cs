//Used this asrticle to create this class
//http://msdn.microsoft.com/en-us/library/2tw134k3(v=VS.100).aspx
//http://msdn.microsoft.com/en-us/library/system.configuration.configurationelement.aspx

using System.Configuration;

namespace Util.Config
{
    /// <summary>
    /// Represents the Dynamic Configuration section that inherits from ConfigurationSection and can be defined in 
    /// the web.config/app.config file of the application.  The contents of this section is used by the Dynamic Configuration
    /// Manager to return the proper values for AppSettings and ConnectionStrings based on the environment of the machine executing
    /// the application.
    /// 
    /// Most of this code with a few minor exceptions is directly copied from this link:
    /// http://msdn.microsoft.com/en-us/library/system.configuration.configurationelement.asp

    /// </summary>
    public class DynamicConfigurationSection : ConfigurationSection
    {
        // Declare a collection element represented in the configuration file by the sub-section 
        // <machines> <add .../> </machines>  
        //
        // Note: the "IsDefaultCollection = false" instructs the .NET Framework to build a nested  
        // section like <machines> ...</machines>.
        [ConfigurationProperty("machines", IsDefaultCollection = false)]
        public MachineEnvironmentCollection Machines
        {
            get
            {
                MachineEnvironmentCollection collection = (MachineEnvironmentCollection)base["machines"];
                return collection;
            }
        }
    }
}

