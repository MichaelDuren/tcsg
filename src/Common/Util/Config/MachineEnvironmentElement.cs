//Used this asrticle to create this class
//http://msdn.microsoft.com/en-us/library/2tw134k3(v=VS.100).aspx
//http://msdn.microsoft.com/en-us/library/system.configuration.configurationelement.aspx

using System.Configuration;

namespace Util.Config
{
    /// <summary>
    /// This class defines an element in a config file that defines a Machine Name (as the key) and the environment that the machine
    /// belongs to.  It will be used by the Dynamic Configuration Manager to determine the environment for the machine currently executing
    /// the application.  The environment identified will determine the set of app settings and connection strings to be used.
    /// </summary>
    public class MachineEnvironmentElement : ConfigurationElement
    {
        /// <summary>
        /// The Machine Name is the actual name of the machine/server that is executing the application.
        /// </summary>
        [ConfigurationProperty("name", IsRequired = true, IsKey = true)]
        public string Name
        {
            get { return (string)this["name"]; }
            set { this["name"] = value; }
        }

        /// <summary>
        /// The Environment represents the environment the machine is a part of.  Common values for this
        /// property are something like 'DEV', 'UAT' or 'PROD' but the user is not limited to that set and may
        /// provide their own value.
        /// </summary>
        [ConfigurationProperty("environment", IsKey = false, IsRequired = true)]
        public string Environment
        {
            get { return (string)this["environment"]; }
            set { this["environment"] = value; }
        }
    }
}
