//Used this asrticle to create this class
//http://msdn.microsoft.com/en-us/library/2tw134k3(v=VS.100).aspx
//http://msdn.microsoft.com/en-us/library/system.configuration.configurationelement.asp

using System;
using System.Configuration;

namespace Util.Config
{
    /// <summary>
    /// Contains a collection of MachineEnvironmentElements and inherits from the standard ConfigurationElementCollection.
    /// 
    /// Most of this code with a few minor exceptions is directly copied from this link:
    /// http://msdn.microsoft.com/en-us/library/system.configuration.configurationelement.asp
    /// </summary>
    [ConfigurationCollection(typeof(MachineEnvironmentElement))]
    public class MachineEnvironmentCollection : ConfigurationElementCollection
    {
        protected override ConfigurationElement CreateNewElement()
        {
            return new MachineEnvironmentElement();
        }

        protected override Object GetElementKey(ConfigurationElement element)
        {
            return ((MachineEnvironmentElement)element).Name;
        }

        public new string AddElementName
        {
            get { return base.AddElementName; }
            set { base.AddElementName = value; }
        }

        public new string ClearElementName
        {
            get { return base.ClearElementName; }
            set { base.ClearElementName = value; }
        }

        public new string RemoveElementName
        {
            get { return base.RemoveElementName; }
        }

        public new int Count
        {
            get { return base.Count; }
        }

        public MachineEnvironmentElement this[int index]
        {
            get { return (MachineEnvironmentElement)BaseGet(index); }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        new public MachineEnvironmentElement this[string MachineName]
        {
            get { return (MachineEnvironmentElement)BaseGet(MachineName); }
        }

        public int IndexOf(MachineEnvironmentElement element)
        {
            return BaseIndexOf(element);
        }

        public void Add(MachineEnvironmentElement element)
        {
            BaseAdd(element);
            // Add custom code here.
        }

        protected override void BaseAdd(ConfigurationElement element)
        {
            //Cannot have duplicate Machine Names so throwIfExists is true
            BaseAdd(element, true);
        }

        public void Remove(MachineEnvironmentElement element)
        {
            if (BaseIndexOf(element) >= 0)
            {
                BaseRemove(element.Name);
            }
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string machineName)
        {
            BaseRemove(machineName);
        }

        public void Clear()
        {
            BaseClear();
        }
    }
}
