﻿//TODO: Get this one updated

namespace Util
{
    /// <summary>
    /// Note: much of this code was borrowed from: http://www.codeproject.com/KB/cs/rhgarrayobject.aspx
    /// </summary>
    public class ArrayTools
    {
        private const int firstDimension = 0;
        private const int secondDimension = 1;
        private const int thirdDimension = 2;

        /// <summary>
        /// ReDimension a one dimensional array
        /// </summary>
        public static T[] ReDimension<T>(T[] oldArray, int arrNewLength)
        {
            // Declare a larger array
            T[] newArray = new T[arrNewLength];

            // Determine if we are shrinking or enlarging
            int rowMax = 0;
            if (oldArray.Length < arrNewLength)
            {
                rowMax = oldArray.Length;
            }
            else
            {
                rowMax = arrNewLength;
            }

            // place values of old array into new array
            for (int row = 0; row < rowMax; row++)
            {
                newArray[row] = oldArray[row];
            }

            return newArray;
        }

        /// <summary>
        /// ReDimension a 2D rectangular array
        /// </summary>
        public static T[,] ReDimension<T>(T[,] oldArray, int arr1stDimLength, int arr2ndDimLength)
        {
            // Declare a larger array
            T[,] newArray = new T[arr1stDimLength, arr2ndDimLength];

            // Determine if we are shrinking or enlarging
            int xMax = 0;
            int yMax = 0;

            // Determine if we are shrinking or enlarging columns
            if (oldArray.GetUpperBound(firstDimension) < (arr1stDimLength - 1))
            {
                xMax = oldArray.GetUpperBound(firstDimension) + 1;
            }
            else
            {
                xMax = arr1stDimLength;
            }

            // Determine if we are shrinking or enlarging rows
            if (oldArray.GetUpperBound(secondDimension) < (arr2ndDimLength - 1))
            {
                yMax = oldArray.GetUpperBound(secondDimension) + 1;
            }
            else
            {
                yMax = arr2ndDimLength;
            }

            // Place values of old array into new array
            for (int x = 0; x < xMax; x++)
            {
                for (int y = 0; y < yMax; y++)
                {
                    newArray[x, y] = oldArray[x, y];
                }
            }

            return newArray;
        }

        /// <summary>
        /// ReDimension a 3D rectangular array
        /// </summary>
        public static T[, ,] ReDimension<T>(T[, ,] oldArray, int arr1stDimLength, int arr2ndDimLength, int arr3rdDimLength)
        {
            // declare a larger array
            T[, ,] newArray = new T[arr1stDimLength, arr2ndDimLength, arr3rdDimLength];

            // Determine if we are shrinking or enlarging
            int xMax = 0;
            int yMax = 0;
            int zMax = 0;

            // Determine if we are shrinking or enlarging columns
            if (oldArray.GetUpperBound(firstDimension) < (arr1stDimLength - 1))
            {
                xMax = oldArray.GetUpperBound(firstDimension) + 1;
            }
            else
            {
                xMax = arr1stDimLength;
            }

            // Determine if we are shrinking or enlarging rows
            if (oldArray.GetUpperBound(secondDimension) < (arr2ndDimLength - 1))
            {
                yMax = oldArray.GetUpperBound(secondDimension) + 1;
            }
            else
            {
                yMax = arr2ndDimLength;
            }

            // Determine if we are shrinking or enlarging depth
            if (oldArray.GetUpperBound(thirdDimension) < (arr3rdDimLength - 1))
            {
                zMax = oldArray.GetUpperBound(thirdDimension) + 1;
            }
            else
            {
                zMax = arr3rdDimLength;
            }

            // Place values of old array into new array
            for (int z = 0; z < zMax; z++)
            {
                for (int x = 0; x < xMax; x++)
                {
                    for (int y = 0; y < yMax; y++)
                    {
                        newArray[x, y, z] = oldArray[x, y, z];
                    }
                }
            }

            return newArray;
        }
    }
}

