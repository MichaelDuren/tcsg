using System;
using System.Collections.Generic;

namespace Util
{
    public static class ExtensionMethods
    {
        /// <summary>
        /// Return the Time portion of the DateTime
        /// </summary>
        public static Time ToTime(this DateTime dateTime)
        {
            return new Time(dateTime);
        }

        //public static string FormatWith(this string format, params object[] args)
        //{
        //    if (format == null)
        //        throw new ArgumentNullException("format");

        //    return string.Format(format, args);
        //}

        ///// <summary>
        ///// Determines if targetType implements the specified Interface type
        ///// </summary>
        //public static bool IsImplementationOf(this Type targetType, Type interfaceType)
        //{
        //    Type t = targetType.GetInterface(interfaceType.FullName);

        //    if (t == null) { return false; }
        //    else { return true; }
        //}

        ///// <summary>
        ///// Extension method to allow String.Split with a string separator
        ///// </summary>
        //public static string[] Split(this string thisString, string separator)
        //{
        //    string[] ss = { separator };
        //    return thisString.Split(ss, StringSplitOptions.None);
        //}

        ///// <summary>
        ///// Gets the DateTime that is the beginning of the day (ie. hour, minute, second set to 0, 0, 0)
        ///// </summary>
        //public static DateTime GetBeginningOfDay(this DateTime dateTime)
        //{
        //    return (DateTime)DateTools.GetBeginningOfDay(dateTime);
        //}

        ///// <summary>
        ///// Gets the DateTime that is the end of the day (ie. hour, minute, second set to 23, 59, 59)
        ///// </summary>
        //public static DateTime GetEndOfDay(this DateTime dateTime)
        //{
        //    return (DateTime)DateTools.GetEndOfDay(dateTime);
        //}

        public static TValue GetValueOrNull<TKey, TValue>(this Dictionary<TKey, TValue> dict, TKey key)
        {
            if (key != null && dict.ContainsKey(key))
            {
                return dict[key];
            }
            else
            {
                return default(TValue);
            }
        }
    }
}
