using System;
using System.Collections.Generic;
using System.Text;

namespace Util
{
    /// <summary>
    /// This class provides utility functions to use with Strings.
    /// </summary>
    public static class StringTools
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //// ******************************************************************************
        //#region Case Conversions methods
        //// ******************************************************************************

        /// <summary> Translates the first character of a string to upper case.  Leaves the
        /// rest alone - tests first for Null originalValue
        /// </summary>
        public static string InitialToUpper(string s)
        {
            if (!IsBlankOrNull(s))
                return Char.ToUpper(s[0]) + s.Substring(1);
            else
                return s;
        }


        /// <summary> Translates the first character of a string to lower case.  Leaves the
        /// rest alone - tests first for Null originalValue
        /// </summary>
        public static string InitialToLower(string s)
        {
            if (!IsBlankOrNull(s))
                return Char.ToLower(s[0]) + s.Substring(1);
            else
                return s;
        }

        ///// <summary> Does a toUpperCase - but first tests for null</summary>
        //public static string ToUpperCase(string s)
        //{
        //    if (!IsBlankOrNull(s))
        //        return s.ToUpper();
        //    else
        //        return s;
        //}

        ///// <summary> Does a toLowerCase - but first tests for null</summary>
        //public static string ToLowerCase(string s)
        //{
        //    if (!IsBlankOrNull(s))
        //        return s.ToLower();
        //    else
        //        return s;
        //}
        //#endregion

        //// ******************************************************************************
        //#region Pad / Fill / Substring / Trim methods
        //// ******************************************************************************

        /// <summary>
        /// Will take the initialString and fill it (on the Right) (with fillString) to the length specified.
        /// If initialString is already long enough, it will do nothing
        /// </summary>
        public static string PadFill(string initialString, string fillString, int length)
        {
            StringBuilder sb;

            if (initialString != null)
            {
                sb = new StringBuilder(initialString);
            }
            else
            {
                sb = new StringBuilder();
            }
            if (fillString == null || fillString.Length == 0)
            {
                fillString = " ";
            }
            while (sb.Length < length)
            {
                sb.Append(fillString);
            }
            return sb.ToString();
        }

        ///// <summary>padFill's an int </summary>
        //public static string PadFill(int value, string fillString, int length)
        //{
        //    return PadFill("" + value, fillString, length);
        //}

        ///// <summary>padFill's a double </summary>
        //public static string PadFill(double value, string fillString, int length)
        //{
        //    return PadFill("" + value, fillString, length);
        //}

        /// <summary>
        /// Will return a String filled with the fillString to the length specified.
        /// </summary>
        public static string Fill(string fillString, int length)
        {
            return PadFill("", fillString, length);
        }

        /// <summary>
        /// Will pad the initialString with blanks (on the Right) to the length specified.
        /// If initialString is already long enough, it will do nothing
        /// </summary>
        public static string Pad(string initialString, int length)
        {
            return PadFill(initialString, " ", length);
        }

        ///// <summary>pad's an int </summary>
        //public static string Pad(int value, int length)
        //{
        //    return Pad("" + value, length);
        //}

        ///// <summary>pad's a double </summary>
        //public static string Pad(double value, int length)
        //{
        //    return Pad("" + value, length);
        //}

        /// <summary>
        /// Will take the initialString and fill it (on the Left) (with fillString) to the length specified.
        /// If initialString is already long enough, it will do nothing
        /// </summary>
        public static string LeftPadFill(string initialString, string fillString, int length)
        {
            StringBuilder sb = new StringBuilder();

            if (initialString != null)
            {
                int stringLength = initialString.Length;
                // If it's already as big or bigger than length, than just return it
                if (stringLength >= length)
                {
                    sb.Append(initialString);
                }
                else
                {
                    sb.Append(Fill(fillString, length - stringLength));
                    sb.Append(initialString);
                }
            }

            return sb.ToString();
        }

        ///// <summary>leftPadFill's an int </summary>
        //public static string LeftPadFill(int value, string fillString, int length)
        //{
        //    return LeftPadFill("" + value, fillString, length);
        //}

        ///// <summary>leftPadFill's a double </summary>
        //public static string LeftPadFill(double value, string fillString, int length)
        //{
        //    return LeftPadFill("" + value, fillString, length);
        //}

        /// <summary>
        /// Will pad the initialString with blanks (on the Left) to the length specified.
        /// If initialString is already long enough, it will do nothing
        /// </summary>
        public static string LeftPad(string initialString, int length)
        {
            return LeftPadFill(initialString, " ", length);
        }

        ///// <summary>leftPad's an int </summary>
        //public static string LeftPad(int value, int length)
        //{
        //    return LeftPad("" + value, length);
        //}

        ///// <summary>leftPad's a double </summary>
        //public static string LeftPad(double value, int length)
        //{
        //    return LeftPad("" + value, length);
        //}

        /// <summary>
        /// Will pad the initialString with zeros (on the Left) to the length specified.
        /// If initialString is already long enough, it will do nothing
        /// </summary>
        public static string LeftZeroPad(string initialString, int length)
        {
            return LeftPadFill(initialString, "0", length);
        }

        ///// <summary>leftZeroPad's an int </summary>
        //public static string LeftZeroPad(int value, int length)
        //{
        //    return LeftZeroPad("" + value, length);
        //}

        ///// <summary>leftZeroPad's a double </summary>
        //public static string LeftZeroPad(double value, int length)
        //{
        //    return LeftZeroPad("" + value, length);
        //}

        /// <summary>
        /// Truncates a string to the specified length - doesn't fail if the string is null or shorter then the specified truncate length
        /// </summary>
        public static string Truncate(string s, int length)
        {
            return Substring(s, 0, length);
        }

        /// <summary>
        /// (from James Coll)
        /// Returns the substring of the given string s starting at the given position i and spanning the 
        /// given l number of characters, or spanning as many characters as are available.  
        /// Returns the empty string if the given string s is null, or the position i is not valid.
        /// </summary>
        public static string Substring(string s, int i, int l)
        {
            if (s == null || i < 0 || i >= s.Length)
            {
                return "";
            }

            if (l < 0 || l > s.Length - i)
            {
                return s.Substring(i, s.Length - i);
            }

            return s.Substring(i, l);
        }

        ///// <summary>
        ///// Returns the given string s, trimmed of leading and trailing whitespace, or the empty string if s is null
        ///// </summary>
        //public static string Trim(string s)
        //{
        //    return s == null ? string.Empty : s.Trim();
        //}
        //#endregion

        //// ******************************************************************************
        //#region String testing methods
        //// ******************************************************************************

        /// <summary> Tests to see if the specified String is either Blank or Null
        /// Note:  a trim() is done to the string before it is tested as blank, thus
        /// "" and " " are both considered blank.
        /// </summary>
        public static bool IsBlankOrNull(string s)
        {
            return String.IsNullOrWhiteSpace(s);
            //if (s == null || s.Trim().Length == 0)
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}
        }

        /// <summary>
        /// Tests ANY object for IsBlankOrNull.  Attempts "Blank" test using ToString() method
        /// </summary>
        public static bool IsBlankOrNull(object o)
        {
            if (o == null || o == DBNull.Value)
            {
                return true;
            }
            else
            {
                return IsBlankOrNull(o.ToString());
            }
        }

        ///// <summary>
        ///// Tests that a String is Not Blank or Null
        ///// </summary>
        ///// <param name="s"></param>
        ///// <returns></returns>
        //public static bool IsNotBlankOrNull(string s)
        //{
        //    if (IsBlankOrNull(s))
        //    {
        //        return false;
        //    }
        //    else
        //    {
        //        return true;
        //    }
        //}

        //// Function to Check for a Valid 'Identifier' (ie. table, column name)
        //public static bool IsIdentifier(String stringToCheck)
        //{
        //    Regex identifierPattern = new Regex("[^a-zA-Z0-9_]");
        //    return !identifierPattern.IsMatch(stringToCheck);
        //}

        ///// <summary>
        ///// Tests if a string is a valid Decimal (ie. it will work with Decimal.Parse())
        ///// </summary>
        //public static bool IsDecimal(string strDecimal)
        //{
        //    decimal d;
        //    return Decimal.TryParse(strDecimal, out d);
        //}

        ///// <summary>
        ///// Tests if a string is a valid Integer (ie. it will work with Int32.Parse())
        ///// </summary>
        ///// <param name="strInteger"></param>
        ///// <returns></returns>
        //public static bool IsInteger(string strInteger)
        //{
        //    Int32 i;
        //    return Int32.TryParse(strInteger, out i);
        //}

        ///*
        ///// <summary> Determines if the String originalValue passed in contains a valid integer.
        ///// The characters in the string must all be decimal digits except the first 
        ///// character may be a minus sign ("-") to indicate a negative originalValue.
        ///// </summary>
        //public static bool IsInteger(string s)
        //{
        //    // Null or blank is NOT a valid integer
        //    if (IsBlankOrNull(s))
        //    {
        //        return false;
        //    }

        //    // Test the first character
        //    char firstCharacter = s[0];
        //    if (!Char.IsDigit(firstCharacter) && firstCharacter != '-')
        //    {
        //        return false;
        //    }

        //    // Step through the string, starting at the SECOND character (index = 1)
        //    for (int i = 1; i < s.Length; i++)
        //    {
        //        if (!Char.IsDigit(s[i]))
        //        {
        //            return false;
        //        }
        //    }
        //    // If you get here, then all is good!
        //    return true;
        //}
        //*/

        ///*
        //public static bool IsInteger(String strNumber)
        //{
        //    Regex objNotIntPattern = new Regex("[^0-9-]");
        //    Regex objIntPattern = new Regex("^-[0-9]+$|^[0-9]+$");
        //    return !objNotIntPattern.IsMatch(strNumber) && objIntPattern.IsMatch(strNumber);
        //}
        //*/


        //// Function to test for Positive Integers.  
        //public static bool IsNaturalNumber(String strNumber)
        //{
        //    Regex objNotNaturalPattern = new Regex("[^0-9]");
        //    Regex objNaturalPattern = new Regex("0*[1-9][0-9]*");
        //    return !objNotNaturalPattern.IsMatch(strNumber) &&
        //            objNaturalPattern.IsMatch(strNumber);
        //}


        //// Function to test for Positive Integers with zero inclusive  
        //public static bool IsWholeNumber(String strNumber)
        //{
        //    Regex objNotWholePattern = new Regex("[^0-9]");
        //    return !objNotWholePattern.IsMatch(strNumber);
        //}

        //// Function to Test for Positive Number both Integer & Real 
        //public static bool IsPositiveNumber(String strNumber)
        //{
        //    Regex objNotPositivePattern = new Regex("[^0-9.]");
        //    Regex objPositivePattern = new Regex("^[.][0-9]+$|[0-9]*[.]*[0-9]+$");
        //    Regex objTwoDotPattern = new Regex("[0-9]*[.][0-9]*[.][0-9]*");
        //    return !objNotPositivePattern.IsMatch(strNumber) &&
        //           objPositivePattern.IsMatch(strNumber) &&
        //           !objTwoDotPattern.IsMatch(strNumber);
        //}


        //// Function to test whether the string is a valid number or not
        ///*
        //public static bool IsNumber(String strNumber)
        //{
        //    Regex objNotNumberPattern = new Regex("[^0-9.-]");
        //    Regex objTwoDotPattern = new Regex("[0-9]*[.][0-9]*[.][0-9]*");
        //    Regex objTwoMinusPattern = new Regex("[0-9]*[-][0-9]*[-][0-9]*");
        //    String strValidRealPattern = "^([-]|[.]|[-.]|[0-9])[0-9]*[.]*[0-9]+$";
        //    String strValidIntegerPattern = "^([-]|[0-9])[0-9]*$";
        //    Regex objNumberPattern = new Regex("(" + strValidRealPattern + ")|(" + strValidIntegerPattern + ")");
        //    return !objNotNumberPattern.IsMatch(strNumber) &&
        //           !objTwoDotPattern.IsMatch(strNumber) &&
        //           !objTwoMinusPattern.IsMatch(strNumber) &&
        //           objNumberPattern.IsMatch(strNumber);
        //}
        //*/


        //// Function To test for Alphabets. 
        //public static bool IsAlpha(String strToCheck)
        //{
        //    Regex objAlphaPattern = new Regex("[^a-zA-Z]");
        //    return !objAlphaPattern.IsMatch(strToCheck);
        //}

        //// Function to Check for AlphaNumeric.
        //public static bool IsAlphaNumeric(string strToCheck)
        //{
        //    Regex objAlphaNumericPattern = new Regex("[^a-zA-Z0-9]");
        //    return !objAlphaNumericPattern.IsMatch(strToCheck);
        //}

        //// Function to Check for AlphaNumeric.
        //public static bool IsAlphaNumeric(string strToCheck, string extraAllowedCharacters)
        //{
        //    // Handle allowing spaces if requested
        //    if (extraAllowedCharacters.Contains(" "))
        //    {
        //        extraAllowedCharacters = extraAllowedCharacters.Replace(" ", "");
        //        strToCheck = strToCheck.Replace(" ", "");
        //    }

        //    // Handle allowing "(" if requested
        //    if (extraAllowedCharacters.Contains("("))
        //    {
        //        extraAllowedCharacters = extraAllowedCharacters.Replace("(", "");
        //        strToCheck = strToCheck.Replace("(", "");
        //    }

        //    // Handle allowing ")" if requested
        //    if (extraAllowedCharacters.Contains(")"))
        //    {
        //        extraAllowedCharacters = extraAllowedCharacters.Replace(")", "");
        //        strToCheck = strToCheck.Replace(")", "");
        //    }

        //    //log.Error(string.Format("***{0}***{1}***", strToCheck, extraAllowedCharacters));

        //    Regex objAlphaNumericPattern = new Regex(string.Format("[^a-zA-Z0-9{0}]", extraAllowedCharacters));
        //    return !objAlphaNumericPattern.IsMatch(strToCheck);
        //}

        ///// <summary> Method: nullSafeEquals
        ///// <p>
        ///// Does an equals() comparing two strings, but doesn't fail if either of the Strings
        ///// are null.
        ///// <p>
        ///// Logic:
        ///// <p>
        ///// if both are null, returns true
        ///// if only one is null, returns false
        ///// if neither are null, tests strings for equality
        ///// 
        ///// </summary>
        ///// <param name="s1,">s2 - strings to compare
        ///// 
        ///// </param>
        ///// <returns> boolean
        ///// </returns>
        //public static bool NullSafeEquals(string s1, string s2)
        //{
        //    if (s1 == null && s2 == null)
        //    {
        //        return true;
        //    }
        //    else if (s1 == null || s2 == null)
        //    {
        //        return false;
        //    }
        //    else
        //    {
        //        return s1.Equals(s2);
        //    }
        //}

        ///// <summary> Method: nullSafeEqualsIgnoreCase
        ///// <p>
        ///// Does an equalsIgnoreCase() comparing two strings, but doesn't fail if either of 
        ///// the Strings are null.
        ///// <p>
        ///// Logic:
        ///// <p>
        ///// if both are null, returns true
        ///// if only one is null, returns false
        ///// if neither are null, tests strings for equality
        ///// 
        ///// </summary>
        ///// <param name="s1,">s2 - strings to compare
        ///// 
        ///// </param>
        ///// <returns> boolean
        ///// </returns>
        //public static bool NullSafeEqualsIgnoreCase(string s1, string s2)
        //{
        //    if (s1 == null && s2 == null)
        //    {
        //        return true;
        //    }
        //    else if (s1 == null || s2 == null)
        //    {
        //        return false;
        //    }
        //    else
        //    {
        //        return s1.ToUpper().Equals(s2.ToUpper());
        //    }
        //}
        //#endregion

        // ******************************************************************************
        #region Get methods - to get typed data from a string
        // ******************************************************************************

        /// <summary>
        /// Gets a DateTime from a object
        /// </summary>
        public static DateTime? GetDateTime(object o)
        {
            return GetDateTime(o, DateTimeKind.Unspecified);
        }


        /// <summary>
        /// Gets a DateTime from a object
        /// </summary>
        public static DateTime GetDateTime(object o, DateTime defaultIfNull)
        {
            return GetDateTime(o, DateTimeKind.Unspecified, defaultIfNull);
        }


        /// <summary>
        /// Gets a DateTime from a object
        /// Sets the DateTime Kind to the specified DateTimeKind
        /// </summary>
        public static DateTime? GetDateTime(object o, DateTimeKind dateTimeKind)
        {
            // Deal with Null
            if (IsBlankOrNull(o))
            {
                return null;
            }

            return GetDateTime(o, dateTimeKind, DateTime.MinValue);
        }

        /// <summary>
        /// Gets a DateTime from a object
        /// Sets the DateTime Kind to the specified DateTimeKind
        /// Supports specification of the default originalValue to use if null
        /// </summary>
        public static DateTime GetDateTime(object o, DateTimeKind dateTimeKind, DateTime defaultIfNull)
        {
            // If originalValue is a DateTime - then just do a cast and set the kind
            if (o is DateTime)
            {
                DateTime dt = (DateTime)o;
                return DateTime.SpecifyKind(dt, dateTimeKind);
            }

            // Deal with Null
            if (IsBlankOrNull(o))
            {
                return defaultIfNull;
            }

            // Try it as a string
            return GetDateTime(o.ToString(), dateTimeKind, DateTime.MinValue);
        }


        /// <summary>
        /// Gets a DateTime from a String
        /// </summary>
        public static DateTime? GetDateTime(string s)
        {
            return GetDateTime(s, DateTimeKind.Unspecified);
        }


        /// <summary>
        /// Gets a DateTime from a String
        /// </summary>
        public static DateTime GetDateTime(string s, DateTime defaultIfNull)
        {
            return GetDateTime(s, DateTimeKind.Unspecified, defaultIfNull);
        }


        /// <summary>
        /// Gets a DateTime from a String
        /// Sets the DateTime Kind to the specified DateTimeKind
        /// </summary>
        public static DateTime? GetDateTime(string s, DateTimeKind dateTimeKind)
        {
            // Handle Nulls
            if (IsBlankOrNull(s))
            {
                return null;
            }

            return GetDateTime(s, dateTimeKind, DateTime.MinValue);
        }


        /// <summary>
        /// Gets a DateTime from a String
        /// Sets the DateTime Kind to the specified DateTimeKind
        /// Supports specification of the default originalValue to use if null
        /// </summary>
        public static DateTime GetDateTime(string s, DateTimeKind dateTimeKind, DateTime defaultIfNull)
        {
            if (IsBlankOrNull(s))
            {
                return defaultIfNull;
            }

            try
            {
                DateTime dt = DateTime.Parse(s);
                return DateTime.SpecifyKind(dt, dateTimeKind);
            }
            catch
            {
                throw new ArgumentOutOfRangeException("Value: " + s + " is not a valid DateTime");
            }
        }


        /// <summary>
        /// Gets an Int16 from an object
        /// </summary>
        public static Int16 GetInt16(object o)
        {
            // If originalValue is an int - then just do a cast
            if (o is Int16)
            {
                return (Int16)o;
            }

            if (o == null || o == DBNull.Value)
            {
                return 0;
            }

            // Try it as a string
            return GetInt16(o.ToString());
        }


        /// <summary>
        /// Gets an Int16 from a String
        /// </summary>
        public static Int16 GetInt16(string s)
        {
            // Accept Blank as valid for 0
            if (IsBlankOrNull(s))
            {
                return 0;
            }

            try
            {
                return Int16.Parse(s);
            }
            catch
            {
                throw new ArgumentOutOfRangeException("Value: " + s + " is not a valid Integer");
            }
        }

        /// <summary>
        /// Gets an Int32 from an object
        /// </summary>
        public static Int32 GetInt32(object o)
        {
            // If originalValue is an int - then just do a cast
            if (o is Int32)
            {
                return (Int32)o;
            }

            if (o == null || o == DBNull.Value)
            {
                return 0;
            }

            // Try it as a string
            return GetInt32(o.ToString());
        }


        /// <summary>
        /// Gets an Int32 from a String
        /// </summary>
        public static Int32 GetInt32(string s)
        {
            // Accept Blank as valid for 0
            if (IsBlankOrNull(s))
            {
                return 0;
            }

            try
            {
                // The following is a workaround to a bug in .Net's Int32.Parse() routine... that SOMETIMES - in very unusual 
                // situations, it FAILS on Int32.Parse("0") or Int32.Parse("1"), etc.

                int returnInt;
                decimal returnDec;
                if (Int32.TryParse(s, System.Globalization.NumberStyles.Integer, System.Globalization.CultureInfo.InvariantCulture, out returnInt))
                {
                    return returnInt;
                }
                else if (decimal.TryParse(s, out returnDec))
                {
                    return (int)decimal.Round(returnDec, 0);
                }
                else
                {
                    return AsciiToInteger(s);
                }
            }
            catch (Exception e)
            {
                throw new ArgumentOutOfRangeException("Value: " + s + " is not a valid Integer.", e);
            }
        }

        /// <summary>
        /// Gets an Int32 from an object
        /// </summary>
        public static Int32? GetNullableInt32(object o)
        {
            // If originalValue is an int - then just do a cast
            if (o is Int32 || o is Int32?)
            {
                return (Int32?)o;
            }

            if (IsBlankOrNull(o))
            {
                return null;
            }

            // Try it as a string
            return GetInt32(o.ToString());
        }


        /// <summary>
        /// Alternate routine to get an integer from a string... used as a "fallback" if Int32.Parse fails (bug in .Net)
        /// </summary>
        /// <param name="stringToConvert"></param>
        /// <returns></returns>
        private static int AsciiToInteger(string stringToConvert)
        {
            int zeroValue = '0';
            char[] characters = stringToConvert.ToCharArray();
            int value = 0;
            for (int i = 0; i < characters.Length; ++i)
            {
                char c = characters[i];
                if (c < '0' || c > '9')
                {
                    throw new FormatException("Input string was not in the correct format.");
                }
                value = 10 * value + (c - zeroValue);
            }
            return value;
        }


        /// <summary>
        /// Gets a Int64 from an object
        /// </summary>
        public static Int64 GetInt64(object o)
        {
            // If originalValue is an int - then just do a cast
            if (o is Int64)
            {
                return (Int64)o;
            }

            if (o == null || o == DBNull.Value)
            {
                return 0;
            }

            // Try it as a string
            return GetInt64(o.ToString());
        }


        /// <summary>
        /// Gets an Int64 from a String
        /// </summary>
        public static Int64 GetInt64(string s)
        {
            // Accept Blank as valid for 0
            if (IsBlankOrNull(s))
            {
                return 0;
            }

            try
            {
                return Int64.Parse(s);
            }
            catch
            {
                throw new ArgumentOutOfRangeException("Value: " + s + " is not a valid Integer");
            }
        }

        /// <summary>
        /// Gets an Int64 from an object
        /// </summary>
        public static Int64? GetNullableInt64(object o)
        {
            // If originalValue is an int - then just do a cast
            if (o is Int64 || o is Int64?)
            {
                return (Int64?)o;
            }

            if (IsBlankOrNull(o))
            {
                return null;
            }

            // Try it as a string
            return GetInt64(o.ToString());
        }


        /// <summary>
        /// Gets a Double from an object
        /// </summary>
        public static double GetDouble(object o)
        {
            // If originalValue is a double - then just do a cast
            if (o is double)
            {
                return (double)o;
            }

            if (o == null || o == DBNull.Value)
            {
                return 0;
            }

            // Try it as a string
            return GetDouble(o.ToString());
        }


        /// <summary>
        /// Gets a Double from a String
        /// </summary>
        public static double GetDouble(string s)
        {
            // Accept Blank as valid for 0
            if (IsBlankOrNull(s))
            {
                return 0;
            }

            try
            {
                return Double.Parse(s);
            }
            catch
            {
                throw new ArgumentOutOfRangeException("Value: " + s + " is not a valid Double");
            }
        }

        /// <summary>
        /// Gets an Double from an object
        /// </summary>
        public static Double? GetNullableDouble(object o)
        {
            // If originalValue is a Double - then just do a cast
            if (o is Double || o is Double?)
            {
                return (Double?)o;
            }

            if (IsBlankOrNull(o))
            {
                return null;
            }

            // Try it as a string
            return GetDouble(o.ToString());
        }


        /// <summary>
        /// Gets a Decimal from an object
        /// </summary>
        public static decimal GetDecimal(object o)
        {
            // If originalValue is a decimal - then just do a cast
            if (o is decimal)
            {
                return (decimal)o;
            }

            if (o == null || o == DBNull.Value)
            {
                return 0;
            }

            // Try it as a string
            return GetDecimal(o.ToString());
        }


        /// <summary>
        /// Gets a Decimal from a String
        /// </summary>
        public static decimal GetDecimal(string s)
        {
            // Accept Blank as valid for 0
            if (IsBlankOrNull(s))
            {
                return 0;
            }

            try
            {
                return Decimal.Parse(s);
            }
            catch
            {
                throw new ArgumentOutOfRangeException("Value: " + s + " is not a valid Decimal");
            }
        }


        /// <summary>
        /// Gets an Decimal from an object
        /// </summary>
        public static Decimal? GetNullableDecimal(object o)
        {
            // If originalValue is a Decimal - then just do a cast
            if (o is Decimal || o is Decimal?)
            {
                return (Decimal?)o;
            }

            if (IsBlankOrNull(o))
            {
                return null;
            }

            // Try it as a string
            return GetDecimal(o.ToString());
        }


        /// <summary>
        /// Gets a Boolean from an object
        /// </summary>
        public static bool GetBoolean(object o)
        {
            // If originalValue is a bool - then just do a cast
            if (o is bool)
            {
                return (bool)o;
            }

            if (o == null || o == DBNull.Value)
            {
                return false;
            }

            // Try it as a string
            return GetBoolean(o.ToString());
        }


        /// <summary>
        /// Gets a Boolean from a String
        /// </summary>
        public static bool GetBoolean(string s)
        {
            // Accept Blank as valid for false
            if (IsBlankOrNull(s))
            {
                return false;
            }

            s = s.Trim().ToLower();

            if (s == "f" || s == "0" || s == "no")
            {
                return false;
            }
            else if (s == "t" || s == "1" || s == "yes")
            {
                return true;
            }

            try
            {
                return Boolean.Parse(s);
            }
            catch
            {
                throw new ArgumentOutOfRangeException("Value: " + s + " is not a valid Boolean");
            }
        }

        /// <summary>
        /// Gets a boolean from an object - if the object is null - it returns null
        /// </summary>
        public static bool? GetNullableBoolean(object o)
        {
            // If originalValue is a bool - then just do a cast
            if (o is bool || o is bool?)
            {
                return (bool?)o;
            }

            if (o == null || o == DBNull.Value)
            {
                return null;
            }

            // Try it as a string
            return GetBoolean(o.ToString());
        }

        /// <summary> Converts an Object to String - null safe
        /// If the object is Null, it returns null
        /// </summary>
        /// <param name="o">- input object to convert to a String
        /// </param>
        public static string GetString(Object o)
        {
            if (o == null)
            {
                return "";
            }
            else
            {
                return o.ToString();
            }
        }

        #endregion

        //// ******************************************************************************
        //#region Other String Methods
        //// ******************************************************************************

        public static string Repeat(string value, int repeatTimes)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < repeatTimes; i++)
            {
                sb.Append(value);
            }
            return sb.ToString();
        }

        ///// <summary>
        ///// Returns an empty string if the original value is null
        ///// </summary>
        ///// <param name="value">a string to be promoted if null</param>
        ///// <returns>returns the given string s, if not null, otherwise returns the empty string</returns>
        //public static string TranslateNull(string value)
        //{
        //    return value == null ? string.Empty : value;
        //}

        ///// <summary>
        ///// Returns either the original value or the valueIfNull value if the original value is null.
        ///// </summary>
        ///// <typeparam name="T"></typeparam>
        ///// <param name="value">Original Value</param>
        ///// <param name="valueIfNull">Value to return if the Original Value is null</param>
        ///// <returns></returns>
        //public static T TranslateNull<T>(T value, T valueIfNull)
        //{
        //    if (value == null)
        //    {
        //        return valueIfNull;
        //    }
        //    else
        //    {
        //        return value;
        //    }
        //}

        //public static string StripHtml(string inputString)
        //{
        //    return Regex.Replace(inputString, @"<(.|\n)*?>", string.Empty);
        //}


        ///// <summary> Replaces values in a string 
        ///// An example is: replace("hello", "ello", "appy") => happy
        ///// 
        ///// (from http://javaalmanac.com/egs/java.lang/ReplaceString.html?l=rel)
        ///// 
        ///// </summary>
        ///// <param name="str">String to replace characters in
        ///// </param>
        ///// <param name="pattern">String to find that will be replaced
        ///// </param>
        ///// <param name="replace">String to put into the target String
        ///// </param>
        //public static string Replace(string str, string pattern, string replace)
        //{
        //    int s = 0;
        //    int e = 0;
        //    StringBuilder result = new StringBuilder();

        //    //UPGRADE_WARNING: Method 'java.lang.String.indexOf' was converted to 'string.IndexOf' which may throw an exception. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1101"'
        //    while ((e = str.IndexOf(pattern, s)) >= 0)
        //    {
        //        result.Append(str.Substring(s, (e) - (s)));
        //        result.Append(replace);
        //        s = e + pattern.Length;
        //    }
        //    result.Append(str.Substring(s));
        //    return result.ToString();
        //}
        //#endregion

        //// ******************************************************************************
        //#region Other String Methods - From www.generationjava.com - StringW class
        //// ******************************************************************************

        ///*

        ///// <summary> 
        ///// Translate characters in a String.
        ///// An example is:  translate("hello", "ho", "jy") => jelly
        ///// If the length of characters to search for is greater than the 
        ///// length of characters to replace, then the last character is 
        ///// used.
        ///// 
        ///// </summary>
        ///// <param name="target">String to replace characters  in
        ///// </param>
        ///// <param name="repl">String to find that will be replaced
        ///// </param>
        ///// <param name="with">String to put into the target String
        ///// </param>
        //public static string Translate(string target, string repl, string with)
        //{
        //    StringBuilder buffer = new StringBuilder(target.Length);
        //    char[] chrs = target.ToCharArray();
        //    char[] withChrs = with.ToCharArray();
        //    int sz = chrs.Length;
        //    int withMax = with.Length - 1;
        //    for (int i = 0; i < sz; i++)
        //    {
        //        int idx = repl.IndexOf((Char)chrs[i]);
        //        if (idx != -1)
        //        {
        //            if (idx > withMax)
        //            {
        //                idx = withMax;
        //            }
        //            buffer.Append(withChrs[idx]);
        //        }
        //        else
        //        {
        //            buffer.Append(chrs[i]);
        //        }
        //    }
        //    return buffer.ToString();
        //}

        ///// <summary> How many times is the substring in the larger string.</summary>
        //static public int CountMatches(string str, string sub)
        //{
        //    int count = 0;
        //    int idx = 0;
        //    //UPGRADE_WARNING: Method 'java.lang.String.indexOf' was converted to 'string.IndexOf' which may throw an exception. 'ms-help://MS.VSCC.2003/commoner/redir/redirect.htm?keyword="jlca1101"'
        //    while ((idx = str.IndexOf(sub, idx)) != -1)
        //    {
        //        count++;
        //        idx += sub.Length;
        //    }
        //    return count;
        //}

        ///// <summary> Is a String a word. Contains only unicode letters.</summary>
        //static public bool IsWord(string str)
        //{
        //    int sz = str.Length;
        //    for (int i = 0; i < sz; i++)
        //    {
        //        if (!Char.IsLetter(str[i]))
        //        {
        //            return false;
        //        }
        //    }
        //    return true;
        //}

        ///// <summary> Does a String contain only unicode letters or digits.</summary>
        //static public bool IsAlphanumeric(string str)
        //{
        //    int sz = str.Length;
        //    for (int i = 0; i < sz; i++)
        //    {
        //        if (!Char.IsLetterOrDigit(str[i]))
        //        {
        //            return false;
        //        }
        //    }
        //    return true;
        //}

        ///// <summary> Does a String contain only unicode digits.</summary>
        //static public bool IsNumeric(object value)
        //{
        //    if (value == null)
        //    {
        //        return false;
        //    }

        //    string str = value.ToString();

        //    int sz = str.Length;
        //    for (int i = 0; i < sz; i++)
        //    {
        //        if (!Char.IsDigit(str[i]))
        //        {
        //            return false;
        //        }
        //    }
        //    return true;
        //}

        ///// <summary> Is a String a valid number (allows leading minus (-) sign and decimal points)
        ///// Doesn't allow scientific notation.
        ///// </summary>
        //static public bool IsNumber(object value)
        //{
        //    if (value == null)
        //    {
        //        return false;
        //    }

        //    string str = value.ToString();

        //    char[] chrs = str.ToCharArray();
        //    int sz = chrs.Length;
        //    bool decimal_Renamed = false;
        //    for (int i = 0; i < sz; i++)
        //    {
        //        // possibly faster as a continuous switch
        //        if ((chrs[i] >= '0') && (chrs[i] <= '9'))
        //        {
        //            continue;
        //        }
        //        if (i == 0)
        //        {
        //            if (chrs[i] == '-')
        //            {
        //                continue;
        //            }
        //        }
        //        if (chrs[i] == '.')
        //        {
        //            if (!decimal_Renamed)
        //            {
        //                decimal_Renamed = true;
        //                continue;
        //            }
        //        }
        //        return false;
        //    }
        //    return true;
        //}

        ///// <summary> Is a String a line, containing only letters, digits or 
        ///// whitespace, and ending with an optional newline.
        ///// NB: Punctuation not allowed.
        ///// </summary>
        //static public bool IsLine(string str)
        //{
        //    // char ch = (char) (0);
        //    char[] chrs = str.ToCharArray();
        //    int sz = chrs.Length - 1;
        //    for (int i = 0; i < sz - 2; i++)
        //    {
        //        if (!Char.IsLetterOrDigit(chrs[i]))
        //        {
        //            if (!Char.IsWhiteSpace(chrs[i]))
        //            {
        //                return false;
        //            }
        //        }
        //    }
        //    if (!Char.IsLetterOrDigit(chrs[sz - 1]))
        //    {
        //        if (!Char.IsWhiteSpace(chrs[sz - 1]))
        //        {
        //            if (chrs[sz - 1] != '\r')
        //            {
        //                return false;
        //            }
        //            else if (chrs[sz] != '\n')
        //            {
        //                return false;
        //            }
        //        }
        //    }
        //    if (!Char.IsLetterOrDigit(chrs[sz]))
        //    {
        //        if (!Char.IsWhiteSpace(chrs[sz]))
        //        {
        //            if (chrs[sz] != '\n')
        //            {
        //                return false;
        //            }
        //        }
        //    }
        //    return true;
        //}
        //*/
        //#endregion

        // ******************************************************************************
        #region Code Building Support Methods
        // ******************************************************************************

        public static string GetHeadingFromFieldName(string fieldName)
        {
            if (IsBlankOrNull(fieldName)) { return ""; }

            List<Char> output = new List<char>();

            char[] characters = fieldName.ToCharArray();
            int lastChar = characters.Length - 1;

            // First character is ALWAYS output
            output.Add(characters[0]);

            // Loop - starting at the SECOND character
            for (int i = 1; i <= lastChar; i++)
            {
                // Translate a "_" to a space
                if (characters[i] == '_')
                {
                    output.Add(' ');
                    continue;
                }

                // Only add a space if this character is Upper Case
                if (Char.IsUpper(characters[i]))
                {
                    // AND only then if the NEXT character is Lower
                    if (i < lastChar && Char.IsLower(characters[i + 1]))
                    {
                        output.Add(' ');
                    }
                    // OR the PREVIOUS character was lower
                    else if (i > 1 && Char.IsLower(characters[i - 1]))
                    {
                        output.Add(' ');
                    }
                }

                // Only other time to add a space if this character is a Digit
                else if (Char.IsDigit(characters[i]))
                {
                    // Only add a space if the PREVIOUS character was NOT a Digit
                    if (i > 1 && !Char.IsDigit(characters[i - 1]))
                    {
                        output.Add(' ');
                    }
                }

                // Add the current character
                output.Add(characters[i]);
            }

            return new string(output.ToArray());
        }
        #endregion

        //// ******************************************************************************
        //#region Methods to dump objects to Strings
        //// ******************************************************************************

        //public static string DumpToString(IDictionary dict)
        //{
        //    return DumpToString(dict, "\n", ":", false);
        //}

        //public static string DumpToString(IDictionary dict, string itemDelimiter, string keyValueDelimiter)
        //{
        //    return DumpToString(dict, itemDelimiter, keyValueDelimiter, false);
        //}

        //public static string DumpToString(IDictionary dict, string itemDelimiter, string keyValueDelimiter, bool sort)
        //{
        //    List<object> keys = new List<object>();
        //    foreach (var key in dict.Keys)
        //    {
        //        keys.Add(key);
        //    }

        //    if (sort)
        //    {
        //        keys.Sort();
        //    }

        //    StringDelimiter sd = new StringDelimiter(itemDelimiter);
        //    foreach (var key in keys)
        //    {
        //        sd.Add("{0}{1}{2}", key, keyValueDelimiter, dict[key]);
        //    }

        //    return sd.ToString();
        //}

        //public static string DumpToString(IEnumerable list)
        //{
        //    return DumpToString("\n", list);
        //}

        //public static string DumpToString(string itemDelimiter, IEnumerable list)
        //{
        //    StringDelimiter sd = new StringDelimiter(itemDelimiter);
        //    foreach (object o in list)
        //    {
        //        if (o != null) { sd.Add(o); }
        //        else { sd.Add(string.Empty); }
        //    }
        //    return sd.ToString();
        //}

        //#endregion
    }
}

