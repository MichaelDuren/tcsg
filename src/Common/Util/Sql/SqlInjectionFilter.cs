
namespace Util.Sql
{
	/// <summary>
	/// Class - to provide basic SQL Injection protection
	/// </summary>
	public class SqlInjectionFilter
	{

		/// <summary>
		/// Method to clean up a string - to make it safe to use in dynamic sql
		/// </summary>
		/// <param name="dirtyString"></param>
		/// <returns></returns>
		public static string Clean(string dirtyString)
		{
			if (dirtyString == null)
			{
				dirtyString = string.Empty;
			}

			string cleanString = dirtyString;

			// Replace ' with ''
			cleanString = cleanString.Replace("'", "''");

			// Remove bad strings
			cleanString = cleanString.Replace(";", string.Empty);
			// cleanString = cleanString.Replace(",", string.Empty);
            
            // SQL Parameter marker
            cleanString = cleanString.Replace("@", string.Empty);
            
            // catalog-extended stored procedures
            cleanString = cleanString.Replace("xp_", string.Empty);

            // Comment delimiters
            cleanString = cleanString.Replace("--", string.Empty);
            cleanString = cleanString.Replace("/*", string.Empty);
            cleanString = cleanString.Replace("*/", string.Empty);
            
            // LIKE wildcard characters
            cleanString = cleanString.Replace("[", string.Empty);
            cleanString = cleanString.Replace("]", string.Empty);
            cleanString = cleanString.Replace("%", string.Empty);
            // cleanString = cleanString.Replace("_", string.Empty);

			return cleanString;
		}

	}
}
