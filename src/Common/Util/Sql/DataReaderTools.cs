﻿using System;
using System.Collections.Generic;
using System.Data;

namespace Util.Sql
{
    public static class DataReaderTools
    {
        /// <summary>
        /// Returns the column definitons for the specified dataReader
        /// </summary>
        public static List<DbColumnDefinition> GetColumnDefinitions(IDataReader dataReader)
        {
            Guard.ArgumentNotNull(dataReader, "dataReader");

            List<DbColumnDefinition> columnDefinitions = new List<DbColumnDefinition>();

            DataTable schemaTable = dataReader.GetSchemaTable();

            foreach (DataRow field in schemaTable.Rows)
            {
                DbColumnDefinition columnDef = new DbColumnDefinition
                {
                    ColumnName = field["ColumnName"] as string,
                    ColumnOrdinal = StringTools.GetInt32(field["ColumnOrdinal"]),
                    ColumnSize = StringTools.GetInt32(field["ColumnSize"]),
                    NumericPrecision = StringTools.GetInt32(field["NumericPrecision"]),
                    NumericScale = StringTools.GetInt32(field["NumericScale"]),
                    DotNetDataType = field["DataType"] as Type,
                    ProviderType = StringTools.GetInt32(field["ProviderType"]),
                    AllowDbNull = StringTools.GetBoolean(field["AllowDbNull"]),
                    IsLong = StringTools.GetBoolean(field["IsLong"]),
                    IsUnique = StringTools.GetBoolean(field["IsUnique"]),
                    IsReadOnly = StringTools.GetBoolean(field["IsReadOnly"]),
                    IsRowVersion = StringTools.GetBoolean(field["IsRowVersion"]),
                };

                columnDefinitions.Add(columnDef);
            }

            return columnDefinitions;
        }

    }
}
