using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Text;
using Util.Collections;
using Util.Sql.Query;

namespace Util.Sql
{
    public static class DbTools
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const string DEFAULT_PARAMETER_PREFIX = "@";

        // ******************************************************************************
        #region Create SQL Methods
        // ******************************************************************************

        ///// <summary>
        ///// Creates a SQL Select statement to select a record by it's primary key
        ///// Example Output: SELECT lModelId, Name, BarCode, lParentId FROM amModel WHERE lModelId = @lModelId
        ///// </summary>
        ///// <param name="columns">String array of columns</param>
        ///// <param name="tableName">Name of the Table (for the FROM clause)</param>
        ///// <param name="primaryKeyName">Name of the Primary Key (for the WHERE clause)</param>
        ///// <param name="primaryKeyValue">Value of the PrimaryKey (if it is a string - 
        ///// it needs to be wrapped in single quotes</param>
        ///// <returns>String _sql - Select Statement</returns>
        //public static string CreateSelectByPrimaryKey(
        //    string[] columns,
        //    string tableName,
        //    string primaryKeyName,
        //    string primaryKeyValue,
        //    bool isSqlServer)
        //{
        //    // Add column names to Delimiter
        //    StringDelimiter sd = new StringDelimiter(", ");
        //    foreach (string column in columns)
        //    {
        //        string modifiedColumnName = column;

        //        if (isSqlServer)
        //        {
        //            modifiedColumnName = String.Format("[{0}]", column);
        //        }
        //        sd.Add(modifiedColumnName);
        //    }

        //    string _sql = string.Format(
        //        @"SELECT {0} FROM {1} WHERE {2} = {3}",
        //        sd.ToString(),
        //        tableName,
        //        primaryKeyName,
        //        primaryKeyValue);

        //    return _sql;
        //}

        //public static string CreateSelectByPrimaryKey(
        //    string[] columns,
        //    string tableName,
        //    string primaryKeyName,
        //    string primaryKeyValue)
        //{
        //    return CreateSelectByPrimaryKey(columns, tableName, primaryKeyName, primaryKeyValue, false);
        //}

        //public static string CreateSelect(
        //    string[] columns,
        //    string tableName,
        //    string[] whereColumns)
        //{
        //    return CreateSelect(columns, tableName, whereColumns, DEFAULT_PARAMETER_PREFIX);
        //}

        //public static string CreateSelect(
        //    string[] columns,
        //    string tableName,
        //    string[] whereColumns,
        //    string parameterPrefix)
        //{
        //    return CreateSelect(columns, tableName, whereColumns, DEFAULT_PARAMETER_PREFIX, false);
        //}

        ///// <summary>
        ///// Creates a SQL Select statement to select data from a table - based on a where clause
        ///// Example Output: SELECT lModelId, Name, BarCode, lParentId FROM amModel WHERE lModelId = @lModelId AND key2 = @key2 AND key3 = @key3
        ///// </summary>
        ///// <param name="columns">String array of columns</param>
        ///// <param name="tableName">Name of the Table (for the FROM clause)</param>
        ///// <param name="whereColumns">String array of column names for the WHERE clause</param>
        ///// <returns>String _sql - Select Statement</returns>
        //public static string CreateSelect(
        //    string[] columns,
        //    string tableName,
        //    string[] whereColumns,
        //    string parameterPrefix,
        //    bool isSqlServer)
        //{
        //    string _sql = CreateSelect(columns, tableName);

        //    if (whereColumns.Length > 0)
        //    {
        //        // Create WHERE clause columns
        //        StringDelimiter sd = new StringDelimiter(" AND ");
        //        foreach (string column in whereColumns)
        //        {
        //            string modifiedColumnName = column;

        //            if (isSqlServer)
        //            {
        //                modifiedColumnName = String.Format("[{0}]", column);
        //            }
        //            sd.Add(modifiedColumnName + " = " + parameterPrefix + column);
        //        }

        //        _sql += string.Format(@" WHERE {0}", sd.ToString());
        //    }

        //    return _sql;
        //}

        ///// <summary>
        ///// Creates a SQL Select statement to select data from a table
        ///// Example Output: SELECT lModelId, Name, BarCode, lParentId FROM amModel
        ///// </summary>
        ///// <param name="columns">String array of columns</param>
        ///// <param name="tableName">Name of the Table (for the FROM clause)</param>
        ///// <returns>String _sql - Select Statement</returns>
        //public static string CreateSelect(
        //    string[] columns,
        //    string tableName,
        //    bool isSqlServer)
        //{
        //    // Create SELECT clause columns
        //    StringDelimiter sd = new StringDelimiter(", ");
        //    foreach (string column in columns)
        //    {
        //        string modifiedColumnName = column;
        //        if (isSqlServer)
        //        {
        //            modifiedColumnName = String.Format("[{0}]", column);
        //        }
        //        sd.Add(modifiedColumnName);
        //    }

        //    string _sql = string.Format(
        //        @"SELECT {0} FROM {1}",
        //        sd.ToString(),
        //        tableName);

        //    return _sql;
        //}

        //public static string CreateSelect(
        //    string[] columns,
        //    string tableName)
        //{
        //    return CreateSelect(columns, tableName, false);
        //}


        //public static string CreateInsert(
        //    string[] columns,
        //    string tableName)
        //{
        //    return CreateInsert(columns, tableName, DEFAULT_PARAMETER_PREFIX);
        //}

        //public static string CreateInsert(
        //    string[] columns,
        //    string tableName,
        //    string parameterPrefix)
        //{
        //    return CreateInsert(columns, tableName, parameterPrefix, false);
        //}
        ///// <summary>
        ///// Creates a SQL Insert statement
        ///// Example Output: INSERT INTO amModel (lModelId, Name, BarCode, lParentId) VALUES (@lModelId, @Name, @BarCode, @lParentId)
        ///// </summary>
        ///// <param name="columns">String array of columns</param>
        ///// <param name="tableName">Name of the Table (for the FROM clause)</param>
        ///// <returns>String _sql - Insert Statement</returns>
        //public static string CreateInsert(
        //    string[] columns,
        //    string tableName,
        //    string parameterPrefix,
        //    bool isSqlServer)
        //{
        //    StringDelimiter sdColumns = new StringDelimiter(", ");
        //    StringDelimiter sdValues = new StringDelimiter(", ");

        //    // Add column names to Delimiters
        //    foreach (string column in columns)
        //    {
        //        string modifiedColumn = column;
        //        if (isSqlServer)
        //        {
        //            modifiedColumn = String.Format("[{0}]", column);
        //        }

        //        sdColumns.Add(modifiedColumn);
        //        sdValues.Add(parameterPrefix + column);
        //    }

        //    string _sql = string.Format(
        //        @"INSERT INTO {0} ({1}) VALUES ({2})",
        //        tableName,
        //        sdColumns.ToString(),
        //        sdValues.ToString());

        //    return _sql;
        //}

        //public static string CreateUpdate(
        //    string[] columns,
        //    string tableName,
        //    string primaryKeyName,
        //    string primaryKeyValue)
        //{
        //    return CreateUpdate(columns, tableName, primaryKeyName, primaryKeyValue, DEFAULT_PARAMETER_PREFIX);
        //}

        //public static string CreateUpdate(
        //    string[] columns,
        //    string tableName,
        //    string primaryKeyName,
        //    string primaryKeyValue,
        //    string parameterPrefix)
        //{
        //    return CreateUpdate(columns, tableName, primaryKeyName, primaryKeyValue, parameterPrefix, false);
        //}
        ///// <summary>
        ///// Creates a SQL Update statement
        ///// Example Output: UPDATE amModel SET Name = @Name, BarCode = @BarCode, lParentId = @lParentId WHERE lModelId = @lModelId
        ///// </summary>
        ///// <param name="columns">String array of columns</param>
        ///// <param name="tableName">Name of the Table (for the FROM clause)</param>
        ///// <param name="primaryKeyName">Name of the Primary Key (for the WHERE clause)</param>
        ///// <param name="primaryKeyValue">Value of the PrimaryKey (if it is a string - 
        ///// it needs to be wrapped in single quotes</param>
        ///// <returns>String _sql - Update Statement</returns>
        //public static string CreateUpdate(
        //    string[] columns,
        //    string tableName,
        //    string primaryKeyName,
        //    string primaryKeyValue,
        //    string parameterPrefix,
        //    bool isSqlServer)
        //{
        //    // Add "Setters" names to Delimiters
        //    StringDelimiter sd = new StringDelimiter(", ");
        //    foreach (string column in columns)
        //    {
        //        string modifiedColumnName = column;
        //        if (isSqlServer)
        //        {
        //            modifiedColumnName = String.Format("[{0}]", column);
        //        }
        //        sd.Add(modifiedColumnName + " = " + parameterPrefix + column);
        //    }

        //    string _sql = string.Format(
        //        @"UPDATE {0} SET {1} WHERE {2} = {3}",
        //        tableName,
        //        sd.ToString(),
        //        primaryKeyName,
        //        primaryKeyValue);

        //    return _sql;
        //}

        ///// <summary>
        ///// Creates a SQL Update statement
        ///// Example Output: UPDATE amModel SET Name = @Name, BarCode = @BarCode, lParentId = @lParentId WHERE lModelId = @lModelId AND key2 = @key2 AND key3 = @key3
        ///// </summary>
        ///// <param name="columns">String array of columns</param>
        ///// <param name="tableName">Name of the Table (for the FROM clause)</param>
        ///// <param name="whereColumns">String array of column names for the WHERE clause</param>
        ///// <returns>String _sql - Update Statement</returns>
        //public static string CreateUpdate(
        //    string[] columns,
        //    string tableName,
        //    string[] whereColumns,
        //    string parameterPrefix,
        //    bool isSqlServer)
        //{
        //    // Create "SET" columns
        //    StringDelimiter sd1 = new StringDelimiter(", ");
        //    foreach (string column in columns)
        //    {
        //        string modifiedColumn = column;
        //        if (isSqlServer)
        //        {
        //            modifiedColumn = String.Format("[{0}]", column);
        //        }
        //        sd1.Add(modifiedColumn + " = " + parameterPrefix + column);
        //    }

        //    // Create WHERE clause columns
        //    StringDelimiter sd2 = new StringDelimiter(" AND ");
        //    foreach (string column in whereColumns)
        //    {
        //        string modifiedColumn = column;
        //        if (isSqlServer)
        //        {
        //            modifiedColumn = String.Format("[{0}]", column);
        //        }

        //        sd2.Add(modifiedColumn + " = " + parameterPrefix + column);
        //    }

        //    // Create the Update Statement
        //    string _sql = string.Format(
        //        @"UPDATE {0} SET {1} WHERE {2}",
        //        tableName,
        //        sd1.ToString(),
        //        sd2.ToString());

        //    return _sql;
        //}

        //public static string CreateDelete(
        //    string tableName,
        //    string[] whereColumns,
        //    string parameterPrefix)
        //{
        //    return CreateDelete(tableName, whereColumns, parameterPrefix, false);
        //}
        ///// <summary>
        ///// Creates a SQL Delete statement
        ///// Example Output: DELETE FROM amModel WHERE lModelId = @lModelId AND key2 = @key2 AND key3 = @key3
        ///// </summary>
        ///// <param name="tableName">Name of the Table (for the FROM clause)</param>
        ///// <param name="whereColumns">String array of column names for the WHERE clause</param>
        ///// <returns>String _sql - Delete Statement</returns>
        //public static string CreateDelete(
        //    string tableName,
        //    string[] whereColumns,
        //    string parameterPrefix,
        //    bool isSqlServer)
        //{
        //    // Create Where Clause
        //    string whereClause = string.Empty;

        //    if (whereColumns.Length > 0)
        //    {
        //        // Create WHERE clause columns
        //        StringDelimiter sd = new StringDelimiter(" AND ");
        //        foreach (string column in whereColumns)
        //        {
        //            string modifiedColumn = column;
        //            if (isSqlServer)
        //            {
        //                modifiedColumn = String.Format("[{0}]", column);
        //            }

        //            sd.Add(modifiedColumn + " = " + parameterPrefix + column);
        //        }

        //        whereClause = "WHERE " + sd.ToString();
        //    }

        //    // Create the Delete Statement
        //    string _sql = string.Format(
        //        @"DELETE FROM {0} {1}",
        //        tableName,
        //        whereClause);

        //    return _sql;
        //}

        #endregion

        // ******************************************************************************
        #region Parameter Methods
        // ******************************************************************************

        /// <summary>
        /// Helper method to easily add a set of parameters to a command
        /// </summary>
        /// <param name="command"></param>
        /// <param name="dataParameters"></param>
        public static void AddParametersToCommand(
            IDbCommand command,
            DataParameters dataParameters)
        {
            // Add any Parameters
            foreach (DataParameter parm in dataParameters)
            {
                // Create DB Parameter
                IDbDataParameter dbParm = command.CreateParameter();
                dbParm.ParameterName = parm.ParameterName;
                dbParm.DbType = parm.DbType;

                // Deal with Nulls
                if (parm.Value == null)
                {
                    dbParm.Value = DBNull.Value;
                }
                else
                {
                    dbParm.Value = parm.Value;
                }

                dbParm.Direction = parm.Direction;

                // Add size if specified
                if (parm.Size > 0)
                {
                    dbParm.Size = parm.Size;
                }

                // Add it to the command
                command.Parameters.Add(dbParm);
            }
        }

        /// <summary>
        /// Helper method to easily get parameter values back from a command 
        ///		used when calling a Stored Procedure - to get Output, InputOutput, ReturnValue values back from a stored procedure
        /// </summary>
        /// <param name="command"></param>
        /// <param name="dataParameters"></param>
        public static void GetParameterValuesFromCommand(
            IDbCommand command,
            DataParameters dataParameters)
        {
            // Only interested if this is for a Stored Procedure
            if (command.CommandType == CommandType.StoredProcedure)
            {
                foreach (DataParameter parm in dataParameters)
                {
                    // Only interested in Non-Input type parameters
                    if (parm.Direction != ParameterDirection.Input)
                    {
                        IDbDataParameter dbParm = (IDbDataParameter)command.Parameters[parm.ParameterName];
                        parm.Value = dbParm.Value;
                    }
                }
            }
        }

        ///// <summary>
        ///// Helper Method - to make it quick and easy to create a DataParameters collection
        ///// </summary>
        ///// <param name="dataParameter"></param>
        ///// <returns>DataParameters collection</returns>
        //public static DataParameters CreateDataParameters(params DataParameter[] dataParameter)
        //{
        //    // Create the Data Parameters collection
        //    DataParameters dp = new DataParameters();
        //    foreach (DataParameter parm in dataParameter)
        //    {
        //        dp.Add(parm);
        //    }
        //    return dp;
        //}

        #endregion

        // ******************************************************************************
        #region Data Reader Convenience Methods
        // ******************************************************************************

        /// <summary>
        /// Returns a Row from the Data Reader as a CaseInsensitiveHashtable
        /// </summary>
        /// <param name="dataReader">IDataReader object to convert to NameValue</param>
        /// <returns>A CaseInsensitiveHashtable of the columns in the row.  
        ///		Returns null if no rows exist in the data reader</returns>
        public static CaseInsensitiveHashtable GetRowAsHashtable(
            IDataReader dataReader)
        {
            int columnCount = dataReader.FieldCount;
            CaseInsensitiveHashtable ht = null;

            // Just get ONE row
            if (dataReader.Read())
            {
                ht = new CaseInsensitiveHashtable();

                // Read and render each column
                for (int column = 0; column <= columnCount - 1; column++)
                {
                    object columnValue = null;

                    if (dataReader.IsDBNull(column))
                    {
                        columnValue = null;
                    }
                    else
                    {
                        columnValue = dataReader[column];
                    }

                    // Add this column to the collection
                    ht.Add(dataReader.GetName(column), columnValue);
                }
            }
            return ht;
        }

        /// <summary>
        /// Returns a Row from the Data Reader as an Array
        /// </summary>
        /// <param name="dataReader">IDataReader object to convert to NameValue</param>
        /// <returns>A Hashtable of the columns in the row.  
        ///		Returns null if no rows exist in the data reader</returns>
        public static object[] GetRowAsArray(
            IDataReader dataReader)
        {
            int columnCount = dataReader.FieldCount;
            ArrayList arrayList = null;

            // Just get ONE row
            if (dataReader.Read())
            {
                arrayList = new ArrayList();

                // Read and render each column
                for (int column = 0; column <= columnCount - 1; column++)
                {
                    object columnValue = null;

                    if (dataReader.IsDBNull(column))
                    {
                        columnValue = null;
                    }
                    else
                    {
                        columnValue = dataReader[column];
                    }

                    // Add this column to the collection
                    arrayList.Add(columnValue);
                }
            }

            // Make sure that we don't get a null exception
            if (arrayList == null)
            {
                return null;
            }
            else
            {
                return arrayList.ToArray();
            }
        }

        /// <summary>
        /// Returns a Row from the Data Reader as an Array
        /// </summary>
        /// <param name="dataReader">IDataReader object to convert to NameValue</param>
        /// <returns>A Hashtable of the columns in the row.  
        ///		Returns null if no rows exist in the data reader</returns>
        public static ArrayList GetRowAsArrayList(
            IDataReader dataReader)
        {
            int columnCount = dataReader.FieldCount;
            ArrayList arrayList = null;

            // Just get ONE row
            if (dataReader.Read())
            {
                arrayList = new ArrayList();

                // Read and render each column
                for (int column = 0; column <= columnCount - 1; column++)
                {
                    object columnValue = null;

                    if (dataReader.IsDBNull(column))
                    {
                        columnValue = null;
                    }
                    else
                    {
                        columnValue = dataReader[column];
                    }

                    // Add this column to the collection
                    arrayList.Add(columnValue);
                }
            }

            // Make sure that we don't get a null exception
            if (arrayList == null)
            {
                return null;
            }
            else
            {
                return arrayList;
            }
        }

        #endregion

        // ******************************************************************************
        #region Identity Insert Methods
        // ******************************************************************************

        //public static void EnableIdentityInsert(IDbConnection connection, string tableName, IDbTransaction transaction = null)
        //{
        //    using (IDbCommand command = connection.CreateCommand())
        //    {
        //        if (transaction != null)
        //        {
        //            command.Transaction = transaction;
        //        }

        //        command.CommandText = String.Format("SET IDENTITY_INSERT {0} ON;", tableName);

        //        try
        //        {
        //            command.ExecuteNonQuery();
        //        }
        //        catch
        //        { }
        //    }
        //}


        //public static void DisableIdentityInsert(IDbConnection connection, string tableName, IDbTransaction transaction = null)
        //{
        //    using (IDbCommand command = connection.CreateCommand())
        //    {
        //        if (transaction != null)
        //        {
        //            command.Transaction = transaction;
        //        }

        //        command.CommandText = String.Format("SET IDENTITY_INSERT {0} OFF;", tableName);

        //        try
        //        {
        //            command.ExecuteNonQuery();
        //        }
        //        catch
        //        { }
        //    }
        //}

        #endregion

        // ******************************************************************************
        #region Constraint Methods
        // ******************************************************************************

        //public static void DisableConstraintsForTable(IDbConnection connection, string tableName, IDbTransaction transaction = null)
        //{
        //    using (IDbCommand command = connection.CreateCommand())
        //    {
        //        if (transaction != null)
        //        {
        //            command.Transaction = transaction;
        //        }

        //        command.CommandText = String.Format("ALTER TABLE {0} NOCHECK CONSTRAINT ALL;", tableName);

        //        try
        //        {
        //            command.ExecuteNonQuery();

        //            log.InfoFormat("Constraints disabled for {0}", tableName);
        //        }
        //        catch (Exception e)
        //        {
        //            log.ErrorFormat("Failed to disable constraints for table {0}.  Message:{1}", tableName, e.Message);
        //            throw e;
        //        }
        //    }
        //}

        //public static void EnableConstraintsForTable(IDbConnection connection, string tableName, IDbTransaction transaction = null)
        //{
        //    using (IDbCommand command = connection.CreateCommand())
        //    {
        //        if (transaction != null)
        //        {
        //            command.Transaction = transaction;
        //        }

        //        command.CommandText = String.Format("ALTER TABLE {0} CHECK CONSTRAINT ALL;", tableName);

        //        try
        //        {
        //            command.ExecuteNonQuery();

        //            log.InfoFormat("Constraints enabled for {0}", tableName);
        //        }
        //        catch (Exception e)
        //        {
        //            log.ErrorFormat("Failed to enable constraints for table {0}.  Message:{1}", tableName, e.Message);
        //            throw e;
        //        }
        //    }
        //}

        //public static void DisableConstraintsForDatabase(IDbConnection connection, IDbTransaction transaction = null)
        //{
        //    using (IDbCommand command = connection.CreateCommand())
        //    {
        //        if (transaction != null)
        //        {
        //            command.Transaction = transaction;
        //        }

        //        command.CommandText = String.Format("EXEC sp_MSforeachtable @command1='ALTER TABLE ? NOCHECK CONSTRAINT ALL';");

        //        try
        //        {
        //            command.ExecuteNonQuery();

        //            log.InfoFormat("Constraints disabled for database");
        //        }
        //        catch (Exception e)
        //        {
        //            log.ErrorFormat("Failed to disable constraints for database.  Message:{0}", e.Message);
        //            throw e;
        //        }
        //    }
        //}

        //public static void EnableConstraintsForDatabase(IDbConnection connection, IDbTransaction transaction = null)
        //{
        //    using (IDbCommand command = connection.CreateCommand())
        //    {
        //        if (transaction != null)
        //        {
        //            command.Transaction = transaction;
        //        }

        //        command.CommandText = String.Format("EXEC sp_MSforeachtable @command1='ALTER TABLE ? CHECK CONSTRAINT ALL';");

        //        try
        //        {
        //            command.ExecuteNonQuery();

        //            log.InfoFormat("Constraints enabled for database");
        //        }
        //        catch (Exception e)
        //        {
        //            log.ErrorFormat("Failed to enable constraints for database.  Message:{0}", e.Message);
        //            throw e;
        //        }
        //    }
        //}

        #endregion

        // ******************************************************************************
        #region Misc Methods
        // ******************************************************************************

        //public static string GetParameterPrefix(IDbConnection connection)
        //{
        //    string parameterPrefix = DEFAULT_PARAMETER_PREFIX;
        //    string connectionType = connection.GetType().Name;

        //    if (connection is System.Data.OleDb.OleDbConnection)
        //    {
        //        if (connection.ConnectionString.ToLower().Contains("msdora"))
        //        {
        //            parameterPrefix = ":";
        //        }
        //    }
        //    else if (connection is System.Data.OracleClient.OracleConnection)
        //    {
        //        parameterPrefix = ":";
        //    }
        //    else
        //    {
        //        parameterPrefix = DEFAULT_PARAMETER_PREFIX;
        //    }

        //    return parameterPrefix;
        //}

        //public static DbType GetDbType(string columnName, string dataType)
        //{
        //    dataType = dataType.Trim().ToLower();
        //    dataType = dataType.Replace("system.", "");

        //    if (dataType == "string" || dataType == "varchar" || dataType == "nvarchar")
        //    {
        //        return DbType.String;
        //    }

        //    else if (dataType == "int16")
        //    {
        //        return DbType.Int16;
        //    }

        //    else if (dataType == "int" || dataType == "int32")
        //    {
        //        return DbType.Int32;
        //    }

        //    else if (dataType == "int64")
        //    {
        //        return DbType.Int64;
        //    }

        //    else if (dataType == "bool" || dataType == "boolean")
        //    {
        //        return DbType.Boolean;
        //    }

        //    else if (dataType == "date" || dataType == "datetime")
        //    {
        //        return DbType.DateTime;
        //    }

        //    else if (dataType == "double")
        //    {
        //        return DbType.Double;
        //    }

        //    else if (dataType == "decimal")
        //    {
        //        return DbType.Decimal;
        //    }

        //    else if (dataType == "guid")
        //    {
        //        return DbType.Guid;
        //    }

        //    else
        //    {
        //        string message = String.Format("Invalid data type: {0} for Column specified.  Column Name: {1}", dataType, columnName);
        //        throw new Exception(message);
        //    }
        //}

        //public static DataColumn CreateDataColumn(string columnName, string dataType)
        //{
        //    dataType = dataType.Trim().ToLower();
        //    dataType = dataType.Replace("system.", "");

        //    DataColumn bulkColumn = new DataColumn();
        //    bulkColumn.ColumnName = columnName;

        //    if (dataType == "string" || dataType == "varchar" || dataType == "nvarchar")
        //    {
        //        bulkColumn.DataType = typeof(string);
        //    }

        //    else if (dataType == "int16")
        //    {
        //        bulkColumn.DataType = typeof(Int16);
        //    }

        //    else if (dataType == "int" || dataType == "int32")
        //    {
        //        bulkColumn.DataType = typeof(Int32);
        //    }

        //    else if (dataType == "int64")
        //    {
        //        bulkColumn.DataType = typeof(Int64);
        //    }

        //    else if (dataType == "bool" || dataType == "boolean")
        //    {
        //        bulkColumn.DataType = typeof(bool);
        //    }

        //    else if (dataType == "date" || dataType == "datetime")
        //    {
        //        bulkColumn.DataType = typeof(DateTime);
        //    }

        //    else if (dataType == "double")
        //    {
        //        bulkColumn.DataType = typeof(double);
        //    }

        //    else if (dataType == "decimal")
        //    {
        //        bulkColumn.DataType = typeof(decimal);
        //    }

        //    else if (dataType == "guid")
        //    {
        //        bulkColumn.DataType = typeof(Guid);
        //    }

        //    else
        //    {
        //        string message = String.Format("Invalid data type: {0} for Column specified.  Column Name: {1}", dataType, columnName);
        //        throw new Exception(message);
        //    }

        //    return bulkColumn;
        //}

        #endregion

        // ******************************************************************************
        #region Execute SQL Methods
        // ******************************************************************************

        public static void ExecuteSqlScriptFile(IDbConnection connection, string filePath)
        {
            log.DebugFormat("ExecuteSqlScript called for SQL Script: {0}", filePath);

            try
            {
                string sqlScript = FileTools.LoadStringFromFile(filePath);
                ExecuteSqlBatch(connection, sqlScript);
            }
            catch (Exception e)
            {
                string message = string.Format("Exception occurred executing SQL Script: {0}", filePath);
                log.Error(message);

                throw new Exception(message, e);
            }
        }

        public static string ExecuteSqlBatch(IDbConnection connection, string sqlBatch)
        {
            StringBuilder sb = new StringBuilder();

            List<string> sqlStatements = GetSqlStatements(sqlBatch);

            foreach (string sqlStatement in sqlStatements)
            {
                string results = ExecuteSqlStatement(connection, sqlStatement);
                log.Info(results);
                sb.Append(results);
            }

            return sb.ToString();
        }

        private static List<string> GetSqlStatements(string sqlBatch)
        {
            List<string> sqlStatements = new List<string>();

            string[] separator = new string[] { "\r\n", "\r" };
            string[] lines = sqlBatch.Split(separator, StringSplitOptions.None);

            StringDelimiter sqlStatement = new StringDelimiter(Environment.NewLine);
            foreach (string line in lines)
            {
                if (line.Trim().ToUpper() == "GO")
                {
                    sqlStatements.Add(sqlStatement.ToString());
                    sqlStatement.Clear();
                    continue;
                }

                sqlStatement.Add(line);
            }

            if (!StringTools.IsBlankOrNull(sqlStatement.ToString()))
            {
                sqlStatements.Add(sqlStatement.ToString());
            }

            return sqlStatements;
        }

        public static string ExecuteSqlStatement(IDbConnection connection, string sql)
        {
            StringDelimiter sd = new StringDelimiter(Environment.NewLine);

            try
            {
                int numRowsAffected = ExecuteNonQuery(connection, sql);
                sd.Add("SQL:");
                sd.Add(sql);
                sd.Add(string.Empty);
                sd.Add("Result: ({0}) rows affected.", numRowsAffected);
            }
            catch (Exception e)
            {
                log.Error("Exception occurred in ExecuteSql: ", e);

                sd.Add("SQL:");
                sd.Add(sql);
                sd.Add(string.Empty);
                sd.Add("Result: ", e.Message);

                if (e.InnerException != null)
                {
                    sd.Add(e.InnerException.Message);
                }
            }

            sd.Add(string.Empty);
            sd.Add("-----------------------------------------");
            sd.Add(string.Empty);

            return sd.ToString();

        }

        private static int ExecuteNonQuery(IDbConnection connection, string sql)
        {
            QueryDef qd = new QueryDef(sql);
            qd.CommandTimeout = 0;
            return QueryEngine.ExecuteNonQuery(connection, qd);
        }

        #endregion
    }
}
