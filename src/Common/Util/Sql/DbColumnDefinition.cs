﻿using System;

namespace Util.Sql
{
    public class DbColumnDefinition
    {
        public string ColumnName { get; set; }
        public int ColumnOrdinal { get; set; }
        public int ColumnSize { get; set; }
        public int NumericPrecision { get; set; }
        public int NumericScale { get; set; }
        public Type DotNetDataType { get; set; } // Example: System.String
        public int ProviderType{ get; set; }
        public bool AllowDbNull { get; set; }
        public bool IsUnique { get; set; }
        public bool IsReadOnly { get; set; }
        public bool IsRowVersion { get; set; }
        public bool IsLong { get; set; }
    }
}
