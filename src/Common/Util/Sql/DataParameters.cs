using System.Collections;
using System.Data;
using System.Text;
using Util.Collections.Generic;
using System.Collections.Generic;

namespace Util.Sql
{
    /// <summary>
    /// Class to hold Data Parameters
    /// </summary>
    public class DataParameter
    {
        public string ParameterName;
        public DbType DbType;
        public object Value;
        public ParameterDirection Direction = ParameterDirection.Input;
        public int Size;

        public DataParameter() { }

        public DataParameter(string parameterName, DbType dbType, object value)
        {
            this.ParameterName = parameterName;
            this.DbType = dbType;
            this.Value = value;
        }

        public DataParameter(string parameterName, DbType dbType, ParameterDirection direction)
        {
            this.ParameterName = parameterName;
            this.DbType = dbType;
            this.Direction = direction;
        }

        public DataParameter(string parameterName, DbType dbType, ParameterDirection direction, int size)
            : this(parameterName, dbType, direction)
        {
            this.Size = size;
        }

        public DataParameter(string parameterName, DbType dbType, ParameterDirection direction, int size, object value)
            : this(parameterName, dbType, direction, size)
        {
            this.Value = value;
        }
    }

    /// <summary>
    /// Helper class to Hold Data Parameters
    /// </summary>
    public class DataParameters : IEnumerable
    {
        // Default Constructor
        public DataParameters() { }

        public DataParameters(List<DataParameter> dataParameters)
        {
            Guard.ArgumentNotNull(dataParameters, "dataParameters");

            foreach (DataParameter dp in dataParameters)
            {
                this.Add(dp);
            }
        }

        // *************************************************
        // Instance Variables
        // *************************************************

        // Contains the DataParameters
        private ArrayList parameters = new ArrayList();

        // Provides item lookup ability on parameter
        private CaseInsensitiveDictionary<DataParameter> parametersLookup = new CaseInsensitiveDictionary<DataParameter>();

        // ******************************************************************************
        // Core Methods
        // ******************************************************************************

        public virtual DataParameter this[string key]
        {
            get
            {
                return parametersLookup[key];
            }
        }

        /// <summary>
        /// Adds a Parameter to the Parameters collection
        /// </summary>
        /// <param name="name">Parameter Name</param>
        /// <param name="dbType">Parameter Data Type</param>
        /// <param name="originalValue">Parameter Value</param>
        public void Add(string parameterName, DbType dbType, object value)
        {
            DataParameter dp = new DataParameter(parameterName, dbType, value);
            parameters.Add(dp);
            parametersLookup.Add(parameterName, dp);
        }

        /// <summary>
        /// Adds a Parameter to the Parameters collection
        /// </summary>
        /// <param name="name">Parameter Name</param>
        /// <param name="dbType">Parameter Data Type</param>
        /// <param name="direction">Parameter Direction</param>
        public void Add(string parameterName, DbType dbType, ParameterDirection direction)
        {
            DataParameter dp = new DataParameter(parameterName, dbType, direction);
            parameters.Add(dp);
            parametersLookup.Add(parameterName, dp);
        }

        /// <summary>
        /// Adds a Parameter to the Parameters collection
        /// </summary>
        /// <param name="name">Parameter Name</param>
        /// <param name="dbType">Parameter Data Type</param>
        /// <param name="direction">Parameter Direction</param>
        /// <param name="size">Parameter Size</param>
        public void Add(string parameterName, DbType dbType, ParameterDirection direction, int size)
        {
            DataParameter dp = new DataParameter(parameterName, dbType, direction, size);
            parameters.Add(dp);
            parametersLookup.Add(parameterName, dp);
        }

        /// <summary>
        /// Adds a Parameter to the Parameters collection
        /// </summary>
        /// <param name="name">Parameter Name</param>
        /// <param name="dbType">Parameter Data Type</param>
        /// <param name="direction">Parameter Direction</param>
        /// <param name="size">Parameter Size</param>
        /// <param name="originalValue">Parameter Value</param>
        public void Add(string parameterName, DbType dbType, ParameterDirection direction, int size, object value)
        {
            DataParameter dp = new DataParameter(parameterName, dbType, direction, size, value);
            parameters.Add(dp);
            parametersLookup.Add(parameterName, dp);
        }

        /// <summary>
        /// Adds a Parameter to the Parameters collection
        /// </summary>
        public void Add(DataParameter dp)
        {
            parameters.Add(dp);
            parametersLookup.Add(dp.ParameterName, dp);
        }

        /// <summary>
        /// Prints a String representation of all of the parameters - 
        /// suitable for use as part of the Cache Key
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            foreach (DataParameter dp in parameters)
            {
                if (dp.DbType == DbType.Binary)
                {
                    sb.AppendFormat("{0}: {1}\n", dp.ParameterName, "binary value");
                }
                else
                {
                    string value = "";
                    if (dp.Value != null)
                    {
                        value = dp.Value.ToString();

                        if (value.Length > 100)
                        {
                            value = value.Substring(0, 99) + "...";
                        }
                    }

                    sb.AppendFormat("{0}: {1}\n", dp.ParameterName, value);
                }
            }
            return sb.ToString();
        }

        // ******************************************************************************
        // IEnumerable / IEnumerator Methods
        // ******************************************************************************

        /* Needed since Implementing IEnumerable*/
        public IEnumerator GetEnumerator()
        {
            foreach (DataParameter dp in parameters)
            {
                yield return dp;
            }
        }

    }
}
