using System;
using System.Collections.Generic;
using System.Data;
using Util.Data;

namespace Util.Sql.Query
{
    public static partial class QueryEngine
    {
        internal class ColumnDefTranslation
        {
            internal string ColumnName;
            internal int SourceColumnIndex;
            internal int DataReaderColumnIndex;
            internal Type DataType;
        }

        public static DataList GetDataList(IDbConnection connection, QueryDef qd)
        {
            return GetDataList(connection, qd, null, null);
        }

        public static DataList GetDataList(IDbConnection connection, QueryDef qd, DataParameters dataParameters)
        {
            return GetDataList(connection, qd, dataParameters, null);
        }

        public static DataList GetDataList(IDbConnection connection, QueryDef qd, DataParameters dataParameters, DataListColumns columns)
        {
            return (DataList)GetDataFromDataReader(connection, qd, dataParameters, GetDataFromDataReaderOption.DataList, columns);
        }

        public static DataEntity GetDataEntity(IDbConnection connection, QueryDef qd)
        {
            return GetDataEntity(connection, qd, null, null);
        }

        public static DataEntity GetDataEntity(IDbConnection connection, QueryDef qd, DataParameters dataParameters)
        {
            return GetDataEntity(connection, qd, dataParameters, null);
        }

        public static DataEntity GetDataEntity(IDbConnection connection, QueryDef qd, DataParameters dataParameters, DataListColumns columns)
        {
            return (DataEntity)GetDataFromDataReader(connection, qd, dataParameters, GetDataFromDataReaderOption.DataEntity, columns);
        }

        // ******************************************************************************
        // Static ExecuteScalar (and related) Methods
        // ******************************************************************************

        /// <summary>
        /// Executes a query and returns the first column of the first row in the resultset returned by the query.
        /// Extra columns or rows are ignored.
        /// </summary>
        /// <param name="connection">DB Connection</param>
        /// <param name="qd">Query Definition</param>
        /// <returns>Object</returns>
        public static DataScalar GetDataScalar(IDbConnection connection, QueryDef qd)
        {
            return GetDataScalar(connection, qd, null, null);
        }

        /// <summary>
        /// Executes a query and returns the first column of the first row in the resultset returned by the query.
        /// Extra columns or rows are ignored.
        /// </summary>
        /// <param name="connection">DB Connection</param>
        /// <param name="qd">Query Definition</param>
        /// <param name="dp">Data Parameters (can be null)</param>
        /// <returns>Object</returns>
        public static DataScalar GetDataScalar(IDbConnection connection, QueryDef qd, DataParameters dataParameters)
        {
            return GetDataScalar(connection, qd, dataParameters, null);
        }

        public static DataScalar GetDataScalar(IDbConnection connection, QueryDef qd, DataParameters dataParameters, DataListColumns columns)
        {
            return (DataScalar)GetDataFromDataReader(connection, qd, dataParameters, GetDataFromDataReaderOption.DataScalar, columns);
        }

        private static DataList DoGetDataFromDataReader_DataList(IDbConnection connection, QueryDef qd, DataParameters dataParameters, DataListColumns columns)
        {
            return DoGetDataList(connection, qd, dataParameters, columns, false);
        }

        private static DataEntity DoGetDataFromDataReader_DataEntity(IDbConnection connection, QueryDef qd, DataParameters dataParameters, DataListColumns columns)
        {
            DataList list = DoGetDataList(connection, qd, dataParameters, columns, true);

            if (list.Rows.Count == 0)
            {
                return null;
            }

            return new DataEntity(list);
        }

        private static DataScalar DoGetDataFromDataReader_DataScalar(IDbConnection connection, QueryDef qd, DataParameters dataParameters, DataListColumns columns)
        {
            DataEntity entity = DoGetDataFromDataReader_DataEntity(connection, qd, dataParameters, columns);

            if (entity == null || entity.Columns.Count == 0)
            {
                return null;
            }

            return new DataScalar(entity);
        }

        private static DataList DoGetDataList(IDbConnection connection, QueryDef qd, DataParameters dataParameters, DataListColumns columns, bool singleRow)
        {
            DataList dataList;

            using (DataReaderAndCommand drac = QueryEngine.ExecuteDataReader(connection, qd, dataParameters, true))
            {
                IDataReader dataReader = drac.DataReader;

                DataListColumns dbColumns = GetColumnDefinitionFromDataReader(dataReader);

                if (columns == null)
                {
                    columns = dbColumns;
                }

                dataList = new DataList(columns);

                List<ColumnDefTranslation> columnDefTranslations = CreateColumnDefTranslations(columns, dbColumns);

                while (dataReader.Read())
                {
                    DataListRow row = dataList.Rows.Add();

                    foreach (ColumnDefTranslation colTran in columnDefTranslations)
                    {
                        int dbColumnIndex = colTran.DataReaderColumnIndex;
                        int columnIndex = colTran.SourceColumnIndex;

                        if (dataReader.IsDBNull(dbColumnIndex))
                        {
                            row[columnIndex] = null;
                        }
                        else
                        {
                            row[columnIndex] = GetDataReaderValue(dataReader, dbColumnIndex, colTran.DataType);
                        }
                    }

                    if (singleRow) { break; }
                }
            }

            return dataList;
        }

        private static DataListColumns GetColumnDefinitionFromDataReader(IDataReader dataReader)
        {
            int columnCount = dataReader.FieldCount;
            DataListColumns columns = new DataListColumns();

            for (int c = 0; c <= columnCount - 1; c++)
            {
                DataListColumn column = new DataListColumn(dataReader.GetName(c), dataReader.GetFieldType(c));
                columns.Add(column);
            }

            return columns;
        }

        private static List<ColumnDefTranslation> CreateColumnDefTranslations(DataListColumns columns, DataListColumns dbColumns)
        {
            List<ColumnDefTranslation> translationList = new List<ColumnDefTranslation>();

            if (columns == dbColumns)
            {
                foreach (DataListColumn column in columns)
                {
                    ColumnDefTranslation colTran = new ColumnDefTranslation();
                    colTran.ColumnName = column.ColumnName;
                    colTran.SourceColumnIndex = column.ColumnIndex;
                    colTran.DataReaderColumnIndex = column.ColumnIndex;
                    colTran.DataType = column.DataType;

                    translationList.Add(colTran);
                }
            }
            else
            {
                foreach (DataListColumn column in columns)
                {
                    DataListColumn dbColumn = dbColumns[column.ColumnName];

                    ColumnDefTranslation colTran = new ColumnDefTranslation();
                    colTran.ColumnName = column.ColumnName;
                    colTran.SourceColumnIndex = column.ColumnIndex;
                    colTran.DataReaderColumnIndex = dbColumn.ColumnIndex;
                    colTran.DataType = dbColumn.DataType;

                    translationList.Add(colTran);
                }
            }

            return translationList;
        }

        private static object GetDataReaderValue(IDataReader dataReader, int dbColumnIndex, Type type)
        {
            if (type == typeof(string)) { return dataReader.GetString(dbColumnIndex); }
            else if (type == typeof(Int32)) { return dataReader.GetInt32(dbColumnIndex); }
            else if (type == typeof(DateTime)) { return dataReader.GetDateTime(dbColumnIndex); }
            else if (type == typeof(Boolean)) { return dataReader.GetBoolean(dbColumnIndex); }
            else if (type == typeof(Decimal)) { return dataReader.GetDecimal(dbColumnIndex); }
            else { return dataReader.GetValue(dbColumnIndex); }
        }
    }
}
