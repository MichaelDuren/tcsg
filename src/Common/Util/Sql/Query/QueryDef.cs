using System.Data;


namespace Util.Sql.Query
{
    /// <summary>
    /// Enumerator specifying what values to use for Caching
    /// </summary>
    public enum CacheOption
    {
        Short,
        Long,
        None
    }

    /// <summary>
    /// Defines a Query to be passed to the DataReaderHelper
    /// </summary>
    public class QueryDef
    {
        // ********************************************************************************
        #region Variables and Properties
        // ********************************************************************************

        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Contains the Select statement to execute.
        /// </summary>
        public string Sql { get; set; }

        /// <summary>
        /// Defines the Query Timeout value (in seconds) to use for a query.  Note: 0 means No Timeout.
        /// </summary>
        public int CommandTimeout { get; set; }

        private CommandType _commandType = CommandType.Text;
        /// <summary>
        /// Defines the Command Type for the query (Default is CommandType.Text
        /// </summary>
        public CommandType CommandType
        {
            get { return _commandType; }
            set { _commandType = value; }
        }

        // ****************
        // Cache values
        // ****************

        /// <summary>
        /// Specifies if the query should be cached at all
        /// </summary>
        public bool CacheQuery { get; private set; }

        /// <summary>
        /// Specifies how many minutes before this cache item times out 
        /// (ignores last accessed)
        /// If BOTH Absolute and Sliding Expiration is set to 0 - then this will NOT CACHE 
        /// </summary>
        public int CacheAbsoluteExpirationMinutes { get; private set; }

        /// <summary>
        /// Specifies how many minutes before this cache item times out.  This is set by the CacheOption value constructor
        /// (restart timer when accessed)
        /// If BOTH Absolute and Sliding Expiration is set to 0 - then this will NOT CACHE
        /// </summary>
        public int CacheSlidingExpirationMinutes { get; private set; }

        #endregion

        // ********************************************************************************
        #region Constructors
        // ********************************************************************************

        public QueryDef()
        {
            CommandTimeout = -1;
        }

        public QueryDef(string sql)
            : this()
        {
            this.Sql = sql;
        }

        public QueryDef(CacheOption cacheOption, string sql)
            : this(sql)
        {
            HandleCacheOption(cacheOption);
        }

        public QueryDef(int absoluteExpirationMinutes, string sql)
            : this(sql)
        {
            this.CacheQuery = true;
            this.CacheAbsoluteExpirationMinutes = absoluteExpirationMinutes;
        }

        public QueryDef(CommandType commandType, string sql)
            : this(sql)
        {
            this.CommandType = commandType;
        }

        public QueryDef(CommandType commandType, CacheOption cacheOption, string sql)
            : this(cacheOption, sql)
        {
            this.CommandType = commandType;
        }

        #endregion

        // ********************************************************************************
        #region Methods
        // ********************************************************************************

        private void HandleCacheOption(CacheOption cacheOption)
        {
            if (cacheOption == CacheOption.Long)
            {
                CacheQuery = true;
                this.CacheSlidingExpirationMinutes = QueryEngineConfigValues.LongCacheTimeout;
            }
            else if (cacheOption == CacheOption.Short)
            {
                CacheQuery = true;
                this.CacheSlidingExpirationMinutes = QueryEngineConfigValues.ShortCacheTimeout;
            }
            else if (cacheOption == CacheOption.None)
            {
                // Do Nothing - this says to NOT cache
            }
        }

        #endregion
    }
}
