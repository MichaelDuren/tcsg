using System;
using System.Data;

namespace Util.Sql.Query
{
    public static partial class QueryEngine
    {
        // ******************************************************************************
        // Static GetDataSet Methods
        // ******************************************************************************

        /// <summary>
        /// Takes a Query definition and returns the results from the Database (or cache).
        /// </summary>
        /// <param name="connection">Database Connection</param>
        /// <param name="qd">Query Definition</param>
        /// <returns>DataReader</returns>
        public static DataSet GetDataSet(
            IDbConnection connection,
            QueryDef qd)
        {
            return ExecuteDataSet(connection, qd, null);
        }

        /// <summary>
        /// Takes a Query definition and returns the results from the Database (or cache).
        /// </summary>
        /// <param name="connection">Database Connection</param>
        /// <param name="qd">Query Definition</param>
        /// <param name="dp">Data Parameters (can be null)</param>
        /// <returns>DataReader</returns>
        public static DataSet GetDataSet(
            IDbConnection connection,
            QueryDef qd,
            DataParameters dataParameters)
        {
            return ExecuteDataSet(connection, qd, dataParameters);
        }

        // ******************************************************************************
        // DataSet Support Methods
        // ******************************************************************************

        private static DataSet ExecuteDataSet(
            IDbConnection connection,
            QueryDef qd,
            DataParameters dataParameters
            )
        {
            // Make sure that data paramaters contains a valid DataParamaters object
            if (dataParameters == null)
            {
                dataParameters = new DataParameters();
            }

            DateTime startTime = DateTime.Now;

            DataSet ds = null;

            // ********************************************
            // Create the Cache Key
            // ********************************************

            string cacheKey = String.Format(
                "GetDataSet: {0}\nParms:\n{1}",
                qd.Sql,
                dataParameters.ToString());


            // ********************************************
            // Process Cache stuff if we are caching
            // ********************************************

            int requestId = requestCount++;
            if (includeSqlInLog)
            {
                log.Info(String.Format("DataSet Query Request {0}:\n{1}", requestId, cacheKey));
            }
            else
            {
                log.Info(String.Format("DataSet Query Request {0}", requestId));
            }

            if (qd.CacheQuery)
            {

                // Attempt to get the results from the cache
                ds = (DataSet)Cache.Get(cacheKey);


                // If the cache didn't have it, get it from the DB
                if (ds == null)
                {
                    ds = DoExecuteDataSet(connection, qd, dataParameters);
                    log.Info(String.Format("Cache MISS - Read from DB (req #: {0}) (Time: {1} ms)",
                        requestId,
                        GetElapsedMilliseconds(startTime)));

                    // Cache the Results
                    Cache.Add(
                        cacheKey,
                        ds,
                        qd.CacheAbsoluteExpirationMinutes,
                        qd.CacheSlidingExpirationMinutes);
                }
                else
                {
                    log.Info(String.Format("Cache HIT (req #: {0})  (Time: {1} ms)",
                        requestId,
                        GetElapsedMilliseconds(startTime)));
                }
            }
            else
            {
                // Get it from the DB
                ds = DoExecuteDataSet(connection, qd, dataParameters);
                log.Info(String.Format("Read from DB (req #: {0}) (Time: {1} ms)",
                    requestId,
                    GetElapsedMilliseconds(startTime)));
            }

            return ds;
        }

        private static DataSet DoExecuteDataSet(
            IDbConnection connection,
            QueryDef qd,
            DataParameters dataParameters)
        {
            DataSet ds;

            log.Debug("Start of DoExecuteDataSet");

            try
            {
                // Create the command
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = qd.Sql;

                    // See if a CommandTimeout was specified
                    if (qd.CommandTimeout != -1)
                    {
                        command.CommandTimeout = qd.CommandTimeout;
                    }

                    if (qd.CommandType == CommandType.StoredProcedure)
                    {
                        command.CommandType = CommandType.StoredProcedure;
                    }

                    DbTools.AddParametersToCommand(command, dataParameters);

                    IDbDataAdapter da = DataAdapterFactory.CreateDataAdapter(command);
                    ds = new DataSet();
                    da.Fill(ds);
                }
            }
            catch (Exception e)
            {
                string message = string.Format(
                    "Exception occurred Executing Query, details follow...\nMessage: {0}\nSQL: {1}\n",
                    e.Message,
                    qd.Sql);
                log.Error(message + "\n---> StackTrace:\n" + e.StackTrace);
                throw new Exception(message, e);
            }

            log.Debug("Completed DoExecuteDataSet");

            return ds;
        }
    }
}
