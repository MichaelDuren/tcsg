using System;
using System.Collections;
using System.Data;
using Util.Collections;
using Util.Data;
using System.Collections.Generic;

namespace Util.Sql.Query
{
    public static partial class QueryEngine
    {
        private enum GetDataFromDataReaderOption
        {
            SingleRow,
            MultipleRows,
            Scalar,

            DataEntity,
            DataList,
            DataScalar
        }

        // ******************************************************************************
        // Static ExecuteScalar (and related) Methods
        // ******************************************************************************

        /// <summary>
        /// Executes a query and returns the first column of the first row in the resultset returned by the query.
        /// Extra columns or rows are ignored.
        /// </summary>
        /// <param name="connection">DB Connection</param>
        /// <param name="qd">Query Definition</param>
        /// <returns>Object</returns>
        public static object GetScalar(
            IDbConnection connection,
            QueryDef qd)
        {
            return GetDataFromDataReader(connection, qd, null, GetDataFromDataReaderOption.Scalar);
        }

        /// <summary>
        /// Executes a query and returns the first column of the first row in the resultset returned by the query.
        /// Extra columns or rows are ignored.
        /// </summary>
        /// <param name="connection">DB Connection</param>
        /// <param name="qd">Query Definition</param>
        /// <param name="dp">Data Parameters (can be null)</param>
        /// <returns>Object</returns>
        public static object GetScalar(
            IDbConnection connection,
            QueryDef qd,
            DataParameters dataParameters)
        {
            return GetDataFromDataReader(connection, qd, dataParameters, GetDataFromDataReaderOption.Scalar);
        }

        /// <summary>
        /// Simplified routine to get a scalar originalValue from a table
        /// </summary>
        /// <param name="connection">DB Connection</param>
        /// <param name="cacheOption"></param>
        /// <param name="tableName"></param>
        /// <param name="getFieldName">Name of Field to return</param>
        /// <param name="whereClause">WHERE clause to use in Select... NOTE: do NOT specify the "WHERE" statement.</param>
        /// <returns>int</returns>
        public static object GetScalar(IDbConnection connection, CacheOption cacheOption, string tableName, string getFieldName, string whereClause)
        {
            // Create the Query Definition
            QueryDef qd = new QueryDef(
                cacheOption,
                string.Format(@"SELECT {0} FROM {1} WHERE {2}", getFieldName, tableName, whereClause));

            // Get the data from the DB
            return QueryEngine.GetScalar(connection, qd);
        }

        /// <summary>
        /// Executes a query and returns the first column of the first row in the resultset returned by the query as a String
        /// Extra columns or rows are ignored.
        /// </summary>
        /// <param name="connection">DB Connection</param>
        /// <param name="qd">Query Definition</param>
        /// <returns>String</returns>
        public static String GetString(
            IDbConnection connection,
            QueryDef qd)
        {
            return GetString(connection, qd, null);
        }

        /// <summary>
        /// Executes a query and returns the first column of the first row in the resultset returned by the query as a String
        /// Extra columns or rows are ignored.
        /// </summary>
        /// <param name="connection">DB Connection</param>
        /// <param name="qd">Query Definition</param>
        /// <param name="dp">Data Parameters (can be null)</param>
        /// <returns>String</returns>
        public static String GetString(
            IDbConnection connection,
            QueryDef qd,
            DataParameters dataParameters)
        {
            object o = GetDataFromDataReader(connection, qd, dataParameters, GetDataFromDataReaderOption.Scalar);
            return o as string;
        }

        /// <summary>
        /// Simplified routine to get a string from a table
        /// </summary>
        /// <param name="connection">DB Connection</param>
        /// <param name="cacheOption"></param>
        /// <param name="tableName"></param>
        /// <param name="getFieldName">Name of Field to return</param>
        /// <param name="whereClause">WHERE clause to use in Select... NOTE: do NOT specify the "WHERE" statement.</param>
        /// <returns>String</returns>
        public static string GetString(IDbConnection connection, CacheOption cacheOption, string tableName, string getFieldName, string whereClause)
        {
            // Create the Query Definition
            QueryDef qd = new QueryDef(
                cacheOption,
                string.Format(@"SELECT {0} FROM {1} WHERE {2}", getFieldName, tableName, whereClause));

            // Get the data from the DB
            return QueryEngine.GetString(connection, qd);
        }

        /// <summary>
        /// Executes a query and returns the first column of the first row in the resultset returned by the query as an integer
        /// Extra columns or rows are ignored.
        /// </summary>
        /// <param name="connection">DB Connection</param>
        /// <param name="qd">Query Definition</param>
        /// <returns>int</returns>
        public static int GetInteger(
            IDbConnection connection,
            QueryDef qd)
        {
            return GetInteger(connection, qd, null);
        }

        /// <summary>
        /// Executes a query and returns the first column of the first row in the resultset returned by the query as an integer
        /// Extra columns or rows are ignored.
        /// </summary>
        /// <param name="connection">DB Connection</param>
        /// <param name="qd">Query Definition</param>
        /// <param name="dp">Data Parameters (can be null)</param>
        /// <returns>int</returns>
        public static int GetInteger(
            IDbConnection connection,
            QueryDef qd,
            DataParameters dataParameters)
        {
            object o = GetDataFromDataReader(connection, qd, dataParameters, GetDataFromDataReaderOption.Scalar);

            // See if we got one - if NOT - then it was not found
            if (o == null)
            {
                return 0;
            }
            else if (o is Int16 || o is Int32 || o is Int64)
            {
                return (int)o;
            }
            else
            {
                return Int32.Parse(o.ToString());
            }
        }

        /// <summary>
        /// Simplified routine to get an integer from a table
        /// </summary>
        /// <param name="connection">DB Connection</param>
        /// <param name="cacheOption"></param>
        /// <param name="tableName"></param>
        /// <param name="getFieldName">Name of Field to return</param>
        /// <param name="whereClause">WHERE clause to use in Select... NOTE: do NOT specify the "WHERE" statement.</param>
        /// <returns>int</returns>
        public static int GetInteger(IDbConnection connection, CacheOption cacheOption, string tableName, string getFieldName, string whereClause)
        {
            // Create the Query Definition
            QueryDef qd = new QueryDef(
                cacheOption,
                string.Format(@"SELECT {0} FROM {1} WHERE {2}", getFieldName, tableName, whereClause));

            // Get the data from the DB
            return QueryEngine.GetInteger(connection, qd);
        }

        // ******************************************************************************
        // Static GetSingleRow Methods
        // ******************************************************************************

        /// <summary>
        /// Method to return a Single Row for a query (as a CaseInsensitiveHashtable).
        /// Takes a Query definition and returns a Single Row from the Database
        /// Returns null if not found
        /// Supports Caching - if defined in the Query
        /// </summary>
        /// <param name="connection">DB Connection</param>
        /// <param name="qd">Query Definition</param>
        /// <returns>CaseInsensitiveHashtable of Columns in the Row - null if not found</returns>
        public static CaseInsensitiveHashtable GetSingleRow(
            IDbConnection connection,
            QueryDef qd)
        {
            return (CaseInsensitiveHashtable)GetDataFromDataReader(connection, qd, null, GetDataFromDataReaderOption.SingleRow);
        }

        /// <summary>
        /// Method to return a Single Row for a query (as a CaseInsensitiveHashtable).
        /// Takes a Query definition and returns a Single Row from the Database
        /// Returns null if not found
        /// Supports Caching - if defined in the Query
        /// </summary>
        /// <param name="connection">DB Connection</param>
        /// <param name="qd">Query Definition</param>
        /// <param name="dp">Data Parameters (can be null)</param>
        /// <returns>CaseInsensitiveHashtable of Columns in the Row - null if not found</returns>
        public static CaseInsensitiveHashtable GetSingleRow(
            IDbConnection connection,
            QueryDef qd,
            DataParameters dataParameters)
        {
            return (CaseInsensitiveHashtable)GetDataFromDataReader(connection, qd, dataParameters, GetDataFromDataReaderOption.SingleRow);
        }

        // ******************************************************************************
        // Static GetMultipleRows Methods
        // ******************************************************************************

        /// <summary>
        /// Method to return Multiple Rows for a query (as an ArrayList of CaseInsensitiveHashtables).
        /// Takes a Query definition and returns a Multiple Rows from the Database
        /// Returns null if not found
        /// Supports Caching - if defined in the Query
        /// </summary>
        /// <param name="connection">DB Connection</param>
        /// <param name="qd">Query Definition</param>
        /// <returns>List of CaseInsensitiveHashtables of Columns in each Row - null if not found</returns>
        public static List<CaseInsensitiveHashtable> GetMultipleRows(
            IDbConnection connection,
            QueryDef qd)
        {
            return (List<CaseInsensitiveHashtable>)GetDataFromDataReader(connection, qd, null, GetDataFromDataReaderOption.MultipleRows);
        }

        /// <summary>
        /// Method to return Multiple Rows for a query (as an ArrayList of CaseInsensitiveHashtables).
        /// Takes a Query definition and returns a Multiple Rows from the Database
        /// Returns null if not found
        /// Supports Caching - if defined in the Query
        /// </summary>
        /// <param name="connection">DB Connection</param>
        /// <param name="qd">Query Definition</param>
        /// <param name="dp">Data Parameters (can be null)</param>
        /// <returns>List of CaseInsensitiveHashtables of Columns in each Row - null if not found</returns>
        public static List<CaseInsensitiveHashtable> GetMultipleRows(
            IDbConnection connection,
            QueryDef qd,
            DataParameters dataParameters)
        {
            return (List<CaseInsensitiveHashtable>)GetDataFromDataReader(connection, qd, dataParameters, GetDataFromDataReaderOption.MultipleRows);
        }

        /// <summary>
        /// Simplified routine to get a list of strings from a table
        /// </summary>
        public static List<string> GetStringList(
            IDbConnection connection,
            QueryDef qd,
            string getFieldName,
            DataParameters dataParameters = null)
        {
            List<CaseInsensitiveHashtable> rows = QueryEngine.GetMultipleRows(connection, qd, dataParameters);
            List<string> returnList = new List<string>();

            if (rows != null)
            {
                foreach (CaseInsensitiveHashtable row in rows)
                {
                    returnList.Add(row[getFieldName] as string);
                }
            }

            return returnList;
        }

        // ******************************************************************************
        // GetScalar / GetSingleRow / GetMultipleRows Support Methods
        // ******************************************************************************

        private static object GetDataFromDataReader(IDbConnection connection, QueryDef qd, DataParameters dataParameters, GetDataFromDataReaderOption option)
        {
            return GetDataFromDataReader(connection, qd, dataParameters, option, null);
        }

        private static object GetDataFromDataReader(
            IDbConnection connection,
            QueryDef qd,
            DataParameters dataParameters,
            GetDataFromDataReaderOption option,
            DataListColumns columns)
        {
            log.Debug(string.Format("Start of GetDataFromDataReader.  Option: {0}", option));

            object cachedObject = null;

            // Make sure that data paramaters contains a valid DataParamaters object
            if (dataParameters == null)
            {
                dataParameters = new DataParameters();
            }

            DateTime startTime = DateTime.Now;

            // ********************************************
            // Create the Cache Key
            // ********************************************

            string cacheKey;
            if (columns == null)
            {
                cacheKey = String.Format(
                    "Get{0}: {1}\nParms:\n{2}",
                    option,
                    qd.Sql,
                    dataParameters.ToString());
            }
            else
            {
                cacheKey = String.Format(
                    "Get{0}: {1}\nParms:\n{2}\nColumns:{3}",
                    option,
                    qd.Sql,
                    dataParameters.ToString(),
                    columns.ToString());
            }

            // ********************************************
            // Process Cache stuff if we are caching
            // ********************************************

            // int requestId = requestCount++; (Don't Increment requestId - because it will be used in the GetDataReader method)
            // log.Info(String.Format("SingleRow Query Request \n{0}", cacheKey));

            if (qd.CacheQuery)
            {
                // Attempt to get the results from the cache
                cachedObject = Cache.Get(cacheKey);

                // If the cache didn't have it, get it from the DB
                if (cachedObject == null)
                {
                    // Get the data - based on the option from the data reader
                    object data = DoGetDataFromDataReader(connection, qd, dataParameters, true, option, columns);

                    log.Info(String.Format("Cache MISS - Read from DB (Time: {0} ms)",
                        GetElapsedMilliseconds(startTime)));

                    // Need special handling if the data was NOT FOUND in the DB - because data will be NULL
                    // Unfortunately - this is ALSO how we tell if something isn't in the cache... thus need
                    // to save a placeholder object to indicate that we already looked it up - and didn't find it
                    if (data == null)
                    {
                        cachedObject = DATA_NOT_FOUND;
                    }
                    else
                    {
                        cachedObject = data;
                    }


                    // Cache the Results
                    Cache.Add(
                        cacheKey,
                        cachedObject,
                        qd.CacheAbsoluteExpirationMinutes,
                        qd.CacheSlidingExpirationMinutes);
                }
                else
                {
                    log.Info(String.Format("Cache HIT (Time: {0} ms)", GetElapsedMilliseconds(startTime)));
                }
                // Need to continue our game to return NULL if we got a DATA_NOT_FOUND
                if (cachedObject == DATA_NOT_FOUND)
                {
                    return null;
                }
                else
                {
                    return cachedObject;
                }
            }

            else
            {
                // Get the data from the DB - based on the option from the data reader
                object data = DoGetDataFromDataReader(connection, qd, dataParameters, false, option, columns);

                log.Info(String.Format("Read from DB (Time: {0} ms)", GetElapsedMilliseconds(startTime)));
                return data;
            }
        }

        private static object DoGetDataFromDataReader(
            IDbConnection connection,
            QueryDef qd,
            DataParameters dataParameters,
            bool allowCaching,
            GetDataFromDataReaderOption option,
            DataListColumns columns)
        {
            try
            {
                switch (option)
                {
                    case GetDataFromDataReaderOption.SingleRow:
                        return DoGetDataFromDataReader_SingleRow(connection, qd, dataParameters, allowCaching);

                    case GetDataFromDataReaderOption.MultipleRows:
                        return DoGetDataFromDataReader_MultipleRow(connection, qd, dataParameters, allowCaching);

                    case GetDataFromDataReaderOption.Scalar:
                        return DoGetDataFromDataReader_Scalar(connection, qd, dataParameters, allowCaching);

                    case GetDataFromDataReaderOption.DataList:
                        return DoGetDataFromDataReader_DataList(connection, qd, dataParameters, columns);

                    case GetDataFromDataReaderOption.DataEntity:
                        return DoGetDataFromDataReader_DataEntity(connection, qd, dataParameters, columns);

                    case GetDataFromDataReaderOption.DataScalar:
                        return DoGetDataFromDataReader_DataScalar(connection, qd, dataParameters, columns);

                    default:
                        throw new ArgumentOutOfRangeException("GetDataFromDataReaderOption", "Invalid option: " + option);
                }
            }
            catch (Exception e)
            {
                string message = string.Format(
                    "Exception occurred Getting Data from DataReader, details follow...\nMessage: {0}\nSQL: {1}\n",
                    e.Message,
                    qd.Sql);
                log.Error(message + "\n---> StackTrace:\n" + e.StackTrace);
                throw new Exception(message, e);
            }
        }

        private static object DoGetDataFromDataReader_Scalar(IDbConnection connection, QueryDef qd, DataParameters dataParameters, bool allowCaching)
        {
            object[] row;
            using (DataReaderAndCommand drac = QueryEngine.ExecuteDataReader(connection, qd, dataParameters, allowCaching))
            {
                row = DbTools.GetRowAsArray(drac.DataReader);
            }

            if (row == null)
            {
                return null;
            }
            else
            {
                return row[0];
            }
        }

        private static object DoGetDataFromDataReader_SingleRow(IDbConnection connection, QueryDef qd, DataParameters dataParameters, bool allowCaching)
        {
            CaseInsensitiveHashtable row;
            using (DataReaderAndCommand drac = QueryEngine.ExecuteDataReader(connection, qd, dataParameters, allowCaching))
            {
                row = DbTools.GetRowAsHashtable(drac.DataReader);
            }
            return row;
        }

        private static object DoGetDataFromDataReader_MultipleRow(IDbConnection connection, QueryDef qd, DataParameters dataParameters, bool allowCaching)
        {
            List<CaseInsensitiveHashtable> rows = null;
            CaseInsensitiveHashtable row;
            using (DataReaderAndCommand drac = QueryEngine.ExecuteDataReader(connection, qd, dataParameters, allowCaching))
            {
                while (true)
                {
                    row = DbTools.GetRowAsHashtable(drac.DataReader);

                    if (row == null) { break; }

                    if (rows == null)
                    {
                        rows = new List<CaseInsensitiveHashtable>();
                    }
                    rows.Add(row);
                }
            }
            return rows;
        }
    }
}
