using System;
using System.Data;

namespace Util.Sql.Query
{
    public static partial class QueryEngine
    {
        // ******************************************************************************
        // ExecuteNonQuery Methods
        // ******************************************************************************

        /// <summary>
        /// Executes an SQL statement against the Connection object and returns the number of rows affected.
        /// </summary>
        /// <param name="connection">DB Connection</param>
        /// <param name="qd">Query Definition</param>
        /// <returns>number of rows affected</returns>
        public static int ExecuteNonQuery(
            IDbConnection connection,
            QueryDef qd)
        {
            return DoExecuteNonQuery(connection, qd, null);
        }

        /// <summary>
        /// Executes an SQL statement against the Connection object and returns the number of rows affected.
        /// </summary>
        /// <param name="connection">DB Connection</param>
        /// <param name="qd">Query Definition</param>
        /// <param name="dp">Data Parameters (can be null)</param>
        /// <returns>number of rows affected</returns>
        public static int ExecuteNonQuery(
            IDbConnection connection,
            QueryDef qd,
            DataParameters dataParameters)
        {
            return DoExecuteNonQuery(connection, qd, dataParameters);
        }

        // ******************************************************************************
        // ExecuteNonQuery Support Methods
        // ******************************************************************************

        private static int DoExecuteNonQuery(
            IDbConnection connection,
            QueryDef qd,
            DataParameters dataParameters)
        {
            log.Debug("Start of DoExecuteNonQuery");

            int rowsAffected = 0;
            DateTime startTime = DateTime.Now;

            // Check for valid Cache Option (NOT able to cache NonQueries - since they update the DB)
            if (qd.CacheQuery == true)
            {
                throw new ArgumentOutOfRangeException("Invalid CacheOption.  Not able to Cache NonQueries");
            }

            // Make sure that data paramaters contains a valid DataParamaters object
            if (dataParameters == null)
            {
                dataParameters = new DataParameters();
            }

            // Log the Query
            int requestId = requestCount++;
            if (includeSqlInLog)
            {
                string msg = String.Format("{0}\nParms:\n{1}", qd.Sql, dataParameters.ToString());
                log.Info(String.Format("ExecuteNonQuery Query Request {0}:\n{1}", requestId, msg));
            }
            else
            {
                log.Info(String.Format("ExecuteNonQuery Query Request {0}", requestId));
            }

            try
            {
                // Create the command (make sure that it's disposed after we're done.
                using (IDbCommand command = connection.CreateCommand())
                {
                    command.CommandText = qd.Sql;

                    // See if a CommandTimeout was specified
                    if (qd.CommandTimeout != -1)
                    {
                        command.CommandTimeout = qd.CommandTimeout;
                    }

                    if (qd.CommandType == CommandType.StoredProcedure)
                    {
                        command.CommandType = CommandType.StoredProcedure;
                    }

                    DbTools.AddParametersToCommand(command, dataParameters);

                    // Execute the NonQuery
                    rowsAffected = command.ExecuteNonQuery();

                    DbTools.GetParameterValuesFromCommand(command, dataParameters);
                }

                log.Info(String.Format("Read from DB (req #: {0}) (Time: {1} ms)",
                    requestId,
                    GetElapsedMilliseconds(startTime)));
            }
            catch (Exception e)
            {
                string message = string.Format(
                    "Exception occurred Executing NonQuery, details follow...\nMessage: {0}\nSQL: {1}\n",
                    e.Message,
                    qd.Sql);
                log.Error(message + "\n---> StackTrace:\n" + e.StackTrace);
                throw new Exception(message, e);
            }

            log.Debug("Completed ExecuteNonQuery");

            return rowsAffected;
        }
    }
}
