﻿using Util.Config;

namespace Util.Sql.Query
{
    public class QueryEngineConfigValues
    {
        public static int ShortCacheTimeout { get; private set; }
        public static int LongCacheTimeout { get; private set; }
        public static bool QueryEngineIncludeSqlInLog { get; private set; }

        static QueryEngineConfigValues()
        {
            int? intValue;
            intValue = StringTools.GetNullableInt32(DynamicConfigurationManager.AppSettings[UtilConstants.AppConfigNames.ShortCacheTimeout]);
            if (intValue == null) { ShortCacheTimeout = 5; } else { ShortCacheTimeout = (int)intValue; }

            intValue = StringTools.GetNullableInt32(DynamicConfigurationManager.AppSettings[UtilConstants.AppConfigNames.LongCacheTimeout]);
            if (intValue == null) { LongCacheTimeout = 20; } else { LongCacheTimeout = (int)intValue; }

            bool? boolValue = StringTools.GetNullableBoolean(DynamicConfigurationManager.AppSettings[UtilConstants.AppConfigNames.QueryEngineIncludeSqlInLog]);
            if (intValue == null) { QueryEngineIncludeSqlInLog = false; } else { QueryEngineIncludeSqlInLog = (bool)boolValue; }
        }
    }
}
