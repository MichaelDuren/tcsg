using System;
using System.Data;

namespace Util.Sql.Query
{
    /// <summary>
    /// UGLY Class - for the purpose of working around a limitation of the way that the 
    /// IBM Db2DataReader Dispose Logic seems to work... Must be able to call Dispose on both the DataReader AND the Command,
    /// However, our QueryEngine Class (ExecuteDataReader() Method) 
    ///		Creates a command
    ///		Uses the command to create the DataReader
    ///		Returns the DataReader - but NOT the command
    ///		Does NOT dispose the command - because if it does, the DataReader is invalidated.
    /// 
    /// This class allows that method to return THIS class - which holds BOTH the DataReader AND the Command
    /// </summary>
    public class DataReaderAndCommand : IDisposable
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public IDataReader DataReader;
        public IDbCommand Command;

        public DataReaderAndCommand(IDataReader dr, IDbCommand command)
        {
            this.DataReader = dr;
            this.Command = command;
        }

        #region IDisposable Members

        public void Dispose()
        {
            if (DataReader != null)
            {
                DataReader.Dispose();
            }
            if (Command != null)
            {
                Command.Dispose();
            }
        }

        #endregion
    }
}
