using System;
using System.Data;

namespace Util.Sql.Query
{
    public static partial class QueryEngine
    {
        // ******************************************************************************
        // GetDataReader Methods
        // ******************************************************************************

        /// <summary>
        /// Method to return a DataReader for a query.
        /// 
        /// NOTE:  The DataReader MUST be closed after it is used
        /// Takes a Query definition and returns a DataReader from the Database
        /// </summary>
        /// <param name="connection">DB Connection</param>
        /// <param name="qd">Query Definition</param>
        /// <returns>DataReaderAndCommand</returns>
        public static DataReaderAndCommand GetDataReader(
            IDbConnection connection,
            QueryDef qd)
        {
            return ExecuteDataReader(connection, qd, null, false);
        }

        /// <summary>
        /// Method to return a DataReader for a query.
        /// 
        /// NOTE:  The DataReader MUST be closed after it is used - best to use a "using block"
        /// Takes a Query definition and returns a DataReader from the Database
        /// </summary>
        /// <param name="connection">DB Connection</param>
        /// <param name="qd">Query Definition</param>
        /// <param name="dp">Data Parameters (can be null)</param>
        /// <returns>DataReaderAndCommand</returns>
        public static DataReaderAndCommand GetDataReader(
            IDbConnection connection,
            QueryDef qd,
            DataParameters dataParameters)
        {
            return ExecuteDataReader(connection, qd, dataParameters, false);
        }

        // ******************************************************************************
        // DataReader Support Methods
        // ******************************************************************************

        private static DataReaderAndCommand ExecuteDataReader(
            IDbConnection connection,
            QueryDef qd,
            DataParameters dataParameters,
            bool allowCaching)
        {
            log.Debug("Start of ExecuteDataReader");

            IDbCommand command = null;
            IDataReader dr = null;
            DateTime startTime = DateTime.Now;

            // Check for valid Cache Option (NOT able to cache DataReaders - since they involve a live DB connection)
            // HOWEVER - the GetSingleRow methods DO support caching - so allow caching if "allowCaching" is set to true
            if (allowCaching == false &&
                qd.CacheQuery == true)
            {
                throw new Exception("Invalid CacheOption.  Not able to Cache DataReaders");
            }

            // Make sure that data paramaters contains a valid DataParamaters object
            if (dataParameters == null)
            {
                dataParameters = new DataParameters();
            }

            // Log the Query
            string cacheKey = String.Format(
                "{0}\nParms:\n{1}",
                qd.Sql,
                dataParameters.ToString());
            int requestId = requestCount++;

            if (includeSqlInLog)
            {
                log.Info(String.Format("DataReader Query Request {0}:\n{1}", requestId, cacheKey));
            }
            else
            {
                log.Info(String.Format("DataReader Query Request {0}", requestId));
            }

            try
            {
                // Create the command
                // NOTE:  Can NOT put the command in a USING... the DataReader fails!
                command = connection.CreateCommand();
                command.CommandText = qd.Sql;

                // See if a CommandTimeout was specified
                if (qd.CommandTimeout != -1)
                {
                    command.CommandTimeout = qd.CommandTimeout;
                }

                if (qd.CommandType == CommandType.StoredProcedure)
                {
                    command.CommandType = CommandType.StoredProcedure;
                }

                DbTools.AddParametersToCommand(command, dataParameters);

                // Execute the Reader
                dr = command.ExecuteReader();

                log.Info(String.Format("Read from DB (req #: {0}) (Time: {1} ms)",
                    requestId,
                    GetElapsedMilliseconds(startTime)));
            }
            catch (Exception e)
            {
                string message = string.Format(
                    "Exception occurred Executing Query, details follow...\nMessage: {0}\nSQL: {1}\n",
                    e.Message,
                    cacheKey);
                log.Error(message + "\n---> StackTrace:\n" + e.StackTrace);
                throw new Exception(message, e);
            }

            log.Debug("Completed ExecuteDataReader");

            return new DataReaderAndCommand(dr, command);
        }
    }
}
