using System;

namespace Util.Sql.Query
{
    /// <summary>
    /// Dummy Class - to store in the Cache - to represent Data NOT FOUND - so that I can tell the 
    /// difference between something that I've PREVIOUSLY looked for in the DB and Did Not Find - and
    /// just regular "NULL" values.
    /// </summary>
    internal class DataNotFound
    {
        internal DataNotFound() { }
    }

    public static partial class QueryEngine
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static int requestCount = 1;

        private static bool includeSqlInLog { get { return QueryEngineConfigValues.QueryEngineIncludeSqlInLog; } }

        private static DataNotFound DATA_NOT_FOUND = new DataNotFound();

        // ******************************************************************************
        // Support Methods
        // ******************************************************************************

        private static long GetElapsedMilliseconds(DateTime startTime)
        {
            TimeSpan ts = DateTime.Now - startTime;
            return ts.Ticks / TimeSpan.TicksPerMillisecond;
        }
    }
}
