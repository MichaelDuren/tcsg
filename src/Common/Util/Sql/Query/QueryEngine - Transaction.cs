using System.Data;

namespace Util.Sql.Query
{
    public static partial class QueryEngine
    {
        public static void BeginTransaction(IDbConnection connection)
        {
            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandText = "BEGIN TRANSACTION";

                command.ExecuteNonQuery();
            }
        }

        public static IDbTransaction GetNewTransaction(IDbConnection connection)
        {
            return connection.BeginTransaction();
        }

        public static bool CommitTransaction(IDbConnection connection)
        {
            bool returnValue = true;

            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandText = "COMMIT";
                command.CommandTimeout = 0;

                try
                {
                    command.ExecuteNonQuery();
                }
                catch
                {
                    returnValue = false;
                    RollbackTransaction(connection);
                }
            }

            return returnValue;
        }

        public static void RollbackTransaction(IDbConnection connection)
        {
            using (IDbCommand command = connection.CreateCommand())
            {
                command.CommandText = "ROLLBACK";
                command.CommandTimeout = 0;

                command.ExecuteNonQuery();
            }
        }
    }
}
