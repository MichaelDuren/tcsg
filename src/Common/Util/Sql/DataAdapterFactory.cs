using System;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Data.OracleClient;
using System.Data.SqlClient;

namespace Util.Sql
{
    /// <summary>
    /// Creates database specific DataAdapter objects.  Determines what type of DB the 
    /// passed in object is for and creates a DB specific DataAdapter for it.
    /// </summary>
    public static class DataAdapterFactory
    {
        /// <summary>
        /// Create a DataAdapter from a Connection - Determines what type of DB the 
        /// connection is for and creates a DB specific DataAdapter for it.
        /// </summary>
        /// <param name="sql">SQL String for Select</param>
        /// <param name="connection">Database Connection </param>
        /// <returns>IDbDataAdapter</returns>
        public static IDbDataAdapter CreateDataAdapter(string sql, IDbConnection connection)
        {
            //if (connection is DB2Connection)
            //{
            //    IDbDataAdapter da = null;
            //    da = new DB2DataAdapter(sql, (DB2Connection)connection);
            //    return da;
            //}
            //else if (connection is OleDbConnection)
            if (connection is OleDbConnection)
            {
                return new OleDbDataAdapter(sql, (OleDbConnection)connection);
            }
            else if (connection is SqlConnection)
            {
                return new SqlDataAdapter(sql, (SqlConnection)connection);
            }
            else if (connection is OdbcConnection)
            {
                return new OdbcDataAdapter(sql, (OdbcConnection)connection);
            }
            else if (connection is OracleConnection)
            {
                return new OracleDataAdapter(sql, (OracleConnection)connection);
            }
            else
            {
                throw new ArgumentException("Invalid Connection object");
            }
        }

        /// <summary>
        /// Create a DataAdapter from a Command - Determines what type of DB the 
        /// command is for and creates a DB specific DataAdapter for it.
        /// </summary>
        /// <param name="cmd">Database Command </param>
        /// <returns>IDbDataAdapter</returns>
        public static IDbDataAdapter CreateDataAdapter(IDbCommand cmd)
        {
            //if (cmd is DB2Command)
            //{
            //    IDbDataAdapter da = null;
            //    da = new DB2DataAdapter((DB2Command)cmd);
            //    return da;
            //}
            //else if (cmd is OleDbCommand)
            if (cmd is OleDbCommand)
            {
                return new OleDbDataAdapter((OleDbCommand)cmd);
            }
            else if (cmd is SqlCommand)
            {
                return new SqlDataAdapter((SqlCommand)cmd);
            }
            else if (cmd is OdbcCommand)
            {
                return new OdbcDataAdapter((OdbcCommand)cmd);
            }
            else if (cmd is OracleCommand)
            {
                return new OracleDataAdapter((OracleCommand)cmd);
            }
            else
            {
                throw new ArgumentException("Invalid Command object");
            }
        }
    }
}
