using System;
using System.Web;
using System.Web.Caching;

namespace Util
{
	/// <summary>
	/// Borrowed from code that Don Woodward wrote
	/// </summary>
	public class Cache
	{
		// ***************
		// private members
		// ***************

		private static System.Web.Caching.Cache _Cache = null;

		// ************
		// constructors
		// ************

		static Cache()
		{
			HttpContext context = HttpContext.Current;
			if (context != null)
				_Cache = context.Cache;
			else
				_Cache = HttpRuntime.Cache;
		}

		private Cache() { }


		// **************
		// public methods
		// **************

		/// <summary>
		/// Add object to the cache - specifying the TimeSpan - for Sliding Expiration
		/// </summary>
		/// <param name="key"></param>
		/// <param name="thing"></param>
		/// <param name="ts">TimeSpan - for Sliding Expiration</param>
		public static void Add(string key, object thing, TimeSpan ts)
		{
			// Prevent exception for trying to add nulls to the cache
			if (key == null || thing == null)
			{
				return;
			}

			_Cache.Add(key, thing, null, System.Web.Caching.Cache.NoAbsoluteExpiration, ts, CacheItemPriority.NotRemovable, null);
		}


		/// <summary>
		/// Add object to the cache - specifying the DateTime - for Absolute Expiration
		/// </summary>
		/// <param name="key"></param>
		/// <param name="thing"></param>
		/// <param name="dt">DateTime - for Absolute Expiration</param>
		public static void Add(string key, object thing, DateTime dt)
		{
			// Prevent exception for trying to add nulls to the cache
			if (key == null || thing == null)
			{
				return;
			}

			_Cache.Add(key, thing, null, dt, TimeSpan.Zero, CacheItemPriority.NotRemovable, null);
		}


		/// <summary>
		/// Add object to the cache - specifying the number of minutes to cache it - for Absolute Expiration
		/// </summary>
		/// <param name="key"></param>
		/// <param name="thing"></param>
		/// <param name="dt">Minutes to cache - for Absolute Expiration</param>
		public static void Add(string key, object thing, int minutes)
		{
			Add(key, thing, DateTime.Now.AddMinutes(minutes));
		}


		/// <summary>
		/// Add object to the cache - NOTE:  BOTH slidingExpirationMinutes and absoluteExpirationMinutes cannot be > 0 AND
		/// at least ONE of them MUST be > 0
		/// </summary>
		/// <param name="key"></param>
		/// <param name="thing"></param>
		/// <param name="absoluteExpirationMinutes">Minutes for Absolute Expiration</param>
		/// <param name="slidingExpirationMinutes">Minutes - for Sliding Expiration</param>
		public static void Add(string key, object thing, int absoluteExpirationMinutes, int slidingExpirationMinutes)
		{
			if (absoluteExpirationMinutes == 0 && slidingExpirationMinutes == 0)
			{
				throw new ArgumentException("slidingExpirationMinutes and absoluteExpirationMinutes cannot BOTH be 0");
			}
			if (absoluteExpirationMinutes > 0 && slidingExpirationMinutes > 0)
			{
				throw new ArgumentException("slidingExpirationMinutes and absoluteExpirationMinutes cannot BOTH be > 0");
			}

			if (absoluteExpirationMinutes > 0)
			{
				Add(key, thing, DateTime.Now.AddMinutes(absoluteExpirationMinutes));
			}
			else
			{
				Add(key, thing, new TimeSpan(0, slidingExpirationMinutes, 0));
			}
		}


		public static object Get(string key)
		{
			return _Cache.Get(key);
		}

		public static void Remove(string key)
		{
			if (_Cache.Get(key) != null)
				_Cache.Remove(key);
		}

		public static void Clear()
		{
			string currentKey = "";
			System.Collections.IDictionaryEnumerator cacheContents = _Cache.GetEnumerator();
			while (cacheContents.MoveNext())
			{
				currentKey = cacheContents.Key.ToString();
				_Cache.Remove(currentKey);
			}
		}
	}
}


