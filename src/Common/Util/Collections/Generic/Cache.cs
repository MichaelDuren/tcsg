using System;
using System.Collections.Generic;

namespace Util.Collections.Generic
{
    public enum CacheType
    {
        AbsoluteExpiration,
        SlidingExpiration
    }


    /// <summary>
    /// Class to support Caching specific (generic) types of keys / values.
    /// 
    /// Supports two types of cache approaches.  Absolute Expration and Sliding Expiration.
    ///		Absolute Expiration allows you to specify the number of minutes to cache an item.  
    ///			Once that time has expired, the item is removed from the cache.
    ///		Sliding Expiration is similar to Absolute Expiration, except that additionally, whenever
    ///			the cached object is retreived from the cache (using Cache[key], ContainsKey or Touch)
    ///			the Expiration time is extended - by the originally specified number of minutes - from 
    ///			the current time.
    /// 
    /// The class uses a "lazy" approach to purging items from the cache - where the purge routine is run
    /// only when one of the cache methods are invoked.  However, purging is not done EVERY time that a
    /// cache method is invoked, only if it has been longer than CachePurgeInterval since it was last run.
    /// 
    /// This class is thread safe.
    /// </summary>
    /// <author>Jim Polizzi</author>
    public class Cache<TKey, TValue> : IEnumerable<KeyValuePair<TKey, TValue>>
    {
        // ******************************************************************************
        #region Nested Classes
        // ******************************************************************************

        internal class CacheItem
        {
            public TKey Key;
            public TValue Value;
            public DateTime PurgeTime;

            private double cacheMinutes;
            private CacheType cacheType;

            public CacheItem(TKey key, TValue value, CacheType cacheType, double cacheMinutes)
            {
                this.Key = key;
                this.Value = value;
                this.cacheType = cacheType;

                // Support cache forever if cacheMinutes = 0
                if (cacheMinutes == 0)
                {
                    this.cacheMinutes = Int32.MaxValue;
                }
                else
                {
                    this.cacheMinutes = cacheMinutes;
                }

                this.PurgeTime = DateTime.Now.AddMinutes(this.cacheMinutes);
            }

            // Update the purge type (if the CacheType == sliding expiration
            public void Touch()
            {
                if (cacheType == CacheType.SlidingExpiration)
                {
                    this.PurgeTime = DateTime.Now.AddMinutes(cacheMinutes);
                }
            }
        }

        #endregion

        // ******************************************************
        // Instance Variables
        // ******************************************************

        private Dictionary<TKey, CacheItem> cacheItems = new Dictionary<TKey, CacheItem>();

        private CacheType cacheType;

        private DateTime nextScan;

        private double cachePurgeInterval;

        // ******************************************************
        // Properties
        // ******************************************************

        public int Count
        {
            get
            {
                CachePurgeScan();
                return cacheItems.Count;
            }
        }

        /// <summary>
        /// Returns all of the Keys in the Cache.
        /// Does NOT refresh the CacheItem.PurgeTime.
        /// </summary>
        public ICollection<TKey> Keys
        {
            get
            {
                CachePurgeScan();
                return cacheItems.Keys;
            }
        }

        /// <summary>
        /// Returns all of the Values in the Cache.
        /// Does NOT refresh the CacheItem.PurgeTime.
        /// </summary>
        public ICollection<TValue> Values
        {
            get
            {
                CachePurgeScan();

                List<TValue> values = new List<TValue>();

                lock (cacheItems)
                {
                    foreach (CacheItem cacheItem in cacheItems.Values)
                    {
                        values.Add(cacheItem.Value);
                    }
                }
                return values;
            }
        }

        public TValue this[TKey key]
        {
            get
            {
                return Get(key);
            }
        }

        // ******************************************************************************
        // Constructors
        // ******************************************************************************

        /// <summary>
        /// Defines a new Cache - of the specified cache type - with a 1 minute cachePurgeInterval
        /// </summary>
        /// <param name="cacheType"></param>
        public Cache(CacheType cacheType) : this(cacheType, 1) { }

        /// <summary>
        /// Defines a new Cache - of the specified cache type.
        /// </summary>
        /// <param name="cacheType"></param>
        /// <param name="cachePurgeInterval">Number of minutes between cache Purge scans</param>
        public Cache(CacheType cacheType, double cachePurgeInterval)
        {
            this.cacheType = cacheType;
            this.cachePurgeInterval = cachePurgeInterval;
            this.nextScan = DateTime.Now.AddMinutes(cachePurgeInterval);
        }

        // ******************************************************************************
        // Methods
        // ******************************************************************************

        /// <summary>
        /// Adds an item to the Cache.
        /// </summary>
        /// <param name="key"></param>
        /// <param name="originalValue"></param>
        /// <param name="cacheMinutes">Determines how many minutes to cache the item.  0 = Cache forever</param>
        public virtual void Add(TKey key, TValue value, double cacheMinutes)
        {
            // Prevent exception for trying to add null keys to the cache
            if (key == null)
            {
                return;
            }

            CachePurgeScan();
            lock (cacheItems)
            {
                // Don't Use cacheItems.Add - because it chokes if there is a duplicate key...
                // cacheItems.Add(key, new CacheItem(key, originalValue, cacheType, cacheMinutes));
                cacheItems[key] = new CacheItem(key, value, cacheType, cacheMinutes);
            }
        }

        public void Clear()
        {
            lock (cacheItems)
            {
                cacheItems.Clear();
            }
        }

        /// <summary>
        /// Determines if the specified Key is in the cache.
        /// Refreshes the CacheItem.PurgeTime if CacheType = SlidingExpiration
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool ContainsKey(TKey key)
        {
            CachePurgeScan();

            lock (cacheItems)
            {
                // If the key is found - then return it
                if (cacheItems.ContainsKey(key))
                {
                    CacheItem cacheItem = cacheItems[key];
                    cacheItem.Touch();
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Specifically refreshes the CacheItem.PurgeTime in the cache (if CacheType = SlidingExpiration)
        /// Does NOT return an exception if the specified item is not in the cache.
        /// </summary>
        /// <param name="key"></param>
        public void Touch(TKey key)
        {
            CachePurgeScan();

            lock (cacheItems)
            {
                if (cacheItems.ContainsKey(key))
                {
                    CacheItem cacheItem = cacheItems[key];
                    cacheItem.Touch();
                }
            }
        }

        /// <summary>
        /// Returns the cached item if found - or null if not.
        /// Refreshes the CacheItem.PurgeTime if CacheType = SlidingExpiration
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public TValue Get(TKey key)
        {
            CachePurgeScan();

            lock (cacheItems)
            {
                // If the key is found - then return it
                if (cacheItems.ContainsKey(key))
                {
                    CacheItem cacheItem = cacheItems[key];
                    cacheItem.Touch();
                    return cacheItem.Value;
                }
                // Else - return null - to indicate that it was not found
                return default(TValue);
            }
        }

        /// <summary>
        /// Returns the cached item if found - or null if not.
        /// Does NOT refresh the CacheItem.PurgeTime
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public TValue GetWithoutRefresh(TKey key)
        {
            CachePurgeScan();

            lock (cacheItems)
            {
                // If the key is found - then return it
                if (cacheItems.ContainsKey(key))
                {
                    CacheItem cacheItem = cacheItems[key];
                    return cacheItem.Value;
                }
                // Else - return null - to indicate that it was not found
                return default(TValue);
            }
        }

        /// <summary>
        /// Removes the item from the cache.  Does NOT fail if the item is not in the cache.
        /// </summary>
        public virtual bool Remove(TKey key)
        {
            CachePurgeScan();
            lock (cacheItems)
            {
                return cacheItems.Remove(key);
            }
        }

        /// <summary>
        /// Scans the Cache - to see which items need to be purged.
        /// Only runs at most every CachePurgeInterval
        /// </summary>
        private void CachePurgeScan()
        {
            DateTime now = DateTime.Now;
            List<TKey> keysToRemove = new List<TKey>();

            lock (cacheItems)
            {
                if (now > nextScan)
                {
                    // Locate items to purge (can't purge here - because can't modify an enumeration
                    foreach (CacheItem cacheItem in cacheItems.Values)
                    {
                        if (cacheItem.PurgeTime <= now)
                        {
                            keysToRemove.Add(cacheItem.Key);
                        }
                    }

                    // Remove the items that have expired
                    foreach (TKey keyToRemove in keysToRemove)
                    {
                        cacheItems.Remove(keyToRemove);
                    }

                    // Reset cachePurgeInterval
                    this.nextScan = DateTime.Now.AddMinutes(this.cachePurgeInterval);
                }
            }
        }

        // ******************************************************************************
        // IEnumerable / IEnumerator Methods
        // ******************************************************************************

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        /// <summary>
        /// Returns an Enumerator of all items in the cache.
        /// Does NOT refresh the CacheItem.PurgeTime.
        /// </summary>
        /// <returns></returns>
        IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator()
        {
            CachePurgeScan();

            // Create a list of the cacheItems values - so that we don't have to lock the cacheItems for the entire time
            // that this enumeration is being accessed by the calling class.
            List<CacheItem> tempCacheItems = new List<CacheItem>();
            lock (cacheItems)
            {
                foreach (CacheItem ci in cacheItems.Values)
                {
                    tempCacheItems.Add(ci);
                }
            }

            foreach (CacheItem cacheItem in tempCacheItems)
            {
                KeyValuePair<TKey, TValue> kvp = new KeyValuePair<TKey, TValue>(cacheItem.Key, cacheItem.Value);
                yield return kvp;
            }
        }

    }
}
