using System;
using System.Collections.Generic;

namespace Util.Collections.Generic
{
    /// <summary>
    /// Class to support (generic) dictionary functionality - but with a case insensitive string key.
    /// Extends Dictionary - but makes it operate in a Case Insensitive way.
    /// </summary>
    public class CaseInsensitiveDictionary<TValue> : Dictionary<string, TValue>
    {
        /*
         * 
         * 
         * NOTE:  Although there is not much to this class - it is COMPLETE - and provides the desired functionality
         * 
         * 
         */

        public CaseInsensitiveDictionary() : base(StringComparer.CurrentCultureIgnoreCase) { }

        public CaseInsensitiveDictionary(CaseInsensitiveDictionary<TValue> dictionary) : base(dictionary, StringComparer.CurrentCultureIgnoreCase) { }

        public CaseInsensitiveDictionary(int capacity) : base(capacity, StringComparer.CurrentCultureIgnoreCase) { }

        public TValue GetValueOrNull(string key)
        {
            return ExtensionMethods.GetValueOrNull(this, key);
        }

    }
}
