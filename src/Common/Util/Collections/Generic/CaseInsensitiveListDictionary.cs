using System;

namespace Util.Collections.Generic
{
	/// <summary>
	/// Extends ListDictionary - but makes it operate in a Case Insensitive way.
	/// </summary>
	public class CaseInsensitiveListDictionary<TValue> : ListDictionary<string, TValue>
	{
		/*
		 * 
		 * 
		 * NOTE:  Although there is not much to this class - it is COMPLETE - and provides the desired functionality
		 * 
		 * 
		 */

		// ******************************************************************************
		// Constructors
		// ******************************************************************************

		public CaseInsensitiveListDictionary() : base(StringComparer.CurrentCultureIgnoreCase) { }

		public CaseInsensitiveListDictionary(CaseInsensitiveListDictionary<TValue> dictionary) : base(dictionary, StringComparer.CurrentCultureIgnoreCase) { }

		public CaseInsensitiveListDictionary(int capacity) : base(capacity, StringComparer.CurrentCultureIgnoreCase) { }


		// ******************************************************
		// Properties
		// ******************************************************

		public override TValue this[string key]
		{
			get
			{
				return dictionary[key];
			}
			set
			{
				// See if the Key already exists to determine how to handle the keys / values lists
				TValue oldValue;
				if (dictionary.TryGetValue(key, out oldValue))
				{
					// Update the ValuesList - Note:  Key does not need to change
					valuesList[valuesList.IndexOf(oldValue)] = value;
				}
				else
				{
					// Add the item to both the keysList and the valuesList
					keysList.Add(key.ToUpper());
					valuesList.Add(value);
				}

				dictionary[key] = value;
			}
		}


		// ******************************************************
		// Methods
		// ******************************************************

		public override void Add(string key, TValue value)
		{
			// Add Key to List
			keysList.Add(key.ToUpper());

			// Add Value to List
			valuesList.Add(value);

			dictionary.Add(key, value);
		}


		public override bool Remove(string key)
		{
			// Find the Value in the Key List - and remove it if found
			keysList.Remove(key.ToUpper());

			// Find the Value in the Value List - and remove it if found
			TValue value;
			if (dictionary.TryGetValue(key, out value))
			{
				valuesList.Remove(value);
			}

			return dictionary.Remove(key);
		}


	}
}
