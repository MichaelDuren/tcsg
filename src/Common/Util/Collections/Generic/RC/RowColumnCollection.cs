using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Util.Collections.Generic.RC
{
    public class RowColumnCollection<TRowKey, TColumnKey> 
    {
        // Properties
        public RCColumns<TRowKey, TColumnKey> Columns { get; private set; }
        public RCRows<TRowKey, TColumnKey> Rows { get; private set; }

        protected CaseInsensitiveDictionary<object> _attributes;
        /// <summary>
        /// Collection Attributes
        /// </summary>
        public CaseInsensitiveDictionary<object> Attributes
        {
            get
            {
                // Initialize if used
                if (_attributes == null) { _attributes = new CaseInsensitiveDictionary<object>(); }
                return _attributes;
            }
        }

        // Constructors
        public RowColumnCollection()
        {
            // Initialize
            this.Columns = new RCColumns<TRowKey, TColumnKey>(this);
            this.Rows = new RCRows<TRowKey, TColumnKey>(this, Columns);
        }
    }
}
