using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Util.Collections.Generic.RC
{
    /// <summary>
    /// This is an implementation of the RowColumnCollection - specifying the Column type as String - and implementing IDataReader - including 
    /// </summary>
    public class StandardRowColumnCollection<TRowKey> : RowColumnCollection<TRowKey, string>, IDataReader, IDisposable
    {
        // Variables (to support IDataReader)
        protected bool _isClosed;
        protected bool _readInitialized;
        protected List<RCRow<TRowKey, string>> _dataReaderRows;
        protected RCRow<TRowKey, string> _currentDataReaderRow;
        protected List<RCCell<TRowKey, string>> _currentDataReaderRowAsList;
        protected int _currentReadRow;

        // Constructors
        public StandardRowColumnCollection() : base() { }

        // ********************************************************************************
        #region IDataReader Members
        // ********************************************************************************

        void _close()
        {
            _isClosed = true;
            _dataReaderRows = null;
            _currentDataReaderRow = null;
            _currentDataReaderRowAsList = null;
            _readInitialized = false;
        }

        void IDataReader.Close()
        {
            _close();
        }

        bool IDataReader.IsClosed
        {
            get { return _isClosed; }
        }

        DataTable IDataReader.GetSchemaTable()
        {
            DataSet dataSet = new DataSet();
            dataSet.Locale = System.Globalization.CultureInfo.CurrentCulture;
            DataTable schemaTable = dataSet.Tables.Add();

            schemaTable.Columns.Add("ColumnName", typeof(string));
            schemaTable.Columns.Add("ColumnOrdinal", typeof(Int32));
            schemaTable.Columns.Add("ColumnSize", typeof(Int32));
            schemaTable.Columns.Add("NumericPrecision", typeof(Int32));
            schemaTable.Columns.Add("NumericScale", typeof(Int32));
            schemaTable.Columns.Add("DataType", typeof(Type));
            schemaTable.Columns.Add("ProviderType", typeof(Int32));
            schemaTable.Columns.Add("AllowDbNull", typeof(bool));
            schemaTable.Columns.Add("IsLong", typeof(bool));
            schemaTable.Columns.Add("IsUnique", typeof(bool));
            schemaTable.Columns.Add("IsReadOnly", typeof(bool));
            schemaTable.Columns.Add("IsRowVersion", typeof(bool));

            int i = 0;
            foreach (var rcColumn in this.Columns.Values)
            {
                DataRow row = schemaTable.NewRow();
                row["ColumnName"] = rcColumn.ColumnKey;
                row["ColumnOrdinal"] = i++;

                // Note: the following are just guesses for default values
                row["ColumnSize"] = -1;
                row["NumericPrecision"] = -1;
                row["NumericScale"] = -1;
                row["DataType"] = typeof(string);
                row["ProviderType"] = 1;
                row["AllowDbNull"] = true;
                row["IsLong"] = false;
                row["IsUnique"] = false;
                row["IsReadOnly"] = false;
                row["IsRowVersion"] = false;

                schemaTable.Rows.Add(row);
            }

            return schemaTable;
        }

        bool IDataReader.NextResult()
        {
            return false;
        }

        bool IDataReader.Read()
        {
            Guard.FailIfTrue(_isClosed, "Invalid Operation - the IDataReader is closed");

            if (!_readInitialized)
            {
                InitializeRead();
            }

            // TODO: Need to confirm that this works - ie. that it's getting the right row count to indicate when it's DONE

            if (_dataReaderRows.Count > _currentReadRow)
            {
                _currentDataReaderRow = _dataReaderRows[_currentReadRow];
                _currentDataReaderRowAsList = _currentDataReaderRow.Cells.Values.ToList();

                _currentReadRow++;
                return true;
            }
            else
            {
                _close();
                return false;
            }
        }

        private void InitializeRead()
        {
            _currentReadRow = 0;
            _dataReaderRows = Rows.Values.ToList();
            _readInitialized = true;
        }

        // ***************************************
        // Not Implemented Methods
        // ***************************************

        int IDataReader.Depth { get { throw new System.NotImplementedException(); } }
        int IDataReader.RecordsAffected { get { throw new System.NotImplementedException(); } }

        #endregion

        // ********************************************************************************
        #region IDataRecord Members
        // ********************************************************************************

        int IDataRecord.FieldCount
        {
            get { return Columns.Count; }
        }

        string IDataRecord.GetName(int i)
        {
            return _currentDataReaderRowAsList[i].Column.ColumnKey.ToString();
        }

        object IDataRecord.GetValue(int i)
        {
            return _currentDataReaderRowAsList[i].Value;
        }

        bool IDataRecord.IsDBNull(int i)
        {
            if (_currentDataReaderRowAsList[i].Value == null) { return true; }
            else { return false; }
        }

        object IDataRecord.this[int i]
        {
            get
            {
                return _currentDataReaderRowAsList[i].Value;
            }
        }

        object IDataRecord.this[string name]
        {
            get
            {
                return _currentDataReaderRow.Cells[name].Value;
            }
        }

        // ***************************************
        // Not Implemented Methods
        // ***************************************

        //object IDataRecord.this[string name] { get { throw new System.NotImplementedException(); } }

        bool IDataRecord.GetBoolean(int i) { throw new System.NotImplementedException(); }
        byte IDataRecord.GetByte(int i) { throw new System.NotImplementedException(); }
        long IDataRecord.GetBytes(int i, long fieldOffset, byte[] buffer, int bufferoffset, int length) { throw new System.NotImplementedException(); }
        char IDataRecord.GetChar(int i) { throw new System.NotImplementedException(); }
        long IDataRecord.GetChars(int i, long fieldoffset, char[] buffer, int bufferoffset, int length) { throw new System.NotImplementedException(); }
        IDataReader IDataRecord.GetData(int i) { throw new System.NotImplementedException(); }
        string IDataRecord.GetDataTypeName(int i) { throw new System.NotImplementedException(); }
        System.DateTime IDataRecord.GetDateTime(int i) { throw new System.NotImplementedException(); }
        decimal IDataRecord.GetDecimal(int i) { throw new System.NotImplementedException(); }
        double IDataRecord.GetDouble(int i) { throw new System.NotImplementedException(); }
        System.Type IDataRecord.GetFieldType(int i) { throw new System.NotImplementedException(); }
        float IDataRecord.GetFloat(int i) { throw new System.NotImplementedException(); }
        System.Guid IDataRecord.GetGuid(int i) { throw new System.NotImplementedException(); }
        short IDataRecord.GetInt16(int i) { throw new System.NotImplementedException(); }
        int IDataRecord.GetInt32(int i) { throw new System.NotImplementedException(); }
        long IDataRecord.GetInt64(int i) { throw new System.NotImplementedException(); }
        int IDataRecord.GetOrdinal(string name) { throw new System.NotImplementedException(); }
        string IDataRecord.GetString(int i) { throw new System.NotImplementedException(); }
        int IDataRecord.GetValues(object[] values) { throw new System.NotImplementedException(); }

        #endregion

        // ********************************************************************************
        #region IDisposable Members
        // ********************************************************************************

        void System.IDisposable.Dispose()
        {
            _close();
        }

        #endregion

    }
}
