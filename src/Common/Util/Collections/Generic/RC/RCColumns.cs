using System.Collections.Generic;

namespace Util.Collections.Generic.RC
{
    // ******************************************************************************
    #region RCColumns
    // ******************************************************************************

    public class RCColumns<TRowKey, TColumnKey> : Dictionary<TColumnKey, RCColumn<TRowKey, TColumnKey>>
    {
        // **************************************
        // Properties
        // **************************************

        public RowColumnCollection<TRowKey, TColumnKey> RowColumnCollection { get; private set; }

        /// <summary>
        /// Gentler "this" - automatically adds and initializes as necessary
        /// </summary>
        public new RCColumn<TRowKey, TColumnKey> this[TColumnKey columnKey]
        {
            get
            {
                if (this.ContainsKey(columnKey))
                {
                    return base[columnKey];
                }
                else
                {
                    RCColumn<TRowKey, TColumnKey> column = new RCColumn<TRowKey, TColumnKey>(columnKey, this);
                    this.Add(columnKey, column);
                    return column;
                }
            }
            set
            {
                // Invoke above "Get" logic - to make sure that we get an initialized Column
                RCColumn<TRowKey, TColumnKey> column = this[columnKey];
                column = value;
            }
        }

        // **************************************
        // Constructors
        // **************************************

        private RCColumns() { }

        public RCColumns(RowColumnCollection<TRowKey, TColumnKey> rowColumnCollection)
        {
            this.RowColumnCollection = rowColumnCollection;
        }

        // **************************************
        // Methods
        // **************************************

        /// <summary>
        /// Add method that automatically initializes the RCColumn
        /// </summary>
        public void Add(TColumnKey columnKey)
        {
            this.Add(columnKey, new RCColumn<TRowKey, TColumnKey>(columnKey, this));
        }

        /// <summary>
        /// Add method - to add multiple columns
        /// </summary>
        public void Add(List<TColumnKey> columnKeys)
        {
            foreach (var columnKey in columnKeys)
            {
                this.Add(columnKey);
            }
        }
    }

    #endregion

    // ******************************************************************************
    #region RCColumn
    // ******************************************************************************

    public class RCColumn<TRowKey, TColumnKey>
    {
        // **************************************
        // Properties
        // **************************************

        public TColumnKey ColumnKey { get; set; }
        public RCColumns<TRowKey, TColumnKey> Columns { get; set; }

        public RCCells<TRowKey, TRowKey, TColumnKey> Cells
        {
            get
            {
                RCCells<TRowKey, TRowKey, TColumnKey> cells = new RCCells<TRowKey, TRowKey, TColumnKey>(this);

                // Get the cells in this column
                foreach (var row in this.Columns.RowColumnCollection.Rows.Values)
                {
                    cells.Base_Add(row.RowKey, row.Cells[this.ColumnKey]);
                }
                return cells;
            }
        }

        private CaseInsensitiveDictionary<object> _attributes;
        /// <summary>
        /// Column Attributes
        /// </summary>
        public CaseInsensitiveDictionary<object> Attributes
        {
            get
            {
                // Initialize if used
                if (_attributes == null) { _attributes = new CaseInsensitiveDictionary<object>(); }
                return _attributes;
            }
        }

        // **************************************
        // Constructors
        // **************************************

        private RCColumn() { }

        public RCColumn(TColumnKey columnKey, RCColumns<TRowKey, TColumnKey> columns)
        {
            this.ColumnKey = columnKey;
            this.Columns = columns;
        }
    }

    #endregion
}
