using System;
using System.Collections.Generic;

namespace Util.Collections.Generic.RC
{
    public enum RowOrColumn { Row, Column }

    // ******************************************************************************
    #region RCCells
    // ******************************************************************************

    public class RCCells<TRowOrColumnKey, TRowKey, TColumnKey> : Dictionary<TRowOrColumnKey, RCCell<TRowKey, TColumnKey>>
    {
        // **************************************
        // Properties
        // **************************************

        /// <summary>
        /// Specifies if this Cells collection is for a Row or a Column.
        /// </summary>
        public RowOrColumn RowOrColumn { get; private set; }

        /// <summary>
        /// If RowOrColumn is Row - then this is a reference to the Row, otherwise, it is null
        /// </summary>
        public RCRow<TRowKey, TColumnKey> Row { get; private set; }

        /// <summary>
        /// If RowOrColumn is Column - then this is a reference to the Column, otherwise, it is null
        /// </summary>
        public RCColumn<TRowKey, TColumnKey> Column { get; private set; }

        /// <summary>
        /// Gets the Total of the Cell Values
        /// </summary>
        public int IntTotal
        {
            get
            {
                int total = 0;
                foreach (var cell in this.Values)
                {
                    total += cell.IntValue;
                }
                return total;
            }
        }

        /// <summary>
        /// Gentler "this" - provides readable exception for invalid operations 
        /// Note:  it is not valid to add cells using 'this.set()'.  
        /// Additionally, it is not valid to add cells to a column cells collection
        /// </summary>
        public new RCCell<TRowKey, TColumnKey> this[TRowOrColumnKey rowOrColumnKey]
        {
            get
            {
                if (this.ContainsKey(rowOrColumnKey))
                {
                    return base[rowOrColumnKey];
                }


                // *************************************
                // The following logic is not safe
                // *************************************

                //else if (RowOrColumn == RowOrColumn.Row)
                //{
                //    // Crazy logic to force a cast (unable to do direct cast with generic type specifiers)
                //    //object o = rowOrColumnKey;
                //    //TColumnKey columnKey = (TColumnKey)o;
                //    TColumnKey columnKey = (TColumnKey)(object)rowOrColumnKey;

                //    RCColumns<TRowKey, TColumnKey> columns = this.Row.Rows.RowColumnCollection.Columns;

                //    // Add column to the columns collection if not already present
                //    if (!columns.ContainsKey(columnKey))
                //    {
                //        columns.Add(columnKey);
                //    }

                //    //  Add new cell to the collection
                //    RCCell<TRowKey, TColumnKey> cell = new RCCell<TRowKey, TColumnKey>(columns[columnKey], this.Row);
                //    this.Base_Add(rowOrColumnKey, cell);
                //    //this.Base_Add(rowOrColumnKey, new RCCell<TRowKey, TColumnKey>(columns[columnKey], this.Row));
                //    return cell;

                //}

                else
                {
                    throw new InvalidOperationException(string.Format(
                        "The key: {0} does not exist in the Cells collection.  All Cells are created when a new Row is created based on the defined columns.",
                        rowOrColumnKey));

                    // "The key: {0} does not exist in the Cells collection. It is not valid to use the 'this.get()' operation to create new cells on a Column.Cells collection.  All Cells must be created when a new Row is created or using the this.get() operation on a Row.Cells collection.",
                }

            }
            set
            {
                throw new InvalidOperationException(
                    "It is not valid to use the 'this.set()' operation to create new cells.  All Cells must be created when a new Row is created based on the defined columns.");
                //"It is not valid to use the 'this.set()' operation to create new cells.  All Cells must be created when a new Row is created or using the this.get() operation on a Row.Cells collection.");
            }
        }

        // **************************************
        // Constructors
        // **************************************

        private RCCells() { }

        public RCCells(RCRow<TRowKey, TColumnKey> row)
        {
            this.RowOrColumn = RowOrColumn.Row;
            this.Row = row;
        }

        public RCCells(RCColumn<TRowKey, TColumnKey> column)
        {
            this.RowOrColumn = RowOrColumn.Column;
            this.Column = column;
        }

        // **************************************
        // Methods
        // **************************************

        public new void Add(TRowOrColumnKey key, RCCell<TRowKey, TColumnKey> value)
        {
            throw new InvalidOperationException(
                "It is not valid to use the 'Add()' method to create new cells.  All Cells must be created when a new Row is created based on the defined columns.");
            //"It is not valid to use the 'Add()' method to create new cells.  All Cells must be created when a new Row is created or using the this.get() operation on a Row.Cells collection.");
        }

        internal void Base_Add(TRowOrColumnKey key, RCCell<TRowKey, TColumnKey> value)
        {
            base.Add(key, value);
        }

    }

    #endregion

    // ******************************************************************************
    #region RCCell
    // ******************************************************************************

    public class RCCell<TRowKey, TColumnKey>
    {
        public int _intValue;
        private bool intValueUsed;

        private bool _booleanValue;
        private bool booleanValueUsed;

        // **************************************
        // Properties
        // **************************************

        public string StringValue { get; set; }
        public DateTime? DateValue { get; set; }
        public object ExtraValue { get; set; }

        public int IntValue
        {
            get { return _intValue; }
            set { _intValue = value; intValueUsed = true; }
        }

        public bool BooleanValue
        {
            get { return _booleanValue; }
            set { _booleanValue = value; booleanValueUsed = true; }
        }

        // Returns the first non-null value from the following: StringValue, IntValue, BooleanValue, DateValue, ExtraValue
        public object Value
        {
            get
            {
                if (StringValue != null) { return StringValue; }
                else if (intValueUsed) { return IntValue; }
                else if (booleanValueUsed) { return BooleanValue; }
                else if (DateValue != null) { return DateValue; }
                else { return ExtraValue; }
            }
        }

        public RCColumn<TRowKey, TColumnKey> Column { get; set; }
        public RCRow<TRowKey, TColumnKey> Row { get; set; }

        private CaseInsensitiveDictionary<object> _attributes;
        /// <summary>
        /// Cell Attributes
        /// </summary>
        public CaseInsensitiveDictionary<object> Attributes
        {
            get
            {
                // Initialize if used
                if (_attributes == null) { _attributes = new CaseInsensitiveDictionary<object>(); }
                return _attributes;
            }
        }

        // **************************************
        // Constructors
        // **************************************

        public RCCell(RCColumn<TRowKey, TColumnKey> column, RCRow<TRowKey, TColumnKey> row)
        {
            this.Column = column;
            this.Row = row;
        }
    }

    #endregion
}
