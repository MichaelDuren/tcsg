using System;
using System.Collections.Generic;

namespace Util.Collections.Generic.RC
{
    public enum RCSortOrder { Ascending = 1, Descending = -1 }

    // ******************************************************************************
    #region RCRows
    // ******************************************************************************

    public class RCRows<TRowKey, TColumnKey> : Dictionary<TRowKey, RCRow<TRowKey, TColumnKey>>
    {
        // **************************************
        // Properties
        // **************************************

        public RCColumns<TRowKey, TColumnKey> Columns { get; private set; }
        public RowColumnCollection<TRowKey, TColumnKey> RowColumnCollection { get; private set; }

        /// <summary>
        /// Gentler "this" - automatically adds and initializes as necessary
        /// </summary>
        public new RCRow<TRowKey, TColumnKey> this[TRowKey rowKey]
        {
            get
            {
                if (this.ContainsKey(rowKey))
                {
                    return base[rowKey];
                }
                else
                {
                    RCRow<TRowKey, TColumnKey> row = new RCRow<TRowKey, TColumnKey>(rowKey, this, Columns);
                    this.Add(rowKey, row);
                    return row;
                }
            }
            set
            {
                // Invoke above "Get" logic - to make sure that we get an initialized Row
                RCRow<TRowKey, TColumnKey> row = this[rowKey];
                row = value;
            }
        }


        // **************************************
        // Constructors
        // **************************************

        private RCRows() { }

        public RCRows(RowColumnCollection<TRowKey, TColumnKey> rowColumnCollection, RCColumns<TRowKey, TColumnKey> columns)
        {
            this.Columns = columns;
            this.RowColumnCollection = rowColumnCollection;
        }

        // **************************************
        // Methods
        // **************************************

        /// <summary>
        /// Add method that automatically initializes the RCRow
        /// </summary>
        public void Add(TRowKey rowKey)
        {
            this.Add(rowKey, new RCRow<TRowKey, TColumnKey>(rowKey, this, Columns));
        }

        /// <summary>
        /// Add method - to add multiple columns
        /// </summary>
        public void Add(List<TRowKey> rowKeys)
        {
            foreach (var rowKey in rowKeys)
            {
                Add(rowKey);
            }
        }

        /// <summary>
        /// Returns the Values collection in Sorted order by the TRowKey
        /// </summary>
        public ICollection<RCRow<TRowKey, TColumnKey>> GetSortedValuesByRowKey()
        {
            return GetSortedValuesByRowKey(RCSortOrder.Ascending);
        }

        /// <summary>
        /// Returns the Values collection in Sorted order by the TRowKey
        /// </summary>
        public ICollection<RCRow<TRowKey, TColumnKey>> GetSortedValuesByRowKey(RCSortOrder rcSortOrder)
        {
            // Copy the values collection
            List<RCRow<TRowKey, TColumnKey>> rows = new List<RCRow<TRowKey, TColumnKey>>();
            foreach (var row in this.Values)
            {
                rows.Add(row);
            }

            int sortOrderModifier = (int)rcSortOrder;

            // Sort the copied rows
            rows.Sort(
                delegate(RCRow<TRowKey, TColumnKey> row1, RCRow<TRowKey, TColumnKey> row2)
                {
                    if (row1.RowKey is IComparable)
                    {
                        return sortOrderModifier * ((IComparable)row1.RowKey).CompareTo((IComparable)row2.RowKey);
                    }
                    else
                    {
                        throw new ArgumentOutOfRangeException("Can only Sort rows with Row Keys that are IComparable");
                    }
                });

            return rows;
        }

        /// <summary>
        /// Returns the Values collection in Sorted order by the Row Total (Cells.IntTotal)
        /// </summary>
        public ICollection<RCRow<TRowKey, TColumnKey>> GetSortedValuesByRowTotal(RCSortOrder rcSortOrder)
        {
            // Copy the values collection
            List<RCRow<TRowKey, TColumnKey>> rows = new List<RCRow<TRowKey, TColumnKey>>();
            foreach (var row in this.Values)
            {
                rows.Add(row);
            }

            int sortOrderModifier = (int)rcSortOrder;

            // Sort the copied rows
            rows.Sort(
                delegate(RCRow<TRowKey, TColumnKey> row1, RCRow<TRowKey, TColumnKey> row2)
                {
                    return sortOrderModifier * (row1.Cells.IntTotal.CompareTo(row2.Cells.IntTotal));
                });

            return rows;
        }
    }

    #endregion

    // ******************************************************************************
    #region RCRow
    // ******************************************************************************

    public class RCRow<TRowKey, TColumnKey>
    {
        // **************************************
        // Properties
        // **************************************

        public TRowKey RowKey { get; set; }
        public RCRows<TRowKey, TColumnKey> Rows { get; set; }

        public RCCells<TColumnKey, TRowKey, TColumnKey> Cells { get; set; }

        private CaseInsensitiveDictionary<object> _attributes;
        /// <summary>
        /// Row Attributes
        /// </summary>
        public CaseInsensitiveDictionary<object> Attributes
        {
            get
            {
                // Initialize if used
                if (_attributes == null) { _attributes = new CaseInsensitiveDictionary<object>(); }
                return _attributes;
            }
        }

        // **************************************
        // Constructors
        // **************************************

        private RCRow()
        {
            this.Cells = new RCCells<TColumnKey, TRowKey, TColumnKey>(this);
        }

        public RCRow(TRowKey rowKey, RCRows<TRowKey, TColumnKey> rows, RCColumns<TRowKey, TColumnKey> columns)
            : this()
        {
            this.RowKey = rowKey;
            this.Rows = rows;

            // Initialize Row Cells
            foreach (TColumnKey columnKey in columns.Keys)
            {
                Cells.Base_Add(columnKey, new RCCell<TRowKey, TColumnKey>(columns[columnKey], this));
            }
        }

    }

    #endregion
}
