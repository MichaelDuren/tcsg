using System;
using System.Collections.Generic;

namespace Util.Collections.Generic
{
    /// <summary>
    /// Class to support (generic) a blending of List AND Dictionary functionality.  Supports standard
    /// Dictionary operations, but also guarantees that the values and keys are able to be returned in
    /// their correct (list) order.  Also supports both the List and Dictionary Indexers (this[int index] and this[TKey key]).
    /// Extends IDictionary.
    /// </summary>
    /// <author>Jim Polizzi</author>
    public class ListDictionary<TKey, TValue> : IDictionary<TKey, TValue>
    {
        // Internal Dictionary
        protected Dictionary<TKey, TValue> dictionary;

        // Lists to contain the internal Key/Value items.
        protected List<TValue> valuesList = new List<TValue>();
        protected List<TKey> keysList = new List<TKey>();


        // ******************************************************************************
        // Constructors
        // ******************************************************************************

        public ListDictionary()
        {
            dictionary = new Dictionary<TKey, TValue>();
        }

        public ListDictionary(IDictionary<TKey, TValue> dictionary)
        {
            dictionary = new Dictionary<TKey, TValue>(dictionary);
        }

        public ListDictionary(IEqualityComparer<TKey> comparer)
        {
            dictionary = new Dictionary<TKey, TValue>(comparer);
        }

        public ListDictionary(Int32 capacity)
        {
            dictionary = new Dictionary<TKey, TValue>(capacity);
        }

        public ListDictionary(IDictionary<TKey, TValue> dictionary, IEqualityComparer<TKey> comparer)
        {
            dictionary = new Dictionary<TKey, TValue>(dictionary, comparer);
        }

        public ListDictionary(Int32 capacity, IEqualityComparer<TKey> comparer)
        {
            dictionary = new Dictionary<TKey, TValue>(capacity, comparer);
        }


        // ******************************************************************************
        // Additional Non-Interface Methods
        // ******************************************************************************

        public void Clear()
        {
            keysList.Clear();
            valuesList.Clear();

            dictionary.Clear();
        }


        public TValue this[int index]
        {
            get
            {
                return valuesList[index];
            }

            /*
            set
            {
                TValue oldValue;
                if (valuesList.Contains(
                    = valuesList.


                valuesList[index] = originalValue;


                // Update the originalValue in THIS dictionary
                dictionary[key] = originalValue;
            }
            */
        }


        // ******************************************************************************
        #region IDictionary<TKey,TValue> Members
        // ******************************************************************************


        // ******************************************************
        // Properties
        // ******************************************************

        public int Count
        {
            get
            {
                return dictionary.Count;
            }
        }

        /// <summary>
        /// Returns the list keys in the correct (list) order
        /// </summary>
        public ICollection<TKey> Keys
        {
            get
            {
                return keysList;
            }
        }


        /// <summary>
        /// Returns the list in the correct (list) order
        /// </summary>
        public ICollection<TValue> Values
        {
            get
            {
                return valuesList;
            }
        }

        public virtual TValue this[TKey key]
        {
            get
            {
                return dictionary[key];
            }
            set
            {
                // See if the Key already exists to determine how to handle the keys / values lists
                TValue oldValue;
                if (dictionary.TryGetValue(key, out oldValue))
                {
                    // Update the ValuesList - Note:  Key does not need to change
                    valuesList[valuesList.IndexOf(oldValue)] = value;
                }
                else
                {
                    // Add the item to both the keysList and the valuesList
                    keysList.Add(key);
                    valuesList.Add(value);
                }

                dictionary[key] = value;
            }
        }


        // ******************************************************
        // Methods
        // ******************************************************


        public virtual void Add(TKey key, TValue value)
        {
            // Add Key to List
            keysList.Add(key);

            // Add Value to List
            valuesList.Add(value);

            dictionary.Add(key, value);
        }


        public virtual bool Remove(TKey key)
        {
            // Find the Value in the Key List - and remove it if found
            keysList.Remove(key);

            // Find the Value in the Value List - and remove it if found
            TValue value;
            if (dictionary.TryGetValue(key, out value))
            {
                valuesList.Remove(value);
            }

            return dictionary.Remove(key);
        }


        public bool ContainsKey(TKey key)
        {
            return dictionary.ContainsKey(key);
        }


        public bool TryGetValue(TKey key, out TValue value)
        {
            return dictionary.TryGetValue(key, out value);
        }


        #endregion


        // ******************************************************************************
        // ICollection Members - NOT IMPLEMENTED
        // ******************************************************************************

        #region ICollection<KeyValuePair<TKey,TValue>> Members

        void ICollection<KeyValuePair<TKey, TValue>>.Add(KeyValuePair<TKey, TValue> item)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        void ICollection<KeyValuePair<TKey, TValue>>.Clear()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        bool ICollection<KeyValuePair<TKey, TValue>>.Contains(KeyValuePair<TKey, TValue> item)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        void ICollection<KeyValuePair<TKey, TValue>>.CopyTo(KeyValuePair<TKey, TValue>[] array, int arrayIndex)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        int ICollection<KeyValuePair<TKey, TValue>>.Count
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        bool ICollection<KeyValuePair<TKey, TValue>>.IsReadOnly
        {
            get { throw new Exception("The method or operation is not implemented."); }
        }

        bool ICollection<KeyValuePair<TKey, TValue>>.Remove(KeyValuePair<TKey, TValue> item)
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion


        // ******************************************************************************
        // IEnumerable Members - NOT IMPLEMENTED
        // ******************************************************************************

        #region IEnumerable<KeyValuePair<TKey,TValue>> Members

        IEnumerator<KeyValuePair<TKey, TValue>> IEnumerable<KeyValuePair<TKey, TValue>>.GetEnumerator()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion


        #region IEnumerable Members

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}
