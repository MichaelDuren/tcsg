using System.Collections;
using System.Data;
using System.Reflection;

namespace Util.Collections
{
    /// <summary>
    /// Class to support Hashtable functionality - but with a case insensitive string key (Extends Hashtable)
    /// </summary>
    public class CaseInsensitiveHashtable : Hashtable
    {

        // CollectionBase cb;

        /*
         * 
         * 
         * NOTE:  Although there is not much to this class - it is COMPLETE - and provides the desired functionality
         * 
         * 
         */


        // Creates the HashTable - but with Case Insensitive Attributes
        public CaseInsensitiveHashtable() : base(CaseInsensitiveHashCodeProvider.Default, CaseInsensitiveComparer.Default) { }


        // *****************************************************************
        // #region Constructors to convert other Types to HashTables
        // *****************************************************************

        public CaseInsensitiveHashtable(DataRow dataRow)
            : base(CaseInsensitiveHashCodeProvider.Default, CaseInsensitiveComparer.Default)
        {
            // Add the Columns of the DataRow to the hashtable
            foreach (DataColumn column in dataRow.Table.Columns)
            {
                string columnName = column.ColumnName;
                this.Add(columnName, dataRow[columnName]);
            }
        }

        public CaseInsensitiveHashtable(object o)
            : base(CaseInsensitiveHashCodeProvider.Default, CaseInsensitiveComparer.Default)
        {
            // Add the Columns of the DataRow to the hashtable
            foreach (PropertyInfo propertyInfo in o.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public))
            {
                this.Add(propertyInfo.Name, propertyInfo.GetValue(o, null));
            }
        }
    }

}
