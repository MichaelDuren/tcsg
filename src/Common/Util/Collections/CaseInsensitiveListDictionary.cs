using System.Collections;


namespace Util.Collections {

	/// <summary>
	/// List Dictionary - with String Keys (set to upper case - so that lookups are effectively case insensitive)
	/// </summary>
	public class CaseInsensitiveListDictionary : ListDictionaryBase { 

		// **************************************************
		// Properties
		// **************************************************

		// access by upper case string key
		public object this[string key] {
			get {return (object) this.InnerListDictionary[key.ToUpper()]; }
			set { this.InnerListDictionary[key.ToUpper()] = value; } 
		}

		// Keys
		public ICollection Keys {
			get {return this.InnerListDictionary.Keys;}
		}

		// Values
		public ICollection Values {
			get {return this.InnerListDictionary.Values;}
		}


		// **************************************************
		// Methods
		// **************************************************

		public void Add(string key, object o) { 
			this.InnerListDictionary.Add(key.ToUpper(), o); 
		} 
		
		public bool Contains(string key) {
			return this.InnerListDictionary.Contains(key.ToUpper());
		}

		public void Remove(string key) {
			this.InnerListDictionary.Remove(key.ToUpper());
		}

	}
}