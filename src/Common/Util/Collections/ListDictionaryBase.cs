using System;
using System.Collections;
using System.Collections.Specialized;

namespace Util.Collections {


			
	// ******************************************************************************
	// Inner ListDictionary Class - with Method Wrappers
	// ******************************************************************************

	/// <summary>
	/// Provides the abstract base class for a strongly typed collection of key-and-originalValue pairs - based on a ListDictionary.
	/// 
	/// For basic use, Extend this class, and implement the Item, Keys, Values properties and the Add, Contains, Remove methods.
	/// </summary>
	public abstract class ListDictionaryBase : IDictionary {

		/// <summary>
		/// Inner ListDictionary Class - with "Hooks" to allow validation
		/// </summary>
		private class _ListDictionary : ListDictionary, IDictionary {

			ListDictionaryBase ldBase;

			public _ListDictionary(ListDictionaryBase ldBase) {
				// Console.WriteLine("In Inner Constructor");
				this.ldBase = ldBase;
			}

			new public object this[object key] {
				get {
					// Console.WriteLine("In Inner Item Get");
					ldBase.OnGet(key);
					return base[key];
				}
				set {
					// Console.WriteLine("In Inner Item Set");
					ldBase.OnSet(key, base[key], value);
					base[key] = value;
				}
			}

			new public void Remove(object key) {
				// Console.WriteLine("In Inner Remove");
				ldBase.OnRemove(key);
				base.Remove(key);
			}

			new public bool Contains(object key) {
				// Console.WriteLine("In Inner Contains");
				return base.Contains(key);
			}

			new public void Add(object key, object value) {
				// Console.WriteLine("In Inner Add");
				ldBase.OnAdd(key, value);
				base.Add(key, value);
			}
		}



		private ListDictionary listDictionary;


		// ******************************************************************************
		// Constructors
		// ******************************************************************************
		
		public ListDictionaryBase() {
			listDictionary = new _ListDictionary(this);
		}


		// ******************************************************************************
		// ListDictionaryBase Members
		// ******************************************************************************

		public IDictionary InnerListDictionary {
			get {
				return listDictionary;
			}
		}

		protected virtual void OnAdd(Object key, Object value)  {
			// Console.WriteLine("In OnInsert");
		}

		protected virtual void OnRemove(Object key)  {
			// Console.WriteLine("In OnRemove");
		}

		protected virtual void OnGet(Object key)  {
			// Console.WriteLine("In OnValidate");
		}

		protected virtual void OnSet(Object key, Object oldValue, Object newValue)  {
			// Console.WriteLine("In OnSet");
		}


		// ******************************************************************************
		// IDictionary Members
		// ******************************************************************************

		// **************************************************
		// Not Implemented Properties - Can be implemented
		// in extending class
		// **************************************************

		bool IDictionary.IsReadOnly {
			get { return false; }
		}
		
		bool IDictionary.IsFixedSize {
			get { return false; }
		}

		object IDictionary.this[object key] {
			get {
				throw new NotImplementedException();
				// return null; 
			}
			set {
				throw new NotImplementedException();
			}
		}

		ICollection IDictionary.Keys {
			get {
				throw new NotImplementedException();
				// return null; 
			}
		}

		ICollection IDictionary.Values {
			get {
				throw new NotImplementedException();
				// return null; 
			}
		}


		// **************************************************
		// Implemented Methods
		// **************************************************

		public void Clear() {
			listDictionary.Clear();
		}

		public IDictionaryEnumerator GetEnumerator() {
			return listDictionary.GetEnumerator();
		}


		// **************************************************
		// Not Implemented Methods
		// **************************************************

		void IDictionary.Remove(object key) {
			throw new NotImplementedException();
		}

		bool IDictionary.Contains(object key) {
			throw new NotImplementedException();
			// return false;
		}

		void IDictionary.Add(object key, object value) {
			throw new NotImplementedException();
		}


		// ******************************************************************************
		// ICollection Members
		// ******************************************************************************

		// **************************************************
		// Implemented Properties
		// **************************************************

		public int Count {
			get { return listDictionary.Count; }
		}

		bool ICollection.IsSynchronized {
			get { return listDictionary.IsSynchronized; }
		}

		object ICollection.SyncRoot {
			get { return listDictionary.SyncRoot; }
		}


		// **************************************************
		// Methods
		// **************************************************
		
		public void CopyTo(Array array, int index) {
			listDictionary.CopyTo(array, index);
		}


		// ******************************************************************************
		// IEnumerable Members - NOT IMPLEMENTED
		// ******************************************************************************

		IEnumerator IEnumerable.GetEnumerator() {
			throw new NotImplementedException();
			// return null;
		}

	}
}
