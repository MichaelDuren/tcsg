using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Util.Collections;

namespace Util
{
    /// <summary>
    /// Tools to make various reflection tasks faster / easier
    /// </summary>
    /// <author>Jim Polizzi</author>
    public static class ReflectionTools
    {
        // ***************************************************************************************
        #region Property Or Field Methods
        // ***************************************************************************************

        /// <summary>
        /// Sets the Value in the specified field of the object
        /// </summary>
        /// <param name="persistableObject"></param>
        /// <param name="ht"></param>
        /// <param name="fieldName"></param>
        public static void SetPropertyOrFieldValue(object o, string fieldName, object value)
        {
            // Get the PropertyInfo object for the column
            PropertyInfo propertyInfo = o.GetType().GetProperty(fieldName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);

            // See if the fieldName is a Property.  if yes, then process as a Property, if not, then try it as a Field
            if (propertyInfo != null)
            {
                // Check for nulls - and make sure that the type can handle nulls - if not, then do nothing (leaving the field unchanged)
                if (value == null && !IsNullable(propertyInfo.PropertyType)) { return; }

                // Clean Up Data Type of originalValue - to match the property data type (if possible)
                value = FixDataType(value, propertyInfo.PropertyType);

                // Set the column's originalValue
                propertyInfo.SetValue(o, value, null);
            }
            else
            {
                // Get the FieldInfo object for the column
                FieldInfo fieldInfo = o.GetType().GetField(fieldName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);

                // Check for nulls - and make sure that the type can handle nulls - if not, then do nothing (leaving the field unchanged)
                if (value == null && !IsNullable(fieldInfo.FieldType)) { return; }

                // Clean Up Data Type of originalValue - to match the property data type (if possible)
                value = FixDataType(value, fieldInfo.FieldType);

                // Set the column's originalValue
                fieldInfo.SetValue(o, value);
            }
        }

        /// <summary>
        /// Gets the Value of the specified field of the object
        /// </summary>
        /// <param name="persistableObject"></param>
        /// <param name="fieldName"></param>
        /// <returns></returns>
        public static object GetPropertyOrFieldValue(object o, string fieldName)
        {
            // Get the PropertyInfo object for the column
            PropertyInfo propertyInfo = o.GetType().GetProperty(fieldName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);

            // See if the fieldName is a Property.  if yes, then process as a Property, if not, then try it as a Field
            if (propertyInfo != null)
            {
                // Get the column's originalValue
                return propertyInfo.GetValue(o, null);
            }
            else
            {
                // Get the FieldInfo object for the column
                FieldInfo fieldInfo = o.GetType().GetField(fieldName, BindingFlags.Instance | BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);

                // Get the column's originalValue
                return fieldInfo.GetValue(o);
            }
        }

        /// <summary>
        /// Determines if the type is able to to be null.  ie. either a reference type, or a NullableType
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsNullable(Type type)
        {
            if (!type.IsPrimitive)
            {
                return true;
            }
            return (IsNullableType(type));
        }

        /// <summary>
        /// Determines if the type is Nullable<>  ie. if it is a Primitive that is flagged as Nullable (ex. if it is an "int?")
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        public static bool IsNullableType(Type type)
        {
            return (type.IsGenericType && type.GetGenericTypeDefinition().Equals(typeof(Nullable<>)));
        }

        #endregion

        // ***************************************************************************************
        #region Property Methods
        // ***************************************************************************************

        public static bool CheckPublicPropertyExists(object o, string propertyName)
        {
            PropertyInfo propertyInfo = GetProperty(o, propertyName);
            if (propertyInfo == null)
            {
                return false;
            }
            return true;
        }

        public static bool CheckPublicPropertyExists(object o, string propertyName, BindingFlags bindingAttr)
        {
            PropertyInfo propertyInfo = GetProperty(o, propertyName, bindingAttr);
            if (propertyInfo == null)
            {
                return false;
            }
            return true;
        }

        public static object GetPropertyValue(object o, string propertyName)
        {
            return GetPropertyValue(o, propertyName, BindingFlags.Instance | BindingFlags.Public);
        }

        public static object GetPropertyValue(object o, string propertyName, BindingFlags bindingAttr)
        {
            PropertyInfo propertyInfo = GetProperty(o, propertyName, bindingAttr);
            if (propertyInfo == null)
            {
                throw new NullReferenceException("Property: " + propertyName + " does not exist in object of type: " + o.GetType().FullName);
            }
            else if (!propertyInfo.CanRead)
            {
                throw new ArgumentException(string.Format("The Property ({0}) must be readable", propertyName));
            }

            try
            {
                return propertyInfo.GetValue(o, null);
            }
            catch (Exception e)
            {
                throw new Exception(string.Format("Exception occurred Getting Property Value for {0} in object of type: {1}", propertyName, o.GetType().FullName), e);
            }
        }

        public static void SetPropertyValue(object o, string propertyName, object value)
        {
            SetPropertyValue(o, propertyName, value, BindingFlags.Instance | BindingFlags.Public, false, false);
        }

        public static void SetPropertyValue(object o, string propertyName, object value, BindingFlags bindingAttr)
        {
            SetPropertyValue(o, propertyName, value, bindingAttr, false, false);
        }

        public static void SetPropertyValue(object o, string propertyName, object value, bool ignoreNonExisting, bool ignoreUnWritable)
        {
            SetPropertyValue(o, propertyName, value, BindingFlags.Instance | BindingFlags.Public, ignoreNonExisting, ignoreUnWritable);
        }

        public static void SetPropertyValue(object o, string propertyName, object value, BindingFlags bindingAttr, bool ignoreNonExisting, bool ignoreUnWritable)
        {
            PropertyInfo propertyInfo = GetProperty(o, propertyName, bindingAttr);
            if (propertyInfo == null)
            {
                if (ignoreNonExisting) { return; }
                throw new NullReferenceException("Property: " + propertyName + " does not exist in object of type: " + o.GetType().FullName);
            }
            else if (!propertyInfo.CanWrite)
            {
                if (ignoreUnWritable) { return; }
                throw new ArgumentException(string.Format("The Property ({0}) must be writeable", propertyName));
            }

            try
            {
                // Clean Up Data Type of originalValue - to match the property data type (if possible)
                value = FixDataType(value, propertyInfo.PropertyType);

                // Set the Properties originalValue
                propertyInfo.SetValue(o, value, null);
            }
            catch (Exception e)
            {
                throw new Exception(string.Format("Exception occurred Setting Property Value for {0},  Value: {1} in object of type: {2}", propertyName, value, o.GetType().FullName), e);
            }
        }


        /// <summary>
        /// Searches for the public property with the specified name.
        /// </summary>
        public static PropertyInfo GetProperty(object o, string propertyName)
        {
            return o.GetType().GetProperty(propertyName);
        }

        /// <summary>
        /// Searches for the specified property, using the specified binding constraints. 
        /// </summary>
        public static PropertyInfo GetProperty(object o, string propertyName, BindingFlags bindingAttr)
        {
            return o.GetType().GetProperty(propertyName, bindingAttr);
        }

        #endregion

        // ***************************************************************************************
        #region Field Methods
        // ***************************************************************************************


        #endregion


        // ***************************************************************************************
        #region Get / Set Object Properties as Dictionary methods
        // ***************************************************************************************

        /// <summary>
        /// Returns the values all of the public properties of an object as a CaseInsensitiveHashTable
        /// </summary>
        public static IDictionary GetObjectPropertyValues(object sourceObject)
        {
            CaseInsensitiveHashtable ht = new CaseInsensitiveHashtable();
            foreach (PropertyInfo propertyInfo in sourceObject.GetType().GetProperties(BindingFlags.Instance | BindingFlags.Public))
            {
                ht.Add(propertyInfo.Name, propertyInfo.GetValue(sourceObject, null));
            }
            return ht;
        }

        /// <summary>
        /// Sets the values of public Properties in an object from the values in the Dictionary.  Only properties with keys in the dictionary are set.
        /// Optionally, ignores (ie. does not set) any values specified in ignoreKeys
        /// </summary>
        public static void SetObjectPropertyValues(object targetObject, IDictionary dictionary, params string[] ignoreKeys)
        {
            // Optimize if no keys specified
            if (ignoreKeys.GetLength(0) == 0)
            {
                foreach (DictionaryEntry de in dictionary)
                {
                    SetPropertyValue(targetObject, de.Key.ToString(), de.Value, true, true);
                }
            }
            else
            {
                List<string> ignoreList = new List<string>(ignoreKeys);

                foreach (DictionaryEntry de in dictionary)
                {
                    string key = de.Key.ToString();

                    if (ignoreList.Contains(key)) { continue; }

                    SetPropertyValue(targetObject, key, de.Value, true, true);
                }
            }
        }


        /// <summary>
        /// Sets the values of public Properties in the target object from the values in source object.
        /// Optionally, ignores (ie. does not set) any values specified in ignoreKeys
        /// </summary>
        public static void SetObjectPropertyValues(object targetObject, object sourceObject, params string[] ignoreKeys)
        {
            IDictionary dictionary = GetObjectPropertyValues(sourceObject);
            SetObjectPropertyValues(targetObject, dictionary, ignoreKeys);
        }

        #endregion

        // ******************************************************************************
        #region Set Value Support Methods
        // ******************************************************************************

        /// <summary>
        /// Method to Clean up - as necessary - the passed in originalValue to match the targetType specified.
        /// Does all required data type conversions.  Throws an exception if conversion is not possible or fails.
        /// </summary>
        /// <param name="originalValue"></param>
        public static object FixDataType(object value, Type targetType)
        {
            object fixedValue = value;

            if (targetType == typeof(string))
            {
                if (value != null)
                {
                    fixedValue = value.ToString();
                }
                else
                {
                    fixedValue = null;
                }
            }

            else if ((targetType == typeof(Int16) || targetType == typeof(Int32)))
            {
                fixedValue = StringTools.GetInt32(value);
            }

            else if ((targetType == typeof(Int16?) || targetType == typeof(Int32?)))
            {
                fixedValue = StringTools.GetNullableInt32(value);
            }

            else if (targetType == typeof(bool))
            {
                fixedValue = StringTools.GetBoolean(value);
            }

            else if (targetType == typeof(bool?))
            {
                fixedValue = StringTools.GetNullableBoolean(value);
            }

            else if (targetType == typeof(DateTime) || targetType == typeof(DateTime?))
            {
                fixedValue = StringTools.GetDateTime(value);
            }

            else if (targetType == typeof(Int64))
            {
                fixedValue = StringTools.GetInt64(value);
            }

            else if (targetType == typeof(Int64?))
            {
                fixedValue = StringTools.GetNullableInt32(value);
            }

            else if (targetType == typeof(decimal))
            {
                fixedValue = StringTools.GetDecimal(value);
            }

            else if (targetType == typeof(decimal?))
            {
                fixedValue = StringTools.GetNullableDecimal(value);
            }

            else if (targetType == typeof(double))
            {
                fixedValue = StringTools.GetDouble(value);
            }

            else if (targetType == typeof(double?))
            {
                fixedValue = StringTools.GetNullableDouble(value);
            }

            else if (targetType == typeof(byte) || targetType == typeof(byte?))
            {
                if (value == null) { fixedValue = null; }
                else { fixedValue = Byte.Parse(value.ToString()); }
            }

            else if (targetType == typeof(object))
            {
                // Do nothing - we already set fixedValue to originalValue above.
            }
            else if (targetType.IsEnum)
            {
                // An Enum should work just fine - it's basically an int
                // Do nothing - we already set fixedValue to originalValue above.
            }
            // Look for Nullable Enum
            else if (
                targetType.IsGenericType &&
                targetType.GetGenericTypeDefinition() == typeof(Nullable<>) &&
                targetType.GetGenericArguments()[0].BaseType == typeof(Enum)
                )
            {
                if (value == null)
                {
                    // A Nullable Enum should work just fine - it's basically an int?
                    // Do nothing - we already set fixedValue to originalValue above.
                }
                else
                {
                    Type enumType = targetType.GetGenericArguments()[0];
                    fixedValue = Enum.ToObject(enumType, value);
                }
            }

            /*
            // If the type is nullable, then get the Underlying Type
            else if (type.IsGenericType && type.GetGenericTypeDefinition().Equals(typeof(Nullable<>)))
            {
                System.ComponentModel.NullableConverter nc = new System.ComponentModel.NullableConverter(type);
                Type underlyingType = nc.UnderlyingType;
                SetValue(originalValue, underlyingType);
            }
            */

            else
            {
                // INVALID - did NOT match the above types
                throw new ArgumentOutOfRangeException(targetType.FullName, string.Format(
                    "Invalid TargetType: {0} for Value Specified: {1} ({2})",
                    targetType, value, value.GetType().ToString()));
            }

            return fixedValue;
        }

        #endregion

        // ***************************************************************************************
        #region Misc Methods
        // ***************************************************************************************

        public static object GetObjectInstance(string assemblyDllPath, string fullyQualifiedClassName)
        {
            // Attempt to get the Assembly
            Assembly assemblyObject = Assembly.LoadFrom(assemblyDllPath);

            // Attempt to Instantiate the Entity Class
            object entityObject = assemblyObject.CreateInstance(fullyQualifiedClassName);
            if (entityObject == null)
            {
                throw new Exception(string.Format("Could not find Class: {0} in Assembly: {1}", fullyQualifiedClassName, assemblyObject.FullName));
            }
            return entityObject;
        }


        /// <summary>
        /// Copies an object - using Binary Serialization
        /// </summary>
        public static T CopyObject<T>(T sourceObject)
        {
            IFormatter formatter = new BinaryFormatter();
            MemoryStream memoryStream = new MemoryStream();

            formatter.Serialize(memoryStream, sourceObject);
            memoryStream.Seek(0, SeekOrigin.Begin);

            T copy = (T)formatter.Deserialize(memoryStream);

            return copy;
        }

        #endregion
    }
}
