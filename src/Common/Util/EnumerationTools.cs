using System;
using System.Collections.Generic;

namespace Util
{
    /// <summary>
    /// Tools for working with Enumerations
    /// </summary>
    public static class EnumerationTools
    {
        /// <summary>
        /// Returns the Enum Value Names as a List of string
        /// </summary>
        public static List<string> GetEnumNames<TEnum>()
        {
            if (!typeof(TEnum).IsEnum)
            {
                throw new ArgumentException("TEnum Must be an Enum", "TEnum");
            }

            List<string> list = new List<string>();

            foreach (TEnum item in Enum.GetValues(typeof(TEnum)))
            {
                list.Add(item.ToString());
            }

            return list;
        }

        /// <summary>
        /// Returns the Enum Values as a List
        /// </summary>
        public static List<TEnum> GetEnumValues<TEnum>()
        {
            if (!typeof(TEnum).IsEnum)
            {
                throw new ArgumentException("TEnum Must be an Enum", "TEnum");
            }

            List<TEnum> list = new List<TEnum>();

            foreach (TEnum item in Enum.GetValues(typeof(TEnum)))
            {
                list.Add(item);
            }

            return list;
        }

        public static bool EnumContains<TEnum>(TEnum value)
        {
            if (!typeof(TEnum).IsEnum)
            {
                throw new ArgumentException("TEnum Must be an Enum", "TEnum");
            }

            List<TEnum> list = GetEnumValues<TEnum>();

            return list.Contains(value);
        }

        /// <summary>
        /// Does an Enum Parse - but deals with the casts
        /// </summary>
        public static TEnum Parse<TEnum>(string stringValue)
        {
            return (TEnum)Enum.Parse(typeof(TEnum), stringValue);
        }

    }
}
