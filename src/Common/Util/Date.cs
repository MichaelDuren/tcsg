﻿using System;
using System.Xml.Serialization;

namespace Util
{
    /// <summary>
    /// Class to represent a Date.
    /// </summary>
    [Serializable]
    public class Date
    {
        // ********************************************************************************
        #region Properties, etc.
        // ********************************************************************************

        private int _year;
        private int _month;
        private int _day;

        [XmlAttribute]
        public int Year
        {
            get { return _year; }
            set { _year = value; }
        }

        [XmlAttribute]
        public int Month
        {
            get { return _month; }
            set
            {
                if (value < 1 || value > 12)
                {
                    throw new ArgumentOutOfRangeException("Month", value, "Month must be between 1 and 12");
                }

                int originalValue = _month;
                _month = value;

                if (Day > 0)
                {
                    try
                    {
                        // Validate by attempting to convert to a DateTime
                        ToDateTime();
                    }
                    catch
                    {
                        // Set back to previous value
                        _month = originalValue;
                        throw new ArgumentOutOfRangeException("Month", value, string.Format("The month ({0}) provided results in an invalid date based on the day provided ({1}).", Month, Day));
                    }
                }
            }
        }

        [XmlAttribute]
        public int Day
        {
            get { return _day; }
            set
            {
                // Forcing validation of the month.  The month must be set before the day can be set
                Month = Month;

                int originalValue = Day;
                _day = value;

                try
                {
                    // Validate by attempting to convert to a DateTime
                    ToDateTime();
                }
                catch
                {
                    // Set back to previous value
                    _day = originalValue;
                    throw new ArgumentOutOfRangeException("Day", value, string.Format("The day ({0}) provided results in an invalid date based on the month ({1}) and year ({2}) provided.", Day, Month, Year));
                }
            }
        }

        /// <summary>
        /// Get the current date.
        /// </summary>
        public static Date Today
        {
            get
            {
                return new Date(DateTime.Today);
            }
        }

        #endregion

        // ********************************************************************************
        #region Constructors and Related
        // ********************************************************************************

        private Date() { }

        /// <summary>
        /// Create date object from string value.  Uses standard DateTime.Parse() logic.
        /// </summary>
        public Date(string value)
        {
            Date date = Parse(value);
            Year = date.Year;
            Month = date.Month;
            Day = date.Day;
        }

        /// <summary>
        /// Create date object from year, month, day
        /// </summary>
        public Date(int year, int month, int day)
        {
            Year = year;
            Month = month;
            Day = day;
        }

        /// <summary>
        /// Create date object from DateTime (just using Year, Month, Day).
        /// </summary>
        public Date(DateTime dateTime)
        {
            Year = dateTime.Year;
            Month = dateTime.Month;
            Day = dateTime.Day;
        }

        #endregion

        // ********************************************************************************
        #region Public Methods
        // ********************************************************************************

        /// <summary>
        /// Returns a new Date that adds the specified number of days to the value fo this instance.
        /// </summary>
        public Date AddDays(double days)
        {
            DateTime dateTime = ToDateTime();
            dateTime = dateTime.AddDays(days);
            return new Date(dateTime);
        }

        /// <summary>
        /// Returns a new Date that adds the specified number of months to the value fo this instance.
        /// </summary>
        public Date AddMonths(int months)
        {
            DateTime dateTime = ToDateTime();
            dateTime = dateTime.AddMonths(months);
            return new Date(dateTime);
        }

        /// <summary>
        /// Returns a new Date that adds the specified number of years to the value fo this instance.
        /// </summary>
        public Date AddYears(int years)
        {
            DateTime dateTime = ToDateTime();
            dateTime = dateTime.AddYears(years);
            return new Date(dateTime);
        }

        public bool Between(Date startDate, Date endDate)
        {
            if (this >= startDate && this <= endDate)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        #endregion

        // ********************************************************************************
        #region Convert Methods
        // ********************************************************************************

        /// <summary>
        /// Convert current Date object to a DateTime (the Time will be 00:00:00).
        /// </summary>
        /// <returns></returns>
        public DateTime ToDateTime()
        {
            return new DateTime(this.Year, this.Month, this.Day);
        }

        /// <summary>
        /// Parse string to a Date.
        /// </summary>
        public static Date Parse(string value)
        {
            Guard.ArgumentNotNull(value, "value");

            try
            {
                DateTime dateTime = DateTime.Parse(value);
                return new Date(dateTime);
            }
            catch 
            {
                throw new ArgumentOutOfRangeException("Error parsing date: " + value);
            }
        }

        /// <summary>
        /// Calculate date difference in days between two Date objects.
        /// </summary>
        public static int DateDiffDays(Date date1, Date date2)
        {
            Guard.ArgumentNotNull(date1, "date1");
            Guard.ArgumentNotNull(date2, "date2");

            DateTime dt1 = date1.ToDateTime();
            DateTime dt2 = date2.ToDateTime();

            TimeSpan ts = dt1 - dt2;
            int daysDiff = (int)Math.Round(ts.TotalDays, 0);
            return daysDiff;
        }

        /// <summary>
        /// Convert current Date object to string.
        /// </summary>
        public override string ToString()
        {
            return String.Format("{0:d}", ToDateTime());
        }

        #endregion

        // ********************************************************************************
        #region Comparison Operators
        // ********************************************************************************

        public static bool operator >(Date d1, Date d2)
        {
            if ((object)d1 == null || (object)d2 == null) { return false; }
            if (DateDiffDays(d1, d2) > 0) { return true; }
            else { return false; }
        }

        public static bool operator <(Date d1, Date d2)
        {
            if ((object)d1 == null || (object)d2 == null) { return false; }
            if (DateDiffDays(d1, d2) < 0) { return true; }
            else { return false; }
        }

        public static bool operator >=(Date d1, Date d2)
        {
            if ((object)d1 == null || (object)d2 == null) { return false; }
            if (DateDiffDays(d1, d2) >= 0) { return true; }
            else { return false; }
        }

        public static bool operator <=(Date d1, Date d2)
        {
            if ((object)d1 == null || (object)d2 == null) { return false; }
            if (DateDiffDays(d1, d2) <= 0) { return true; }
            else { return false; }
        }

        public static bool operator ==(Date d1, Date d2)
        {
            if ((object)d1 == null && (object)d2 == null) { return true; }
            if ((object)d1 == null || (object)d2 == null) { return false; }
            if (DateDiffDays(d1, d2) == 0) { return true; }
            else { return false; }
        }

        public static bool operator !=(Date d1, Date d2)
        {
            if ((object)d1 == null && (object)d2 == null) { return false; }
            if ((object)d1 == null || (object)d2 == null) { return false; }
            if (DateDiffDays(d1, d2) != 0) { return true; }
            else { return false; }
        }

        #endregion
    }
}
