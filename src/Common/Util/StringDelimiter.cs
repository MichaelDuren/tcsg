using System;
using System.Collections;
using System.Text;

namespace Util
{
    /// <summary>
    /// Utility to create delimited Strings.  
    /// Operates like StringBuilder - but creates Delimited strings using the specified delimiter
    /// Note: The defalut delimiter is: StringDelimiter.COMMA
    /// All items but the LAST have a delimiter after it.
    /// </summary>
    public sealed class StringDelimiter
    {
        private String delimiter;
        private StringBuilder sb = new StringBuilder();
        private bool isFirst = true;

        // ***************************************************
        // Delimiter options
        // ***************************************************

        public const string COMMA = ",";
        public const string COMMA_SPACE = ", ";
        public const string COMMA_NEWLINE = ",\n";
        public const string TAB = "\t";
        public const string NEWLINE = "\n";

        // ******************************************************************************
        // Properties
        // ******************************************************************************

        public bool IsEmpty
        {
            get
            {
                if (sb.Length == 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        /// <summary>
        /// If true - then a blank or null value will be ignored (ie. the value - and it's delimiter will NOT be created)
        /// </summary>
        public bool IgnoreBlankOrNull { get; set; }

        // ******************************************************************************
        // Constructors
        // ******************************************************************************

        /// <summary>
        /// Default constructor for StringDelimiter.  Default delimiter is StringDelimiter.COMMA
        /// </summary>
        public StringDelimiter()
        {
            this.delimiter = COMMA;
        }

        /// <summary>
        /// </summary>
        /// <param name="delimiter">- specify what to use for a delimiter
        /// </param>
        public StringDelimiter(string delimiter)
        {
            this.delimiter = delimiter;
        }

        public StringDelimiter(string delimiter, bool ignoreBlankOrNull)
            : this(delimiter)
        {
            this.IgnoreBlankOrNull = ignoreBlankOrNull;
        }

        // ******************************************************************************
        // Methods
        // ******************************************************************************	

        /// <summary>
        /// Adds a String to the Delimited String - handles delimiting
        /// </summary>
        /// <param name="s">String to add</param>
        public void Add(string s)
        {
            // Handle Ignore blank or Null
            if (IgnoreBlankOrNull && StringTools.IsBlankOrNull(s))
            {
                return;
            }

            // Add the delimiter before all items but the first
            if (isFirst == true)
            {
                isFirst = false;
            }
            else
            {
                sb.Append(delimiter);
            }

            sb.Append(s);
        }

        public void Add(object o)
        {
            this.Add(o.ToString());
        }

        public void Add(string format, params object[] args)
        {
            this.Add(string.Format(format, args));
        }

        /// <summary>
        /// Takes a list - and adds the ToString of each of it's items to the string delimiter
        /// </summary>
        /// <param name="list"></param>
        public void AddList(IList list)
        {
            foreach (object o in list)
            {
                if (o == null)
                {
                    this.Add(null);
                }
                else if (o is IList)
                {
                    AddList((IList)o);
                }
                //
                // THE FOLLOWING IS UNSAFE!!!
                //
                //else if (o is IEnumerable)
                //{
                //    foreach (object item in (IEnumerable)o)
                //    {
                //        Add(item.ToString());
                //    }
                //}
                else
                {
                    this.Add(o.ToString());
                }
            }
        }

        /// <summary>
        /// Returns the String originalValue of the Delimited String
        /// </summary>
        public override string ToString()
        {
            return sb.ToString();
        }

        /// <summary>
        /// Clears the Delimited String
        /// </summary>
        public void Clear()
        {
            sb.Length = 0;
            isFirst = true;
        }

        // ******************************************************************************
        // Static Methods
        // ******************************************************************************

        /// <summary>
        /// Concatenates two or more values - separated by the specified separater.
        /// Handles/allows null values.
        /// </summary>
        public static string Concatenate(string separater, params object[] values)
        {
            return Concatenate(separater, false, values);
        }

        /// <summary>
        /// Concatenates two or more values - separated by the specified separater.
        /// Handles/allows null values.
        /// </summary>
        public static string Concatenate(string separater, bool ignoreBlankOrNull, params object[] values)
        {
            StringDelimiter sd = new StringDelimiter(separater, ignoreBlankOrNull);

            sd.AddList(values);
            return sd.ToString();
        }

        ///// <summary>
        ///// Concatenates two or more values - separated by the specified separater.
        ///// Handles/allows null values.
        ///// </summary>
        //public static string Concatenate(string separater, IList list)
        //{
        //    return Concatenate(separater, false, list);
        //}

        ///// <summary>
        ///// Concatenates two or more values - separated by the specified separater.
        ///// Handles/allows null values.
        ///// </summary>
        //public static string Concatenate(string separater, bool ignoreBlankOrNull, IList list)
        //{
        //    StringDelimiter sd = new StringDelimiter(separater, ignoreBlankOrNull);

        //    sd.AddList(list);
        //    return sd.ToString();
        //}
    
    }
}