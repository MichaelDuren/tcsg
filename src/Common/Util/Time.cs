﻿//2012.12.01 - Complete - except Time Zone methods

//Original source came from http://www.csharphelp.com/2007/09/c-time-class/

using System;
using System.Xml.Serialization;

namespace Util
{
    [Serializable]
    public class Time
    {
        // ********************************************************************************
        #region Properties, etc.
        // ********************************************************************************

        public const char TIME_SEPERATOR = ':';

        private int _hour;
        private int _minute;
        private int _second;

        [XmlAttribute]
        public int Hour
        {
            get { return _hour; }
            set
            {
                if (value < 0 || value > 23)
                {
                    throw new ArgumentOutOfRangeException("Hour", value, "Hour must be between 0 and 23");
                }
                _hour = value;
            }
        }

        [XmlAttribute]
        public int Minute
        {
            get { return _minute; }
            set
            {
                if (value < 0 || value > 59)
                {
                    throw new ArgumentOutOfRangeException("Minute", value, "Minute must be between 0 and 59");
                }
                _minute = value;
            }
        }

        [XmlAttribute]
        public int Second
        {
            get { return _second; }
            set
            {
                if (value < 0 || value > 59)
                {
                    throw new ArgumentOutOfRangeException("Second", value, "Second must be between 0 and 59");
                }
                _second = value;
            }
        }

        #endregion

        // ********************************************************************************
        #region Constructors
        // ********************************************************************************

        /// <summary>
        /// Create time object from current system time.
        /// </summary>
        public Time()
        {
            Hour = DateTime.Now.Hour;
            Minute = DateTime.Now.Minute;
            Second = DateTime.Now.Second;
        }

        /// <summary>
        /// Create time object from string value must be seperated as TIME_SEPERATOR constant.
        /// </summary>
        public Time(string value)
        {
            string[] vals = value.Split(TIME_SEPERATOR);
            Hour = int.Parse(vals[0]);
            Minute = int.Parse(vals[1]);

            if (vals.Length > 2)
            {
                Second = int.Parse(vals[2]);
            }
        }

        /// <summary>
        /// Create time object from parameters hour, minute and seconds.
        /// </summary>
        public Time(int hour, int minute, int second)
        {
            Hour = hour;
            Minute = minute;
            Second = second;
        }

        /// <summary>
        /// Create time object from a specified DateTime (just using Hour, Minute, Second)
        /// </summary>
        public Time(DateTime dateTime)
        {
            Hour = dateTime.Hour;
            Minute = dateTime.Minute;
            Second = dateTime.Second;
        }

        /// <summary>
        /// Create time object from seconds.
        /// </summary>
        /// <param name="seconds"></param>
        public Time(int seconds)
        {
            Minute = seconds / 60;
            Second = seconds % 60;

            Hour = Minute / 60;
            Minute = Minute % 60;
        }

        #endregion

        // ********************************************************************************
        #region Public methods
        // ********************************************************************************

        /// <summary>
        /// Add new time object and addition (+) it to previus time object.
        /// </summary>
        public Time Add(Time time)
        {
            this.Hour += time.Hour;
            this.Minute += time.Minute;
            this.Second += time.Second;

            return new Time(GetStringTime(this.ToSeconds()));
        }

        /// <summary>
        /// Add new string value and addition (+) it to previus time object.
        /// </summary>
        public Time Add(string value)
        {
            return Add(new Time(value));
        }

        /// <summary>
        /// Determines if the specified Time is between (inclusive) the start and end time
        /// </summary>
        public bool Between(Time startTime, Time endTime)
        {
            if (this >= startTime && this <= endTime)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Get current system time.
        /// </summary>
        public static Time Now()
        {
            DateTime dt = DateTime.Now;
            return GetTimeFromSeconds(ToSeconds(dt));
        }


        /// <summary>
        /// Calculate time difference in seconds between two time objects.
        /// </summary>
        public static int TimeDiffSeconds(Time time1, Time time2)
        {
            int _secs1 = time1.ToSeconds();
            int _secs2 = time2.ToSeconds();

            int _secs = _secs1 - _secs2;

            return _secs;
        }

        /// <summary>
        /// Calculate time difference between two time objects.
        /// </summary>
        public static Time TimeDiff(Time time1, Time time2)
        {
            return GetTimeFromSeconds(TimeDiffSeconds(time1, time2));
        }

        /// <summary>
        /// Calculate time difference between two string values.
        /// </summary>
        public static Time TimeDiff(string time1, string time2)
        {
            try
            {
                Time t1 = new Time(time1);
                Time t2 = new Time(time2);
                return TimeDiff(t1, t2);
            }
            catch (Exception e)
            {
                throw new Exception(string.Format("Exception occurred doing TimeDiff for: {0} / {1}", time1, time2), e);
            }
        }

        /// <summary>
        /// Calculate time difference between two DateTime objects.
        /// </summary>
        public static Time TimeDiff(DateTime dateTime1, DateTime dateTime2)
        {
            try
            {
                TimeSpan span = dateTime1 - dateTime2;
                return new Time(span.Seconds);
            }
            catch (Exception e)
            {
                throw new Exception(string.Format("Exception occurred doing TimeDiff for: {0} / {1}", dateTime1, dateTime2), e);
            }
        }

        /// <summary>
        /// Calculate time difference between two second values.
        /// </summary>
        public static Time TimeDiff(int seconds1, int seconds2)
        {
            Time t1 = new Time(seconds1);
            Time t2 = new Time(seconds2);
            return TimeDiff(t1, t2);
        }

        #endregion

        // ********************************************************************************
        #region Convert methods
        // ********************************************************************************

        /// <summary>
        /// Convert current time object to seconds.
        /// </summary>
        public int ToSeconds()
        {
            return this.Hour * 3600 + this.Minute * 60 + this.Second;
        }

        /// <summary>
        /// Convert DateTime object to seconds.
        /// </summary>
        public static int ToSeconds(DateTime dateTime)
        {
            return dateTime.Hour * 3600 + dateTime.Minute * 60 + dateTime.Second;
        }

        /// <summary>
        /// Convert current time object to string.
        /// </summary>
        public override string ToString()
        {
            return String.Format("{0:00}:{1:00}:{2:00}", Hour, Minute, Second);
        }

        /// <summary>
        /// Convert seconds to time object.
        /// </summary>
        public static Time GetTimeFromSeconds(int seconds)
        {
            int _mins = seconds / 60;
            seconds = seconds % 60;

            int _hours = _mins / 60;
            _mins = _mins % 60;

            return new Time(_hours, _mins, seconds);
        }

        /// <summary>
        /// Convert seconds to string time.
        /// </summary>
        private string GetStringTime(int seconds)
        {
            int _mins = seconds / 60;
            seconds = seconds % 60;

            int _hours = _mins / 60;
            _mins = _mins % 60;

            this.Hour = _hours;
            this.Minute = _mins;
            this.Second = seconds;

            return String.Format("{0:00}:{1:00}:{2:00}", _hours, _mins, seconds); ;
        }

        /// <summary>
        /// Parse string to time.
        /// </summary>
        public static Time Parse(string value)
        {
            try
            {
                return new Time(value);
            }
            catch
            {
                throw new ApplicationException("Error parsing time: " + value);
            }
        }

        #endregion

        // ********************************************************************************
        #region Add / Subtract time objects
        // ********************************************************************************

        public static Time operator +(Time t1, Time t2)
        {
            Time t3 = new Time(t1.Hour, t1.Minute, t1.Second);
            t3.Add(t2);
            return t3;
        }

        public static Time operator -(Time t1, Time t2)
        {
            return TimeDiff(t1, t2);
        }

        #endregion

        // ********************************************************************************
        #region Comparison Operators
        // ********************************************************************************

        public static bool operator >(Time t1, Time t2)
        {
            if ((object)t1 == null || (object)t2 == null) { return false; }
            if (TimeDiffSeconds(t1, t2) > 0) { return true; }
            else { return false; }
        }

        public static bool operator <(Time t1, Time t2)
        {
            if ((object)t1 == null || (object)t2 == null) { return false; }
            if (TimeDiffSeconds(t1, t2) < 0) { return true; }
            else { return false; }
        }

        public static bool operator >=(Time t1, Time t2)
        {
            if ((object)t1 == null || (object)t2 == null) { return false; }
            if (TimeDiffSeconds(t1, t2) >= 0) { return true; }
            else { return false; }
        }

        public static bool operator <=(Time t1, Time t2)
        {
            if ((object)t1 == null || (object)t2 == null) { return false; }
            if (TimeDiffSeconds(t1, t2) <= 0) { return true; }
            else { return false; }
        }

        public static bool operator ==(Time t1, Time t2)
        {
            if ((object)t1 == null && (object)t2 == null) { return true; }
            if ((object)t1 == null || (object)t2 == null) { return false; }
            if (TimeDiffSeconds(t1, t2) == 0) { return true; }
            else { return false; }
        }

        public static bool operator !=(Time t1, Time t2)
        {
            if ((object)t1 == null && (object)t2 == null) { return false; }
            if ((object)t1 == null || (object)t2 == null) { return true; }
            if (TimeDiffSeconds(t1, t2) != 0) { return true; }
            else { return false; }
        }

        #endregion

    }
}
