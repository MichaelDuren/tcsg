﻿//2012.08.25 - Complete

using System.Collections.Generic;

namespace Util.Data
{
    public class DataListRows
    {
        private readonly DataList parentDataList;

        private List<DataListRow> internalList = new List<DataListRow>();

        public DataListRow this[int index]
        {
            get
            {
                return internalList[index];
            }
        }

        public int Count
        {
            get
            {
                return internalList.Count;
            }
        }

        private DataListRows() { }

        internal DataListRows(DataList parentDataList)
        {
            this.parentDataList = parentDataList;
        }

        public DataListRow Add(object[] rowData)
        {
            Guard.FailIfTrue(rowData.Length != parentDataList.columnCount, "rowData must have same number of columns as the DataList.");

            DataListRow row = new DataListRow(parentDataList, rowData);
            internalList.Add(row);

            return row;
        }

        public DataListRow Add()
        {
            object[] row = new object[parentDataList.columnCount];
            return Add(row);
        }

        public void RemoveAt(int index)
        {
            internalList.RemoveAt(index);
        }

        public void Clear()
        {
            internalList.Clear();
        }

        public IEnumerator<DataListRow> GetEnumerator()
        {
            foreach (var row in internalList)
            {
                yield return row;
            }
        }

    }
}
