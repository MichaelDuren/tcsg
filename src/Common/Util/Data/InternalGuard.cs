﻿using System;

namespace Util.Data
{
    internal static class InternalGuard
    {
        public static void ArgumentNotNull(object argumentValue, string argumentName)
        {
            if (argumentValue == null)
            {
                throw new ArgumentNullException(argumentName);
            }
        }

        public static void ValueNotNull(object value, string exceptionMessage)
        {
            if (value == null)
            {
                throw new InvalidOperationException(exceptionMessage);
            }
        }
    }
}
