﻿//2012.08.25 - Complete

using System.Collections.Generic;

namespace Util.Data
{
    public class DataList
    {
        internal readonly int columnCount;

        internal readonly DataListType dataListType;

        public DataListColumns Columns { get; private set; }

        public DataListRows Rows { get; private set; }

        private DataList()
        {
            Rows = new DataListRows(this);
            Columns = new DataListColumns();
        }

        public DataList(int columnCount)
            : this()
        {
            this.dataListType = DataListType.DataOnly;
            this.columnCount = columnCount;

            this.Columns = null;
        }

        public DataList(List<string> columnNames)
            : this()
        {
            InternalGuard.ArgumentNotNull(columnNames, "columnNames");

            this.dataListType = DataListType.ColumnNamesOnly;
            this.columnCount = columnNames.Count;

            foreach (var columnName in columnNames)
            {
                this.Columns.Add(columnName, null);
            }
        }

        public DataList(DataListColumns columns)
            :this()
        {
            InternalGuard.ArgumentNotNull(columns, "Columns");

            this.dataListType = DataListType.ColumnNamesAndDataTypes;
            this.columnCount = columns.Count;

            foreach (var column in columns)
            {
                this.Columns.Add(column);
            }
        }
    }
}
