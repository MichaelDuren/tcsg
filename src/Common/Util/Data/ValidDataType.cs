﻿//2012.08.25 - Complete

namespace Util.Data
{
    public enum ValidDataType
    {
        String,
        Boolean,
        DateTime,
        Decimal,
        Double,
        Int16,
        Int32,
        Int64,
        Single,
        NullableBoolean,
        NullableDateTime,
        NullableDecimal,
        NullableDouble,
        NullableInt16,
        NullableInt32,
        NullableInt64,
        NullableSingle
    }
}
