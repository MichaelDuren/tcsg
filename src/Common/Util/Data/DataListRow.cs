﻿//2012.08.25 - Complete

using System;

namespace Util.Data
{
    public class DataListRow
    {
        private readonly DataList parentDataList;

        private object[] rowData;

        public bool IsDirty { get; private set; }

        internal DataListRow(DataList parentDataList, object[] rowData)
        {
            InternalGuard.ArgumentNotNull(parentDataList, "ParentDataList");
            InternalGuard.ArgumentNotNull(rowData, "rowData");

            if (rowData.Length != parentDataList.columnCount)
            {
                throw new InvalidOperationException("Column counts do not match between RowData and column count from Parent Data List.");
            }

            this.parentDataList = parentDataList;
            this.rowData = rowData;
        }

        public object this[int index]
        {
            get
            {
                CheckColumnIndex(index);
                return rowData[index];
            }
            set
            {
                CheckColumnIndex(index);

                if (this.parentDataList.dataListType == DataListType.ColumnNamesAndDataTypes)
                {
                    object junk = ReflectionTools.FixDataType(value, this.parentDataList.Columns[index].DataType);
                }

                rowData[index] = value;
                this.IsDirty = true;
            }
        }

        public object this[string columnName]
        {
            get
            {
                int index = GetColumnIndex(columnName);
                return this[index];
            }
            set
            {
                int index = GetColumnIndex(columnName);
                this[index] = value;
            }
        }

        private void CheckColumnIndex(int index)
        {
            if (index >= parentDataList.columnCount)
            {
                throw new ArgumentOutOfRangeException("index", "Specified index is out of the range of the defined columns.");
            }
        }

        private int GetColumnIndex(string columnName)
        {
            if (parentDataList.dataListType == DataListType.DataOnly)
            {
                throw new InvalidOperationException("Cannot lookup column name for DataListType.DataOnly.");
            }

            if (parentDataList.Columns.ContainsKey(columnName))
            {
                return parentDataList.Columns[columnName].ColumnIndex;
            }
            else
            {
                throw new ArgumentOutOfRangeException("ColumnName", "Specified columnName is not one of the defined columns.");
            }
        }
    }
}
