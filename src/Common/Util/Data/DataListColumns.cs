﻿//2012.08.25 - Complete

using System;
using System.Collections.Generic;
using Util.Collections.Generic;

namespace Util.Data
{
    public interface IDataListColumn
    {
        string ColumnName { get; }
        Type DataType { get; }
    }

    public class DataListColumn : IDataListColumn
    {
        internal int ColumnIndex { get; set; }
        public string ColumnName { get; internal set; }
        public Type DataType { get; internal set; }

        public DataListColumn(string columnName, Type dataType)
        {
            InternalGuard.ArgumentNotNull(columnName, "ColumnName");

            this.ColumnName = columnName;
            this.DataType = dataType;
        }
    }

    public class DataListColumns //: IList<DataListColumn>
    {
        private int nextColumnIndex;

        private CaseInsensitiveListDictionary<DataListColumn> internalListDictionary = new CaseInsensitiveListDictionary<DataListColumn>();

        public DataListColumn this[int index]
        {
            get
            {
                return internalListDictionary[index];
            }
        }

        public DataListColumn this[string key]
        {
            get
            {
                return internalListDictionary[key];
            }
        }

        public int Count
        {
            get
            {
                return internalListDictionary.Count;
            }
        }

        internal DataListColumns() { }

        public void Add(string columnName, Type dataType)
        {
            this.Add(new DataListColumn(columnName, dataType));
        }

        public void Add(IDataListColumn column)
        {
            this.Add(new DataListColumn(column.ColumnName, column.DataType));
        }

        public void Add(DataListColumn column)
        {
            if (this.ContainsKey(column.ColumnName))
            {
                throw new InvalidOperationException("Duplicate column name: " + column.ColumnName);
            }

            column.ColumnIndex = this.nextColumnIndex++;
            internalListDictionary.Add(column.ColumnName, column);
        }

        public bool ContainsKey(string columnName)
        {
            return internalListDictionary.ContainsKey(columnName);
        }

        public IEnumerator<DataListColumn> GetEnumerator()
        {
            foreach (var column in internalListDictionary.Values)
            {
                yield return column;
            }
        }

        public override string ToString()
        {
            StringDelimiter sd = new StringDelimiter(@"\n");
            foreach (DataListColumn column in this)
            {
                sd.Add("{0} {1}", column.DataType, column.ColumnName);
            }

            return sd.ToString();
        }

        //int IList<DataListColumn>.IndexOf(DataListColumn item)
        //{
        //    throw new NotImplementedException();
        //}

        //int IList<DataListColumn>.Insert(int index, DataListColumn item)
        //{
        //    throw new NotImplementedException();
        //}

        //int IList<DataListColumn>.Remove(int index)
        //{
        //    throw new NotImplementedException();
        //}

        //DataListColumn IList<DataListColumn>.this[int index]
        //{
        //    get
        //    {
        //        throw new NotImplementedException();
        //    }
        //    set
        //    {
        //        throw new NotImplementedException();
        //    }
        //}

        //void ICollection<DataListColumn>.Add(DataListColumn item)
        //{
        //    throw new NotImplementedException();
        //}

        //void ICollection<DataListColumn>.Clear()
        //{
        //    throw new NotImplementedException();
        //}

        //void ICollection<DataListColumn>.Contains(DataListColumn item)
        //{
        //    throw new NotImplementedException();
        //}

        //void ICollection<DataListColumn>.CopyTo(DataListColumn[] array, int arrayIndex)
        //{
        //    throw new NotImplementedException();
        //}

        //bool ICollection<DataListColumn>.IsReadOnly
        //{
        //    get
        //    {
        //        throw new NotImplementedException();
        //    }
        //}

        //void ICollection<DataListColumn>.Remove(DataListColumn item)
        //{
        //    throw new NotImplementedException();
        //}

        //IEnumerator<DataListColumn> IEnumerable<DataListColumn>.GetEnumerator()
        //{
        //    throw new NotImplementedException();
        //}

        //System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        //{
        //    throw new NotImplementedException();
        //}
    }

}
