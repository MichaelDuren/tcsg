﻿//2012.08.25 - Complete


namespace Util.Data
{
    public enum DataListType
    {
        DataOnly,
        ColumnNamesOnly,
        ColumnNamesAndDataTypes
    }
}
