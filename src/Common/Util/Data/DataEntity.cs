﻿//2012.08.25 - Complete

using System;
using System.Collections.Generic;

namespace Util.Data
{
    public class DataEntity
    {
        internal readonly DataList dataList;

        public DataListColumns Columns
        {
            get
            {
                return dataList.Columns;
            }
        }

        public DataListRow Row
        {
            get
            {
                if (dataList.Rows.Count == 0)
                {
                    dataList.Rows.Add();
                }
                return dataList.Rows[0];
            }
        }

        private DataEntity() { }

        public DataEntity(int columnCount)
        {
            this.dataList = new DataList(columnCount);
        }

        public DataEntity(List<string> columnNames)
        {
            this.dataList = new DataList(columnNames);
        }

        public DataEntity(DataListColumns columns)
        {
            this.dataList = new DataList(columns);
        }

        public DataEntity(DataList dataList)
        {
            if (dataList.Rows.Count > 1)
            {
                throw new InvalidOperationException("DataList can not contain more than 1 row if being used to initialize a DataEntity.");
            }

            this.dataList = dataList;
        }


    }
}
