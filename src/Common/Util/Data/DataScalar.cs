﻿//2012.08.25 - Complete

using System;

namespace Util.Data
{
    public class DataScalar
    {
        public bool IsDirty { get; private set; }

        public Type DataType { get; set; }

        private object _value;

        public object Value
        {
            get
            {
                return _value;
            }
            set
            {
                if (this.DataType != null)
                {
                    object junk = ReflectionTools.FixDataType(value, this.DataType);
                }

                _value = value;
                this.IsDirty = true;
            }
        }

        private DataScalar() { }

        public DataScalar(Type dataType)
        {
            this.DataType = DataType;
        }

        public DataScalar(Type dataType, object value)
        {
            this.DataType = dataType;
            this.Value = value;

            this.IsDirty = false;
        }

        public DataScalar(DataEntity entity)
        {
            if (entity == null || entity.Columns.Count != 0)
            {
                throw new InvalidOperationException("DataEntity cannot be null and must contain 1 column if being used to initialize a DataScalar.");
            }

            DataListColumn column = entity.Columns[0];
            this.DataType = column.DataType;
            this.Value = entity.Row[0];
        }
    }
}
