﻿using System;
using System.IO;
using System.Text;

namespace Util
{
    /// <summary>
    /// This class provides utilities to help with standard file operations
    /// </summary>
    public class FileTools
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary> This returns the OS originalValue for a NewLine</summary>
        public static readonly string lineSeparator = "\r\n";


        // ******************************************************************************
        // Load Methods
        // ******************************************************************************

        /// <summary>
        /// Handles Loading a String originalValue from a file.
        /// Returns null if the file does not exist.
        /// </summary>
        /// <param name="FileNameAndPath">- Name of the file - can simply be a file name or
        /// can be a complete file path.  The default directory is the current directory
        /// </param>
        public static string LoadStringFromFile(string fileNameAndPath)
        {
            StringBuilder sb = new StringBuilder();

            FileInfo file = new FileInfo(fileNameAndPath);

            if (!File.Exists(file.FullName))
            {
                return null;
            }

            string fileContents = null;
            try
            {
                using (StreamReader rdr = File.OpenText(file.FullName))
                {
                    fileContents = rdr.ReadToEnd();
                }
            }
            catch (IOException e)
            {
                throw new IOException("Exception while loading file: " + fileNameAndPath, e);
            }

            return fileContents;
        }


        // ******************************************************************************
        // Save Methods
        // ******************************************************************************

        /// <summary>
        /// Handles Saving a String originalValue to a file
        /// if the file already exists, it throws IOException and skips the save 
        /// unless the force originalValue is true.
        /// </summary>
        /// <param name="output">- String originalValue to save to a file
        /// </param>
        /// <param name="FileNameAndPath">- Name of the file - can simply be a file name or
        /// can be a complete file path.  The default directory is the current directory
        /// </param>
        /// <param name="force">- boolean - specifies if we should "force" the write, or instead if we 
        /// should throw an IOException if a file already exists.
        /// </param>
        public static void SaveStringToFile(string output, string fileNameAndPath, bool force)
        {
            // See if the file already exists (ok if Force specified)
            FileInfo file = new FileInfo(fileNameAndPath);
            if (File.Exists(file.FullName) && !force)
            {
                throw new IOException("File: " + fileNameAndPath + " already exists and Force option not specified");
            }
            else
            {
                try
                {
                    using (StreamWriter wtr = File.CreateText(file.FullName))
                    {
                        wtr.Write(output);
                        wtr.Close();
                    }
                }
                catch (IOException e)
                {
                    throw new IOException("Save to file " + fileNameAndPath + " failed", e);
                }
            }
        }

        // ******************************************************************************
        // Other Methods
        // ******************************************************************************

        /// <summary> 
        /// Returns the Base Path for the application.  Can be used in either a Web or Non-Web context
        /// </summary>
        public static string GetAppRootDirectory()
        {
            return AppDomain.CurrentDomain.BaseDirectory;
        }

    }
}
