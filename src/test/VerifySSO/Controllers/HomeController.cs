﻿using ESP.AppServer.Entities;
using ESP.AppServer.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using VerifySSO.Managers;

namespace VerifySSO.Controllers
{
    public class HomeController : Controller
    {
        [AllowAnonymous]
        public ActionResult Index()
        {
            UserProfile userProfile = null;
            // User is authenticated, but there is no authentication cookie yet, so the user must login
            UserManager userManager = new UserManager();

            userProfile = userManager.GetCurrentUserProfile();

            if (userProfile == null)
            {
                userProfile = new UserProfile() { LoginId = "UNKNOWN", Email = "UNKNOWN", FirstName = "UNKNOWN", LastName = "UNKNOWN" };
            }

            return View(userProfile);
        }
    }
}
