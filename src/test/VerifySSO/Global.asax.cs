﻿using ESP.AppServer;
using ESP.AppServer.Entities;
using System;
using System.Security.Principal;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace VerifySSO
{
    public class MvcApplication : HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }
        protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        {  
            // Is the user authenticated?
            if (!HttpContext.Current.Request.IsAuthenticated)
            {
                string userId = HttpContext.Current.Request.ServerVariables[AppServerContext.SSOHeaderUID];

                if (userId != null)
                {
                    GenericIdentity webIdentity = new GenericIdentity(userId);
                    GenericPrincipal principal = new GenericPrincipal(webIdentity, null);
                    HttpContext.Current.User = principal;
                }
            }
        }

        protected void Application_PreRequestHandlerExecute(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie == null)
            {
                string userId = null;
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    userId = HttpContext.Current.Request.ServerVariables[AppServerContext.SSOHeaderUID];

                    if (userId != null)
                    {
                        GenericIdentity webIdentity = new GenericIdentity(userId);
                        GenericPrincipal principal = new GenericPrincipal(webIdentity, null);
                        HttpContext.Current.User = principal;
                    }
                }
                else
                {
                    userId = HttpContext.Current.User.Identity.Name;
                }

                if (userId != null)
                {
                    String firstName = HttpContext.Current.Request.ServerVariables[AppServerContext.SSOHeaderFirstName];
                    String lastName = HttpContext.Current.Request.ServerVariables[AppServerContext.SSOHeaderLastName];
                    String emailAddress = HttpContext.Current.Request.ServerVariables[AppServerContext.SSOHeaderEMail];
                    UserProfile userProfileData = new UserProfile()
                    {
                        LoginId = HttpContext.Current.User.Identity.Name,
                        FirstName = firstName,
                        LastName = lastName,
                        Email = emailAddress
                    };
                    // Add the user profile to the cache.  
                    HttpContext.Current.Session.Add(Constants.DomainAndLoginId, userProfileData);
                    FormsAuthentication.SetAuthCookie(HttpContext.Current.User.Identity.Name, true);
                }
            }
        }
    }
}
