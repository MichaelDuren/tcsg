﻿using ESP.AppServer;
using ESP.AppServer.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;

namespace VerifySSO.Managers
{
    public class UserManager
    {
        public UserProfile GetCurrentUserProfile()
        {
            UserProfile userProfile = null;

            if (HttpContext.Current.Session != null)
            {
                userProfile = HttpContext.Current.Session[Constants.DomainAndLoginId] as UserProfile;

                FormsAuthentication.SignOut();
                // reset the session, this is only for testing.
                HttpContext.Current.Session.Remove(Constants.DomainAndLoginId);
            }
            return userProfile;
        }

    }
}