﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Xml.Serialization;
using ESP.AppServer.Managers;
using ESP.AppServer;

namespace ESP
{
    class Program
    {
        static void Main(string[] args)
        {
            bool test = false;
            List<String> gesSubjects;
            GESManager gesManager = null;
            List<string> serializedItems = new List<string>();
            List<GESManager.GESActionItemList> listSubjectsAndEntitlements = new List<GESManager.GESActionItemList>();

            gesManager = new GESManager();

            // Get all roles in the GES Portal Namespace
            var gesRoles = gesManager.gesGetAllRoles();
            System.Console.WriteLine("GES returned the following roles: ");
            System.Console.WriteLine(" - " + String.Join("\r\n - ", gesRoles.ToArray()));

            // Get all users in the GES Portal Namespace
            gesSubjects = gesManager.gesGetAllSubjects();
            System.Console.WriteLine("GES returned the following subjects: ");
            System.Console.WriteLine(" - " + String.Join("\r\n - ", gesSubjects.ToArray()));

            // for each user, get user detail from directory, get the resource they can access, get actions
            // they can perform against resource.
            foreach (string subject in gesSubjects)
            {
                Dictionary<String, List<String>> subjectResourcesAndActions;

                System.Console.WriteLine("------------------------------------------------------------");
                System.Console.WriteLine("LoginId: " + subject);

                // get the roles that this user has
                var subjectRoles = gesManager.gesGetSubjectRoles(subject);
                if (subjectRoles != null)
                {
                    System.Console.WriteLine(" SubjectRoles: " + String.Join(", ", subjectRoles.ToArray()));
                }

                // get sources this user may access
                subjectResourcesAndActions = gesManager.gesGetPermittedResources(subject);

                if (subjectResourcesAndActions != null)
                {
                    var gesActionItem = subjectResourcesAndActions.Select(kv => new GESManager.GESActionItem()
                    {
                        userAction = kv.Key,
                        resourceIds = kv.Value
                    }).ToArray();

                    var gesActionItemList = new GESManager.GESActionItemList()
                    {
                        subject = subject,
                        subjectResourcesAndActions = gesActionItem,
                        subjectRoles = subjectRoles
                    };
                    listSubjectsAndEntitlements.Add(gesActionItemList);

                    foreach (KeyValuePair<String, List<String>> resourceAndAction in subjectResourcesAndActions)
                    {
                        var action = resourceAndAction.Key;
                        System.Console.WriteLine(" Resource " + resourceAndAction.Key + " - Action: " + action);

                        foreach (string resource in resourceAndAction.Value)
                        {
                            // For testing purposes, we need a unique value for the code signing type id.
                            System.Console.WriteLine(" " + resourceAndAction.Key + ": " + resource);
                        }
                    }
                }
            }

            MemoryStream stream = new MemoryStream();
            XmlSerializer serializer = new XmlSerializer(typeof(GESManager.GESActionItemList[]),
                                 new XmlRootAttribute() { ElementName = "GESActionItemList" });

            serializer.Serialize(stream, listSubjectsAndEntitlements.ToArray());

            // convert stream to string
            stream.Position = 0;
            StreamReader reader = new StreamReader(stream);
            var serliaizedPermittedResources = reader.ReadToEnd();

            File.WriteAllText("outGESSubjectsAndEntitlements.xml", serliaizedPermittedResources);

        }
    }
}

