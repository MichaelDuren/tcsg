﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography;
using System.IO;
using CSG.AppServer.Managers;

using CSG.AppServer.Db;
using CSG.AppServer.Entities;
using CSG.AppServer;
using System.Data.SqlClient;
using Util.Config;

namespace LoadCert
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                if (args.Length < 2)
                {
                    Console.WriteLine("Usage: loadCert certName certFile");
                    throw new Exception("The argument should be 2");
                }
                string fileName = string.Empty;
                string tempFileName = string.Empty;
                string combinedFilenames = string.Empty;
                fileName = Path.GetFileName(args[1]);
               // System.Threading.Thread.Sleep(20000);

                var cert = new X509Certificate2(fileName);
                var issuerDn = CertificateManager.ParseDistinguishedName(cert.Issuer);
                var subjectDn = CertificateManager.ParseDistinguishedName(cert.Subject);
                var base64Cert = Convert.ToBase64String(cert.Export(X509ContentType.Cert), Base64FormattingOptions.None);
                // display the certificate for confirmation.
                Certificate newCert = new Certificate()
                {
                    IssuedTo = subjectDn.Find(item => item.Key.Equals("CN")).Value,
                    Name = args[0],
                    DistinguishedName = cert.Subject,
                    InSecurityWorld = false,
                    InStore = true,
                    Thumbprint = cert.Thumbprint,
                    IssuedBy = issuerDn.Find(item => item.Key.Equals("CN")).Value,
                    MimeEncoded = base64Cert,
                    NotAfter = cert.NotAfter,
                    NotBefore = cert.NotBefore
                };
                if (!newCert.InValidityPeriod)
                {
                    throw new Exception("Certificate is expired or not yet valid.");

                }
                string dbcs = null;
                if (dbcs == null)
                {
                    dbcs = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.BaseDatabaseConnectionStringName];
                }
                AppServerInitializer.InitializeLoggingEnvironmentVariables();
                string dbcon = DynamicConfigurationManager.GetConnectionString(dbcs).ConnectionString;
                ESPDbDomainService domainService = new ESPDbDomainService();
                var systemSettings = domainService.GetSystemSettings().ToList();
                CertificateManager certMgr = new CertificateManager();
                certMgr.SaveCertificate(newCert);
                Console.WriteLine("Certificate loading finished.");

            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Certificate loading failed.");
            }
            


        }
    }
}
