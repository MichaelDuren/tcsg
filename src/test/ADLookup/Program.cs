﻿using System;
using System.Collections.Generic;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.DirectoryServices.ActiveDirectory;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADLookup
{
    class Program
    {
        
        static void Main(string[] args)
        {
            if (args.Count() < 3)
            {
                Console.WriteLine("usage: ADLookup <domain> <container> <user 1> <user 2> ... <user n>");
                return;
            }

            bool bFindFailed = false;
            string domainOrServerName = args[0];
            string container = args[1];
            List<string> userNames = new List<string>();
            for (int i=2; i < args.Count(); ++i)
            {
                userNames.Add(args[i]);
            }


            // Directory Seacher Test
            var currentForest = Forest.GetCurrentForest();
            var gc = currentForest.FindGlobalCatalog();
            var startTime = DateTime.Now;
            using (var userSearcher = gc.GetDirectorySearcher())
            {
                foreach (string userName in userNames)
                {
                    userSearcher.Filter = string.Format("(&(objectCategory=person)(userprincipalname={0}))", userName);
                    SearchResult result = userSearcher.FindOne();

                    if (result != null)
                    {
                        /*
                        var email = result.Properties["mail"];
                        var firstName = result.Properties["sn"];
                        var lastName = result.Properties["givenname"];
                        Console.WriteLine("Found user {0}, with attributes: ", userName);
                        if (email != null && email.Count > 0)
                        {
                            Console.WriteLine("  EMail:      {0}", email[0]);
                        }
                        if (firstName != null && firstName.Count > 0)
                        {
                            Console.WriteLine("  First Name: {0}", firstName[0]);
                        }
                        if (lastName != null && lastName.Count > 0)
                        {
                            Console.WriteLine("  Last Name:  {0}", lastName[0]);
                        }
                        */
                    }
                    else
                    {
                        Console.WriteLine("USER NOT FOUND: " + userName);
                        bFindFailed = true;
                        break;
                    }
                }
            }
            var endTime = DateTime.Now;
            if (!bFindFailed)
            {
                Console.WriteLine("DirectorySearch found all users in {0}ms.", endTime.Subtract(startTime).Milliseconds);
            }
        }
    }
}
