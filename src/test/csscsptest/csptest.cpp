

#include <Windows.h>

extern "C"
{
#include <wincrypt.h>
#include "cspdk.h"
#include "csscsp.h"

BOOL WINAPI CPRefreshStore(const char *pszProvName);

}

void main(void)
{
	HCRYPTPROV hProv;
	VTableProvStruc vTable;

	vTable.pszProvName = CSSCSP_NAME;
	vTable.pbContextInfo = NULL;
	vTable.cbContextInfo = 0;
	vTable.dwProvType = 0;
	vTable.FuncReturnhWnd = NULL;
	vTable.FuncVerifyImage = NULL;
	vTable.Version = 0;

	CPRefreshStore(CSSCSP_NAME);

	CPAcquireContext(&hProv, "", 0, &vTable);
}