// Transform.java
// Copyright (C) 2008-2011 Thales e-Security Inc.  All rights reserved.
package com.thales.esecurity.asg.cssjca;

import java.io.IOException;
import java.io.ByteArrayOutputStream;

/** A collection of functions for transforming arbitrary byte arrays
 *  to and from printable strings. */
public class Transform {
    private static final char[] hex = {'0','1','2','3','4','5','6','7',
                                       '8','9','a','b','c','d','e','f'};

    /** Converts a byte array to a hexidecimal encoded string. */
    public static String byte2hex(byte[] data) {
        StringBuilder out = new StringBuilder();
        for (byte b : data)
            out.append(hex[(b >> 4) & 0xF]).append(hex[b & 0xF]);
        return out.toString();
    }

    /** Converts a hexidecimal encoded string to a byte array.  This
     *  routine ignores colon and space characters in its input.
     *  Other characters not normally associated with hexidecimal
     *  result in an IllegalArgumentException. */
    public static byte[] hex2byte(String data) {
        byte[] dest = new byte[(data.length() + 1) / 2];
        int    didx = 0;
        for (char c : data.toCharArray()) {
            c = Character.toLowerCase(c);
            if ((c >= '0') && (c <= '9')) {
                if ((didx % 2) == 0)
                    dest[didx++/2] = (byte)((c - '0') << 4);
                else dest[didx++/2] |= c - '0';
            } else if ((c >= 'a') && (c <= 'f')) {
                if ((didx % 2) == 0)
                    dest[didx++/2] = (byte)((c - 'a' + 10) << 4);
                else dest[didx++/2] |= c - 'a' + 10;
            } else if ((c == ':') || (c == ' '))
                continue;
            else throw new IllegalArgumentException
                     ("Invalid hexidecimal character \'" + c + "\'.");
        }
        return dest;
    }

    /** Converts a byte array to a Base64 encoded string. */
    public static String byte2b64(byte[] data) {
        final String b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" +
            "abcdefghijklmnopqrstuvwxyz" + "0123456789" + "+/";
        StringBuilder out = new StringBuilder();
        for (int pos = 0; pos < data.length; pos += 3) {
            int aa = data[pos] & 0xFF;
            int bb = (pos + 1 < data.length) ?
                (data[pos + 1] & 0xFF) : 0;
            int cc = (pos + 2 < data.length) ?
                (data[pos + 2] & 0xFF) : 0;
            out.append(b64.charAt(aa >> 2));
            out.append(b64.charAt(((aa & 0x03) << 4) | (bb >> 4)));
            out.append((pos + 1 < data.length) ? b64.charAt
                       (((bb & 0x0F) << 2) | ((cc >> 6) & 0x03)) : '=');
            out.append((pos + 2 < data.length) ? b64.charAt
                       (cc & 0x3F) : '=');
        }
        return out.toString();
    }

    /** Converts a Base64 encoded string to a byte array.  This
     *  routine ignores white space in its input.  Other characters
     *  that are not alphanumeric, plus or slash result in an
     *  IllegalArgumentException. */
    public static byte[] b642byte(String data) {
        // ASCII characters that are part of Base64 have their bit
        // values.  Characters that are not part of Base64 are 0xFF
        // and can be detected as errors.
        final byte[] b64 = {
            (byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0xFF,
            (byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0xFF,
            (byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0xFF,
            (byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0xFF,
            (byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0xFF,
            (byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0xFF,
            (byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0xFF,
            (byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0xFF,
            (byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0xFF,
            (byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0xFF,
            (byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0x3E,
            (byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0x3F,
            (byte)0x34, (byte)0x35, (byte)0x36, (byte)0x37,
            (byte)0x38, (byte)0x39, (byte)0x3a, (byte)0x3b,
            (byte)0x3C, (byte)0x3D, (byte)0xFF, (byte)0xFF,
            (byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0xFF,
            (byte)0xFF, (byte)0x00, (byte)0x01, (byte)0x02,
            (byte)0x03, (byte)0x04, (byte)0x05, (byte)0x06,
            (byte)0x07, (byte)0x08, (byte)0x09, (byte)0x0A,
            (byte)0x0B, (byte)0x0C, (byte)0x0D, (byte)0x0E,
            (byte)0x0F, (byte)0x10, (byte)0x11, (byte)0x12,
            (byte)0x13, (byte)0x14, (byte)0x15, (byte)0x16,
            (byte)0x17, (byte)0x18, (byte)0x19, (byte)0xFF,
            (byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0xFF,
            (byte)0xFF, (byte)0x1A, (byte)0x1B, (byte)0x1C,
            (byte)0x1D, (byte)0x1E, (byte)0x1F, (byte)0x20,
            (byte)0x21, (byte)0x22, (byte)0x23, (byte)0x24,
            (byte)0x25, (byte)0x26, (byte)0x27, (byte)0x28,
            (byte)0x29, (byte)0x2A, (byte)0x2B, (byte)0x2C,
            (byte)0x2D, (byte)0x2E, (byte)0x2F, (byte)0x30,
            (byte)0x31, (byte)0x32, (byte)0x33, (byte)0xFF,
            (byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0xFF,
        };
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        byte[] values = new byte[3];
        int pos = 0;
        for (char c : data.toCharArray()) {
            if (Character.isWhitespace(c))
                continue;
            else if (c == '=') {
                if (pos < 2)
                    throw new IllegalArgumentException
                        ("Invalid padding for base64.");
                pos--; // discard partially encoded padding byte
                baos.write(values, 0, pos);
                break;
            } else if (((int)c < b64.length) && (b64[(int)c]) >= 0) {
                int b = b64[(int)c];
                if (pos == 0) {
                    values[0] = (byte)(0xFC & (b << 2));
                } else if (pos == 1) {
                    values[0] |= (byte)(0x0F & (b >> 4));
                    values[1] = (byte)(0xF0 & (b << 4));
                } else if (pos == 2) {
                    values[1] |= (byte)(0x0F & (b >> 2));
                    values[2] = (byte)(0xC0 & (b << 6));
                } else if (pos == 3) {
                    values[2] |= (byte)(0x3F & b);
                }
                if (++pos == 4) {
                    baos.write(values, 0, values.length);
                    pos = 0;
                }
            } else throw new IllegalArgumentException
                       ("Invalid Base64 character \'" + c + "\'.");
        }
        return baos.toByteArray();
    }

    /** A simple unit test for string transformations.  Each command
     *  line argument is converted to hexidecimal and back and then
     *  converted to Base64 and back. */
    public static void main(String[] args) {
        for (String arg : args) {
            System.out.println("=== " + arg);
            byte[] abytes = arg.getBytes();
            String hex = byte2hex(abytes);
            String hex_reverse = new String(hex2byte(hex));
            String b64 = byte2b64(abytes);
            String b64_reverse = new String(b642byte(b64));
            System.out.println("  hex: " + hex + " " +
                               (arg.equals(hex_reverse) ? "==>" : "!=>")
                               + " " + hex_reverse);
            System.out.println("  b64: " + b64 + " " +
                               (arg.equals(b64_reverse) ? "==>" : "!=>")
                               + " " + b64_reverse);
        }
    }
}
