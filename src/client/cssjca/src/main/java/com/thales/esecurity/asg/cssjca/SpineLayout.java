// SpineLayout.java
// Copyright (C) 2011 Thales e-Security Inc.  All rights reserved.
package com.thales.esecurity.asg.cssjca;

import java.util.ArrayList;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.LayoutManager;
import java.awt.Component;
import java.awt.Container;
import java.awt.Button;
import java.awt.Label;
import java.awt.TextField;
import java.awt.TextArea;
import java.awt.Frame;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/** <p>A simple layout manager capable of arranging components along a
 *  central spine.  This is convenient for forms where labels and text
 *  should be lined up.</p>
 *
 *  <p>When adding a component on the right a series of null entries
 *  are used to pad the left.  This makes it important to either
 *  add all the left components first or to add alternate right
 *  and left starting with the left.</p> */
public class SpineLayout implements LayoutManager {
    // INVARIANT: rights.size() <= lefts.size()
    private ArrayList<Component> rights  = new ArrayList<Component>();
    private ArrayList<Component> lefts   = new ArrayList<Component>();
    private ArrayList<Component> bottoms = new ArrayList<Component>();
    private ArrayList<Component> stack   = new ArrayList<Component>();
    private int spacing = 0;

    public SpineLayout(int spacing) { this.spacing = spacing; }
    public SpineLayout() {}

    public void addLayoutComponent(String name, Component comp) {
        if (name.toLowerCase().equals("right")) {
            rights.add(comp);
            while (lefts.size() < rights.size())
                lefts.add(null);
        } else if (name.toLowerCase().equals("left"))
            lefts.add(comp);
        else if (name.toLowerCase().equals("bottom"))
            bottoms.add(comp);
        else stack.add(comp);
    }

    public void removeLayoutComponent(Component comp) {
        lefts.remove(comp);
        rights.remove(comp);
        bottoms.remove(comp);
        stack.remove(comp);
        while (rights.size() < lefts.size())
            rights.add(null);
    }

    protected Dimension computeSize(Container parent,
                                    boolean preferred) {
        boolean first;
	Dimension result = new Dimension(0, 0);
        Insets insets = parent.getInsets();
        result.width  += insets.left + insets.right;
        result.height += insets.top  + insets.bottom;
        first = true;
        for (int i = 0; i < rights.size(); i++) {
            Dimension left_size = preferred ?
                lefts.get(i).getPreferredSize() :
                lefts.get(i).getMinimumSize();
            Dimension right_size = preferred ?
                rights.get(i).getPreferredSize() :
                rights.get(i).getMinimumSize();
            int row_width = (preferred ? spacing : 0) +
                ((left_size.width > right_size.width) ?
                 (2 * left_size.width) : (2 * right_size.width));
            if (result.width < row_width)
                result.width = row_width;
            result.height += (left_size.height > right_size.height) ?
                left_size.height : right_size.height;
            if (first)
                first = false;
            else if (preferred)
                result.height += spacing;
        }
        if (preferred && (rights.size() > 0) && (bottoms.size() > 0))
            result.height += spacing;
        Dimension bottom_size = new Dimension(0, 0);
        first = true;
        for (Component c : bottoms) {
            Dimension d = preferred ? c.getPreferredSize() :
                c.getMinimumSize();
            bottom_size.width += d.width;
            if (bottom_size.height < d.height)
                bottom_size.height = d.height;
            if (first)
                first = false;
            else if (preferred)
                bottom_size.width += spacing;
        }
        if (result.width < bottom_size.width)
            result.width = bottom_size.width;
        result.height += bottom_size.height;

        if (preferred && (bottoms.size() > 0) && (stack.size() > 0))
            result.height += spacing;
        first = true;
        for (Component c : stack) {
            Dimension d = preferred ? c.getPreferredSize() :
                c.getMinimumSize();
            result.height += d.height;
            if (result.width < d.width)
                result.width = d.width;
            if (first)
                first = false;
            else if (preferred)
                result.height += spacing;
        }
        return result;
    }

    public Dimension minimumLayoutSize(Container parent) {
        // Returns the preferred size for the moment because the
        // layout routine doesn't adjust for less than the desired
        // amount of space.  Components simply overflow so there's
        // no point in providing a smaller minimum.
        //
        // In the future it might make sense to make this layout
        // manager degrade more gracefully when available space
        // is limited.  For instance, spacing could be eliminated.
        return computeSize(parent, true);
    }

    public Dimension preferredLayoutSize(Container parent)
    { return computeSize(parent, true); }

    public void layoutContainer(Container parent) {
        boolean first;
	Dimension size = parent.getSize();
	Insets insets  = parent.getInsets();
	int x, y, width, height;
	x = insets.left;
	y = insets.top;
	width  = size.width  - (insets.left + insets.right);
	height = size.height - (insets.top  + insets.bottom);

        // Compute desired proportions
        ArrayList<Dimension> prefs = new ArrayList<Dimension>();
        ArrayList<Dimension> right_prefs = new ArrayList<Dimension>();
        ArrayList<Dimension> left_prefs = new ArrayList<Dimension>();
        for (int i = 0; i < rights.size(); i++) {
            Dimension left_pref  = lefts.get(i).getPreferredSize();
            Dimension right_pref = rights.get(i).getPreferredSize();
            Dimension pref = new Dimension
                ((left_pref.width > right_pref.width) ?
                 (2 * left_pref.width) : (2 * right_pref.width),
                 (left_pref.height > right_pref.height) ?
                 left_pref.height : right_pref.height);
            left_prefs.add(left_pref);
            right_prefs.add(right_pref);
            prefs.add(pref);
        }
        Dimension bottom_size = new Dimension(0, 0);
        first = true;
        for (Component c : bottoms) {
            Dimension current = c.getPreferredSize();
            if (bottom_size.height < current.height)
                bottom_size.height = current.height;
            bottom_size.width += current.width;
            if (first)
                first = false;
            else bottom_size.width += spacing;
        }

        // Perform actual layout operations
        int x_used = 0, y_used = 0;
        first = true;
        for (int i = 0; i < rights.size(); i++) {
            if (first)
                first = false;
            else y_used += spacing;
            Dimension pref = prefs.get(i);
            Dimension left_pref = left_prefs.get(i);
            Dimension right_pref = right_prefs.get(i);
            int row_height = (left_pref.height > right_pref.height) ?
                left_pref.height : right_pref.height;
            lefts.get(i).setBounds
                (x + ((width - spacing) / 2) - left_pref.width,
                 y + y_used + ((row_height - left_pref.height) / 2),
                 left_pref.width, left_pref.height);
            rights.get(i).setBounds
                (x + ((width + spacing) / 2),
                 y + y_used + ((row_height - right_pref.height) / 2),
                 right_pref.width, right_pref.height);
            y_used += pref.height;
        }
        y_used += spacing;
        first = true;
        for (Component c : bottoms) {
            Dimension s = c.getPreferredSize();
            c.setBounds(x + x_used + ((width - bottom_size.width) / 2),
                        y + y_used, s.width, s.height);
            x_used += s.width;
            x_used += spacing;
        }
        y_used += bottom_size.height + spacing;
        for (Component c : stack) {
            Dimension s = c.getPreferredSize();
            c.setBounds(x + ((width - s.width) / 2),
                        y + y_used, s.width, s.height);
            y_used += s.height + spacing;
        }
    }

    public static void main(String args[]) {
	Frame frame = new Frame("SpineLayout Test");
	frame.addWindowListener(new WindowAdapter() {
		public void windowClosing(WindowEvent e) {
		    System.exit(0);
		}});
	frame.setLayout(new SpineLayout(0));
        frame.add(new Label("Left"), "LEFT");
        frame.add(new Label("Right"), "RIGHT");
        frame.add(new Label("Name"), "LEFT");
        frame.add(new TextField("", 8), "RIGHT");
        frame.add(new Label("Phone"), "LEFT");
        frame.add(new TextField("", 12), "RIGHT");
	frame.add(new Button("one"), "BOTTOM");
	frame.add(new Button("two"), "BOTTOM");
	frame.add(new Button("three"), "BOTTOM");
        frame.add(new TextArea(3, 60), "STACK");
	frame.pack();
	frame.setVisible(true);
    }
}
