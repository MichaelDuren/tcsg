// Copyright (C) 2008 THALES Information Systems Security.
// All rights reserved.
package com.thales.esecurity.asg.cssjca;


import java.io.IOException;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.net.Socket;
import java.security.cert.X509Certificate;
import java.security.PrivateKey;
import java.security.Principal;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import javax.net.SocketFactory;
import javax.net.ServerSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.KeyManager;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.X509KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.SSLServerSocketFactory;

/** Simplifies the use of JSSE for SSL clients and servers.  Selecting
 *  a KeyStore and key alias to use is ordinarily cumbersome.  Using
 *  this class reduces it to a two step process.
 *
 *  Here is an example server:
 *
 *  <pre>
 *  ServerSocketFactory sfactory = SSLHelper.createServer
 *      (truststore, null, trustpass, keystore, null, storepass,
 *       keyalias, keypass, "SSL");
 *  SSLServerSocket server = (SSLServerSocket)
 *      sfactory.createServerSocket(port, backlog, address);
 *  server.setNeedClientAuth(true);
 *  while (true)
 *      processConnection((SSLSocket)server.accept());
 *  </pre>
 *
 *  Clients are constructed this way:
 *
 *  <pre>
 *  SocketFactory cfactory = SSLHelper.createClient
 *      (truststore, null, trustpass, keystore, null, storepass,
 *       keyalias, keypass, "SSL");
 *  Socket s = cfactory.createSocket(address, port);
 *  </pre>
 */
public class SSLHelper {

    /** Supports applications that need to select a client or server
     *  key by alias for use with X509 Certificates.  When others are
     *  present the server may choose something other than the desired
     *  key alias.  This class supports only instances of
     *  X509KeyManager and will need to be altered if that interface
     *  is extended in the future releases of Java. */
    private static class AliasX509KeyManager implements X509KeyManager {
        private X509KeyManager base  = null;
        private String         alias = null;
    
        /** Creates a wrapped KeyManager that filters out keys with
         *  aliases that don't match the supplied value. */
        public AliasX509KeyManager(X509KeyManager km, String a) {
            base  = km;
            alias = a;
        }

        /** Implements X509KeyManager. */
        public X509Certificate[] getCertificateChain(String alias)
        { return base.getCertificateChain(alias); }

        /** Implements X509KeyManager. */
        public String[] getClientAliases(String keyType,
                                         Principal[] issuers)
        { return base.getClientAliases(keyType, issuers); }

        /** Implements X509KeyManager. */
        public PrivateKey getPrivateKey(String alias)
        { return base.getPrivateKey(alias); }

        /** Implements X509KeyManager. */
        public String[] getServerAliases(String keyType,
                                         Principal[] issuers)
        { return base.getServerAliases(keyType, issuers); }
    
        /** Implements X509KeyManager but filters by specified alias. */
        public String chooseClientAlias(String[] keyType,
                                        Principal[] issuers,
                                        Socket socket) {
            for (int i = 0; i < keyType.length; i++) {
                String[] aliases =
                    base.getClientAliases(keyType[i], issuers);
                if (aliases != null)
                    for (int j = 0; j < aliases.length; j++)
                        if (aliases[j].equals(alias))
                            return alias;
            }
            return null;
        }

        /** Implements X509KeyManager but filters by specified alias. */
        public String chooseServerAlias(String keyType,
                                        Principal[] issuers,
                                        Socket socket) {
            String[] aliases = base.getServerAliases(keyType, issuers);
            if (aliases != null)
                for (int i = 0; i < aliases.length; i++)
                    if (aliases[i].equals(alias))
                        return alias;
            return null;
        }
    }

    /** Instances of this class serve no purpose. */
    private SSLHelper() {}

    /** Convinces an array of KeyManager instances to choose only 
     *  keys with the specified alias. */
    public static KeyManager[] forceAlias(KeyManager[] kms,
                                          String alias) {
        if ((alias != null) && (alias.length() > 0))
            for (int i = 0; i < kms.length; i++)
                if (kms[i] instanceof X509KeyManager)
                    kms[i] = new AliasX509KeyManager
                        ((X509KeyManager)kms[i], alias);
        return kms;
    }

    /** Creates an SSL context using a combination of custom KeyStore,
     *  TrustStore and key alias.  Supply null for any of these to get
     *  defaults.  This method requires an already open KeyStore and
     *  TrustStore and so is more flexible but less convenient. */
    public static SSLContext createContext
        (KeyStore truststore, KeyStore keystore, String keyalias,
         char[] keypass, String protocol) throws
             NoSuchAlgorithmException,
             KeyStoreException,
             KeyManagementException,
             UnrecoverableKeyException {
        KeyManager[] kms = null;
        if (keystore != null) {
            KeyManagerFactory kmf =
                KeyManagerFactory.getInstance
                (KeyManagerFactory.getDefaultAlgorithm());
            kmf.init(keystore, keypass);
            kms = forceAlias(kmf.getKeyManagers(), keyalias);
        }
        
        TrustManager[] tms = null;
        if (truststore != null) {
            TrustManagerFactory tmf =
                TrustManagerFactory.getInstance
                (TrustManagerFactory.getDefaultAlgorithm());
            tmf.init(truststore);
            tms = tmf.getTrustManagers();
        }
        
        SSLContext sslc = SSLContext.getInstance(protocol);
        sslc.init(kms, tms, null);
        return sslc;
    }

    /** Creates an SSL context using a combination of custom KeyStore,
     *  TrustStore and key alias.  Supply null for any of these to get
     *  defaults.  This method opens the KeyStore and TrustStore as
     *  files and is therefore less flexible but more convenient. */
    private static SSLContext createContext
        (String truststore, String trusttype, char[] trustpass,
         String keystore,   String storetype, char[] storepass,
         String keyalias,   char[] keypass,   String protocol) throws
             NoSuchAlgorithmException, FileNotFoundException,
             IOException, KeyStoreException,
             KeyManagementException, UnrecoverableKeyException,
             CertificateException {
        KeyStore ks = null;
        if (storetype == null)
            storetype = KeyStore.getDefaultType();
        KeyStore ts = null;
        if (trusttype == null)
            trusttype = KeyStore.getDefaultType();

        if (keystore != null) {
            ks = KeyStore.getInstance(storetype);
            ks.load(new FileInputStream(keystore), keypass);
        }
        if (truststore != null) {
            ts = KeyStore.getInstance(trusttype);
            ts.load(new FileInputStream(truststore), trustpass);
        }
        return createContext(ts, ks, keyalias,
                             (keypass == null ? storepass : keypass),
                             protocol);
    }

    /** Returns a factory for creating secure server sockets. */
    public static ServerSocketFactory createServer
        (String truststore, String trusttype, char[] trustpass,
         String keystore,   String storetype, char[] storepass,
         String keyalias,   char[] keypass,   String protocol) throws
             NoSuchAlgorithmException, FileNotFoundException,
             IOException, KeyStoreException,
             KeyManagementException, UnrecoverableKeyException,
             CertificateException {
        if ((keystore != null) || (truststore != null)) {
            SSLContext sslc = createContext
                (truststore, trusttype, trustpass,
                 keystore, storetype, storepass,
                 keyalias, keypass, protocol);
            return sslc.getServerSocketFactory();
        } else return SSLServerSocketFactory.getDefault();
    }

    /** Returns a factory for creating secure client sockets. */
    public static SocketFactory createClient
        (String truststore, String trusttype, char[] trustpass,
         String keystore,   String storetype, char[] storepass,
         String keyalias,   char[] keypass,   String protocol) throws
             NoSuchAlgorithmException, FileNotFoundException,
             IOException, KeyStoreException,
             KeyManagementException, UnrecoverableKeyException,
             CertificateException {
        if ((keystore != null) || (truststore != null)) {
            SSLContext sslc = createContext
                (truststore, trusttype, trustpass,
                 keystore, storetype, storepass,
                 keyalias, keypass, protocol);
            return sslc.getSocketFactory();
        } else return SSLSocketFactory.getDefault();
    }

    /** Settings for SSL configuration. */
    public static class Options extends Settings.Base {
        public String truststore = null;
        public String trusttype  = null;
        public char[] trustpass  = null;
        public String keystore   = null;
        public String storetype  = null;
        public char[] storepass  = null;
        public String keyalias   = null;
        public char[] keypass    = null;
        public String protocol   = "SSL";

        public void convert_truststore(String value)
        { truststore = value; }
        public void convert_trusttype(String value)
        { trusttype = value; }
        public void convert_trustpass(String value)
        { trustpass = value.toCharArray(); }
        public void convert_keystore(String value)
        { keystore = value; }
        public void convert_storetype(String value)
        { storetype = value; }
        public void convert_storepass(String value)
        { storepass = value.toCharArray(); }
        public void convert_keyalias(String value)
        { keyalias = value; }
        public void convert_keypass(String value)
        { keypass = value.toCharArray(); }
        public void convert_protocol(String value)
        { protocol = value; }
    }

    /** Returns a factory for creating secure server sockets. */
    public static ServerSocketFactory createServer(Options o) throws
             NoSuchAlgorithmException, FileNotFoundException,
             IOException, KeyStoreException,
             KeyManagementException, UnrecoverableKeyException,
             CertificateException {
        return createServer(o.truststore, o.trusttype, o.trustpass,
                            o.keystore,   o.storetype, o.storepass,
                            o.keyalias,   o.keypass,   o.protocol);
    }

    /** Returns a factory for creating secure client sockets. */
    public static SocketFactory createClient(Options o) throws
             NoSuchAlgorithmException, FileNotFoundException,
             IOException, KeyStoreException,
             KeyManagementException, UnrecoverableKeyException,
             CertificateException {
        return createClient(o.truststore, o.trusttype, o.trustpass,
                            o.keystore,   o.storetype, o.storepass,
                            o.keyalias,   o.keypass,   o.protocol);
    }

}
