// CSSJCA.java
// Copyright (C) 2008-2011 Thales e-Security Inc.  All rights reserved.
package com.thales.esecurity.asg.cssjca;

import java.lang.reflect.Method;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.ArrayList;
import java.io.InputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

/** A simple command line option parser with support for parsing
 *  configuration files.  Each type of setting has a value member
 *  which contains the user specified data after processing. */
public class Settings {

    /** Base exception for problems with setting parsing. */
    public static class Problem extends RuntimeException {
        public Problem(Throwable cause) { super(cause); }
        public Problem(java.lang.String msg) { super(msg); }
    }
    public static class UnknownSetting extends Problem {
        public java.lang.String name;
        public UnknownSetting(java.lang.String name)
        { super("Unknown setting \"" + name + "\""); this.name = name; }
    }
    public static class UnnecessaryArgument extends Problem {
        public java.lang.String name;
        public java.lang.String argument;
        public UnnecessaryArgument(java.lang.String name,
                                   java.lang.String argument) {
            super("Setting " + name + " does not accept an argument " +
                  "but \"" + argument + "\" was provided.");
            this.name = name; this.argument = argument;
        }
    }
    public static class InvalidArgument extends Problem {
        public java.lang.String name;
        public java.lang.String argument;
        public InvalidArgument(java.lang.String name,
                               java.lang.String argument,
                               java.lang.String msg) {
            super("Setting " + name + " cannot accept argument \"" +
                  argument + "\": " + msg);
            this.name = name; this.argument = argument;
        }
    }
    public static class MissingArgument extends Problem {
        public java.lang.String name;
        public MissingArgument(java.lang.String name)
        { super("Setting " + name + " requires an argument."); }
    }
    public static class ConfigProblem extends Problem {
        java.lang.String file;
        public ConfigProblem(java.lang.String file,
                             java.lang.String msg)
        { super(msg); this.file = file; }
    }

    public static class UnknownProblem extends Problem
    { public UnknownProblem(Throwable cause) { super(cause); } }

    /** Represents one or more settings that can be configured by
     *  command line arguments or a configuration file. */
    public static interface Settable {
        /** Returns a name representing the option specified by the
         *  given {@link java.lang.String} or null if this object
         *  doesn't support any setting by that name. */
        public java.lang.String match(java.lang.String option);

        /** Associate the setting specified by <code>name</code> with
         *  <code>arg</code> if possible.  A <code>Problem</code>
         *  subclass is generally thrown if not. */
        public void convert(java.lang.String name,
                            java.lang.String arg);

        /** Returns true iff the configuration option is permissible
         *  to call without an argument.  Options that are essentially
         *  switches will return true here.  Options that take
         *  mandatory arguments will return false. */
        public boolean apply(java.lang.String lname);
    }

    public static class Base implements Settable {
        public boolean applied = false;
        protected java.lang.String descr = null;
        protected java.lang.String name = null;
        
        /** Creates a degenerate Setting suitable for subclass use. */
        protected Base() {}

        /** Creates a configuration option with a single long name. */
        public Base(java.lang.String name) { this.name = name; }

        /** Returns the setting specific apply method or null if none
         *  exists. */
        private Method subApply(java.lang.String name) {
            Method result = null;
            try {
                return getClass().getMethod
                    ("apply_" + name);
            } catch (NoSuchMethodException ex) {}
            return result;
        }
        
        /** Returns the setting specific convert method or null if
         *  none exists. */
        private Method subConvert(java.lang.String name) {
            Method result = null;
            try {
                return getClass().getMethod
                    ("convert_" + name, java.lang.String.class);
            } catch (NoSuchMethodException ex) {}
            return result;
        }

        /** Implements Settable. */
        public java.lang.String match(java.lang.String name) {
            if ((subConvert(name) != null) ||
                (subApply(name) != null) ||
                ((this.name != null) && (this.name.equals(name))))
                return name;
            return null;
        }

        /** Implements Settable. */
        public void convert(java.lang.String name,
                            java.lang.String arg) {
            Method m = subConvert(name);
            if (m != null) {
                try {
                    m.invoke(this, arg);
                } catch (IllegalAccessException ex) {
                } catch (InvocationTargetException ex) {
                    Throwable cause = ex.getCause();
                    if (cause instanceof Problem)
                        throw (Problem)cause;
                    else throw new UnknownProblem(cause);
                }
            }
        }

        /** Implements Settable. */
        public boolean apply(java.lang.String name) {
            applied = true;
            try {
                Method m = subApply(name);
                if (m != null) {
                    m.invoke(this);
                    return true;
                }
            } catch (IllegalAccessException ex) {
            } catch (InvocationTargetException ex) {
                Throwable cause = ex.getCause();
                if (cause instanceof Problem)
                    throw (Problem)cause;
                else throw new UnknownProblem(cause);
            }
            return subConvert(name) == null;
        }
    }
    
    private ArrayList<Settable> settings = new ArrayList<Settable>();
    public Settings() {}
    public <T extends Settable> T add(T s)
    { settings.add(s); return s; }

    protected static java.lang.String mkArg   = "=";
    protected static java.lang.String mkLong  = "--";

    public List parseArgs(Iterable<java.lang.String> args) 
    {
        Settable         nextSetting = null;
        java.lang.String nextName    = null;
        boolean fill = false;
        ArrayList<java.lang.String> extra = new ArrayList<java.lang.String>();
        for (java.lang.String arg : args) 
        {
            if (fill) 
            {
                extra.add(arg);
            } 
            else if (nextSetting != null) 
            {
                nextSetting.convert(nextName, arg);
                nextSetting = null;
            } 
            else 
            {
                if (arg.equals(mkLong)) 
                {
                    fill = true;
                } 
                else if (arg.startsWith(mkLong)) 
                {
                    boolean matched = false;
                    for (Settable setting : settings) 
                    {
                        java.lang.String lname = null;
                        matched = true;
                        int marg = arg.indexOf(mkArg);
                        if ((marg >= 0) && ((lname = setting.match(arg.substring(mkLong.length(), marg))) != null)) 
                        {
                            if (!setting.apply(lname))
                            {
                                setting.convert(lname, arg.substring(marg + 1));
                            }
                            else 
                            {
                                throw new UnnecessaryArgument(lname, arg.substring(marg + 1));
                            }
                        } else if ((lname = setting.match(arg.substring(mkLong.length()))) != null) 
                        {
                            if (!setting.apply(lname)) 
                            {
                                nextSetting = setting;
                                nextName = lname;
                            }
                        } 
                        else
                        {
                            matched = false;
                        }
                        if (matched) break;
                    }
                    if (!matched)
                    {
                        throw new UnknownSetting(arg);
                    }
                } 
                else
                {
                    extra.add(arg);
                }
            }
        }
        return extra;
    }

    public List<java.lang.String> parseArgs(java.lang.String[] args)
    { 
        return parseArgs(Arrays.asList(args)); 
    }

    public void parseConfig(BufferedReader reader) throws IOException {
        java.lang.String line;
        while ((line = reader.readLine()) != null) {
            if (line.matches("^\\s*(#.*)?")) // empty and comments lines
                continue;

            java.lang.String name;
            boolean matched = false;
            int point = line.indexOf(mkArg);
            if (point >= 0) {
                name = line.substring(0, point).trim();
                java.lang.String arg =
                    line.substring(point + mkArg.length());
                for (Settable setting : settings) {
                    if (setting.match(name) != null) {
                        if (!setting.apply(name))
                            setting.convert(name, arg);
                        else throw new UnnecessaryArgument(name, arg);
                        matched = true;
                        break;
                    }
                }
            } else {
                name = line.trim();
                for (Settable setting : settings) {
                    if (setting.match(name) != null) {
                        if (!setting.apply(name))
                            throw new MissingArgument(name);
                        matched = true;
                        break;
                    }
                }
            }
            if (!matched)
                throw new UnknownSetting(name);
        }
    }

    public void parseConfig(InputStreamReader stream) throws IOException
    { parseConfig(new BufferedReader(stream)); }

    public void parseConfig(InputStream stream) throws IOException
    { parseConfig(new BufferedReader(new InputStreamReader(stream))); }

    /** A simple switch setting.  A public <code>value</code>
     *  field permits access to the parsed result. */
    public static class Switch extends Base {
        public boolean value = false;
        public Switch(java.lang.String name) { super(name); }
        public boolean apply(java.lang.String name)
        { value = true; return true; }
    }

    /** A simple counter setting.  A public <code>value</code>
     *  field permits access to the parsed result. */
    public static class Counter extends Base {
        public int value;
        public Counter(java.lang.String name, int defl)
        { super(name);  value = defl; }
        public Counter(java.lang.String name) { this(name, 0); }
        public boolean apply(java.lang.String name)
        { value++; return true; }
    }

    /** A simple string setting.  A public <code>value</code>
     *  field permits access to the parsed result. */
    public static class String extends Base {
        public java.lang.String value;
        public String(java.lang.String name, java.lang.String defl)
        { super(name);  value = defl; }
        public String(java.lang.String name) { this(name, null); }
        public boolean apply(java.lang.String name) { return false; }
        public void convert(java.lang.String name,
                            java.lang.String arg) { value = arg; }
    }

    /** A simple integer setting.  A public <code>value</code>
     *  field permits access to the parsed result. */
    public static class Integer extends Base {
        public int value;
        public Integer(java.lang.String name, int defl)
        { super(name);  value = defl; }
        public Integer(java.lang.String name) { this(name, 0); }
        public boolean apply(java.lang.String name) { return false; }
        public void convert(java.lang.String name,
                            java.lang.String arg)
        { value = check(name, arg); }

        public static int check(java.lang.String name,
                            java.lang.String arg) {
            try { return java.lang.Integer.parseInt(arg); }
            catch (NumberFormatException ex) {
                throw new InvalidArgument
                    (name, arg, "must be an integer.");
            }
        }
    }

    /** A simple integer setting.  A public <code>value</code>
     *  field permits access to the parsed result. */
    public static class Long extends Base {
        public long value;
        public Long(java.lang.String name, long defl)
        { super(name);  value = defl; }
        public Long(java.lang.String name) { this(name, 0); }
        public boolean apply(java.lang.String name) { return false; }
        public void convert(java.lang.String name,
                            java.lang.String arg)
        { value = check(name, arg); }

        public static long check(java.lang.String name,
                                 java.lang.String arg) {
            try { return java.lang.Long.parseLong(arg); }
            catch (NumberFormatException ex) {
                throw new InvalidArgument
                    (name, arg, "must be a long integer.");
            }
        }
    }

    /** An internet address setting.  A public <code>value</code>
     *  field permits access to the parsed result. */
    public static class Address extends Base {
        public InetAddress value;
        public Address(java.lang.String name, InetAddress defl)
        { super(name);  value = defl; }
        public Address(java.lang.String name) { this(name, null); }
        public boolean apply(java.lang.String name) { return false; }
        public void convert(java.lang.String name,
                            java.lang.String arg)
        { value = check(name, arg); }

        public static InetAddress check(java.lang.String name,
                                        java.lang.String arg) {
            try {
                return InetAddress.getByName(arg);
            } catch (UnknownHostException ex) {
                throw new InvalidArgument(name, arg, "host unknown.");
            }
        }
    }    

    /** A print stream setting.  A public <code>value</code> field
     *  permits access to the parsed result. */
    public static class PrintStream extends Base {
        public java.io.PrintStream value;
        public PrintStream(java.lang.String name,
                           java.io.PrintStream defl)
        { super(name);  value = defl; }
        public PrintStream(java.lang.String name) { this(name, null); }
        public boolean apply(java.lang.String name) { return false; }
        public void convert(java.lang.String name,
                            java.lang.String arg)
        { value = check(name, arg); }

        public static java.io.PrintStream check(java.lang.String name,
                                                java.lang.String arg) {
            try {
                return new java.io.PrintStream
                    (new FileOutputStream(arg, true));
            } catch (FileNotFoundException ex) {
                throw new InvalidArgument(name, arg, "file not found.");
            }
        }
    }    

    /** Configuration file setting.  The entire file is read and
     *  parsed at configuration time, so the order in which options
     *  are presented matters. */
    public static class Config implements Settable {
        private Settings settings;
        private java.lang.String name;

        public Config(Settings s, java.lang.String n,
                      java.lang.String defl) {
            name = n; settings = s;
            if (defl != null)
                doConfig(defl);
        }
        public java.lang.String match(java.lang.String option)
        { return name.equals(option) ? name : null; }
        public void convert(java.lang.String name,
                            java.lang.String arg) { doConfig(arg); }
        public boolean apply(java.lang.String lname) { return false; }

        private void doConfig(java.lang.String arg) {
            try {
                settings.parseConfig(new FileInputStream(arg));
            } catch (FileNotFoundException ex) {
                throw new ConfigProblem(arg, ex.getMessage());
            } catch (IOException ex) {
                throw new ConfigProblem(arg, ex.getMessage());
            } catch (Settings.Problem ex) {
                throw new ConfigProblem(arg, ex.getMessage());
            }
        }
    }

    /** A simple unit test. */
    public static void main(java.lang.String[] args) {
        Settings settings = new Settings();
        Integer apple = settings.add(new Integer("apple"));
        String banana = settings.add(new String("banana"));

        class Color extends Base {
            public int shiny = 0;
            public int red   = 0;
            public int green = 0;
            public int blue  = 0;

            public void apply_shiny() { shiny++; }
            public void convert_red(java.lang.String arg)
            { red = Integer.check("red", arg); }
            public void convert_green(java.lang.String arg)
            { green = Integer.check("green", arg); }
            public void convert_blue(java.lang.String arg)
            { blue = Integer.check("blue", arg); }
        }
        Color c = settings.add(new Color());

        settings.parseArgs(args);
        System.out.println("apple  = " + apple.value);
        System.out.println("banana = " + banana.value);
        System.out.println("shiny = " + c.shiny);
        System.out.println("red = " + c.red);
        System.out.println("green = " + c.green);
        System.out.println("blue = " + c.blue);
    }
}
