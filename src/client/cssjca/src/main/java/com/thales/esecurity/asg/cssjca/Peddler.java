// Peddler.java
// Copyright (C) 2011 Thales e-Security Inc.  All rights reserved.
package com.thales.esecurity.asg.cssjca;


import java.applet.Applet;
import java.awt.Toolkit;
import java.awt.Image;
import java.awt.Component;
import java.awt.Container;
import java.awt.Panel;
import java.awt.CardLayout;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.TextField;
import java.awt.TextArea;
import java.awt.MenuBar;
import java.awt.Menu;
import java.awt.MenuItem;
import java.awt.MenuShortcut;
import java.awt.Button;
import java.awt.Frame;
import java.awt.FileDialog;
import java.awt.event.WindowListener;
import java.awt.event.WindowEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.Date;
import java.util.Enumeration;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;

public class Peddler extends Applet
    implements WindowListener, ActionListener, Runnable {
    private KeyStore inputks = null;
    private FileDialog chooser = null;
    private final Label status = new Label("Select New or Load from File menu to begin.");
    private final Panel cardp = new Panel();
    private final CardLayout cards = new CardLayout(5, 5);
    private final TextArea console = new TextArea();
    private final TextField kstype = new TextField(KeyStore.getDefaultType(), 8);
    private final TextField kspass = new TextField("", 8);
    private final TextField ksname = new TextField("", 25);
    private final TextArea  ksview = new TextArea(2, 0);
    private final Button    create_button = new Button("Create");
    private final TextField cssurl = new TextField("https://css.example.com:443", 25);
    private final TextField oauthurl = new TextField("https://css.example.com:443", 25);
    private final TextField alias   = new TextField("", 8);
    private final TextField cssout  = new TextField("", 25);
    private final TextField cssin   = new TextField("", 25);
    private final TextField csspass = new TextField("", 8);
    private final TextField cssname  = new TextField("", 25);
    private final TextField csspassv = new TextField("", 8);
    private final TextField cssurlv  = new TextField("", 25);
    private final TextField cssaliav = new TextField("", 8);
    private final TextArea  cssview  = new TextArea();

    private CSSJCA.KSData cssksd = null;

    private Thread viewer = null;
    private boolean update_view = false;

    private void reportException(Exception ex) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(baos);
        ex.printStackTrace(out);
        console.setText(new String(baos.toByteArray()));
        status.setText("Error: " + ex.getMessage());
    }

    private void setKSData(CSSJCA.KSData ksd, String fname,
                           String passwd) throws Exception {
        cssksd = ksd;
        cssurlv.setText(ksd.serverUrl);
        cssaliav.setText(ksd.alias);
        cssname.setText(fname);
        csspassv.setText(passwd);
        cards.show(cardp, "View");
        repaint();
        synchronized (this) {
            cssview.setText("Working...");
            update_view = true;
            notify();
        }
    }

    @Override
    public void run() {
        // All of this complexity is an attempt to make viewing the
        // contents of a CSSJCA KeyStore responsive.  This has to be
        // done outside the UI thread because it could take a while.
        // Also, if one request is already in progress but the user
        // gives up (for example, because a connection is taking a
        // while to timeout or the wrong file was selected) it's
        // important to display only the most recent request.
        //
        // An ideal implementation might cancel a pending request
        // when a new one comes in, but it's not clear that a
        // URLConnection will support that.
        while (!Thread.interrupted()) {
            try {
                CSSJCA.KSData ksd = null;
                synchronized (this) {
                    if (!update_view) {
                        wait();
                        if (viewer == null)
                            break;
                    }
                    if (update_view) {
                        ksd = cssksd;
                        update_view = false;
                    }
                }
                if (ksd != null) {
                    StringBuilder sb = new StringBuilder();
                    Enumeration<String> aliases = ksd.ks.aliases();
                    while (aliases.hasMoreElements()) {
                        String strAlias = aliases.nextElement();
                        if (ksd.ks.isKeyEntry(strAlias) &&
                            ksd.ks.getCertificateChain(strAlias) != null)
                            sb.append("Client: " + strAlias + "\n");
                    }
                    for (String name : ksd.update().keySet())
                        sb.append("Key: " + name + "\n");
                    synchronized (this) {
                        if (!update_view)
                            cssview.setText(sb.toString());
                    }
                }
            } catch (InterruptedException ex) {
                break;
            } catch (Exception ex) {
                synchronized (this) {
                    ByteArrayOutputStream baos =
                        new ByteArrayOutputStream();
                    PrintStream out = new PrintStream(baos);
                    ex.printStackTrace(out);
                    cssview.setText(new String(baos.toByteArray()));
                }
            }
        }
    }

    /** Returns true iff it should be possible to create a KeyStore. */
    private boolean createReady() {
        return ((cssurl.getText().length() > 0) &&
                (cssout.getText().length() > 0) &&
                (inputks != null));
    }

    private Button addButton(Container c, Button b, String constraint) {
        b.addActionListener(this);
        c.add(b, constraint);
        return b;
    }

    @Override
    public void init() {
        Component parent, top = getParent();
        while ((parent = top.getParent()) != null)
            top = parent;
        chooser = new FileDialog
            ((Frame)top, "CSSJCA - Select input KeyStore");
        cardp.setLayout(cards);
        cardp.add(new Panel(), "Blank");

        Panel crashp = new Panel();
        crashp.setLayout(new GridLayout());
        console.setEditable(false);
        crashp.add(console, "STACK");
        cardp.add(crashp, "Console");

        Panel aboutp = new Panel();
        aboutp.setLayout(new GridLayout());
        TextArea about_text = new TextArea
            ("CSSJCA " + CSSJCA.version + "\n" +
             "Copyright (C) 2011 by Thales e-Security Inc.\n" +
             "All rights reserved.\n\n" +
             "This program creates and manages CSSJCA KeyStores");
        about_text.setEditable(false);
        aboutp.add(about_text);
        cardp.add(aboutp, "About");

        Panel viewp = new Panel();
        viewp.setLayout(new SpineLayout());
        viewp.add(new Label("Code Sign Server URL"), "LEFT");
        viewp.add(cssurlv, "RIGHT");
        viewp.add(new Label("Client Alias (optional)"), "LEFT");
        viewp.add(cssaliav, "RIGHT");
        viewp.add(new Label("CSSJCA pass phrase"), "LEFT");
        csspassv.setEchoChar('*');
        viewp.add(csspassv, "RIGHT");
        addButton(viewp, new Button("CSSJCA file name"), "LEFT");
        cssname.setEnabled(false);
        viewp.add(cssname, "RIGHT");
        addButton(viewp, new Button("Refresh"), "BOTTOM");
        addButton(viewp, new Button("Save"), "BOTTOM");
        cssview.setEditable(false);
        viewp.add(cssview, "STACK");
        cardp.add(viewp, "View");

        Panel loadp = new Panel();
        loadp.setLayout(new SpineLayout());
        loadp.add(new Label("CSSJCA pass phrase"), "LEFT");
        csspass.setEchoChar('*');
        loadp.add(csspass, "RIGHT");
        addButton(loadp, new Button("CSSJCA input file"), "LEFT");
        cssin.setEnabled(false);
        loadp.add(cssin, "RIGHT");
        addButton(loadp, new Button("Open"), "BOTTOM");
        cardp.add(loadp, "Load");

        Panel newp = new Panel();
        newp.setLayout(new SpineLayout());
        newp.add(new Label("Code Sign Server URL"), "LEFT");
        newp.add(cssurl, "RIGHT");
        newp.add(new Label("Client Alias (optional)"), "LEFT");
        newp.add(alias, "RIGHT");
        newp.add(new Label("KeyStore pass phrase"), "LEFT");
        kspass.setEchoChar('*');
        newp.add(kspass, "RIGHT");
        newp.add(new Label("KeyStore type"), "LEFT");
        newp.add(kstype, "RIGHT");
        addButton(newp, new Button("KeyStore input file"), "LEFT");
        ksname.setEnabled(false);
        newp.add(ksname, "RIGHT");
        addButton(newp, new Button("CSSJCA output file"), "LEFT");
        cssout.setEnabled(false);
        newp.add(cssout, "RIGHT");
        addButton(newp, create_button, "BOTTOM").setEnabled(false);
        ksview.setEditable(false);
        newp.add(ksview, "STACK");
        cardp.add(newp, "New");

        setLayout(new BorderLayout());
        add(status, BorderLayout.PAGE_END);
        add(cardp, BorderLayout.CENTER);
    }

    @Override
    public synchronized void start() {
        if (viewer == null) {
            viewer = new Thread(this);
            viewer.start();
        }
    }
    @Override
    public synchronized void stop() {
        Thread t = viewer;
        viewer = null;
        t.interrupt();
    }

    // ActionListener Implementation -----------------------------------
    @Override
    public void actionPerformed(ActionEvent ev) {
        try {
            String action = ev.getActionCommand();
            if (action.equals("Quit"))
                System.exit(0);
            else if (action.equals("Console")) {
                cards.show(cardp, "Console");
                status.setText("Most recent Java exception");
            } else if (action.equals("About")) {
                cards.show(cardp, "About");
                status.setText("");
            } else if (action.equals("New")) {
                cards.show(cardp, "New");
                status.setText("Create a new CSSJCA KeyStore");
            } else if (action.equals("Load")) {
                cards.show(cardp, "Load");
                status.setText("Load an existing CSSJCA KeyStore");
            } else if (action.equals("KeyStore input file")) {
                chooser.setMode(FileDialog.LOAD);
                chooser.show();
                String fname = chooser.getFile();
                if (fname != null) {
                    KeyStore ks = KeyStore.getInstance
                        (kstype.getText());
                    ks.load(new FileInputStream(fname),
                            kspass.getText().toCharArray());
                    ksname.setText(fname);
                    status.setText("Loaded " + kstype.getText() +
                                   " KeyStore " + ksname.getText());
                    inputks = ks;
                    if (createReady())
                        create_button.setEnabled(true);

                    ksview.setText("");
                    StringBuilder sb = new StringBuilder();
                    Enumeration<String> aliases = ks.aliases();
                    while (aliases.hasMoreElements()) {
                        String current = aliases.nextElement();
                        if (ks.isKeyEntry(current)) {
                            sb.append("Key: " + current + "\n");
                            for (Certificate c :
                                     ks.getCertificateChain(current)) {
                                X509Certificate xc = (X509Certificate)c;
                                sb.append
                                    ("  Subject: " +
                                     xc.getSubjectX500Principal() + "\n");
                                sb.append
                                    ("  Issuer: " +
                                     xc.getIssuerX500Principal() + "\n");
                            }
                        } else {
                            sb.append("Cert: " + current + "\n");
                            X509Certificate xc = (X509Certificate)
                                ks.getCertificate(current);
                            sb.append
                                ("  Subject: " +
                                 xc.getSubjectX500Principal() + "\n");
                            sb.append
                                ("  Issuer: " +
                                 xc.getIssuerX500Principal() + "\n");
                        }
                    }
                    ksview.setText(sb.toString());
                }
            } else if (action.equals("CSSJCA output file")) {
                chooser.setMode(FileDialog.SAVE);
                chooser.show();
                String fname = chooser.getFile();
                if (fname != null) {
                    cssout.setText(fname);
                    if (createReady())
                        create_button.setEnabled(true);
                }
            } else if (action.equals("Create")) {
                char[] chpass = kspass.getText().toCharArray();
                CSSJCA.KSData ksd = new CSSJCA.KSData
                    (inputks, chpass, alias.getText(),
                     cssurl.getText(), oauthurl.getText(), "");
                FileOutputStream f = null;
                try {
                    f = new FileOutputStream(cssout.getText());
                    ksd.store(new ObjectOutputStream(f), chpass);
                    setKSData(ksd, cssout.getText(), kspass.getText());
                    status.setText("Created CSSJCA KeyStore " +
                                   cssout.getText());
                } finally { if (f != null) f.close(); }
            } else if (action.equals("CSSJCA input file")) {
                char[] chpass = csspass.getText().toCharArray();
                chooser.setMode(FileDialog.LOAD);
                chooser.show();
                String fname = chooser.getFile();
                if (fname != null) {
                    cssin.setText(fname);
                    FileInputStream f = null;
                    try {
                        f = new FileInputStream(cssin.getText());
                        CSSJCA.KSData ksd = CSSJCA.KSData.load
                            (new ObjectInputStream(f), chpass);
                        setKSData(ksd, cssin.getText(),
                                  csspass.getText());
                        status.setText("Loaded CSSJCA KeyStore " +
                                       cssin.getText());
                    } finally { if (f != null) f.close(); }
                }
            } else if (action.equals("Open")) {
                char[] chpass = csspass.getText().toCharArray();
                FileInputStream f = null;
                try {
                    f = new FileInputStream(cssin.getText());
                    CSSJCA.KSData ksd = CSSJCA.KSData.load
                        (new ObjectInputStream(f), chpass);
                    setKSData(ksd, cssin.getText(), csspass.getText());
                    status.setText("Loaded CSSJCA KeyStore " +
                                   cssin.getText());
                } finally { if (f != null) f.close(); }
            } else if (action.equals("CSSJCA file name")) {
                chooser.setMode(FileDialog.SAVE);
                chooser.show();
                String fname = chooser.getFile();
                if (fname != null)
                    cssname.setText(fname);
            } else if (action.equals("Refresh")) {
                cssksd.serverUrl = cssurlv.getText();
                cssksd.alias = cssaliav.getText();
                setKSData(cssksd, cssname.getText(),
                          csspassv.getText());
                status.setText("Refreshed at " + new Date());
            } else if (action.equals("Save")) {
                char[] chpass = csspassv.getText().toCharArray();
                cssksd.serverUrl = cssurlv.getText();
                cssksd.alias = cssaliav.getText();
                FileOutputStream f = null;
                try {
                    f = new FileOutputStream(cssname.getText());
                    cssksd.store(new ObjectOutputStream(f), chpass);
                    setKSData(cssksd, cssname.getText(),
                              csspassv.getText());
                    status.setText("Saved CSSJCA KeyStore " +
                                   cssname.getText());
                } finally { if (f != null) f.close(); }
            }
        } catch (Exception ex) { reportException(ex); }
    }

    // WindowListener Implementation -----------------------------------
    @Override
    public void windowOpened(WindowEvent ev) {}
    @Override
    public void windowClosing(WindowEvent ev) { System.exit(0); }
    @Override
    public void windowClosed(WindowEvent ev) {}
    @Override
    public void windowIconified(WindowEvent ev) {}
    @Override
    public void windowDeiconified(WindowEvent ev) {}
    @Override
    public void windowActivated(WindowEvent ev) {}
    @Override
    public void windowDeactivated(WindowEvent ev) {}

    public static void main(String[] args) {
	Peddler p = new Peddler();
        MenuItem menu_new = new MenuItem("New");
        menu_new.setShortcut(new MenuShortcut(KeyEvent.VK_N, false));
        menu_new.addActionListener(p);
        MenuItem menu_load = new MenuItem("Load");
        menu_load.setShortcut(new MenuShortcut(KeyEvent.VK_L, false));
        menu_load.addActionListener(p);
        MenuItem menu_quit = new MenuItem("Quit");
        menu_quit.setShortcut(new MenuShortcut(KeyEvent.VK_Q, false));
        menu_quit.addActionListener(p);
        MenuItem menu_console = new MenuItem("Console");
        menu_console.setShortcut
            (new MenuShortcut(KeyEvent.VK_C, false));
        menu_console.addActionListener(p);
        MenuItem menu_about = new MenuItem("About");
        menu_about.setShortcut(new MenuShortcut(KeyEvent.VK_A, false));
        menu_about.addActionListener(p);

        Menu menu_file = new Menu("File");
        menu_file.setShortcut(new MenuShortcut(KeyEvent.VK_F, false));
        menu_file.add(menu_new);
        menu_file.add(menu_load);
        menu_file.add(menu_quit);
        Menu menu_tools = new Menu("Tools");
        menu_tools.setShortcut(new MenuShortcut(KeyEvent.VK_T, false));
        menu_tools.add(menu_console);
        Menu menu_help = new Menu("Help");        
        menu_help.setShortcut(new MenuShortcut(KeyEvent.VK_H, false));
        menu_help.add(menu_about);
        MenuBar mb = new MenuBar();
        mb.add(menu_file);
        mb.add(menu_tools);
        mb.add(menu_help);

        Toolkit tk = Toolkit.getDefaultToolkit();
	Frame f = new Frame("CSSJCA - KeyStore Utility");
	f.addWindowListener(p);
	f.add(p);
	f.setSize(480, 320);
        f.setMenuBar(mb);
        Image img;
        img = tk.getImage(p.getClass().getClassLoader().getResource("images/icon.png"));
        f.setIconImage(img);
      //  f.setIconImage(tk.getImage(p.getClass().getClassLoader().
      //                             getResource("images/icon.png")));
        p.init();
        f.pack();
	f.show();
	p.start();
    }
}
