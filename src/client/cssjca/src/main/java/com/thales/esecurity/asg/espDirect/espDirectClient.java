/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thales.esecurity.asg.espDirect;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.thales.esecurity.asg.cssjca.CSSJCA;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSession;


/**
 *
 * @author mike
 */
public class espDirectClient {
        
    String url;
    String oauthUrl;
    String oauthClientId;
    private SSLSocketFactory sf;
    

    public espDirectClient(String serverUrl, String oauthServerUrl, 
                           String oauthClient, SSLSocketFactory sf) {
        url = serverUrl;
        oauthUrl = oauthServerUrl;
        oauthClientId = oauthClient;
        this.sf = sf;
    }
    
    /** Constructs an HTTPS connection suitable for mutually
     *  authenticated communication with the Code Sign Server.
     * @param command
     * @param httpParameters
     * @param body
     * @return  */
    public HttpsURLConnection getConnection(String command,
                                            String httpParameters,
                                            String body) throws espDirectException
    {    

        String bearerToken = "";
        try
        {
            bearerToken = getBearerToken();        
        }
        catch(espDirectException ex)
        {
            System.out.println(".");
            System.out.println(ex.getMessage());
            espDirectException espEx;
            espEx = new espDirectException("Error communicating with OAuth server, IOException.");
            throw espEx;
        }
        
        HttpsURLConnection serverConn=null;
        try
        {
            // TODO: remove hardcoded subject.
            URL destination = new URL(url + "/api/espDirect/" + command);
            URLConnection uconn = destination.openConnection();
            uconn.setRequestProperty("User-Agent", "CSSJCA/" + CSSJCA.version);
            serverConn = (HttpsURLConnection)uconn;
            serverConn.setSSLSocketFactory(sf);
            serverConn.setHostnameVerifier(new HostnameVerifier() {
                        public boolean verify
                            (String hostname, SSLSession session)
                        { return true; /* accept any hostname */ }
                    });
            serverConn.setRequestProperty("access_token", bearerToken);            

            if (body.length() > 0) 
            {
                // set the request method and enable connection for writing output
                serverConn.setRequestMethod("POST");
                serverConn.setRequestProperty("Content-Type","application/json"); 

                serverConn.setUseCaches(false);
                serverConn.setDoInput(true);
                serverConn.setDoOutput(true);
                OutputStream os = serverConn.getOutputStream();
                os.write( body.getBytes() );    
                os.close();                
            }
            else
            {
                serverConn.setRequestMethod("GET");  
            }
        }  
        catch (JsonIOException ex)
        {
            System.out.println("Unable to read json stream.");                    
            System.out.println(ex.getMessage());                    
            espDirectException espEx = 
                    new espDirectException("Unable to read response stream from server.");
            throw espEx;
        } 
        catch (JsonSyntaxException ex) 
        {
            System.out.println("Response from server is not a valid json message.");
            System.out.println(ex.getMessage());
            espDirectException espEx = 
                    new espDirectException("Response from server is not a valid json message.");
            throw espEx;
        } 
        catch (IOException ex) 
        {
            System.out.println(".");
            System.out.println(ex.getMessage());
            espDirectException espEx = 
                    new espDirectException("Error communicating with server, IOException.");
            throw espEx;
            
        } 

        // write the
        return serverConn;
    }
        
    /** Authenticates to the OAuth server to obtain an authorization
     token for the Portal
     * @return 
     * @throws com.thales.esecurity.asg.espDirect.espDirectClient.espDirectException */
    public String getBearerToken() throws espDirectException
    {
        HttpsURLConnection serverConn;
        String contentParameters;
        int responseCode;
        Gson g = new Gson();
        Map<String, String> responseValues;
        String token = "";
        
        try
        {
            URL destination = new URL(oauthUrl);
            URLConnection uconn = destination.openConnection();
            uconn.setRequestProperty("User-Agent", "CSSJCA/" + CSSJCA.version);
            serverConn = (HttpsURLConnection)uconn;
            serverConn.setSSLSocketFactory(sf);
            serverConn.setHostnameVerifier(new HostnameVerifier() {
                        public boolean verify
                            (String hostname, SSLSession session)
                        { return true; /* accept any hostname */ }
                    });
                            
            serverConn.setRequestMethod("POST");
            serverConn.setRequestProperty("Content-Type","application/x-www-form-urlencoded"); 

            String clientId = "mduren";
            contentParameters = "client_id=";
            contentParameters += oauthClientId;
            contentParameters += "&grant_type=client_credentials";

            serverConn.setUseCaches(false);
            serverConn.setDoInput(true);
            serverConn.setDoOutput(true);
            OutputStream os = serverConn.getOutputStream();
            os.write( contentParameters.getBytes() );    
            os.close();                
            responseCode = serverConn.getResponseCode();

            System.out.println("\nSending 'GET' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);

            Reader reader = new InputStreamReader(serverConn.getInputStream(), "UTF-8");  
            
            responseValues = g.fromJson(reader, new TypeToken<Map<String, String>>(){}.getType());

            String tokenType = responseValues.get("token_type");
            String expiresIn = responseValues.get("expires_in");
            token = responseValues.get("access_token");
        }
        catch (JsonIOException ex)
        {
            System.out.println("Unable to read json stream.");                    
            System.out.println(ex.getMessage());                    
            espDirectException espEx = 
                    new espDirectException("Unable to read response stream from server.");
            throw espEx;
        } 
        catch (JsonSyntaxException ex) 
        {
            System.out.println("Response from server is not a valid json message.");
            System.out.println(ex.getMessage());
            espDirectException espEx = 
                    new espDirectException("Response from server is not a valid json message.");
            throw espEx;
        } 
        catch (IOException ex) 
        {
            System.out.println(".");
            System.out.println(ex.getMessage());
            espDirectException espEx = 
                    new espDirectException("Error communicating with server, IOException.");
            throw espEx;
            
        } 
        return token;
    }
    /** Collects information about available keys from the Code
     *  Sign Server.  This is likely to have relatively high
     *  latency and to change infrequently so it should not be
     *  called more often than necessary.
     * @return
     * @throws espDirectException*/
    public SigningType[] espListSigningTypes() throws espDirectException
    {
        HttpsURLConnection serverConn = null;
        APIListSigningTypes apiResponse;
        int responseCode;
        Reader reader;  
        SigningType[] signingTypes = null;
        try 
        {
            Gson g = new Gson();
            serverConn = getConnection("espListSigningProfiles", "", "");
            responseCode = serverConn.getResponseCode();

            System.out.println("\nSending 'GET' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);

            reader = new InputStreamReader(serverConn.getInputStream(), "UTF-8");  

            // convert the response to json
            apiResponse = g.fromJson(reader, APIListSigningTypes.class);

            if (apiResponse.returnCode.errorCode == 0)
            {
                signingTypes = apiResponse.signingTypes;
            }
            else
            {
                // log the error from the server
                System.out.println("Error returned from the server: " + 
                                   apiResponse.returnCode.errorCode);
                // log the error from the server
                System.out.println(apiResponse.returnCode.errorString);                    
            }
        }
        catch (espDirectException ex)
        {
            throw ex;
        } 
        catch (JsonSyntaxException ex) 
        {
            System.out.println("Response from server is not a valid json message.");
            System.out.println(ex.getMessage());
            espDirectException espEx = 
                    new espDirectException("Response from server is not a valid json message.");
            throw espEx;
        } 
        catch (IOException ex) 
        {
            System.out.println(".");
            System.out.println(ex.getMessage());
            espDirectException espEx = 
                    new espDirectException("Error communicating with server, IOException.");
            throw espEx;            
        } 
        finally
        {
            if (serverConn != null)
            {
                try {serverConn.disconnect();}catch(Exception ex){}
            }
        }

        return signingTypes;
    }
    
    /** Perform a raw signature with the key corresponding to
     *  the certificate associated with this key.
     * @param req
     * @return 
     * @throws com.thales.esecurity.asg.espDirect.espDirectClient.espDirectException */
    public String espSignHash(SignHashRequest req) throws espDirectException
    {
        HttpsURLConnection serverConn = null;
        APISignHash apiResponse;
        int responseCode;
        Reader reader;  
        String signatureValue = "";
        try 
        {
            Gson g = new Gson();
            Path path = Paths.get("signingMetadata.json");
            if (Files.exists((path)))
            {
                
                String metaInfo = new String(Files.readAllBytes(path), StandardCharsets.UTF_8);
                Map<String, String> dict;
                dict = g.fromJson(metaInfo, new TypeToken<Map<String, String>>(){}.getType());
                req.metadataField1Value = dict.get("MetadataField1Value");
                req.metadataField2Value = dict.get("MetadataField2Value");
                req.releaseName = dict.get("ReleaseName");
            }
            serverConn = getConnection("espSignHash", "", g.toJson(req));
            if (serverConn == null)
            {
                String errorMsg = "Unable to connect to server.";                
                espDirectException espEx = new espDirectException(errorMsg);
                espEx.apiErrorCode = -1; 
                throw espEx;
            }

            responseCode = serverConn.getResponseCode();

            System.out.println("\nSending 'GET' request to URL : " + url);
            System.out.println("Response Code : " + responseCode);

            if (responseCode != 200 && responseCode != 201)
            {
                serverConn.disconnect();
                String errorMsg = "HTTP Error returned: " + responseCode;                
                espDirectException espEx = new espDirectException(errorMsg);
                espEx.apiErrorCode = -1; 
                throw espEx;
            }
            reader = new InputStreamReader(serverConn.getInputStream(), "UTF-8");  

            // convert the response to json
            apiResponse = g.fromJson(reader, APISignHash.class);
                        
            if (apiResponse.returnCode.errorCode == 0)
            {
                signatureValue = apiResponse.response.signatureValue;
            }
            else
            {
                // log the error from the server
                System.out.println("Error returned from the server: " + 
                                   apiResponse.returnCode.errorCode);
                // log the error from the server
                System.out.println(apiResponse.returnCode.errorString);                    

                espDirectException espEx = 
                    new espDirectException("API Returned error: " + apiResponse.returnCode.errorString);
                espEx.apiErrorCode = apiResponse.returnCode.errorCode;
                throw espEx;
            }
        }
        catch (espDirectException ex)
        {
            throw ex;
        } 
        catch (JsonSyntaxException ex) 
        {
            System.out.println("Response from server is not a valid json message.");
            System.out.println(ex.getMessage());
            espDirectException espEx = 
                    new espDirectException("Response from server is not a valid json message.");
            throw espEx;
        } 
        catch (IOException ex) 
        {
            System.out.println(".");
            System.out.println(ex.getMessage());
            espDirectException espEx = 
                    new espDirectException("Error communicating with server, IOException.");
            throw espEx;            
        } 
        finally
        {
            if (serverConn != null)
            {
                try {serverConn.disconnect();}catch(Exception ex){}
            }
        }
        return signatureValue;
    }
    
    
    public class espDirectException extends Exception {
        public int apiErrorCode;
        public espDirectException(String message) {
            super(message);
        }
    }
}
