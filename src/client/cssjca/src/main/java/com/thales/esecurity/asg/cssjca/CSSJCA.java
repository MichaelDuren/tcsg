// CSSJCA.java
// Copyright (C) 2011 Thales e-Security Inc.  All rights reserved.
package com.thales.esecurity.asg.cssjca;

import com.google.gson.Gson;
import com.thales.esecurity.asg.espDirect.SignHashRequest;
import com.thales.esecurity.asg.espDirect.SigningType;
import com.thales.esecurity.asg.espDirect.espDirectClient;
import com.thales.esecurity.asg.espDirect.espDirectClient.espDirectException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InvalidObjectException;
import java.util.Date;
import java.util.Enumeration;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;
import java.util.ArrayList;
import java.security.Provider;
import java.security.Security;
import java.security.AccessController;
import java.security.KeyStore;
import java.security.MessageDigest;
import java.security.Signature;
import java.security.Key;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.KeyStoreSpi;
import java.security.SignatureSpi;
import java.security.SignatureException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.KeyManagementException;
import java.security.UnrecoverableKeyException;
import java.security.cert.X509Certificate;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateException;
import java.security.interfaces.RSAPublicKey;
import java.util.List;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.SSLContext;
import javax.security.auth.x500.X500Principal;

/** <p>A provider for the Java Cryptography Architecture which is
 *  backed by the Code Signing Server produced for Adobe by the Thales
 *  Advanced Solutions Group.  This provider introduces a
 *  <q>CSSJCA</q> KeyStore type.  Creating such a KeyStore requires a
 *  URL at which the Code Sign Server can be reached and an internal
 *  KeyStore which contains the credentials necessary to authenticate
 *  to the Code Sign Server in both directions.</p>
 *
 *  <p>Aliases exposed by a CSSJCA KeyStore are exactly the values
 *  advertised by the Code Sign Server and cannot be amended or
 *  altered by application code.  PrivateKey objects taken from a
 *  CSSJCA KeyStore can be used with a Signature object but since they
 *  correspond to keys maintained by the Code Sign Server there is no
 *  way to extract their key material.</p>
 *
 *  <p>The internal KeyStore object used to create a CSSJCA KeyStore
 *  must contain at least one TrustedCertificateEntry with a
 *  certificate reachable by the chain provided by the Code Sign
 *  Server.</p>
 *
 *  <p>Also, since the Code Sign Server requires client
 *  authentication, the internal KeyStore must contain at least one
 *  PrivateKeyEntry with a corresponding certificate.  If there is
 *  more than one such entry an alias may be specified to select one.
 *  Otherwise the process by which a PrivateKeyEntry is selected is
 *  undefined.</p>
 *
 *  <p>A TrustedCertificateEntry in the internal KeyStore will be
 *  stitched together with the certificate provided by the Code Sign
 *  Server, so that calling the {@link KeyStore.getCertificateChain}
 *  method may return an array with a size greater than one.  This
 *  stitching process relies exclusively on the distinguished names of
 *  the subject and issuer in X.509 certificates and does not perform
 *  any signature validation.</p>
 *
 *  <p>Only a TrustedCertificateEntry will be used for stitching.  The
 *  performance should be <code>O(nlog(n))</code> where <code>n</code>
 *  is the number of certificate entries.  A PrivateKeyEntry is never
 *  used for stitching.</p> */
public final class CSSJCA extends Provider
{
    private static final String kstag = "CSSJCA-KeyStore";
    public static final float version = 1.01f;

    private void init() {
        put("KeyStore.CSSJCA",
            "com.thales.esecurity.asg.cssjca.CSSJCA$KeyStoreImp");
        put("Signature.NONEwithRSA",
            "com.thales.esecurity.asg.cssjca.CSSJCA$NONEwithRSA");
        put("Signature.MD5withRSA",
            "com.thales.esecurity.asg.cssjca.CSSJCA$MD5withRSA");
        put("Signature.SHA1withRSA",
            "com.thales.esecurity.asg.cssjca.CSSJCA$SHA1withRSA");
        put("Signature.SHA256withRSA",
            "com.thales.esecurity.asg.cssjca.CSSJCA$SHA256withRSA");
        put("Signature.SHA384withRSA",
            "com.thales.esecurity.asg.cssjca.CSSJCA$SHA384withRSA");
        put("Signature.SHA512withRSA",
            "com.thales.esecurity.asg.cssjca.CSSJCA$SHA512withRSA");
    }

    public CSSJCA() {
        super("CSSJCA", version,
              "CSSJCA (RSA signing; CSSJCA keystore)");
        AccessController.doPrivileged
            (new java.security.PrivilegedAction()
                { @Override public Object run() { init(); return null; }});
    }

    // KeyStore Implementation -----------------------------------------

    /** Represents a private signature key corresponding to a certificate
     *  advertised by the Code Sign Server.  Because the Code Sign Server
     *  requires a complete Base64 encoded certificate as a selector this
     *  information is stored unaltered.  This class also stores the
     *  decoded X509Certificate since this is useful as well (primarily
     *  for fetching the corresponding public key). */
    static class CSSKey implements PrivateKey {
        private espDirectClient espClient;
        private KSData contents;
        public X509Certificate  decoded_cert;
        public String           encoded_cert;
        public String           certLabel;

        public CSSKey(KSData contents, espDirectClient espClient, 
                      CertificateFactory cf, String encoded_cert, String label)
            throws CertificateException {
            ByteArrayInputStream bais = new ByteArrayInputStream
                (Transform.b642byte(encoded_cert));
            this.decoded_cert = (X509Certificate)
                cf.generateCertificate(bais);
            this.encoded_cert = encoded_cert;
            this.contents = contents;
            this.certLabel = label;
            this.espClient = espClient;
        }

        /** Perform a raw signature with the key corresponding to
         *  the certificate associated with this key. */
        public byte[] sign(byte[] plain, String digestAlgorithm)
            throws SignatureException, espDirectException {

            SignHashRequest req = new SignHashRequest();
            

            String signatureValue;                   
            req.hashValue = Transform.byte2b64(plain);
            // convert to a digest alg string that the API recognizes by
            // removing the "-"
            req.hashAlg = digestAlgorithm.replaceAll("-", "");
            req.signingTypeGuid = certLabel;
            // returns the base64 encoded value
            signatureValue = espClient.espSignHash(req);

            return Transform.b642byte(signatureValue);
        }

        /** Implements PrivateKey */
        @Override public String getAlgorithm() { return "RSA"; }
        /** Implements PrivateKey */
        @Override public byte[] getEncoded() { return null; }
        /** Implements PrivateKey */
        @Override public String getFormat() { return null; }
    }

    static class KSConfigData
    {
        public String kstag;
        public float version;
        public String serverUrl;
        public String oauthUrl;
        public String oauthClientId;
        public String alias;
        public String type;        
    }
    
    /** Represents the stored aspects of the KeyStore.  In particular,
     *  this object contains the URL at which to reach the Code Sign
     *  Server, the alias within the internal KeyStore to use for
     *  client authentication and the internal KeyStore itself.  This
     *  object handles loading, storing and key list updates. */
    static class KSData {
        // Note that the certmap is not modified after the constructor
        // and so does not create thread safety problems.
        private TreeMap<String,ArrayList<X509Certificate>> certmap =
            new TreeMap<String,ArrayList<X509Certificate>>();
        private SSLSocketFactory sf;
        KeyStore ks;
        String alias;
        String serverUrl;
        String oauthUrl;
        String oauthClientId;
        private boolean bUpdated = false;
        private espDirectClient espClient = null;


        /** Creates the internal contents of the KeyStore. */
        KSData(KeyStore ks, char[] phrase, String alias, String url, 
               String oauthUrl, String oauthClientId)
            throws NoSuchAlgorithmException, KeyStoreException,
                   KeyManagementException, UnrecoverableKeyException 
        {
            // Makes it easy to look up certificates according to
            // their X509 subject.  This is used to dynamically
            // construct certificate chains for private keys.
            Enumeration<String> aliases = ks.aliases();
            while (aliases.hasMoreElements()) 
            {
                String current = aliases.nextElement();
                if (ks.isCertificateEntry(current)) 
                {
                    X509Certificate other = (X509Certificate)ks.getCertificate(current);
                    String name = other.getSubjectX500Principal().getName();
                    ArrayList<X509Certificate> mapitem = certmap.get(name);
                    if (mapitem == null) 
                    {
                        mapitem = new ArrayList<X509Certificate>();
                        certmap.put(name, mapitem);
                    }
                    mapitem.add(other);
                } 
                else if (ks.isKeyEntry(current)) 
                {
                    try 
                    {
                        ks.getKey(current, phrase);
                    } 
                    catch (UnrecoverableKeyException ex) 
                    {
                        throw new KeyStoreProblem
                            ("CSSJCA requires all key pass phrases " +
                             "to match the KeyStore but \"" + current +
                             "\" does not", ex);
                    }
                }
            }

            SSLContext ssl_ctx = SSLHelper.createContext(ks, ks, alias, phrase, "TLS");
            this.sf    = ssl_ctx.getSocketFactory();
            this.serverUrl   = url;
            this.oauthUrl = oauthUrl;
            this.oauthClientId = oauthClientId;
            this.ks    = ks;
            this.alias = alias;
            espClient = new espDirectClient(url, oauthUrl, oauthClientId, this.sf);
        }

        /** Writes this KeyStore to an ObjectOutputStream for offline
         *  storage. */
        public void store(ObjectOutputStream out, char[] passwd)
            throws IOException, KeyStoreException,
                   NoSuchAlgorithmException, CertificateException 
        {
            KSConfigData configInfo;
            configInfo = new KSConfigData();
            configInfo.kstag = CSSJCA.kstag;
            configInfo.alias = (alias != null) ? alias : "";
            configInfo.oauthUrl = oauthUrl;
            configInfo.oauthClientId = oauthClientId;
            configInfo.serverUrl = serverUrl;
            configInfo.version = CSSJCA.version;
            configInfo.type = ks.getType();
            
            // serialize the config data into a json object
            Gson g = new Gson();
            String configString = g.toJson(configInfo, KSConfigData.class);

            out.writeUTF(configString);
            ks.store(out, passwd);
        }

        /** Reads a stored KeyStore for use in an application. */
        public static KSData load(ObjectInputStream in, char[] passwd)
            throws IOException, KeyStoreException,
                   NoSuchAlgorithmException, CertificateException,
                   UnrecoverableKeyException, KeyManagementException 
        {
            // serialize the config data into a json object
            Gson g = new Gson();            
            KSConfigData configInfo = g.fromJson(in.readUTF(), KSConfigData.class);

            if (!configInfo.kstag.equals(CSSJCA.kstag))
            {
                throw new InvalidObjectException("CSSJCA: Stream is not a CSSJCA KeyStore.");
            }
            float version = configInfo.version;
            if ((int)version != (int)CSSJCA.version)
            {
                throw new UnsupportedOperationException
                    ("CSSJCA: provider version " + CSSJCA.version +
                     "cannot support version " + version +
                     " KeyStores.");
            }
            String url = configInfo.serverUrl;
            String oauthUrl = configInfo.oauthUrl;
            String oauthClientId = configInfo.oauthClientId;
            String alias = configInfo.alias;
            if (alias.length() == 0)
            {
                alias = null;
            }
            String kstype = configInfo.type;
            KeyStore ks = KeyStore.getInstance(kstype);
            ks.load(in, passwd);
            return new KSData(ks, passwd, alias, url, oauthUrl, oauthClientId);
        }


        
        /** Collects information about available keys from the Code
         *  Sign Server.  This is likely to have relatively high
         *  latency and to change infrequently so it should not be
         *  called more often than necessary. */
        public Map<String,CSSKey> update() throws CertificateException, espDirectException 
        {            
            SigningType signingTypes[];
            TreeMap<String,CSSKey> result = new TreeMap<String,CSSKey>();
            int i;
            
            signingTypes = espClient.espListSigningTypes();
            CertificateFactory cf = CertificateFactory.getInstance("X.509");
            if (signingTypes != null)
            {
                for (i = 0; i < signingTypes.length; ++i)
                {
                    CSSKey cssKey;

                    // create a new key entry
                    cssKey = new CSSKey(this, espClient, cf, signingTypes[i].certificate,
                                        signingTypes[i].guid);

                    // add the entry to the result list
                    if (signingTypes[i] != null)
                    {
                        result.put(signingTypes[i].guid, cssKey);   
                    }
                }
            }
            return result;
        }

        /** Create a certificate chain starting from the given
         *  certificate and chain as far up as possible using
         *  entries in the internal KeyStore. */
        public Certificate[] chain(X509Certificate cert) {
            ArrayList<Certificate> result = new ArrayList<Certificate>();
            X509Certificate current = cert;
            boolean done = false;
            while (!done) {
                X500Principal issuer = current.getIssuerX500Principal();
                X500Principal subject =
                    current.getSubjectX500Principal();
                result.add(current);

                // Stop at any self-signed certificate
                if (subject.equals(issuer))
                    break;

                // Find an issuer certificate that matches, if possible
                done = true;
                ArrayList<X509Certificate> suitors =
                    certmap.get(issuer.getName());
                if (suitors == null)
                    break;
                for (X509Certificate suitor : suitors) {
                    try {
                        current.verify(suitor.getPublicKey());
                        current = suitor;
                        done = false;
                        break;
                    } catch (NoSuchAlgorithmException ex) {
                    } catch (NoSuchProviderException ex) {
                    } catch (InvalidKeyException ex) {
                    } catch (SignatureException ex) {
                    } catch (CertificateException ex) {}
                }
            }
            return result.toArray(new Certificate[0]);
        }
    }

    /** Captures checked exceptions thrown by internals of a CSSJCA
     *  KeyStore that cannot be thrown directly because the signature
     *  of the engine classes doesn't include them. */
    public static class KeyStoreProblem extends RuntimeException {
        private KeyStoreProblem(Throwable cause) { super(cause); }
        private KeyStoreProblem(String message, Throwable cause)
        { super(message, cause); }
    }

    /** An internal component of the CSSJCA provider -- not for
     *  application use.  This implements the CSSJCA KeyStore type. */
    public static final class KeyStoreImp extends KeyStoreSpi {

        /** Maximum number of milliseconds since last update.  Larger
         *  values will reduce HTTPS traffic but allow stale data to
         *  persist longer.  This might be made externally
         *  configurable. */
        private static final long freshness = 10000;

        private KSData contents = null;
        private Map<String,CSSKey> keycerts =
            new TreeMap<String,CSSKey>();
        private long last_update = 0;

        public KeyStoreImp() {}
        KeyStoreImp(KSData contents) { this.contents = contents; }

        /** Ensures that the internal state of this class is up to
         *  date according to the freshness policy. */
        private synchronized void ensure() {
            if (contents == null)
                throw new IllegalStateException("KeyStore not loaded.");
            long now = System.currentTimeMillis();
            if (last_update + freshness < now) {
                try {
                    keycerts = contents.update();
                }
                catch (espDirectException ex)
                    { throw new KeyStoreProblem(ex); }
                catch (CertificateException ex)
                    { throw new KeyStoreProblem(ex); }
                last_update = now;
            }
        }

        /** Implements KeyStoreSpi. */
        public void engineLoad(InputStream stream, char[] password)
            throws IOException, NoSuchAlgorithmException,
                   CertificateException {
            try {
                contents = KSData.load
                    (new ObjectInputStream(stream), password);
            }
            catch (KeyStoreException ex)
                { throw new KeyStoreProblem(ex); }
            catch (KeyManagementException ex)
                { throw new KeyStoreProblem(ex); }
            catch (UnrecoverableKeyException ex)
                { throw new KeyStoreProblem(ex); }
        }

        /** Implements KeyStoreSpi. */
        public void engineStore(OutputStream stream, char[] password)
            throws IOException, NoSuchAlgorithmException,
                   CertificateException {
            if (contents == null)
                throw new IllegalStateException("KeyStore not loaded.");
            try {
                contents.store
                    (new ObjectOutputStream(stream), password);
            } catch (KeyStoreException ex)
                { throw new KeyStoreProblem(ex); }
        }

        /** Implements KeyStoreSpi. */
        public Enumeration<String> engineAliases() {
            ensure();
            return Collections.enumeration(keycerts.keySet());
        }

        /** Implements KeyStoreSpi. */
        public boolean engineContainsAlias(String alias)
        { ensure(); return keycerts.containsKey(alias); }

        /** Implements KeyStoreSpi. */
        public void engineDeleteEntry(String alias) {
            throw new UnsupportedOperationException
                ("Code Sign Server does not support key deletion.");
        }

        /** Implements KeyStoreSpi. */
        public Certificate engineGetCertificate(String alias) {
            ensure();
            CSSKey csskey = keycerts.get(alias);
            return (csskey != null) ? csskey.decoded_cert : null;
        }

        /** Implements KeyStoreSpi. */
        public String engineGetCertificateAlias(Certificate cert)
        { ensure(); return null; /* FIXME */}

        /** Implements KeyStoreSpi. */
        public Certificate[] engineGetCertificateChain(String alias) {
            ensure();
            CSSKey csskey = keycerts.get(alias);
            return (csskey != null) ?
                contents.chain(csskey.decoded_cert) : null;
        }

        /** Implements KeyStoreSpi. */
        public Date engineGetCreationDate(String alias) {
            ensure();
            CSSKey csskey = keycerts.get(alias);
            return (csskey != null) ?
                csskey.decoded_cert.getNotBefore() : null;
        }

        /** Implements KeyStoreSpi. */
        public Key engineGetKey(String alias, char[] password) {
            ensure();
            return keycerts.get(alias);
        }

        /** Implements KeyStoreSpi. */
        public boolean engineIsCertificateEntry(String alias)
        { ensure(); return false; }

        /** Implements KeyStoreSpi. */
        public boolean engineIsKeyEntry(String alias)
        { ensure(); return keycerts.containsKey(alias); }

        /** Implements KeyStoreSpi. */
        public void engineSetCertificateEntry
            (String alias, Certificate cert) {
            throw new UnsupportedOperationException
                ("Code Sign Server does not support HTTPS " +
                 "certificate insertion.");
        }

        /** Implements KeyStoreSpi. */
        public void engineSetKeyEntry
            (String alias, byte[] key, Certificate[] chain) {
            throw new UnsupportedOperationException
                ("Code Sign Server does not support HTTPS " +
                 "key creation.");
        }

        /** Implements KeyStoreSpi. */
        public void engineSetKeyEntry
            (String alias, Key key, char[] password,
             Certificate[] chain) {
            throw new UnsupportedOperationException
                ("Code Sign Server does not support HTTPS " +
                 "key creation.");
        }

        /** Implements KeyStoreSpi. */
        public int engineSize() { ensure(); return keycerts.size(); }
    }

    // Signature Implementation ----------------------------------------

    /** Implements raw RSA signatures.  Subclasses can apply digests
     *  and padding by setting the protected digestInfo and
     *  digestAlgorithm fields. */
    private static class SignatureImp extends SignatureSpi {
        protected byte[] digestInfo = new byte[0];
        protected String digestAlgorithm = null;
        protected CSSKey csskey = null;
        protected ByteArrayOutputStream baos = null;

        protected Object engineGetParameter(String param)
        { return null; }
        protected void engineSetParameter(String param, Object value) {}

        protected void engineUpdate(byte b) { baos.write(b); }
        protected void engineUpdate(byte[] b, int off, int len)
        { baos.write(b, off, len); }

        protected void engineInitSign(PrivateKey kprv) {
            if (kprv == null)
                throw new UnsupportedOperationException
                    ("CSSJCA provider can not sign with null keys.");
            else if (!(kprv instanceof CSSKey))
                throw new UnsupportedOperationException
                    ("CSSJCA provider can only sign with keys " +
                     "contained in a CSSJCA KeyStore.");
            csskey = (CSSKey)kprv;
            baos = new ByteArrayOutputStream();
        }

        @Override
        protected byte[] engineSign() throws SignatureException {
            if (csskey == null)
                throw new SignatureException
                    ("Signature not initialized.");
            try {
                // Performs a signature.  If the protected
                // digestAlgorithm string is null this method performs
                // a raw signature.  
                byte[] plain = baos.toByteArray();
                if (digestAlgorithm != null) {
                    MessageDigest md = MessageDigest.getInstance(digestAlgorithm);
                    plain = md.digest(plain);
                }

                int siglen =
                    ((RSAPublicKey)csskey.decoded_cert.
                     getPublicKey()).getModulus().bitLength() / 8;

                byte[] result = csskey.sign(plain, digestAlgorithm);

                // Code Sign Server seems to return signatures with
                // a leading zero byte when the high bit is set on
                // the most significant byte, probably as a result
                // of Bignum conversion.  This causes a sanity check
                // in the JCA verification code to raise an exception,
                // making the signature impossible to verify.
                //
                // Here we strip the leading zero byte if necessary
                // so that things work as expected.
                if ((result.length == siglen + 1) && (result[0] == 0)) {
                    byte[] update = new byte[siglen];
                    System.arraycopy(result, 1, update, 0, siglen);
                    result = update;
                }
                return result;
            }
            catch (espDirectException ex)
                { throw new SignatureException(ex); }
            catch (NoSuchAlgorithmException ex)
                { throw new SignatureException(ex); }
        }

        protected void engineInitVerify(PublicKey publicKey) {
            throw new UnsupportedOperationException
                ("RSA verification not supported.");
        }
        protected boolean engineVerify(byte[] sigBytes) {
            throw new UnsupportedOperationException
                ("RSA verification not supported.");
        }
    }

    /** An internal component of the CSSJCA provider -- not for
     *  application use.  This implements raw RSA signatures. */
    public static final class NONEwithRSA extends SignatureImp {}

    /** An internal component of the CSSJCA provider -- not for
     *  application use.  This implements RSA signatures of
     *  MD5 digests. */
    public static final class MD5withRSA extends SignatureImp {
        public MD5withRSA() {
            // See RFC 3447 section 9.2 for specification details.
            //     http://tools.ietf.org/html/rfc3447#section-9.2
            digestInfo = new byte[] {
                (byte)0x30, (byte)0x20, (byte)0x30, (byte)0x0c,
                (byte)0x06, (byte)0x08, (byte)0x2a, (byte)0x86,
                (byte)0x48, (byte)0x86, (byte)0xf7, (byte)0x0d,
                (byte)0x02, (byte)0x05, (byte)0x05, (byte)0x00,
                (byte)0x04, (byte)0x10,
            };
            digestAlgorithm = "MD5";
        }
    }

    /** An internal component of the CSSJCA provider -- not for
     *  application use.  This implements RSA signatures of
     *  SHA1 digests. */
    public static final class SHA1withRSA extends SignatureImp {
        public SHA1withRSA() {
            // See RFC 3447 section 9.2 for specification details.
            //     http://tools.ietf.org/html/rfc3447#section-9.2
            digestInfo = new byte[] {
                (byte)0x30, (byte)0x21, (byte)0x30, (byte)0x09,
                (byte)0x06, (byte)0x05, (byte)0x2b, (byte)0x0e,
                (byte)0x03, (byte)0x02, (byte)0x1a, (byte)0x05,
                (byte)0x00, (byte)0x04, (byte)0x14,
            };
            digestAlgorithm = "SHA-1";
        }
    }

    /** An internal component of the CSSJCA provider -- not for
     *  application use.  This implements RSA signatures of
     *  SHA256 digests. */
    public static final class SHA256withRSA extends SignatureImp {
        public SHA256withRSA() {
            // See RFC 3447 section 9.2 for specification details.
            //     http://tools.ietf.org/html/rfc3447#section-9.2
            digestInfo = new byte[] {
                (byte)0x30, (byte)0x31, (byte)0x30, (byte)0x0d,
                (byte)0x06, (byte)0x09, (byte)0x60, (byte)0x86,
                (byte)0x48, (byte)0x01, (byte)0x65, (byte)0x03,
                (byte)0x04, (byte)0x02, (byte)0x01, (byte)0x05,
                (byte)0x00, (byte)0x04, (byte)0x20,
            };
            digestAlgorithm = "SHA-256";
        }
    }

    /** An internal component of the CSSJCA provider -- not for
     *  application use.  This implements RSA signatures of
     *  SHA384 digests. */
    public static final class SHA384withRSA extends SignatureImp {
        public SHA384withRSA() {
            // See RFC 3447 section 9.2 for specification details.
            //     http://tools.ietf.org/html/rfc3447#section-9.2
            digestInfo = new byte[] {
                (byte)0x30, (byte)0x41, (byte)0x30, (byte)0x0d,
                (byte)0x06, (byte)0x09, (byte)0x60, (byte)0x86,
                (byte)0x48, (byte)0x01, (byte)0x65, (byte)0x03,
                (byte)0x04, (byte)0x02, (byte)0x02, (byte)0x05,
                (byte)0x00, (byte)0x04, (byte)0x30,
            };
            digestAlgorithm = "SHA-384";
        }
    }

    /** An internal component of the CSSJCA provider -- not for
     *  application use.  This implements RSA signatures of
     *  SHA512 digests. */
    public static final class SHA512withRSA extends SignatureImp {
        public SHA512withRSA() {
            // See RFC 3447 section 9.2 for specification details.
            //     http://tools.ietf.org/html/rfc3447#section-9.2
            digestInfo = new byte[] {
                (byte)0x30, (byte)0x51, (byte)0x30, (byte)0x0d,
                (byte)0x06, (byte)0x09, (byte)0x60, (byte)0x86,
                (byte)0x48, (byte)0x01, (byte)0x65, (byte)0x03,
                (byte)0x04, (byte)0x02, (byte)0x03, (byte)0x05,
                (byte)0x00, (byte)0x04, (byte)0x40,
            };
            digestAlgorithm = "SHA-512";
        }
    }

    // Test Methods ----------------------------------------------------

    /** <p>A command line utility useful for configuring and testing
     *  the CSSJCA provider.  This utility assumes that all keys are
     *  PrivateKeys and that all Certificates are X509Certificates.</p>
     *
     *  <p>Given a URL and a KeyStore containing the authentication
     *  credentials necessary for interacting with the Code Sign
     *  Server, a CSSJCA KeyStore can be created this way:</p>
     *  <pre>
     *     $ java -jar cssjca.jar CSSJCA \
     *         --infile input.ks --inpass secret \
     *         --out cssjca.ks --url http://css.example.com:8443
     *  </pre>
     *
     *  <p>The following displays the keys and certificate chains
     *  made available by the Code Sign Server:</p>
     *  <pre>
     *     $ java -jar cssjca.jar CSSJCA \
     *         --ksfile cssjca.ks --kspass secret
     *  </pre>
     *
     *  <p>This performs a signature test with the key that has
     *  <code>test</code> as an alias:</p>
     *  <pre>
     *     $ java -jar cssjca.jar CSSJCA \
     *         --ksfile cssjca.ks --kspass secret \
     *         --sigkey test --sigpro CSSJCA
     *  </pre>
     *  <p>The <code>--sigpro</code> option may not be necessary
     *  because recent versions of the JRE are able to bind a
     *  <code>java.security.Signature</code> object to the same
     *  provider as the key presented during initialization.</p> */
    public static void main(String[] args) throws Exception {
        Settings s = new Settings();
        Settings.String infile = s.add
            (new Settings.String
             ("infile", System.getProperty("user.home") +
              System.getProperty("file.separator") + ".keystore"));
        Settings.String intype =
            s.add(new Settings.String
                  ("intype", KeyStore.getDefaultType()));
        Settings.String inpass = s.add(new Settings.String("inpass"));
        Settings.String ksfile = s.add(new Settings.String("ksfile", 
             System.getProperty("user.home") + System.getProperty("file.separator") + ".keystore"));
        Settings.String kstype = s.add(new Settings.String("kstype", "CSSJCA"));
        Settings.String kspass = s.add(new Settings.String("kspass"));
        Settings.String alias = s.add(new Settings.String("alias"));
        Settings.String url = s.add(new Settings.String("url"));
        Settings.String oauthUrl = s.add(new Settings.String("oauthServerUrl"));
        Settings.String oauthClientId = s.add(new Settings.String("oauthClientId"));
        Settings.String out = s.add(new Settings.String("out"));
        Settings.String sigalg = s.add(new Settings.String("sigalg", "SHA1withRSA"));
        Settings.String sigkey = s.add(new Settings.String("sigkey"));
        Settings.String sigpro = s.add(new Settings.String("sigpro"));
        Settings.String plain = s.add(new Settings.String("plain", "sign-me"));
        List<java.lang.String> extras = s.parseArgs(args);

        if (!extras.isEmpty())
        {
            // The following line makes it unnecessary to edit the
            // $JAVA_HOME/lib/security/java.security file.  This is
            // helpful when testing.
            Security.addProvider(new CSSJCA());

            KeyStore ks = KeyStore.getInstance(kstype.value);
            FileInputStream fis = null;
            try {
                fis = new FileInputStream(ksfile.value);
                
                char[] chpass;
                chpass = (kspass.value != null) ? kspass.value.toCharArray() : 
                        Dispatch.passwdPrompt("Enter KeyStore pass phrase: ");
                
                ks.load(fis, chpass);
            } 
            finally 
            { 
                if (fis != null)
                {
                    fis.close();
                } 
            }            
        }
        else if (url.value != null)  // Create a CSSJCA KeyStore
        { 
            char[] chpass = (inpass.value != null) ?
                inpass.value.toCharArray() :
                Dispatch.passwdPrompt("Enter KeyStore pass phrase: ");
            KeyStore ks = KeyStore.getInstance(intype.value);
            FileInputStream fis = null;
            try 
            {
                fis = new FileInputStream(infile.value);
                ks.load(fis, chpass);
            } 
            finally 
            { 
                if (fis != null) 
                {
                    fis.close();
                } 
            }
            KSData c = new KSData(ks, chpass, alias.value, url.value, 
                                  oauthUrl.value, oauthClientId.value);

            if (out.value != null) 
            {
                FileOutputStream fos = null;
                try 
                {
                    fos = new FileOutputStream(out.value);
                    c.store(new ObjectOutputStream(fos), chpass);
                    System.out.println("Created CSSJCA KeyStore: " + out.value);
                } 
                finally 
                { 
                    if (fos != null) 
                    {
                        fos.close();
                    } 
                }
            }
        } 
        else 
        {   // Perform a CSSJCA provider test.
            // The following line makes it unnecessary to edit the
            // $JAVA_HOME/lib/security/java.security file.  This is
            // helpful when testing.
            Security.addProvider(new CSSJCA());

            char[] chpass = (kspass.value != null) ?
                kspass.value.toCharArray() :
                Dispatch.passwdPrompt("Enter KeyStore pass phrase: ");

            KeyStore ks = KeyStore.getInstance(kstype.value);
            FileInputStream fis = null;
            try 
            {
                fis = new FileInputStream(ksfile.value);
                ks.load(fis, chpass);
            } 
            finally 
            { 
                if (fis != null) 
                {
                    fis.close();
                } 
            }

            if (sigkey.value == null) 
            {
                Enumeration<String> aliases = ks.aliases();
                while (aliases.hasMoreElements()) 
                {
                    String current = aliases.nextElement();
                    if (ks.isKeyEntry(current)) 
                    {
                        System.out.println("Key: " + current);
                        for (Certificate c : ks.getCertificateChain(current)) 
                        {
                            X509Certificate xc = (X509Certificate)c;
                            System.out.println("  Certificate:");
                            System.out.println("  - Serial Number: " + xc.getSerialNumber());
                            System.out.println("  - Expires: " + xc.getNotAfter());
                            System.out.println("  - Subject: " + xc.getSubjectX500Principal());
                            System.out.println("  - Issuer: " + xc.getIssuerX500Principal());
                        }
                    } 
                    else 
                    {
                        X509Certificate xc = (X509Certificate)ks.getCertificate(current);
                        System.out.println("Certificate: " + current);
                        System.out.println("  - Serial Number: " + xc.getSerialNumber());
                        System.out.println("  - Expires: " + xc.getNotAfter());
                        System.out.println("  - Subject: " + xc.getSubjectX500Principal());
                        System.out.println("  - Issuer: " + xc.getIssuerX500Principal());
                    }
                }
            } 
            else 
            {
                Signature sig = (sigpro.value != null) ?
                    Signature.getInstance(sigalg.value, sigpro.value) :
                    Signature.getInstance(sigalg.value);;
                sig.initSign((PrivateKey)ks.getKey
                             (sigkey.value, chpass));
                sig.update(plain.value.getBytes());
                byte[] sigbytes = sig.sign();
                System.out.println("Signature (" + sigalg.value + ") " +
                                   sigbytes.length + " bytes: " +
                                   Transform.byte2hex(sigbytes));

                Signature ver = Signature.getInstance(sigalg.value);
                ver.initVerify
                    (ks.getCertificate(sigkey.value).getPublicKey());
                ver.update(plain.value.getBytes());
                System.out.println("Verify: " + ver.verify(sigbytes));
            }
        }
    }
}
