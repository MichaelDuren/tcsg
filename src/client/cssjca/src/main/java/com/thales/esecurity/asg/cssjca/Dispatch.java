// Dispatch.java
// Copyright (C) 2010 Thales e-Security, Inc.  All rights reserved.
package com.thales.esecurity.asg.cssjca;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.io.PrintStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Console;
import java.io.IOException;
import java.io.FileNotFoundException;
import java.security.KeyStore;
import javax.crypto.KeyGenerator;
import javax.crypto.Cipher;

/** Entry point for JAR which dispatches requests to other classes. */
public class Dispatch {
    protected Dispatch() {}
    
    /** Collect a password without echoing characters if possible. */
    public static char[] passwdPrompt(String prompt) {
        // Java 1.6 introduced a Console class that makes it easy to
        // prompt for a pass phrase without echoing the characters
        // back to the terminal.  Unfortunately earlier Java releases
        // don't have this, so this code uses reflection to grab it
        // if possible and fall back to echoing if necessary.
        char[] result = null;
        Console cons = System.console();
        if (cons != null) 
        {
            result = cons.readPassword("[%s]", prompt);
        }
        return result;
    }

    /** Entry point that dispatches to other classes. */
    public static void main(String[] args) throws Exception
    {
        int result = 0;
        if (args.length == 0) {
            Peddler.main(new String[]{});
        } else {
            try {
                // Should be: Arrays.copyOfRange(args, 1, args.length)
                // But that requires Java 1.6 or later.
                String[] subargs = new String[args.length - 1];
                for (int i = 1; i < args.length; i++)
                    subargs[i - 1] = args[i];
                
                Class<?> c = Class.forName
                    (Dispatch.class.getPackage().getName() + "." +
                     args[0]);
                Method m = c.getMethod("main", args.getClass());
                m.invoke(null, new Object[] { subargs });
                result = 0;
            } catch (InvocationTargetException ex) {
                ex.getCause().printStackTrace(System.err);
            } catch (ClassNotFoundException ex) {
                System.err.println("No such command: " + args[0]);
            } catch (NoSuchMethodException ex) {
                System.err.println("Invalid command: " + args[0]);
            } catch (IllegalAccessException ex) {
                System.err.println("Private command: " + args[0]);
            }
        }
        if (result != 0)
            System.exit(result);
    }
}
