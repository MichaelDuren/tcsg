package com.thales.esecurity.asg.cssjca;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.security.KeyStore;

/** <p>A simple command line utility that can pull a key from one
 *  KeyStore into another.  This does the job of the -importkeystore
 *  option to keytool, which was introduced in Java 1.6 so that Java
 *  1.5 installations can solve the same problem.</p>
 *  <pre>
      java -jar build/cssjca.jar Migrate \
           --infile key.p12 --intype pkcs12 \
           --ksfile test-migrate.ks --kstype jks \
           --source 1 --dest css-client
    </pre> */
public class Migrate {
    public static void main(String[] args) throws Exception {
        Settings s = new Settings();
        Settings.String source = s.add(new Settings.String("source"));
        Settings.String dest = s.add(new Settings.String("dest"));
        Settings.String kspass = s.add(new Settings.String("kspass"));
        Settings.String ksfile = s.add
            (new Settings.String
             ("ksfile", System.getProperty("user.home") +
              System.getProperty("file.separator") + ".keystore"));
        Settings.String kstype = s.add
            (new Settings.String("kstype", KeyStore.getDefaultType()));
        Settings.String inpass = s.add(new Settings.String("inpass"));
        Settings.String infile = s.add
            (new Settings.String
             ("infile", System.getProperty("user.home") +
              System.getProperty("file.separator") + ".keystore"));
        Settings.String intype = s.add
            (new Settings.String("intype", KeyStore.getDefaultType()));
        s.parseArgs(args);
        char[] in_chpass = (inpass.value != null) ?
            inpass.value.toCharArray() :
            Dispatch.passwdPrompt("Enter KeyStore pass phrase:");
        char[] ks_chpass = (kspass.value != null) ?
            kspass.value.toCharArray() :
            Dispatch.passwdPrompt("Enter KeyStore pass phrase:");

        FileInputStream fin = null;

        KeyStore ks = KeyStore.getInstance(kstype.value);
        try {
            ks.load(((ksfile.value != null) &&
                     (ksfile.value.length() > 0)) ?
                    fin = new FileInputStream(ksfile.value) : null,
                    ks_chpass);
        } catch (FileNotFoundException ex) {
            ks.load(null, ks_chpass);
        } finally { if (fin != null) { fin.close(); fin = null; } }

        KeyStore in = KeyStore.getInstance(intype.value);
        try {
            in.load(((infile.value != null) &&
                     (infile.value.length() > 0)) ?
                    fin = new FileInputStream(infile.value) : null,
                    in_chpass);
        } finally { if (fin != null) { fin.close(); fin = null; } }

        if ((source.value != null) && (dest.value != null)) {
            if (!in.isKeyEntry(source.value))
                throw new IllegalStateException
                    ("Source key is not present in input KeyStore.");
            ks.setKeyEntry(dest.value, in.getKey
                           (source.value, in_chpass),
                           ks_chpass, in.getCertificateChain
                           (source.value));

            FileOutputStream fout = null;
            try {
                ks.store(fout = new FileOutputStream(ksfile.value),
                         ks_chpass);
            } finally {
                if (fout != null) {
                    fout.close(); fout = null;
                }
            }
        } else System.out.println
                   ("Specify source and dest to migrate a key.");
    }
}
