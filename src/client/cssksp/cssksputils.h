

#ifndef __CSSKSPUTILS_H__
#define __CSSKSPUTILS_H__

#include "../csscsp/cspdk.h"

typedef BOOL(CALLBACK *_fp_CPAcquireContext)(HCRYPTPROV *phProv, LPCSTR szContainer, DWORD dwFlags, PVTableProvStruc pVTable);
typedef BOOL(CALLBACK *_fp_CPAcquireContextW)(HCRYPTPROV *phProv, LPCWSTR szContainer, DWORD dwFlags, void *pVTable);
typedef BOOL(CALLBACK *_fp_CPReleaseContext)(HCRYPTPROV hProv, DWORD dwFlags);
typedef BOOL(CALLBACK *_fp_CPDeriveKey)(HCRYPTPROV hProv, ALG_ID Algid, HCRYPTHASH hHash, DWORD dwFlags, HCRYPTKEY *phKey);
typedef BOOL(CALLBACK *_fp_CPDestroyKey)(HCRYPTPROV hProv, HCRYPTKEY hKey);
typedef BOOL(CALLBACK *_fp_CPSetKeyParam)(HCRYPTPROV hProv, HCRYPTKEY hKey, DWORD dwParam, CONST BYTE *pbData, DWORD dwFlags);
typedef BOOL(CALLBACK *_fp_CPGetKeyParam)(HCRYPTPROV hProv, HCRYPTKEY hKey, DWORD dwParam, LPBYTE pbData, OUT LPDWORD pcbDataLen, DWORD dwFlags);
typedef BOOL(CALLBACK *_fp_CPSetProvParam)(HCRYPTPROV hProv, DWORD dwParam, CONST BYTE *pbData, DWORD dwFlags);
typedef BOOL(CALLBACK *_fp_CPGetProvParam)(HCRYPTPROV hProv, DWORD dwParam, LPBYTE pbData, OUT LPDWORD pcbDataLen, DWORD dwFlags);
typedef BOOL(CALLBACK *_fp_CPSetHashParam)(HCRYPTPROV hProv, HCRYPTHASH hHash, DWORD dwParam, CONST BYTE *pbData, DWORD dwFlags);
typedef BOOL(CALLBACK *_fp_CPGetHashParam)(HCRYPTPROV hProv, HCRYPTHASH hHash, DWORD dwParam, LPBYTE pbData, OUT LPDWORD pcbDataLen, DWORD dwFlags);
typedef BOOL(CALLBACK *_fp_CPExportKey)(HCRYPTPROV hProv, HCRYPTKEY hKey, HCRYPTKEY hPubKey, DWORD dwBlobType, DWORD dwFlags, LPBYTE pbData, OUT LPDWORD pcbDataLen);
typedef BOOL(CALLBACK *_fp_CPImportKey)(HCRYPTPROV hProv, CONST BYTE *pbData, DWORD cbDataLen, HCRYPTKEY hPubKey, DWORD dwFlags, HCRYPTKEY *phKey);
typedef BOOL(CALLBACK *_fp_CPEncrypt)(HCRYPTPROV hProv, HCRYPTKEY hKey, HCRYPTHASH hHash, BOOL fFinal, DWORD dwFlags, OUT LPBYTE pbData, OUT LPDWORD pcbDataLen, DWORD cbBufLen);
typedef BOOL(CALLBACK *_fp_CPDecrypt)(HCRYPTPROV hProv, HCRYPTKEY hKey, HCRYPTHASH hHash, BOOL fFinal, DWORD dwFlags, OUT LPBYTE pbData, OUT LPDWORD pcbDataLen);
typedef BOOL(CALLBACK *_fp_CPCreateHash)(HCRYPTPROV hProv, ALG_ID Algid, HCRYPTKEY hKey, DWORD dwFlags, HCRYPTHASH *phHash);
typedef BOOL(CALLBACK *_fp_CPHashData)(HCRYPTPROV hProv, HCRYPTHASH hHash, CONST BYTE *pbData, DWORD cbDataLen, DWORD dwFlags);
typedef BOOL(CALLBACK *_fp_CPHashSessionKey)(HCRYPTPROV hProv, HCRYPTHASH hHash, HCRYPTKEY hKey, DWORD dwFlags);
typedef BOOL(CALLBACK *_fp_CPSignHash)(HCRYPTPROV hProv, HCRYPTHASH hHash, DWORD dwKeySpec, LPCWSTR szDescription, DWORD dwFlags, LPBYTE pbSignature, OUT LPDWORD pcbSigLen);
typedef BOOL(CALLBACK *_fp_CPDestroyHash)(HCRYPTPROV hProv, HCRYPTHASH hHash);
typedef BOOL(CALLBACK *_fp_CPVerifySignature)(HCRYPTPROV hProv, HCRYPTHASH hHash, CONST BYTE *pbSignature, DWORD cbSigLen, HCRYPTKEY hPubKey, LPCWSTR szDescription, DWORD dwFlags);
typedef BOOL(CALLBACK *_fp_CPGenRandom)(HCRYPTPROV hProv, DWORD cbLen, LPBYTE pbBuffer);
typedef BOOL(CALLBACK *_fp_CPGetUserKey)(HCRYPTPROV hProv, DWORD dwKeySpec, HCRYPTKEY *phUserKey);
typedef BOOL(CALLBACK *_fp_CPDuplicateHash)(HCRYPTPROV hProv, HCRYPTHASH hHash, LPDWORD pdwReserved, DWORD dwFlags, HCRYPTHASH *phHash);
typedef BOOL(CALLBACK *_fp_CPDuplicateKey)(HCRYPTPROV hProv, HCRYPTKEY hKey, LPDWORD pdwReserved, DWORD dwFlags, HCRYPTKEY *phKey);

typedef BOOL(CALLBACK *_fp_CPSignHashValue)(HCRYPTPROV hProv, BYTE *pbHash, DWORD dwHashLen, LPCWSTR szDescription, DWORD dwFlags, LPBYTE pbSignature, OUT LPDWORD pcbSigLen);

extern _fp_CPAcquireContext cspAcquireContext;
extern _fp_CPAcquireContextW cspCPAcquireContextW;
extern _fp_CPReleaseContext cspCPReleaseContext;
extern _fp_CPDeriveKey cspCPDeriveKey;
extern _fp_CPDestroyKey cspCPDestroyKey;
extern _fp_CPSetKeyParam cspCPSetKeyParam;
extern _fp_CPGetKeyParam cspCPGetKeyParam;
extern _fp_CPSetProvParam cspCPSetProvParam;
extern _fp_CPGetProvParam cspCPGetProvParam;
extern _fp_CPSetHashParam cspCPSetHashParam;
extern _fp_CPGetHashParam cspCPGetHashParam;
extern _fp_CPExportKey cspCPExportKey;
extern _fp_CPImportKey cspCPImportKey;
extern _fp_CPEncrypt cspCPEncrypt;
extern _fp_CPDecrypt cspCPDecrypt;
extern _fp_CPCreateHash cspCPCreateHash;
extern _fp_CPHashData cspCPHashData;
extern _fp_CPHashSessionKey cspCPHashSessionKey;
extern _fp_CPSignHash cspCPSignHash;
extern _fp_CPDestroyHash cspCPDestroyHash;
extern _fp_CPVerifySignature cspCPVerifySignature;
extern _fp_CPGenRandom cspCPGenRandom;
extern _fp_CPGetUserKey cspCPGetUserKey;
extern _fp_CPDuplicateHash cspCPDuplicateHash;
extern _fp_CPDuplicateKey cspCPDuplicateKey;

extern _fp_CPSignHashValue cspCPSignHashValue;
 
extern SECURITY_STATUS  CSSKSPLoadCSP(CSSKSP_PROVIDER *pProvider);
extern CSSKSP_PROVIDER *CssKspValidateProvHandle(__in    NCRYPT_PROV_HANDLE hProvider);
#endif //__CSSKSPUTILS_H__
