

#include "stdafx.h"
#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <wincrypt.h>
#include <bcrypt.h>
#include <ncrypt.h>
#include <ncrypt_provider.h>
#include <bcrypt_provider.h>
#include "cssksp.h"
#include "cssksputils.h"

CHAR g_szLogDir[MAX_PATH];


_fp_CPAcquireContext cspAcquireContext;
_fp_CPAcquireContextW cspCPAcquireContextW;
_fp_CPReleaseContext cspCPReleaseContext;
_fp_CPDeriveKey cspCPDeriveKey;
_fp_CPDestroyKey cspCPDestroyKey;
_fp_CPSetKeyParam cspCPSetKeyParam;
_fp_CPGetKeyParam cspCPGetKeyParam;
_fp_CPSetProvParam cspCPSetProvParam;
_fp_CPGetProvParam cspCPGetProvParam;
_fp_CPSetHashParam cspCPSetHashParam;
_fp_CPGetHashParam cspCPGetHashParam;
_fp_CPExportKey cspCPExportKey;
_fp_CPImportKey cspCPImportKey;
_fp_CPEncrypt cspCPEncrypt;
_fp_CPDecrypt cspCPDecrypt;
_fp_CPCreateHash cspCPCreateHash;
_fp_CPHashData cspCPHashData;
_fp_CPHashSessionKey cspCPHashSessionKey;
_fp_CPSignHash cspCPSignHash;
_fp_CPDestroyHash cspCPDestroyHash;
_fp_CPVerifySignature cspCPVerifySignature;
_fp_CPGenRandom cspCPGenRandom;
_fp_CPGetUserKey cspCPGetUserKey;
_fp_CPDuplicateHash cspCPDuplicateHash;
_fp_CPDuplicateKey cspCPDuplicateKey;

_fp_CPSignHashValue cspCPSignHashValue;

BOOL g_cspLibraryIsLoaded;

/*******************************************************************
* DESCRIPTION :     Open the CSP library and map it's functions
*
* INPUTS :
*            CSSKSP_PROVIDER *pProvider      Provider data
* OUTPUTS :
* RETURN :
*            ERROR_SUCCESS                   The function was successful.
*            NTE_FAIL                        The library failed to load
*/
SECURITY_STATUS CSSKSPLoadCSP(CSSKSP_PROVIDER *pProvider)
{
    SECURITY_STATUS status = NTE_INTERNAL_ERROR;

    // TODO: Get the DLL's name from the registry or a config file
    pProvider->hCspDLL = LoadLibrary(L"csscsp.dll");
    if (pProvider->hCspDLL == NULL) {
        //DebugOut("Unable to load CSP Library");
        status = NTE_FAIL;
    }
    else {
        status = ERROR_SUCCESS;

        // get the proc address 
        if ((cspAcquireContext = (_fp_CPAcquireContext)GetProcAddress(pProvider->hCspDLL, "CPAcquireContext")) == NULL) { status = NTE_FAIL; goto cleanup; }
       // if ((cspCPAcquireContextW = (_fp_CPAcquireContextW)GetProcAddress(pProvider->hCspDLL, "CPAcquireContextW")) == NULL) { status = NTE_FAIL; goto cleanup; }
        if ((cspCPReleaseContext = (_fp_CPReleaseContext)GetProcAddress(pProvider->hCspDLL, "CPReleaseContext")) == NULL) { status = NTE_FAIL; goto cleanup; }
        if ((cspCPDeriveKey = (_fp_CPDeriveKey)GetProcAddress(pProvider->hCspDLL, "CPDeriveKey")) == NULL) { status = NTE_FAIL; goto cleanup; }
        if ((cspCPDestroyKey = (_fp_CPDestroyKey)GetProcAddress(pProvider->hCspDLL, "CPDestroyKey")) == NULL) { status = NTE_FAIL; goto cleanup; }
        if ((cspCPSetKeyParam = (_fp_CPSetKeyParam)GetProcAddress(pProvider->hCspDLL, "CPSetKeyParam")) == NULL) { status = NTE_FAIL; goto cleanup; }
        if ((cspCPGetKeyParam = (_fp_CPGetKeyParam)GetProcAddress(pProvider->hCspDLL, "CPGetKeyParam")) == NULL) { status = NTE_FAIL; goto cleanup; }
        if ((cspCPSetProvParam = (_fp_CPSetProvParam)GetProcAddress(pProvider->hCspDLL, "CPSetProvParam")) == NULL) { status = NTE_FAIL; goto cleanup; }
        if ((cspCPGetProvParam = (_fp_CPGetProvParam)GetProcAddress(pProvider->hCspDLL, "CPGetProvParam")) == NULL) { status = NTE_FAIL; goto cleanup; }
        if ((cspCPSetHashParam = (_fp_CPSetHashParam)GetProcAddress(pProvider->hCspDLL, "CPSetHashParam")) == NULL) { status = NTE_FAIL; goto cleanup; }
        if ((cspCPGetHashParam = (_fp_CPGetHashParam)GetProcAddress(pProvider->hCspDLL, "CPGetHashParam")) == NULL) { status = NTE_FAIL; goto cleanup; }
        if ((cspCPExportKey = (_fp_CPExportKey)GetProcAddress(pProvider->hCspDLL, "CPExportKey")) == NULL) { status = NTE_FAIL; goto cleanup; }
        if ((cspCPImportKey = (_fp_CPImportKey)GetProcAddress(pProvider->hCspDLL, "CPImportKey")) == NULL) { status = NTE_FAIL; goto cleanup; }
        if ((cspCPEncrypt = (_fp_CPEncrypt)GetProcAddress(pProvider->hCspDLL, "CPEncrypt")) == NULL) { status = NTE_FAIL; goto cleanup; }
        if ((cspCPDecrypt = (_fp_CPDecrypt)GetProcAddress(pProvider->hCspDLL, "CPDecrypt")) == NULL) { status = NTE_FAIL; goto cleanup; }
        if ((cspCPCreateHash = (_fp_CPCreateHash)GetProcAddress(pProvider->hCspDLL, "CPCreateHash")) == NULL) { status = NTE_FAIL; goto cleanup; }
        if ((cspCPHashData = (_fp_CPHashData)GetProcAddress(pProvider->hCspDLL, "CPHashData")) == NULL) { status = NTE_FAIL; goto cleanup; }
        if ((cspCPHashSessionKey = (_fp_CPHashSessionKey)GetProcAddress(pProvider->hCspDLL, "CPHashSessionKey")) == NULL) { status = NTE_FAIL; goto cleanup; }
        if ((cspCPSignHash = (_fp_CPSignHash)GetProcAddress(pProvider->hCspDLL, "CPSignHash")) == NULL) { status = NTE_FAIL; goto cleanup; }
        if ((cspCPDestroyHash = (_fp_CPDestroyHash)GetProcAddress(pProvider->hCspDLL, "CPDestroyHash")) == NULL) { status = NTE_FAIL; goto cleanup; }
        if ((cspCPVerifySignature = (_fp_CPVerifySignature)GetProcAddress(pProvider->hCspDLL, "CPVerifySignature")) == NULL) { status = NTE_FAIL; goto cleanup; }
        if ((cspCPGenRandom = (_fp_CPGenRandom)GetProcAddress(pProvider->hCspDLL, "CPGenRandom")) == NULL) { status = NTE_FAIL; goto cleanup; }
        if ((cspCPGetUserKey = (_fp_CPGetUserKey)GetProcAddress(pProvider->hCspDLL, "CPGetUserKey")) == NULL) { status = NTE_FAIL; goto cleanup; }
        if ((cspCPDuplicateHash = (_fp_CPDuplicateHash)GetProcAddress(pProvider->hCspDLL, "CPDuplicateHash")) == NULL) { status = NTE_FAIL; goto cleanup; }
        if ((cspCPDuplicateKey = (_fp_CPDuplicateKey)GetProcAddress(pProvider->hCspDLL, "CPDuplicateKey")) == NULL) { status = NTE_FAIL; goto cleanup; }

		if ((cspCPSignHashValue = (_fp_CPSignHashValue)GetProcAddress(pProvider->hCspDLL, "CPSignHashValue")) == NULL) { status = NTE_FAIL; goto cleanup; }

    cleanup:
        if (status != ERROR_SUCCESS) {
            FreeLibrary(pProvider->hCspDLL);
        }
    }


    return status;
}

///////////////////////////////////////////////////////////////////////////////
/*****************************************************************************
* DESCRIPTION :    Validate KSP provider handle
*
* INPUTS :
*           NCRYPT_PROV_HANDLE hProvider                A NCRYPT_PROV_HANDLE handle
*
* RETURN :
*           A pointer to a SAMPLEKSP_PROVIDER struct    The function was successful.
*           NULL                                        The handle is invalid.
*/
CSSKSP_PROVIDER *CssKspValidateProvHandle(__in    NCRYPT_PROV_HANDLE hProvider)
{
    CSSKSP_PROVIDER *pProvider = NULL;
	int i;

    if (hProvider == 0)
    {
        return NULL;
    }

	// verify this is in the list of open providers
	for (i = 0; i < MAX_PROVIDER_HANDLES; ++i)
	{
		if (g_openProviders[i] == hProvider)
		{
			pProvider = (CSSKSP_PROVIDER *)hProvider;
			break;
		}
	}

	if (pProvider == NULL)
	{
		return NULL;
	}


    // make sure this handle is at least in the proper heap
    if (HeapValidate(GetProcessHeap(), 0, pProvider) == FALSE)
    {
		return NULL;
    }

    if (pProvider->cbLength < sizeof(CSSKSP_PROVIDER) ||
        pProvider->dwMagic != CSSKSP_PROVIDER_MAGIC)
    {
        return NULL;
    }

    return pProvider;
}

