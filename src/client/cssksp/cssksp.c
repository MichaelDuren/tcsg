// cssksp.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include <windows.h>
#include <stdlib.h>
#include <wincrypt.h>
#include <bcrypt.h>
#include <ncrypt.h>

#include "cssksp.h"
#include "../csscsp/csscsp.h"
#include "../csscommon/csslog.h"
#include "cssksputils.h"

#define CSSKSP_INTERFACE_VERSION BCRYPT_MAKE_INTERFACE_VERSION(1,0) //version of the KSP interface


// list of valid provider handles
NCRYPT_PROV_HANDLE g_openProviders[MAX_PROVIDER_HANDLES];

///////////////////////////////////////////////////////////////////////////////
//
// Ncrypt key storage provider function table
//
///////////////////////////////////////////////////////////////////////////////
NCRYPT_KEY_STORAGE_FUNCTION_TABLE CSSKSPFunctionTable =
{
    CSSKSP_INTERFACE_VERSION,
    CSSKSPOpenProvider,
    CSSKSPOpenKey,
    CSSKSPCreatePersistedKey,
    CSSKSPGetProviderProperty,
    CSSKSPGetKeyProperty,
    CSSKSPSetProviderProperty,
    CSSKSPSetKeyProperty,
    CSSKSPFinalizeKey,
    CSSKSPDeleteKey,
    CSSKSPFreeProvider,
    CSSKSPFreeKey,
    CSSKSPFreeBuffer,
    CSSKSPEncrypt,
    CSSKSPDecrypt,
    CSSKSPIsAlgSupported,
    CSSKSPEnumAlgorithms,
    CSSKSPEnumKeys,
    CSSKSPImportKey,
    CSSKSPExportKey,
    CSSKSPSignHash,
    CSSKSPVerifySignature,
    CSSKSPPromptUser,
    CSSKSPNotifyChangeKey,
    CSSKSPSecretAgreement,
    CSSKSPDeriveKey,
    CSSKSPFreeSecret
};


///////////////////////////////////////////////////////////////////////////////
//
// Variables
//
///////////////////////////////////////////////////////////////////////////////
HINSTANCE g_hInstance;

///////////////////////////////////////////////////////////////////////////////
//
// Dll entry
//
///////////////////////////////////////////////////////////////////////////////
BOOL WINAPI DllMain(HMODULE hInstDLL, DWORD dwReason, LPVOID lpvReserved)
{
    UNREFERENCED_PARAMETER(lpvReserved);

    g_hInstance = (HINSTANCE)hInstDLL;

    if (dwReason == DLL_PROCESS_ATTACH)
    {
        // initialize global values for logging and config
        InitGlobals();
    }
    else if (dwReason == DLL_PROCESS_DETACH)
    {
    }
    return TRUE;
}

///////////////////////////////////////////////////////////////////////////////


/******************************************************************************
* DESCRIPTION :     Get the KSP key storage Interface function
*                   dispatch table
*
* INPUTS :
*            LPCWSTR pszProviderName        Name of the provider (unused)
*            DWORD   dwFlags                Flags (unused)
* OUTPUTS :
*            char    **ppFunctionTable      The key storage interface function
*                                           dispatch table
* RETURN :
*            ERROR_SUCCESS                  The function was successful.
*/
NTSTATUS WINAPI GetKeyStorageInterface(__in   LPCWSTR pszProviderName,
                                       __out  NCRYPT_KEY_STORAGE_FUNCTION_TABLE **ppFunctionTable,
                                       __in   DWORD dwFlags)
{
    UNREFERENCED_PARAMETER(pszProviderName);
    UNREFERENCED_PARAMETER(dwFlags);

    *ppFunctionTable = &CSSKSPFunctionTable;

    return ERROR_SUCCESS;
}


/*******************************************************************
* DESCRIPTION :     Load and initialize the KSP provider
*
* INPUTS :
*            LPCWSTR pszProviderName         Name of the provider
*            DWORD   dwFlags                 Flags (unused)
* OUTPUTS :
*            NCRYPT_PROV_HANDLE *phProvider  The provider handle
* RETURN :
*            ERROR_SUCCESS                   The function was successful.
*            NTE_INVALID_PARAMETER           One or more parameters are invalid.
*            NTE_NO_MEMORY                   A memory allocation failure occurred.
*/
SECURITY_STATUS WINAPI CSSKSPOpenProvider(__out   NCRYPT_PROV_HANDLE *phProvider,
                                          __in    LPCWSTR pszProviderName,
                                          __in    DWORD   dwFlags)
{
    SECURITY_STATUS status = NTE_INTERNAL_ERROR;
    CSSKSP_PROVIDER *pProvider = NULL;
    DWORD cbLength = 0;
    size_t cbProviderName = 0;
	int i;  // loop counter

    UNREFERENCED_PARAMETER(dwFlags);

    DebugOut("CSSKSPOpenProvider called.");

    // Validate input parameters.
    if (phProvider == NULL)
    {
        status = NTE_INVALID_PARAMETER;
        goto cleanup;
    }
    if (pszProviderName == NULL)
    {
        status = NTE_INVALID_PARAMETER;
        goto cleanup;
    }

    //The size of the provider name should be limited.
    cbProviderName = (wcslen(pszProviderName) + 1) * sizeof(WCHAR);
    if (cbProviderName > MAXUSHORT)
    {
        status = NTE_INVALID_PARAMETER;
        goto cleanup;
    }

    // Allocate memory for provider object.
    cbLength = sizeof(CSSKSP_PROVIDER)+(DWORD)cbProviderName;
    pProvider = (CSSKSP_PROVIDER*)HeapAlloc(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, cbLength);

    //Assign values to fields of the provider handle.
    pProvider->cbLength = cbLength;
    pProvider->dwMagic = CSSKSP_PROVIDER_MAGIC;
    pProvider->dwFlags = 0;
	pProvider->openKeyHandleCount = 0;
	memset(pProvider->openKeyHandles, 0, sizeof(pProvider->openKeyHandles));
    pProvider->pszName = (LPWSTR)(pProvider + 1); // put the name at the end of the provider structure
    if ((_wcsicmp(pszProviderName, CSSKSP_SECONDARY_PROVIDER_NAME) == 0) || (_wcsicmp(pszProviderName, LCSSCSP_SECONDARY_NAME) == 0))
    {
        pProvider->bSecondary = TRUE;
    }
    else
    {
        pProvider->bSecondary = FALSE;
    }
    CopyMemory(pProvider->pszName, pszProviderName, cbProviderName);
    pProvider->hCspDLL = NULL;

    // load the csscsp dll, note that this is the last thing that can faild, and therefore is not cleaned up
    if (CSSKSPLoadCSP(pProvider) != ERROR_SUCCESS)
    {
        LogError("Failed to load the csscsp.dll");
        status = NTE_FAIL;
        goto cleanup;
    } 

	// add this to the list of valid handle values, this list is checked in the free function
	for (i = 0; i < MAX_PROVIDER_HANDLES; ++i)
	{
		if (g_openProviders[i] == 0)
		{
			g_openProviders[i] = (NCRYPT_PROV_HANDLE)pProvider;
			break;
		}
	}
	// we have to return an error if we cannot find a slot in the list of provider handles
	if (i >= MAX_PROVIDER_HANDLES)
	{
		DebugOut("CSSKSPOpenProvider: too many open provider handles for this process.");
		status = NTE_BUFFER_TOO_SMALL;
		goto cleanup;
	}

    //Assign the output value.
    *phProvider = (NCRYPT_PROV_HANDLE)pProvider;
    pProvider = NULL;
    status = ERROR_SUCCESS;
cleanup:
    if (pProvider)
    {     
        CSSKSPFreeProvider((NCRYPT_PROV_HANDLE)pProvider);
        pProvider = NULL;
    }
    return status;
}


/******************************************************************************
* DESCRIPTION :     Release a handle to the KSP provider
*
* INPUTS :
*            NCRYPT_PROV_HANDLE hProvider    A handle to the KSP provider
* RETURN :
*            ERROR_SUCCESS                   The function was successful.
*            NTE_INVALID_HANDLE              The handle is not a valid KSP
*                                            provider handle.
*/
SECURITY_STATUS WINAPI CSSKSPFreeProvider(__in NCRYPT_PROV_HANDLE hProvider)
{
    SECURITY_STATUS Status = NTE_INTERNAL_ERROR;
    CSSKSP_PROVIDER *pProvider = NULL;
    int i;

    DebugOut("CSSKSPFreeProvider called.");

    // Validate input parameters.
    pProvider = CssKspValidateProvHandle(hProvider);

    if (pProvider == NULL)
    {
        Status = NTE_INVALID_HANDLE;
        goto cleanup;
    }

    // release the csp dll
    if (pProvider->hCspDLL)
    {
        FreeLibrary(pProvider->hCspDLL);
        pProvider->hCspDLL = 0;
    }

	// free the entry in the list of providers
	for (i = 0; i < MAX_PROVIDER_HANDLES; ++i)
	{
		if (g_openProviders[i] == hProvider)
		{
			g_openProviders[i] = NULL;
			break;
		}
	}

    ZeroMemory(pProvider, pProvider->cbLength);
    HeapFree(GetProcessHeap(), 0, pProvider);

    Status = ERROR_SUCCESS;
cleanup:

    return Status;
}


/******************************************************************************
* DESCRIPTION :     Open a key in the key storage provider
*
* INPUTS :
*            NCRYPT_PROV_HANDLE hProvider    A handle to the KSP provider
*            LPCWSTR pszKeyName              Name of the key
*            DWORD  dwLegacyKeySpec          Flags for legacy key support (unused)
*            DWORD   dwFlags                 Flags (unused)
* OUTPUTS:
*            NCRYPT_KEY_HANDLE               A handle to the opened key
* RETURN :
*            ERROR_SUCCESS                   The function was successful.
*            NTE_INVALID_HANDLE              The handle is not a valid KSP
*                                            provider handle.
*            NTE_INVALID_PARAMETER           One or more parameters are invalid.
*            NTE_NO_MEMORY                   A memory allocation failure occurred.
*/
SECURITY_STATUS WINAPI CSSKSPOpenKey(__inout NCRYPT_PROV_HANDLE hProvider,
                                     __out   NCRYPT_KEY_HANDLE *phKey,
                                     __in    LPCWSTR pszKeyName,
                                     __in_opt DWORD  dwLegacyKeySpec,
                                     __in    DWORD   dwFlags)
{
    SECURITY_STATUS Status = NTE_NOT_SUPPORTED;
    HCRYPTPROV hKey;
    CSSKSP_PROVIDER *pProvider = NULL;
    CSSKSP_KEY *pKey = NULL;
    VTableProvStruc vTable;
    char *pKeyName = NULL;
    char *pProviderName = NULL;
    size_t strSize;
    int wstrsize;
    int i;

    DebugOut("CSSKSPOpenKey called.");

    //
    // Validate input parameters.
    //
    UNREFERENCED_PARAMETER(dwLegacyKeySpec);
    UNREFERENCED_PARAMETER(dwFlags);

    pProvider = CssKspValidateProvHandle(hProvider);
    if (pProvider == NULL)
    {
        Status = NTE_INVALID_HANDLE;
        goto cleanup;
    }

    if ((phKey == NULL) || (pszKeyName == NULL))
    {
        Status = NTE_INVALID_PARAMETER;
        goto cleanup;
    }

	if (pProvider->openKeyHandleCount > MAX_OPEN_KEY_HANDLES) 
	{
		Status = NTE_BUFFER_TOO_SMALL;
		DebugOut("CSSKSPOpenKey: there are too many open keys for this provider.");
		goto cleanup;
	}

    // convert the provider name to str from lstr
    wstrsize = lstrlenW(pProvider->pszName) + 1;
    pProviderName = (char *)HeapAlloc(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, wstrsize);

    wcstombs_s(&strSize, pProviderName, (size_t)wstrsize, pProvider->pszName, (size_t)wstrsize);

    if (!pProvider->bSecondary)
    {
        vTable.pszProvName = CSSCSP_NAME;
    }
    else
    {
        vTable.pszProvName = CSSCSP_SECONDARY_NAME;
    }

    // convert the provider name to str from lstr
    wstrsize = lstrlenW(pszKeyName) + 1;
    pKeyName = (char *)HeapAlloc(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, wstrsize);

    wcstombs_s(&strSize, pKeyName, (size_t)wstrsize, pszKeyName, (size_t)wstrsize);

    // acquite the CSP context with the key name.
    if (cspAcquireContext(&hKey, pKeyName, dwFlags, &vTable) == FALSE)
    {
        Status = NTE_FAIL;
        goto cleanup;
    }

    // The calling layer had better free this key or else it will leak.
    *phKey = hKey;

	// save the key for validation checking on freekey.
	for (i = 0; i < MAX_OPEN_KEY_HANDLES; ++i)
	{
		if (pProvider->openKeyHandles[pProvider->openKeyHandleCount] == 0)
		{
			pProvider->openKeyHandles[pProvider->openKeyHandleCount] = hKey;
			pProvider->openKeyHandleCount++;
		}
	}

    Status = ERROR_SUCCESS;
cleanup:
    if (pKeyName) 
    {
        HeapFree(GetProcessHeap(), 0, pKeyName);
        pKeyName = NULL;
    }

    if (pProviderName) 
    {
        HeapFree(GetProcessHeap(), 0, pProviderName);
        pProviderName = NULL;
    }
    return Status;
}

/******************************************************************************
* DESCRIPTION :  Exports a sample key storage key into a memory BLOB.
*
* INPUTS :
*            NCRYPT_PROV_HANDLE hProvider     A handle to a KSP provider
*                                             object.
*            NCRYPT_KEY_HANDLE hKey           A handle to the KSP key
*                                             object to export.
*            NCRYPT_KEY_HANDLE hExportKey     Unused
*            LPCWSTR pszBlobType              Type of the key blob.
*            NCryptBufferDesc *pParameterList Additional parameter information.
*            DWORD   cbOutput                 Size of the key blob.
*            DWORD   dwFlags                  Flags
*
* OUTPUTS:
*            PBYTE pbOutput                  Key blob.
*            DWORD * pcbResult               Required size of the key blob.
* RETURN :
*            ERROR_SUCCESS                   The function was successful.
*            NTE_INVALID_HANDLE              The handle is not a valid KSP
*                                            provider handle.
*            NTE_INVALID_PARAMETER           One or more parameters are invalid.
*            NTE_BAD_FLAGS                   dwFlags contains invalid value.
*            NTE_NOT_SUPPORTED               The type of the key blob is not
*                                            supported.
*            NTE_NO_MEMORY                   A memory allocation failure occurred.
*            NTE_INTERNAL_ERROR              Encoding failed.
*/
SECURITY_STATUS WINAPI CSSKSPExportKey(__in    NCRYPT_PROV_HANDLE hProvider,
                                       __in    NCRYPT_KEY_HANDLE hKey,
                                       __in_opt NCRYPT_KEY_HANDLE hExportKey,
                                       __in    LPCWSTR pszBlobType,
                                       __in_opt NCryptBufferDesc *pParameterList, 
                                       __out_bcount_part_opt(cbOutput, *pcbResult) PBYTE pbOutput,
                                       __in    DWORD   cbOutput,
                                       __out   DWORD * pcbResult,
                                       __in    DWORD   dwFlags)
{
    SECURITY_STATUS Status = ERROR_SUCCESS;
    NTSTATUS          ntStatus = STATUS_INTERNAL_ERROR;
    DWORD cbKey = 0;
    BYTE *pKey  = NULL;

    NCRYPT_KEY_HANDLE hPublicKey = 0;
    NCRYPT_PROV_HANDLE hProv = 0;

    DebugOut("CSSKSPExportKey called");

    // TODO: validate the key handle
    // TODO: Convert blob type

    if (cspCPExportKey(hKey, 1234, 1234, PUBLICKEYBLOB, 0, NULL, &cbKey) == FALSE)
    {
        Status = NTE_FAIL;
        goto cleanup;
    }
    pKey = (BYTE *)HeapAlloc(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, cbKey);
    if (cspCPExportKey(hKey, 1234, 1234, PUBLICKEYBLOB, 0, pKey, &cbKey) == FALSE)
    {
        Status = NTE_FAIL;
        goto cleanup;
    }

    // open a key storage provider the can be used to conver the key from
    // the legacy format to a format that is supported by BCrypt
    ntStatus = NCryptOpenStorageProvider(&hProv, MS_KEY_STORAGE_PROVIDER, 0);
    if (ntStatus != ERROR_SUCCESS) 
    {
        LogError("NCryptOpenStorageProvider failed with error: 0x%08x", ntStatus);
        Status = NTE_FAIL;
        goto cleanup;
    }

    // import the legacy key
    ntStatus = NCryptImportKey(hProv, 0, LEGACY_RSAPUBLIC_BLOB, NULL,
                                &hPublicKey, pKey, cbKey, 0);
    if (ntStatus != ERROR_SUCCESS) 
    {
        LogError("NCryptImportKey failed with error: 0x%08x", ntStatus);
        Status = NTE_FAIL;
        goto cleanup;
    }

    // now export the key in the requested format
    Status = NCryptExportKey(hPublicKey, 0, pszBlobType, pParameterList,  pbOutput, cbOutput, pcbResult, dwFlags);
    if (Status != ERROR_SUCCESS) 
    {
        LogError("NCryptExportKey failed with error: 0x%08x", Status);
        goto cleanup;
    }

cleanup:

    if (pKey)
    {
        HeapFree(GetProcessHeap(), 0, pKey);
        pKey = NULL;
    }

    // free the key
    if (hPublicKey) 
    {
        NCryptDeleteKey(hPublicKey, 0);
    }

    // free the provider
    if (hProv) 
    {
        NCryptFreeObject(hProv);
    }

    return Status;
}

/******************************************************************************
* DESCRIPTION :  creates a signature of a hash value.
*
* INPUTS :
*            NCRYPT_PROV_HANDLE hProvider    A handle to a KSP provider
*                                            object
*            NCRYPT_KEY_HANDLE hKey          A handle to a KSP key object
*            VOID    *pPaddingInfo           Padding information is padding sheme
*                                            is used
*            PBYTE  pbHashValue              Hash to sign.
*            DWORD  cbHashValue              Size of the hash.
*            DWORD  cbSignature              Size of the signature
*            DWORD  dwFlags                  Flags
* OUTPUTS:
*            PBYTE  pbSignature              Output buffer containing signature.
*                                            If pbOutput is NULL, required buffer
*                                            size will return in *pcbResult.
*            DWORD * pcbResult               Required size of the output buffer.
* RETURN :
*            ERROR_SUCCESS                   The function was successful.
*            NTE_BAD_KEY_STATE               The key identified by the hKey
*                                            parameter has not been finalized
*                                            or is incomplete.
*            NTE_INVALID_HANDLE              The handle is not a valid KSP
*                                            provider or key handle.
*            NTE_INVALID_PARAMETER           One or more parameters are invalid.
*            NTE_BUFFER_TOO_SMALL            Output buffer is too small.
*            NTE_BAD_FLAGS                   dwFlags contains invalid value.
*/
SECURITY_STATUS WINAPI CSSKSPSignHash(__in    NCRYPT_PROV_HANDLE hProvider,
                                      __in    NCRYPT_KEY_HANDLE hKey,
                                      __in_opt    VOID  *pPaddingInfo,
                                      __in_bcount(cbHashValue) PBYTE pbHashValue,
                                      __in    DWORD   cbHashValue,
                                      __out_bcount_part_opt(cbSignaturee, *pcbResult) PBYTE pbSignature,
                                      __in    DWORD   cbSignaturee,
                                      __out   DWORD * pcbResult,
                                      __in    DWORD   dwFlags)
{
    SECURITY_STATUS Status = NTE_INTERNAL_ERROR;
    CSSKSP_PROVIDER *pProvider = NULL;

    DebugOut("CSSKSPSignHash called.");
    pProvider = CssKspValidateProvHandle(hProvider);
    if (pProvider == NULL)
    {
        Status = NTE_INVALID_HANDLE;
        goto cleanup;
    }
        
    if (cspCPSignHashValue(hKey, pbHashValue, cbHashValue, NULL, dwFlags, pbSignature, pcbResult) == FALSE) 
    {
        Status = NTE_INTERNAL_ERROR;
        goto cleanup;
    }
    else
    {
        int i, sigLen;

        if (pcbResult != NULL && pbSignature != NULL)
        {
            sigLen = *pcbResult;
            // cspCPSignHashValue returns value is little endian format, we need to return a big-endian number
	        for( i = 0; i < sigLen/2; ++i)
	        {
		        BYTE b = pbSignature[i];
		        pbSignature[i] = pbSignature[sigLen-1-i];
		        pbSignature[sigLen-1-i] = b;
	        }
        }
    }
    Status = ERROR_SUCCESS;
cleanup:
    return Status;
}


/******************************************************************************
* DESCRIPTION :     Create a new key and stored it into the user profile
*                   key storage area
*
* INPUTS :
*            NCRYPT_PROV_HANDLE hProvider    A handle to the KSP provider
*            LPCWSTR pszAlgId                Cryptographic algorithm to create the key
*            LPCWSTR pszKeyName              Name of the key
*            DWORD  dwLegacyKeySpec          Flags for legacy key support (unused)
*            DWORD   dwFlags                 0|NCRYPT_OVERWRITE_KEY_FLAG
* OUTPUTS:
*            NCRYPT_KEY_HANDLE               A handle to the opened key
* RETURN :
*            ERROR_SUCCESS                   The function was successful.
*            NTE_INVALID_HANDLE              The handle is not a valid KSP
*                                            provider handle.
*            NTE_EXISTS                      The key already exists.
*            NTE_INVALID_PARAMETER           One or more parameters are invalid.
*            NTE_NO_MEMORY                   A memory allocation failure occurred.
*            NTE_NOT_SUPPORTED               The algorithm is not supported.
*            NTE_BAD_FLAGS                   dwFlags contains invalid value.
*/
SECURITY_STATUS WINAPI CSSKSPCreatePersistedKey(__in    NCRYPT_PROV_HANDLE hProvider,
                                                __out   NCRYPT_KEY_HANDLE *phKey,
                                                __in    LPCWSTR pszAlgId,
                                                __in_opt LPCWSTR pszKeyName,
                                                __in    DWORD   dwLegacyKeySpec,
                                                __in    DWORD   dwFlags)
{
    SECURITY_STATUS Status = NTE_NOT_SUPPORTED;

    DebugOut("CSSKSPCreatePersistedKey called.");

    return Status;
}

/******************************************************************************
* DESCRIPTION :  Retrieves the value of a named property for a key storage
*                provider object.
*
* INPUTS :
*            NCRYPT_PROV_HANDLE hProvider    A handle to the KSP provider
*            LPCWSTR pszProperty             Name of the property
*            DWORD   cbOutput                Size of the output buffer
*            DWORD   dwFlags                 Flags
* OUTPUTS:
*            PBYTE   pbOutput                Output buffer containing the value
*                                            of the property.  If pbOutput is NULL,
*                                            required buffer size will return in
*                                            *pcbResult.
*            DWORD * pcbResult               Required size of the output buffer
* RETURN :
*            ERROR_SUCCESS                   The function was successful.
*            NTE_INVALID_HANDLE              The handle is not a valid KSP
*                                            provider handle.
*            NTE_NOT_FOUND                   Cannot find such a property.
*            NTE_INVALID_PARAMETER           One or more parameters are invalid.
*            NTE_BUFFER_TOO_SMALL            Output buffer is too small.
*            NTE_NOT_SUPPORTED               The property is not supported.
*            NTE_BAD_FLAGS                   dwFlags contains invalid value.
*/
SECURITY_STATUS WINAPI CSSKSPGetProviderProperty(__in    NCRYPT_PROV_HANDLE hProvider,
                                                 __in    LPCWSTR pszProperty,
                                                 __out_bcount_part_opt(cbOutput, *pcbResult) PBYTE pbOutput,
                                                 __in    DWORD   cbOutput,
                                                 __out   DWORD * pcbResult,
                                                 __in    DWORD   dwFlags)
{
    SECURITY_STATUS Status = NTE_NOT_SUPPORTED;

    DebugOut("CSSKSPGetProviderProperty called.");

    return Status;
}

/******************************************************************************
* DESCRIPTION :  Retrieves the value of a named property for a key storage
*                key object.
*
* INPUTS :
*            NCRYPT_PROV_HANDLE hProvider    A handle to a KSP provider
*                                            object
*            NCRYPT_KEY_HANDLE hKey          A handle to a KSP key object
*            LPCWSTR pszProperty             Name of the property
*            DWORD   cbOutput                Size of the output buffer
*            DWORD   dwFlags                 Flags
* OUTPUTS:
*            PBYTE   pbOutput                Output buffer containing the value
*                                            of the property.  If pbOutput is NULL,
*                                            required buffer size will return in
*                                            *pcbResult.
*            DWORD * pcbResult               Required size of the output buffer
* RETURN :
*            ERROR_SUCCESS                   The function was successful.
*            NTE_INVALID_HANDLE              The handle is not a valid KSP
*                                            provider handle.
*            NTE_NOT_FOUND                   Cannot find such a property.
*            NTE_INVALID_PARAMETER           One or more parameters are invalid.
*            NTE_BUFFER_TOO_SMALL            Output buffer is too small.
*            NTE_NOT_SUPPORTED               The property is not supported.
*            NTE_BAD_FLAGS                   dwFlags contains invalid value.
*/
SECURITY_STATUS WINAPI CSSKSPGetKeyProperty(__in    NCRYPT_PROV_HANDLE hProvider,
                                            __in    NCRYPT_KEY_HANDLE hKey,
                                            __in    LPCWSTR pszProperty,
                                            __out_bcount_part_opt(cbOutput, *pcbResult) PBYTE pbOutput,
                                            __in    DWORD   cbOutput,
                                            __out   DWORD * pcbResult,
                                            __in    DWORD   dwFlags)
{
    SECURITY_STATUS Status = NTE_INTERNAL_ERROR;
    CSSKSP_PROVIDER *pProvider = NULL;
    DWORD dwProperty = 0;
    DWORD cbResult = 0;
    LPWSTR pszAlgorithm = NULL;
    LPWSTR pszAlgorithmGroup = NULL;
    DWORD cbSecurityInfo = 0;
    DWORD cbTmp = 0;

    DebugOut("CSSKSPGetKeyProperty called: %S", pszProperty);

    //
    // Validate input parameters.
    //
    pProvider = CssKspValidateProvHandle(hProvider);

    if(pProvider == NULL)
    {
        Status = NTE_INVALID_HANDLE;
        goto cleanup;
    }

    if ((pszProperty == NULL) ||
        (wcslen(pszProperty) > NCRYPT_MAX_PROPERTY_NAME) ||
        (pcbResult == NULL))
    {
        Status = NTE_INVALID_PARAMETER;
        goto cleanup;
    }

    //NCRYPT_SILENT_FLAG is ignored in this KSP.
    dwFlags &= ~NCRYPT_SILENT_FLAG;

    //If this is to get the security descriptor, the flags
    //must be one of the OWNER_SECURITY_INFORMATION |GROUP_SECURITY_INFORMATION |
    //DACL_SECURITY_INFORMATION|LABEL_SECURITY_INFORMATION | SACL_SECURITY_INFORMATION.
    if (wcscmp(pszProperty, NCRYPT_SECURITY_DESCR_PROPERTY) == 0)
    {
        if ((dwFlags == 0) || ((dwFlags & ~(OWNER_SECURITY_INFORMATION |
            GROUP_SECURITY_INFORMATION |
            DACL_SECURITY_INFORMATION |
            LABEL_SECURITY_INFORMATION |
            SACL_SECURITY_INFORMATION)) != 0))
        {
            Status = NTE_BAD_FLAGS;
            goto cleanup;
        }
    }
    else
    {
        //Otherwise,Only NCRYPT_PERSIST_ONLY_FLAG is a valid flag.
        if (dwFlags & ~NCRYPT_PERSIST_ONLY_FLAG)
        {
            Status = NTE_BAD_FLAGS;
            goto cleanup;
        }
    }

    //
    //Determine length of requested property.
    //
    if (wcscmp(pszProperty, NCRYPT_ALGORITHM_PROPERTY) == 0)
    {
        dwProperty = CSSKSP_ALGORITHM_PROPERTY;
        pszAlgorithm = BCRYPT_RSA_ALGORITHM;
        cbResult = (DWORD)(wcslen(pszAlgorithm) + 1) * sizeof(WCHAR);
    } 
    else if (wcscmp(pszProperty, NCRYPT_ALGORITHM_GROUP_PROPERTY) == 0)
    {
        dwProperty = CSSKSP_ALGORITHM_GROUP_PROPERTY;
        pszAlgorithmGroup = NCRYPT_RSA_ALGORITHM_GROUP;
        cbResult = (DWORD)(wcslen(pszAlgorithmGroup) + 1) * sizeof(WCHAR);
    }
    else 
    {
        Status = NTE_NOT_SUPPORTED;
        goto cleanup;
    }

    //
    // Validate the size of the output buffer.
    //

    *pcbResult = cbResult;

    if (pbOutput == NULL)
    {
        Status = ERROR_SUCCESS;
        goto cleanup;
    }

    if (cbOutput < *pcbResult)
    {
        Status = NTE_BUFFER_TOO_SMALL;
        goto cleanup;
    }
    
    //
    // Retrieve the requested property data.
    //
    switch (dwProperty)
    {
    case CSSKSP_ALGORITHM_PROPERTY:
        CopyMemory(pbOutput, pszAlgorithm, cbResult);
        break;
    case CSSKSP_ALGORITHM_GROUP_PROPERTY:
        CopyMemory(pbOutput, pszAlgorithmGroup, cbResult);
        break;

    }
    Status = ERROR_SUCCESS;

cleanup:

    return Status;
}


/******************************************************************************
* DESCRIPTION :  Sets the value for a named property for a CNG key storage
*                provider object.
*
* INPUTS :
*            NCRYPT_PROV_HANDLE hProvider    A handle to the KSP provider
*            LPCWSTR pszProperty             Name of the property
*            PBYTE   pbInput                 Input buffer containing the value
*                                            of the property
*            DWORD   cbOutput                Size of the input buffer
*            DWORD   dwFlags                 Flags
*
* RETURN :
*            ERROR_SUCCESS                   The function was successful.
*            NTE_INVALID_HANDLE              The handle is not a valid KSP
*                                            provider handle.
*            NTE_INVALID_PARAMETER           One or more parameters are invalid.
*            NTE_NOT_SUPPORTED               The property is not supported.
*            NTE_BAD_FLAGS                   dwFlags contains invalid value.
*            NTE_NO_MEMORY                   A memory allocation failure occurred.
*/
SECURITY_STATUS
WINAPI
CSSKSPSetProviderProperty(
__in    NCRYPT_PROV_HANDLE hProvider,
__in    LPCWSTR pszProperty,
__in_bcount(cbInput) PBYTE pbInput,
__in    DWORD   cbInput,
__in    DWORD   dwFlags)
{
    SECURITY_STATUS Status = NTE_NOT_SUPPORTED;

    DebugOut("CSSKSPSetProviderProperty called.");

    return Status;
}

/******************************************************************************
* DESCRIPTION :  Set the value of a named property for a key storage
*                key object.
*
* INPUTS :
*            NCRYPT_PROV_HANDLE hProvider    A handle to a KSP provider
*                                            object
*            NCRYPT_KEY_HANDLE hKey          A handle to a KSP key object
*            LPCWSTR pszProperty             Name of the property
*            PBYTE   pbInput                 Input buffer containing the value
*                                            of the property
*            DWORD   cbOutput                Size of the input buffer
*            DWORD   dwFlags                 Flags
*
* RETURN :
*            ERROR_SUCCESS                   The function was successful.
*            NTE_INVALID_HANDLE              The handle is not a valid KSP
*                                            provider handle or a valid key handle
*            NTE_INVALID_PARAMETER           One or more parameters are invalid.
*            NTE_NOT_SUPPORTED               The property is not supported.
*            NTE_BAD_FLAGS                   dwFlags contains invalid value.
*            NTE_NO_MEMORY                   A memory allocation failure occurred.
*/
SECURITY_STATUS WINAPI CSSKSPSetKeyProperty(__in    NCRYPT_PROV_HANDLE hProvider,
                                            __inout NCRYPT_KEY_HANDLE hKey,
                                            __in    LPCWSTR pszProperty,
                                            __in_bcount(cbInput) PBYTE pbInput,
                                            __in    DWORD   cbInput,
                                            __in    DWORD   dwFlags)
{
    SECURITY_STATUS Status = ERROR_SUCCESS;

    DebugOut("CSSKSPSetKeyProperty called.");

    return Status;
}

/******************************************************************************
* DESCRIPTION :     Completes a key storage key. The key cannot be used
*                   until this function has been called.
*
* INPUTS :
*            NCRYPT_PROV_HANDLE hProvider    A handle to the KSP provider
*            NCRYPT_KEY_HANDLE hKey          A handle to a KSP key
*            DWORD   dwFlags                 Flags
* RETURN :
*            ERROR_SUCCESS                   The function was successful.
*            NTE_INVALID_HANDLE              The handle is not a valid KSP
*                                            provider handle.
*            NTE_INVALID_PARAMETER           One or more parameters are invalid.
*            NTE_NO_MEMORY                   A memory allocation failure occurred.
*            NTE_BAD_FLAGS                   The dwFlags parameter contains a
*                                            value that is not valid.
*/
SECURITY_STATUS WINAPI CSSKSPFinalizeKey(__in    NCRYPT_PROV_HANDLE hProvider,
                                         __in    NCRYPT_KEY_HANDLE hKey,
                                         __in    DWORD   dwFlags)
{
    SECURITY_STATUS Status = NTE_NOT_SUPPORTED;
    DebugOut("CSSKSPFinalizeKey called.");
    return Status;
}

/******************************************************************************
* DESCRIPTION :     Deletes a CNG KSP key
*
* INPUTS :
*            NCRYPT_PROV_HANDLE hProvider    A handle to the KSP provider
*            NCRYPT_KEY_HANDLE hKey          Handle to a KSP key
*            DWORD   dwFlags                 Flags
* RETURN :
*            ERROR_SUCCESS                   The function was successful.
*            NTE_INVALID_HANDLE              The handle is not a valid KSP
*                                            provider or key handle.
*            NTE_BAD_FLAGS                   The dwFlags parameter contains a
*                                            value that is not valid.
*            NTE_INTERNAL_ERROR              Key file deletion failed.
*/
SECURITY_STATUS WINAPI CSSKSPDeleteKey(__in    NCRYPT_PROV_HANDLE hProvider,
                                       __inout NCRYPT_KEY_HANDLE hKey,
__in    DWORD   dwFlags)
{
    SECURITY_STATUS Status = NTE_NOT_SUPPORTED;

    DebugOut("CSSKSPDeleteKey called.");

    return Status;
}

/******************************************************************************
* DESCRIPTION :     Free a CNG KSP key object
*
* INPUTS :
*            NCRYPT_PROV_HANDLE hProvider    A handle to the KSP provider
*            NCRYPT_KEY_HANDLE hKey          A handle to a KSP key
* RETURN :
*            ERROR_SUCCESS                   The function was successful.
*            NTE_INVALID_HANDLE              The handle is not a valid KSP
*/
SECURITY_STATUS WINAPI CSSKSPFreeKey(__in    NCRYPT_PROV_HANDLE hProvider,
                                     __in    NCRYPT_KEY_HANDLE hKey)
{
    SECURITY_STATUS Status = NTE_INTERNAL_ERROR;	
	CSSKSP_PROVIDER *pProvider;
	int i;

    DebugOut("CSSKSPFreeKey called.");

	pProvider = CssKspValidateProvHandle(hProvider);
	if (pProvider == NULL)
	{
		Status = NTE_INVALID_HANDLE;
	}
	else
	{
		// let this fall through the loop with success, meaning a missing
		// handle value will return success
		Status = ERROR_SUCCESS;
		// only free handles that are valid, see openkey
		for (i = 0; i < MAX_OPEN_KEY_HANDLES; ++i)
		{
			if (hKey == pProvider->openKeyHandles[i])
			{
				// free the key handle slot and decrement count
				pProvider->openKeyHandles[i] = 0;
				pProvider->openKeyHandleCount--;

				// hKey == hProvider for the CSP
				if (cspCPReleaseContext((HCRYPTPROV)hKey, 0) == FALSE)
				{
					LogError("Failed to release CSP context for hKey: 0x%08x", hKey);
					Status = NTE_FAIL;
				}
				else
				{
					Status = ERROR_SUCCESS;
					// remove the handle from our list
					pProvider->openKeyHandles[i] = 0;
				}
				break;
			}
		}
	}
    return Status;
}

/******************************************************************************
* DESCRIPTION :     free a CNG KSP memory buffer object
*
* INPUTS :
*            PVOID   pvInput                 The buffer to free.
* RETURN :
*            ERROR_SUCCESS                   The function was successful.
*/
SECURITY_STATUS
WINAPI
CSSKSPFreeBuffer(
__deref PVOID   pvInput)
{
    SECURITY_STATUS Status = NTE_NOT_SUPPORTED;

    DebugOut("CSSKSPFreeBuffer called.");

    return Status;
}


/******************************************************************************
* DESCRIPTION :  encrypts a block of data.
*
* INPUTS :
*            NCRYPT_PROV_HANDLE hProvider    A handle to a KSP provider
*                                            object.
*            NCRYPT_KEY_HANDLE hKey          A handle to a KSP key object.
*            PBYTE   pbInput                 Plain text data to be encrypted.
*            DWORD   cbInput                 Size of the plain text data.
*            VOID    *pPaddingInfo           Padding information if padding sheme
*                                            is used.
*            DWORD   cbOutput                Size of the output buffer.
*            DWORD   dwFlags                 Flags
* OUTPUTS:
*            PBYTE   pbOutput                Output buffer containing encrypted
*                                            data.  If pbOutput is NULL,
*                                            required buffer size will return in
*                                            *pcbResult.
*            DWORD * pcbResult               Required size of the output buffer
* RETURN :
*            ERROR_SUCCESS                   The function was successful.
*            NTE_BAD_KEY_STATE               The key identified by the hKey
*                                            parameter has not been finalized
*                                            or is incomplete.
*            NTE_INVALID_HANDLE              The handle is not a valid KSP
*                                            provider or key handle.
*            NTE_INVALID_PARAMETER           One or more parameters are invalid.
*            NTE_BUFFER_TOO_SMALL            Output buffer is too small.
*            NTE_BAD_FLAGS                   dwFlags contains invalid value.
*/
SECURITY_STATUS WINAPI CSSKSPEncrypt(__in    NCRYPT_PROV_HANDLE hProvider,
                                     __in    NCRYPT_KEY_HANDLE hKey,
                                     __in_bcount(cbInput) PBYTE pbInput,
                                     __in    DWORD   cbInput,
                                     __in    VOID *pPaddingInfo,
                                     __out_bcount_part_opt(cbOutput, *pcbResult) PBYTE pbOutput,
                                     __in    DWORD   cbOutput,
                                     __out   DWORD * pcbResult,
                                     __in    DWORD   dwFlags)
{
    SECURITY_STATUS Status = NTE_NOT_SUPPORTED;

    DebugOut("CSSKSPEncrypt called.");

    return Status;
}

/******************************************************************************
* DESCRIPTION :  Decrypts a block of data.
*
* INPUTS :
*            NCRYPT_PROV_HANDLE hProvider    A handle to a KSP provider
*                                            object.
*            NCRYPT_KEY_HANDLE hKey          A handle to a KSP key object.
*            PBYTE   pbInput                 Encrypted data blob.
*            DWORD   cbInput                 Size of the encrypted data blob.
*            VOID    *pPaddingInfo           Padding information if padding sheme
*                                            is used.
*            DWORD   cbOutput                Size of the output buffer.
*            DWORD   dwFlags                 Flags
* OUTPUTS:
*            PBYTE   pbOutput                Output buffer containing decrypted
*                                            data.  If pbOutput is NULL,
*                                            required buffer size will return in
*                                            *pcbResult.
*            DWORD * pcbResult               Required size of the output buffer
* RETURN :
*            ERROR_SUCCESS                   The function was successful.
*            NTE_BAD_KEY_STATE               The key identified by the hKey
*                                            parameter has not been finalized
*                                            or is incomplete.
*            NTE_INVALID_HANDLE              The handle is not a valid KSP
*                                            provider or key handle.
*            NTE_INVALID_PARAMETER           One or more parameters are invalid.
*            NTE_BUFFER_TOO_SMALL            Output buffer is too small.
*            NTE_BAD_FLAGS                   dwFlags contains invalid value.
*/
SECURITY_STATUS WINAPI CSSKSPDecrypt(__in    NCRYPT_PROV_HANDLE hProvider,
                                     __in    NCRYPT_KEY_HANDLE hKey,
                                     __in_bcount(cbInput) PBYTE pbInput,
                                     __in    DWORD   cbInput,
                                     __in    VOID *pPaddingInfo,
                                     __out_bcount_part_opt(cbOutput, *pcbResult) PBYTE pbOutput,
                                     __in    DWORD   cbOutput,
                                     __out   DWORD * pcbResult,
                                     __in    DWORD   dwFlags)
{
    SECURITY_STATUS Status = NTE_NOT_SUPPORTED;

    DebugOut("CSSKSPDecrypt called.");

    return Status;
}

/******************************************************************************
* DESCRIPTION :  Determines if a key storage provider supports a
*                specific cryptographic algorithm.
*
* INPUTS :
*            NCRYPT_PROV_HANDLE hProvider    A handle to a KSP provider
*                                            object
*            LPCWSTR pszAlgId                Name of the cryptographic
*                                            Algorithm in question
*            DWORD   dwFlags                 Flags
* RETURN :
*            ERROR_SUCCESS                   The algorithm is supported.
*            NTE_INVALID_HANDLE              The handle is not a valid KSP
*                                            provider or key handle.
*            NTE_INVALID_PARAMETER           One or more parameters are invalid.
*            NTE_BAD_FLAGS                   dwFlags contains invalid value.
*            NTE_NOT_SUPPORTED               This algorithm is not supported.
*/
SECURITY_STATUS WINAPI CSSKSPIsAlgSupported(__in    NCRYPT_PROV_HANDLE hProvider,
                                            __in    LPCWSTR pszAlgId,
                                            __in    DWORD   dwFlags)
{
    SECURITY_STATUS Status = NTE_NOT_SUPPORTED;

    DebugOut("CSSKSPIsAlgSupported called.");
    return Status;
}

/******************************************************************************
* DESCRIPTION :  Obtains the names of the algorithms that are supported by
*                the sample key storage provider.
*
* INPUTS :
*            NCRYPT_PROV_HANDLE hProvider    A handle to a KSP provider
*                                            object.
*            DWORD   dwAlgOperations         The crypto operations that are to
*                                            be enumerated.
*            DWORD   dwFlags                 Flags
*
* OUTPUTS:
*            DWORD * pdwAlgCount             Number of supported algorithms.
*            NCryptAlgorithmName **ppAlgList List of supported algorithms.
* RETURN :
*            ERROR_SUCCESS                   The function was successful.
*            NTE_INVALID_HANDLE              The handle is not a valid KSP
*                                            provider handle.
*            NTE_INVALID_PARAMETER           One or more parameters are invalid.
*            NTE_BAD_FLAGS                   dwFlags contains invalid value.
*            NTE_NOT_SUPPORTED               The crypto operations are not supported.
*/
SECURITY_STATUS WINAPI CSSKSPEnumAlgorithms(__in    NCRYPT_PROV_HANDLE hProvider,
                                            __in    DWORD   dwAlgOperations,
                                            __out   DWORD * pdwAlgCount,
                                            __deref_out_ecount(*pdwAlgCount) NCryptAlgorithmName **ppAlgList,
                                            __in    DWORD   dwFlags)
{
    SECURITY_STATUS Status = NTE_NOT_SUPPORTED;

    DebugOut("CSSKSPEnumAlgorithms called.");

    return Status;
}

/******************************************************************************
* DESCRIPTION :  Obtains the names of the keys that are stored by the provider.
*
* INPUTS :
*            NCRYPT_PROV_HANDLE hProvider    A handle to a KSP provider
*                                            object
*            LPCWSTR pszScope                Unused
*            NCryptKeyName **ppKeyName       Name of the retrieved key
*            PVOID * ppEnumState             Enumeration state information
*            DWORD   dwFlags                 Flags
*
* OUTPUTS:
*            PVOID * ppEnumState             Enumeration state information that
*                                            is used in subsequent calls to
*                                            this function.
* RETURN :
*            ERROR_SUCCESS                   The function was successful.
*            NTE_INVALID_HANDLE              The handle is not a valid KSP
*                                            provider handle.
*            NTE_INVALID_PARAMETER           One or more parameters are invalid.
*            NTE_BAD_FLAGS                   dwFlags contains invalid value.
*            NTE_NOT_SUPPORTED               NCRYPT_MACHINE_KEY_FLAG is not
*                                            supported.
*            NTE_NO_MEMORY                   A memory allocation failure occurred.
*/
SECURITY_STATUS WINAPI CSSKSPEnumKeys(__in    NCRYPT_PROV_HANDLE hProvider,
                                      __in_opt LPCWSTR pszScope,
                                      __deref_out NCryptKeyName **ppKeyName,
                                      __inout PVOID * ppEnumState,
                                      __in    DWORD   dwFlags)
{
    SECURITY_STATUS Status = NTE_NOT_SUPPORTED;

    DebugOut("CSSKSPEnumKeys called.");


    return Status;
}

/******************************************************************************
* DESCRIPTION :  Imports a key into the KSP from a memory BLOB.
*
* INPUTS :
*            NCRYPT_PROV_HANDLE hProvider     A handle to a KSP provider
*                                             object.
*            NCRYPT_KEY_HANDLE hImportKey     Unused
*            LPCWSTR pszBlobType              Type of the key blob.
*            NCryptBufferDesc *pParameterList Additional parameter information.
*            PBYTE   pbData                   Key blob.
*            DWORD   cbData                   Size of the key blob.
*            DWORD   dwFlags                  Flags
*
* OUTPUTS:
*            NCRYPT_KEY_HANDLE *phKey        KSP key object imported
*                                            from the key blob.
* RETURN :
*            ERROR_SUCCESS                   The function was successful.
*            NTE_INVALID_HANDLE              The handle is not a valid KSP
*                                            provider handle.
*            NTE_INVALID_PARAMETER           One or more parameters are invalid.
*            NTE_BAD_FLAGS                   dwFlags contains invalid value.
*            NTE_NOT_SUPPORTED               The type of the key blob is not
*                                            supported.
*            NTE_NO_MEMORY                   A memory allocation failure occurred.
*            NTE_INTERNAL_ERROR              Decoding failed.
*/
SECURITY_STATUS WINAPI CSSKSPImportKey(__in    NCRYPT_PROV_HANDLE hProvider,
                                       __in_opt NCRYPT_KEY_HANDLE hImportKey,
                                       __in    LPCWSTR pszBlobType,
                                       __in_opt NCryptBufferDesc *pParameterList,
                                       __out   NCRYPT_KEY_HANDLE *phKey,
                                       __in_bcount(cbData) PBYTE pbData,
                                       __in    DWORD   cbData,
                                       __in    DWORD   dwFlags)
{
    SECURITY_STATUS Status = NTE_NOT_SUPPORTED;

    DebugOut("CSSKSPImportKey called.");

    return Status;
}

/******************************************************************************
* DESCRIPTION :  Verifies that the specified signature matches the specified hash
*
* INPUTS :
*            NCRYPT_PROV_HANDLE hProvider    A handle to a KSP provider
*                                            object.
*            NCRYPT_KEY_HANDLE hKey          A handle to a KSP key object
*            VOID    *pPaddingInfo           Padding information is padding sheme
*                                            is used.
*            PBYTE  pbHashValue              Hash data
*            DWORD  cbHashValue              Size of the hash
*            PBYTE  pbSignature              Signature data
*            DWORD  cbSignaturee             Size of the signature
*            DWORD  dwFlags                  Flags
*
* RETURN :
*            ERROR_SUCCESS                   The signature is a valid signature.
*            NTE_BAD_KEY_STATE               The key identified by the hKey
*                                            parameter has not been finalized
*                                            or is incomplete.
*            NTE_INVALID_HANDLE              The handle is not a valid KSP
*                                            provider or key handle.
*            NTE_INVALID_PARAMETER           One or more parameters are invalid.
*            NTE_BAD_FLAGS                   dwFlags contains invalid value.
*/
SECURITY_STATUS
WINAPI
CSSKSPVerifySignature(
__in    NCRYPT_PROV_HANDLE hProvider,
__in    NCRYPT_KEY_HANDLE hKey,
__in_opt    VOID *pPaddingInfo,
__in_bcount(cbHashValue) PBYTE pbHashValue,
__in    DWORD   cbHashValue,
__in_bcount(cbSignaturee) PBYTE pbSignature,
__in    DWORD   cbSignaturee,
__in    DWORD   dwFlags)
{
    SECURITY_STATUS Status = NTE_NOT_SUPPORTED;

    DebugOut("CSSKSPVerifySignature called.");


    return Status;
}

SECURITY_STATUS
WINAPI
CSSKSPPromptUser(
__in    NCRYPT_PROV_HANDLE hProvider,
__in_opt NCRYPT_KEY_HANDLE hKey,
__in    LPCWSTR  pszOperation,
__in    DWORD   dwFlags)
{
    UNREFERENCED_PARAMETER(hProvider);
    UNREFERENCED_PARAMETER(hKey);
    UNREFERENCED_PARAMETER(pszOperation);
    UNREFERENCED_PARAMETER(dwFlags);

    DebugOut("CSSKSPPromptUser called.");
    return NTE_NOT_SUPPORTED;
}

SECURITY_STATUS
WINAPI
CSSKSPNotifyChangeKey(
__in    NCRYPT_PROV_HANDLE hProvider,
__inout HANDLE *phEvent,
__in    DWORD   dwFlags)
{
    UNREFERENCED_PARAMETER(hProvider);
    UNREFERENCED_PARAMETER(phEvent);
    UNREFERENCED_PARAMETER(dwFlags);

    DebugOut("CSSKSPNotifyChangeKey called.");
    return NTE_NOT_SUPPORTED;
}


SECURITY_STATUS
WINAPI
CSSKSPSecretAgreement(
__in    NCRYPT_PROV_HANDLE hProvider,
__in    NCRYPT_KEY_HANDLE hPrivKey,
__in    NCRYPT_KEY_HANDLE hPubKey,
__out   NCRYPT_SECRET_HANDLE *phAgreedSecret,
__in    DWORD   dwFlags)
{
    UNREFERENCED_PARAMETER(hProvider);
    UNREFERENCED_PARAMETER(hPrivKey);
    UNREFERENCED_PARAMETER(hPubKey);
    UNREFERENCED_PARAMETER(phAgreedSecret);
    UNREFERENCED_PARAMETER(dwFlags);

    DebugOut("CSSKSPSecretAgreement called.");
    return NTE_NOT_SUPPORTED;
}


SECURITY_STATUS
WINAPI
CSSKSPDeriveKey(
__in        NCRYPT_PROV_HANDLE   hProvider,
__in_opt    NCRYPT_SECRET_HANDLE hSharedSecret,
__in        LPCWSTR              pwszKDF,
__in_opt    NCryptBufferDesc     *pParameterList,
__out_bcount_part_opt(cbDerivedKey, *pcbResult) PUCHAR pbDerivedKey,
__in        DWORD                cbDerivedKey,
__out       DWORD                *pcbResult,
__in        ULONG                dwFlags)
{
    UNREFERENCED_PARAMETER(hProvider);
    UNREFERENCED_PARAMETER(hSharedSecret);
    UNREFERENCED_PARAMETER(pwszKDF);
    UNREFERENCED_PARAMETER(pParameterList);
    UNREFERENCED_PARAMETER(pbDerivedKey);
    UNREFERENCED_PARAMETER(cbDerivedKey);
    UNREFERENCED_PARAMETER(pcbResult);
    UNREFERENCED_PARAMETER(dwFlags);

    DebugOut("CSSKSPDeriveKey called.");
    return NTE_NOT_SUPPORTED;
}

SECURITY_STATUS
WINAPI
CSSKSPFreeSecret(
__in    NCRYPT_PROV_HANDLE hProvider,
__in    NCRYPT_SECRET_HANDLE hSharedSecret)
{
    UNREFERENCED_PARAMETER(hProvider);
    UNREFERENCED_PARAMETER(hSharedSecret);

    DebugOut("CSSKSPFreeSecret called.");
    return NTE_NOT_SUPPORTED;
}
