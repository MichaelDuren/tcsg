// csskspconfig.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <certenroll.h>
#include <windows.h>
#include <wincrypt.h>
#include <stdlib.h>
#include <stdio.h>
#include <bcrypt.h>
#include <ncrypt.h>
#include <conio.h>
#include <tchar.h>
#include <atlbase.h>
#include "..\cssksp\cssksp.h"
#include "..\csscommon\csslog.h"

extern BOOL g_debug = FALSE;

// Forward declaration.
HRESULT enumProviders(void);
///////////////////////////////////////////////////////////////////////////////
//
// Forward declarations of local routines...
//
///////////////////////////////////////////////////////////////////////////////
static void DisplayUsage(void);

static void EnumProviders(void);

static void RegisterProvider(void);

static void UnRegisterProvider(void);


///////////////////////////////////////////////////////////////////////////////
//
// The following section defines the characteristics of the
// provider being registered...
//
///////////////////////////////////////////////////////////////////////////////
//
// File name of sample key storage provider's binary. *NO* path.
//
#define CSSKSP_BINARY       L"cssksp.dll"

//
// An array of algorithm names, all belonging to the
// same algorithm class...
//
PWSTR AlgorithmNames[1] = {
    NCRYPT_KEY_STORAGE_ALGORITHM
};

//
// Definition of ONE class of algorithms supported
// by the provider...
//
CRYPT_INTERFACE_REG AlgorithmClass = {
    NCRYPT_KEY_STORAGE_INTERFACE,       // ncrypt key storage interface
    CRYPT_LOCAL,                        // Scope: local system only
    1,                                  // One algorithm in the class
    AlgorithmNames                      // The name(s) of the algorithm(s) in the class
};

//
// An array of ALL the algorithm classes supported
// by the provider...
//
PCRYPT_INTERFACE_REG AlgorithmClasses[1] = {
    &AlgorithmClass
};

//
// Definition of the provider's user-mode binary...
//
CRYPT_IMAGE_REG CSSKSPImage = {
    CSSKSP_BINARY,                   // File name of the sample KSP binary
    1,                                  // Number of algorithm classes the binary supports
    AlgorithmClasses                    // List of all algorithm classes available
};

PWSTR szAlias[] = {L"Thales CSS Cryptographic Provider", L"Thales CSS Secondary Provider", CSSKSP_SECONDARY_PROVIDER_NAME};

//
// Definition of the overall provider...
//
CRYPT_PROVIDER_REG CSSKSPProvider = {
    3,
    szAlias,
    &CSSKSPImage,  // Image that provides user-mode support
    NULL              // Image that provides kernel-mode support (*MUST* be NULL)
};
///////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
//
// Main entry point...
//
///////////////////////////////////////////////////////////////////////////////
int __cdecl wmain(__in int argc, __in_ecount(argc) PWSTR *argv)
{
    if (argc > 1)
    {
        InitGlobals();
        g_debug = 1;

        if ((_wcsicmp(argv[1], L"-register") == 0))
        {
            RegisterProvider();
        }
        else if ((_wcsicmp(argv[1], L"-unregister") == 0))
        {
            UnRegisterProvider();
        }
        else if ((_wcsicmp(argv[1], L"-enum") == 0))
        {
            EnumProviders();
        }
        else
        {
            wprintf(L"Unrecognized command \"%s\"\n", argv[1]);
            DisplayUsage();
        }
    }
    else
    {
        DisplayUsage();
    }

    return 0;
}

///////////////////////////////////////////////////////////////////////////////

void DisplayUsage()
{
    wprintf(L"Usage: CSSKSPconfig -enum | -register | -unregister\n");
    exit(1);
}
///////////////////////////////////////////////////////////////////////////////

void EnumProviders()
{
    NTSTATUS ntStatus = STATUS_SUCCESS;

    DWORD cbBuffer = 0;
    PCRYPT_PROVIDERS pBuffer = NULL;
    DWORD i = 0;

    ntStatus = BCryptEnumRegisteredProviders(&cbBuffer, &pBuffer);

    if (NT_SUCCESS(ntStatus))
    {
        if (pBuffer == NULL)
        {
            LogError("BCryptEnumRegisteredProviders returned a NULL ptr\n");
        }
        else
        {
            for (i = 0; i < pBuffer->cProviders; i++)
            {
                wprintf(L"%s\n", pBuffer->rgpszProviders[i]);
            }
        }
    }
    else
    {
        LogError("BCryptEnumRegisteredProviders failed with error code 0x%08x\n", ntStatus);
    }

    if (pBuffer != NULL)
    {
        BCryptFreeBuffer(pBuffer);
    }
}
///////////////////////////////////////////////////////////////////////////////

void
RegisterProvider(
void
)
{
    NTSTATUS ntStatus = STATUS_SUCCESS;

    //
    // Make CNG aware that our provider
    // exists...
    //
    ntStatus = BCryptRegisterProvider(
        CSSKSP_PROVIDER_NAME,
        0,                          // Flags: fail if provider is already registered
        &CSSKSPProvider
        );
    if (!NT_SUCCESS(ntStatus))
    {
        LogError("BCryptRegisterProvider failed with error code 0x%08x\n", ntStatus);
    }

    //
    // Add the algorithm name to the priority list of the
    // symmetric cipher algorithm class. (This makes it
    // visible to BCryptResolveProviders.)
    //
    ntStatus = BCryptAddContextFunction(
        CRYPT_LOCAL,                    // Scope: local machine only
        NULL,                           // Application context: default
        NCRYPT_KEY_STORAGE_INTERFACE,   // Algorithm class
        NCRYPT_KEY_STORAGE_ALGORITHM,   // Algorithm name
        CRYPT_PRIORITY_BOTTOM           // Lowest priority
        );
    if (!NT_SUCCESS(ntStatus))
    {
        LogError("BCryptAddContextFunction failed with error code 0x%08x\n", ntStatus);
    }

    //
    // Identify our new provider as someone who exposes
    // an implementation of the new algorithm.
    //
    ntStatus = BCryptAddContextFunctionProvider(
        CRYPT_LOCAL,                    // Scope: local machine only
        NULL,                           // Application context: default
        NCRYPT_KEY_STORAGE_INTERFACE,   // Algorithm class
        NCRYPT_KEY_STORAGE_ALGORITHM,   // Algorithm name
        CSSKSP_PROVIDER_NAME,        // Provider name
        CRYPT_PRIORITY_BOTTOM           // Lowest priority
        );
    if (!NT_SUCCESS(ntStatus))
    {
        LogError("BCryptAddContextFunctionProvider failed with error code 0x%08x\n", ntStatus);
    }
}
///////////////////////////////////////////////////////////////////////////////

void
UnRegisterProvider()
{
    NTSTATUS ntStatus = STATUS_SUCCESS;

    //
    // Tell CNG that this provider no longer supports
    // this algorithm.
    //
    ntStatus = BCryptRemoveContextFunctionProvider(
        CRYPT_LOCAL,                    // Scope: local machine only
        NULL,                           // Application context: default
        NCRYPT_KEY_STORAGE_INTERFACE,   // Algorithm class
        NCRYPT_KEY_STORAGE_ALGORITHM,   // Algorithm name
        CSSKSP_PROVIDER_NAME         // Provider
        );
    if (!NT_SUCCESS(ntStatus))
    {
        LogError("BCryptRemoveContextFunctionProvider failed with error code 0x%08x\n", ntStatus);
    }


    //
    // Tell CNG to forget about our provider component.
    //
    ntStatus = BCryptUnregisterProvider(CSSKSP_PROVIDER_NAME);
    if (!NT_SUCCESS(ntStatus))
    {
        LogError("BCryptUnregisterProvider failed with error code 0x%08x\n", ntStatus);
    }
}
///////////////////////////////////////////////////////////////////////////////





// enumeratinginstalledproviders.cpp : Defines the entry point for the console application.
//



HRESULT enumProviders(void)
{
    CComPtr<ICspInformations>     pCSPs;   // Provider collection
    CComPtr<ICspInformation>      pCSP;    // Provider instgance
    HRESULT           hr = S_OK;  // Return value
    long              lCount = 0;     // Count of providers
    CComBSTR          bstrName;            // Provider name
    VARIANT_BOOL      bLegacy;             // CryptoAPI or CNG

    // Create a collection of cryptographic providers.
    hr = CoCreateInstance(
        __uuidof(CCspInformations),
        NULL,
        CLSCTX_INPROC_SERVER,
        __uuidof(ICspInformations),
        (void **)&pCSPs);
    if (FAILED(hr)) return hr;

    // Add the providers installed on the computer.
    hr = pCSPs->AddAvailableCsps();
    if (FAILED(hr)) return hr;

    // Retrieve the number of installed providers.
    hr = pCSPs->get_Count(&lCount);
    if (FAILED(hr)) return hr;

    // Print the providers to the console. Print the
    // name and a value that specifies whether the 
    // CSP is a legacy or CNG provider.
    for (long i = 0; i<lCount; i++)
    {
        hr = pCSPs->get_ItemByIndex(i, &pCSP);
        if (FAILED(hr)) return hr;

        hr = pCSP->get_Name(&bstrName);
        if (FAILED(hr)) return hr;

        hr = pCSP->get_LegacyCsp(&bLegacy);
        if (FAILED(hr)) return hr;

        if (VARIANT_TRUE == bLegacy)
            wprintf_s(L"%2d. Legacy: ", i);
        else
            wprintf_s(L"%2d. CNG: ", i);

        wprintf_s(L"%s\n", static_cast<wchar_t*>(bstrName.m_str));

        pCSP = NULL;

    }

    printf_s("\n\nHit any key to continue: ");
    _getch();

    return hr;

}
