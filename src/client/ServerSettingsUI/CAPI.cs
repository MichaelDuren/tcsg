﻿using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Runtime.InteropServices;
using System.Diagnostics;

namespace ThalesCSS
{
    public class CAPI
    {
        public CAPI()
        {
        }
        public const uint PROV_RSA_FULL = 1;
        public const uint CRYPT_VERIFYCONTEXT = 0xF0000000;
        public const uint CRYPT_NEWKEYSET = 0x00000008;
        public enum ALG_ID
        {
            CALG_MD5 = 0x00008003
        }
        [DllImport("advapi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CryptAcquireContext(out IntPtr phProv, string pszContainer, string pszProvider, uint dwProvType, uint dwFlags);
        [DllImport("advapi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CryptCreateHash(IntPtr hProv, ALG_ID Algid, IntPtr hKey, uint dwFlags, out  IntPtr phHash);
        [DllImport("advapi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CryptHashData(IntPtr hHash, byte[] pbData, int dwDataLen, uint dwFlags);
        [DllImport("advapi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CryptDeriveKey(IntPtr hProv, ALG_ID Algid, IntPtr hBaseData, uint dwFlags, ref IntPtr phKey);
        [DllImport("advapi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CryptDestroyHash(IntPtr hHash);
        [DllImport("advapi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CryptDecrypt(IntPtr hKey, IntPtr hHash, [MarshalAs(UnmanagedType.Bool)]bool Final, uint dwFlags, byte[] pbData, ref int pdwDataLen);
        [DllImport("advapi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CryptDestroyKey(IntPtr hKey);
        [DllImport("advapi32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CryptReleaseContext(IntPtr hProv, uint dwFlags);
        [DllImport("csscsp.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool CPRefreshStore(string pszContainer);

        public class HighLevel
        {
            public static unsafe bool PingProv(string sProv, uint uiProvType)
            {
                IntPtr hCryptProv;
                bool rc = false;
                // Get a handle to the default provider. 
                if (!CryptAcquireContext(out hCryptProv, null, sProv, uiProvType, CRYPT_VERIFYCONTEXT))
                {
                    int ec;
                    ec = Marshal.GetLastWin32Error();
                    Debug.Write(ec.ToString());
                }
                else
                {
                    try
                    {
                        rc = CPRefreshStore(sProv);

                        // Release provider handle. 
                        if (hCryptProv != IntPtr.Zero)
                        {
                            CryptReleaseContext(hCryptProv, 0);
                        }
                    }
                    catch (Exception e)
                    {
                        Debug.Write(e.Message);
                    }
                }
                return rc;
            }
        }

    }
}