﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Runtime.InteropServices;
using ThalesCSS;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace ServerSettingsUI
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        
        [DllImport("kernel32.dll", SetLastError = true)]
        private static extern bool FreeConsole();

        [STAThread]
        static int Main( string[] args)
        {  
            RegistryKey csskey = null;
            string newUrl = null;

             if (args.Length >= 1)
             { 
                 if (args.Length > 1 && args[1].Contains("https:"))
                 {
                     newUrl = args[1];

                 }

                 csskey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\ThalesASG\CSSCSP", true);
                 if (csskey == null)
                 {
                     Console.WriteLine("The 'Secondary Thales CodeSign Server' CSP is null and not installed properly.", "Error");
                     return 1;
                 }

                 try
                 {
                     if (args[0].Contains("refresh"))
                     {
                         if (newUrl != null)
                         {
                             csskey.SetValue("ServerBaseURL", newUrl, RegistryValueKind.String);
                             Console.WriteLine("Thales CSS Cryptographic Provider Server URL updated.");
                         }

                         if (CAPI.HighLevel.PingProv("Thales CSS Cryptographic Provider", CAPI.PROV_RSA_FULL))
                         {
                             Console.WriteLine("Thales CSS Cryptographic Provider key list refreshed.");
                             return 0;
                         }
                         else
                         {
                             Console.WriteLine("Thales CSS Server URL update failed!");
                             return 1;
                         }
                     }
                     else
                     {
                         Console.WriteLine("Invalid command line option");
                         return 1;
                     }

                 }
                 catch (Exception e)
                 {
                     // Ignore exception writing to registry and try existing settings.
                     Console.WriteLine("The 'Thales CSS Server' registry key is null exception. " + e.Message);
                 }
                 return 1;
                   
             }
             else if ( args.Length > 0 )
             {
                 Console.WriteLine("The ServerSettingsUI command line usage:");
                 Console.WriteLine("	ServerSettingsUI https://192.168.0.1:8443");
                 Console.WriteLine("argument: Thales CSS Secondary Server URL must contains 'https' ");

                 return 1;
             }

            // dimiss console and run form
             FreeConsole();
             Application.EnableVisualStyles();
             Application.SetCompatibleTextRenderingDefault(false);
             Application.Run(new MainForm());
             
            return 0;
            
        }
    }
}
