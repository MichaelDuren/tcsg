﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using System.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Net;
using ThalesCSS;
using System.Net.Security;

namespace ServerSettingsUI
{
    public partial class MainForm : Form
    {
        public static int CERT_STORE_PROV_SYSTEM = 10;
        public static int CERT_SYSTEM_STORE_CURRENT_USER = (1 << 16);
        public static int CERT_SYSTEM_STORE_LOCAL_MACHINE = (2 << 16);

        private RegistryKey csskey = null;
        private bool bReadOnly = false;

        static bool AcceptServerCertificate(object sender, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }

        public MainForm()
        {
            InitializeComponent();

            // set this for the server checks
            ServicePointManager.ServerCertificateValidationCallback +=
            delegate (object sender, X509Certificate certificate,
                                    X509Chain chain,
                                    SslPolicyErrors sslPolicyErrors)
            {
                return AcceptServerCertificate(sender, sslPolicyErrors);
            };
        }

        private void OnOK(object sender, EventArgs e)
        {
            // CSP settings
            //
            try
            {
                if (!bReadOnly && csskey != null)
                {
                    string baseURL = tbBaseURL.Text;
                    string certHash = tbCertHash.Text;
                    string oauthURL = tbOauthServerURL.Text;
                    string oauthClientId = tbOAuthClientId.Text;
                    string metaDataFile = tbMetadataFile.Text;

                    UInt32 permitMismatch = (uint)(cbPermitName.Checked ? 1 : 0);
                    UInt32 permitChain = (uint)(cbPermitChain.Checked ? 1 : 0);

                    StringBuilder strHash = new StringBuilder();
                    for (int i = 0; i < certHash.Length; i++)
                        if (certHash[i] != ' ')
                            strHash.Append(certHash[i]);

                    StringBuilder strHash2 = new StringBuilder();

                    csskey.SetValue("ServerBaseURL", baseURL, RegistryValueKind.String);
                    csskey.SetValue("OAuthServerURL", oauthURL, RegistryValueKind.String);
                    csskey.SetValue("OAuthClientId", oauthClientId, RegistryValueKind.String);
                    csskey.SetValue("MetadataFile", metaDataFile, RegistryValueKind.String);
                    csskey.SetValue("PermitServerCertNameMismatch", permitMismatch, RegistryValueKind.DWord);
                    csskey.SetValue("PermitServerCertChainErrors", permitChain, RegistryValueKind.DWord);
                    csskey.SetValue("SSLClientCertHash", strHash.ToString(), RegistryValueKind.String);

                }
            }
            catch (SecurityException)
            {
            }
            Close();
        }

        private void OnCancel(object sender, EventArgs e)
        {
            Close();
        }

        private void OnBrowse(object sender, EventArgs e)
        {
            SystemCerts sysCerts = new SystemCerts(SystemCerts.CERT_SYSTEM_STORE_LOCAL_MACHINE);
            // Use clientAuth EKU OID to show SSL Client Certs
            X509Certificate sslCert = sysCerts.SelectCertDialog("Select an SSL Client Certificate", "1.3.6.1.5.5.7.3.2");
            if( sslCert != null )
            {
                tbCertName.Text = sslCert.Subject.ToString();
                tbCertHash.Text = sslCert.GetCertHashString();
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            UInt32 on = 1;
            UInt32 off = 0;

            // CSP settings
            //
            try
            {
                csskey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\ThalesASG\CSSCSP", true);
                if (csskey == null)
                    csskey = Registry.LocalMachine.CreateSubKey(@"SOFTWARE\ThalesASG\CSSCSP");

            }
            catch (SecurityException)
            {
                textBoxInfo.Text = "You do not have sufficient permissions to modify the CSSCSP registry settings. You can test existing settings with the 'Refresh Signing Certificates' button." ;
            }
            if (csskey == null)
            {
                // Open read-only, disable controls
                bReadOnly = true;
                csskey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\ThalesASG\CSSCSP", false);
                tbBaseURL.ReadOnly = true;
                tbOauthServerURL.ReadOnly = true;
                tbOAuthClientId.ReadOnly = true;
                tbMetadataFile.ReadOnly = true;
                cbPermitName.Enabled = false;
                cbPermitChain.Enabled = false;
                button1.Enabled = false;
                btCertSelect.Enabled = false;
                
            }
            if (csskey != null)
            {
                tbBaseURL.Text = csskey.GetValue("ServerBaseURL", "https://localhost:443").ToString();
                tbCertHash.Text = csskey.GetValue("SSLClientCertHash", "").ToString();
                tbOauthServerURL.Text = csskey.GetValue("OAuthServerURL", "https://oauthserver:443").ToString();
                tbOAuthClientId.Text = csskey.GetValue("OAuthClientId", "client_id").ToString();
                tbMetadataFile.Text = csskey.GetValue("MetadataFile", "metadata.json").ToString();
                
                cbPermitName.Checked = (0 != (UInt32.Parse(csskey.GetValue("PermitServerCertNameMismatch", on).ToString())));
                cbPermitChain.Checked = (0 != (UInt32.Parse(csskey.GetValue("PermitServerCertChainErrors", off).ToString())));
            }
            else
            {
                Close();
            }
            SystemCerts sysCerts = new SystemCerts(SystemCerts.CERT_SYSTEM_STORE_LOCAL_MACHINE);
            X509Certificate sslCert = sysCerts.FindCert(tbCertHash.Text);
            if (sslCert != null)
            {
                tbCertName.Text = sslCert.Subject.ToString();
            }
            else
            {
                tbCertName.Text = tbCertHash.Text;
            }

        }

        private bool validateServerURL(String urlString)
        {
            HttpWebRequest req = null;
            bool bResult = true;
            
            try
            {
                req = (HttpWebRequest)WebRequest.Create(urlString);
                req.Method = "GET";
                req.Credentials = CredentialCache.DefaultCredentials;
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
                                                     | SecurityProtocolType.Ssl3 
                                                     | SecurityProtocolType.Tls11 
                                                     | SecurityProtocolType.Tls12;

                // allows for validation of SSL conversations
                ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
                try
                {
                    HttpWebResponse resp = (HttpWebResponse)req.GetResponse();
                }
                catch (Exception ex)
                {
                    Exception innerEx;

                    innerEx = ex.InnerException;
                    if (innerEx is System.Net.Sockets.SocketException)
                    {
                        bResult = false;
                        MessageBox.Show(String.Format("An exception occurred when connecting to the specified server and port.\r\n\r\n" +
                                                      "   {0}\r\n\r\n" +
                                                      "Exception: " + innerEx.Message + "\r\n\r\n" +
                                                      "Verify that this value matches the server name and port number for the CSS.",
                                                      urlString), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }                    
                }
            }
            catch (Exception ex)
            {
                bResult = false;
                MessageBox.Show(String.Format("The specified server and port is not a valid URL String:\r\n\r\n" +
                                              "   {0}\r\n\r\n" +
                                              "Verify the format and try again.",
                                              urlString), "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return bResult;
        }

        private void refreshingSigningCerts_Click(object sender, EventArgs e)
        {
            // CSP settings
            //
            if (null == Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Cryptography\Defaults\Provider\Thales CSS Cryptographic Provider", false))
            {
                MessageBox.Show("The 'Thales CodeSign Server' CSP is not installed properly.", "Error");
                return;
            }
            try
            {
                if (!bReadOnly && csskey != null)
                {
                    csskey.SetValue("OAuthClientId", tbOAuthClientId.Text, RegistryValueKind.String);
                    csskey.SetValue("MetadataFile", tbMetadataFile.Text, RegistryValueKind.String);                    
                    csskey.SetValue("OAuthServerURL", tbOauthServerURL.Text, RegistryValueKind.String);
                    csskey.SetValue("ServerBaseURL", tbBaseURL.Text, RegistryValueKind.String);
                    csskey.SetValue("PermitServerCertNameMismatch", (uint)(cbPermitName.Checked ? 1 : 0), RegistryValueKind.DWord);
                    csskey.SetValue("PermitServerCertChainErrors", (uint)(cbPermitChain.Checked ? 1 : 0), RegistryValueKind.DWord);
                    csskey.SetValue("SSLClientCertHash", tbCertHash.Text, RegistryValueKind.String);
                }
            }
            catch (SecurityException)
            {
                // Ignore exception writing to registry and try existing settings.
            }

            // make sure the specified URL is valid before attempting to listkeys
            if (validateServerURL(tbBaseURL.Text))
            {
                Cursor = Cursors.WaitCursor;

                if (CAPI.HighLevel.PingProv("Thales CSS Cryptographic Provider", CAPI.PROV_RSA_FULL))
                {
                    Cursor = Cursors.Default;
                    SystemCerts sysCerts = new SystemCerts(SystemCerts.CERT_SYSTEM_STORE_CURRENT_USER);
                    // Use codeSigning EKU OID
                    sysCerts.SelectCertDialog("The CSS Signing Certificate list was updated.", "1.3.6.1.5.5.7.3.3");
                }
                else
                {
                    Cursor = Cursors.Default;
                    String msg;

                    msg = String.Format("Unable to retrieve certificate list.\r\n\r\n");
                    // display a different error message if http was specifed vs https
                    HttpWebRequest req = (HttpWebRequest)WebRequest.Create(tbBaseURL.Text);
                    if (req.RequestUri.Scheme != "https")
                    {
                        msg += "HTTP was specified in the URI, make sure that HTTPS is not required.\r\n\r\n";
                        msg += "Also, \r\n";
                    }

                    msg += "1. Verify that the 'ThalesCodesign' service is running.\r\n" +
                           "2. Consult the Windows Application Event Log for entries from 'ThalesCodesign'.";
                    
                    MessageBox.Show(msg, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void FileBrowse_Click(object sender, EventArgs e)
        {
            openFileDialog1.FileName = tbMetadataFile.Text;
            openFileDialog1.DefaultExt = "json";
            openFileDialog1.CheckFileExists = false;
            openFileDialog1.Filter = "Json files (*.json)|*.json|All files (*.*)|*.*";
            DialogResult result = openFileDialog1.ShowDialog();
            // OK button was pressed.
            if (result == DialogResult.OK)
            {
                tbMetadataFile.Text = openFileDialog1.FileName;

            }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }
    }
}
