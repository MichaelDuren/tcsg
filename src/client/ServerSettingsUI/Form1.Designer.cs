﻿namespace ServerSettingsUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.label1 = new System.Windows.Forms.Label();
            this.tbBaseURL = new System.Windows.Forms.TextBox();
            this.lSSLClientCertificate = new System.Windows.Forms.Label();
            this.tbCertName = new System.Windows.Forms.TextBox();
            this.cbPermitName = new System.Windows.Forms.CheckBox();
            this.cbPermitChain = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btCertSelect = new System.Windows.Forms.Button();
            this.refreshingSigningCerts = new System.Windows.Forms.Button();
            this.tbCertHash = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxInfo = new System.Windows.Forms.TextBox();
            this.tbOauthServerURL = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(28, 79);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(181, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "CodeSign Portal base URL:";
            // 
            // tbBaseURL
            // 
            this.tbBaseURL.Location = new System.Drawing.Point(225, 75);
            this.tbBaseURL.Margin = new System.Windows.Forms.Padding(4);
            this.tbBaseURL.Name = "tbBaseURL";
            this.tbBaseURL.Size = new System.Drawing.Size(385, 22);
            this.tbBaseURL.TabIndex = 1;
            // 
            // lSSLClientCertificate
            // 
            this.lSSLClientCertificate.AutoSize = true;
            this.lSSLClientCertificate.Location = new System.Drawing.Point(28, 143);
            this.lSSLClientCertificate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lSSLClientCertificate.Name = "lSSLClientCertificate";
            this.lSSLClientCertificate.Size = new System.Drawing.Size(140, 17);
            this.lSSLClientCertificate.TabIndex = 2;
            this.lSSLClientCertificate.Text = "SSL client certificate:";
            // 
            // tbCertName
            // 
            this.tbCertName.Location = new System.Drawing.Point(225, 139);
            this.tbCertName.Margin = new System.Windows.Forms.Padding(4);
            this.tbCertName.Multiline = true;
            this.tbCertName.Name = "tbCertName";
            this.tbCertName.ReadOnly = true;
            this.tbCertName.Size = new System.Drawing.Size(337, 24);
            this.tbCertName.TabIndex = 3;
            // 
            // cbPermitName
            // 
            this.cbPermitName.AutoSize = true;
            this.cbPermitName.Location = new System.Drawing.Point(119, 202);
            this.cbPermitName.Margin = new System.Windows.Forms.Padding(4);
            this.cbPermitName.Name = "cbPermitName";
            this.cbPermitName.Size = new System.Drawing.Size(353, 21);
            this.cbPermitName.TabIndex = 4;
            this.cbPermitName.Text = "Permit SSL server certificate name mismatch errors";
            this.cbPermitName.UseVisualStyleBackColor = true;
            // 
            // cbPermitChain
            // 
            this.cbPermitChain.AutoSize = true;
            this.cbPermitChain.Location = new System.Drawing.Point(119, 238);
            this.cbPermitChain.Margin = new System.Windows.Forms.Padding(4);
            this.cbPermitChain.Name = "cbPermitChain";
            this.cbPermitChain.Size = new System.Drawing.Size(289, 21);
            this.cbPermitChain.TabIndex = 5;
            this.cbPermitChain.Text = "Permit SSL server certificate chain errors";
            this.cbPermitChain.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button1.Location = new System.Drawing.Point(367, 318);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(100, 28);
            this.button1.TabIndex = 6;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.OnOK);
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(496, 318);
            this.button2.Margin = new System.Windows.Forms.Padding(4);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(100, 28);
            this.button2.TabIndex = 7;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.OnCancel);
            // 
            // btCertSelect
            // 
            this.btCertSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCertSelect.Location = new System.Drawing.Point(572, 137);
            this.btCertSelect.Margin = new System.Windows.Forms.Padding(4);
            this.btCertSelect.Name = "btCertSelect";
            this.btCertSelect.Size = new System.Drawing.Size(40, 28);
            this.btCertSelect.TabIndex = 8;
            this.btCertSelect.Text = "...";
            this.btCertSelect.UseVisualStyleBackColor = true;
            this.btCertSelect.Click += new System.EventHandler(this.OnBrowse);
            // 
            // refreshingSigningCerts
            // 
            this.refreshingSigningCerts.Location = new System.Drawing.Point(40, 318);
            this.refreshingSigningCerts.Margin = new System.Windows.Forms.Padding(4);
            this.refreshingSigningCerts.Name = "refreshingSigningCerts";
            this.refreshingSigningCerts.Size = new System.Drawing.Size(201, 28);
            this.refreshingSigningCerts.TabIndex = 9;
            this.refreshingSigningCerts.Text = "Refresh Signing Certificates";
            this.refreshingSigningCerts.UseVisualStyleBackColor = true;
            this.refreshingSigningCerts.Click += new System.EventHandler(this.refreshingSigningCerts_Click);
            // 
            // tbCertHash
            // 
            this.tbCertHash.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.tbCertHash.Location = new System.Drawing.Point(225, 166);
            this.tbCertHash.Margin = new System.Windows.Forms.Padding(4);
            this.tbCertHash.Name = "tbCertHash";
            this.tbCertHash.Size = new System.Drawing.Size(337, 22);
            this.tbCertHash.TabIndex = 11;
            this.tbCertHash.Text = "[hidden certhash]";
            this.tbCertHash.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(28, 30);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(328, 17);
            this.label3.TabIndex = 12;
            this.label3.Text = "Thales ASG CodeSign Portal - CSP Client Settings:";
            // 
            // textBoxInfo
            // 
            this.textBoxInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxInfo.Location = new System.Drawing.Point(56, 279);
            this.textBoxInfo.Margin = new System.Windows.Forms.Padding(4);
            this.textBoxInfo.Multiline = true;
            this.textBoxInfo.Name = "textBoxInfo";
            this.textBoxInfo.ReadOnly = true;
            this.textBoxInfo.Size = new System.Drawing.Size(556, 31);
            this.textBoxInfo.TabIndex = 13;
            this.textBoxInfo.Text = "Change settings then press Refresh Signing Certificates to test the server connec" +
    "tion.";
            // 
            // tbOauthServerURL
            // 
            this.tbOauthServerURL.Location = new System.Drawing.Point(225, 106);
            this.tbOauthServerURL.Margin = new System.Windows.Forms.Padding(4);
            this.tbOauthServerURL.Name = "tbOauthServerURL";
            this.tbOauthServerURL.Size = new System.Drawing.Size(385, 22);
            this.tbOauthServerURL.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(28, 110);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(130, 17);
            this.label4.TabIndex = 14;
            this.label4.Text = "OAuth Server URL:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(649, 358);
            this.Controls.Add(this.tbOauthServerURL);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxInfo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbCertHash);
            this.Controls.Add(this.refreshingSigningCerts);
            this.Controls.Add(this.btCertSelect);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cbPermitChain);
            this.Controls.Add(this.cbPermitName);
            this.Controls.Add(this.tbCertName);
            this.Controls.Add(this.lSSLClientCertificate);
            this.Controls.Add(this.tbBaseURL);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thales ASG CSSCSP Configuration";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbBaseURL;
        private System.Windows.Forms.Label lSSLClientCertificate;
        private System.Windows.Forms.TextBox tbCertName;
        private System.Windows.Forms.CheckBox cbPermitName;
        private System.Windows.Forms.CheckBox cbPermitChain;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btCertSelect;
        private System.Windows.Forms.Button refreshingSigningCerts;
        private System.Windows.Forms.TextBox tbCertHash;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxInfo;
        private System.Windows.Forms.TextBox tbOauthServerURL;
        private System.Windows.Forms.Label label4;
    }
}

