﻿namespace ServerSettingsUI
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.label1 = new System.Windows.Forms.Label();
            this.tbBaseURL = new System.Windows.Forms.TextBox();
            this.lSSLClientCertificate = new System.Windows.Forms.Label();
            this.tbCertName = new System.Windows.Forms.TextBox();
            this.cbPermitName = new System.Windows.Forms.CheckBox();
            this.cbPermitChain = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btCertSelect = new System.Windows.Forms.Button();
            this.refreshingSigningCerts = new System.Windows.Forms.Button();
            this.tbCertHash = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.textBoxInfo = new System.Windows.Forms.TextBox();
            this.tbOauthServerURL = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.tbOAuthClientId = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tbMetadataFile = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.FileBrowse = new System.Windows.Forms.Button();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(21, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "CodeSign Portal base URL:";
            // 
            // tbBaseURL
            // 
            this.tbBaseURL.Location = new System.Drawing.Point(169, 61);
            this.tbBaseURL.Name = "tbBaseURL";
            this.tbBaseURL.Size = new System.Drawing.Size(290, 20);
            this.tbBaseURL.TabIndex = 1;
            // 
            // lSSLClientCertificate
            // 
            this.lSSLClientCertificate.AutoSize = true;
            this.lSSLClientCertificate.Location = new System.Drawing.Point(21, 149);
            this.lSSLClientCertificate.Name = "lSSLClientCertificate";
            this.lSSLClientCertificate.Size = new System.Drawing.Size(107, 13);
            this.lSSLClientCertificate.TabIndex = 2;
            this.lSSLClientCertificate.Text = "SSL client certificate:";
            // 
            // tbCertName
            // 
            this.tbCertName.Location = new System.Drawing.Point(169, 142);
            this.tbCertName.Multiline = true;
            this.tbCertName.Name = "tbCertName";
            this.tbCertName.ReadOnly = true;
            this.tbCertName.Size = new System.Drawing.Size(254, 20);
            this.tbCertName.TabIndex = 3;
            // 
            // cbPermitName
            // 
            this.cbPermitName.AutoSize = true;
            this.cbPermitName.Location = new System.Drawing.Point(89, 245);
            this.cbPermitName.Name = "cbPermitName";
            this.cbPermitName.Size = new System.Drawing.Size(264, 17);
            this.cbPermitName.TabIndex = 4;
            this.cbPermitName.Text = "Permit SSL server certificate name mismatch errors";
            this.cbPermitName.UseVisualStyleBackColor = true;
            // 
            // cbPermitChain
            // 
            this.cbPermitChain.AutoSize = true;
            this.cbPermitChain.Location = new System.Drawing.Point(89, 274);
            this.cbPermitChain.Name = "cbPermitChain";
            this.cbPermitChain.Size = new System.Drawing.Size(217, 17);
            this.cbPermitChain.TabIndex = 5;
            this.cbPermitChain.Text = "Permit SSL server certificate chain errors";
            this.cbPermitChain.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button1.Location = new System.Drawing.Point(275, 339);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.OnOK);
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(372, 339);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 7;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.OnCancel);
            // 
            // btCertSelect
            // 
            this.btCertSelect.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btCertSelect.Location = new System.Drawing.Point(429, 141);
            this.btCertSelect.Name = "btCertSelect";
            this.btCertSelect.Size = new System.Drawing.Size(30, 23);
            this.btCertSelect.TabIndex = 8;
            this.btCertSelect.Text = "...";
            this.btCertSelect.UseVisualStyleBackColor = true;
            this.btCertSelect.Click += new System.EventHandler(this.OnBrowse);
            // 
            // refreshingSigningCerts
            // 
            this.refreshingSigningCerts.Location = new System.Drawing.Point(30, 339);
            this.refreshingSigningCerts.Name = "refreshingSigningCerts";
            this.refreshingSigningCerts.Size = new System.Drawing.Size(151, 23);
            this.refreshingSigningCerts.TabIndex = 9;
            this.refreshingSigningCerts.Text = "Refresh Signing Certificates";
            this.refreshingSigningCerts.UseVisualStyleBackColor = true;
            this.refreshingSigningCerts.Click += new System.EventHandler(this.refreshingSigningCerts_Click);
            // 
            // tbCertHash
            // 
            this.tbCertHash.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.tbCertHash.Location = new System.Drawing.Point(169, 216);
            this.tbCertHash.Name = "tbCertHash";
            this.tbCertHash.Size = new System.Drawing.Size(254, 20);
            this.tbCertHash.TabIndex = 11;
            this.tbCertHash.Text = "[hidden certhash]";
            this.tbCertHash.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(21, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(246, 13);
            this.label3.TabIndex = 12;
            this.label3.Text = "Thales ASG CodeSign Portal - CSP Client Settings:";
            // 
            // textBoxInfo
            // 
            this.textBoxInfo.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBoxInfo.Location = new System.Drawing.Point(42, 308);
            this.textBoxInfo.Multiline = true;
            this.textBoxInfo.Name = "textBoxInfo";
            this.textBoxInfo.ReadOnly = true;
            this.textBoxInfo.Size = new System.Drawing.Size(417, 25);
            this.textBoxInfo.TabIndex = 13;
            this.textBoxInfo.Text = "Change settings then press Refresh Signing Certificates to test the server connec" +
    "tion.";
            // 
            // tbOauthServerURL
            // 
            this.tbOauthServerURL.Location = new System.Drawing.Point(169, 88);
            this.tbOauthServerURL.Name = "tbOauthServerURL";
            this.tbOauthServerURL.Size = new System.Drawing.Size(290, 20);
            this.tbOauthServerURL.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(21, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(99, 13);
            this.label4.TabIndex = 14;
            this.label4.Text = "OAuth Server URL:";
            // 
            // tbOAuthClientId
            // 
            this.tbOAuthClientId.Location = new System.Drawing.Point(169, 115);
            this.tbOAuthClientId.Name = "tbOAuthClientId";
            this.tbOAuthClientId.Size = new System.Drawing.Size(290, 20);
            this.tbOAuthClientId.TabIndex = 17;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(21, 122);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 13);
            this.label2.TabIndex = 16;
            this.label2.Text = "OAuth Cilent ID:";
            // 
            // tbMetadataFile
            // 
            this.tbMetadataFile.Location = new System.Drawing.Point(168, 169);
            this.tbMetadataFile.Name = "tbMetadataFile";
            this.tbMetadataFile.Size = new System.Drawing.Size(255, 20);
            this.tbMetadataFile.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 176);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(74, 13);
            this.label5.TabIndex = 18;
            this.label5.Text = "Metadata File:";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // FileBrowse
            // 
            this.FileBrowse.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FileBrowse.Location = new System.Drawing.Point(429, 167);
            this.FileBrowse.Name = "FileBrowse";
            this.FileBrowse.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.FileBrowse.Size = new System.Drawing.Size(30, 23);
            this.FileBrowse.TabIndex = 20;
            this.FileBrowse.Text = "...";
            this.FileBrowse.UseVisualStyleBackColor = true;
            this.FileBrowse.Click += new System.EventHandler(this.FileBrowse_Click);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialog1_FileOk);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(487, 390);
            this.Controls.Add(this.FileBrowse);
            this.Controls.Add(this.tbMetadataFile);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.tbOAuthClientId);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.tbOauthServerURL);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBoxInfo);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.tbCertHash);
            this.Controls.Add(this.refreshingSigningCerts);
            this.Controls.Add(this.btCertSelect);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cbPermitChain);
            this.Controls.Add(this.cbPermitName);
            this.Controls.Add(this.tbCertName);
            this.Controls.Add(this.lSSLClientCertificate);
            this.Controls.Add(this.tbBaseURL);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Thales ASG CSSCSP Configuration";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox tbBaseURL;
        private System.Windows.Forms.Label lSSLClientCertificate;
        private System.Windows.Forms.TextBox tbCertName;
        private System.Windows.Forms.CheckBox cbPermitName;
        private System.Windows.Forms.CheckBox cbPermitChain;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btCertSelect;
        private System.Windows.Forms.Button refreshingSigningCerts;
        private System.Windows.Forms.TextBox tbCertHash;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBoxInfo;
        private System.Windows.Forms.TextBox tbOauthServerURL;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox tbOAuthClientId;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tbMetadataFile;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button FileBrowse;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}

