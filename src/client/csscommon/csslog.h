#pragma once

#include <Windows.h>

#ifdef __cplusplus
extern "C" {
#endif

void DebugOut(LPCSTR format, ...);
void LogError(LPCSTR format, ...);
void InitGlobals();

#ifdef __cplusplus
}
#endif
