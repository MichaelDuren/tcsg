
#define UNICODE 1

#include "csslog.h"
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

// Globals
BOOL g_debug = FALSE;

wchar_t g_szLogDir[MAX_PATH];
// g_szLogDir[MAX_PATH];

void WriteLog(LPCSTR szBuf, ...)
{
    time_t rawtime;
    struct tm lt;
	wchar_t szPath[MAX_PATH];
	HANDLE hFile;
	DWORD cbWritten;

	time( &rawtime );
    localtime_s( &lt, &rawtime );

	swprintf_s(szPath, MAX_PATH, L"%s\\csscsp-%04d-%02d-%02d.log", g_szLogDir, lt.tm_year+1900, lt.tm_mon+1, lt.tm_mday);

	hFile = CreateFile( szPath, FILE_APPEND_DATA, FILE_SHARE_READ, NULL, 
						OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL );
	if (hFile != INVALID_HANDLE_VALUE) 
	{ 
		SetFilePointer(hFile, 0, NULL, FILE_END);
		WriteFile(hFile, szBuf, (DWORD)strlen(szBuf), &cbWritten, NULL); 
		CloseHandle(hFile);
	}
}

void DebugOut(LPCSTR format, ...)
{
	if( g_debug )
	{
		CHAR szBuf[256];
		CHAR szFormat[256];
		time_t rawtime;
		struct tm lt;
		va_list l_Arg;
		va_start(l_Arg, format);

		time( &rawtime );
		localtime_s( &lt, &rawtime );

		sprintf_s(szFormat, sizeof(szFormat), "csscsp %04d-%02d-%02d %02d:%02d:%02d - %s\r\n", lt.tm_year+1900, lt.tm_mon+1, 
			lt.tm_mday, lt.tm_hour, lt.tm_min, lt.tm_sec, format);

		vsprintf_s(szBuf, sizeof(szBuf), szFormat, l_Arg);
		OutputDebugStringA(szBuf);
		WriteLog(szBuf);
		printf(szBuf);
	}
}

LPSTR GetLastErrorText() 
{ 
    // Retrieve the system error message for the last-error code

    LPSTR lpMsgBuf;
	DWORD ret;
    DWORD dw = GetLastError(); 
	
    ret = FormatMessageA(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | 
        FORMAT_MESSAGE_FROM_SYSTEM |
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        dw,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPSTR) &lpMsgBuf,
        0, NULL );
	if ( ret == 0 )
		lpMsgBuf = LocalAlloc(LPTR, 32);
	return lpMsgBuf;
}

void LogError(LPCSTR format, ...)
{
	CHAR szBuf[256];
    CHAR szFormat[256];
	CHAR *psz;
	LPSTR pszLastError = NULL;
    time_t rawtime;
    struct tm lt;
	va_list l_Arg;
	va_start(l_Arg, format);

	time( &rawtime );
    localtime_s( &lt, &rawtime );

	if( GetLastError() )
	{
		pszLastError = GetLastErrorText();

		psz = pszLastError + strlen(pszLastError) - 1;
		while( *psz == '\n' || *psz == '\r' )
			--psz;
		*psz = 0;
		sprintf_s(szFormat, sizeof(szFormat), "csscsp %04d-%02d-%02d %02d:%02d:%02d ERROR (0x%x:%s) %s\r\n", lt.tm_year+1900, lt.tm_mon+1, 
			lt.tm_mday, lt.tm_hour, lt.tm_min, lt.tm_sec, GetLastError(), pszLastError, format);
		LocalFree(pszLastError);
	}
	else
	{
		sprintf_s(szFormat, sizeof(szFormat), "csscsp %04d-%02d-%02d %02d:%02d:%02d ERROR: %s\r\n", lt.tm_year+1900, lt.tm_mon+1, 
			lt.tm_mday, lt.tm_hour, lt.tm_min, lt.tm_sec, format);
	}
	vsprintf_s(szBuf, sizeof(szBuf), szFormat, l_Arg);
	WriteLog(szBuf);
	if( g_debug )
	{
		OutputDebugStringA(szBuf);
		printf(szBuf);
	}
}

void InitGlobals()
{
	char *pEnvVar = NULL;
	DWORD attribs;
    char szTempVal[MAX_PATH];
    char *szTempEnv = NULL;
    size_t len;
    size_t convertedChars = 0;

#ifdef _DEBUG
	g_debug = TRUE;
#else
    _dupenv_s(&szTempEnv, &len, "CSSCSP_DEBUG");
    if (szTempEnv) 
    {
    	g_debug = *szTempEnv == '0' ? FALSE : TRUE;
        free(szTempEnv);
    }
#endif
	*g_szLogDir = 0;

    szTempEnv = NULL;
    _dupenv_s(&szTempEnv, &len, "CSSCSP_LOGDIR");

    if( szTempEnv == NULL) 
    {
        szTempEnv = NULL;
        _dupenv_s(&szTempEnv, &len, "ProgramData");
    }

    if( szTempEnv == NULL) 
    {
        szTempEnv = NULL;
        _dupenv_s(&szTempEnv, &len, "ALLUSERSPROFILE");
    }

    if( szTempEnv != NULL) 
    {
        strcpy_s(szTempVal, sizeof(szTempVal), szTempEnv);
		strcat_s(szTempVal, sizeof(szTempVal), "\\Application Data");
        free(szTempEnv);
    }

	strcat_s(szTempVal, sizeof(szTempVal), "\\ThalesCSSCSP");
	attribs = GetFileAttributesA(szTempVal);
	if (attribs == INVALID_FILE_ATTRIBUTES)
    {
		CreateDirectoryA(szTempVal, NULL);
    }

    // copy the folder into the wstr global variable
    mbstowcs_s(&convertedChars, g_szLogDir, strlen(szTempVal) + 1, szTempVal, _TRUNCATE);

}
