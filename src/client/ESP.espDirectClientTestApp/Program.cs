﻿using ESP.espDirectClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ESP.espDirectClientTestApp
{
    class Program
    {
        static void Main(string[] args)
        {
            var clientLib = new espDirectClient.espDirectClientLib();

            clientLib.GetBearerToken();
            ServerManager serverManager = new ServerManager();

            serverManager.startPipeServer();

        }
    }
}
