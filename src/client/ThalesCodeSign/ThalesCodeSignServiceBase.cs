﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Threading;
using System.Text;
using ESP.espDirectClient;

namespace ThalesCSS
{
    public class ThalesCodeSignServiceBase : System.ServiceProcess.ServiceBase
    {
        public const int SERVER_INSTANCES = 20;
        protected Thread m_thread;
        protected ManualResetEvent m_shutdownEvent;
        protected TimeSpan m_delay;


        public ThalesCodeSignServiceBase()
        {
        }

        protected override void OnStart(string[] args)
        {
            // create our threadstart object to wrap our delegate method
            ThreadStart ts = new ThreadStart(this.ServiceMain);

            // create the manual reset event and set it to an initial state of unsignaled
            m_shutdownEvent = new ManualResetEvent(false);

            // create the worker thread
            m_thread = new Thread(ServiceMain);
            m_thread.Start();

            // call the base class so it has a chance to perform any work it needs to
            base.OnStart(args);
        }

        protected override void OnStop()
        {
        }

        public static void StartPipeServer()
        {
            SignaturePipeServer sig = new SignaturePipeServer();
            ListKeysPipeServer keys = new ListKeysPipeServer();
            SignaturePipeServer sig2 = new SignaturePipeServer(true);
            ListKeysPipeServer keys2 = new ListKeysPipeServer(true);
            sig.Run();
            keys.Run();
            sig2.Run();
            keys2.Run();
            //will return when client disconnects.
        }

        protected void ServiceMain()
        {
            ServerManager serverManager = new ServerManager();

            serverManager.startPipeServer();
        }

    }
}
