﻿using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using Microsoft.Win32;
using System.ServiceProcess;
using System.Security.AccessControl;
using System.Runtime.InteropServices;

namespace ThalesCSS
{
    public class x64Info
    {
        static bool Is64BitProcess()
        {
            return (IntPtr.Size == 8);
        }

        static bool Is64BitOS()
        {
            if (IntPtr.Size == 8)  // 64-bit programs run only on Win64 
            {
                return true;
            }
            else  // 32-bit programs run on both 32-bit and 64-bit Windows 
            {
                // Detect whether the current process is a 32-bit process  
                // running on a 64-bit system. 
                bool flag;
                return (DoesWin32MethodExist("kernel32.dll", "IsWow64Process")
                    && (NativeMethods.IsWow64Process(NativeMethods.GetCurrentProcess(), out flag)
                    && flag));
            }
        }

        static bool DoesWin32MethodExist(string moduleName, string methodName)
        {
            IntPtr moduleHandle = NativeMethods.GetModuleHandle(moduleName);
            if (moduleHandle == IntPtr.Zero)
            {
                return false;
            }
            return (NativeMethods.GetProcAddress(moduleHandle, methodName) != IntPtr.Zero);
        } 

    }
  
    class NativeMethods 
    { 
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)] 
        public static extern IntPtr GetCurrentProcess(); 
 
        public const Int32 PROCESS_QUERY_INFORMATION = 0x0400; 
 
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)] 
        public static extern IntPtr OpenProcess(int dwDesiredAccess, 
            [MarshalAs(UnmanagedType.Bool)]bool bInheritHandle, 
            int dwProcessId); 
 
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)] 
        [return: MarshalAs(UnmanagedType.Bool)] 
        public static extern bool CloseHandle(IntPtr hObject); 
 
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)] 
        public static extern IntPtr GetModuleHandle(string moduleName); 
 
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)] 
        public static extern IntPtr GetProcAddress(IntPtr hModule, 
            [MarshalAs(UnmanagedType.LPStr)]string procName); 
 
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)] 
        [return: MarshalAs(UnmanagedType.Bool)] 
        public static extern bool IsWow64Process(IntPtr hProcess, out bool wow64Process); 
    } 
}