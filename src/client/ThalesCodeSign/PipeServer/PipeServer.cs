﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.IO;
using System.IO.Pipes;
using System.ComponentModel;
using System.Threading;
using ThalesCSS.Interfaces;
using ThalesCSS.Utilities;
using Microsoft.Win32;

namespace ThalesCSS.Server
{
    public class PipeServer : ICommunicationServer
    {
        #region private fields

        protected string _pipeName;
        protected bool _secondaryFlag = false;
        private readonly SynchronizationContext _synchronizationContext;
        private readonly IDictionary<string, ICommunicationServer> _servers; // ConcurrentDictionary is thread safe
        private const int _maxNumberOfServerInstances = -1;

        #endregion

        #region c'tor

        public PipeServer()
        {
       //     _pipeName = Guid.NewGuid().ToString();
            _synchronizationContext = AsyncOperationManager.SynchronizationContext;
            _servers = new ConcurrentDictionary<string, ICommunicationServer>();
            _pipeName = "ThalesCSSSign";

        }

        #endregion

        #region events

        public event EventHandler<MessageReceivedEventArgs> MessageReceivedEvent;
        public event EventHandler<ClientConnectedEventArgs> ClientConnectedEvent;
        public event EventHandler<ClientDisconnectedEventArgs> ClientDisconnectedEvent;

        #endregion

        #region ICommunicationServer implementation

        public string ServerId
        {
            get { return _pipeName; }
        }

        public void Start()
        {
            StartNamedPipeServer();
        }

        public void StartUp()
        {
            StartFirstNamedPipeServer();
        }
       
        public void Stop()
        {
            foreach (var server in _servers.Values)
            {
                try
                {
                    UnregisterFromServerEvents(server);
                    server.Stop();
                }
                catch (Exception)
                {
                    Logger.Error("Fialed to stop server");
                }
            }

            _servers.Clear();
        }

        #endregion

        #region private methods

        /// <summary>
        /// Starts a new NamedPipeServerStream that waits for connection
        /// </summary>
        private void StartFirstNamedPipeServer()
        {
            int i;

            for (i = 0; i < 30; ++i)
            {
                try
                {
                    var server = new InternalPipeServer(_pipeName, _maxNumberOfServerInstances);
                    _servers[server.Id] = server;

                    // register handlers from connect, disconnect, and message received
                    server.ClientConnectedEvent += ClientConnectedHandler;
                    server.ClientDisconnectedEvent += ClientDisconnectedHandler;
                    server.MessageReceivedEvent += MessageReceivedHandler;

                    // start this server
                    server.Start();
                }
                catch (IOException ex)
                {
                    // max number of server instances reached.  ignore.
                }
            }
        }
        
        /// <summary>
        /// Starts a new NamedPipeServerStream that waits for connection
        /// </summary>
        private void StartNamedPipeServer()
        {
            try 
            {
                var server = new InternalPipeServer(_pipeName, _maxNumberOfServerInstances);
                _servers[server.Id] = server;

                // register handlers from connect, disconnect, and message received
                server.ClientConnectedEvent += ClientConnectedHandler;
                server.ClientDisconnectedEvent += ClientDisconnectedHandler;
                server.MessageReceivedEvent += MessageReceivedHandler;

                // start this server
                server.Start();
            }
            catch (IOException ex)
            {
                // max number of server instances reached.  ignore.
            }
        }

        /// <summary>
        /// Stops the server that belongs to the given id
        /// </summary>
        /// <param name="id"></param>
        private void StopNamedPipeServer(string id)
        {
            UnregisterFromServerEvents(_servers[id]);
            _servers[id].Stop();
            _servers.Remove(id);
        }

        /// <summary>
        /// Unregisters from the given server's events
        /// </summary>
        /// <param name="server"></param>
        private void UnregisterFromServerEvents(ICommunicationServer server)
        {
            server.ClientConnectedEvent -= ClientConnectedHandler;
            server.ClientDisconnectedEvent -= ClientDisconnectedHandler;
            server.MessageReceivedEvent -= MessageReceivedHandler;
        }

        /// <summary>
        /// Fires MessageReceivedEvent in the current thread
        /// </summary>
        /// <param name="eventArgs"></param>
        private void OnMessageReceived(MessageReceivedEventArgs eventArgs)
        {
            _synchronizationContext.Post(e => MessageReceivedEvent.SafeInvoke(this, (MessageReceivedEventArgs) e),
                eventArgs);

            ICommunicationServer server = _servers[eventArgs.ClientId];

            if (eventArgs.Message.Length < 10)
            {
                // this is the length, just ignore it
            }
            else
            {
                ProcessRequest(eventArgs.Message, (InternalPipeServer)server);
                StopNamedPipeServer(eventArgs.ClientId);
                GC.Collect();
            }

        }

        bool secondaryFlag = false;

        public string GetURL()
        {
            string url = "http://error-missingregkey";
            try
            {
                using (RegistryKey regKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\ThalesASG\CSSCSP"))
                {
                    object value = regKey.GetValue(secondaryFlag ? "ServerBaseURL2" : "ServerBaseURL");

                    if (value == null)
                    {
                        throw new Exception(secondaryFlag ? @"Registry key: SOFTWARE\ThalesASG\CSSCSP\ServerBaseURL2 not found" : @"Registry key: SOFTWARE\ThalesASG\CSSCSP\ServerBaseURL not found");
                    }
                    url = value.ToString();
                }
            }
            catch (Exception ex)
            {
             //   LogError("GetURL", ex);
            }
            return url;
        }

        public void ParseHeaders(string data, out StringDictionary headers)
        {
            string name = null;
            string value = null;
            headers = new StringDictionary();
            try
            {
                int pos;
                int eolPos;
                while (data.Length > 0)
                {
                    pos = data.IndexOf(":");

                    if (pos >= 0)
                    {
                        // start with the value name
                        name = data.Substring(0, pos);
                        data = data.Substring(pos + 2);

                        // look for the double \r\n
                        eolPos = data.IndexOf("\r\n\r\n");
                        value = data.Substring(0, eolPos);

                        value = value.Replace("\r\n", "");
                        headers.Add(name, value);

                        // move to the end of the value
                        data = data.Substring(eolPos + 4);
                    }
                    else
                    {
                        // bad format
                        data = "";
                    }

                }
            }
            catch (Exception ex)
            {
           //     LogError("ParseHeaders", ex);
            }
        }

        // Override to handle data sent to and from the pipe
        private  void ProcessRequest(String req, InternalPipeServer ps)
        {
            StringDictionary headers;
            StringDictionary response;

//            DebugOut("Sig Server:", PipeName);
            HttpsSession session = new HttpsSession(GetURL(), secondaryFlag);
            string strResp = null;

            try
            {
                ParseHeaders(req, out headers);

                if (session.DoHttpGet("/sign", headers, out response))
                {
                    strResp = response[HttpsSession.CS_SIG];
                    if (strResp == null)
                    {
                        string strError = response[HttpsSession.CS_ERROR];
                        if (strError != null)
                        {
                            strResp = String.Format("{0}: {1}\r\n", HttpsSession.CS_ERROR, strError);
                        }
                    }
                    response.Clear();
                    response = null;
                }
                else
                {
//                    DebugOut(String.Format("DoHttpGet /sign returns {0} values.", response.Count));
                }
                
                // Write the response as the number of bytes remaining
                // followed by the data.  All text.
                if (strResp != null)
                {
                    ps.Write(strResp);
                }
                else
                {
                    ps.Write("");
                }
                
            }
            catch (Exception e)
            {
//                LogError("SignaturePipeServer.ProcessRequest", e);
                string msg = String.Format("{0}: {1}\r\n", HttpsSession.CS_ERROR, e.Message);
                ps.Write(msg);
           }
            headers = null;
            response = null;
        }


        /// <summary>
        /// Fires ClientConnectedEvent in the current thread
        /// </summary>
        /// <param name="eventArgs"></param>
        private void OnClientConnected(ClientConnectedEventArgs eventArgs)
        {
            _synchronizationContext.Post(e => ClientConnectedEvent.SafeInvoke(this, (ClientConnectedEventArgs) e),
                eventArgs);
        }

        /// <summary>
        /// Fires ClientDisconnectedEvent in the current thread
        /// </summary>
        /// <param name="eventArgs"></param>
        private void OnClientDisconnected(ClientDisconnectedEventArgs eventArgs)
        {
            _synchronizationContext.Post(
                e => ClientDisconnectedEvent.SafeInvoke(this, (ClientDisconnectedEventArgs) e), eventArgs);
        }

        /// <summary>
        /// Handles a client connection. Fires the relevant event and prepares for new connection.
        /// </summary>
        private void ClientConnectedHandler(object sender, ClientConnectedEventArgs eventArgs)
        {
            OnClientConnected(eventArgs);

            StartNamedPipeServer(); // Create a additional server as a preparation for new connection
        }

        /// <summary>
        /// Hanldes a client disconnection. Fires the relevant event ans removes its server from the pool
        /// </summary>
        private void ClientDisconnectedHandler(object sender, ClientDisconnectedEventArgs eventArgs)
        {
            OnClientDisconnected(eventArgs);

            StopNamedPipeServer(eventArgs.ClientId);
        }

        /// <summary>
        /// Handles a message that is received from the client. Fires the relevant event.
        /// </summary>
        private void MessageReceivedHandler(object sender, MessageReceivedEventArgs eventArgs)
        {
            OnMessageReceived(eventArgs);
        }

        #endregion
    }

}


