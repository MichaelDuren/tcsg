﻿using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.Diagnostics;
using Microsoft.Win32;
using System.IO;
using ESP.espDirectClient.APITypes;
using System.Security.Cryptography.X509Certificates;
using Newtonsoft.Json;
using static System.Environment;

[assembly: log4net.Config.XmlConfigurator(Watch = true)]

namespace ESP.espDirectClient
{
    public class espDirectUtils
    {
        const int MAX_BUFLEN = 100000;
        static bool _debug = true;
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public static void LogError(string context, Exception e)
        {
            log.ErrorFormat("{0}, {1}", context, e.Message);
            Console.WriteLine("{0}, {1}", context, e.Message);
        }

        public static void LogError(string message)
        {
            log.Error(message);
        }

        public static void LogInfo(string message)
        {
            if (_debug)
            {
                log.Info(message);
            }
        }

        public static void LogDebug(string message)
        {
            if (_debug)
            {
                log.Debug(message);
            }
        }

        public static void LogDebug(string context, string message)
        {
            string logMessage = String.Format("{0}: {1}", context, message);
            LogDebug(logMessage);
        }
    }
}
