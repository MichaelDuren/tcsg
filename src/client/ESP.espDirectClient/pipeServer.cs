﻿using System;
using System.Collections.Generic;
using System.IO.Pipes;
using System.Linq;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Specialized;
using System.Diagnostics;
using Microsoft.Win32;
using System.IO;
using ESP.espDirectClient.APITypes;
using System.Security.Cryptography.X509Certificates;
using Newtonsoft.Json;

namespace ESP.espDirectClient
{
    public class pipeServer
    {
        const int MAX_BUFLEN = 100000;

        bool running;
        Thread runningThread;
        EventWaitHandle terminateHandle = new EventWaitHandle(false, EventResetMode.AutoReset);
        protected string PipeName;
        protected bool secondaryFlag = false;
        PipeSecurity _ps;
        SecurityIdentifier _sid;
        PipeAccessRule _pr;

        ///********************************************************************************************
        ///  ServerLoop
        ///  
        /// <summary>
        /// Main loop of the signing service.
        /// </summary>
        private void ServerLoop()
        {
            _ps = new PipeSecurity();
            _sid = new SecurityIdentifier(WellKnownSidType.AuthenticatedUserSid, null);
            _pr = new PipeAccessRule(_sid, PipeAccessRights.FullControl, AccessControlType.Allow);
            _ps.AddAccessRule(_pr);

            while (running)
            {
                ProcessNextClient();
            }
            terminateHandle.Set();
        }

        ///********************************************************************************************
        ///  Run
        ///  
        /// <summary>
        /// Starts service thread(s).
        /// </summary>
        public void Run()
        {
            running = true;
            runningThread = new Thread(ServerLoop);
            runningThread.Start();
        }

        ///********************************************************************************************
        ///  Run
        ///  
        /// <summary>
        /// Stops service thread(s).
        /// </summary>
        public void Stop()
        {
            running = false;
            terminateHandle.WaitOne();
        }

        ///********************************************************************************************
        ///  ProcessNextClient
        ///  
        /// <summary>
        /// .
        /// </summary>
        public void ProcessNextClient()
        {
            NamedPipeServerStream pipeStream = null;
            try
            {
                pipeStream = new NamedPipeServerStream(PipeName, PipeDirection.InOut,
                                    NamedPipeServerStream.MaxAllowedServerInstances, PipeTransmissionMode.Byte,
                                    PipeOptions.None, 4096, 4096, _ps);
                pipeStream.WaitForConnection();

                //Spawn a new thread for each request and continue waiting
                Thread t = new Thread(PipeThreadMain);
                t.Start(pipeStream);
            }
            catch (Exception e)
            {
                LogError("ProcessNextClient", e);
                Thread.Sleep(1000);
                //If there are no more available connections (254 in use already) then just keep looping until one is avail
            }
        }

        ///********************************************************************************************
        ///  ProcessRequest
        ///  
        /// <summary>
        ///  Overridden by individual pipe implementations
        /// </summary>
        public virtual void ProcessRequest(string pipein, StreamWriter pipeout)
        {
        }

        ///********************************************************************************************
        ///  PipeThreadMain
        ///  
        /// <summary>
        ///  Pipe management logic. Calls processrequest when a message is received.
        /// </summary>
        public void PipeThreadMain(object o)
        {
            try
            {
                using (NamedPipeServerStream pipeStream = (NamedPipeServerStream)o)
                {
                    using (StreamReader pipein = new StreamReader(pipeStream))
                    using (StreamWriter pipeout = new StreamWriter(pipeStream))
                    {
                        // First line is byte count of the rest
                        string sData = pipein.ReadLine();
                        if (sData.Length == 0)
                        {
                            throw new Exception("PipeServer: invalid data received.");
                        }

                        int cbBuf = Int32.Parse(sData);

                        if (cbBuf == 0)
                        {
                            ProcessRequest(null, pipeout);
                        }
                        else if (cbBuf > 0 && cbBuf < MAX_BUFLEN)
                        {
                            Char[] buffer = new Char[cbBuf];
                            pipein.Read(buffer, 0, cbBuf);
                            String strBuf = new String(buffer);
                            ProcessRequest(strBuf, pipeout);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogError("Pipe Communication Error ", ex);
            }
        }

        public string GetURL()
        {
            string url = "http://error-missingregkey";
            try
            {
                using (RegistryKey regKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\ThalesASG\CSSCSP"))
                {
                    object value = regKey.GetValue("ServerBaseURL");

                    if (value == null)
                    {
                        throw new Exception(@"Registry key: SOFTWARE\ThalesASG\CSSCSP\ServerBaseURL not found");
                    }
                    url = value.ToString();
                }
            }
            catch (Exception ex)
            {
                LogError("GetURL", ex);
            }
            return url;
        }


        protected void LogError(string context, Exception e)
        {
            string sSource = "ThalesCodeSign";
            string sLog = "Application";
            string sEvent;

            if (e != null)
            {
                sEvent = String.Format("{0}:\n{1}", context, e.Message);
            }
            else
            {
                sEvent = context;
            }

            if (!EventLog.SourceExists(sSource))
            {
                EventLog.CreateEventSource(sSource, sLog);
            }

            EventLog.WriteEntry(sSource, sEvent, EventLogEntryType.Error);
            Console.WriteLine(sEvent);
        }

        protected void DebugOut(string message)
        {
            string sSource = "ThalesCodeSign";
            string sLog = "Application";

            if (!EventLog.SourceExists(sSource))
            {
                EventLog.CreateEventSource(sSource, sLog);
            }

            EventLog.WriteEntry(sSource, message, EventLogEntryType.Information);
        }

        protected static void DebugOut(string context, string message)
        {
            string sSource = "ThalesCodeSign";
            string sLog = "Application";

            if (!EventLog.SourceExists(sSource))
            {
                EventLog.CreateEventSource(sSource, sLog);
            }

            EventLog.WriteEntry(sSource, String.Format("{0}: {1}", context, message), EventLogEntryType.Information);
        }

    }

    ///********************************************************************************************
    ///  ListKeysPipeServer
    ///  
    /// <summary>
    ///  Pipe implementation for list keys operation.
    /// </summary>
    ///********************************************************************************************
    public class ListKeysPipeServer : pipeServer
    {
        public ListKeysPipeServer()
        {
            PipeName = "ThalesCSSListKeys";
        }

        public ListKeysPipeServer(bool flag)
        {
            PipeName = "ThalesCSSSecondaryListKeys";
            secondaryFlag = true;
        }


        ///********************************************************************************************
        ///  ProcessRequest
        ///  
        /// <summary>
        ///  Processes requests that come in through the list keys pipe.
        /// </summary>
        public override void ProcessRequest(string req, StreamWriter pipeout)
        {
            int errorCode = 0;
            StringBuilder strReply = new StringBuilder(4096);
            DebugOut("listkeys Server:", PipeName);
            var clientLib = new espDirectClientLib();
            try
            {
                String cnCerts = "{}";
                espDirectSigningType[] signingTypes;
                
                // get the signing types by calling the espDirection API function
                if (clientLib.espListSigningTypes(out signingTypes) == true)
                {
                    if (signingTypes != null)
                    {
                        // put certs into list
                        cnCerts = "{";
                        StringDictionary cnCertPairs = new StringDictionary();
                        var lastObject = signingTypes.Last();
                        foreach (var signingType in signingTypes)
                        {
                            // we only need the signing types that are configured for raw signing.
                            if (signingType.certificate != null && signingType.certificate.Length > 0 && signingType.rawSign == true)
                            {
                                X509Certificate cert = new X509Certificate(Convert.FromBase64String(signingType.certificate));

                                // append the name and cert in a json-like string
                                // use a Guid as the key container name 
                                cnCerts += "{" + signingType.guid + "," + signingType.certificate + "}";

                                // if this is now the last object, then add a comma separator to the string
                                if (!signingType.Equals(lastObject))
                                {
                                    cnCerts += ",";
                                }
                            }
                        }
                        cnCerts += "}";
                        strReply.Append(cnCerts);

                        var outputFile = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData), "ThalesCSSCSP", 
                                                      "sigingProfiles.json");
                        // save the returned data in a json file
                        using (StreamWriter outFile = File.CreateText(outputFile))
                        {
                            JsonSerializer serializer = new JsonSerializer();
                            serializer.Serialize(outFile, signingTypes);
                        }
                    }
                }
                else
                {
                    strReply.Append("Error: failed to list signing types.");
                    errorCode = -1;
                }
            }
            catch (Exception ex)
            {
                strReply.Append("Error: Exception - " + ex.Message);
                errorCode = -1;
            }

            // write the response to the client.
            // <errorCode>\n<length>\n<data>
            pipeout.WriteLine(errorCode);
            pipeout.WriteLine(strReply.Length);
            pipeout.Write(strReply);
        }
    }


    public class SignaturePipeServer : pipeServer
    {
        public SignaturePipeServer()
        {
            PipeName = "ThalesCSSSign";
        }

        public SignaturePipeServer(bool flag)
        {
            PipeName = "ThalesCSSSecondarySign";
            secondaryFlag = true;
        }


        ///********************************************************************************************
        ///  ProcessRequest
        ///  
        /// <summary>
        ///  Processes requests that come in through the signature pipe.
        /// </summary>
        public override void ProcessRequest(string req, StreamWriter pipeout)
        {
            int errorCode = 0;
            String signature;
            StringBuilder strReply = new StringBuilder(4096);
            DebugOut("signhash Server:", PipeName);
            var clientLib = new espDirectClientLib();
            try
            {
                // JSON to object 
                espSignHashRequest signHashReq = JsonConvert.DeserializeObject<espSignHashRequest>(req);


                // call the client library to request the hash from the Portal
                if (clientLib.espSignHash(signHashReq, out signature) == true)
                {
                    // Write the response as the number of bytes remaining
                    // followed by the data.  All text.
                    if (signature != null)
                    {
                        strReply.Append(signature);
                    }
                }
                else
                {
                    DebugOut(String.Format("DoHttpGet /sign failed"));
                }

            }
            catch (Exception ex)
            {
                strReply.Append("Error String: " + ex.Message);
                errorCode = -1;
            }

            // write the response to the client.
            // <errorCode>\n<length>\n<data>
            pipeout.WriteLine(errorCode);
            pipeout.WriteLine(strReply.Length);
            pipeout.Write(strReply);
        }
    }
}
