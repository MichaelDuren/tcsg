﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using ESP.espDirectClient.APITypes;
using Microsoft.Win32;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using System.Collections.Generic;

namespace ESP.espDirectClient
{
    public class espDirectClientLib
    {
        private string m_url { get; set; }
        private static string _bearerToken = string.Empty;
        private static DateTime _bearerTokenExpiration = DateTime.Now.AddDays(-5);
        private bool _testing = true;

        public static List<KeyValuePair<string, string>> ParseDistinguishedName(string input)
        {
            int i = 0;
            int a = 0;
            int v = 0;
            var attribute = new char[50];
            var value = new char[200];
            var inAttribute = true;
            string attributeString, valueString;
            var names = new List<KeyValuePair<string, string>>();

            while (i < input.Length)
            {
                char ch = input[i++];
                switch (ch)
                {
                    case '\\':
                        value[v++] = ch;
                        value[v++] = input[i++];
                        break;
                    case '=':
                        inAttribute = false;
                        break;
                    case ',':
                        inAttribute = true;
                        attributeString = new string(attribute).Substring(0, a);
                        valueString = new string(value).Substring(0, v);
                        names.Add(new KeyValuePair<string, string>(attributeString, valueString));
                        a = v = 0;
                        break;
                    default:
                        if (inAttribute)
                        {
                            attribute[a++] = ch;
                        }
                        else
                        {
                            value[v++] = ch;
                        }
                        break;
                }
            }

            attributeString = new string(attribute).Substring(0, a);
            valueString = new string(value).Substring(0, v);
            names.Add(new KeyValuePair<string, string>(attributeString, valueString));
            return names;
        }
        public espDirectClientLib()
        {
            ServicePointManager.ServerCertificateValidationCallback +=
                delegate (object sender, X509Certificate certificate,
                                        X509Chain chain,
                                        SslPolicyErrors sslPolicyErrors)
                {
                    return AcceptServerCertificate(sender, sslPolicyErrors);
                };

        }

        public string GetPortalURL()
        {
            string url = "http://error-missingregkey";
            try
            {
                using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64))
                using (var regKey = hklm.OpenSubKey(@"SOFTWARE\ThalesASG\CSSCSP"))
                {
                    object value = regKey.GetValue("ServerBaseURL");

                    if (value == null)
                    {
                        throw new Exception(@"Registry key: SOFTWARE\ThalesASG\CSSCSP\ServerBaseURL not found");
                    }
                    url = value.ToString();
                }
            }
            catch (Exception ex)
            {
                espDirectUtils.LogError("GetPortalURL", ex);
            }
            return url;
        }

        public string GetOAuthServerURL()
        {
            string url = "http://error-missingregkey";
            try
            {
                using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64))
                using (var regKey = hklm.OpenSubKey(@"SOFTWARE\ThalesASG\CSSCSP"))
                {
                    object value = regKey.GetValue("OAuthServerURL");

                    if (value == null)
                    {
                        throw new Exception(@"Registry key: SOFTWARE\ThalesASG\CSSCSP\OAuthServerURL not found");
                    }
                    url = value.ToString();
                }
            }
            catch (Exception ex)
            {
                espDirectUtils.LogError("GetOAuthServerURL", ex);
            }
            return url;
        }
        public string GetOAuthClientId()
        {
            string url = "http://error-missingregkey";
            try
            {
                using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64))
                using (var regKey = hklm.OpenSubKey(@"SOFTWARE\ThalesASG\CSSCSP"))
                {
                    object value = regKey.GetValue("OAuthClientId");

                    if (value == null)
                    {
                        throw new Exception(@"Registry key: SOFTWARE\ThalesASG\CSSCSP\OAuthClientId not found");
                    }
                    url = value.ToString();
                }
            }
            catch (Exception ex)
            {
                espDirectUtils.LogError("GetOAuthServerURL", ex);
            }
            return url;
        }

        ///********************************************************************************************
        ///  espListSigningTypes
        ///  
        /// <summary>
        ///  Processes requests that come in through the signature pipe.
        /// </summary>
        public bool espListSigningTypes(out espDirectSigningType[] signingTypes)
        {
            var parameters = new StringDictionary();
            string response;
            signingTypes = null;
            bool result = false;

            try
            {
                HttpStatusCode resultCode;
                resultCode = InvokeAPIFunction("espListSigningProfiles", parameters, "", out response);
                if (resultCode == HttpStatusCode.OK)
                {  
                    APIListSigningTypes apiResponse = JsonConvert.DeserializeObject<APIListSigningTypes>(response);
                    signingTypes = apiResponse.signingTypes;
                    if (apiResponse.returnCode.errorCode != 0)
                    {
                        espDirectUtils.LogError(string.Format("Error returned from espListSigningProfiles: {0}", 
                                                                     apiResponse.returnCode.errorCode));
                        result = false;

                    }
                    else
                    {
                        result = true;
                    }
                }
            }
            catch (Exception)
            {
                result = false;
            }

            return result;
        }

        ///********************************************************************************************
        ///  espSignHash
        ///  
        /// <summary>
        ///  Calls the espSignHash on the Portal server.  Function serializes the request,
        ///  calls the InvokeAPIFunction, and deserializes the response.
        /// </summary>
        public bool espSignHash(espSignHashRequest signHashReq, out String signature)
        {
            var parameters = new StringDictionary() { { "SUBJECT", "1" } };
            string response;
            signature = "";
            bool rc = false;

            try
            {
                HttpStatusCode resultCode;
                resultCode = InvokeAPIFunction("espSignHash", parameters, JsonConvert.SerializeObject(signHashReq), out response);
                if (resultCode == HttpStatusCode.OK)
                {
                    APISignHashResult apiResponse =  JsonConvert.DeserializeObject<APISignHashResult>(response);
                    if (apiResponse.returnCode.errorCode == 0)
                    {
                        signature = apiResponse.response.signatureValue;
                        rc = true;
                    }
                    else
                    {
                        // TODO: interpret the error code
                        espDirectUtils.LogDebug(String.Format("Server return API error code: %d", apiResponse.returnCode.errorCode));
                        signature = "";
                        rc = false;
                    }
                }
                else
                {
                    // record the error and return 

                }
            }
            catch (Exception)
            {

            }

            return rc;
        }

        static bool AcceptServerCertificate(object sender, SslPolicyErrors sslPolicyErrors)
        {
            bool retVal = true;
            RegistryKey regKey = null;
            bool sflag = false;
            HttpWebRequest pReq = (HttpWebRequest)sender;

            using (regKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\ThalesASG\CSSCSP"))
            {
                if (regKey != null)
                {
                    if (SslPolicyErrors.None != (sslPolicyErrors & SslPolicyErrors.RemoteCertificateChainErrors))
                    {
                        object value = regKey.GetValue(sflag ? "PermitServerCertChainErrors2" : "PermitServerCertChainErrors");
                        if (value == null || UInt32.Parse(value.ToString()) == 0)
                        {
                            espDirectUtils.LogError("AcceptServerCertificate: SslPolicyErrors.RemoteCertificateChainErrors not permitted.");
                            retVal = false;
                            goto cleanup;
                        }
                    }

                    if (SslPolicyErrors.None != (sslPolicyErrors & SslPolicyErrors.RemoteCertificateNameMismatch))
                    {
                        object value = regKey.GetValue(sflag ? "PermitServerCertNameMismatch2" : "PermitServerCertNameMismatch");
                        if (value == null || UInt32.Parse(value.ToString()) == 0)
                        {
                            espDirectUtils.LogError("AcceptServerCertificate: SslPolicyErrors.RemoteCertificateNameMismatch not permitted.");
                            retVal = false;
                        }
                    }
                }
                else
                {
                    espDirectUtils.LogError("AcceptServerCertificate: Registry entry not found.");
                    retVal = false;
                }
            }
            cleanup:

            return retVal;
        }

        ///********************************************************************************************
        ///  GetBearerToken
        ///  
        /// <summary>
        ///  Retrieves bearer token from the OAuth server
        /// </summary>
        public string GetBearerToken()
        {
            HttpWebResponse response = null;
            HttpWebRequest request = null;
            string token = String.Empty;
            string expiresIn = String.Empty;

            lock (_bearerToken)
            {
                if (_bearerToken.Length > 0)
                {
                    if (DateTime.Compare(DateTime.Now.AddSeconds(2), _bearerTokenExpiration) < 0)
                    {
                        // use the current token
                        token = _bearerToken;
                    }
                }
            }

            // update the token, but with the lock statements below, 
            // one token should persist after this process and remain until it expires.
            if (token.Length == 0)
            {
                string jsonResponse = string.Empty;
                string strURL = GetOAuthServerURL();
                string contentParameters = string.Empty;

                if (_bearerToken.Length > 0)
                {
                    espDirectUtils.LogInfo("Current bearer token is expiring, getting a new token");
                }
                espDirectUtils.LogInfo("Requesting bearer token from: " + strURL);

                var sslCert = GetSSLClientCertificate();
                if (sslCert == null)
                {
                    espDirectUtils.LogError(String.Format("GetBearerToken, {0}", "Unable to find an SSL client certificate to use."));
                    espDirectUtils.LogDebug(String.Format("GetBearerToken, {0}", "Will connect to server without client authentication."));

                    return string.Empty;
                }
                else
                {
                    request = (HttpWebRequest)WebRequest.Create(strURL);
                    request.Credentials = CredentialCache.DefaultCredentials;
                    request.ClientCertificates.Add(sslCert);
                    request.Method = "POST";
                    request.ContentType = "application/x-www-form-urlencoded";
                    string grantType = "client_credentials";
                    contentParameters = "client_id=" + GetOAuthClientId();
                    contentParameters += "&grant_type=" + grantType;
                }

                request.ContentLength = contentParameters.Length;
                Stream dataStream = request.GetRequestStream();
                using (StreamWriter writer = new StreamWriter(request.GetRequestStream(), Encoding.ASCII))
                {
                     writer.Write(contentParameters);
                }

                try
                {
                    // Get the server respsonse
                    response = (HttpWebResponse)request.GetResponse();

                    // this should not be null, but this check shows that we care
                    if (response != null)
                    { 
                        var rc = response.StatusCode;
                        string tokenType = string.Empty;

                        espDirectUtils.LogDebug(String.Format("Status code return from OAuth server: {0}", rc));

                        // Add headers to response dictionary
                        if (response.ContentLength != 0)
                        {
                            // Read message Content
                            using (Stream responseStream = response.GetResponseStream())
                            {
                                using (StreamReader sr = new StreamReader(responseStream, Encoding.GetEncoding("utf-8")))
                                {
                                    jsonResponse = sr.ReadToEnd();
                                    espDirectUtils.LogDebug("GetBearerToken", String.Format("Response received from OAuth server: {0} bytes", jsonResponse.Length));
                                }
                            }
                        }


                        if (jsonResponse.Length == 0)
                        {
                            espDirectUtils.LogError("OAuth server returned a zero length response.");
                        }
                        else
                        {
                            var dict = JsonConvert.DeserializeObject<Dictionary<string, string>>(jsonResponse);

                            tokenType = dict["token_type"];
                            expiresIn = dict["expires_in"];
                            token = dict["access_token"];

                            espDirectUtils.LogInfo(String.Format("Obtained new bearer token that expires in {0} minutes", expiresIn));
                            espDirectUtils.LogDebug("GetBearerToken", String.Format("token_type: {0}", tokenType));
                            espDirectUtils.LogDebug("GetBearerToken", String.Format("expires_in: {0}", expiresIn));
                            espDirectUtils.LogDebug("GetBearerToken", String.Format("access_token: {0} bytes", token.Length));

                        }

                        if (tokenType == null)
                        {
                            throw new Exception("There was no token_type header value in the AOuth server response.");
                        }

                        if (tokenType.ToLower() != "bearer")
                        {
                            throw new Exception("OAuth token_type is no of type Bearer: " + tokenType);
                        }

                        // expires_in is in seconds
                        var expiration = DateTime.Now.AddSeconds(System.Convert.ToInt32(expiresIn)/2);
                        lock (_bearerToken)
                        {
                            // assign the new values
                            _bearerToken = token;
                            _bearerTokenExpiration = expiration;
                        }
                    }
                }
                catch (WebException ex)
                {
                    using (var stream = ex.Response.GetResponseStream())
                    using (var reader = new StreamReader(stream))
                    {
                        espDirectUtils.LogError("Server returned the following content: " + reader.ReadToEnd());
                    }
                }
                catch (Exception ex)
                {
                    espDirectUtils.LogError("GetBearerToken", ex);
                }

            }
            return _bearerToken;
        }

        ///********************************************************************************************
        ///  InvokeAPIFunction
        ///  
        /// <summary>
        ///  Called to send command to the Portal server.
        /// </summary>
        private HttpStatusCode InvokeAPIFunction(string path, StringDictionary httpParameters, String body, out string jsonResponse)
        {
            HttpStatusCode rc = HttpStatusCode.InternalServerError;
            HttpWebResponse response = null;
            StringDictionary respValues = new StringDictionary();
            HttpWebRequest request = null;
            jsonResponse = null;
            string token;

            try
            {
                // get the OAuth token
                token = GetBearerToken();

                string strURL = String.Format("{0}/api/espDirect/{1}", GetPortalURL(), path);

                if (httpParameters.Count > 0)
                {
                    strURL += "?";
                    int count = 0;
                    foreach (DictionaryEntry de in httpParameters)
                    {
                        strURL += de.Key + "=" + de.Value;
                        ++count;
                        if (count < httpParameters.Count)
                        {
                            strURL += ",";
                        }
                    }
                }

                request = (HttpWebRequest)WebRequest.Create(strURL);

                request.Headers.Add("access_token", token);

                if (body.Length > 0)
                {
                    request.ContentType = "text/json";
                    request.Method = "POST";
                    request.ContentLength = body.Length;

                    Stream dataStream = request.GetRequestStream();
                    using (StreamWriter writer = new StreamWriter(request.GetRequestStream(), Encoding.ASCII))
                    {
                        writer.Write(body);
                    }
                }
                request.Credentials = CredentialCache.DefaultCredentials;

                // Do HTTP GET
                using (response = (HttpWebResponse)request.GetResponse())
                {
                    rc = response.StatusCode;
                    espDirectUtils.LogDebug("DoHttpGet", String.Format("Response received from Portal: {0}", strURL));
                    // Add headers to response dictionary
                    if (response.ContentLength > 0)
                    {
                        // Read message Content
                        using (Stream responseStream = response.GetResponseStream())
                        {
                            using (StreamReader sr = new StreamReader(responseStream, Encoding.GetEncoding("utf-8")))
                            {
                                jsonResponse = sr.ReadToEnd();
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                espDirectUtils.LogError("DoHttpGet Exception", e);
                throw e;
            }
            return rc;
        }
       

        private X509Certificate2 GetSSLClientCertificate()
        {
            X509Certificate2 sslCert = null;
            SystemCerts sysCerts = new SystemCerts(SystemCerts.CERT_SYSTEM_STORE_LOCAL_MACHINE);
            try
            {
                using (var hklm = RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64))
                using (var regKey = hklm.OpenSubKey(@"SOFTWARE\ThalesASG\CSSCSP"))
                {
                    if (regKey == null)
                    {
                        throw new Exception("Configuration error: Registry entry not found.");
                    }

                    string value = (string)regKey.GetValue("SSLClientCertHash");
                    if (value != null && value.Length > 0)
                    {
                        StoreLocation certLocation = StoreLocation.LocalMachine;
                        X509Store store = new X509Store("MY", certLocation);
                        try
                        {
                            store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
                            X509Certificate2Collection certCollection = (X509Certificate2Collection)store.Certificates;
                            if (certCollection != null)
                            {
                                X509Certificate2Collection foundCollection = (X509Certificate2Collection)certCollection.Find(X509FindType.FindByThumbprint,
                                                                                                                             value,
                                                                                                                             false);
                                if (foundCollection.Count < 1)
                                {
                                    throw new Exception("Configuration error: No SSL client certificate has been selected.");
                                }
                                else
                                {
                                    sslCert = foundCollection[0];

                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                    else
                    {
                        throw new Exception("Configuration error: No SSL client certificate has been selected.");
                    }
                }
            }
            catch (Exception e)
            {
                espDirectUtils.LogError("GetSSLClientCertificate", e);
            }

            return sslCert;
        }
    }
}
