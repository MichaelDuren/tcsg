﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace ESP.espDirectClient
{
    public class ServerManager
    {
        public const int SERVER_INSTANCES = 20;
        protected ManualResetEvent m_shutdownEvent;
        protected TimeSpan m_delay;
        protected Thread m_thread;

        protected void ServerMain()
        {
            bool bSignaled = false;
            SignaturePipeServer[] sigServers = new SignaturePipeServer[SERVER_INSTANCES];
            SignaturePipeServer[] secondarySigServers = new SignaturePipeServer[SERVER_INSTANCES];
            ListKeysPipeServer keys = new ListKeysPipeServer();
            ListKeysPipeServer secondaryKeys = new ListKeysPipeServer(true);

            for (int i = 0; i < SERVER_INSTANCES; i++)
            {
                sigServers[i] = new SignaturePipeServer();
                sigServers[i].Run();

                secondarySigServers[i] = new SignaturePipeServer(true);
                secondarySigServers[i].Run();
            }

            keys.Run();
            secondaryKeys.Run();

            while (true)
            {
                // wait for the event to be signaled or for the configured delay
                bSignaled = m_shutdownEvent.WaitOne(m_delay, true);

                // if we were signaled to shutdow, exit the loop
                if (bSignaled == true)
                    break;

                // work is done in the server threads
                Thread.Sleep(5000);
            }
        }

        // start the main thread that services named pipe requests
        public bool startPipeServer()
        {
            // create our threadstart object to wrap our delegate method
            ThreadStart ts = new ThreadStart(this.ServerMain);

            // create the manual reset event and set it to an initial state of unsignaled
            m_shutdownEvent = new ManualResetEvent(false);

            // create the worker thread
            m_thread = new Thread(ServerMain);
            m_thread.Start();

            return true;
        }
    }
}
