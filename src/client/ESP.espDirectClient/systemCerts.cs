﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Runtime.InteropServices;

namespace ESP.espDirectClient
{
    class SystemCerts
    {
        public static int CERT_STORE_PROV_SYSTEM = 10;
        public static int CERT_SYSTEM_STORE_CURRENT_USER = (1 << 16);
        public static int CERT_SYSTEM_STORE_LOCAL_MACHINE = (2 << 16);

        int m_nStoreType;

        [DllImport("CRYPT32", EntryPoint = "CertOpenStore", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern IntPtr CertOpenStore(
            int storeProvider, int encodingType,
            int hcryptProv, int flags, string pvPara);

        [DllImport("CRYPT32", EntryPoint = "CertEnumCertificatesInStore", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern IntPtr CertEnumCertificatesInStore(
            IntPtr storeProvider,
            IntPtr prevCertContext);

        [DllImport("CRYPT32", EntryPoint = "CertCloseStore", CharSet = CharSet.Unicode, SetLastError = true)]
        public static extern bool CertCloseStore(
            IntPtr storeProvider,
            int flags);

        [DllImport("CryptDlg", CharSet = CharSet.Auto, SetLastError = true)]
        public static extern bool CertSelectCertificate(
            ref CERT_SELECT_STRUCT pCertSelectInfo );

        [StructLayout(LayoutKind.Sequential)]
        public struct CERT_SELECT_STRUCT
        {
            public int dwSize;
            public IntPtr hwndParent;
            public IntPtr hInstance;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string pTemplateName;
            public uint dwFlags;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string szTitle;
            public uint cCertStore;
            //[MarshalAs(UnmanagedType.ByValArray)]
            public IntPtr arrayCertStore;
            public string szPurposeOid;
            public uint cCertContext;
            //[MarshalAs(UnmanagedType.ByValArray)]
            public IntPtr arrayCertContext;
            public IntPtr lCustData;
            public IntPtr pfnHook;
            public IntPtr pfnFilter;
            [MarshalAs(UnmanagedType.LPWStr)]
            public string szHelpFileName;
            public uint dwHelpId;
            public IntPtr hprov;

        }
        X509CertificateCollection m_certs;

        public SystemCerts(int nStoreType)
        {
            m_nStoreType = nStoreType;
            m_certs = new X509CertificateCollection();
        }
        ~SystemCerts()
        {
            m_certs.Clear();
        }

        public int Init()
        {
            IntPtr currentCertContext;
            IntPtr hCertStore = CertOpenStore(CERT_STORE_PROV_SYSTEM, 0, 0, m_nStoreType, "MY");
            if (hCertStore != IntPtr.Zero)
            {
                currentCertContext = CertEnumCertificatesInStore(hCertStore, (IntPtr)0);
                int i = 0;
                while (currentCertContext != (IntPtr)0)
                {
                    m_certs.Insert(i++, new X509Certificate(currentCertContext));
                    currentCertContext = CertEnumCertificatesInStore(hCertStore, currentCertContext);
                }
                CertCloseStore(hCertStore, 0);
            }
            return m_certs.Count;
        }

        public int Count()
        {
            return m_certs.Count;
        }

        public X509Certificate this[int index]
        {
            get
            {
                // Check the index limits.
                if (index < 0 || index > m_certs.Count)
                    return null;
                else
                    return m_certs[index];
            }
        }

        public X509Certificate FindCert(string sCertHash)
        {
            if (m_certs.Count == 0)
                Init();

            for (int i = 0; i < m_certs.Count; i++)
            {
                if (m_certs[i].GetCertHashString().Equals(sCertHash, StringComparison.InvariantCultureIgnoreCase))
                    return m_certs[i];
            }
            return null;
        }
    }
}
