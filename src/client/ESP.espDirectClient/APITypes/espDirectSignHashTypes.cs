﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ESP.espDirectClient.APITypes
{
    public class espSignHashRequest
    {
        public string hashAlg { get; set; }
        public string hashValue { get; set; }
        public int signingTypeId { get; set; }
        public String signingTypeGuid { get; set; }
        public string releaseName { get; set; }
    }

    public class espSignHashResponse
    {
        public string signatureValue { get; set; }
        public int requestId { get; set; }
    }

    public class APISignHashResult
    { 
        public espDirectErrorInfo returnCode { get; set;}

        public espSignHashResponse response { get; set; }
    }
}