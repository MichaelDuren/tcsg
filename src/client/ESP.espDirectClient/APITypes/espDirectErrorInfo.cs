﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ESP.espDirectClient.APITypes
{
    public class espDirectErrorInfo
    {
        public int errorCode { get; set; }
        public string errorString { get; set; }
    }
}