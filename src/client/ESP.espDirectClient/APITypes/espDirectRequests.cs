﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ESP.espDirectClient.APITypes
{
    public class espCodesignRequest
    {
        public int requestId { get; set; }
        public int signingTypeId { get; set; }
        public string requestStatus { get; set; }
        public string metadataField1Value { get; set; }
        public string metadataField2Value { get; set; }
        public string metadataField1Label { get; set; }
        public string metadataField2Label { get; set; }
    }

    public class espCodesignRequestCreateParams
    {
        public int signingTypeId { get; set; }
        public string metadataField1Value { get; set; }
        public string metadataField2Value { get; set; }
        public string fileType { get; set; }
    }

    public class APICodesignRequestStatus
    { 
        public espDirectErrorInfo returnCode { get; set;}

        public espCodesignRequest request { get; set; }
    }
}