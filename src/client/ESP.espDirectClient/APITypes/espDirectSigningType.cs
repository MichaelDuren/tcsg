﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ESP.espDirectClient.APITypes
{
    public class espDirectSigningType
    {
        public int signingTypeId { get; set; }
        public string signingTypeName { get; set; }
        public string signingTypeDescription { get; set; }
        public string certificateName { get; set; }
        public string certificate { get; set; }
        public string metadataField1Label { get; set; }
        public string metadataField2Label { get; set; }
        public string guid { get; set; }
        public bool rawSign { get; set; }
    }

    public class APIListSigningTypes
    { 
        public espDirectErrorInfo returnCode { get; set;}
        public espDirectSigningType[] signingTypes;
    }
}