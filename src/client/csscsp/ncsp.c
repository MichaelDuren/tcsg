/*
*
*	nCipher Crypto Service Provider, C source code
*
*	NCSP.C
*		CSP-to-internal functions veneer
*
*
* Versions
*	1997.02.10	IH	Original
*	1997.05.06		SHA-1 ASN.1 identifier changed
*       1998.10.16      JRH     Added support for machine containers
*       1999.05.25      JRH     Added support for key management
*	2002.03.08	NAN	Started adding support for AES
*
* (C) Copyright NCipher Ltd 1997-1999. All rights reserved. Company Confidential.
*
 */

#include <nfast-fixup.h> // we need to include this before windows.h (which comes from ncsp.h)
#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "nonmagic.h"
#include "ncsp.h" // includes windows.h which takes care of wincrypt and winerror
#include "../nfmodexp/nfkmfns.h"
#include "ncspconf.h"
#include "wizfns.h"
#include "../nfmodexp/nfkmsupport.h"
#include "../nfmodexp/nfkmkeyblob.h"

#include "hashdialog.h"

#include "des.h" // for the parity check fns in the wrapped key code.
#include "rc4.h" // for the fast random number generation.

#include "debugcs.h"

//#include <lmcons.h> // this has to go after ncsp.h otherwise windef.h redefines PASCAL, doh.

/* Key function parameters --------------------------------------------------- */

/* Should we claim to be CRYPT_IMPL_HARDWARE even when
   faking it on the softserver? Are we CRYPT_IMPL_MIXED
   until we support key management on the hardware too? ih */
/* Finally - we have KM on hardware and will now not work
   without hardware present. jrh */
static DWORD impl_type = CRYPT_IMPL_HARDWARE;

NFast_AppHandle app;

BOOL nfastpresent;
BOOL FIPS3compatible;
BOOL nfastchecked;

const char *RegKeyName = "Software\\nCipher\\Cryptography\\" DLLNAME "\\UserKeys\\";
const char *RegMKeyName = "Software\\nCipher\\Cryptography\\MachineKeys\\" DLLNAME; // for compatibility.

#define SSL3ABLE CRYPT_FLAG_SSL3
#define TLSABLE CRYPT_FLAG_TLS1
#define SIGNABLE CRYPT_FLAG_SIGNING

// the fixed IV for second stage of CMS key wrapping .
BYTE CMSIV[8] = { 0x4a, 0xdd, 0xa2, 0x2c, 0x79, 0xe8, 0x21, 0x05 };

/* all the foo_LEN constants are defined in ncspconf.h according to the #defines
 * passed in by the build system. They vary depending on which version of the CSP
 * is being built. */

#define MAX_ALGS (sizeof(our_algs_ex)/sizeof(our_algs_ex[0]))
static const PROV_ENUMALGS_EX our_algs_ex[] =
{
  /* aiAlgid
     dwDefaultLen    dwMinLen    dwMaxLen    dwProtocols
     dwNameLen       szName      dwLongNameLen szLongName[40];
     */
#ifndef SIGONLY
  { CALG_RC4,
    DEF_SESSION_KEY_LEN,  MIN_SESSION_KEY_LEN,  MAX_SESSION_KEY_LEN, SSL3ABLE | TLSABLE,
    4, "RC4", 4, "RC4" },

  { CALG_DES,
    64,  56,  64, SSL3ABLE | TLSABLE, 4, "DES", 26, "Data Encryption Standard" },

#ifndef BASEPROV
  { CALG_3DES,
    192,  168,  192, SSL3ABLE | TLSABLE, 5, "3DES", 12, "Triple-DES" },
  { CALG_3DES_112,
    128,  112,  128, SSL3ABLE | TLSABLE, 9, "3DES_112", 26, "Triple-DES with two keys" },
//We only want the following in our Enhanced RSA_AES provider
  /*{ CALG_AES,
    128, 128, 128, SSL3ABLE | TLSABLE, 4, "AES", 12, "128-bit AES" },*/
  { CALG_AES_128,
    128, 128, 128, SSL3ABLE | TLSABLE, 8, "AES_128", 12, "128-bit AES" },
  { CALG_AES_192,
    192, 192, 192, SSL3ABLE | TLSABLE, 8, "AES_192", 12, "192-bit AES" },
  { CALG_AES_256,
    256, 256, 256, SSL3ABLE | TLSABLE, 8, "AES_256", 12, "256-bit AES" },
#endif

#endif

  { CALG_SHA1,
    160, 160, 160, SSL3ABLE | TLSABLE | SIGNABLE, 6, "SHA-1", 23, "Secure Hash Algorithm" },
  { CALG_SHA256,
    256, 256, 256, SSL3ABLE | TLSABLE | SIGNABLE, 8, "SHA-256", 12, "256-bit SHA" },
  { CALG_SHA384,
    384, 384, 384, SSL3ABLE | TLSABLE | SIGNABLE, 8, "SHA-384", 12, "384-bit SHA" },
  { CALG_SHA512,
    512, 512, 512, SSL3ABLE | TLSABLE | SIGNABLE, 8, "SHA-512", 12, "512-bit SHA" },

  { CALG_MD5,
    128, 128, 128, SSL3ABLE | TLSABLE | SIGNABLE, 4, "MD5", 18, "Message Digest 5"  },

#ifndef SIGONLY
  { CALG_SSL3_SHAMD5,
    288, 288, 288, 0, 12, "SSL3 SHAMD5", 28, "SSL3 combined SHA-MD5 hash" },
#endif

  { CALG_MAC,
    64,  64,  64,  0, 4, "MAC", 19, "Message Authentication Code"  },

#ifdef RSACSP
  { CALG_RSA_SIGN,
    DEF_PUBLIC_KEY_SIGN_LEN, MIN_PUBLIC_KEY_SIGN_LEN, MAX_PUBLIC_KEY_SIGN_LEN, SSL3ABLE | TLSABLE | SIGNABLE,
    9, "RSA_SIGN", 15, "RSA Signature" },

#ifndef SIGONLY
  { CALG_RSA_KEYX,
    DEF_PUBLIC_KEY_KEYX_LEN, MIN_PUBLIC_KEY_KEYX_LEN, MAX_PUBLIC_KEY_KEYX_LEN, SSL3ABLE | TLSABLE | SIGNABLE,
    9, "RSA_KEYX", 18, "RSA Key Exchange" },
#endif
#else
  { CALG_DSS_SIGN,
    DEF_PUBLIC_KEY_SIGN_LEN, MIN_PUBLIC_KEY_SIGN_LEN, MAX_PUBLIC_KEY_SIGN_LEN, SSL3ABLE | TLSABLE | SIGNABLE,
    9, "DSA_SIGN", 15, "DSS Signature" },

#ifndef SIGONLY
  { CALG_DH_SF,
    DEF_PUBLIC_KEY_KEYX_LEN, MIN_PUBLIC_KEY_KEYX_LEN, MAX_PUBLIC_KEY_KEYX_LEN, SSL3ABLE | TLSABLE | SIGNABLE,
    8, "DH_KEYX", 33, "Diffie-Hellman Store and Forward" },

  { CALG_DH_EPHEM,
    DEF_PUBLIC_KEY_KEYX_LEN, MIN_PUBLIC_KEY_KEYX_LEN, MAX_PUBLIC_KEY_KEYX_LEN, SSL3ABLE | TLSABLE | SIGNABLE,
    8, "DH_KEYX", 25, "Diffie-Hellman Ephemeral" },
#endif
#endif

  { CALG_HMAC,
    0,  0,  0,  0,   5, "HMAC", 12, "Hugo's MAC"  },

};

// Change the following macro if the list extends.
#define CALG_LASTALGONLIST CALG_HMAC

/* ----------------------------------------- */

/*const char *nteErrors[] = {
   "NTE_OP_OK",
   "NTE_BAD_UID",
   "NTE_BAD_HASH",
   "NTE_BAD_KEY",
   "NTE_BAD_LEN",
   "NTE_BAD_DATA",
   "NTE_BAD_SIGNATURE",
   "NTE_BAD_VER",
   "NTE_BAD_ALGID",
   "NTE_BAD_FLAGS",
   "NTE_BAD_TYPE",
   "NTE_BAD_KEY_STATE",
   "NTE_BAD_HASH_STATE",
   "NTE_NO_KEY",
   "NTE_NO_MEMORY",
   "NTE_EXISTS",
   "NTE_PERM",
   "NTE_NOT_FOUND",
   "NTE_DOUBLE_ENCRYPT",
   "NTE_BAD_PROVIDER",
   "NTE_BAD_PROV_TYPE",
   "NTE_BAD_PUBLIC_KEY",
   "NTE_BAD_KEYSET",
   "NTE_PROV_TYPE_NOT_DEF",
   "NTE_PROV_TYPE_ENTRY_BAD",
   "NTE_KEYSET_NOT_DEF",
   "NTE_KEYSET_ENTRY_BAD",
   "NTE_PROV_TYPE_NO_MATCH",
   "NTE_SIGNATURE_FILE_BAD",
   "NTE_PROVIDER_DLL_FAIL",
   "NTE_PROV_DLL_NOT_FOUND",
   "NTE_BAD_KEYSET_PARAM",
   "NTE_FAIL",
   "NTE_SYS_ERR",
   "NTE_SILENT_CONTEXT",
   "NTE_RANDOM",
   "ERROR_INVALID_PARAMETER",
   "ERROR_MORE_DATA",
   "ERROR_NO_MORE_ITEMS",
   "ERROR_RANDOM"
};

static const char *errorString(int errNum)
{
   if (errNum >= 0x80090000)
   {
      errNum -= 0x80090000;
      if ((errNum >= 0) && (errNum <= 34))
         return nteErrors[errNum];
      else
         return nteErrors[35];
   }
   switch (errNum)
   {
      case 87:
         return nteErrors[36];
         break;
      case 234:
         return nteErrors[37];
         break;
      case 259:
         return nteErrors[38];
         break;
      default:
         return nteErrors[39];
         break;
   }
}*/

BOOL _chkerr( HRESULT errcode, int line, const char *fname, int val )
{
   char errstring[255];
   char *msgBuf;

   ENTER("_chkerr");
   if ((errcode != 0) && (errcode != 259))
   {
      if (!FormatMessage(FORMAT_MESSAGE_ALLOCATE_BUFFER |
                         FORMAT_MESSAGE_FROM_SYSTEM |
                         FORMAT_MESSAGE_IGNORE_INSERTS,
                         NULL,
                         errcode,
                         MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // Default language
                         (LPTSTR) &msgBuf,
                         0,
                         NULL))
      {
         sprintf(errstring, "%x (unknown name, error from FormatMessage was %x) at line %d of %s, val = %d",
                 errcode, GetLastError(), line, fname, val);
      }
      else
      {
         size_t errlen = strlen(msgBuf);

         if(errlen >= 2) {
            msgBuf[errlen-1] = 0;
            msgBuf[errlen-2] = 0;
         }

         memset(errstring, 0, sizeof(errstring));
         snprintf(errstring, sizeof(errstring) - 1, "%x (%s) at line %d of %s, val = %d", errcode, msgBuf, line, fname, val);
         dbg_stre("NCSP error= ",errstring);
         /* These will not appear in the event log. Returning an error from a function is not necessarily
          * the result of something going wrong. */

         LocalFree(msgBuf);
      }
   }

   if (errcode != 0)
   {
      SetLastError(errcode);
      return FALSE;
   }
   return TRUE;
}

/* -------------------- */

void reverse_bytes ( unsigned char *dst, unsigned char *src, int len )
{
   unsigned char tmp, *dst2, *src2;

   ENTER("reverse_bytes");
   dst2 = dst + len-1;
   src2 = src + len-1;

   while ( src2 >= src )
   {
      tmp = *src++;
      *dst++ = *src2--;
      *dst2-- = tmp;
   }
}

/* -------------------- */

#define HPROV_ANY 0xFFFFFFFF

#define ValidateProv(x) _ValidateProv(x,__FILE__,__LINE__)

static PCONTAINER _ValidateProv ( HCRYPTPROV hProv, const char *filename, const int lineno )
{
   PCONTAINER pC = (PCONTAINER) hProv;

   ENTER("_ValidateProv");

   if (hProv != HPROV_ANY)
   {
      if ( pC == NULL || pC->tag != TAG_CONTAINER_OBJ  )
      {
         dbg_printfe("ValidateProv: pC = %x, pC->tag = %x (should be %x) [%s line %d]\n",
                     pC, pC ? pC->tag : 0, TAG_CONTAINER_OBJ, filename, lineno);
         chkerr(NTE_BAD_UID);
         return NULL;
      }

#ifdef CONTEXTTHREADCHECK
      /* we don't want to stop re-entrancy from within the same thread, because
         it will always be from insinde the library and therefore on purpose.   */
      if ( (pC->using_thread != 0 ) &&
           ((pC->using_thread != GetCurrentThreadId()) || (pC->using_process != GetCurrentProcessId())) )
      {
         dbg_vale("Clashing process/threads - creating process = ",pC->calling_process);
         dbg_vale("                            calling process = ",GetCurrentProcessId());
         dbg_vale("                             owning process = ",pC->using_process);
         dbg_vale("                            creating thread = ",pC->calling_thread);
         dbg_vale("                             calling thread = ",GetCurrentThreadId());
         dbg_vale("                              owning thread = ",pC->using_thread);
         dbg_printfe("                                              [%s line %d]\n", filename, lineno);
         chkerr(NTE_BAD_KEYSET);
         return NULL;
      }
      else
      {
         pC->thread_count++;
         pC->using_process = GetCurrentProcessId();
         pC->using_thread = GetCurrentThreadId();
      }
#endif
      /* This next part is the important bit. All CPfoo functions will call
       * this function at the start and all functions presenting a crypto
       * interface will fail if this call does not find an nFast.
       * GenRandom and a couple of others do not fail because they need
       * to be executable during bootup, when the hardserver is not running. */
      if ((pC->flags & CF_NFASTPRESENT) == FALSE)
      {
         dbg_printfe("ValidateProv: no working nFast [%s line %d]\n", filename, lineno);
         chkerr(NTE_NOT_FOUND);
         return NULL;
      }
   }
   return pC;
}

static BOOL ReleaseProv(HCRYPTPROV hProv)
{
#ifdef CONTEXTTHREADCHECK

   PCONTAINER pC = (PCONTAINER) hProv;

   ENTER("ReleaseProv");

   if (hProv != HPROV_ANY)
   {
      if ((pC == NULL) || (pC->tag != TAG_CONTAINER_OBJ) || (pC->using_thread == 0))
      {
         dbg_vale("Released unused container, hProv = ",hProv);
         chkerr(NTE_FAIL);
         return FALSE;
      }
      else
      {
         pC->thread_count--;
         if (pC->thread_count = 0)
         {
            pC->using_process = 0;
            pC->using_thread = 0;
         }
      }
   }
#endif

   return TRUE;
}


/* -------------------- */

#define ValidateKey(hp,hk,pm,m) _ValidateKey(hp,hk,pm,m,__FILE__,__LINE__)

static PKEY_OBJ _ValidateKey ( HCRYPTPROV hProv, HCRYPTKEY hKey, unsigned int perm_mask,
                              const char *message, const char *filename, const int lineno )
{
   PKEY_OBJ pK = (PKEY_OBJ) hKey;
   PCONTAINER pC = (PCONTAINER) hProv;

   ENTER("_ValidateKey");

   /*printf("pK = %x, pK->tag = %x, TAG_KEY_OBJ = %x\n",pK,pK->tag,TAG_KEY_OBJ);*/
   if ( pK == NULL || pK->tag != TAG_KEY_OBJ )
   {
      dbg_printfe("ValidateKey: [%s line %d]\n", filename, lineno);
      if (pK == NULL)
         dbg_stre("ValidateKey: pK == NULL","");
      else
      {
         dbg_vale("ValidateKey: pK->tag != TAG_KEY_OBJ (0xCE435332) but == ",pK->tag);
         dbg_ptre("Key == ", pK);
         dbg_vale("             pK->alloc_size         = ", pK->alloc_size);
         dbg_ptre("             pK->container          = ", pK->container);
         dbg_vale("             pK->nfastprivhandle    = ", (int)pK->nfastprivhandle);
         dbg_vale("             pK->nfastpubhandle     = ", (int)pK->nfastpubhandle);
         dbg_vale("             pK->kcp.alg_type          = ", (int)pK->kcp.alg_type);
         dbg_vale("             pK->kcp.permissions       = ", pK->kcp.permissions);
         dbg_vale("             pK->kcp.flags             = ", pK->kcp.flags);
         dbg_vale("             pK->kcp.key_secret_len    = ", pK->kcp.key_secret_len);
         dbg_vale("             pK->kcp.blk_size_bits     = ", pK->kcp.blk_size_bits);
         dbg_vale("             pK->kcp.block_mode        = ", pK->kcp.block_mode);
         dbg_vale("             pK->kcp.feedback_len_bits = ", pK->kcp.feedback_len_bits);
         dbg_vale("             pK->kcp.pad_mode          = ", pK->kcp.pad_mode);
         dbg_vale("             pK->kcp.init_done         = ", pK->kcp.init_done);
         dbg_vale("Current process: ", GetCurrentProcessId());
         dbg_vale("Current thread : ", GetCurrentThreadId());
      }
      if (pC) debugobjectstore(pC->keys, "Key store");
      chkerr(NTE_BAD_KEY);
      return NULL;
   }

   /* HPROV_ANY used occasionally by internal routines calling each other.
    * Signifies don't check for the correct container (and don't indirect
    * through pC!) */
   if ( hProv != HPROV_ANY )
   {
      if ( pC == NULL || pK->container != pC || pC->tag != TAG_CONTAINER_OBJ  )
      {
         dbg_printfe("ValidateKey: pC = %x, pK->container = %x, pC->tag = %x (should be %x) [%s line %d]\n",
                     pC, pK->container, pC ? pC->tag : 0xDEADDEAD, TAG_CONTAINER_OBJ, filename, lineno);
         if (pC) debugobjectstore(pC->keys, "Key store");
         chkerr(NTE_BAD_UID);
         return NULL;
      }
      // next fn now only called on key creatin and deletion.

#ifdef PREALLOCATEDCHECK
      if (!checkpreallocatedkeys(pC))
      {
         chkerr(NTE_BAD_KEY);
         return NULL;
      }
#endif
   }

   if ( (pK->kcp.permissions & perm_mask) != perm_mask )
   {
      dbg_printf("ValidateKey: bad permissions (pK->kcp.permissions = %x, perm_mask = %x) [%s line %d]\n",
                 pK->kcp.permissions, perm_mask, filename, lineno);
      chkerr(NTE_PERM);
      return NULL;
   }

   if (pK->kcp.flags & KF_USER_WARN)
   {
      char usermessage[255];

      if (hProv != HPROV_ANY) /* ... but this is bad, because we now have no way of knowing
                                 whether the calling function's _real_ hProv was SILENT or not. */
      {
         if (pC->flags & CF_SILENT)
         {
            chkerr(NTE_SILENT_CONTEXT);
            return NULL;
         }
      }

      sprintf(usermessage,"A user key is about to be  %.200s. Is this OK?",
              message);
      if (MessageBox(NULL,usermessage,"nCipher Cryptography",
               MB_YESNO | MB_ICONQUESTION | MB_SETFOREGROUND) != IDYES)
      {
         chkerr(NTE_PERM);
         return NULL;
      }
   }
   return pK;
}

BOOL CheckPKReady(PKEY_OBJ pK)
{

   ENTER("CheckPKReady");
   if ((pK->kcp.alg_type != CALG_RSA_KEYX) &&
       (pK->kcp.alg_type != CALG_RSA_SIGN) &&
       (pK->kcp.alg_type != CALG_DSS_SIGN) &&
       (pK->kcp.alg_type != CALG_DH_SF) &&
       (pK->kcp.alg_type != CALG_DH_EPHEM))
      return TRUE;

   if ((pK->nfastpubhandle == 0) && (pK->nfastprivhandle == 0))
   {
      dbg_stre("CheckPKReady: neither nFast handle present. DH/DSA key halfway made?", "");
      return FALSE;
   }
   return TRUE;
}

static void LockKey(HCRYPTKEY hKey)
{
   ENTER("LockKey");
#ifdef LOCKKEYS
   if (hKey)
   {
      EnterCriticalSection(&( ((PKEY_OBJ)hKey)->critical ));
   }
#endif
}

static void UnlockKey(HCRYPTKEY hKey)
{
   ENTER("UnlockKey");
#ifdef LOCKKEYS
   if (hKey)
   {
      LeaveCriticalSection(&( ((PKEY_OBJ)hKey)->critical ));
   }
#endif
}


/* -------------------- */

#define ValidateHash(hp,hh,pm) _ValidateHash(hp,hh,pm,__FILE__,__LINE__)

static PHASH_OBJ _ValidateHash ( HCRYPTPROV hProv, HCRYPTHASH hHash, int perm_mask,
                                const char *filename, const int lineno  )
{
   PHASH_OBJ pH = (PHASH_OBJ) hHash;
   PCONTAINER pC = (PCONTAINER) hProv;

   ENTER("_ValidateHash");
   if ( pH == NULL || pH->tag != TAG_HASH_OBJ )
   {
      dbg_printfe("ValidateHash: [%s line %d]\n", filename, lineno);
      if (pH == NULL)
      {
         dbg_stre("ValidateHash: pH == NULL", "");
      }
      else
      {
         dbg_vale("ValidateHash: pH->tag != TAG_HASH_OBJ (0xCE435331) but == ",pH->tag);
         dbg_ptre("Hash == ", pH);
         dbg_vale("             pH->alloc_size         = ", pH->alloc_size);
         dbg_vale("             pH->alg_type           = ", (int)pH->alg_type);
         dbg_ptre("             pH->container          = ", pH->container);
         dbg_vale("             pH->permissions        = ", pH->permissions);
         dbg_vale("             pH->finalised          = ", pH->finalised);
         dbg_vale("             pH->valueset           = ", pH->valueset);
      }
      if (pC) debugobjectstore(pC->hashes, "Hash store");
      chkerr(NTE_BAD_HASH);
      return NULL;
   }

   if (( hProv != HPROV_ANY ) && (pH->container != NULL)) // see comments for ValidateKey.
   {
      if ( pC == NULL || pH->container != pC || pC->tag != TAG_CONTAINER_OBJ  )
      {
         dbg_printfe("ValidateHash: pC = %x, pH->container = %x, pC->tag = %x (should be %x) [%s line %d]\n",
                     pC, pH->container, pC ? pC->tag : 0xDEADDEAD, TAG_CONTAINER_OBJ, filename, lineno);
         if (pC) debugobjectstore(pC->hashes, "Hash store");
         chkerr(NTE_BAD_UID);
         return NULL;
      }
   }

   if ( (pH->permissions & perm_mask) != perm_mask )
   {
      dbg_printf("ValidateHash: bad permissions (pK->kcp.permissions = %x, perm_mask = %x) [%s line %d]\n",
                 pH->permissions, perm_mask, filename, lineno);
      chkerr(NTE_PERM);
      return NULL;
   }

   return pH;
}

/* -------------------- */

static int GetAlgType ( HCRYPTHASH hHash )
{
  PHASH_OBJ pH = (PHASH_OBJ) hHash;

  ENTER("GetAlgType");
  assert(pH->tag == TAG_HASH_OBJ);
  return pH->alg_type;
}

/* ---------------------------------------------- */

#define Roundup(a) (((a)+3) & ~3)

BOOL SwapKeyObj(PKEY_OBJ pK1, PKEY_OBJ pK2)
{
   KEY_OBJ Ktemp;
   void *special1, *special2;

   ENTER("SwapKeyObj");
   dbg_printf("Swapping key objects: key 1 = 0x%x, key 2 = 0x%x.\n", pK1, pK2);
   if (pK1->global != pK2->global)
   {
      dbg_printf("Two keys are not both global or both not global!\n"
                 "pK1->global = %d, pK2->global = %d.\n", pK1->global, pK2->global);
      return FALSE;
   }
   dbg_printf("pK1->nfastpubhandle = 0x%x, pK1->nfastprivhandle = 0x%x.\n",
              pK1->nfastpubhandle, pK1->nfastprivhandle);
   dbg_printf("pK2->nfastpubhandle = 0x%x, pK2->nfastprivhandle = 0x%x.\n",
              pK2->nfastpubhandle, pK2->nfastprivhandle);


   /* swap all the information *except* the 'special' bit,
    * which has to associate with the specific memory of the key. */
   special1 = pK1->special;
   special2 = pK2->special;

   memcpy(&Ktemp, pK1,    sizeof(KEY_OBJ));       /* pK1 -----> pKtemp */
   memcpy(pK1,    pK2,    sizeof(KEY_OBJ));       /* pK2 -----> pK1    */
   memcpy(pK2,    &Ktemp, sizeof(KEY_OBJ));       /* pKtemp --> pK2    */

   pK1->special = special1;
   pK2->special = special2;

   dbg_printf("pK1->nfastpubhandle = 0x%x, pK1->nfastprivhandle = 0x%x.\n",
              pK1->nfastpubhandle, pK1->nfastprivhandle);
   dbg_printf("pK2->nfastpubhandle = 0x%x, pK2->nfastprivhandle = 0x%x.\n",
              pK2->nfastpubhandle, pK2->nfastprivhandle);

   return TRUE;
}


PKEY_OBJ CreateKeyObj ( PCONTAINER pKC, unsigned int key_size, unsigned int IV_size,
                        unsigned int fn_ctx_size, unsigned int extra_ctx_size,
                        unsigned int oaep_params_size, BOOL global )
{
   unsigned char *pblk;
   PKEY_OBJ pKO;
   void *special;
   int tot_size;

   ENTER("CreateKeyObj");
   assert ( pKC != NULL );

   /* Allocate memory ------------- */

   tot_size = sizeof(KEY_OBJ) +
      Roundup(key_size) +
      Roundup(IV_size) +
      Roundup(fn_ctx_size) +
      Roundup(extra_ctx_size) +
      Roundup(oaep_params_size);

   dbg_val("CreateKeyObj:    key_size = ", key_size);
   dbg_val("                  IV_size = ", IV_size);
   dbg_val("              fn_ctx_size = ", fn_ctx_size);
   dbg_val("           extra_ctx_size = ", extra_ctx_size);
   dbg_val("         oaep_params_size = ", oaep_params_size);
   dbg_val("                 tot_size = ", tot_size);

   //EnterCriticalSection(&(pKC->critical));
   // Shouldn't need to get this; we get locks on all the keys we need anyway and it was taking bloody ages.

   if (!claimobject(pKC->keys, global, tot_size, &((void *)pKO), &special))
   {
      chkerr(NTE_NO_MEMORY);
      return NULL;
   }

   pKO->special = special;
   pKO->global = global;
   pblk = ((unsigned char *)pKO+sizeof(KEY_OBJ));

   /* Allocate fields ---------- */

   if ( key_size > 0 )
   {
      pKO->key.ptr = pblk;
      pKO->key.len = key_size;
      memset(pKO->key.ptr, 0, pKO->key.len);
      pblk += Roundup(key_size);
   }

   if ( IV_size > 0 )
   {
      pKO->IV.ptr = pblk;
      pKO->IV.len = IV_size;
      memset(pKO->IV.ptr, 0, pKO->IV.len);
      pblk += Roundup(IV_size);
   }

   if ( fn_ctx_size > 0 )
   {
      pKO->fn_ctx.ptr = pblk;
      pKO->fn_ctx.len = fn_ctx_size;
      memset(pKO->fn_ctx.ptr, 0, pKO->fn_ctx.len);
      pblk += Roundup(fn_ctx_size);
   }

   if ( extra_ctx_size > 0 )
   {
      pKO->extra_ctx.ptr = pblk;
      pKO->extra_ctx.len = extra_ctx_size;
      memset(pKO->extra_ctx.ptr, 0, pKO->extra_ctx.len);
      pblk += Roundup(extra_ctx_size);
   }

   if ( oaep_params_size > 0 )
   {
      pKO->oaep_params.ptr = pblk;
      pKO->oaep_params.len = oaep_params_size;
      memset(pKO->oaep_params.ptr, 0, pKO->oaep_params.len);
      pblk += Roundup(oaep_params_size);
   }

   pKO->alloc_size = tot_size;
   pKO->tag = TAG_KEY_OBJ;
   pKO->nfastprivhandle = 0; /* place-holder. An nFast will never give out a handle of 0. */
   pKO->nfastpubhandle = 0;  /* so we can use this to test for an RSA key w/o needing separate flag. */
   pKO->multilock = NULL;
   pKO->container = pKC;

   dbg_ptr("Got key object : ", pKO);
   dbg_ptr("     container : ", pKO->container);

   return pKO;
}

/* ------------------------------ */

void DestroyKeyObj ( PKEY_OBJ pKO )
{
   int i;
   PCONTAINER pC = pKO->container;
   //PKEY_OBJ next;
   //PKEY_OBJ prev;
   //int malloced, preallocated;

   ENTER("DestroyKeyObj");
   dbg_ptr("DestroyKeyObj", pKO);

   /* Check for RSA keys and destroy on the unit */
   if ((pKO->nfastprivhandle) && (pKO->kcp.flags & KF_PERMANENT))
      // don't destroy either of the two permanent keys on the unit...
      dbg_val("Not destroying permanent key, kcp.flags = ",(int)(pKO->kcp.flags));
   else if (pKO->nfastpubhandle)
      NFast_DestroyKey(pKO, TRUE);

   /* Delete any internal references to it */

   for ( i=0; i<USER_KEYS_MAX; i++ )
   {
      if ( pC->userkeys[i] == pKO )
      {
         EnterCriticalSection(&(pC->critical));
         pC->userkeys[i] = NULL;
         LeaveCriticalSection(&(pC->critical));
      }
   }

   //CLEAR ( pKO, pKO->alloc_size );
   CLEAR(pKO->key.ptr, pKO->key.len); // assumes memset does nothing if count = 0.
   CLEAR(pKO->IV.ptr, pKO->IV.len);
   CLEAR(pKO->fn_ctx.ptr, pKO->fn_ctx.len);
   CLEAR(pKO->extra_ctx.ptr, pKO->extra_ctx.len);
   CLEAR(pKO->oaep_params.ptr, pKO->oaep_params.len);
   pKO->nfastprivhandle = 0;
   pKO->nfastpubhandle = 0;
   CLEAR(&(pKO->kcp), sizeof(pKO->kcp));
   pKO->tag = 0;
   pKO->alloc_size = 0;

   if (!releaseobject(pKO->special))
      chkerr(NTE_BAD_KEY);
}

/* -------------------- */

PHASH_OBJ CreateHashObj ( PCONTAINER pKC,
			  unsigned int context_size,
			  unsigned int hashval_size, BYTE *preallocated )
{
   unsigned char *pblk;
   PHASH_OBJ pHO;
   void *special;
   int tot_size;

   ENTER("CreateHashObj");
   tot_size = sizeof(HASH_OBJ) + Roundup(context_size)+ Roundup(hashval_size);

   dbg_val("CreateHashObj:  context_size = ", context_size);
   dbg_val("                hashval_size = ", hashval_size);
   dbg_val("                    tot_size = ", tot_size);

   if (preallocated)
   {
      pHO = (PHASH_OBJ)preallocated;
      pblk = preallocated+sizeof(HASH_OBJ);
      pHO->special = NULL; /* very important! This will be used in DestroyHashObj to
                              check whether we need to release this properly! */
   }
   else
   {
      if (pKC == NULL)
      {
         dbg_stre("Creating a non-constant block hash without a container!", "");
         return NULL;
      }

      if (!claimobject(pKC->hashes, FALSE, tot_size, &((void *)pHO), &special))
      {
         chkerr(NTE_NO_MEMORY);
         return NULL;
      }

      pHO->special = special;
      pblk = ((unsigned char *)pHO + sizeof(HASH_OBJ));
   }

   if ( context_size > 0 )
   {
      pHO->hashctx.ptr = pblk;
      pHO->hashctx.len = context_size;
      pblk += Roundup(context_size);
   }
   else
   {
      pHO->hashctx.ptr = NULL;
      pHO->hashctx.len = 0;
   }

   if ( hashval_size > 0 )
   {
      pHO->hashval.ptr = pblk;
      pHO->hashval.len = hashval_size;
      pblk += Roundup(hashval_size);
   }
   else
   {
      pHO->hashval.ptr = NULL;
      pHO->hashval.len = 0;
   }


   /* Add to master list */

   pHO->alloc_size = tot_size;
   pHO->tag = TAG_HASH_OBJ;

   dbg_ptr("Got hash object : ", pHO);
   dbg_ptr("      container : ", pHO->container);

   return pHO;
}

/* ------------------------------ */

void DestroyHashObj ( PHASH_OBJ pHO )
{
   PCONTAINER pC = pHO->container;
   ENTER("DestroyHashObj");
   dbg_ptr("Freeing hash object   ", pHO);

   // Most of this switch statement makes sure we free the keys of keyed hashes at this point.
   switch (pHO->alg_type)
   {
      case CALG_MAC:
         {
            PMAC_STATE pstate = (PMAC_STATE) (pHO->hashctx.ptr);
            if (pstate->key)
            {
               PKEY_OBJ pKO;
               dbg_ptr("Keyed MAC; also freeing key, pstate->key = ", pstate->key);
               /* Replacing next line because we don't want to validate the provider
                * or the key; the provider could well be zero. */
               /*CPDestroyKey((HCRYPTPROV)(pHO->container), (HCRYPTKEY)pstate->key);*/
               pKO = ValidateKey ((HCRYPTPROV)(HPROV_ANY), (HCRYPTKEY)pstate->key,
                                  0, "deleted" );
               if ( pKO != NULL )
                  DestroyKeyObj(pKO);
            }
         }
         break;

      case CALG_HMAC:
         {
            PHMAC_STATE pstate = (PHMAC_STATE) (pHO->hashctx.ptr);
            if (pstate->key)
            {
               PKEY_OBJ pKO;
               dbg_ptr("Keyed HMAC; also freeing key, pstate->key = ", pstate->key);
               /*CPDestroyKey((HCRYPTPROV)(pHO->container), (HCRYPTKEY)pstate->key);*/
               pKO = ValidateKey ((HCRYPTPROV)(HPROV_ANY), (HCRYPTKEY)pstate->key,
                                  0, "deleted" );
               if ( pKO != NULL )
                  DestroyKeyObj(pKO);
            }
            // next bit because the hashval for a HMAC doesn't get allocated until the
            // parameters get set.
            if (pHO->hashval.ptr)
            {
               dbg_str("freeing HMAC hashval ptr", "");
               CLEAR(pHO->hashval.ptr, pHO->hashval.len);
               free(pHO->hashval.ptr);
               pHO->hashval.ptr = NULL;
            }
         }
         break;
   }

   if (pHO->hashctx.ptr) CLEAR(pHO->hashctx.ptr, pHO->hashctx.len);
   if (pHO->hashval.ptr) CLEAR(pHO->hashval.ptr, pHO->hashval.len);
   pHO->hashfn = NULL;
   pHO->permissions = 0;
   pHO->finalised = 0;
   pHO->valueset = 0;
   pHO->tag = 0;
   pHO->alloc_size = 0;

   pHO->hashctx.ptr = NULL;
   pHO->hashctx.len = 0;
   pHO->hashval.ptr = NULL;
   pHO->hashval.len = 0;

   pHO->alg_type = 0;

   if (pHO->special)
   {
      /* we're not a special prealloced hash... release it as normal. */
      if (!releaseobject(pHO->special))
         chkerr(NTE_BAD_KEY);
   }
}

#define OPAQUE_BLOB_MAGIC	0x10203040

typedef struct
{
  PUBLICKEYSTRUC blobhdr;	// All blobs should start with this
  int magic;			// May want other stuff here
}
OPAQUE_BLOB_HEADER;

static unsigned int opaque_blob_len ( PKEY_OBJ pK )
{
   ENTER("opaque_blob_len");
  return sizeof(OPAQUE_BLOB_HEADER) + sizeof(KEY_OBJ) + pK->key.len + pK->IV.len + pK->fn_ctx.len + pK->extra_ctx.len;
}

/* ---------------------- */

/* See the PKCS spec for details on this. */

static int get_PKCS1_len ( unsigned char *block, int len, int block_type )
{
   ENTER("get_PKCS1_len");
  if ( block[len-1] != 0 || block[len-2] != block_type ) /* Not PKCS1 */
    return -1;

  /* Search for zero */
  for ( len = len-3; len >= 0; len-- )
    {
      if ( block[len] == 0 )
	break;
    }

  return len;
}
/* ---------------------- */

static BOOL gen_nonzero_random ( PCONTAINER pC, unsigned char *buf, int len )
{
  int i = 0;

  ENTER("get_nonzero_random");
  buf[0] = 0;

  while ( len > 0 )
    {
      while ( buf[0] == 0 )
	{
	  if (!NFast_GenerateRandom(buf, len))
	    return FALSE;
	}

      buf++;
      len--;
    }
  return TRUE;
}

/* ---------------------- */

BOOL build_pkcs1_block (PCONTAINER pC, int blktype, unsigned char *buf, int srclen,
			       int maxlen )
{
  /* Pads the data in 'buf', currently at length 'srclen', to length 'maxlen', as per PKCS1 */

  ENTER("build_pkcs1_block");
  if ( srclen+4 > maxlen )
    return chkerr(NTE_BAD_LEN);

  buf[srclen] = 0;
  if ( blktype == 2 )
    {
      //Randomise ( pC, srclen + maxlen );
      if (!gen_nonzero_random (pC, buf+srclen+1, maxlen-srclen-3 ))
	return FALSE;
    }
  else if ( blktype == 1 )
    memset ( buf+srclen+1, 0xFF, maxlen-srclen-3 );
  else
    memset ( buf+srclen+1, 0, maxlen-srclen-3 );

  buf[maxlen-2] = blktype;
  buf[maxlen-1] = 0;
  return TRUE;
}


/* ---------------------------- */

PKEY_OBJ GetUserKey(PCONTAINER pC, int keynum, BOOL forcereload)
{
   PKEY_OBJ pKO = NULL;

   ENTER("GetUserKey");
   EnterCriticalSection(&(pC->critical)); /* hope this doesn't cause contention... */
   if ((pC->userkeys[keynum] == NULL) || (forcereload)) /* Already there */
   {
      if (pC->userkeys[keynum] == NULL)
      {
         dbg_printf("GetUserKey: allocating new pC->userkeys[%d].\n", keynum);
         pC->userkeys[keynum] = CreateKeyObj ( pC, 0, 0, 0, 0, 0, TRUE );
         // no internal key material
         if ( pC->userkeys[keynum] == NULL )
         {
            chkerr(NTE_NO_MEMORY);
            LeaveCriticalSection(&(pC->critical));
            return NULL;
         }
         pC->userkeys[keynum]->kcp.flags = KF_PRIVATE_KEY | KF_PUBLIC_KEY | KF_PERMANENT;
      }
      dbg_printf("GetUserKey: (re-)loading pC->userkeys[%d].\n", keynum);
      /* not now getting multilock.
       * Also LoadKMFile calls PermsFromKeys for us. */
      if (!NFast_LoadKMFile(pC->userkeys[keynum], keynum, FALSE))
      {
         dbg_printf("Destroying pC->userkeys[keynum] = 0x%x.\n", pC->userkeys[keynum]);
         DestroyKeyObj(pC->userkeys[keynum]);
         pC->userkeys[keynum] = NULL;
         chkerr(GetLastError());
         LeaveCriticalSection(&(pC->critical));
         return NULL;
      }
   }
   LeaveCriticalSection(&(pC->critical));

   dbg_printf("GetUserKey: duplicating pC->userkeys[%d].\n", keynum);
   if (!ValidateKey ( (HCRYPTPROV) pC, (HCRYPTKEY)pC->userkeys[keynum], 0, "opened" )) {
     chkerr(NTE_FAIL);
     return NULL;
   }
   if (!CPDuplicateKey((HCRYPTPROV) pC, (HCRYPTKEY)pC->userkeys[keynum], NULL, 0,
                       (HCRYPTKEY*)&pKO))
      return NULL;

   dbg_printf("GetUserKey has duplicated: pKO = 0x%x, priv handle = 0x%x, pub handle = 0x%x.\n",
              pKO, pKO->nfastprivhandle, pKO->nfastpubhandle);

   return pKO;
}

/* ---------------------- */

static BOOL ImportWrappedKey ( PCONTAINER pC, PSIMPLE_KEY_BLOB pSKB,
			       unsigned int len, HCRYPTKEY *hContent, HCRYPTKEY hExportKey )
{
   PKEY_OBJ pKey = NULL;
   PKEY_OBJ pContent = NULL;
   HCRYPTKEY hKey;
   HCRYPTPROV hProv = (HCRYPTPROV)pC;
   int bloblen;

   ENTER("ImportWrappedKey");
   if (len < SIMPLE_HEADER_SIZE)
      return chkerr(NTE_BAD_DATA);

   bloblen = len - SIMPLE_HEADER_SIZE;
   dbg_val("Import a symmetric/symmetric key, length = ", bloblen);

   // we need to copy the key because we reset the IV twice, and don't want to do it on the original.
   if (!CPDuplicateKey(hProv, hExportKey, NULL, 0, &hKey))
   {
      dbg_val("Unable to copy the export key, last error = ",GetLastError());
      return FALSE;
   }

   pKey = ValidateKey( hProv, hKey, CRYPT_READ, "used to encrypt an WINAPI keyblob");

   dbg_val("init_done for import key = ", pKey->kcp.init_done);

   if (pSKB->blobhdr.aiKeyAlg != pKey->kcp.alg_type)
   {
      // the key to wrap with must be the same as the key to be unwrapped...
      dbg_str("Sym/sym key algorithms not the same","");
      return chkerr(NTE_BAD_KEY);
   }

   switch (pSKB->blobhdr.aiKeyAlg)
   {
      case CALG_3DES:
         {
            int IVlen;

            BYTE TEMP32[40];
            int len;
            DWORD keymode = CRYPT_MODE_CBC;

            BYTE *IV, *TEMP1;

            HCRYPTHASH hICV;
            BYTE newICV[20];
            BYTE *CEKICV, *CEK, *ICV;

            dbg_str("Unwrapping DES3 key with DES3 key", "");

            // 1) If the wrapped content-encryption key is not 40 octets, then error.
            if (bloblen != 40)
            {
               dbg_val("Length of DES3 blob != 40 bytes, = ", bloblen);
               return NTE_BAD_DATA;
            }

            // 2) Decrypt the wrapped key in CBC mode using the encryption key, with the standard
            // fixed IV. Call the output TEMP3.
            if (!CPGetKeyParam(hProv, hKey, KP_IV, NULL, &IVlen, 0))
            {
               dbg_str("Unable to get IV len","");
               return FALSE;
            }
            if (IVlen != 8)
            {
               dbg_val("IV length for DES3 key returned != 8, = ", IVlen);
               return chkerr(NTE_BAD_DATA);
            }
            if (!CPSetKeyParam(hProv, hKey, KP_IV, CMSIV, 0))
            {
               dbg_str("Unable to set first IV value","");
               return chkerr(NTE_BAD_DATA);
            }
            if (!CPSetKeyParam(hProv, hKey, KP_MODE, (unsigned char *)(&keymode), 0))
               // does default to this, but just to make sure...
            {
               dbg_str("Unable to set key mode to CBC","");
               return FALSE;
            }

            memcpy(TEMP32, pSKB->keyblk, 40);
            len = 40;
            if (!CPDecrypt(hProv, hKey, 0, FALSE, 0, TEMP32, &len))
            {
               dbg_str("Unable to decrypt to TEMP2","");
               return FALSE;
            }
            pKey->kcp.init_done = 0; // ... which is what the final flag should have done.
            if (len != 40)
            {
               dbg_val("Length of first decrypted data != 40, = ", len);
               return chkerr(NTE_BAD_DATA);
            }

            // 3) TEMP2 := TEMP3 in reverse order.
            reverse_bytes(TEMP32, TEMP32, 40);

            // 4) Decompose TEMP2 into 8 bytes of IV and 32 bytes of TEMP1.
            IV = TEMP32;
            TEMP1 = TEMP32+8;

            // 5) Decrypt TEMP1 in CBC mode using the IV from step 4. Plaintext = CEKICV.
            if (!CPSetKeyParam(hProv, hKey, KP_IV, IV, 0)) // will just take the 1st 8 bytes.
            {
               dbg_str("Unable to set second IV value","");
               return FALSE;
            }
            len = 32;
            if (!CPDecrypt(hProv, hKey, 0, FALSE, 0, TEMP1, &len))
            {
               dbg_str("Unable to decrypt to TEMP1","");
               return FALSE;
            }
            pKey->kcp.init_done = 0;
            if (len != 32)
            {
               dbg_val("Length of second decrypted data != 32, = ", len);
               return chkerr(NTE_BAD_DATA);
            }

            // 6) CEKICV becomes 24 bytes of CEK and 8 bytes of ICV.
            CEKICV = TEMP1;
            CEK = CEKICV;
            ICV = CEKICV+24;

            // 7) Calculate the checksum of CEK (1st 8 bytes of SHA-1 hash) and check it is the same
            //    as ICV.
            if (!CPCreateHash(hProv, CALG_SHA1, 0, 0, &hICV))
            {
               dbg_str("Unable to create hash object for ICV", "");
               return FALSE;
            }
            if (!CPHashData(hProv, hICV, CEK, 24, 0))
            {
               dbg_str("Unable to hash LCEKPAD for ICV", "");
               return FALSE;
            }
            len = 20;
            if (!CPGetHashParam(hProv, hICV, HP_HASHVAL, newICV, &len, 0))
            {
               dbg_str("Unable to get ICV hash value","");
               return FALSE;
            }
            CPDestroyHash(hProv, hICV);
            if (memcmp(newICV, ICV, 8) != 0)
            {
               dbg_block("Two ICVs are not identical - stored ICV = ", ICV, 8);
               dbg_block("                         calculated ICV = ", newICV, 8);
               return chkerr(NTE_BAD_DATA);
            }
            if (!NFDES_CheckParity(CEK, 24))
            {
               dbg_str("Parity check failed on imported key material","");
               return chkerr(NTE_BAD_DATA);
            }
            if (!chkerr(MakeSymKey(pC, pSKB->blobhdr.aiKeyAlg, CEK, 24, 0, &pContent, 24*8)))
            {
               dbg_str("Key assembly failed","");
               return FALSE;
            }
            pContent->kcp.permissions = CRYPT_EXPORT | CRYPT_READ | CRYPT_WRITE | CRYPT_MAC |
                                        CRYPT_ENCRYPT | CRYPT_DECRYPT;
         }
         break;

      default:
         return chkerr(NTE_BAD_ALGID);
   }
   dbg_block("import-final-wrapped-key", pContent->key.ptr, pContent->key.len);

   CPDestroyKey(hProv, hKey);

   *hContent = (HCRYPTKEY)pContent;
   return TRUE;
}

/* ---------------------- */

static BOOL ImportOpaqueKey( PCONTAINER pC, const unsigned char *data, unsigned int len,
                             HCRYPTKEY *phkey, BOOL exportable )
{
   OPAQUE_BLOB_HEADER obh;
   KEY_OBJ impkey;
   KEY_OBJ *pK;
   HRESULT rc;

   ENTER("ImportOpaqueKey");
   if ( len < sizeof(OPAQUE_BLOB_HEADER) + sizeof(KEY_OBJ) )
      return chkerr(NTE_BAD_DATA);

   /* Check header */
   memcpy(&obh, data, sizeof(OPAQUE_BLOB_HEADER));
   data += sizeof(OPAQUE_BLOB_HEADER);

   if ( obh.magic != OPAQUE_BLOB_MAGIC )
      return chkerr(NTE_BAD_DATA);

   /* Get key body */

   memcpy(&impkey, data, sizeof(KEY_OBJ) );
   data += sizeof(KEY_OBJ);

   /* Now check fields */
   /* XXX: 3rd test commented out because schannel on Whistler is rounding up the blob lengths... */
   if ( impkey.tag != TAG_KEY_OBJ ||
        obh.blobhdr.aiKeyAlg != impkey.kcp.alg_type /*||
                                                      len != opaque_blob_len(&impkey)*/ )
   {
      dbg_val("impkey.tag = ", impkey.tag);
      dbg_val("TAG_KEY_OBJ = ", TAG_KEY_OBJ);
      dbg_val("obh.blobhdr.aiKeyAlg = ", obh.blobhdr.aiKeyAlg);
      dbg_val("impkey.kcp.alg_type = ", impkey.kcp.alg_type);
      dbg_val("len = ", len);
      dbg_val("opaque_blob_len(&impkey) = ", opaque_blob_len(&impkey));
      return chkerr(NTE_BAD_DATA);
   }

   pK=CreateKeyObj(pC, impkey.key.len, impkey.IV.len, impkey.fn_ctx.len, impkey.extra_ctx.len,
                   impkey.oaep_params.len, FALSE );

   if ( pK==NULL ) return chkerr(NTE_NO_MEMORY);

   /* Copy data fields over */

   if ( pK->key.len > 0 )
   {
      memcpy(pK->key.ptr, data, pK->key.len);
      data += pK->key.len;
   }

   if ( pK->IV.len > 0 )
   {
      memcpy(pK->IV.ptr, data, pK->IV.len);
      data += pK->IV.len;
   }

   if ( pK->fn_ctx.len > 0 )
   {
      memcpy(pK->fn_ctx.ptr, data, pK->fn_ctx.len);
      data += pK->fn_ctx.len;
   }

   if ( pK->extra_ctx.len > 0 )
   {
      memcpy(pK->extra_ctx.ptr, data, pK->extra_ctx.len);
      data += pK->extra_ctx.len;
   }

   /* Copy various control fields over */
   pK->kcp = impkey.kcp;
   if (exportable)
      pK->kcp.permissions |= CRYPT_EXPORTABLE;

   /* Set up function pointers. Note it's NOT safe just to put them in
      the blob, despite them being code addresses. The DLL may move, or
      the blob may survive between different versions of the DLL. This
      also provides some checking that the algorithm ID isn't totally
      cockeyed. */

   if (!(pK->kcp.alg_type & ALG_CLASS_HASH))
   {
      dbg_str("ImportOpaqueKey: algorithm isn't hash to put function ptrs in","");
      rc=InitFunctionPointers(pK);
      if ( rc != NTE_OP_OK )
      {
         DestroyKeyObj(pK);
         return chkerr(rc);
      }
   }
   else
   {
      dbg_str("ImportOpaqueKey: algorithm is hash so no function ptrs","");
   }

   dbg_val("ImportOpaqueKey: key algorithm = ",pK->kcp.alg_type);

   *phkey = (HCRYPTKEY)pK;
   return TRUE;
}


/* ---------------------- */

static BOOL ImportOpaqueNFKMKey( PCONTAINER pC, const unsigned char *data, unsigned int len, HCRYPTKEY *phkey )
{
   NFKMKEYBLOB *nfkmkeyblob = (NFKMKEYBLOB *)data;
   DWORD rc, expected_len;
   NFKM_WorldInfo *worldinfo = 0;
   NFKM_Key *nfkmkey = 0;
   NFKM_KeyIdent keyident, *newident = 0;
   NFKM_CardSet *cardset = 0;
   M_KeyMgmtFile kmfile;
   NF_Unmarshal_Context umc;
   NF_Free_Context fc;
   struct NF_UserData ud = UGS_INIT;
   M_Status st;
   KEY_OBJ *pKO = 0;

   ENTER("ImportOpaqueNFKMKey");

   /* Initialization might have failed */
   if(!global_pool.hApp) {
     rc = NTE_NO_MEMORY;
     goto error;
   }

   ud.app = global_pool.hApp;
   fc.u = &ud;
   FILLZERO(kmfile);

   if (len < sizeof(NFKMKEYBLOB))
   {
      dbg_printf("ImportOpaqueNFKMKey: input data too short.\n");
      rc = NTE_BAD_LEN;
      goto error;
   }

   /* some bits of the struct will have checked before we're called so we won't recheck I think */
   if ((nfkmkeyblob->publickeystruc.bVersion != CUR_BLOB_VERSION) || (nfkmkeyblob->publickeystruc.reserved != 0))
   {
      dbg_printf("Blobheader incorrect.\n");
      rc = NTE_BAD_TYPE;
      goto error;
   }
   /* Check magic number */
   if (nfkmkeyblob->rsapubkey.magic != OPAQUE_NFKMKEYBLOB_MAGIC)
   {
      dbg_printf("ImportOpaqueNFKMKey: Magic number incorrect.\n");
      rc = NTE_BAD_TYPE;
      goto error;
   }

   /* check lengths before we copy out the keyblob */
   expected_len = NFKMKEYBLOB_LEN(nfkmkeyblob->len);
   if (len != expected_len) {
      dbg_printf("ImportOpaqueNFKMKey: Received and expected data length mismatch.\n");
      dbg_val("len = ", len);
      dbg_val("expected_len = ", expected_len);
      rc = NTE_BAD_LEN;
      goto error;
   }

   /* And here we will validate any of the data we need to (i.e. hknso, cardset, etc) */
   if ((st = NFKM_getinfo(global_pool.hApp, &worldinfo, 0)) != Status_OK)
   {
      dbg_nf("ImportOpaqueNFKMKey: Unable to get Security World information.", st);
      rc = NTE_FAIL;
   }
   if ((rc = memcmp(nfkmkeyblob->hknso.bytes, worldinfo->hknso.bytes, sizeof(M_Hash))) != 0)
   {
      dbg_printf("ImportOpaqueNFKMKey: HKNSO of world not as expected.\n");
      dbg_block("World HKNSO", worldinfo->hknso.bytes, sizeof(M_Hash));
      dbg_block("Blob's HKNSO", nfkmkeyblob->hknso.bytes, sizeof(M_Hash));
      rc = NTE_FAIL;
      goto error;
   }

   if ((nfkmkeyblob->flags & NFKM_NKF_ProtectionCardSet) || (nfkmkeyblob->flags & NFKM_NKF_ProtectionModule))
   {
      if (nfkmkeyblob->flags & NFKM_NKF_ProtectionCardSet)
      {
         if (((st = NFKM_findcardset(global_pool.hApp, &nfkmkeyblob->cardset, &cardset, 0)) != Status_OK) || !cardset)
         {
            dbg_nf("ImportOpaqueNFKMKey: Unable to find required cardset", st);
            if (!st)
               dbg_printf("NFKM_findcardset, cardset = 0x%p.\n", cardset);
            rc = NTE_FAIL;
            goto error;
         }
      }
   }
   else
   {
      dbg_printf("ImportOpaqueNFKMKey: A known protection scheme was not found on the key.\n");
      dbg_val("nfkmkeyblob flags =", nfkmkeyblob->flags);
      rc = NTE_FAIL;
      goto error;
   }

   /* we need to unmarshal our blob data into a M_KeyMgmtFile struct and then
    * we can have the NFKM library turn it into a NFKM_Key for us.
    */
   umc.ip = nfkmkeyblob->blob;
   umc.remain = nfkmkeyblob->len;
   umc.u = &ud;
   if ((st = NF_Unmarshal_KeyMgmtFile(&umc, &kmfile)) != Status_OK)
   {
      dbg_nf("ImportOpaqueNFKMKey: Unmarshalling error data", st);
      rc = NTE_FAIL;
      goto error;
   }
   if (umc.remain)
   {
      dbg_printf("Still have leftover bytes from unmarrshalling.  Unexpected.");
      rc = NTE_FAIL;
      goto error;
   }
   if ((st = nfkm__loadkey_kmdata(global_pool.hApp, kmfile, &nfkmkey, 0, NULL)) != Status_OK)
   {
      dbg_nf("ImportOpaqueNFKMKey: Error loading key into memory", st);
      rc = NTE_FAIL;
      goto error;
   }

   if ((rc = memcmp(nfkmkeyblob->hash.bytes, nfkmkey->hash.bytes, sizeof(M_Hash))) != 0)
   {
      dbg_printf("ImportOpaqueNFKMKey: Keyhash mismatch");
      dbg_block("Opaque blob keyhash", nfkmkeyblob->hash.bytes, sizeof(M_Hash));
      dbg_block("NFKM blob keyhash", nfkmkey->hash.bytes, sizeof(M_Hash));
      rc = NTE_FAIL;
      goto error;
   }

   /* Getting here all should be okay and we know the key blob was good.
    * Next need to check name is unique, set bits for current container, etc.
    */
   keyident.appname = nfkmkey->appname;
   keyident.ident = nfkmkey->ident;

   if ((st = NextAvailableIdent(global_pool.hApp, keyident, &newident, 0)) != Status_OK)
   {
      dbg_nf("ImportOpaqueNFKMKey: Unable to set a new ident for key", st);
      rc = NTE_FAIL;
      goto error;
   }

   nfkmkey->ident = newident->ident;
   if ((st = NFKM_recordkey(global_pool.hApp, nfkmkey, 0)) != Status_OK)
   {
      dbg_nf("ImportOpaqueNFKMKey: Unable to save key to disk", st);
      rc = NTE_FAIL;
      goto error;
   }

   if ((rc = SetKeyIdent(pC->cinfo, algid2keynum(nfkmkeyblob->publickeystruc.aiKeyAlg), newident, 0)) != NTE_OP_OK)
   {
      dbg_printf("ImportOpaqueNFKMKey: Unable to re-association key with container.\n");
      if ((st = NFKM_forgetkey(global_pool.hApp, *newident, 0)) != Status_OK)
      {
         dbg_printf("ImportOpaqueNFKMKey: Unable to delete file 'key_%s_%s'.\n",
                    newident->appname, newident->ident);
         dbg_nf("ImportOpaqueNFKMKey: NFKM_forgetkey returned", st);
      }
      goto error;
   }

   if ((pKO = GetUserKey(pC, algid2keynum(nfkmkeyblob->publickeystruc.aiKeyAlg), FALSE)) == NULL)
   {
      dbg_printf("ImportOpaqueNFKMKey: Unable to load imported key.\n");
      rc = GetLastError();
      goto error;
   }
   *phkey = (HCRYPTKEY)pKO;

error:
   /* clean up bits */
   if (newident)
   {
      /* points us back to the ident malloced by NFKM */
      nfkmkey->ident = keyident.ident;
      /* free newident and members */

   }
   if (nfkmkey) NFKM_freekey(global_pool.hApp, nfkmkey, 0);
   if (cardset) NFKM_freecardset(global_pool.hApp, cardset, 0);
   if (worldinfo) NFKM_freeinfo(global_pool.hApp, &worldinfo, 0);
   NF_Free_KeyMgmtFile(&fc, &kmfile);

   if (rc != NTE_OP_OK)
   {
      dbg_val("ImportOpaqueNFKMKey: NCSP Error, rc =", rc);
      SetLastError(rc);
      return FALSE;
   }
   else
      return TRUE;
}

/* ---------------------- */

static BOOL ExportWrappedKey( HCRYPTPROV hProv, PKEY_OBJ pContent, HCRYPTKEY hExportKey, unsigned char *pbData, DWORD *pdwDataLen)
{
   PKEY_OBJ pKey;
   HCRYPTKEY hKey;
   PSIMPLE_KEY_BLOB pSKB;
   BYTE BlobBuf[MAX_BLOB_SIZE];

   ENTER("ExportWrappedKey");
   dbg_str("Export of symmetric/symmetric key","");

   // we need to copy the key because we reset the IV twice, and don't want to do it on the original.
   if (!CPDuplicateKey(hProv, hExportKey, NULL, 0, &hKey))
   {
      dbg_val("Unable to copy the export key, last error = ",GetLastError());
      return FALSE;
   }

   pKey = ValidateKey( hProv, hKey, CRYPT_READ, "used to encrypt an WINAPI keyblob");

   if (pKey == NULL)
      return chkerr(NTE_NO_KEY);

   if (pContent->kcp.alg_type != pKey->kcp.alg_type)
   {
      dbg_str("Sym/sym algorithms not identical","");
      return chkerr(NTE_BAD_ALGID);
   }

   dbg_val("init_done for export key = ", pKey->kcp.init_done);

   pSKB = (PSIMPLE_KEY_BLOB) BlobBuf;
   pSKB->blobhdr.bType = SYMMETRICWRAPKEYBLOB;
   pSKB->blobhdr.bVersion = CUR_BLOB_VERSION;
   pSKB->blobhdr.reserved = 0;
   pSKB->blobhdr.aiKeyAlg = pKey->kcp.alg_type;
   pSKB->alg_id = pContent->kcp.alg_type;

   // my goodness, these _are_ complicated. There's also lots of code duplication, which I've kept in
   // for ease of debugging (so I can see the variables as they appear in the spec. etc) but which
   // could be combined into a higher-level switch statement.

   switch (pContent->kcp.alg_type)
   {
      case CALG_3DES:
         {
            BYTE CEK[24];          // key-encryption key material.

            BYTE ICV[20];          // checksum on the key material (only need 1st 8 bytes)
            HCRYPTHASH hICV;       // hash to calculate above
            int len;

            BYTE CEKICV[32];       // concatenation of above 2.

            BYTE IV[8];            // input vector  for 1st encryption.
            int IVlen;             // length of IV expected...
            DWORD keymode = CRYPT_MODE_CBC;

            BYTE TEMP1[40];        // result of first encryption. Won't be more than 40.
            BYTE TEMP23[40];       // TEMP2 and TEMP3.

            dbg_str("Wrapping DES3 key with DES3 key","");

            if (pbData == NULL)
            {
               *pdwDataLen = 40+SIMPLE_HEADER_SIZE;
               dbg_val("Get-size call, returned size = ", *pdwDataLen);
               return TRUE;
            }

            // 1) Set odd parity for each of the DES key octets comprising the content-encryption key,
            //    call the result CEK.
            if (pContent->key.len != 24)
            {
               dbg_val("Len of 3DES key material != 24, = ", pContent->key.len);
               return chkerr(NTE_BAD_LEN);
            }
            memcpy(CEK, pContent->key.ptr, pContent->key.len);
            NFDES_SetParity(CEK, 24);

            // 2) Compute an 8 octet key checksum value [1st 8 bytes of a SHA-1 hash] on CEK, call
            // the result ICV.
            if (!CPCreateHash(hProv, CALG_SHA1, 0, 0, &hICV))
            {
               dbg_str("Unable to create hash object for ICV", "");
               return FALSE;
            }
            if (!CPHashData(hProv, hICV, CEK, 24, 0))
            {
               dbg_str("Unable to hash CEK for ICV", "");
               return FALSE;
            }
            len = 20;
            if (!CPGetHashParam(hProv, hICV, HP_HASHVAL, ICV, &len, 0))
            {
               dbg_str("Unable to get ICV hash value","");
               return FALSE;
            }
            CPDestroyHash(hProv, hICV);

            // 3) Let CEKICV = CEK || ICV.
            memcpy(CEKICV, CEK, 24);
            memcpy(CEKICV+24, ICV, 8);

            // 4) Generate 8 octets at random call the result IV.
            if (!NFast_GenerateRandom(IV, 8))
            {
               dbg_str("Unable to generate random IV","");
               return FALSE;
            }

            // 5) Encrypt CEKICV in CBC mode using the key-encryption key. Use the IV generated in the
            // previous step. Call the ciphertext TEMP1.
            if (!CPGetKeyParam(hProv, hKey, KP_IV, NULL, &IVlen, 0))
            {
               dbg_str("Unable to get IV len","");
               return FALSE;
            }
            if (IVlen != 8)
            {
               dbg_val("IV length for 3DES key returned != 8, = ", IVlen);
               return chkerr(NTE_BAD_DATA);
            }
            if (!CPSetKeyParam(hProv, hKey, KP_IV, IV, 0))
            {
               dbg_str("Unable to set first IV value","");
               return FALSE;
            }
            if (!CPSetKeyParam(hProv, hKey, KP_MODE, (unsigned char *)(&keymode), 0))
               // does default to this, but just to make sure...
            {
               dbg_str("Unable to set key mode to CBC","");
               return FALSE;
            }
            memcpy(TEMP1, CEKICV, 32);
            len = 32;
            // no FINAL flag in next function - don't want to add padding at the end.
            if (!CPEncrypt(hProv, hKey, 0, FALSE, 0, TEMP1, &len, 40))
            {
               dbg_str("Unable to encrypt to CEKICV","");
               return FALSE;
            }
            pKey->kcp.init_done = 0;
            if (len != 32)
            {
               dbg_val("Length of first encrypted data != 32, = ", len);
               return chkerr(NTE_BAD_DATA);
            }

            // 6) Let TEMP2 = IV || TEMP1.
            memcpy(TEMP23, IV, 8);
            memcpy(TEMP23+8, TEMP1, 32);

            // 7) Reverse the order of the bytes in TEMP2. Call the result TEMP3.
            reverse_bytes(TEMP23, TEMP23, 40);

            // 8) Encrypt TEMP3 in CBC mode using 0x4adda22c79e82105 as the IV. The ciphertext is 40 bytes long.
            if (!CPSetKeyParam(hProv, hKey, KP_IV, CMSIV, 0))
            {
               dbg_str("Unable to set second IV value","");
               return FALSE;
            }
            len = 40;
            if (!CPEncrypt(hProv, hKey, 0, FALSE, 0, TEMP23, &len, 40))
            {
               dbg_str("Unable to encrypt TEMP3","");
               return FALSE;
            }
            pKey->kcp.init_done = 0;
            if (len != 40)
            {
               dbg_val("Length of second encrypted data != 40, = ", len);
               return chkerr(NTE_BAD_DATA);
            }

            memcpy(pSKB->keyblk, TEMP23, 40);
            *pdwDataLen = 40+SIMPLE_HEADER_SIZE;
         }
         break;

      default:
         {
            dbg_val("Unsupported algorithm for wrapped keys - only RC4 and 3DES supported. Algorithm was ",
                    pKey->kcp.alg_type);
            return NTE_BAD_ALGID;
         }
   }
   memcpy(pbData, pSKB, *pdwDataLen);
   CLEAR(BlobBuf, *pdwDataLen);
   return TRUE;
}

/* ---------------------- */

static BOOL ExportOpaqueKey ( PKEY_OBJ pK, unsigned char *ptr, DWORD *pdwDataLen )
{
   unsigned len;
   OPAQUE_BLOB_HEADER obh;

   ENTER("ExportOpaqueKey");
   /* Our export is very unsubtle, just a memory copy.
      This should probably be MAC'd & encrypted with a global key */

   len = opaque_blob_len(pK);

   /* Handle 'length only' calls */

   if ( ptr == NULL )
   {
      *pdwDataLen = len;
      return TRUE;
   }
   else if ( len > *pdwDataLen )
   {
      *pdwDataLen = len;
      return chkerr(ERROR_MORE_DATA);
   }

   obh.blobhdr.bType=OPAQUEKEYBLOB;
   obh.blobhdr.bVersion=CUR_BLOB_VERSION;
   obh.blobhdr.reserved=0;
   obh.blobhdr.aiKeyAlg=pK->kcp.alg_type;
   obh.magic=OPAQUE_BLOB_MAGIC;

   memcpy(ptr, &obh, sizeof(OPAQUE_BLOB_HEADER));
   ptr += sizeof(OPAQUE_BLOB_HEADER);

   memcpy(ptr, pK, sizeof(KEY_OBJ) );
   ptr += sizeof(KEY_OBJ);

   if ( pK->key.len > 0 )
   {
      memcpy(ptr, pK->key.ptr, pK->key.len);
      ptr += pK->key.len;
   }

   if ( pK->IV.len > 0 )
   {
      memcpy(ptr, pK->IV.ptr, pK->IV.len);
      ptr += pK->IV.len;
   }

   if ( pK->fn_ctx.len > 0 )
   {
      memcpy(ptr, pK->fn_ctx.ptr, pK->fn_ctx.len);
      ptr += pK->fn_ctx.len;
   }

   if ( pK->extra_ctx.len > 0 )
   {
      memcpy(ptr, pK->extra_ctx.ptr, pK->extra_ctx.len);
      ptr += pK->extra_ctx.len;
   }

   *pdwDataLen = len;
   return TRUE;
}

/* ---------------------- */

static BOOL ExportOpaqueNFKMKey ( PKEY_OBJ pK, unsigned char *ptr, DWORD *pdwDataLen )
{
   unsigned int len, pubhdrlen;
   NFKMKEYBLOB *nfkmblob = (NFKMKEYBLOB *)ptr;
   DWORD rc = NTE_FAIL;
   int keynum = algid2keynum(pK->kcp.alg_type);
   NFKM_Key *key = 0;
   NFKM_KeyIdent *keyident = 0;
   M_Status st;
   NFKM_WorldInfo *world = 0;
   char *filename = 0;
   HANDLE file = INVALID_HANDLE_VALUE;
   DWORD filesz = 0, filesz_read;

   ENTER("ExportOpaqueNFKMKey");

   /* Initialization might have failed */
   if(!global_pool.hApp) {
     rc = NTE_NO_MEMORY;
     goto error;
   }
   if (pK->kcp.flags & (KF_HASCOUNTER | KF_KITB))
   {
     /* error and return, as it's got key counting */
     dbg_printf("ExportOpaqueNFKMKey: Key has key counting enabled, or is a KITB key.  "
                "It is not able to be exported.\n");
     rc = NTE_BAD_KEY;
     goto error;
   }

   if (!GetKeyFile(pK, keynum, &key))
   {
     rc = NTE_NO_KEY;
     goto error;
   }

   /* determine length needed for file buffer */
   if (rc = GetKeyIdent(pK->container->cinfo, keynum, &keyident, NULL))
   {
      dbg_printf("ExportOpaqueNFKMKey: Unable to get key ident.\n");
      goto error;
   }
   if ((filename = GetIdentFilename(keyident)) == NULL)
   {
      dbg_printf("ExportOpaqueNFKMKey: Unable to determine full filename for key_%s_%s.\n",
                 keyident->appname, keyident->ident);
      rc = NTE_FAIL;
      goto error;
   }
   if ((file = CreateFile(filename, GENERIC_READ, FILE_SHARE_READ, NULL,
                          OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL)) == INVALID_HANDLE_VALUE)
   {
      dbg_printf("ExportOpaqueNFKMKey: Unable to open file %s.\n", filename);
      rc = GetLastError();
      goto error;
   }
   if ((filesz = GetFileSize(file, NULL)) == INVALID_FILE_SIZE)
   {
      dbg_printf("ExportOpaqueNFKMKey: Unable to determine size of file %s.\n", filename);
      rc = GetLastError();
      goto error;
   }

   /* check 'ptr' is long enough */
   len = NFKMKEYBLOB_LEN(filesz);
   if ( ptr == NULL )
   {
      *pdwDataLen = len;
      rc = NTE_OP_OK;
      goto error;
   }
   else if ( len > *pdwDataLen )
   {
      *pdwDataLen = len;
      rc = ERROR_MORE_DATA;
      goto error;
   }

   if (!ReadFile(file, nfkmblob->blob, filesz, &filesz_read, 0))
   {
      dbg_printf("ExportOpaqueNFKMKey: Error reading bytes from file.\n");
      rc = GetLastError();
      goto error;
   }
   if (filesz != filesz_read)
   {
      dbg_printf("ExportOpaqueNFKMKey: Did not read expected number of bytes from file.\n");
      rc = NTE_BAD_LEN;
      goto error;
   }

   nfkmblob->publickeystruc.bType = OPAQUEKEYBLOB;
   nfkmblob->publickeystruc.bVersion = CUR_BLOB_VERSION;
   nfkmblob->publickeystruc.reserved = 0;
   nfkmblob->publickeystruc.aiKeyAlg = pK->kcp.alg_type;

   if (!NFast_ExportKeys(pK, NEK_JUSTHEADER, (unsigned char *)nfkmblob, pK->kcp.blk_size_bits, &pubhdrlen, 0))
   {
     dbg_printf("ExportOpaqueNFKMKey: Unable to export public key data.\n");
     rc = NTE_FAIL;
     goto error;
   }

   /* We'll set the magic number for our new blob type here */
   nfkmblob->rsapubkey.magic = OPAQUE_NFKMKEYBLOB_MAGIC;

   if ((st = NFKM_getinfo(global_pool.hApp, &world, 0)) != Status_OK)
   {
      dbg_nf("ExportOpaqueNFKMKey: Unable to get Security World information", st);
      rc = NTE_FAIL;
      goto error;
   }

   memcpy(&nfkmblob->hknso, &world->hknso, sizeof(M_Hash));
   nfkmblob->flags = key->flags;
   if (key->flags & NFKM_NKF_ProtectionCardSet)
     memcpy(&nfkmblob->cardset, &key->cardset, sizeof(NFKM_CardSetIdent));
   memcpy(&nfkmblob->hash, &key->hash, sizeof(M_Hash));
   nfkmblob->len = filesz;

   /* We got to the end, success. */
   rc = NTE_OP_OK;
   *pdwDataLen = len;

error:
   /* clean up stuff */
   if (filename) free(filename);
   if (file != INVALID_HANDLE_VALUE) CloseHandle(file);
   if (keyident) FreeIdent(keyident);
   if (key) NFKM_freekey(global_pool.hApp, key, 0);
   if (world) NFKM_freeinfo(global_pool.hApp, &world, NULL);

   if (rc != NTE_OP_OK)
   {
      dbg_val("ExportOpaqueNFKMKey: NCSP Error =", rc);
      SetLastError(rc);
      return FALSE;
   }
   else
      return TRUE;
}

/* WINAPI functions: hash stuff -------------------------------------------- */

NFRC4_CONTEXT random_context;
unsigned int random_len;
char random_data[256];

#define MAX_RANDOM 32767

void GenRandom_setup()
{
   ENTER("GenRandom_Setup");
   random_len = 0;
   NFast_GenerateRandom(random_data, 256);
   NFRC4_SetKey(&random_context, random_data, 256);
}

BOOL WINAPI CPGenRandom ( HCRYPTPROV hProv, DWORD dwLen, unsigned char *pbBuffer )
{
   PCONTAINER pC;
   char random_result[256];
   int noofbytesleft;
   unsigned char *pbBufferNew = pbBuffer;

   ENTER("CPGenRandom");
   check_init();

   dbg_ptrf("CPGenRandom: hProv=", (void*)hProv);
   dbg_val ("            length=", dwLen);
   dbg_val ("        random_len=", random_len);

   pC = ValidateProv ( hProv );
   if ((pC == NULL) && (GetLastError() != NTE_NOT_FOUND))
      return FALSE; // doesn't fail if the nFast is not present.
   else
      pC = (PCONTAINER) hProv; // grotty, I know, to de-abstract this

   if (random_len > MAX_RANDOM)
   {
      dbg_str("Too much random data generated, re-seeding RC4 key", "");
      GenRandom_setup();
   }

   noofbytesleft = dwLen;
   while (noofbytesleft > 0)
   {
      int nooftocopy = (noofbytesleft > 256) ? 256 : noofbytesleft;
      NFRC4_EncryptDecrypt(&random_context, random_data, random_result, 256, 0);
      //dbg_block("Random block generated: ", random_result, 256);
      memcpy(pbBufferNew, random_result, nooftocopy);
      //dbg_val("Copying bytes across, bytes copied = ", nooftocopy);
      noofbytesleft -= nooftocopy;
      pbBufferNew += nooftocopy;
      random_len += nooftocopy;
      memcpy(random_data, random_result, 256);
   }
   //dbg_block("Random data: ", pbBuffer, dwLen);

   ReleaseProv(hProv);

   return TRUE;
}

/* ----------------------------------- */

BOOL CPCreateHashNoLink(PCONTAINER pC, ALG_ID alg_id, HCRYPTHASH *phHash, BYTE *preallocated )
{
   PHASH_OBJ pHO;

   ENTER("CPCreateHashNoLink");
   if (!chkerr(MakeHash(pC, alg_id, 0, &pHO, preallocated)))
   {
      return FALSE;
   }

   pHO->permissions = CRYPT_READ | CRYPT_WRITE;
   pHO->valueset = 0;
   *phHash = (HCRYPTHASH) pHO;
   //dbg_vale("Created unlinked hash object ", (int)pHO);

   return TRUE;
}

BOOL WINAPI CPCreateHash ( HCRYPTPROV hProv, ALG_ID alg_id, HCRYPTKEY hKey,
			     DWORD dwFlags, HCRYPTHASH *phHash )
{
   PHASH_OBJ pHO;
   PKEY_OBJ  pKO;
   PCONTAINER pC;

   ENTER("CpCreateHash");
    check_init();

   dbg_ptrf("CPCreateHash: hProv=", (void*)hProv);
   dbg_val ("             alg_id=",alg_id);
   dbg_ptr ("                key=", (void*)hKey);
   dbg_val ("              flags=",dwFlags);

   pC = ValidateProv ( hProv );
   if ( pC == NULL )
      return FALSE;

   if ( dwFlags != 0 )
   {
      ReleaseProv(hProv);
      return chkerr (NTE_BAD_FLAGS);
   }

   if ( hKey )
   {
      pKO=ValidateKey(hProv, hKey, CRYPT_READ, "encrypt a hash");
      if ( pKO==NULL )
      {
         ReleaseProv(hProv);
         return FALSE;
      }
      LockKey(hKey);
   }
   else
   {
      pKO=NULL;
   }


   if ( !chkerr(MakeHash ( pC, alg_id, pKO, &pHO, NULL )) )
   {
      ReleaseProv(hProv);
      UnlockKey(hKey);
      return FALSE;
   }

   UnlockKey(hKey);

   pHO->permissions = CRYPT_READ | CRYPT_WRITE;
   pHO->valueset = 0;
   *phHash = (HCRYPTHASH) pHO;

   ReleaseProv(hProv);
   return TRUE;
}

/* --------------------------------------- */

BOOL WINAPI CPDuplicateHash( HCRYPTPROV hProv, HCRYPTHASH hHash,
			       DWORD *pdwReserved, DWORD dwFlags, HCRYPTHASH *phHash )
{
   PCONTAINER pC;
   PHASH_OBJ pHO,newpHO;

   ENTER("CPDuplicateHash");
   check_init();

   /* TODO: Consider replacing most of this function with a call to MakeHash (in symalgs.c) */

   dbg_ptrf("CPDuplicateHash: hProv=", (void*)hProv);
   dbg_ptr ("                  hash=", (void*)hHash);

   pC = ValidateProv(hProv);
   if (pC == NULL)
   {
      return FALSE;
   }

   pHO = ValidateHash(hProv, hHash, 0);
   if (pHO == NULL)
   {
      ReleaseProv(hProv);
      return FALSE;
   }

   if ((pdwReserved != NULL) || (dwFlags != 0))
   {
      ReleaseProv(hProv);
      return chkerr(NTE_BAD_FLAGS);
   }

   newpHO = CreateHashObj((PCONTAINER) hProv,
                          pHO->hashctx.len,pHO->hashval.len, NULL);
   if (newpHO == NULL)
   {
      ReleaseProv(hProv);
   }

   memcpy(newpHO->hashctx.ptr,pHO->hashctx.ptr,pHO->hashctx.len);

   newpHO->alg_type = pHO->alg_type;
   newpHO->permissions = pHO->permissions;
   newpHO->finalised = pHO->finalised;
   newpHO->hashfn = pHO->hashfn;

   // extra processing needed for keyed hashes...
   if (pHO->alg_type == CALG_MAC)
   {
      if (!MAC_Duplicate(hProv, (PMAC_STATE) (pHO->hashctx.ptr), (PMAC_STATE) (newpHO->hashctx.ptr)))
         return FALSE;
   }
   else if (pHO->alg_type == CALG_HMAC)
   {
      if (!HMAC_Duplicate(hProv, (PHMAC_STATE) (pHO->hashctx.ptr), (PHMAC_STATE) (newpHO->hashctx.ptr)))
         return FALSE;
      if (pHO->hashval.ptr)
      {
         newpHO->hashval.ptr = malloc(pHO->hashval.len);
         if (newpHO->hashval.ptr == NULL)
            return FALSE;
         newpHO->hashval.len = pHO->hashval.len;
      }
   }

   if (newpHO->hashval.ptr)
   {
      memcpy(newpHO->hashval.ptr,pHO->hashval.ptr,pHO->hashval.len);
   }

   *phHash = (HCRYPTHASH) newpHO;

   ReleaseProv(hProv);
   return TRUE;
}

/* ------------------------ */

BOOL WINAPI CPGetHashParam ( HCRYPTPROV hProv,
			       HCRYPTHASH hHash,
			       DWORD dwParam,
			       unsigned char *pbData,
			       DWORD *pdwDataLen,
			       DWORD dwFlags )
{
   void *copy_ptr;
   DWORD copy_len;

   PHASH_OBJ pHO;
   PCONTAINER pC;

   ENTER("CPGetHashParam");
   check_init();

   dbg_ptrf("CPGetHashParam: hProv=", (void*)hProv);
   dbg_val ("                param=",dwParam);

   pC = ValidateProv(hProv);
   if (pC == NULL)
   {
      return FALSE;
   }


   pHO = ValidateHash ( hProv, hHash, CRYPT_READ );
   if ( pHO == NULL )
   {
      ReleaseProv(hProv);
      return FALSE;
   }

   if ( dwFlags != 0 )
   {
      ReleaseProv(hProv);
      return chkerr(NTE_BAD_FLAGS);
   }

   switch ( dwParam )
   {
      case HP_ALGID:
         dbg_val("    Alg ID=",pHO->alg_type);
         copy_ptr = &(pHO->alg_type);
         copy_len = 4;
         break;

      case HP_HASHSIZE:
         dbg_val("    Alg length=",pHO->hashval.len);
         copy_ptr = &(pHO->hashval.len);
         copy_len = 4;
         break;

      case HP_HASHVAL:
         if (pHO->alg_type == CALG_TLS1PRF)
         {
            /* Here we bypass the normal copy_ptr stuff because the size of reply
             * we want depends exactly on the value of pdwDataLen passed in. */
            if (!GetTLSFinishedParam(pC, pHO, pbData, *pdwDataLen))
            {
               ReleaseProv(hProv);
               return chkerr(NTE_BAD_HASH_STATE);
            }
            else
            {
               ReleaseProv(hProv);
               return TRUE;
            }
         }
         else
         {
            FinaliseHash(pHO);
            dbg_block("Hash value",pHO->hashval.ptr,pHO->hashval.len);
            copy_ptr = pHO->hashval.ptr;
            /* possibility of above being NULL if we have SCHANNEL_MASTER_HASH. */
            copy_len = pHO->hashval.len;
         }
         break;

      case HP_HMAC_INFO:
         if (pHO->alg_type != CALG_HMAC)
         {
            ReleaseProv(hProv);
            return chkerr(NTE_BAD_HASH);
         }

         ReleaseProv(hProv);
         return chkerr(NTE_BAD_TYPE);
         /* for now... might implement this later. In fact, there's no entry in the API for
            reading this value, only setting it. */
         break;


      default:
         ReleaseProv(hProv);
         return chkerr(NTE_BAD_TYPE);
   }

   if ( pbData == NULL ) /* Get-size call */
   {
      *pdwDataLen = copy_len;
      ReleaseProv(hProv);
      return TRUE;
   }
   else if ( *pdwDataLen < copy_len )
   {
      *pdwDataLen = copy_len;
      ReleaseProv(hProv);
      return chkerr(ERROR_MORE_DATA);
   }
   if (copy_ptr == NULL) // probably an SCHANNEL_MASTER_HASH caught at the wrong time.
   {
      ReleaseProv(hProv);
      dbg_str("copy_ptr was null in GetHashParam","");
      return chkerr(NTE_BAD_HASH_STATE);
   }

   memcpy ( pbData, copy_ptr, copy_len );
   *pdwDataLen = copy_len;
   ReleaseProv(hProv);
   return TRUE;
}

/* ------------------------ */

BOOL WINAPI CPSetHashParam ( HCRYPTPROV hProv,
			       HCRYPTHASH hHash,
			       DWORD dwParam,
			       const BYTE *pbData,
			       DWORD dwFlags )
{
   PCONTAINER pC;
   PHASH_OBJ pHO;

   ENTER("CPSetHashParam");
   check_init();

   dbg_ptrf("CPSetHashParam: hProv=", (void*)hProv);
   dbg_val ("                param=",dwParam);

   pC = ValidateProv(hProv);
   if (pC == NULL)
   {
      return FALSE;
   }

   pHO = ValidateHash ( hProv, hHash, CRYPT_WRITE );
   if ( pHO == NULL )
   {
      ReleaseProv(hProv);
      return FALSE;
   }

   if ( dwFlags != 0 || pbData == NULL )
   {
      ReleaseProv(hProv);
      return chkerr(NTE_BAD_FLAGS);
   }

   switch ( dwParam )
   {
      case HP_HASHVAL:
         dbg_block("Old hash value",pHO->hashval.ptr,pHO->hashval.len);
         /*pHO->finalised = 1; No! This is only set _after_ signing or otherwise _using_ the value */
         if (pHO->hashval.ptr == NULL) {
           ReleaseProv(hProv);
           return chkerr(NTE_BAD_HASH);
         }
         memcpy( pHO->hashval.ptr, pbData, pHO->hashval.len );
         pHO->valueset = 1;
         dbg_block("New hash value", pHO->hashval.ptr, pHO->hashval.len);
         break;

      case HP_HMAC_INFO:
         if (pHO->alg_type != CALG_HMAC)
         {
            ReleaseProv(hProv);
            return chkerr(NTE_BAD_HASH);
         }

         if (!HMAC_Setup( pHO, (PHMAC_INFO) pbData))
         {
            ReleaseProv(hProv);
            return FALSE;
         }
         break;

      case HP_TLS1PRF_LABEL:
      case HP_TLS1PRF_SEED:
         if (pHO->alg_type != CALG_TLS1PRF)
         {
            ReleaseProv(hProv);
            return chkerr(NTE_BAD_HASH);
         }
         if (!SetTLSFinishedParam(pHO, dwParam, (CRYPT_DATA_BLOB *)pbData))
         {
            ReleaseProv(hProv);
            return chkerr(NTE_NO_MEMORY); // only error condition is running out of memory...
         }
         break;

      default:
         ReleaseProv(hProv);
         return chkerr(NTE_BAD_TYPE);
   }

   ReleaseProv(hProv);
   return TRUE;
}

/* ************************************ */

/* Dialogue-box control functions. */

extern BOOL CentreWindow(HWND whandle);


INT_PTR APIENTRY PassPhraseDialogProc(HWND handle, UINT message, WPARAM param, LPARAM outputp)
{
   int commandID;
   static char **sarray = NULL;

   ENTER("PassPhraseDialogProc");
   switch (message)
   {
      case WM_INITDIALOG:
         //SetDlgItemText(handle,VALUE,"Default password");
         sarray = (char **)outputp;
         CentreWindow(handle);
         break;

      case WM_DESTROY:
         break;

      case WM_COMMAND:
         commandID = LOWORD(param);
         switch (commandID)
         {
            case IDOK:
               assert(sarray != NULL);
               GetDlgItemText(handle,TEXT1,sarray[0],255);
               GetDlgItemText(handle,TEXT2,sarray[1],255);
               EndDialog(handle,1);
               return TRUE;
               break;

            case IDCANCEL:
               EndDialog(handle,0);
               return TRUE;
               break;
         }
         break;
   }

   return FALSE;
}

/* ******************************************* */

BOOL WINAPI CPHashData ( HCRYPTPROV hProv,
			   HCRYPTHASH hHash,
			   const BYTE *pbData,
			   DWORD dwDataLen,
			   DWORD dwFlags )
{
   PCONTAINER pC;
   PHASH_OBJ pHO;
   char passphrase[255];
   const BYTE *hashdata;

   ENTER("CPHashData");
   check_init();

   dbg_ptrf("CPHashData: hProv=", (void*)hProv);
   dbg_ptr ("             hash=", (void*)hHash);

   pC = ValidateProv(hProv);
   if (pC == NULL)
   {
      return FALSE;
   }


   pHO = ValidateHash ( hProv, hHash, CRYPT_WRITE );
   if ( pHO == NULL )
   {
      ReleaseProv(hProv);
      return FALSE;
   }

   if ((dwFlags != 0) && !(dwFlags & CRYPT_USERDATA))
   {
      ReleaseProv(hProv);
      return chkerr(NTE_BAD_FLAGS);
   }

   if ((pbData == NULL ) && !(dwFlags & CRYPT_USERDATA))
      // only allowed NULL data ptr if we're inputting data via the dialog box.
   {
      ReleaseProv(hProv);
      return chkerr(ERROR_INVALID_PARAMETER);
   }

   if ( pHO->finalised )
   {
      ReleaseProv(hProv);
      return chkerr(NTE_BAD_HASH_STATE);
   }

   if (dwFlags & CRYPT_USERDATA)
   {
      /* get a pass-phrase to hash from the user */
      INT_PTR dboxreturn;
      char checkphrase[255];
      char *stringarray[2];
      BOOL gotpassphrase = FALSE;
      HANDLE hinst = LoadLibrary("ncspmess.dll");

      if (hinst == NULL)
      {
         dbg_vall("Unable to load the nCipher CSP message DLL. Last error was ",GetLastError());
      }

      if (pC->flags & CF_SILENT)
      {
         ReleaseProv(hProv);
         return chkerr(NTE_SILENT_CONTEXT);
      }

      stringarray[0] = passphrase;
      stringarray[1] = checkphrase;

      while (gotpassphrase == FALSE)
      {
         dboxreturn = DialogBoxParam(hinst, MAKEINTRESOURCE(ENTER_HASH), NULL, PassPhraseDialogProc,
                                     (LPARAM) stringarray);

         if (dboxreturn == -1)
         {
            /* system error with dialogue box */
            ReleaseProv(hProv);
            return chkerr(GetLastError());
         }
         if (dboxreturn == 0)
         {
            /* user clicked cancel/pressed escape */
            ReleaseProv(hProv);
            return chkerr(NTE_BAD_DATA);
         }
         /* check two passphrases are identical */
         if (strcmp(passphrase,checkphrase) != 0)
         {
            if (MessageBox(NULL,"The two pass-phrases are different - please re-enter.",
                           "nCipher Cryptography",
                           MB_OK | MB_ICONWARNING | MB_SETFOREGROUND) != IDOK)
            {
               ReleaseProv(hProv);
               return chkerr(GetLastError());
            }
         }
         /* next line introduces new flag, from newcsp.h. We probably want a better method
          * of stopping users using foolish passwords. */
         else if ((strcmp(passphrase,"") == 0) && (dwFlags & CRYPT_NEEDPASSPHRASE))
         {
            if (MessageBox(NULL,"You have entered an empty pass-phrase - please re-enter.",
                           "nCipher Cryptography",
                           MB_OK | MB_ICONWARNING | MB_SETFOREGROUND) != IDOK)
            {
               ReleaseProv(hProv);
               return chkerr(GetLastError());
            }
         }
         else if ((strcmp(passphrase,"") == 0))
         {
            switch (MessageBox(NULL,"You have entered an empty pass-phrase - are you sure?",
                               "nCipher Cryptography",
                               MB_YESNO | MB_ICONQUESTION | MB_SETFOREGROUND))
            {
               case IDYES:
                  gotpassphrase = TRUE;
                  break;

               case IDNO:
                  break;

               default:
                  ReleaseProv(hProv);
                  return chkerr(GetLastError());
            }
         }
         else
         {
            gotpassphrase = TRUE;
         }
      }
      hashdata = passphrase;
      dwDataLen = (DWORD)strlen(passphrase);
   }
   else
      hashdata = pbData;


   /* Check for CALG_SSL3_SHAMD5
      "Once a handle to the hash is obtained then the hash
      value is set with the CryptSetHashParam call.
      Calls to CryptHashData will fail with a handle
      to a hash of this type."  */
   if ( pHO->hashfn == NULL )
   {
      ReleaseProv(hProv);
      return chkerr(NTE_BAD_ALGID);
   }

   dbg_block("CPHashData: data to hash", hashdata, dwDataLen);
   if ( dwDataLen > 0 )
   {
      pHO->hashfn ( pHO->hashctx.ptr, hashdata, dwDataLen );
   }

   ReleaseProv(hProv);
   return TRUE;
}

/* ------------------------ */

BOOL WINAPI CPHashSessionKey ( HCRYPTPROV hProv,
				 HCRYPTHASH hHash,
				 HCRYPTKEY hKey,
				 DWORD dwFlags )
{
   int i;

   PHASH_OBJ pHO;
   PKEY_OBJ pKO;
   unsigned char masterblk[48];
   HRESULT rc;
   PCONTAINER pC;

   unsigned char *buff;
   unsigned int length;

   ENTER("CPHashSessionKey");

   check_init();

   dbg_ptrf("CPHashSessionKey: hProv=", (void*)hProv);
   dbg_ptr ("                   hash=", (void*)hHash);
   dbg_ptr ("                    key=", (void*)hKey);

   pC = ValidateProv(hProv);
   if (pC == NULL)
   {
      return FALSE;
   }
   pHO = ValidateHash ( hProv, hHash, CRYPT_WRITE );
   if ( pHO == NULL )
   {
      ReleaseProv(hProv);
      return FALSE;
   }

   pKO = ValidateKey ( hProv, hKey, CRYPT_READ, "hashed");
   if ( pKO == NULL )
   {
      ReleaseProv(hProv);
      return FALSE;
   }
   LockKey(hKey);

   if (( dwFlags != CRYPT_LITTLE_ENDIAN) && (dwFlags != 0) )
   {
      ReleaseProv(hProv);
      UnlockKey(hKey);
      return chkerr(NTE_BAD_FLAGS);
   }

   if ( pHO->finalised )
   {
      ReleaseProv(hProv);
      UnlockKey(hKey);
      return chkerr(NTE_BAD_HASH_STATE);
   }

   if ( pHO->hashfn == NULL )
   {
      ReleaseProv(hProv);
      UnlockKey(hKey);
      return chkerr(NTE_BAD_ALGID);
   }

   if ( (pKO->kcp.flags & (KF_SCHANNEL_KEY|KF_SCHANNEL_MAC|KF_BLK_CIPHER|KF_STREAM_CIPHER)) == 0 )
   {
      ReleaseProv(hProv);
      UnlockKey(hKey);
      return chkerr(NTE_BAD_TYPE);
   }

   switch (pKO->kcp.alg_type)
   {
      case CALG_SSL3_MASTER:
      case CALG_TLS1_MASTER:
         dbg_str("Session key is a master key - getting master secret","");

         rc = GetMasterSecret(pKO, masterblk);
         if (rc != NTE_OP_OK)
         {
            dbg_val("Error from master secret fetch",rc);
            UnlockKey(hKey);
            ReleaseProv(hProv);
            return rc;
         }
         buff = masterblk;
         length = 48;
         dbg_block("Master secret = ",masterblk,48);
         break;

      default:
         dbg_str("Session key is a normal key - using key value","");
         buff = pKO->key.ptr;
         length = pKO->kcp.key_secret_len;
   }


   if ( dwFlags & CRYPT_LITTLE_ENDIAN )
   {
      pHO->hashfn(pHO->hashctx.ptr, buff, length );
   }
   else /* Normal usage: Hash bytes in reverse order (perverse, non?) */
   {
      i=length;
      while ( i-- > 0 )
         pHO->hashfn ( pHO->hashctx.ptr, buff+i, 1 );
   }

   UnlockKey(hKey);

   ReleaseProv(hProv);
   return TRUE;
}

/* ------------------------------------------ */

BOOL CPDestroyHashNoLink(HCRYPTHASH hHash)
{

   ENTER("CPDestoyHashNoLink");
   //dbg_vale("Destroying unlinked hash object ", (int)hHash);
   DestroyHashObj((PHASH_OBJ)hHash);
   //dbg_vale("Destroyed unlinked hash object ", (int)hHash);
   return TRUE;
}

BOOL WINAPI CPDestroyHash ( HCRYPTPROV hProv, HCRYPTHASH hHash )
{
   PHASH_OBJ pHO;
   PCONTAINER pC;

   ENTER("CPDestoyHash");
   check_init();

   dbg_ptrf("CPDestroyHash: hProv=", (void*)hProv);
   dbg_ptr ("                hash=", (void*)hHash);

   pC = ValidateProv(hProv);
   if (pC == NULL)
   {
      return FALSE;
   }


   pHO = ValidateHash ( hProv, hHash, 0 );
   if ( pHO == NULL )
   {
      ReleaseProv(hProv);
      return FALSE;
   }

   DestroyHashObj( pHO );
   ReleaseProv(hProv);
   return TRUE;
}


/* WINAPI functions: key stuff --------------------------------------------- */

BOOL WINAPI CPDeriveKey ( HCRYPTPROV hProv, ALG_ID Algid, HCRYPTHASH hBaseData, DWORD dwFlags,
                            HCRYPTKEY *phKey )
{
#ifdef SIGONLY
   return chkerr(NTE_BAD_PROV_TYPE);
#else  /* not SIGONLY */

   PCONTAINER pC;
   PKEY_OBJ pK = NULL;
   PHASH_OBJ pH;
   unsigned int bitlen;
   const PROV_ENUMALGS_EX *palgs_ex;

   ENTER("CPDeriveKey");
   check_init();

   dbg_ptrf("CPDeriveKey: hProv=", (void*)hProv);
   dbg_val ("             algid=",Algid);
   dbg_ptr ("         hBaseData=", (void*)hBaseData);
   dbg_val ("             flags=",dwFlags);

   pC = ValidateProv(hProv);
   if ( pC == NULL )
      return FALSE;

   if (GET_ALG_CLASS(Algid) != ALG_CLASS_MSG_ENCRYPT) // only check lengths on non-SSL key types
   {
      for ( palgs_ex=our_algs_ex; palgs_ex->aiAlgid != CALG_LASTALGONLIST; palgs_ex++ )
      {
         if ( palgs_ex->aiAlgid == Algid )
            goto found_it;
      }
      ReleaseProv(hProv);
      return chkerr(NTE_BAD_ALGID);
   }

found_it:
   pH = ValidateHash(hProv,hBaseData, CRYPT_READ);
   if ( pH == NULL )
   {
      ReleaseProv(hProv);
      return FALSE;
   }

   if (phKey == NULL)
   {
      ReleaseProv(hProv);
      return chkerr(ERROR_INVALID_PARAMETER);
   }

   /* See comments in CPGenKey */
   bitlen = (dwFlags & KEY_LENGTH_MASK) >> 16;

   if (GET_ALG_CLASS(Algid) != ALG_CLASS_MSG_ENCRYPT)
   {
      if (bitlen != 0)
      {
         if ((bitlen < palgs_ex->dwMinLen) || (bitlen > palgs_ex->dwMaxLen))
         {
            ReleaseProv(hProv);
            return chkerr(ERROR_INVALID_PARAMETER);
         }
      }
      else
         bitlen = palgs_ex->dwDefaultLen;
   }

   /* Since KEYOBJ.key_secret_len is in bytes, the bit
      length better be a multiple of 8 (we assume
      throughout that bytes are octets) */
   if ( (bitlen % 8) != 0 )
   {
      ReleaseProv(hProv);
      return ERROR_INVALID_PARAMETER;
   }

   if (dwFlags & CRYPT_UPDATE_KEY)
   {
      pK = (PKEY_OBJ) *phKey;
      if (pK == NULL)
      {
         dbg_str("Unable to get existing pK from *phKey", "");
         ReleaseProv(hProv);
         return chkerr(ERROR_INVALID_PARAMETER);
      }
   }

   if (dwFlags & CRYPT_USER_PROTECTED)
   {
      if (pC->flags & CF_SILENT)
      {
         ReleaseProv(hProv);
         return chkerr(NTE_SILENT_CONTEXT);
      }

      if (MessageBox(NULL,"A user key is about to be created","User key creation",
                     MB_OK | MB_ICONINFORMATION | MB_SETFOREGROUND) != IDOK)
      {
         ReleaseProv(hProv);
         return FALSE;
      }
   }

   FinaliseHash ( pH );

   switch ( GET_ALG_CLASS(Algid) )
   {
      case ALG_CLASS_DATA_ENCRYPT:
         if (pH->hashval.ptr == NULL) { /* 'cos of SCHANNEL_MASTER */
           ReleaseProv(hProv);
           return chkerr(NTE_BAD_HASH);
         }

         if (dwFlags & CRYPT_UPDATE_KEY)
         {
            if (!chkerr(UpdateSymKey(pH->hashval.ptr, pH->hashval.len, pK)))
            {
               ReleaseProv(hProv);
               return FALSE;
            }
         }
         else
         {
            /*Largest key is AES 256...this size is still good though.*/
            BYTE byteblock[256];
            unsigned int byteblocksize = (palgs_ex->dwMaxLen) >> 3;
            BOOL needtoexpand = FALSE;

            memset(byteblock, 0, 256);

            dbg_printf("Key alg = 0x%x, hash alg = 0x%x.\n",
                       Algid, pH->alg_type);
            dbg_printf("Checking for expand-a-hash requirement: max. key size = %d, hash size = %d.\n",
                       byteblocksize,  pH->hashval.len);

            if (byteblocksize > pH->hashval.len)
            {
               dbg_printf("Need to expand, max, key size > hash size.\n");
               needtoexpand = TRUE;
            }
            if ((Algid == CALG_AES_128) && (pH->hashval.len <= 20))
            {
               /* I think this is a bug in the MS provider. I don't
                * understand why it seems to want expansion for 16 and 20
                * byte hashes; AES128 keys are 16 bytes long. */
               dbg_printf("Need to expand - workaround for AES128.\n");
               needtoexpand = TRUE;
            }

            if (needtoexpand)
            {
               // need to do some magic to expand small hash for large key: see
               // http://discuss.microsoft.com/SCRIPTS/WA-MSD.EXE?A2=ind9911c&L=cryptoapi&F=&S=&P=528
               // for details (with reference to 3DES keys specifically,
               // but also relevant for AES now.)

               char datatohash[64]; // max. hash length atm = 20 bytes.
               HCRYPTHASH temphash;
               DWORD hashlen;

               // Hash no. 1: XOR hash value with block of 0x36
               memset(datatohash, 0x36, 64);
               xorblock(datatohash, datatohash, pH->hashval.ptr, pH->hashval.len);
               if (!CPCreateHash(hProv, pH->alg_type, 0, 0, &temphash))
               {
                  dbg_str("Unable to create first temp. hash for expanding hash derivation input", "");
                  return FALSE;
               }
               if (!CPHashData(hProv, temphash, datatohash, 64, 0))
               {
                  dbg_str("Unable to hash first data block for expanding hash derivation input", "");
                  return FALSE;
               }
               hashlen = pH->hashval.len;
               if (!CPGetHashParam(hProv, temphash, HP_HASHVAL, byteblock, &hashlen, 0))
               {
                  dbg_str("Unable to get first hash value for expanding hash derivation input", "");
                  return FALSE;
               }
               CPDestroyHash(hProv, temphash);

               // Hash no. 2: XOR hash value with block of 0x5c
               memset(datatohash, 0x5c, 64);
               xorblock(datatohash, datatohash, pH->hashval.ptr, pH->hashval.len);
               if (!CPCreateHash(hProv, pH->alg_type, 0, 0, &temphash))
               {
                  dbg_str("Unable to create first temp. hash for expanding hash derivation input", "");
                  return FALSE;
               }
               if (!CPHashData(hProv, temphash, datatohash, 64, 0))
               {
                  dbg_str("Unable to hash first data block for expanding hash derivation input", "");
                  return FALSE;
               }
               hashlen = pH->hashval.len;
               if (!CPGetHashParam(hProv, temphash, HP_HASHVAL, byteblock+pH->hashval.len, &hashlen, 0))
               {
                  dbg_str("Unable to get first hash value for expanding hash derivation input", "");
                  return FALSE;
               }
               CPDestroyHash(hProv, temphash);

               dbg_block("Expanded block input: ", byteblock, 2*(pH->hashval.len));
            }
            else
            {
               /* byteblocksize <= hashlen; if it were more we would do the
                * expand-a-hash bit. */
               memcpy(byteblock, pH->hashval.ptr, byteblocksize);
               dbg_block("Not expanded block input: ", byteblock, byteblocksize);
            }

            if ( !chkerr(MakeSymKey ( pC, Algid, byteblock, byteblocksize,
                                      dwFlags & 0xffff, &pK, bitlen ) ) )
            {
               ReleaseProv(hProv);
               return FALSE;
            }
         }


         pK->kcp.permissions = ((dwFlags & CRYPT_EXPORTABLE) ? CRYPT_EXPORT : 0)
         | CRYPT_READ | CRYPT_WRITE | CRYPT_MAC | CRYPT_ENCRYPT | CRYPT_DECRYPT;
         break;

      case ALG_CLASS_MSG_ENCRYPT:
         if (dwFlags & CRYPT_UPDATE_KEY)
         {
            /* don't even want to think about writing update-key code for SSL. Bleeagh. */
            ReleaseProv(hProv);
            return chkerr(NTE_BAD_FLAGS);
         }
         dbg_printf("We're deriving an SSL key. pH = 0x%x.\n", pH);

         if ( !chkerr(DeriveSSLKey(pC, Algid, pH, dwFlags, &pK)) )
         {
            ReleaseProv(hProv);
            return FALSE;
         }

         pK->kcp.permissions = ((dwFlags & CRYPT_EXPORTABLE) ? CRYPT_EXPORT : 0)
         | CRYPT_READ | CRYPT_WRITE | CRYPT_ENCRYPT | CRYPT_DECRYPT;
         break;

      default:
         ReleaseProv(hProv);
         return chkerr(NTE_BAD_ALGID);
   }

   if (dwFlags & CRYPT_USER_PROTECTED)
   {
      pK->kcp.flags |= KF_USER_WARN;
   }

   //*phKey = (HCRYPTKEY)pK;

   (PKEY_OBJ)(*phKey) = ValidateKey(hProv, (HCRYPTKEY)pK, CRYPT_READ, "debugged");

   dbg_ptrf("       new phKey=", (void*)*phKey);
   ReleaseProv(hProv);
   return TRUE;

#endif /* SIGONLY */
}

/* ------------------------------------------ */

BOOL getlengths(ALG_ID algid, unsigned int *minlen, unsigned int *maxlen, unsigned int *defaultlen)
{
   const PROV_ENUMALGS_EX *palgs_ex;

   ENTER("getlengths");
   for ( palgs_ex=our_algs_ex; palgs_ex->aiAlgid != CALG_LASTALGONLIST; palgs_ex++ )
   {
      if ( palgs_ex->aiAlgid == algid )
         goto found_it;
   }
   return FALSE;

found_it:
   *minlen = palgs_ex->dwMinLen;
   *maxlen = palgs_ex->dwMaxLen;
   *defaultlen = palgs_ex->dwDefaultLen;
   return TRUE;
}


BOOL WINAPI CPGenKey ( HCRYPTPROV hProv, ALG_ID Algid, DWORD dwFlags, HCRYPTKEY *phKey )
{
   PCONTAINER pC;
   PKEY_OBJ pK = NULL;
   unsigned int newperm;
   unsigned int bitlen;
   //M_ACL privACL, pubACL;
   const PROV_ENUMALGS_EX *palgs_ex;

   ENTER("CPGenKey");
   check_init();

   dbg_ptrf("CPGenKey: hProv=", (void*)hProv);
   dbg_val ("          algid=", Algid);
   dbg_val ("          flags=", dwFlags);

   pC = ValidateProv(hProv);
   if ( pC == NULL )
      return FALSE;

#ifdef RSACSP
   if (Algid==AT_SIGNATURE) Algid = CALG_RSA_SIGN;
   if (Algid==AT_KEYEXCHANGE) Algid = CALG_RSA_KEYX;
#else
   if (Algid==AT_SIGNATURE) Algid = CALG_DSS_SIGN;
   if (Algid==AT_KEYEXCHANGE) Algid = CALG_DH_SF;
#endif

   if (GET_ALG_CLASS(Algid) != ALG_CLASS_MSG_ENCRYPT) // don't include SSL algs on list
   {
      for ( palgs_ex=our_algs_ex; palgs_ex->aiAlgid != CALG_LASTALGONLIST; palgs_ex++ )
      {
         if ( palgs_ex->aiAlgid == Algid )
            goto found_it;
      }
      ReleaseProv(hProv);
      return chkerr(NTE_BAD_ALGID);
   }

found_it:

   // NB the CRYPT_EXPORT flag is _always_ set. We always allow keys to be exportable
   // (but only with admin cards).
   newperm = CRYPT_READ | CRYPT_WRITE | CRYPT_EXPORT | CRYPT_ENCRYPT | CRYPT_DECRYPT;

   bitlen = (dwFlags & KEY_LENGTH_MASK) >> 16;

   /* Since KEYOBJ.key_secret_len is in bytes, the bit
      length better be a multiple of 8 (we assume
      throughout that bytes are octets) */
   if ( (bitlen % 8) != 0 )
   {
      ReleaseProv(hProv);
      return chkerr(NTE_BAD_LEN);
   }

   if (GET_ALG_CLASS(Algid) != ALG_CLASS_MSG_ENCRYPT)
   {
      if (bitlen != 0)
      {
         if ((bitlen < palgs_ex->dwMinLen) || (bitlen > palgs_ex->dwMaxLen))
         {
            dbg_val("Bit length a bad no. of bits - requested bit length = ", bitlen);
            dbg_val("                                 minimum bit length = ", palgs_ex->dwMinLen);
            dbg_val("                                 maximum bit length = ", palgs_ex->dwMaxLen);
            ReleaseProv(hProv);
            return chkerr(ERROR_INVALID_PARAMETER);
         }
      }
      else
         bitlen = palgs_ex->dwDefaultLen;
   }
   dbg_val("           bitlen=", bitlen);

   if ( Algid == CALG_RSA_KEYX || Algid == CALG_RSA_SIGN ||
        Algid == CALG_DSS_SIGN || Algid == CALG_DH_SF || Algid == CALG_DH_EPHEM)
   {
      if (pC->flags & CF_VERIFY_ONLY)
      {
         dbg_printf("Private key generation not permitted with a VERIFY_CONTEXT\n");
         return chkerr(NTE_PERM);
      }
      if (!GenKey(pC, Algid, dwFlags, bitlen, newperm, &pK))
      {
         ReleaseProv(hProv);
         return FALSE;
      }
      else
      {
         *phKey = (HCRYPTKEY) pK;
         ReleaseProv(hProv);

         dbg_val("GenKey generated RSA pair: private key = ", pK->nfastprivhandle);
         dbg_val("                            public key = ", pK->nfastpubhandle);

         return TRUE;
      }
   }

#ifdef SIGONLY

   dbg_strl("An attempt was made to create a symmetric encryption key using the signature-only CSP","");
   ReleaseProv(hProv);
   return chkerr(NTE_BAD_PROV_TYPE);

#else /* not SIGONLY */

   switch ( GET_ALG_CLASS(Algid) )
   {
      case ALG_CLASS_DATA_ENCRYPT:
         if ( !chkerr( MakeSymKey(pC, Algid, NULL, 0,
                                  dwFlags & (CRYPT_CREATE_SALT | CRYPT_NO_SALT), &pK, bitlen) ))
         {
            ReleaseProv(hProv);
            return FALSE;
         }

         pK->kcp.permissions = newperm | CRYPT_ENCRYPT | CRYPT_DECRYPT | CRYPT_MAC;
         dbg_block("New key (secret part only)", pK->key.ptr, pK->kcp.key_secret_len);
         break;

      case ALG_CLASS_MSG_ENCRYPT:
         if ( !chkerr( MakeSSLKey(pC, Algid, NULL, 0, dwFlags, &pK ) ))
         {
            ReleaseProv(hProv);
            return FALSE;
         }
         pK->kcp.permissions = newperm;
         break;

      default:
         ReleaseProv(hProv);
         return chkerr(NTE_BAD_ALGID);
   }

   if (dwFlags & CRYPT_USER_PROTECTED)
   {
      if (pC->flags & CF_SILENT)
      {
         DestroyKeyObj(pK);
         ReleaseProv(hProv);
         return chkerr(NTE_SILENT_CONTEXT);
      }

      if (MessageBox(NULL,"A user key is about to be created","User key creation",
                     MB_OK | MB_ICONINFORMATION | MB_SETFOREGROUND) != IDOK)
      {
         DestroyKeyObj(pK);
         ReleaseProv(hProv);
         return FALSE;
      }
      else
      {
         pK->kcp.flags |= KF_USER_WARN;
      }
   }

   *phKey = (HCRYPTKEY)pK;

   dbg_ptrf( "      new phKey=", (void*)*phKey);

   ReleaseProv(hProv);
   return TRUE;

#endif /* SIGONLY */

}

/* ------------------------------------------ */

BOOL WINAPI CPDuplicateKey( HCRYPTPROV hProv, HCRYPTKEY hKey,
			      DWORD *pdwReserved, DWORD dwFlags, HCRYPTKEY *phKey )
{
   PKEY_OBJ pKO,newpKO;
   PCONTAINER pC;

   ENTER("CPDuplicateKey");
   check_init();

   dbg_ptrf("CPDuplicateKey: hProv=", (void*)hProv);
   dbg_ptr ("                  key=", (void*)hKey);

   pC = ValidateProv(hProv);
   if (pC == NULL)
      return FALSE;

   pKO = ValidateKey(hProv, hKey, 0, "duplicated");
   if (pKO == NULL)
   {
      ReleaseProv(hProv);
      return FALSE;
   }

   LockKey(hKey);

   if ((pdwReserved != NULL) || (dwFlags != 0))
   {
      UnlockKey(hKey);
      ReleaseProv(hProv);
      return chkerr(NTE_BAD_FLAGS);
   }

   newpKO = CreateKeyObj((PCONTAINER) hProv,
                         pKO->key.len,pKO->IV.len,pKO->fn_ctx.len,pKO->extra_ctx.len,
                         pKO->oaep_params.len,
                         pKO->global);
   if (newpKO == NULL)
   {
      UnlockKey(hKey);
      ReleaseProv(hProv);
      return chkerr(NTE_NO_MEMORY);
   }

   memcpy(newpKO->key.ptr,pKO->key.ptr,pKO->key.len);
   memcpy(newpKO->IV.ptr,pKO->IV.ptr,pKO->IV.len);
   memcpy(newpKO->fn_ctx.ptr,pKO->fn_ctx.ptr,pKO->fn_ctx.len);
   memcpy(newpKO->extra_ctx.ptr,pKO->extra_ctx.ptr,pKO->extra_ctx.len);

   newpKO->kcp = pKO->kcp;
   newpKO->fns = pKO->fns;

   /* RSA key - need to duplicate the keys on the unit if they're not permanent. */
   if ((pKO->nfastprivhandle) && (pKO->kcp.flags & KF_PERMANENT))
   {
      // keep the same token ID.
      newpKO->nfastprivhandle = pKO->nfastprivhandle;
      newpKO->nfastpubhandle = pKO->nfastpubhandle;
      newpKO->multilock = pKO->multilock;
   }
   else if (pKO->nfastpubhandle) // only bother duplicating if we're some sort of nFast key.
   {
      // we'll need a new token ID for this: one token per key only.
      if (!NFast_DuplicateKey(newpKO, pKO))
      {
         UnlockKey(hKey);
         DestroyKeyObj(newpKO);
         ReleaseProv(hProv);
         return chkerr(GetLastError());
      }
   }

   *phKey = (HCRYPTKEY) newpKO;

   dbg_ptrf("           new hKey=", (void*)*phKey);

   UnlockKey(hKey);
   ReleaseProv(hProv);
   return TRUE;
}


/* ------------------------------------------ */


BOOL WINAPI CPGetKeyParam ( HCRYPTPROV hProv, HCRYPTKEY hKey,
			      DWORD dwParam, unsigned char *pbData, DWORD *pdwDataLen,
			      DWORD dwFlags )
{
   PCONTAINER pC;
   PKEY_OBJ pKO;
   DWORD len_out;
   DWORD length;
   void *copy_ptr;
   BYTE keyblob[MAX_BLOB_SIZE];
   ULARGE_INTEGER keycounter_temp;

   ENTER("CPGetKeyParam");
   check_init();

   dbg_ptrf("CPGetKeyParam: hProv=", (void*)hProv);
   dbg_ptr ("                 key=", (void*)hKey);
   dbg_val ("               param=",dwParam);

   memset(keyblob, 0, MAX_BLOB_SIZE);

   pC = ValidateProv(hProv);
   if (pC == NULL)
   {
      return FALSE;
   }

   if ( dwFlags != 0 )
   {
      ReleaseProv(hProv);
      return chkerr(NTE_BAD_FLAGS);
   }

   pKO = ValidateKey(hProv, hKey, CRYPT_READ, "interrogated");
   if ( pKO == NULL )
   {
      ReleaseProv(hProv);
      return FALSE;
   }
   LockKey(hKey);

   switch ( dwParam )
   {
      case KP_ALGID:
         len_out = 4;
         copy_ptr = &(pKO->kcp.alg_type);
         dbg_val("Alg id = ", pKO->kcp.alg_type);
         break;

      case KP_BLOCKLEN:
         len_out = 4;
         copy_ptr = &(pKO->kcp.blk_size_bits);
         dbg_val("Block len = ", pKO->kcp.blk_size_bits);
         break;

      case KP_KEYLEN:
         len_out = 4;
         length = pKO->kcp.key_secret_len * 8;
         copy_ptr = &(length);
         dbg_val("Key len = ", length);
         break;

      case KP_SALT:
         dbg_str("Salt follows: ","");
         if ((pKO->kcp.alg_type == CALG_AES_128) ||
             (pKO->kcp.alg_type == CALG_AES_192) ||
             (pKO->kcp.alg_type == CALG_AES_256))
            goto type_error;

         len_out = pKO->kcp.salt_len;
         copy_ptr = pKO->key.ptr + pKO->kcp.key_secret_len;
         break;

      case KP_PERMISSIONS:
         len_out = 4;
         copy_ptr = &(pKO->kcp.permissions);
         dbg_val("Permissions = ", pKO->kcp.permissions);
         break;

      case KP_EFFECTIVE_KEYLEN:
         if ((pKO->kcp.alg_type != CALG_DES) &&
             (pKO->kcp.alg_type != CALG_3DES_112) &&
             (pKO->kcp.alg_type != CALG_3DES))
            goto type_error;

         len_out = 4;
         length = pKO->kcp.effective_len_bits;
         copy_ptr = &(length);
         dbg_val("Effective key length = ", length);
         break;

      case KP_IV:
         dbg_str("IV follows: ","");
         if ( (pKO->kcp.flags & KF_BLK_CIPHER) == 0 )
         {
            len_out = 0;
            copy_ptr = pKO->IV.ptr;
         }

         len_out = pKO->IV.len;
         copy_ptr = pKO->IV.ptr;
         break;

      case KP_PADDING:
         //if ( (pKO->kcp.flags & KF_BLK_CIPHER) == 0 )
         //   goto type_error;

         len_out = 4;
         copy_ptr = &(pKO->kcp.pad_mode);
         dbg_val("Padding = ", pKO->kcp.pad_mode);
         break;

      case KP_MODE:
         //if ( (pKO->kcp.flags & KF_BLK_CIPHER) == 0 )
         //   goto type_error;

         len_out = 4;
         copy_ptr = &(pKO->kcp.block_mode);
         dbg_val("Cipher mode = ", pKO->kcp.block_mode);
         break;

      case KP_MODE_BITS:
         //if ( (pKO->kcp.flags & KF_BLK_CIPHER) == 0 )
         //   goto type_error;

         len_out = 4;
         copy_ptr = &(pKO->kcp.feedback_len_bits);
         dbg_val("Mode bits = ", pKO->kcp.feedback_len_bits);
         break;

#ifdef DSACSP
      case KP_P:
      case KP_Q:
      case KP_G:
         if ((pKO->kcp.alg_type != CALG_DSS_SIGN) &&
             (pKO->kcp.alg_type != CALG_DH_SF) &&
             (pKO->kcp.alg_type != CALG_DH_EPHEM))
            goto type_error;

         if (!CheckPKReady(pKO))
            return chkerr(NTE_BAD_KEY_STATE);

         if (!GetDDKeyParam(pKO, dwParam, keyblob, MAX_BLOB_SIZE, &len_out, &copy_ptr))
            return FALSE;
         break;
#endif

#define KP_GET_USE_COUNT 42
      case KP_GET_USE_COUNT:
         if (!(pKO->kcp.flags & KF_HASCOUNTER) || !(pKO->kcp.flags & KF_PERMANENT ))
         {
            dbg_printf("Wrong type of key for a key counter; pKO->kcp.flags = 0x%x.\n",
                       pKO->kcp.flags);
            goto type_error;
         }
         len_out = sizeof(ULARGE_INTEGER);
         if (pbData != NULL)
         {
            if (!NFast_GetNVRAMCounter(pKO, &keycounter_temp))
            {
               UnlockKey(hKey);
               ReleaseProv(hProv);
               return chkerr(NTE_NOT_FOUND);
            }
            copy_ptr = &keycounter_temp;
         }
         break;

#ifdef RSACSP
     case KP_OAEP_PARAMS:
         if ( (pKO->kcp.flags & KF_OAEP_PARAMS) == 0 )
         {
            len_out = 0;
            copy_ptr = pKO->oaep_params.ptr;
         }

         len_out = pKO->oaep_params.len;
         copy_ptr = pKO->oaep_params.ptr;
         break;
#endif

      default:
         goto type_error;
   }

   if ( pbData == NULL ) /* Get-size call */
   {
      *pdwDataLen = len_out;
      dbg_val("... get-len call, length returned = ", len_out);
      UnlockKey(hKey);
      ReleaseProv(hProv);
      return TRUE;
   }
   else if ( *pdwDataLen < len_out )
   {
      *pdwDataLen = len_out;
      dbg_val("... not enough space, space required = ", len_out);
      UnlockKey(hKey);
      ReleaseProv(hProv);
      return chkerr ( ERROR_MORE_DATA );
   }

   if ( len_out > 0 )
   {
      dbg_block("... returned data = ", copy_ptr, len_out);
      memcpy ( pbData, copy_ptr, len_out );
   }

   *pdwDataLen = len_out;
   UnlockKey(hKey);
   ReleaseProv(hProv);
   return TRUE;

type_error:
   *pdwDataLen = 0;
   UnlockKey(hKey);
   ReleaseProv(hProv);
   return chkerrval ( NTE_BAD_TYPE, dwParam );
}

/* ------------------------------------------ */

BOOL WINAPI CPSetKeyParam ( HCRYPTPROV hProv, HCRYPTKEY hKey,
			      DWORD dwParam, const BYTE *pbData, DWORD dwFlags )
{
   PKEY_OBJ pKO;
   PCONTAINER pC;
   BOOL result;

   ENTER("CPSetKeyParam");
   check_init();

   dbg_ptrf("CPSetKeyParam: hProv = ", (void*)hProv);
   dbg_ptr ("                 key = ", (void*)hKey);
   dbg_val ("              param  = ", dwParam);
   dbg_val ("             dwFlags = ", dwFlags);

   pC = ValidateProv(hProv);
   if (pC == NULL)
   {
      return FALSE;
   }

   if ((pbData == NULL) && (dwParam != KP_X) && (dwParam != KP_OAEP_PARAMS))
   {
      ReleaseProv(hProv);
      return chkerr ( ERROR_INVALID_PARAMETER );
   }
   // there used to be a check for flags being non-0 as well, but some undocumented
   // parameter that IIS uses seems to use a non-0 flag, and it was blatting the error
   // log on a SSL soak.

   pKO = ValidateKey(hProv, hKey, CRYPT_WRITE, "changed");
   if ( pKO == NULL )
   {
      ReleaseProv(hProv);
      return FALSE;
   }
   LockKey(hKey);

   if ((pKO->kcp.alg_type == CALG_AGREEDKEY_ANY) && (dwParam == KP_ALGID))
   {
      ALG_ID algid;

      algid = *((DWORD *) pbData);
      result = InitDHSharedSecret(pKO, algid, dwFlags);
   }
   else
   {
      switch ( GET_ALG_CLASS(pKO->kcp.alg_type) )
      {
         case ALG_CLASS_DATA_ENCRYPT:
            result = chkerr(SetCryptKeyParam(pKO,dwParam,pbData));
            break;

         case ALG_CLASS_MSG_ENCRYPT:
            result = chkerr(SetSSLKeyParam(pKO,dwParam,pbData));
            break;

         case ALG_CLASS_SIGNATURE:
         case ALG_CLASS_KEY_EXCHANGE:
            result = chkerr(SetPubKeyParam(pKO, dwParam, pbData));
            break;

         default:
            result = chkerr(NTE_BAD_TYPE);
            break;
      }
   }

   UnlockKey(hKey);
   ReleaseProv(hProv);
   return result;
}

/* ------------------------------------------ */

BOOL encrypt_one_block(BYTE *srcdst, unsigned int *dstlen, PCONTAINER pC,
                       PKEY_OBJ pKO, DWORD dwFlags)
{
   unsigned int modlen = pKO->kcp.blk_size_bits >> 3;
   unsigned int startlen = *dstlen;
   unsigned int lenrestrict;
   BOOL oaep;

   ENTER("encrypt_one_block");
   if ( (dwFlags & CRYPT_OAEP) == CRYPT_OAEP )
   {
     lenrestrict = 42;
     oaep = TRUE;
   }
   else
   {
     lenrestrict = 11;
     oaep = FALSE;
   }

   if ( (startlen) > (modlen-lenrestrict) ) // *dstlen starts life as length of data minus padding.
   {
      dbg_str("encrypt_one_block: *dstlen too large at start","");
      return chkerr(NTE_BAD_DATA);
   }

   /*if ( !build_pkcs1_block ( pC, 0x02, srcdst, *dstlen, modlen ) )
   {
      dbg_str("encrypt_one_block: build_pcks1_block failed","");
      return chkerr(NTE_FAIL);
   }*/

   *dstlen = modlen;

   if (!(NFast_RSA_Encrypt(srcdst, startlen, srcdst, dstlen, pKO,
                           oaep, (pKO->kcp.flags & KF_OAEP_PARAMS) ? &pKO->oaep_params : NULL)))
   {
      dbg_str("encrypt_one_block, NFast_RSA_Encrypt failed","");
      return chkerr(NTE_FAIL);
   }

   return TRUE;
}



BOOL WINAPI CPEncrypt ( HCRYPTPROV hProv,
			  HCRYPTKEY hKey,
			  HCRYPTHASH hHash,
			  BOOL final,
			  DWORD dwflags,
			  unsigned char *pbdata,
			  DWORD *pdwDataLen,
			  DWORD dwBufLen )
{
   PKEY_OBJ pKO;
   PCONTAINER pC;
   unsigned int lenrestrict;

   ENTER("CPEncrypt");
   check_init();

   dbg_ptrf("CPEncrypt: hProv=", (void*)hProv);
   dbg_ptr ("             key=", (void*)hKey);
   dbg_val ("           final=",final);
   dbg_val ("           flags=",dwflags);
   if (pdwDataLen) dbg_val("   *pdwDataLen=",*pdwDataLen);

   pC = ValidateProv(hProv);
   if (pC == NULL)
      return FALSE;

   pKO = ValidateKey(hProv, hKey, CRYPT_ENCRYPT, "used to encrypt something");
   if ( pKO == NULL )
   {
      ReleaseProv(hProv);
      return FALSE;
   }
   LockKey(hKey);

   dbg_val("     init_done=", pKO->kcp.init_done);

   if (pKO->kcp.alg_type == CALG_AGREEDKEY_ANY)
   {
      dbg_str("DH secret key not initialised yet", "");
      ReleaseProv(hProv);
      return chkerr(NTE_BAD_KEY_STATE);
   }

   /* Hash input data */

   if ( (hHash != (HCRYPTHASH) 0) && (pbdata != NULL) ) /* not a get-size call */
   {
      if ( !CPHashData(hProv, hHash, pbdata, *pdwDataLen, 0 ) )
      {
         UnlockKey(hKey);
         ReleaseProv(hProv);
         return FALSE;
      }
   }

   if ( pKO->kcp.flags & (KF_BLK_CIPHER | KF_STREAM_CIPHER) )
   {
      if ( !pKO->kcp.init_done )
      {
         if ( !chkerr( InitSymKey(pKO) ) )
         {
            UnlockKey(hKey);
            ReleaseProv(hProv);
            return FALSE;
         }
         pKO->kcp.init_done = 1;
      }

      if ( !chkerr(SymEncryptDecrypt ( pKO, pbdata, pdwDataLen, dwBufLen, final, 0 ) ) )
      {
         UnlockKey(hKey);
         ReleaseProv(hProv);
         return FALSE;
      }

      if ( final )
         pKO->kcp.init_done = 0;

      UnlockKey(hKey);
      ReleaseProv(hProv);
      return TRUE;
   }

   /* encrypt_one_block assumes we're _not_ doing a get-size call. Sort all this out
    * in this 'ere procedure. */

   /* Now we allow encryption with public key and decryption with private. */
   if (pKO->kcp.flags & KF_PUBLIC_KEY)
   {
      unsigned int modlen = pKO->kcp.blk_size_bits >> 3;
      if (pbdata) dbg_block("Data to encrypt: ", pbdata, *pdwDataLen);

      dbg_val("Direct RSA encryption: modlen = ", modlen);

      if ( (dwflags & CRYPT_OAEP) == CRYPT_OAEP )
      {
        lenrestrict = 42;
      }
      else
      {
        lenrestrict = 11;
      }

      if (dwflags & CRYPT_RSALARGEBLOCK)
      {
         // our flag, in newcsp.h. Allow many |key|-11-sized blocks to be encrypted together.
         unsigned noofblocks, lastlength;

         lastlength = ((*pdwDataLen) % (modlen-lenrestrict));
         noofblocks = ((*pdwDataLen)/(modlen-lenrestrict)) +
           ( (lastlength > 0) ? 1 : 0);

         dbg_val("Large blocks flag passed in, length of data = ", *pdwDataLen);
         dbg_val("                     no. of blocks required = ", noofblocks);
         dbg_val("                       length of last block = ", lastlength);

         if (pbdata == NULL)
         {
            // get-size call...
            *pdwDataLen = noofblocks*modlen;
         }
         else
         {
            BYTE *tempbuffer;
            unsigned encryptloop, startlen;

            if (dwBufLen < (noofblocks * modlen))
            {
               dbg_str("Output buffer too small", "");
               UnlockKey(hKey);
               ReleaseProv(hProv);
               return chkerr(NTE_BAD_LEN);
            }

            tempbuffer = malloc(noofblocks*modlen);
            if (!tempbuffer)
            {
               dbg_strl("The CSP has run out of memory trying to do direct RSA encryption","");
               UnlockKey(hKey);
               ReleaseProv(hProv);
               return FALSE;
            }

            for (encryptloop = 0; encryptloop < noofblocks; encryptloop++)
            {
               if ((encryptloop == noofblocks-1) && (lastlength > 0))
                  startlen = lastlength;
               else
                  startlen = modlen-lenrestrict;

               dbg_val("Encrypting one block, start len = ", startlen);

               memcpy(tempbuffer+(encryptloop*modlen), pbdata+(encryptloop*(modlen-11)), startlen);

               if (!encrypt_one_block(tempbuffer+(encryptloop*modlen),
                                      &startlen, pC, pKO, dwflags))
               {
                  UnlockKey(hKey);
                  ReleaseProv(hProv);
                  return FALSE;
               }
            }
            memcpy(pbdata, tempbuffer, noofblocks*modlen);
            free(tempbuffer);
            *pdwDataLen = noofblocks*modlen;
            dbg_val("Encrypt finished, total length = ", *pdwDataLen);
         }
      }
      else // standard behaviour.
      {
         dbg_printf("Standard single block behaviour.\n");
         if (pbdata == NULL)
         {
            // get-size call...
            dbg_printf("Get-size call; setting length required for output block = %d.\n",
                       modlen);
            *pdwDataLen = modlen;
         }
         else
         {
            dbg_printf("Encryption call; length of input data = %d.\n", *pdwDataLen);
            /* Changed next line to be > rather than != - we should allow less-than. */
            if (*pdwDataLen > modlen-lenrestrict) // this value must have a sensible
                                          // length in, for any call
            {
               dbg_printf("Input data wrong size; input data len = %d but must be <= %d.\n",
                          *pdwDataLen, modlen-lenrestrict);
               UnlockKey(hKey);
               ReleaseProv(hProv);
               return chkerr(NTE_BAD_DATA);
            }

            if (dwBufLen < modlen) // buffer len only needed if not a get-size call.
            {
               dbg_printf("Output buffer too small, length = %d but must be %d.\n",
                          dwBufLen, modlen);
               UnlockKey(hKey);
               ReleaseProv(hProv);
               return chkerr(NTE_BAD_LEN);
            }

            if (!encrypt_one_block(pbdata, pdwDataLen, pC, pKO, dwflags))
            {
               UnlockKey(hKey);
               ReleaseProv(hProv);
               return FALSE;
            }
            dbg_val("Output size = ", *pdwDataLen);
         }

      }
      UnlockKey(hKey);
      ReleaseProv(hProv);
      return TRUE;
   }

   UnlockKey(hKey);
   ReleaseProv(hProv);
   return chkerr( NTE_BAD_KEY_STATE );
}

/* ------------------------------------------ */

BOOL WINAPI CPDecrypt ( HCRYPTPROV hProv,
			  HCRYPTKEY hKey,
			  HCRYPTHASH hHash,
			  BOOL final,
			  DWORD dwflags,
			  unsigned char *pbdata,
			  DWORD *pdwDataLen )
{
   PKEY_OBJ pKO;
   PCONTAINER pC;

   ENTER("CPDecrypt");
   check_init();

   dbg_ptrf("CPDecrypt: hProv=", (void*)hProv);
   dbg_ptr ("             key=", (void*)hKey);
   dbg_val ("           final=",final);
   dbg_val ("           flags=",dwflags);
   dbg_val ("     *pdwDataLen=", *pdwDataLen);

   pC = ValidateProv(hProv);
   if (pC == NULL)
      return FALSE;
   if (pbdata == NULL)
      return FALSE;

   pKO = ValidateKey(hProv, hKey, CRYPT_DECRYPT, "used to decrypt something" );
   if ( pKO == NULL )
   {
      ReleaseProv(hProv);
      return FALSE;
   }
   LockKey(hKey);

   dbg_val("     init_done=", pKO->kcp.init_done);

   if (pKO->kcp.alg_type == CALG_AGREEDKEY_ANY)
   {
      dbg_str("DH secret key not initialised yet", "");
      return chkerr(NTE_BAD_KEY_STATE);
   }


   if ((pKO->kcp.flags & KF_BLK_CIPHER) && (!final))
   {
      // if we've not got the final flag set, the no. of bits must be a
      // multiple of the block size.
      if ((*pdwDataLen % (pKO->kcp.blk_size_bits >> 3)) != 0)
      {
         dbg_val("No. of bits of ciphertext supplied not a multiple of the block size; ciphertext len = ",
                 *pdwDataLen);
         dbg_val("                                                        pKO->kcp.blk_size_bits >> 3 = ",
                 pKO->kcp.blk_size_bits >> 3);
         return chkerr(NTE_BAD_DATA);
      }
   }

   if ( pKO->kcp.flags & (KF_BLK_CIPHER | KF_STREAM_CIPHER) )
   {
      if ( !pKO->kcp.init_done )
      {
         if ( !chkerr( InitSymKey(pKO) ))
         {
            UnlockKey(hKey);
            ReleaseProv(hProv);
            return FALSE;
         }

         pKO->kcp.init_done = 1;
      }

      if ( !chkerr(SymEncryptDecrypt ( pKO, pbdata, pdwDataLen, *pdwDataLen, final, 1 ) ))
      {
         UnlockKey(hKey);
         ReleaseProv(hProv);
         return FALSE;
      }

      if ( final )
         pKO->kcp.init_done = 0;

      /* Hash output data */

      if ( hHash )
      {
         UnlockKey(hKey);
         ReleaseProv(hProv);
         return CPHashData(hProv, hHash, pbdata, *pdwDataLen, 0 );
         /* don't need to release 'cos the hash will acquire and release by itself */
      }

      UnlockKey(hKey);
      ReleaseProv(hProv);
      return TRUE;
   }

   /* Now we allow decryption with public key and decryption with private. */
   if (pKO->kcp.flags & KF_PRIVATE_KEY)
   {
      unsigned int modlen = pKO->kcp.blk_size_bits >> 3;

      dbg_val("Direct RSA decryption: modlen = ", modlen);

      if (dwflags & CRYPT_RSALARGEBLOCK)
      {
         unsigned noofblocks;

         if ( ((*pdwDataLen) % modlen) != 0 )
         {
            UnlockKey(hKey);
            ReleaseProv(hProv);
            return chkerr(NTE_BAD_DATA);
         }
         noofblocks = ((*pdwDataLen) / modlen); // should be exact, this time.

         dbg_val("Large blocks flag passed in, length of data = ", *pdwDataLen);
         dbg_val("                     no. of blocks required = ", noofblocks);


         if (pbdata == NULL)
         {
            *pdwDataLen = noofblocks*modlen; // redundant...
         }
         else
         {
            BYTE *tempbuffer;
            unsigned decryptloop, finishlen;

            tempbuffer = malloc(*pdwDataLen);
            if (!tempbuffer)
            {
               dbg_strl("The CSP has run out of memory trying to do direct RSA decryption","");
               UnlockKey(hKey);
               ReleaseProv(hProv);
               return FALSE;
            }

            memcpy(tempbuffer, pbdata, *pdwDataLen);

            for (decryptloop = 0; decryptloop < noofblocks; decryptloop++)
            {
               finishlen = modlen;
               if ( !(NFast_RSA_Decrypt(tempbuffer+(decryptloop*modlen),modlen,
                                        pbdata+(decryptloop*(modlen-11)),&finishlen,
                                        pKO, (dwflags & CRYPT_OAEP) == CRYPT_OAEP ?
                                        TRUE : FALSE)))
               {
                  UnlockKey(hKey);
                  ReleaseProv(hProv);
                  return chkerr(GetLastError());
               }
               /*reverse_bytes (pbdata+(decryptloop*(modlen-11)), pbdata+(decryptloop*(modlen-11)), finishlen );*/
               dbg_val("Decrypted one block, finish len = ", finishlen);
               dbg_block("Block data = ", pbdata+(decryptloop*(modlen-11)), finishlen);

            }
            // ... which is now the length of the final decrypted block (all the rest will have been
            // modlen-11).
            //
            free(tempbuffer);
            *pdwDataLen = ((noofblocks-1)*(modlen-11)) + finishlen;
            dbg_val("Decrypt finished, total length = ", *pdwDataLen);
            dbg_block("Total decrypted data = ", pbdata, *pdwDataLen);
         }
      }
      else
      {
         /* Data must be equal to modulus length */
         dbg_val("Standard single block behaviour, length of data = " ,*pdwDataLen);

         if (pbdata == NULL)
            *pdwDataLen = modlen; // shouldn't ever get here.
         else
         {
            if (*pdwDataLen != modlen)
            {
               dbg_printf("Input data wrong size; given size = %d but must be %d.\n",
                          *pdwDataLen, modlen);
               UnlockKey(hKey);
               ReleaseProv(hProv);
               return chkerr(NTE_BAD_DATA);
            }
            if ( !(NFast_RSA_Decrypt(pbdata, modlen, pbdata, pdwDataLen, pKO,
                                     (dwflags & CRYPT_OAEP) == CRYPT_OAEP ?
                                        TRUE : FALSE)))
            {
               UnlockKey(hKey);
               ReleaseProv(hProv);
               return chkerr(GetLastError());
            }
            //reverse_bytes (pbdata, pbdata, *pdwDataLen );
            dbg_val("Output size = ", *pdwDataLen);
            dbg_block("Output data = ", pbdata, *pdwDataLen);
         }
      }

      UnlockKey(hKey);
      ReleaseProv(hProv);
      return TRUE;
   }

   UnlockKey(hKey);
   ReleaseProv(hProv);
   return chkerr ( NTE_BAD_KEY_STATE );
}

/* ------------------------------------------ */

BOOL WINAPI CPDestroyKey ( HCRYPTPROV hProv, HCRYPTKEY hKey )
{
   PKEY_OBJ pKO;
   PCONTAINER pC;

   ENTER("CPDestroyKey");
   check_init();

   dbg_ptrf("CPDestroyKey: hProv=", (void*)hProv);
   dbg_ptr ("                key=", (void*)hKey);

   pC = ValidateProv(hProv);
   if (pC == NULL)
   {
      return FALSE;
   }

   pKO = ValidateKey ( hProv, hKey, 0, "deleted" );
   if ( pKO == NULL )
   {
      ReleaseProv(hProv);
      return FALSE;
   }

   DestroyKeyObj ( pKO );
   ReleaseProv(hProv);
   return TRUE;
}



/* Key import/export stuff ----------------------------------------------- */

BOOL WINAPI CPImportKey ( HCRYPTPROV hProv,
			    const BYTE *pbData,
			    DWORD dwDataLen,
			    HCRYPTKEY hImportKey,
			    DWORD dwFlags,
			    HCRYPTKEY *phKey_out )
{
   PCONTAINER pC;
   PUBLICKEYSTRUC *pHdr;
   RSAPUBKEY *pHdr2;       // Used to check opaque blob magic
   PKEY_OBJ pK, pK_debug;
   BOOL result;
   unsigned char pbDataCopy[MAX_BLOB_SIZE];

   ENTER("CPImportKey");
   check_init();

   dbg_ptrf ("CPImportKey: hProv  = ", (void*)hProv);
   dbg_block("           : pbData = ", pbData, dwDataLen);
   dbg_ptr  ("           : ikey   = ", (void*)hImportKey);
   dbg_val  ("           : flags  = ", dwFlags);

   /* Validate parameters */

   pC = ValidateProv ( hProv );
   if ( pC == NULL )
      return FALSE;

   pK = NULL;

   /* This is grotty and horrible and will be commented out in the release
    * version.  The 'nice' way to do this is to create an RSA key with a
    * modulus of 1 and use that for the encryption key. I only put this in
    * so I could run some known-answer tests and hadn't seen the KB article
    * about the official hack.  The file test.c in the /test subdirectory
    * now contains the required functionality, in printRawSessionKey(hProv,
    * hKey). */

   if ( (dwFlags != 0 ) &&
        !(dwFlags & (CRYPT_EXPORTABLE | CRYPT_SET_KEY_DATA | CRYPT_VOLATILE
                     | CRYPT_NO_SALT | CRYPT_USER_PROTECTED | CRYPT_OAEP )) )
   {
      ReleaseProv(hProv);
      return chkerr ( NTE_BAD_FLAGS );
   }

   if (pbData == NULL)
   {
      ReleaseProv(hProv);
      return chkerr(ERROR_INVALID_PARAMETER);
   }

   if (dwFlags == CRYPT_SET_KEY_DATA)
   {
      /* import a key directly, writing the data less the first word to the key.
         the first word being the algorithm. */
      if ( !chkerr(MakeSymKey(pC, *((int*)pbData),
                              (BYTE *)(pbData+4), (dwDataLen-4),
                              CRYPT_NO_SALT | CRYPT_SET_KEY_DATA,
                              &pK,(dwDataLen-4) << 3)))
      {
         ReleaseProv(hProv);
         return FALSE;
      }
      // next line: always set the CRYPT_EXPORT flag now.
      /*pK->kcp.permissions = ((dwFlags & CRYPT_EXPORTABLE) ? CRYPT_EXPORT : 0)
         | CRYPT_READ | CRYPT_WRITE | CRYPT_ENCRYPT | CRYPT_DECRYPT;*/
      pK->kcp.permissions = CRYPT_EXPORT | CRYPT_READ | CRYPT_WRITE |
         CRYPT_ENCRYPT | CRYPT_DECRYPT | CRYPT_MAC;

      if (dwFlags & CRYPT_USER_PROTECTED) // a joke, for this type of key...
         pK->kcp.flags |= KF_USER_WARN;

      *phKey_out = (HCRYPTKEY)pK;
      dbg_ptrf("       new hacked phKey=", (void*)*phKey_out);
      ReleaseProv(hProv);
      return TRUE;
   }

   /* Check blob header */

   pHdr = (PUBLICKEYSTRUC *)pbData;

   if ((dwFlags & CRYPT_OAEP) && (pHdr->bType != SIMPLEBLOB))
   {
      return chkerr ( NTE_BAD_FLAGS );
   }

   if (dwDataLen < sizeof(PUBLICKEYSTRUC) || pHdr->bVersion != CUR_BLOB_VERSION)
   {
      ReleaseProv(hProv);
      return chkerr(NTE_BAD_VER);
   }

   /* We want to be able to check if the blob is infact a NFKMKEYBLOB
    * before we get to the switch statement
    */
   pHdr2 = (RSAPUBKEY *)(pbData + sizeof(PUBLICKEYSTRUC));

   /* We need to allow for more data iff it's an opaque NFKM keyblob.
    * For right now I'll assume these blobs may have data more that MAX_BLOB_SIZE and
    * try to have the code work around that fact.
    */
   if (( dwDataLen > MAX_BLOB_SIZE ) && (pHdr2->magic != OPAQUE_NFKMKEYBLOB_MAGIC))
   {
      ReleaseProv(hProv);
      return chkerr(NTE_BAD_DATA);
   }

   dbg_val("Import key, key type is ",pHdr->bType);

   // make sure we don't overwrite the original blob (because you might
   // want to re-import from the same blob twice). This is consistent with
   // the behaviour of the MS provider. (e.g. RSA decryption uses same
   // source as dest).
   if (pHdr2->magic != OPAQUE_NFKMKEYBLOB_MAGIC)
   {
     memset(pbDataCopy, 0, MAX_BLOB_SIZE);
     memcpy(pbDataCopy, pbData, dwDataLen);
     dbg_val("Import key, key type after copy is ",pHdr->bType);
   }

   /* Check blob type */

   switch ( pHdr->bType )
   {
      case PUBLICKEYBLOB:
         if ((pHdr->aiKeyAlg == CALG_DH_EPHEM) || (pHdr->aiKeyAlg == CALG_DH_SF))
         {
            if ((pHdr->aiKeyAlg == CALG_DH_EPHEM) && !hImportKey)
            {
               dbg_str("Ephemeral DH key import requested but no "
                       "import key passed in!", "");
               ReleaseProv(hProv);
               return chkerr(NTE_NO_KEY);
            }
            result = ImportDHSharedSecret(pC, pbDataCopy, dwDataLen,
                                          hImportKey, phKey_out);
         }
         else
         {
            if (hImportKey )
            {
               ReleaseProv(hProv);
               return chkerr(ERROR_INVALID_PARAMETER);
            }
            result = ImportPublicKey ( pC, pbDataCopy, dwDataLen, phKey_out );
         }
         break;

      case PRIVATEKEYBLOB:
         /* Is the blob encrypted? */

         if ( hImportKey )
         {
            dbg_val("ImportPrivateKey: encrypted key length =",dwDataLen);
            dbg_block("ImportPrivateKey: encrypted key information =",
                      pbDataCopy,dwDataLen);

            dwDataLen -= PRIVATEBLOB_UNENCRYPTED_SIZE;
            LockKey(hImportKey);

            if ( !CPDecrypt ( hProv, hImportKey, 0, 1, 0,
                              pbDataCopy + PRIVATEBLOB_UNENCRYPTED_SIZE,
                              &dwDataLen ) )
            {
               UnlockKey(hImportKey);
               return FALSE;
            }

            UnlockKey(hImportKey);
            dwDataLen += PRIVATEBLOB_UNENCRYPTED_SIZE;
         }

         result = ImportPrivateKey(pC, pbDataCopy, dwDataLen, phKey_out,
                  (dwFlags & CRYPT_VOLATILE) != CRYPT_VOLATILE,
                  (dwFlags & CRYPT_EXPORTABLE) == CRYPT_EXPORTABLE );
         break;

      case SIMPLEBLOB:
#ifdef SIGONLY
         dbg_strl("An attempt was made to import a symmetric key"
                  "with the signature-only provider","");
         ReleaseProv(hProv);
         return chkerr(NTE_BAD_PROV_TYPE);
#else /* not SIGONLY */

         LockKey(hImportKey);
         result = ImportSimpleKey(pC, (PSIMPLE_KEY_BLOB) pbDataCopy,
                                   dwDataLen, phKey_out,  hImportKey, dwFlags);
         UnlockKey(hImportKey);
         break;
#endif /* SIGONLY */


      case OPAQUEKEYBLOB:
         /* No import key allowed */
         if ( hImportKey )
         {
            ReleaseProv(hProv);
            return chkerr(ERROR_INVALID_PARAMETER);
         }

         if (pHdr2->magic == OPAQUE_NFKMKEYBLOB_MAGIC)
           result = ImportOpaqueNFKMKey(pC, pbData, dwDataLen, phKey_out);
         else
           result = ImportOpaqueKey(pC, pbDataCopy, dwDataLen, phKey_out,
                    (dwFlags & CRYPT_EXPORTABLE) == CRYPT_EXPORTABLE);
         break;

      case SYMMETRICWRAPKEYBLOB:
#ifdef SIGONLY
         dbg_strl("An attempt was made to import a symmetric key"
                  "with the signature-only provider","");
         ReleaseProv(hProv);
         return chkerr(NTE_BAD_PROV_TYPE);
#else /* not SIGONLY */
         if (!hImportKey)
         {
            ReleaseProv(hProv);
            return chkerr(ERROR_INVALID_PARAMETER);
         }
         LockKey(hImportKey);
         result = ImportWrappedKey( pC, (PSIMPLE_KEY_BLOB)pbDataCopy,
                                    dwDataLen, phKey_out, hImportKey);
         UnlockKey(hImportKey);
         break;
#endif

      case PLAINTEXTKEYBLOB:
         {
            /* import the key with no wrapping, in each case;
             * send it straight to the appropriate fns we already have. */
            if ( hImportKey ) {
               ReleaseProv(hProv);
               return chkerr(ERROR_INVALID_PARAMETER);
            }
            switch (GET_ALG_CLASS(pHdr->aiKeyAlg)) {
            case ALG_CLASS_SIGNATURE:
            case ALG_CLASS_KEY_EXCHANGE:
               dbg_printf("PLAINTEXTKEYBLOB import, ImportPrivateKey called.\n");
               result = ImportPrivateKey(pC, pbDataCopy, dwDataLen, phKey_out,
                        (dwFlags & CRYPT_VOLATILE) != CRYPT_VOLATILE,
                        (dwFlags & CRYPT_EXPORTABLE) == CRYPT_EXPORTABLE );
               break;

            case ALG_CLASS_DATA_ENCRYPT:
            case ALG_CLASS_MSG_ENCRYPT:
#ifdef SIGONLY
               dbg_strl("An attempt was made to import a symmetric key"
                  "with the signature-only provider","");
               ReleaseProv(hProv);
               return chkerr(NTE_BAD_PROV_TYPE);
#else /* not SIGONLY */
               dbg_printf("PLAINTEXTKEYBLOB import, ImportSimpleKey called.\n");
               result = ImportSimpleKey(pC, (PSIMPLE_KEY_BLOB) pbDataCopy,
                                         dwDataLen, phKey_out, 0, dwFlags);
               break;
#endif

            default:
               dbg_printf("Unknown PLAINTEXTKEYBLOB type 0x%x.\n",
                          GET_ALG_CLASS(pHdr->aiKeyAlg));
               ReleaseProv(hProv);
               return chkerr(NTE_BAD_ALGID);
            }
         }
         break;

      default:
         dbg_printf("Unknown blob type 0x%x.\n",pHdr->bType);
         ReleaseProv(hProv);
         return chkerr(NTE_BAD_TYPE);
   }

   if (result)
      pK_debug = ValidateKey(hProv, (HCRYPTKEY)(*phKey_out), CRYPT_READ,
                             "debugged");

   ReleaseProv(hProv);
   dbg_ptrf("        new hKey=", (void*)*phKey_out);
   return result;
}


/* ---------------------- */

BOOL WINAPI CPExportKey ( HCRYPTPROV hProv,
			    HCRYPTKEY hKey,
			    HCRYPTKEY hExportKey,
			    DWORD dwBlobType,
			    DWORD dwFlags,
			    unsigned char *pbData,
			    DWORD *pdwDataLen )
{
   PKEY_OBJ pK, pKE;
   PCONTAINER pC;
   BOOL result;

   ENTER("CPExportKey");
   check_init();

   dbg_ptrf("CPExportKey: hProv=", (void*)hProv);
   dbg_val ("              type=", dwBlobType );
   dbg_ptr ("               key=", (void*)hExportKey);

   pC = ValidateProv(hProv);
   if (pC == NULL)
   {
      return FALSE;
   }

   if ( dwBlobType == PUBLICKEYBLOB ) /* Can always export */
      pK = ValidateKey ( hProv, hKey, CRYPT_READ, "WINAPI" );
   else
      pK = ValidateKey ( hProv, hKey, CRYPT_EXPORT, "WINAPI" );

   if ( pK == NULL )
   {
      ReleaseProv(hProv);
      return FALSE;
   }

   if (hExportKey)
   {
      pKE = ValidateKey ( hProv, hExportKey, CRYPT_READ, "used to export another key" );
      if ( pKE == NULL )
      {
         ReleaseProv(hProv);
         return FALSE;
      }
   }

   if ((dwFlags & CRYPT_DESTROYKEY) && (dwBlobType != OPAQUEKEYBLOB))
   {
      return chkerr(NTE_BAD_FLAGS);
   }

   if ((dwFlags & CRYPT_OAEP) && (dwBlobType != SIMPLEBLOB))
   {
      return chkerr(NTE_BAD_FLAGS);
   }

   dbg_str("ExportKey: key checks out OK","");

   LockKey(hKey);
   LockKey(hExportKey);

   switch ( dwBlobType )
   {
      case PUBLICKEYBLOB:
         result = ExportPublicKey ( pK, hExportKey, pbData, pdwDataLen, dwFlags );
         break;

      case PRIVATEKEYBLOB:
         /* this isn't allowed in VERIFY contexts but the user should
          * never have got his hands on a private key handle anyway. */
         result = ExportPrivateKey(pK, hExportKey, pbData, pdwDataLen);
         break;

      case SIMPLEBLOB:
         /* check for presence of export key? */
#ifdef SIGONLY
         /* We should never have got into this state! */
         dbg_strl("The signature provider has managed to load a "
                  "symmetric key for simple blob export","");
         assert(FALSE);
#endif
         if (!hExportKey)
            result = chkerr(NTE_NO_KEY);
         else
            result = ExportSimpleKey(pK, hExportKey, pbData, pdwDataLen, dwFlags);
         break;

      case OPAQUEKEYBLOB:
         if (hExportKey)
            result = chkerr(ERROR_INVALID_PARAMETER);
         else {
            if (pK->nfastprivhandle)
               result = ExportOpaqueNFKMKey( pK, pbData, pdwDataLen );
            else
               result = ExportOpaqueKey ( pK, pbData, pdwDataLen );
         }
         break;

      case SYMMETRICWRAPKEYBLOB:
#ifdef SIGONLY
         /* We should never have got into this state! */
         dbg_strl("The signature provider has managed to load a "
                  "symmetric key for wrapped blob export","");
         assert(FALSE);
#endif
         if (!hExportKey)
            result = chkerr(ERROR_INVALID_PARAMETER);
         else
            result = ExportWrappedKey( (HCRYPTPROV)pC, pK, hExportKey, pbData, pdwDataLen);
         break;

      case PLAINTEXTKEYBLOB:
         if (hExportKey)
            result = chkerr(ERROR_INVALID_PARAMETER);
         else {
            switch (GET_ALG_CLASS(pK->kcp.alg_type)) {
            case ALG_CLASS_DATA_ENCRYPT:
            case ALG_CLASS_MSG_ENCRYPT:
#ifdef SIGONLY
               /* We should never have got into this state! */
               dbg_strl("The signature provider has managed to load a "
                        "symmetric key for wrapped blob export","");
               assert(FALSE);
#endif
               dbg_printf("PLAINTEXTKEYBLOB export.\n");
               result = ExportSimpleKey(pK, 0, pbData, pdwDataLen, dwFlags);
               break;

            default:
               result = chkerr(NTE_BAD_ALGID);
            }
         }
         break;

      default:
         dbg_str("ExportKey: bad keyblob type","");
         result = chkerr(NTE_BAD_TYPE);
         break;
   }

   if (dwFlags & CRYPT_DESTROYKEY)
   {
      if (!CPDestroyKey(hProv, hKey))
      {
         UnlockKey(hKey);
         UnlockKey(hExportKey);
         ReleaseProv(hProv);
         return FALSE;
      }
   }

   UnlockKey(hKey);
   UnlockKey(hExportKey);
   ReleaseProv(hProv);

   if (pbData != NULL)
      dbg_block("         : blob=", pbData, *pdwDataLen);
   return result;
}

/* ----------------------------------- */

BOOL WINAPI CPGetUserKey ( HCRYPTPROV hProv, DWORD dwKeySpec, HCRYPTKEY *phUserKey )
{
   PCONTAINER pC = NULL;
   PKEY_OBJ pKO = NULL;

   ENTER("CPGetUserKey");
   check_init();

   pC = ValidateProv(hProv);
   if ( pC == NULL )
      return FALSE;

   if (pC->flags & CF_VERIFY_ONLY)
   {
      dbg_printf("No private keys in a VERIFY context.\n");
      return chkerr(NTE_NO_KEY);
   }

   dbg_ptrf("CPGetUserKey: hProv=", (void*)hProv);
   dbg_val ("               spec=", dwKeySpec );

   switch ( dwKeySpec )
   {
      case AT_KEYEXCHANGE:
         pKO = GetUserKey (pC, USER_KEY_EXCHANGE, FALSE);
         break;

      case AT_SIGNATURE:
         pKO = GetUserKey (pC, USER_KEY_SIGN, FALSE);
         break;

      default:
         return chkerr(NTE_BAD_KEY);
   }


   if ( pKO == NULL )
   {
      ReleaseProv(hProv);
      return FALSE;
   }

   *phUserKey = (HCRYPTKEY) pKO;
   dbg_ptrf("    new phUserKey=", (void*)*phUserKey);
   ReleaseProv(hProv);
   return TRUE;
}


/* Signature stuff ========================================== */

static unsigned char ASN1_MD5[18] =
{
  0x10, 0x04, 0x00, 0x05, 0x05, 0x02, 0x0D, 0xF7,
  0x86, 0x48, 0x86, 0x2A, 0x08, 0x06, 0x0C, 0x30,
  0x20, 0x30
};


#if 0
/* The value given below follows MS's documentation, but is in fact in error.
   See CryptoAPI digest 2-May-1997.
*/
static unsigned char ASN1_SHA1[15] =
{
  0x14, 0x04, 0x00, 0x05, 0x12, 0x02, 0x03, 0x0E,
  0x2B, 0x05, 0x06, 0x09, 0x30, 0x21, 0x30
};
#endif

static unsigned char ASN1_SHA1[15] =
{
  0x14, 0x04, 0x00, 0x05, 0x1A, 0x02, 0x03, 0x0E,
  0x2B, 0x05, 0x06, 0x09, 0x30, 0x21, 0x30
};

/*
 * These next three are new.
 * I reverse-engineered the first one, then gave up and Googled.
 * http://cert.uni-stuttgart.de/archive/ietf-openpgp/2001/04/msg00044.html
 */

static unsigned char ASN1_SHA256[19] =
{
   0x20, 0x04, 0x00, 0x05, 0x01, 0x02, 0x04, 0x03,
   0x65, 0x01, 0x48, 0x86, 0x60, 0x09, 0x06, 0x0d,
   0x30, 0x31, 0x30
};

static unsigned char ASN1_SHA384[19] =
{
   0x30, 0x04, 0x00, 0x05, 0x02, 0x02, 0x04, 0x03,
   0x65, 0x01, 0x48, 0x86, 0x60, 0x09, 0x06, 0x0d,
   0x30, 0x41, 0x30
};

static unsigned char ASN1_SHA512[19] =
{
   0x40, 0x04, 0x00, 0x05, 0x03, 0x02, 0x04, 0x03,
   0x65, 0x01, 0x48, 0x86, 0x60, 0x09, 0x06, 0x0d,
   0x30, 0x51, 0x30
};

#define CALG2ASN1(type) case CALG_##type##: \
                          memcpy(dst+len, ASN1_##type##, sizeof(ASN1_##type##)); \
                          len += sizeof(ASN1_##type##); \
                          break

/* --------------------------------------- */

static BOOL build_signing_block ( unsigned char *dst, unsigned int len_max,
				  HCRYPTPROV hProv, HCRYPTHASH hHash, LPCWSTR descr, BOOL addoid )
{
   DWORD len, hashlen;
   PCONTAINER pC;

   ENTER("build_signing_block");

   pC = ValidateProv ( hProv );
   if ( pC == NULL )
      return FALSE;

   /* Add 'description' */
   /*printf("bsb, description...");*/

   if ((GetAlgType(hHash) != CALG_SSL3_SHAMD5) && (descr != NULL))
   {
      /* Can't call HashData with this hash type - it fails. You shouldn't
         be using this parameter anyway. */
      len = 0;
      while ( descr[len] != 0 ) len++;

      if ( !CPHashData ( hProv, hHash, (unsigned char *)descr, len*2, 0 ))
         return FALSE;
   }


   /* Get hash data */
   /*printf("OK\nget hash data...");*/

   /* check our private key is long enough to sign this hash. */
   len = sizeof(DWORD);
   if (!CPGetHashParam(HPROV_ANY, hHash, HP_HASHSIZE, (unsigned char *)&hashlen, &len, 0))
      return FALSE;
   if (hashlen > len_max)
      return chkerr(NTE_BAD_LEN);

   len = len_max;
   if ( !CPGetHashParam( HPROV_ANY, hHash, HP_HASHVAL, dst, &len, 0) )
      return FALSE;

   /* Reverse bytes */
   /*printf("OK\nreverse bytes...");*/

   reverse_bytes ( dst, dst, len );

   /* Add algorithm identifier */
   /*printf("OK\nAlg id...");*/

   if (addoid)
   {
      switch ( GetAlgType(hHash) )
      {

         CALG2ASN1(MD5);
         CALG2ASN1(SHA1);
         CALG2ASN1(SHA256);
         CALG2ASN1(SHA384);
         CALG2ASN1(SHA512);

         case CALG_SSL3_SHAMD5:
            break;

         default:
            ReleaseProv(hProv);
            return chkerr(NTE_BAD_ALGID); /* Shouldn't do this! */
      }
   }

   /* Make block to be signed */

   if ( !build_pkcs1_block ( pC, 0x01, dst, len, len_max ) )
   {
      ReleaseProv(hProv);
      return FALSE;
   }

   ReleaseProv(hProv);
   return TRUE;
}

/* --------------------------------------- */

BOOL WINAPI CPSignHash ( HCRYPTPROV hProv,
			   HCRYPTHASH hHash,
			   DWORD dwKeySpec,
			   LPCWSTR sDescription,
			   DWORD	dwFlags,
			   unsigned char *pbSignature,
			   DWORD *pdwSigLen )
{
   BYTE BlobBuf[MAX_BLOB_SIZE];
   PCONTAINER pC;
   PKEY_OBJ pSignKey;
   HCRYPTKEY hSignKey;
   DWORD len;
   BOOL res;
   unsigned int reqlen;

   ENTER("CPSifnHash");

   check_init();

   dbg_ptrf("CPSignHash: hProv=", (void*)hProv);
   dbg_ptr ("             hash=", (void*)hHash);
   dbg_val ("             spec=",dwKeySpec);

   pC = ValidateProv(hProv);
   if ( pC == NULL )
      return FALSE;

   if ( (dwFlags != 0 ) && (dwFlags != CRYPT_NOHASHOID) )
   {
      ReleaseProv(hProv);
      return chkerr(NTE_BAD_FLAGS);
   }

   if (dwKeySpec == AT_KEYEXCHANGE)
   {
#ifdef RSACSP
      /* Well, this is allowed by the spec. but brainless. Now we're going all export-
         controlled we don't want to allow this. */
      /* No, no, in fact IIS 5 does actually do this. Doh. We won't fail, then. */
      dbg_str("Attempt to use the exchange RSA key to sign a hash - didn't fail, though.","");
      //ReleaseProv(hProv);
      //return chkerr(NTE_BAD_ALGID);
#else
      dbg_str("Attempt to use a DH key to sign a hash!", "");
      return chkerr(NTE_BAD_ALGID);
#endif
   }

   /* Get signing key */

   if ( !CPGetUserKey ( hProv, dwKeySpec, &hSignKey ) )
   {
      /* don't release here 'cos it'll have just been released */
      return FALSE;
   }
   pSignKey = (PKEY_OBJ)hSignKey;

#ifdef RSACSP
   len = pSignKey->kcp.blk_size_bits >> 3;
   reqlen = len;
#else
   if (GetAlgType(hHash) != CALG_SHA1)
   {
      dbg_val("The DSA CSP can only sign SHA1 hashes; hash passed in was type ",
              GetAlgType(hHash));
      return chkerr(NTE_BAD_HASH);
   }
   len = 20;
   reqlen = 40;
#endif

   if ((pbSignature == NULL) || (*pdwSigLen == 0))
   {
      pbSignature = NULL;
      *pdwSigLen = reqlen;
      res = TRUE;
      goto clear_exit;
   }
   else if (*pdwSigLen < reqlen)
   {
      *pdwSigLen = reqlen;
      pbSignature = NULL;
      res = chkerr(ERROR_MORE_DATA);
      goto clear_exit;
   }

   /* don't get the hash value until we know we're not doing a get-size call;
    * we don't want the hash to finalise. */
#ifdef RSACSP
   /* Make PKCS#1 signature block */
   if ( !build_signing_block( BlobBuf, len, hProv, hHash, sDescription, dwFlags == 0) )
   {
      res = FALSE;
      goto clear_exit;
   }
#else
   if ( !CPGetHashParam( hProv, hHash, HP_HASHVAL, BlobBuf, &len, 0) )
   {
      dbg_str("Unable to get hash value prior to signing", "");
      return FALSE;
   }
#endif

   /* Sign it with private key & copy out */

   if (!(NFast_Sign(BlobBuf, len, pbSignature, pdwSigLen, pSignKey)))
   {
      res = FALSE;
      goto clear_exit;
   }
   res = TRUE;
   dbg_block("Signature is: ", pbSignature, *pdwSigLen);

clear_exit:
   CLEAR(BlobBuf, len);
   ReleaseProv(hProv);
   CPDestroyKey(hProv,hSignKey);
   return res;
}

/* --------------------------------------- */

BOOL WINAPI CPVerifySignature ( HCRYPTPROV hProv,
				  HCRYPTHASH hHash,
				  const BYTE *pbSignature,
				  DWORD dwSigLen,
				  HCRYPTKEY hPubKey,
				  LPCWSTR sDescription,
				  DWORD dwFlags )
{
   BYTE BlobBuf[MAX_BLOB_SIZE];
   PKEY_OBJ pPubKey;
   BOOL res, sig_OK;
   PCONTAINER pC;
   DWORD len;

   ENTER("CPVerifySignature");

   check_init();

   dbg_ptrf("CPVerifySignature: hProv=", (void*)hProv);
   dbg_ptr("                  hash=", (void*)hHash);
   dbg_ptr("            public key=", (void*)hPubKey);
   dbg_block("             signature=", pbSignature, dwSigLen);

   pC = ValidateProv(hProv);
   if (pC == NULL)
   {
      return FALSE;
   }

   /* Validate parameters & lengths */

   if ( (dwFlags != 0 ) && (dwFlags != CRYPT_NOHASHOID) )
   {
      ReleaseProv(hProv);
      return chkerr(NTE_BAD_FLAGS);
   }

   pPubKey = ValidateKey ( hProv, hPubKey, CRYPT_READ, "used to verify a signature" );
   if ( pPubKey == NULL )
   {
      ReleaseProv(hProv);
      return FALSE;
   }
   if (!CheckPKReady(pPubKey))
      return chkerr(NTE_BAD_KEY_STATE);

   if ((pPubKey->kcp.alg_type != CALG_RSA_SIGN) && (pPubKey->kcp.alg_type != CALG_DSS_SIGN))
   {
#ifdef RSACSP
      /* Well, this is allowed by the spec. but brainless. Now we're going all export-
         controlled we don't want to allow this. */
      /* No, no, in fact IIS 5 does actually do this. Doh. We won't fail, then. */
      dbg_str("Attempt to use the exchange RSA key to verify a signature - didn't fail, though.","");
      //ReleaseProv(hProv);
      //return chkerr(NTE_BAD_ALGID);
#else
      dbg_str("Attempt to use a DH key to sign a hash!", "");
      return chkerr(NTE_BAD_ALGID);
#endif
   }

#ifdef RSACSP
   /* Make PKCS#1 signature block */
   len = pPubKey->kcp.blk_size_bits >> 3;
   if ( !build_signing_block( BlobBuf, len, hProv, hHash, sDescription, dwFlags == 0) )
   {
      res = FALSE;
      goto clear_exit;
   }
#else
   if (GetAlgType(hHash) != CALG_SHA1)
   {
      dbg_val("The DSA CSP can only verify SHA1 hashes; hash passed in was type", GetAlgType(hHash));
      return chkerr(NTE_BAD_HASH);
   }
   len = 20;
   if ( !CPGetHashParam( hProv, hHash, HP_HASHVAL, BlobBuf, &len, 0) )
   {
      dbg_str("Unable to get hash value prior to signing", "");
      return FALSE;
   }
#endif

   /* Decrypt signature */

   LockKey(hPubKey);
   dbg_val("Verification public key ID = ",pPubKey->nfastpubhandle);
   if (!(NFast_Verify(BlobBuf, len, (BYTE *)(pbSignature), dwSigLen, pPubKey, &sig_OK)))
   {
      res = chkerr(NTE_FAIL);
      UnlockKey(hPubKey);
      goto clear_exit;
   }
   UnlockKey(hPubKey);

   /* Compare it */
   if (sig_OK)
      res = TRUE;
   else
      res = chkerr(NTE_BAD_SIGNATURE);

clear_exit:
   CLEAR(BlobBuf, 2*dwSigLen);
   ReleaseProv(hProv);
   return res;
}



/* WINAPI functions: provider stuff ---------------------------------------- */

CRITICAL_SECTION nfastchecklock;

void checknfastpresent()
{

   ENTER("checknfastpresent");
   EnterCriticalSection(&nfastchecklock);

   if (!nfastchecked)
   {
      BOOL KMpresent;

      nfastpresent = NFast_Present(&KMpresent, &FIPS3compatible);
      if (!KMpresent)
      {
         dbg_strl("A suitable nFast accelerator is not present. MS CAPI functions have been disabled. ","");
         nfastpresent = FALSE;
         /* Here the check is made. If this flag gets set to FALSE this context can only
         * ever be used for random numbers and getting provider parameters.
         * If the nFast is removed while the machine is on all bets are off anyway,
         * as the machine will break horribly. Open contexts do not survive over a reboot. */
      }
      else
         nfastpresent = TRUE;

      nfastchecked = TRUE;
   }

   LeaveCriticalSection(&nfastchecklock);
}

BOOL WINAPI CPAcquireContext ( HCRYPTPROV *phProv, const char *pszContainer,
				 DWORD dwFlags, PVTableProvStruc pVTable )
{
   PCONTAINER pC;
   char BlobBuf[20];
   M_Hash *hash = (M_Hash *)BlobBuf;
   char currentusername[MAX_USERNAME_LEN];
   CInfo cinfo;
   int unamelen = MAX_USERNAME_LEN;

   ENTER("CPAcquireContext");

   check_init();

   dbg_valf("CPAcquireContext: flags=", dwFlags);

   *phProv = (HCRYPTPROV) 0;

   if (!GetUserName(currentusername, &unamelen))
   {
      dbg_printfe("Unable to get current user name; last error was 0x%x, exiting.\n");
      return chkerr(GetLastError());
   }

   if (dwFlags & CRYPT_VERIFYCONTEXT)
   {
      if (pszContainer != NULL)
      {
         dbg_str("Verify context called with a non-NULL container name","");
         return chkerr(ERROR_INVALID_PARAMETER);
      }
      pszContainer = currentusername;
      if (dwFlags & CRYPT_DELETEKEYSET)
      {
         dbg_printf("Trying to delete a VERIFYCONTEXT, exiting gracefully...\n");
         return TRUE;
      }
      /* NEWKEYSET is ignored for VCs; it implicitly aways exists. */
   }
   else
   {
      /* the container-file-setup stuff is only relevant for non-verify contexts. */
      /*if ((dwFlags & CRYPT_DELETEKEYSET) && (pszContainer == NULL))
      {
         dbg_str("Attempt to delete the default container, exiting gracefully","");
         return TRUE;
      }*/

      if ( pszContainer == NULL )
         pszContainer = currentusername;

      cinfo.containername = pszContainer;
      cinfo.username = (dwFlags & CRYPT_MACHINE_KEYSET) ? NULL : currentusername;
      cinfo.dllname = DLLNAME;

      dbg_str("                   name=", pszContainer);
      dbg_val("        calling process=", GetCurrentProcessId());
      dbg_val("         calling thread=", GetCurrentThreadId());

      if ( dwFlags & CRYPT_DELETEKEYSET )
      {
         /* DeleteKMFile will delete any NVRAM areas (counters, KITB stuff)
          * as it goes; if it can't delete these it'll return FALSE. */
         BOOL ok = TRUE;
         BOOL silent = dwFlags & CRYPT_SILENT ? TRUE : FALSE;
         if (!NFast_DeleteKMFile(cinfo, silent, USER_KEY_EXCHANGE, 1))
            ok = FALSE;
         if (!NFast_DeleteKMFile(cinfo, silent, USER_KEY_SIGN, 1))
            ok = FALSE;
         if (!ok)
         {
            if (GetLastError() == NTE_SILENT_CONTEXT)
               return chkerr(NTE_SILENT_CONTEXT);
            else
               return chkerr(NTE_BAD_KEY_STATE);

         }
         return chkerr(DeleteContainer(cinfo));
      }
      else if ( dwFlags & CRYPT_NEWKEYSET )
      {
         /* Just creates a placeholder in the registry. */
         if ( !chkerr(CreateContainer(cinfo)) )
            return FALSE;
      }
      else if ( (dwFlags == 0 ) || (dwFlags & CRYPT_MACHINE_KEYSET) )
      {
         /* Checks the file entry exists. */
         if ( !chkerr(CheckContainer(cinfo)) )
            return FALSE;
      }
   }

   /* Allocate a local memory block for this context */
   pC = (PCONTAINER) malloc ( sizeof(CONTAINER) );
   if ( pC == NULL )
   {
      dbg_strl("The CSP has run of memory trying to create a new context","");
      return chkerr(NTE_NO_MEMORY);
   }
   memset ( pC, 0, sizeof(CONTAINER) );

#define PREALLOCATEDBLOCKSIZE 20
#define PREALLOCATEDKEYSIZE 2048
#define PREALLOCATEDHASHSIZE 1024
   if (!makeobjectstore(PREALLOCATEDBLOCKSIZE, PREALLOCATEDKEYSIZE, pC, &(pC->keys)))
      return chkerr(NTE_NO_MEMORY);
   if (!makeobjectstore(PREALLOCATEDBLOCKSIZE, PREALLOCATEDHASHSIZE, pC, &(pC->hashes)))
      return chkerr(NTE_NO_MEMORY);

   /* Set flags */

   if (dwFlags & CRYPT_MACHINE_KEYSET) pC->flags |= CF_MACHINE;
   if (dwFlags & CRYPT_VERIFYCONTEXT) pC->flags |= CF_VERIFY_ONLY;
   if (dwFlags & CRYPT_SILENT) pC->flags |= CF_SILENT;

   /* we now only do this once per LoadLibrary. Only one thread can
    * do the checking at once; it's locked while it's happening (as it's quite long).*/
   checknfastpresent();
   pC->flags |= nfastpresent ? CF_NFASTPRESENT : 0;
   pC->flags |= FIPS3compatible ? CF_FIPS3COMPATIBLE : 0;

   /* note that we now allow module keys in a FIPS world. */
   pC->flags |= checkregkey("UseModuleKeys") ? CF_MODULEKEYS : 0;

   pC->tag = TAG_CONTAINER_OBJ;

   if(!(pC->container_name = malloc(strlen(pszContainer)+1))) {
      dbg_strl("The CSP has run of memory trying to copy the container name","");
      return chkerr(NTE_NO_MEMORY);
   }
   memset(pC->container_name, 0, strlen(pszContainer)+1);
   strcpy (pC->container_name, pszContainer);
   pC->cinfo.containername = pC->container_name;

   strncpy(pC->user_name, currentusername, MAX_USERNAME_LEN);
   pC->cinfo.username = (dwFlags & CRYPT_MACHINE_KEYSET) ? NULL : pC->user_name;
   pC->cinfo.dllname = DLLNAME;

   /* We can pretend to be a number of different types
      of provider if we want (e.g. both DSS and RSA).
      This lets us know what sort of provider the user asked
      CryptAcquireContext for. Not available in version 1.*/
   /* This doesn't seem to be passing us a version 2 struct in NT 4,
      but it does in NT 5. */
   if (pVTable->Version >= 2)
      pC->dwProvType = pVTable->dwProvType;
   else
#ifdef RSACSP
//  What about or AES provider...
#ifdef SIGONLY
      pC->dwProvType = PROV_RSA_SIG;
#else
      pC->dwProvType = PROV_RSA_FULL;
#endif
#else
#ifdef SIGONLY
      pC->dwProvType = PROV_DSS;
#else
      pC->dwProvType = PROV_DSS_DH;
#endif
#endif
   pC->SGCflags = 0;

#ifdef CONTEXTTHREADCHECK
   pC->calling_process = GetCurrentProcessId();
   pC->calling_thread = GetCurrentThreadId();
   pC->thread_count = 0;
#endif

   InitializeCriticalSection(&(pC->critical));
   pC->multilock = MRSW_Initialise();
   if (pC->multilock == NULL)
   {
      dbg_str("Unable to create multilock", "");
      return chkerr(GetLastError());
   }

   dbg_ptrf("  OK, handle = ", pC);
   dbg_valf("       flags = ", pC->flags);

   *phProv = (HCRYPTPROV) pC;
   return TRUE;
}

/* ------------------------------ */

BOOL WINAPI CPGetProvParam ( HCRYPTPROV hProv, DWORD dwParam,
			       unsigned char *pbData, DWORD *pdwDataLen, DWORD dwFlags )
{
   PCONTAINER pC;
   PROV_ENUMALGS enum_norm;
   PROV_ENUMALGS_EX enum_xtd;
   void *copy_ptr;
   unsigned int copy_len;
   int sig_key_inc;
   int keyset_type;
   int version;
   int truevalue = TRUE;
   BOOL result;

   ENTER("CPGetProvName");

   check_init();

   if (pbData==NULL)
      dbg_printff("CPGetProvParam (getsize):  hProv = %x, param = %d\n",hProv, dwParam);
   else
      dbg_printff("CPGetProvParam : hProv = %x, param = %d\n",hProv, dwParam);

   pC = ValidateProv(hProv);
   if ((pC == NULL ) && (GetLastError() != NTE_NOT_FOUND))
      return FALSE;
   else
      pC = (PCONTAINER) hProv;

   switch ( dwParam )
   {
   case PP_PROVTYPE:
      if ( dwFlags != 0 ) goto flag_error;
      if ( pC->dwProvType )
      {
         dbg_val("  Provider type=", pC->dwProvType);
         copy_ptr = &(pC->dwProvType);
         copy_len = sizeof(pC->dwProvType);
      }
      else
      {
         ReleaseProv(hProv);
         return chkerr(NTE_BAD_TYPE); // can this ever happen?
      }
      break;

      /* I'm not sure about UNIQUE_CONTAINER. The documentation is sketchy.
       * I think it should possibly return the file name of the KM files,
       * or something like that. I'll ask MS people. */
   case PP_CONTAINER:
   case PP_UNIQUE_CONTAINER:
      if ( dwFlags != 0 ) goto flag_error;
      dbg_str("  Container name=",pC->container_name);
      copy_ptr = pC->container_name;
      copy_len = (unsigned int)strlen (pC->container_name) + 1;
      break;

   case PP_IMPTYPE:
      if ( dwFlags != 0 ) goto flag_error;
      dbg_val("  Implementation type=",impl_type);
      copy_ptr = &impl_type;
      copy_len = 4;
      break;

   case PP_VERSION:
      if ( dwFlags != 0 ) goto flag_error;
      // this ignores the VERSION_RELEASEPATCH.
      version = 0x100*VERSION_RELEASEMAJOR + VERSION_RELEASEMINOR;
      dbg_val("  Version=",version);
      copy_ptr = &version;
      copy_len = 4;
      break;

   case PP_NAME:
      if ( dwFlags != 0 ) goto flag_error;
      switch (pC->dwProvType)
      {
      case PROV_RSA_AES:
      case PROV_RSA_FULL:
      case PROV_RSA_SIG:
      case PROV_DSS_DH:
      case PROV_DSS:
         dbg_str("  Provider name=",CSPNAME);
         copy_ptr = CSPNAME;
         copy_len = (unsigned int)strlen(CSPNAME)+1;
         break;

#ifndef SIGONLY
      case PROV_RSA_SCHANNEL:
      case PROV_DH_SCHANNEL:
         dbg_str("  Provider name=",CSPSNAME);
         copy_ptr = CSPSNAME;
         copy_len = (unsigned int)strlen(CSPSNAME)+1;
         break;
#endif

      default:
         ReleaseProv(hProv);
         return chkerr(NTE_BAD_TYPE);
      }
      break;

   case PP_ENUMALGS:
   case PP_ENUMALGS_EX:
      if ( dwFlags & CRYPT_FIRST )
         pC->enum_counter = 0;
      dbg_val((dwParam==PP_ENUMALGS) ?
              "  Enumerate algorithms" : "  Extended enumerate algorithms", pC->enum_counter);

      if ( pC->enum_counter >= MAX_ALGS )
      {
         ReleaseProv(hProv);
         return chkerr( ERROR_NO_MORE_ITEMS );
      }

      enum_xtd = our_algs_ex[pC->enum_counter];
      if ( pbData != NULL )
         pC->enum_counter++;

      //if ( pC->dwProvType != PROV_RSA_SCHANNEL )
      //  enum_xtd.dwProtocols=0;
      // I don't think it matters is we return this and we're not an SCHANNEL provider.
      // We need to return the SIGNABLE flag whatever anyway.

      if ( dwParam==PP_ENUMALGS )
      {
         enum_norm.aiAlgid=enum_xtd.aiAlgid;
         enum_norm.dwBitLen=enum_xtd.dwDefaultLen;
         enum_norm.dwNameLen=enum_xtd.dwNameLen;
         memcpy(enum_norm.szName, enum_xtd.szName, enum_xtd.dwNameLen);
         copy_ptr = &enum_norm;
         copy_len = sizeof(enum_norm);
      }
      else
      {
         copy_ptr = &enum_xtd;
         copy_len = sizeof(enum_xtd);
      }
      break;

   case PP_ENUMCONTAINERS:
      {
         if (pbData != NULL)
         {
            char *name;

            dbg_val("  Enumerate containers, counter = ", pC->enum_counter);

            /* changed next line to decide whether or not to enum machine keys or
             * user keys from the type of context it's called from, rather than passing
             * in the CRYPT_MACHINE_CONTEXT flag. Documentation bug, caught by
             * CA testing at MS.*/
            name = EnumContainers(dwFlags & CRYPT_FIRST, pC->cinfo);
            if ( name == NULL )
            {
               dbg_str("No name returned for EnumContainers - no more items.", "");
               ReleaseProv(hProv);
               return chkerr( ERROR_NO_MORE_ITEMS );
            }

            copy_ptr = name;
            copy_len = (unsigned int)strlen(name) + 1;
            dbg_str("EnumContainers returned container name ", name);
         }
         else
         {
            copy_ptr = NULL;
            copy_len = LongestContainer(pC->cinfo);
            dbg_val("EnumContainers getsize, max. size = ", copy_len);
         }
         break;
      }

   case PP_KEYSET_SEC_DESCR:
      dbg_str("  Keyset security description","");
      result = GetKeySecurity( pC->cinfo,
                               (SECURITY_DESCRIPTOR *)pbData, pdwDataLen,
                               (SECURITY_INFORMATION) (dwFlags));
      ReleaseProv(hProv);
      return chkerr(result);

      /* increment of key sizes. 256 assumes 512,768 or 1024-bit keys */
   case PP_SIG_KEYSIZE_INC:
   case PP_KEYX_KEYSIZE_INC:
      sig_key_inc = PUBLIC_KEY_INCREMENT;
      dbg_val("  Keysize increment: ",sig_key_inc);
      copy_ptr = &sig_key_inc;
      copy_len = 4;
      break;

   case PP_KEYSET_TYPE:
      keyset_type = 0; // FIND OUT WHAT THIS MEANS!!!
      dbg_val("  Keyset type: ", keyset_type);
      copy_ptr = &keyset_type;
      copy_len = 4;
      break;

   case PP_ENUMEX_SIGNING_PROT:
      dbg_printf("  Able to signal signable hashes returned as TRUE.\n");
      copy_ptr = &truevalue;
      copy_len = 4;
      break;

#ifdef RSACSP
   case PP_SGC_INFO:
      dbg_val("  SGC info for container: ",pC->SGCflags);
      copy_ptr = &(pC->SGCflags);
      copy_len = 4;
      break;
#endif

   case PP_CRYPT_COUNT_KEY_USE:
      version = (pC->flags & CF_KEYCOUNTING) ? 1 : 0;
      dbg_printf("  Key counting parameter value: %d.\n", version);
      copy_ptr = &version;
      copy_len = 4;
      break;

   case PP_NO_HOST_STORAGE:
      version = (pC->flags & CF_KITB) ? 1 : 0;
      dbg_printf("  KITB parameter value: %d.\n", version);
      copy_ptr = &version;
      copy_len = 4;
      break;

   case PP_USE_HARDWARE_RNG:
      dbg_printf("  Hardware RNG returned as TRUE.\n");
      copy_ptr = &truevalue;
      copy_len = 4;
      break;

   case PP_KEYSPEC:
      dbg_printf("  Key spec returned as AT_SIGNATURE | AT_KEYEXCHANGE\n");
      version = AT_SIGNATURE | AT_KEYEXCHANGE;
      copy_ptr = &version;
      copy_len = 4;
      break;

   default:
      ReleaseProv(hProv);
      return chkerrval( NTE_BAD_TYPE , dwParam);
   }

   if ( pbData == NULL ) /* Get-size call */
   {
      *pdwDataLen = copy_len;
      dbg_printf("Parameter length = %d\n.", copy_len);
      ReleaseProv(hProv);
      return TRUE;
   }
   else if ( *pdwDataLen < copy_len )
   {
      *pdwDataLen = copy_len;
      ReleaseProv(hProv);
      return chkerr(ERROR_MORE_DATA);
   }

   if ( copy_len > 0 )
      memcpy ( pbData, copy_ptr, copy_len );
   dbg_blockf("Parameter returned = ", copy_ptr, copy_len);

   *pdwDataLen = copy_len;
   ReleaseProv(hProv);
   return TRUE;

flag_error:
   ReleaseProv(hProv);
   return chkerr ( NTE_BAD_FLAGS );
}

/* ------------------------------ */

BOOL set_nvram_flag(const BYTE *pbData, const char *what, PCONTAINER pC, unsigned flag)
{
  DWORD val;

   ENTER("set_nvram_flag");

   val = *((DWORD *)pbData);
   if (!val)
   {
      dbg_printf("Disabling %s. \n", what);
      pC->flags &= ~flag;
   }
   else
   {
      if (!NFast_AreWeNVRAMCapable())
      {
         dbg_printf("No modules are NVRAM capable.\n");
         return FALSE;
      }
      else
      {
         dbg_printf("Enabling %s.\n", what);
         pC->flags |= flag;
      }
   }
   return TRUE;
}

BOOL WINAPI CPSetProvParam ( HCRYPTPROV hProv, DWORD dwParam,
			       const BYTE *pbData, DWORD dwFlags )
{
   PCONTAINER pC;
   BOOL result;

   ENTER("CPSetProvParam");

   check_init();

   dbg_printff("CPSetProvParam: hProv = %x, param = %d\n",hProv, dwParam);

   pC = ValidateProv(hProv);
   if ( pC == NULL )
      return FALSE;

   switch ( dwParam )
   {
      case PP_CLIENT_HWND:              // ignore this parameter.
      case PP_USE_HARDWARE_RNG:         // yes, yes, yes. We'll use hardware
                                        // even if this isn't specified, of course.
         ReleaseProv(hProv);
         return TRUE;
         break;

      case PP_KEYSET_SEC_DESCR:
         result = SetKeySecurity( pC->cinfo,
                                  (SECURITY_DESCRIPTOR *) pbData,
                                  (SECURITY_INFORMATION) (dwFlags));
         ReleaseProv(hProv);
         return chkerr(result);
         break;

         // dunno how to do SGC for DSA prov... needs pubexp stuff (see the
         // call to SPQueryCFLevel)!
#ifdef RSACSP
      case PP_SGC_INFO:
         {
            PPUBLIC_KEYX_BLOB pubxkey;
            PKEY_OBJ xkey;
            DWORD pubxkeylen;

            dbg_str("Setting the SGC info for the container","");
            xkey = GetUserKey(pC,USER_KEY_EXCHANGE, FALSE);
            dbg_str("Got key for SGC info","");
            if (xkey == NULL)
            {
               dbg_str("Set SGC info, no exchange key present, failed","");
               return FALSE;
            }
            if (!ExportPublicKey(xkey, 0, NULL, &pubxkeylen, 0))
            {
               dbg_str("Set SGC info, can't get size of public key, failed","");
               return FALSE;
            }
            dbg_val("Got length of required blob, len = ",pubxkeylen);
            pubxkey = (PPUBLIC_KEYX_BLOB)malloc(pubxkeylen);
            dbg_str("Allocated blob","");
            if (!pubxkey)
            {
               dbg_strl("The CSP has run out of memory trying to get the SGC parameters","");
               return FALSE;
            }
            if (!ExportPublicKey(xkey, 0, (unsigned char *)pubxkey, &pubxkeylen, 0))
            {
               dbg_str("Set SGC info, can't get public key, failed","");
               return FALSE;
            }
            dbg_str("WINAPI public key","");
            if (!SPQueryCFLevel((PCCERT_CONTEXT) pbData, &(pubxkey->data[0]),
                                (pubxkey->rsahdr.bitlen) >> 3,
                                pubxkey->rsahdr.pubexp, &(pC->SGCflags)))
            {
               dbg_val("Set SGC info, can't get CF level, last error was",GetLastError());
               return FALSE;
            }
            dbg_val("Set SGC info, CF level = ",pC->SGCflags);
            free(pubxkey);
            return TRUE;
            break;
         }
#endif

      case PP_SIGNATURE_PIN:
         if (!NFast_HashPP(pbData, &(pC->pins[USER_KEY_SIGN])))
         {
            dbg_printf("Unable to set signature PIN.\n");
            ReleaseProv(hProv);
            return chkerr(NTE_FAIL);
         }
         pC->flags |= CF_SIGPIN;
         break;

      case PP_KEYEXCHANGE_PIN:
         if (!NFast_HashPP(pbData, &(pC->pins[USER_KEY_EXCHANGE])))
         {
            dbg_printf("Unable to set key exchange PIN.\n");
            ReleaseProv(hProv);
            return chkerr(NTE_FAIL);
         }
         pC->flags |= CF_KEYXPIN;
         break;

      case PP_CRYPT_COUNT_KEY_USE:
         if (!set_nvram_flag(pbData, "key counting", pC, CF_KEYCOUNTING)) {
            ReleaseProv(hProv);
            return chkerr(NTE_NOT_FOUND);
         }
         break;

      case PP_NO_HOST_STORAGE:
         if (!set_nvram_flag(pbData, "KITB", pC, CF_KITB)) {
            ReleaseProv(hProv);
            return chkerr(NTE_NOT_FOUND);
         }
         break;

      default:
         ReleaseProv(hProv);
         return chkerrval( NTE_BAD_TYPE , dwParam);
   }
   ReleaseProv(hProv);
   return TRUE;
}

/* ----------------------------------- */

BOOL WINAPI CPReleaseContext ( HCRYPTPROV hProv, DWORD dwFlags )
{
   PCONTAINER pC;

   ENTER("CPReleaseContext");

   check_init();

   dbg_valf("CPReleaseContext, prov=", (int) hProv );

   pC = ValidateProv(hProv);
   if ( pC == NULL )
      return FALSE;

   /* Any keys objects which refer to the permanent keys will have the PERMANENT flag
      set, so DeleteKeyObj will not delete the keys stored on the unit. So if they exist,
      we delete them explicitly. */
   if (pC->userkeys[0] != NULL)
   {
      dbg_ptr("Destroying pC->userkeys[0] = ", pC->userkeys[0]);
      NFast_DestroyKey(pC->userkeys[0], TRUE);
   }
   if (pC->userkeys[1] != NULL)
   {
      dbg_ptr("Destroying pC->userkeys[1] = ", pC->userkeys[1]);
      NFast_DestroyKey(pC->userkeys[1], TRUE);
   }

   /* Destroy & release all allocated key & hash objects */
   freeobjectstore(&(pC->keys));
   freeobjectstore(&(pC->hashes));

   dbg_ptr("Freeing pC->multilock = ", pC->multilock);
   MRSW_Free(pC->multilock);
   DeleteCriticalSection(&(pC->critical));

   /* Destroy container object */

   free(pC->container_name);
   CLEAR ( pC, sizeof(CONTAINER) );
   free(pC);

   if ( dwFlags != 0 )
   {
      // MSDN library now says if the flags are 0, return the error _but_release_the_context_.
      ReleaseProv(hProv);
      return chkerr ( NTE_BAD_FLAGS );
   }

   return TRUE;
}

CRITICAL_SECTION setuplock;

void check_init(void)
{
  static int done_init = 0;

  if (done_init) return;

  ENTER("check_init");

  EnterCriticalSection(&setuplock);

  if (!done_init) {
    SetLoggingFromRegistry(DLLNAME "debug");
    nfkm_setup();
    app = global_pool.hApp;
    GenRandom_setup();
    nfastchecked = FALSE;

    done_init = 1;
  }

  LeaveCriticalSection(&setuplock);
}

BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID reserved)
{
   switch (fdwReason)
   {
      case DLL_PROCESS_ATTACH:
         InitializeCriticalSection(&nfastchecklock);
         InitializeCriticalSection(&setuplock);
         break;

      case DLL_PROCESS_DETACH:
         nfkm_free();
         DeleteCriticalSection(&setuplock);
         DeleteCriticalSection(&nfastchecklock);
         break;
   }
   return TRUE;
}

/* ----------------------------------- */

/*HRESULT WINAPI DllRegisterServer(void)
{
  return NTE_OP_OK;
}

HRESULT WINAPI DllUnregisterServer(void)
{
  return NTE_OP_OK;
}*/

