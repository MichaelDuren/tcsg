// csscsp.h : General header file for the csscsp project
//
#pragma once

#define CSSCSP_NAME              "Thales CSS Cryptographic Provider"
#define CSSCSP_SECONDARY_NAME    "Thales CSS Secondary Provider"
#define LCSSCSP_NAME            L"Thales CSS Cryptographic Provider"
#define LCSSCSP_SECONDARY_NAME  L"Thales CSS Secondary Provider"


