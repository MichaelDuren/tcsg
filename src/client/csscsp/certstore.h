#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#define CSSCSP_NAME              "Thales CSS Cryptographic Provider"
#define CSSCSP_SECONDARY_NAME    "Thales CSS Secondary Provider"
#define LCSSCSP_NAME            L"Thales CSS Cryptographic Provider"
#define LCSSCSP_SECONDARY_NAME  L"Thales CSS Secondary Provider"

BOOL AddEncodedCertificateToStore(LPSTR pszContainer, LPCSTR encodedCert, BOOL secondaryFlag);
BOOL AddCertContextToStore(LPSTR pszContainer, PCCERT_CONTEXT pCertContext, BOOL secondaryFlag);
BOOL DecodeCertificate(LPCSTR encodedCert, PCCERT_CONTEXT* ppContext);
void PurgeCertStore(BOOL secondaryFlag );

#ifdef __cplusplus
}
#endif
