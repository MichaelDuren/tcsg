#include "stdafx.h"
#include <stdio.h>
#include <windows.h>
#include <wincrypt.h>

#include "csscsp.h"
#include "cssutil.h"
#include "certstore.h"

#pragma comment(lib, "crypt32.lib")

#define MY_ENCODING_TYPE  (PKCS_7_ASN_ENCODING | X509_ASN_ENCODING)
#define LCSSCSP_NAME L"Thales CSS Cryptographic Provider"
#define LCSSCSP_SECONDARY_NAME L"Thales CSS Secondary Provider"

// Globals
extern BOOL g_debug;


BOOL DecodeCertificate(LPCSTR encodedCert, PCCERT_CONTEXT* ppContext)
{
	BOOL rc = FALSE;
	PCCERT_CONTEXT pCertCon1 = NULL;
	DWORD    fmtUsed = 0; // ignored
	DWORD    bCertLen = 0;
	BYTE *   bCert = NULL;

	if( CryptStringToBinaryA(encodedCert, (DWORD)strlen(encodedCert), CRYPT_STRING_BASE64, NULL, &bCertLen, 0, &fmtUsed) )
	{
		bCert = (BYTE *)malloc(bCertLen);
		if( bCert == NULL )
		{
			SetLastError(NTE_NO_MEMORY);
			return FALSE;
		}
		CryptStringToBinaryA(encodedCert, (DWORD)strlen(encodedCert), CRYPT_STRING_BASE64, bCert, &bCertLen, 0, &fmtUsed);

		pCertCon1 = CertCreateCertificateContext(MY_ENCODING_TYPE, (PBYTE)bCert, bCertLen );
		if( pCertCon1 != NULL )
		{
			*ppContext = pCertCon1;
			rc = TRUE;
		}
		else
		{
			LogError("CertCreateCertificateContext:");
		}
		free(bCert);
        bCert = NULL;
	}
	else
	{
		LogError( "CryptStringToBinaryA returned:" );
	}
	return rc;
}

BOOL AddCertContextToStore(LPSTR pszContainer, PCCERT_CONTEXT pCertContext, BOOL secondaryFlag)
{
	BOOL ret = FALSE;
	HCERTSTORE hSystemStore = 0;
	CRYPT_KEY_PROV_INFO cryptprivkeyinfo;
	LPCWSTR storeName = L"MY";
	DWORD cbwstr = (DWORD)(strlen(pszContainer)+1) * sizeof(WCHAR);
	LPWSTR wstr;

	// Open the named System Store
	if( !(hSystemStore = CertOpenStore( CERT_STORE_PROV_SYSTEM, 0, (HCRYPTPROV_LEGACY)NULL, CERT_SYSTEM_STORE_CURRENT_USER, storeName )) )
	{
		LogError( "Unable to open the current user's personal certificate store:" );
		return FALSE;
	}

	wstr = (LPWSTR)malloc(cbwstr);
	if( wstr == NULL )
	{
		SetLastError(NTE_NO_MEMORY);
		LogError("");
		CertCloseStore( hSystemStore, 0 );
		return FALSE;
	}

	MultiByteToWideChar(0, 0, pszContainer, (DWORD)strlen(pszContainer)+1, wstr,(DWORD) strlen(pszContainer)+1);

	// Set a certificate property associating the private key to our CSP
	cryptprivkeyinfo.pwszContainerName = wstr;
	cryptprivkeyinfo.pwszProvName = secondaryFlag? LCSSCSP_SECONDARY_NAME:LCSSCSP_NAME;
	cryptprivkeyinfo.dwProvType = PROV_RSA_FULL;
	cryptprivkeyinfo.dwKeySpec = AT_SIGNATURE;
	cryptprivkeyinfo.dwFlags = 0;
	cryptprivkeyinfo.rgProvParam = NULL;
	cryptprivkeyinfo.cProvParam = 0;

	if( CertSetCertificateContextProperty( pCertContext, CERT_KEY_PROV_INFO_PROP_ID, 
											0, &cryptprivkeyinfo) )
	{
		// Add the new certificate to the store
		if( CertAddCertificateContextToStore( hSystemStore, pCertContext, CERT_STORE_ADD_ALWAYS, NULL ) )
		{
			if (g_debug)
			{
				CHAR szName[128];
				if( CertGetNameStringA( pCertContext, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, szName, sizeof(szName)/sizeof(TCHAR)))
					DebugOut( "Certificate \"%s\" added successfully.", szName);
			}
			ret = TRUE;
		}
		else
			LogError("CertAddCertificateContextToStore:");
	}
	else
		LogError("CertSetCertificateContextProperty:");
	free(wstr);
    wstr = NULL;
	CertCloseStore( hSystemStore, 0 );
	return ret;
}



//*****************************************************************
//
//
//*****************************************************************
void PurgeCertStore( BOOL secondaryFlag )
{
	HANDLE           hCertStore = NULL;        
	PCCERT_CONTEXT   pCertContext=NULL;      
	PCCERT_CONTEXT   pDupCertContext=NULL;      
	CRYPT_KEY_PROV_INFO *pCryptKeyProvInfo = NULL;
	DWORD cbData;

	LPCWSTR storeName = L"MY";

	// Open the named System Store
	if( !(hCertStore = CertOpenStore( CERT_STORE_PROV_SYSTEM, 0, (HCRYPTPROV_LEGACY)NULL, CERT_SYSTEM_STORE_CURRENT_USER, storeName )) )
	{
		LogError( "Unable to open the LOCAL_MACHINE certificate store");
		return;
	}

	while( pCertContext = CertEnumCertificatesInStore( hCertStore, pCertContext) )
	{
		DWORD dwPropId = CERT_KEY_PROV_INFO_PROP_ID;
		if( !(CertGetCertificateContextProperty( pCertContext, dwPropId, NULL, &cbData )) )
		{
			LogError( "CertGetCertificateContextProperty CERT_KEY_PROV_INFO_PROP_ID:");
			goto errout;
		}

		if( !(pCryptKeyProvInfo = (CRYPT_KEY_PROV_INFO *)malloc(cbData)) )
		{
			SetLastError(NTE_NO_MEMORY);
			LogError( "malloc:");
			goto errout;
		}

		if( CertGetCertificateContextProperty( pCertContext, dwPropId, pCryptKeyProvInfo, &cbData) )
		{
			if(( 0 == wcscmp( pCryptKeyProvInfo->pwszProvName, LCSSCSP_NAME)) && !secondaryFlag )
			{
				if (g_debug)
				{
					CHAR szName[128];
					if( CertGetNameStringA( pCertContext, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, szName, sizeof(szName)/sizeof(CHAR))) 
                    {
						DebugOut( "Deleting certificate \"%s\"", szName);
                    }
				}
				if( pDupCertContext = CertDuplicateCertificateContext(pCertContext) )
				{
					if( !CertDeleteCertificateFromStore(pDupCertContext) )
						LogError( "CertDeleteCertificateFromStore:");

                    // this must be deleted
                 //   CertFreeCertificateContext(pDupCertContext);
				}
				else
                {
					LogError( "CertDuplicateCertificateContext:");
                }
			}

			if( (0 == wcscmp( pCryptKeyProvInfo->pwszProvName, LCSSCSP_SECONDARY_NAME )) && secondaryFlag )
			{
				if (g_debug)
				{
					CHAR szName[128];
					if( CertGetNameStringA( pCertContext, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, szName, sizeof(szName)/sizeof(CHAR)))
						DebugOut( "Deleting certificate \"%s\"", szName);
				}
				if( pDupCertContext = CertDuplicateCertificateContext(pCertContext) )
				{
					if( !CertDeleteCertificateFromStore(pDupCertContext) )
						LogError( "CertDeleteCertificateFromStore:");
				}
				else
                {
					LogError( "CertDuplicateCertificateContext:");
                }
			}

		}
		else
			LogError( "CertGetCertificateContextProperty CERT_KEY_PROV_INFO_PROP_ID:");
		free(pCryptKeyProvInfo);
        pCryptKeyProvInfo = NULL;

	} // end the outer while loop. Move on to the next certificate.

errout:
//	if(pCertContext)
//		CertFreeCertificateContext(pCertContext);
	CertCloseStore(hCertStore,0);
}



BOOL AddEncodedCertificateToStore(LPSTR pszContainer, LPCSTR encodedCert, BOOL secondaryFlag)
{
	BOOL rc = FALSE;
	PCCERT_CONTEXT pCert = NULL;
	if( DecodeCertificate(encodedCert, &pCert) )
	{
		rc = AddCertContextToStore(pszContainer, pCert, secondaryFlag);
		CertFreeCertificateContext(pCert);
	}

	return rc;
}

