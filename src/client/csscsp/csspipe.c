#include "stdafx.h"

#include <stdio.h>
#include <conio.h>
#include <tchar.h>
#include <winerror.h>
#include "csspipe.h"
#include "certstore.h"
#include "cssutil.h"
#include "rsapkcs1padding.h"

#define BUFSIZE 4096
#define MY_ENCODING_TYPE  (PKCS_7_ASN_ENCODING | X509_ASN_ENCODING)

BOOL PipeTransact(LPSTR lpszPipename, LPCSTR pszMessage, LPSTR *ppszResponse);
int encode_hash(unsigned char* out, DWORD *pcbout, const unsigned char* hash, DWORD cbhash);

//******************************************************************************
//   ThalesCSPGetCertList
//
// On success, *ppszCerts will contain: one line of ';' delimited aliases ,
// and one line for each corresponding certificate delimited with lines of "--------"
//
//******************************************************************************
BOOL ThalesCSPGetCertList(char ** ppszCerts)
{
	BOOL rc = FALSE;
	char *pszData = NULL;
	LPSTR pszPipeName = "\\\\.\\pipe\\ThalesCSSListKeys";


	rc = PipeTransact(pszPipeName, NULL, &pszData);
	if (rc)
	{
		*ppszCerts = pszData;
	}
	else if (pszData)
	{
		if (*pszData)
			DebugOut(pszData);
		HeapFree(GetProcessHeap(), 0, pszData);
		pszData = NULL;
	}
	return rc;
}

/******************************************************************************
* ThalesCSPSign
*
* DESCRIPTION :     Performs the sign operation via the ThalesCodeSign service
*
* INPUTS :
*             PCONTAINER pCont
*             BYTE *pbData         -  Pointer to the data to sign
*             DWORD cbData         -  Number of bytes in the buffer point to by pbData
*             int hashAlgId            -  algorithm used to crate hash value
*             DWORD *pcbSignature  -  number of bytes in sig buffer
*
* OUTPUTS :
*             BYTE *pbSignature    -  Buffer to get the sig
*             DWORD *pcbSignature  -  number of bytes copied into sig buffer
*
* RETURN :
*             TRUE                     The function was successful.
* Remarks :
*
*
*
*/
BOOL ThalesCSPSign(PCONTAINER pCont, BYTE *pbData, DWORD cbData,
	int hashAlgId, BYTE *pbSignature, DWORD *pcbSignature)
{
	BOOL rc = FALSE;
	char *pszResponse = NULL;
	char *pszEncodedData = NULL;
	DWORD cbEncodedData;
	char *pszTempString = NULL;
	BYTE *pTempBuf = NULL;
	DWORD dwFlags = 0;
	int nLeadingZero = 0;
	DWORD cbSignature = 0;
	DWORD dwKeyLen;
	DWORD i;
	unsigned char *pkcs1 = NULL;
	char *szHashAlgId;
	LPSTR pszPipeName = "\\\\.\\pipe\\ThalesCSSSign";


	// the CSS always returns a signature that is the size of the key; no encoding 
	dwKeyLen = CertGetPublicKeyLength(MY_ENCODING_TYPE, &pCont->pCertContext->pCertInfo->SubjectPublicKeyInfo) / 8;
	if (pbSignature == NULL)
	{
		*pcbSignature = dwKeyLen;
		return TRUE;
	}

	// we will return a buffer that is dwKeyLen, so make sure there will be room
	if (*pcbSignature < dwKeyLen)
	{
		LogError("Failed to open http session.");
		goto cleanup;
	}

	// get a string representation of the haah alg id
	switch (hashAlgId)
	{
	case CALG_SHA_256:
		szHashAlgId = "SHA256"; // BCRYPT_SHA256_ALGORITHM;
		break;
	case CALG_SHA_384:
		szHashAlgId = "SHA384"; // BCRYPT_SHA384_ALGORITHM;
		break;
	case CALG_SHA_512:
		szHashAlgId = "SHA512"; // BCRYPT_SHA512_ALGORITHM;
		break;
	case CALG_MD5:
		szHashAlgId = "MD5"; // BCRYPT_MD5_ALGORITHM;
		break;
	case CALG_SHA1:
		szHashAlgId = "SHA1"; // BCRYPT_SHA1_ALGORITHM;
		break;
	default:
		szHashAlgId = "UNKNOWN";
	}

    if (CryptBinaryToStringA(pbData, cbData, CRYPT_STRING_BASE64, NULL, &cbEncodedData))
	{
		pszTempString = (char*)HeapAlloc(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, cbEncodedData);
	    if (CryptBinaryToStringA(pbData, cbData, CRYPT_STRING_BASE64, pszTempString, &cbEncodedData))
		{
			// The code below builds a json string for the request.  While primitive, the approach
			// should be relatively easy to verify as correct.

			size_t indexSB = 0;
			char *json_prefix = "{";
			char *signingTypeGuid_prefix = "\"signingTypeGuid\" : \""; // signingTypeGuid field label w/ quotes
			char *hashValue_prefix = "\"hashValue\" : \""; // hashValue field label w/ quotes
			char *hashAlg_prefix = "\"hashAlg\" : \""; // hashAlg field label w/ quotes
			char *midfix = "\", "; // I'm not sure midfix is in the dictionary :)
			char *json_postfix = "\"}"; // we include the terminating quote here :(

										// compiler should optimize away strlen for static strings
			cbEncodedData = (int)(strlen(json_prefix) + strlen(signingTypeGuid_prefix) + strlen(pCont->szContainer) + strlen(midfix) +
				strlen(hashValue_prefix) + strlen(pszTempString) + strlen(midfix) +
				strlen(hashAlg_prefix) + strlen(szHashAlgId) + strlen(json_postfix) + 1);
			pszEncodedData = (char*)HeapAlloc(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS | HEAP_ZERO_MEMORY, cbEncodedData);

			// json_prefix
			memcpy_s(pszEncodedData + indexSB, cbEncodedData - indexSB, json_prefix, strlen(json_prefix));
			indexSB += strlen(json_prefix);
			// signingTypeGuid_prefix
			memcpy_s(pszEncodedData + indexSB, cbEncodedData - indexSB, signingTypeGuid_prefix, strlen(signingTypeGuid_prefix));
			indexSB += strlen(signingTypeGuid_prefix);
			// pCont->szContainer
			memcpy_s(pszEncodedData + indexSB, cbEncodedData - indexSB, pCont->szContainer, strlen(pCont->szContainer));
			indexSB += strlen(pCont->szContainer);
			// midfix
			memcpy_s(pszEncodedData + indexSB, cbEncodedData - indexSB, midfix, strlen(midfix));
			indexSB += strlen(midfix);
			// hashValue_prefix
			memcpy_s(pszEncodedData + indexSB, cbEncodedData - indexSB, hashValue_prefix, strlen(hashValue_prefix));
			indexSB += strlen(hashValue_prefix);
			// pszTempString
			memcpy_s(pszEncodedData + indexSB, cbEncodedData - indexSB, pszTempString, strlen(pszTempString));
			indexSB += strlen(pszTempString);
			// midfix
			memcpy_s(pszEncodedData + indexSB, cbEncodedData - indexSB, midfix, strlen(midfix));
			indexSB += strlen(midfix);
			// hashAlg_prefix
			memcpy_s(pszEncodedData + indexSB, cbEncodedData - indexSB, hashAlg_prefix, strlen(hashAlg_prefix));
			indexSB += strlen(hashAlg_prefix);
			// szHashAlgId
			memcpy_s(pszEncodedData + indexSB, cbEncodedData - indexSB, szHashAlgId, strlen(szHashAlgId));
			indexSB += strlen(szHashAlgId);
			// json_postfix
			memcpy_s(pszEncodedData + indexSB, cbEncodedData - indexSB, json_postfix, strlen(json_postfix));
			indexSB += strlen(json_postfix);
		}
		else
		{
			LogError("Failed to CryptBinaryToStringA: 0x%08x", GetLastError());
			goto cleanup;
		}
		HeapFree(GetProcessHeap(), 0, pszTempString);
	}
	else
	{
		LogError("Failed to CryptBinaryToStringA: 0x%08x", GetLastError());
		goto cleanup;
	}

	// transact with the managed service and get the reponse
	if (PipeTransact(pszPipeName, pszEncodedData, &pszResponse) == FALSE)
	{
		LogError("The PipeSubmit operation failed: 0x%08x", GetLastError());
		goto cleanup;
	}

	if (!pszEncodedData)
	{
		LogError("The CSS failed to return a valid signature response");
		goto cleanup;
	}

	// convert the hash value to a string and then add to the header
	if (!CryptStringToBinaryA(pszResponse, 0, CRYPT_STRING_BASE64, NULL, &cbEncodedData, NULL, &dwFlags))
	{
		LogError("Failed to decode response into a binary value.");
		goto cleanup;
	}

	// The returned data is the base64 encoding of the bytes of a big-edian
	// integer.  The decode data may be smaller than the key size if the integer
	// starts with one or more 0 bytes; these will not be encoded.  But we want
	// to return something that is exactly the key size so we need to alloc 
	// space equal to the key size (and must include the zero value bytes).
	if (cbEncodedData < dwKeyLen)
	{
		cbEncodedData = dwKeyLen;
	}

	// allocate a buffer that we know will hold the decoded data.  Make sure memory is zeroed
	// to account for leading zeros that are not in the encoding, see comment just aboce.
	pTempBuf = (BYTE *)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY | HEAP_GENERATE_EXCEPTIONS, cbEncodedData);
	if (!CryptStringToBinaryA(pszResponse, 0, CRYPT_STRING_BASE64, pTempBuf, &cbEncodedData, NULL, &dwFlags))
	{
		LogError("Failed to decode response into a binary value.");
		goto cleanup;
	}

	// Convert big to little-endian.
	for (i = 0; i < cbEncodedData / 2; ++i)
	{
		BYTE b = pTempBuf[i];
		pTempBuf[i] = pTempBuf[cbEncodedData - 1 - i];
		pTempBuf[cbEncodedData - 1 - i] = b;
	}

	// the buffer cannot be larger then dwKeyLen+1
	if (cbEncodedData >= (dwKeyLen + 1))
	{
		cbEncodedData = dwKeyLen;
	}

	memcpy_s(pbSignature, *pcbSignature, pTempBuf, dwKeyLen);

	// if the data had a leading zero, that zero is not at the end of
	// the buffer and will be truncated by returning dwKeyLen for the
	// size of the buffer.
	*pcbSignature = dwKeyLen;

	rc = TRUE;

cleanup:

	// free any buffers that are allocated
	if (pszEncodedData)
	{
		HeapFree(GetProcessHeap(), 0, pszEncodedData);
		pszEncodedData = NULL;
	}
	if (pszResponse)
	{
		HeapFree(GetProcessHeap(), 0, pszResponse);
		pszEncodedData = NULL;
	}
	if (pTempBuf)
	{
		HeapFree(GetProcessHeap(), 0, pTempBuf);
		pTempBuf = NULL;
	}
	return rc;
}

/******************************************************************************
* PipeTransact
*
* DESCRIPTION :
*
* INPUTS :
*
* OUTPUTS :
*
* RETURN :
* Remarks :
*
*
*
*/

BOOL PipeTransact(LPSTR lpszPipename, LPCSTR pszMessage, LPSTR *ppszResponse)
{
	HANDLE hPipe;
	char   chBuf[BUFSIZE];
	char   *pBufPos;
	BOOL   fSuccess = FALSE;
	DWORD  cbLastRead, cbToWrite, cbWritten;
	LPSTR pszResponse = NULL;
	int    nReads = 0;
	DWORD cbSent = 0;
	DWORD cbInBuffer = 0;
	DWORD bufferSize = 0;
	DWORD cbRead = 0;
	DWORD dwErrorCode;
	DWORD dwLength;
	BOOL bReadResponse = FALSE;

	// Try to open a named pipe; wait for it, if necessary. 

	while (1)
	{
		hPipe = CreateFileA(
			lpszPipename,   // pipe name 
			GENERIC_READ |  // read and write access 
			GENERIC_WRITE | FILE_WRITE_ATTRIBUTES,
			0,              // no sharing 
			NULL,           // default security attributes
			OPEN_EXISTING,  // opens existing pipe 
			0,              // default attributes 
			NULL);          // no template file 


							// Break if the pipe handle is valid. 
		if (hPipe != INVALID_HANDLE_VALUE)
			break;

		// Exit if an error other than ERROR_PIPE_BUSY occurs. 
		if (GetLastError() != ERROR_PIPE_BUSY)
		{
			LogError("Could not open named pipe: %s", lpszPipename);
			return FALSE;
		}

		// All pipe instances are busy, so wait for 5 seconds, max. 

		if (!WaitNamedPipeA(lpszPipename, 5000))
		{
			LogError("Could not open named pipe %s: 5 second wait timed out.", lpszPipename);
			return FALSE;
		}
	}

	// Send a message to the pipe server. 
	cbToWrite = 0;
	if (pszMessage) {
		cbToWrite = (DWORD)strlen(pszMessage);
	}

	// Write a line containing the number of bytes in the (remaining) message
	sprintf_s(chBuf, sizeof(chBuf), "%d\r\n", cbToWrite);
	fSuccess = WriteFile(hPipe, chBuf, (DWORD)strlen(chBuf), &cbWritten, NULL);
	if (!fSuccess)
	{
		LogError("WriteFile to pipe failed. GLE=%d", GetLastError());
		return FALSE;
	}

	// write the message
	if (pszMessage)
	{
		fSuccess = WriteFile(hPipe, pszMessage, cbToWrite, &cbWritten, NULL);
		if (!fSuccess)
		{
			LogError("WriteFile to pipe failed. GLE=%d", GetLastError());
			return FALSE;
		}
	}

	nReads = 0;
	cbLastRead = 0;
	cbRead = 0;
	pszResponse = NULL;
	do
	{
		pBufPos = chBuf;
		cbInBuffer = 0;
		// read until we know there is at least enough data to get the error code and length values.
		while (cbInBuffer < 20)
		{
			fSuccess = ReadFile(hPipe, &chBuf[cbInBuffer], BUFSIZE - cbInBuffer, &cbLastRead, NULL);
			if (fSuccess)
			{
				cbInBuffer += cbLastRead;
			}
			else
			{
				LogError("ReadFile from pipe failed. GLE=%d", GetLastError());
				return FALSE;
			}
		}

		++nReads;
		if (fSuccess)
		{
			// if this is the first reading, it should be the error code and length, each 4 bytes
			if (pszResponse == NULL)
			{
				// there should be <error code>\r\n<length>\r\n at the begining of the buffer
				// Above read ensures there is at least 20 bytes of data in out buffer
				sscanf_s(chBuf, "%d", &dwErrorCode);
				while (*pBufPos != '\n' && cbInBuffer > 0)
				{
					++pBufPos;
					--cbInBuffer;
				}
				// move past the \n and make sure we did not go past the end of the buffer
				++pBufPos;
				--cbInBuffer;
				if (cbInBuffer <= 0)
				{
					LogError("Error in data formatting from server");
					return FALSE;
				}

				sscanf_s(pBufPos, "%d", &dwLength);
				while (*pBufPos != '\n' && cbInBuffer > 0)
				{
					++pBufPos;
					--cbInBuffer;
				}

				if (*pBufPos != '\n')
				{
					// this means we ran out of data before the \n, error out of caution.
					LogError("Error in data formatting from server");
					return FALSE;
				}
				else
				{
					// move past the \n and make sure we did not go past the end of the buffer
					++pBufPos;
					--cbInBuffer;

					// allocate the response buffer
					if (dwLength > 0)
					{
						bufferSize = dwLength + 1;  // +1 for the null at the end of the string
						pszResponse = HeapAlloc(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, bufferSize);
					}

				}
			}

			// if there is any data in the buffer, copy it into the response string.
			if (cbInBuffer > 0)
			{
				// truncate if there is more in the buffer than we need.
				if (cbInBuffer > (dwLength - cbRead))
				{
					cbInBuffer = dwLength;
				}
				// copy data into response buffer _s
				memcpy_s(pszResponse + cbRead, bufferSize - cbRead, pBufPos, cbInBuffer);

				// set  cbRead to be what is in the response buffer.
				cbRead += cbInBuffer;
			}

			// are we done reading dwLength?
			if (cbRead >= dwLength)
			{
				if (cbRead > (dwLength + 1))
				{
					// This is just a sanity check on the counters, we should not be able to get here.
					LogError("Error in data buffer sizes when reading response");
					return FALSE;
				}
				pszResponse[cbRead] = 0;
				bReadResponse = TRUE;
				break;
			}
		}
		else
		{
			LogError("ReadFile from pipe failed. GLE=%d", GetLastError());
			return FALSE;
		}
	} while (nReads < 50);

	if (dwErrorCode != 0)
	{
		// an error has occured, log the error 
		LogError("An error was returned from the pip server, %d", dwErrorCode);
		return FALSE;
	}
	*ppszResponse = pszResponse;
	CloseHandle(hPipe);

	return fSuccess;
}