/*
 *
 *	nCipher Crypto Service Provider, C source code
 *
 *	NCSP.H
 *		CSP-specific data structures
 *
 *
 * Versions
 *	1997.02.10	IH	Original
 *       1999.05.25      JRH     Key management support finished
 *
 * (C) Copyright NCipher Ltd 1997-1999. All rights reserved. Company Confidential.
 *
 */

#ifndef __NCSP_H
#define __NCSP_H

#include "newcsp.h"
#include "containr.h"
#include "logfns.h"
#include "cspdk.h"
#include "wincrypt.h"
#include "prealloc.h"
#include "mrswlock.h"

/* XXX remove these #define when we get this in the SDK. */
#ifndef PP_CRYPT_COUNT_KEY_USE
#define PP_CRYPT_COUNT_KEY_USE 41
#endif

#ifndef PP_NO_HOST_STORAGE
#define PP_NO_HOST_STORAGE 44
#endif

/* Errors... */
extern BOOL _chkerr(HRESULT errcode, int line, const char *fname, int val);
#define chkerr(a) _chkerr(a,__LINE__,__FILE__,0)
#define chkerrval(a,val) _chkerr(a,__LINE__,__FILE__,val)
_declspec(dllimport) void WINAPI SetLastError( DWORD dwErrCode );

// next line in for debugging...
//#define CONTEXTTHREADCHECK

extern const char *RegKeyName;
extern const char *RegMKeyName;

/* Object-type tags -------------------------------- */

#define TAG_CONTAINER_OBJ	0xCE435330
#define TAG_HASH_OBJ		0xCE435331
#define TAG_KEY_OBJ		0xCE435332

/* Key containers --------------------------------- */

struct nf_KeyObj;
struct nf_HashObj;

typedef struct _container
{
   int tag; /* = TAG_CONTAINER_OBJ */

   ObjectStore *keys;
   ObjectStore *hashes;

   int flags;
#define CF_VERIFY_ONLY          1       // verify context (no permanent keys)
#define CF_MACHINE              2       // machine container
#define CF_NFASTPRESENT         4       // nfast verified to be present.
#define CF_FIPS3COMPATIBLE      8       // nfast is in FIPS 3 mode
#define CF_SILENT              16       // silent context
#define CF_MODULEKEYS          32       // using module keys rather than operator card set keys
#define CF_SIGPIN              64       // signature PIN has been supplied
#define CF_KEYXPIN            128       // key exchange PIN has been supplied
#define CF_KEYCOUNTING        256       // keys should be created with key counters.
#define CF_KITB               512       // keys should be created KITB.

   int SGCflags; // can this key container do SGC?

   char *container_name;
   char user_name[MAX_USERNAME_LEN];
   CInfo cinfo;

   struct nf_KeyObj *userkeys[USER_KEYS_MAX];
   M_Hash pins[USER_KEYS_MAX];
#define PINPRESENT(container, keynum) \
   (container->flags & ((keynum == USER_KEY_SIGN) ? CF_SIGPIN : CF_KEYXPIN))

   int enum_counter;
   DWORD dwProvType;

#ifdef CONTEXTTHREADCHECK
   DWORD calling_process;
   DWORD calling_thread;

   DWORD using_process;
   DWORD using_thread;

   DWORD thread_count;
#endif

   CRITICAL_SECTION critical;

   MRSW_Handle multilock;
   // multiple-reader single-writer lock protecting access to nfast-specific
   // portions of keys.
} CONTAINER, *PCONTAINER;


typedef struct
{
   unsigned int len;
   unsigned char *ptr;
} VARBLK, *PVARBLK;

/* Key object ------------------------------------- */

/* These contain data which is common to algorithms & may usefully be
   set or retrieved with CPGetKeyParam/CPSetKeyParam
*/

typedef void (*BlkCryptFnPtr) ( void *ctx, unsigned char *src,
				unsigned char *dst, int DecFlag );

typedef void (*StreamCryptFnPtr) ( void *ctx, unsigned char *src,
				 unsigned char *dst, unsigned int len, int DecFlag );

typedef struct nf_KeyCtrlParams
{
   /* These are all fields than can just be copied into & out of OPAQUE blobs */
   ALG_ID alg_type;

   unsigned int permissions;
   unsigned int flags;
#define KF_BLK_CIPHER        0x1
#define KF_STREAM_CIPHER     0x2
#define KF_PUBLIC_KEY        0x4
#define KF_PRIVATE_KEY       0x8
#define KF_SCHANNEL_KEY     0x10
#define KF_SCHANNEL_MAC     0x20
#define KF_DOES_ECB        0x100
#define KF_DOES_CBC        0x200
#define KF_DOES_CFB        0x400
#define KF_DOES_OFB        0x800
#define KF_PERMANENT      0x1000
#define KF_DHSS           0x2000 // DH shared-secret key that's not got an alg type yet.
#define KF_MODULEKEY      0x4000
#define KF_HASCOUNTER     0x8000
#define KF_USER_WARN     0x10000
#define KF_KITB          0x20000
#define KF_OAEP_PARAMS   0x40000

   unsigned int key_secret_len;     /* No of secret bytes in 'key' */
   unsigned int salt_len;           /* No. of salt bytes in 'key'  */
   unsigned int effective_len_bits; /* 'effective' key length      */
   unsigned int blk_size_bits;  /* 0 for stream ciphers; blksize in bits for blk ciphers,
                                   no of modulus bits for RSA-type ciphers */
   unsigned int block_mode;
   unsigned int feedback_len_bits;
   unsigned int pad_mode;
#define NO_PADDING 0

   unsigned int init_done; /* Set when all private state is initialised, cleared when
                     'final' encrypt/decrypt has been done */
} KEYCTRLPARAMS, *PKEYCTRLPARAMS;

typedef struct nf_KeyObj
{
   int tag; /* = TAG_KEY_OBJ */
   int alloc_size;

   CRITICAL_SECTION critical;

   PCONTAINER container;  /* Root container which owns this */
   void *special;
   BOOL global;

   KEYCTRLPARAMS kcp;

   VARBLK  key;      /* Complete key */
   VARBLK  IV;       /* Initialisation Vector (for non-ECB modes only) */
   VARBLK fn_ctx;  /* Context to pass to CryptFn (usually unwound key state) */
   VARBLK extra_ctx; /* Anything else we've forgotten */
   VARBLK oaep_params; /* OAEP parameters */

   union {
      BlkCryptFnPtr BlockFn;
      StreamCryptFnPtr StreamFn;
   } fns;

   M_KeyID nfastprivhandle;
   M_KeyID nfastpubhandle;
   /* This is for a RSA key, which is now stored on the nFast.
      Non-RSA keys will have this as NULL for now.  RSA keys will have
      key,IV,fx_ctx,extra_ctx,fns all NULL.  nfastcardsetname is the name on
      the operator card set that the keys were created with. */

   MRSW_Handle multilock; // multiple-reader single-writer lock.
} KEY_OBJ, *PKEY_OBJ;

/* BEWARE!!! If you add any fields to the above, make sure you also change CreateKeyObj
 * to zero them. Otherwise foolish bugs will happen, grrr. */


/* Hash object ----------------------------------- */

typedef void (*HashFnPtr) (void *ctx, const unsigned char *data, unsigned int len );

typedef struct nf_HashObj
{
   int tag; /* = TAG_HASH_OBJ */
   int alloc_size;
   int alg_type;
   unsigned int blksize;

   CRITICAL_SECTION critical;

   PCONTAINER container;
   void *special;

   int permissions;
   int finalised;
   int valueset;

   HashFnPtr hashfn;
   VARBLK  hashctx;   /* Context for hashfn */
   VARBLK  hashval;   /* Area to hold result */
}
  HASH_OBJ, *PHASH_OBJ;

typedef struct _MAC_STATE
{
   PKEY_OBJ key;     /* key to hash with */
   /*PHASH_OBJ parent; */
   BOOL hashed;      /* have we calculated a hash yet? */
   BYTE *ibuffer;    /* overflow buffer */
   BYTE iblength;  /* number of bytes in overflow buffer */
}
  MAC_STATE, *PMAC_STATE;

/* XXX Hard-coded to be the largest hash blocksize. */
#define HMAC_MAXBLOCKSIZE       128
typedef struct _VARBLK64
{
   unsigned int len;
   unsigned char ptr[HMAC_MAXBLOCKSIZE];
}
  VARBLK64, *PVARBLK64;

typedef struct _HMAC_STATE
{
   PKEY_OBJ key;
   PHASH_OBJ hash;
   ALG_ID alg;
   VARBLK64 inner;
   VARBLK64 outer;
   VARBLK64 keyval;
   VARBLK64 innerX;
   VARBLK64 outerX;
}
  HMAC_STATE, *PHMAC_STATE;



/* Public key formats -------------------------------------- */

/* Oh, looking at this, I could use the same struct with some cunning
 * unionage for all 6 different types of blob. Hmmm. */

#define RSA1_MAGIC_VAL 0x31415352
#define RSA2_MAGIC_VAL 0x32415352

#define DSS1_MAGIC_VAL 0x31535344
#define DSS2_MAGIC_VAL 0x32535344
#define DH1_MAGIC_VAL  0x31484400
#define DH2_MAGIC_VAL  0x32484400

#define DSS3_MAGIC_VAL 0x33535344
#define DSS4_MAGIC_VAL 0x34535344
#define DH3_MAGIC_VAL  0x33484400
#define DH4_MAGIC_VAL  0x34484400

#ifdef RSACSP

// Key exchange key blob information

typedef struct
{
   PUBLICKEYSTRUC blobhdr;
   RSAPUBKEY  rsahdr;
   unsigned char data[1];    // modulus[rsahdr.bitlen/8]
}
  PUBLIC_KEYX_BLOB, *PPUBLIC_KEYX_BLOB;

#define PUBLIC_HEADER_SIZE (sizeof(PUBLICKEYSTRUC)+sizeof(RSAPUBKEY))

typedef struct
{
   PUBLICKEYSTRUC blobhdr;
   RSAPUBKEY  rsahdr;
   unsigned char data[1];       // modulus[rsahdr.bitlen/8];
                                // prime1[rsahdr.bitlen/16];
                                // prime2[rsahdr.bitlen/16];
                                // exponent1[rsahdr.bitlen/16];
                                // exponent2[rsahdr.bitlen/16];
                                // coefficient[rsahdr.bitlen/16];
                                // privateExponent[rsahdr.bitlen/8];
}
  PRIVATE_KEYX_BLOB, *PPRIVATE_KEYX_BLOB;

// Signature key blob information

// for RSA, the signature key blobs are the same as the key exchange blobs.
typedef PUBLIC_KEYX_BLOB PUBLIC_SIG_BLOB;
typedef PPUBLIC_KEYX_BLOB PPUBLIC_SIG_BLOB;

typedef PRIVATE_KEYX_BLOB PRIVATE_SIG_BLOB;
typedef PPRIVATE_KEYX_BLOB PPRIVATE_SIG_BLOB;

#define PRIVATE_HEADER_SIZE (sizeof(PUBLICKEYSTRUC)+sizeof(RSAPUBKEY))

#else

// Key exchange key blob information

typedef struct
{
   PUBLICKEYSTRUC blobhdr;
   DHPUBKEY  dhhdr;
   unsigned char data[1];    // y[dhhdr.bitlen/8] - y = (G^X) mod P
}
  PUBLIC_KEYX_BLOB, *PPUBLIC_KEYX_BLOB;

#define PUBLIC_HEADER_SIZE (sizeof(PUBLICKEYSTRUC)+sizeof(DHPUBKEY))

typedef struct
{
   PUBLICKEYSTRUC blobhdr;
   DHPUBKEY  dhhdr;
   unsigned char data[1];       // prime[dhhdr.bitlen/8];
                                // generator[dhhdr.bitlen/8];
                                // secret[dhhdr.bitlen/8];
}
  PRIVATE_KEYX_BLOB, *PPRIVATE_KEYX_BLOB;

// Signature key blob information
//
typedef struct
{
   PUBLICKEYSTRUC blobhdr;
   DSSPUBKEY  dsshdr;
   unsigned char data[1];    // modulus[dsshdr.bitlen/8]
                                // prime[20]
                                // generator[dsshdr.bitlen/8]
                                // publickey[dsshdr.bitlen/8]
                                // DSSSEED seedstruct
}
  PUBLIC_SIG_BLOB, *PPUBLIC_SIG_BLOB;

//#define PUBLIC_HEADER_SIZE (sizeof(PUBLICKEYSTRUC)+sizeof(DSSPUBKEY))
// FUDGE ALERT!!! It just happens that sizeof(DSSPUBKEY)=sizeof(DHPUBKEY) so this works.

typedef struct
{
   PUBLICKEYSTRUC blobhdr;
   DSSPUBKEY  dsshdr;
   unsigned char data[1];       // modulus[dsshdr.bitlen/8]
                                // prime[20]
                                // generator[dsshdr.bitlen/8]
                                // privatekey[20]
                                // DSSSEED seedstruct
}
  PRIVATE_SIG_BLOB, *PPRIVATE_SIG_BLOB;

// New style version 3 blobs and magic numbers.

typedef struct
{
   PUBLICKEYSTRUC blobhdr;
   DHPUBKEY_VER3 dhhdrv3;
   unsigned char data[1];       // p[dhhdrv3.bitlenP/8]
                                // q[dhhdrv3.bitlenQ/8] - can be optional if bitlenQ = 0.
                                // g[dhhdrv3.bitlenG/8]
                                // j[dhhdrv3.bitlenJ/8] - can be optional if bitlenJ = 0.
                                // y[dhhdrv3.bitlenY/8] - not present on SetKeyParam with KP_PUB_PARAMS.
}
  PUBLIC_KEYX_BLOB_V3, *PPUBLIC_KEYX_BLOB_V3;

typedef struct
{
   PUBLICKEYSTRUC blobhdr;
   DHPRIVKEY_VER3 dhhdrv3;
   unsigned char data[1];       // p[dhhdrv3.bitlenP/8]
                                // q[dhhdrv3.bitlenQ/8] - can be optional if bitlenQ = 0.
                                // g[dhhdrv3.bitlenG/8]
                                // j[dhhdrv3.bitlenJ/8] - can be optional if bitlenJ = 0.
                                // y[dhhdrv3.bitlenY/8] - not present on SetKeyParam with KP_PUB_PARAMS.
                                // x[dhhdrv3.bitlenX/8] - not present on SetKeyParam with KP_PUB_PARAMS.
}
  PRIVATE_KEYX_BLOB_V3, *PPRIVATE_KEYX_BLOB_V3;


typedef struct
{
   PUBLICKEYSTRUC blobhdr;
   DSSPUBKEY_VER3 dsshdrv3;
   unsigned char data[1];       // p[dsshdrv3.bitlenP/8]
                                // q[dsshdrv3.bitlenQ/8] - can be optional if bitlenQ = 0.
                                // g[dsshdrv3.bitlenG/8]
                                // j[dsshdrv3.bitlenJ/8] - can be optional if bitlenJ = 0.
                                // y[dsshdrv3.bitlenY/8] - not present on SetKeyParam with KP_PUB_PARAMS.
}
  PUBLIC_SIG_BLOB_V3, *PPUBLIC_SIG_BLOB_V3;

typedef struct
{
   PUBLICKEYSTRUC blobhdr;
   DSSPRIVKEY_VER3 dsshdrv3;
   unsigned char data[1];       // p[dhhdrv3.bitlenP/8]
                                // q[dhhdrv3.bitlenQ/8] - can be optional if bitlenQ = 0.
                                // g[dhhdrv3.bitlenG/8]
                                // j[dhhdrv3.bitlenJ/8] - can be optional if bitlenJ = 0.
                                // y[dhhdrv3.bitlenY/8] - not present on SetKeyParam with KP_PUB_PARAMS.
                                // x[dhhdrv3.bitlenX/8] - not present on SetKeyParam with KP_PUB_PARAMS.
}
  PRIVATE_SIG_BLOB_V3, *PPRIVATE_SIG_BLOB_V3;

#endif

// In each case, if you export a blob encrypted you encrypt everything
// except the initial PUBLICKEYSTRUC.
#define PRIVATEBLOB_UNENCRYPTED_SIZE sizeof(PUBLICKEYSTRUC)

typedef struct
{
   PUBLICKEYSTRUC blobhdr;
   int alg_id;
   unsigned char keyblk[1];
}
  SIMPLE_KEY_BLOB, *PSIMPLE_KEY_BLOB;

typedef struct
{
  BLOBHEADER blobhdr;
  DWORD cbData;
  unsigned char pbData[1];
} PLAINTEXT_KEY_BLOB, *PPLAINTEXT_KEY_BLOB;

#define SIMPLEBLOB_UNENCRYPTED_SIZE sizeof(PUBLICKEYSTRUC)
#define SIMPLE_HEADER_SIZE (sizeof(PUBLICKEYSTRUC) + sizeof(int))

#define MAX_BLOB_SIZE (2324)	/* 4Kbit private key blob (8+12+9*4096/16) */

/* Exports from NCSP.C which are used internally ----------- */


#define CLEAR(ptr,len) memset(ptr,0,len)

void check_init(void);

extern BOOL CheckPKReady(PKEY_OBJ pK);

extern PKEY_OBJ CreateKeyObj ( PCONTAINER pC,
                               unsigned int key_size,
                               unsigned int IV_size,
                               unsigned int fn_ctx_size,
                               unsigned int extra_ctx_size,
                               unsigned int oeap_params_size,
                               BOOL global );

extern void DestroyKeyObj ( PKEY_OBJ pKO );
extern BOOL SwapKeyObj(PKEY_OBJ pK1, PKEY_OBJ pK2);

extern PHASH_OBJ CreateHashObj ( PCONTAINER pC,
	                         unsigned int context_size,
                                 unsigned int hashval_size, BYTE *preallocated );

extern void DestroyHashObj ( PHASH_OBJ pHO );

extern BOOL CPCreateHashNoLink(PCONTAINER pC, ALG_ID alg_id, HCRYPTHASH *phHash, BYTE *preallocated);
extern BOOL CPDestroyHashNoLink(HCRYPTHASH hHash);

extern void reverse_bytes(unsigned char *dst, unsigned char *src, int len);
extern BOOL build_pkcs1_block (PCONTAINER pC, int blktype, unsigned char *buf, int srclen,
			       int maxlen );

extern PKEY_OBJ GetUserKey(PCONTAINER pC, int keynum, BOOL forcereload);

extern BOOL getlengths(ALG_ID algid, unsigned int *minlen, unsigned int *maxlen, unsigned int *defaultlen);

/* Exports from pubfns.c */

extern BOOL ExportPublicKey(PKEY_OBJ pK, HCRYPTKEY hExportKey, unsigned char *pbData,
                            DWORD *pdwDataLen, DWORD dwFlags);
extern BOOL ExportPrivateKey(PKEY_OBJ pK, HCRYPTKEY hExportKey, unsigned char *pbData,
                             DWORD *pdwDataLen);
extern BOOL ExportSimpleKey(PKEY_OBJ pK, HCRYPTKEY hExportKey, unsigned char *pbData,
                            DWORD *pdwDataLen, DWORD dwFlags);

extern BOOL ImportPublicKey(PCONTAINER pC, unsigned char *pBuffer, unsigned int len, HCRYPTKEY *phkey);
extern BOOL ImportPrivateKey(PCONTAINER pC, unsigned char *pBuffer, unsigned int len,
                             HCRYPTKEY *phkey, BOOL save_it, BOOL exportable);
extern BOOL ImportSimpleKey(PCONTAINER pC, PSIMPLE_KEY_BLOB pSKB, unsigned int len,
                             HCRYPTKEY *phkey, HCRYPTKEY decrypt_key, unsigned flags );

extern BOOL GenKey(PCONTAINER pC, ALG_ID Algid, DWORD dwFlags, unsigned int bitlen,
                      unsigned int newperm, PKEY_OBJ *pKout);

extern HRESULT SetPubKeyParam(PKEY_OBJ pKO, DWORD dwParam, const BYTE *data);
extern BOOL GetDDKeyParam(PKEY_OBJ pKO, DWORD dwParam, BYTE *keyblob, unsigned int blobsizein,
                          DWORD *len_out, void **copy_ptr);

extern BOOL ImportDHSharedSecret(PCONTAINER pC, unsigned char *pBuffer, unsigned int len,
                                 HCRYPTKEY hImportKey, HCRYPTKEY *phKey_out);

extern BOOL InitDHSharedSecret(PKEY_OBJ pKO, ALG_ID algid, DWORD dwFlags);


/* Exports from SymAlgs.C -------------------------------- */

#define MAX_IV_LEN      8

extern void dbg_key(PKEY_OBJ pKO);

extern void xorblock(char *out, char *in1, char *in2, unsigned int length);

extern HRESULT SetCryptKeyParam ( PKEY_OBJ pKO, DWORD dwParam, const BYTE *data );

extern HRESULT SetSSLKeyParam ( PKEY_OBJ pKO, DWORD dwParam, const BYTE *data );

extern HRESULT MakeSSLKey ( PCONTAINER pC, int alg_id, unsigned char *init_data, unsigned int len,
	                    DWORD flags, PKEY_OBJ *pKout );

extern HRESULT GetMasterSecret(PKEY_OBJ pKO, char *masterblk);

extern HRESULT DeriveSSLKey( PCONTAINER pC, ALG_ID Algid, PHASH_OBJ pH, DWORD dwFlags, PKEY_OBJ *pK_out);

extern BOOL SetTLSFinishedParam(PHASH_OBJ pHO, DWORD dwParam, CRYPT_DATA_BLOB *datablob);

extern BOOL GetTLSFinishedParam(PCONTAINER pC, PHASH_OBJ pHO, unsigned char *buffer, int length);

extern PKEY_OBJ CreateMaxKeyObj(PCONTAINER pC, unsigned int extra_ctx_size);

extern HRESULT MakeSymKey ( PCONTAINER pC, ALG_ID alg_id, unsigned char *init_data,
			    unsigned int len, int use_salt, PKEY_OBJ *pKout, unsigned int bitlen );

extern HRESULT PostInitSymKey(PKEY_OBJ pKO, unsigned int secretlenbits, unsigned int saltlenbytes,
                              ALG_ID alg_id);

extern HRESULT UpdateSymKey( unsigned char *init_data, unsigned int len, PKEY_OBJ pKout);

extern HRESULT InitSymKey ( PKEY_OBJ pKO );
extern HRESULT InitFunctionPointers ( PKEY_OBJ pKO );

extern HRESULT SymEncryptDecrypt ( PKEY_OBJ pKO, unsigned char *data,
		                   DWORD *len_in_out, DWORD out_max, int final, int decrypt );

extern HRESULT MakeHash ( PCONTAINER pC, int alg_id, PKEY_OBJ pKO, PHASH_OBJ *pHout, BYTE *preallocated );

extern HRESULT FinaliseHash ( PHASH_OBJ pH );

extern BOOL MAC_Duplicate(HCRYPTPROV hProv, PMAC_STATE oldstate, PMAC_STATE newstate);

extern HRESULT HMAC_Setup(PHASH_OBJ state, PHMAC_INFO info);
extern BOOL HMAC_Duplicate(HCRYPTPROV hProv, PHMAC_STATE oldstate, PHMAC_STATE newstate);


extern BOOL PRF(PCONTAINER pC,
         unsigned char *secret, int secretlen, unsigned char *label, int labellen, unsigned char *seed, int seedlen,
         unsigned char *result, int resultlen);


/* In StubAlgs.C or SymAlgs.C depending on USE_NFAST_CLIENT */
extern HRESULT GenerateRandom ( PCONTAINER pC, unsigned char *seed, unsigned int seedlen,
		                unsigned char *outbuf, unsigned int outlen );

#define Randomise(pC, seed)

/* ------------------------------------------------------ */
/* SGC cert checking code, from Jeff, in sgccheck.c */

extern BOOL SPQueryCFLevel(PCCERT_CONTEXT pCertContext, BYTE *pbExchKeyMod, DWORD cbExchKeyMod,
                           DWORD dwExchKeyExpo, DWORD *pdwSGCFlags);


/* Our exports ------------------------------------------- */

#define CRYPT_SET_KEY_DATA 0x0000C000

/* All these have moved; we're using the proper CSP DK header files now. */

#endif
