// csscsp.c : Defines the exported functions for the CSP.
//

#include "stdafx.h"
#include <stdio.h>
#include <wincrypt.h>
#include "cspdk.h"

#include "csspipe.h"
#include "certstore.h"
#include "cssutil.h"
#include "../csscommon/csslog.h"

PCSS_CERT g_pCssCerts = NULL;
DWORD	  g_nCssCerts = 0;

PCSS_CERT g_pCssSecondaryCerts = NULL;
DWORD	  g_nCssSecondaryCerts = 0;

// certenrolls_begin -- PROV_ENUMALGS_EX
PROV_ENUMALGS_EX myAlgs[1] = {{ CALG_SHA_256 , 256, 256, 256, 0, 7, "SHA256", 7, "SHA256"}};

#define MY_ENCODING_TYPE  (PKCS_7_ASN_ENCODING | X509_ASN_ENCODING)

static BOOL GetSigningCert(LPCSTR pszContainer, LPCSTR* ppszEncodedCert, PCCERT_CONTEXT *ppCertContext, BOOL secondaryFlag)
{
	DWORD i;

	PCSS_CERT pCssCerts = secondaryFlag?g_pCssSecondaryCerts:g_pCssCerts;
	DWORD	  nCssCerts = secondaryFlag?g_nCssSecondaryCerts:g_nCssCerts;

	for ( i= 0; i < nCssCerts; i++ )
	{
		DebugOut( "two strings: %s****%s****%d****n:%d", pCssCerts[i].szAlias, pszContainer, i, nCssCerts);

		if( !strcmp( pCssCerts[i].szAlias, pszContainer) )
		{
			*ppszEncodedCert = pCssCerts[i].pszEncodedCert;
			*ppCertContext = pCssCerts[i].pCertContext;
			DebugOut( "cert found out!!!");
			return TRUE;
		}
		DebugOut( "after compare");

	}
	return FALSE;
}

/******************************************************************************
* FindSigningCert
*
* DESCRIPTION :     Locates the requested signing cert in the My cert store
*
* INPUTS :
*            LPCWSTR pszContainer           Name of the key container
*            BOOL secondaryFlag             True of the secondary CSP is used
* OUTPUTS :
*            LPCSTR* ppszEncodedCert        Cert encoded as a string, must be 
*                                             by caller freed 
*            PCCERT_CONTEXT *ppCertContext  Returns the context of the cert,
*                                             must be freed by caller
* RETURN :
*            TRUE                           The function was successful.
*/
static BOOL FindSigningCert(LPCSTR pszContainer, LPCSTR* ppszEncodedCert, 
                            PCCERT_CONTEXT *ppCertContext, BOOL secondaryFlag)
{
	HANDLE           hCertStore = NULL;        
	PCCERT_CONTEXT   pCertContext=NULL;  
    DWORD            dwPropId = CERT_KEY_PROV_INFO_PROP_ID;
    BOOL             fMore = TRUE;
	CRYPT_KEY_PROV_INFO *pCryptKeyProvInfo = NULL;
	DWORD cbData;
    size_t converted             = 0;
    const size_t newsize         = 100;
    wchar_t *pszWideContainer    = NULL;
    size_t containerLen          = 0; 
    BOOL rc                      = FALSE;
    LPSTR szEncodedCert          = NULL;

	LPCWSTR storeName = L"MY";

    containerLen = strlen(pszContainer) + 1; 
    pszWideContainer = (wchar_t *)HeapAlloc(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, containerLen * sizeof(WORD) );
    mbstowcs_s(&converted, pszWideContainer, containerLen, pszContainer, _TRUNCATE);

    // find the certificate in the store
	// Open the named System Store
	if( !(hCertStore = CertOpenStore( CERT_STORE_PROV_SYSTEM, 0, (HCRYPTPROV_LEGACY)NULL, CERT_SYSTEM_STORE_CURRENT_USER, storeName )) )
	{
		LogError( "Unable to open the LOCAL_MACHINE certificate store");
		goto cleanup;
	}

    while (fMore && (pCertContext = CertFindCertificateInStore(hCertStore, 0, 0,
                               CERT_FIND_PROPERTY, &dwPropId, pCertContext))) 
    {
		DWORD dwPropId = CERT_KEY_PROV_INFO_PROP_ID;
		if( !CertGetCertificateContextProperty( pCertContext, dwPropId, NULL, &cbData ) )
		{
			LogError( "CertGetCertificateContextProperty CERT_KEY_PROV_INFO_PROP_ID:");
			goto cleanup;
		}
		
        pCryptKeyProvInfo = (CRYPT_KEY_PROV_INFO *)HeapAlloc(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, cbData);
		if (!CertGetCertificateContextProperty( pCertContext, dwPropId, pCryptKeyProvInfo, &cbData) )
		{
			LogError( "CertGetCertificateContextProperty failed.");
			goto cleanup;
        }

        // first check the container name
        if (wcscmp(pCryptKeyProvInfo->pwszContainerName, pszWideContainer) == 0) 
        {
// 			if ((!secondaryFlag) && ( 0 == wcscmp( pCryptKeyProvInfo->pwszProvName,LCSSCSP_NAME )) ||
//                (secondaryFlag) && (0 == wcscmp( pCryptKeyProvInfo->pwszProvName, LCSSCSP_SECONDARY_NAME )))
 			if (( 0 == wcscmp( pCryptKeyProvInfo->pwszProvName,LCSSCSP_NAME )) ||
                (0 == wcscmp( pCryptKeyProvInfo->pwszProvName, LCSSCSP_SECONDARY_NAME )))
			{
                DWORD cchString = 0;

                // we have the key that we are looking for
                *ppCertContext = CertDuplicateCertificateContext(pCertContext);
                if (*ppCertContext == NULL) 
                {
                    LogError("CertDuplicateCertificateContext failed.");
                    goto cleanup;
                }

                // the encoded cert is referenced in the DoSign operation, so return a pointer to it
                if (CryptBinaryToString(pCertContext->pbCertEncoded, pCertContext->cbCertEncoded, 0, NULL, &cchString) == FALSE) 
                {
                    LogError("CryptBinaryToString failed.");
                    goto cleanup;
                }

                szEncodedCert = (LPSTR)HeapAlloc(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, cchString);

                if (CryptBinaryToString(pCertContext->pbCertEncoded, pCertContext->cbCertEncoded, CRYPT_STRING_BASE64, szEncodedCert, &cchString) == FALSE) 
                {
                    LogError("CryptBinaryToString failed.");
                    goto cleanup;
                }
                *ppszEncodedCert = szEncodedCert;

                // DONE
                rc = TRUE;
                break;
            }
        }
    }
cleanup:

    if (pCryptKeyProvInfo) 
    {
        HeapFree(GetProcessHeap(), 0, pCryptKeyProvInfo);
        pCryptKeyProvInfo = NULL;
    }

    if (hCertStore) 
    {
        CertCloseStore(hCertStore, 0);
    }

    if (pszWideContainer) 
    {
        HeapFree(GetProcessHeap(), 0, pszWideContainer);
        pszWideContainer = NULL;
    }
    
    if (!rc && szEncodedCert)
    {
        HeapFree(GetProcessHeap(), 0, szEncodedCert);
        szEncodedCert = NULL;
        *ppszEncodedCert = NULL;
    }

	return rc;
}


//******************************************************************************
//   CPAcquireContext
//
//
//******************************************************************************
BOOL WINAPI CPAcquireContext( HCRYPTPROV *phProv, const char *pszContainer,
				 DWORD dwFlags, PVTableProvStruc pVTable )
{
	BOOL rc = FALSE;
	PCONTAINER pCont = NULL;

    DebugOut("CPAcquireContext container: %s", pszContainer ? pszContainer : "NULL" );
	DebugOut("CPAcquireContext provider : %s", pVTable->pszProvName );

    pCont = (PCONTAINER)HeapAlloc(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS | HEAP_ZERO_MEMORY, sizeof(CONTAINER));

	// check if it is secondary provider
	if (pVTable->pszProvName != NULL && strcmp( pVTable->pszProvName, CSSCSP_SECONDARY_NAME) == 0 )
    {
		pCont->secondaryFlag = TRUE;
    }
	else
    {
		pCont->secondaryFlag = FALSE;
    }

    if (pszContainer != NULL)
	{
        // we have been called with a container name, get the signing cert from the Windows store.
        if (!FindSigningCert(pszContainer, &pCont->pszEncodedCert, &pCont->pCertContext, pCont->secondaryFlag) )
	    {
		    SetLastError(NTE_NOT_FOUND);
		    LogError("Unable to find requested signing certificate.");
            goto cleanup;
	    }
		strcpy_s((char*)pCont->szContainer, sizeof(pCont->szContainer), (const char*)pszContainer);
    }

    //---------------------------------------------------------------
    // Get a handle to the default PROV_RSA_FULL provider.
    if( CryptAcquireContext(
			&pCont->hProv, 
			NULL, 
			NULL, 
			PROV_RSA_FULL, 
			CRYPT_VERIFYCONTEXT) == FALSE)
    {
		LogError("CPAcquireContext PROV_RSA_FULL failed");
        goto cleanup;
    }

    *phProv = (HCRYPTPROV)pCont;
    pCont = NULL;
    rc = TRUE;

cleanup:

    if (pCont)
    {
        // use the release context function to clean up
        CPReleaseContext((HCRYPTPROV)pCont, 0);
    }
	return rc;
}

//******************************************************************************
//   CPRefreshStore
//
//
//******************************************************************************
BOOL WINAPI CPRefreshStore( const char *pszProvName )
{
    BOOL rc = FALSE;
    BOOL secondaryFlag = FALSE;

    // check if it is secondary provider
	if (pszProvName != NULL && strcmp( pszProvName, CSSCSP_SECONDARY_NAME) == 0 )
    {
		secondaryFlag = TRUE;
    }
	else
    {
		secondaryFlag = FALSE;
    }

	if( !GetAuthorizedSigningCertificates( NULL, NULL, secondaryFlag) )
	{
		DebugOut("Unable get signing certificate list from CodeSign server.");
	}
    else
    {
        rc = TRUE;
    }

    return rc;
}

BOOL WINAPI CPReleaseContext( HCRYPTPROV hProv, DWORD dwFlags )
{
    PCONTAINER pCont = (PCONTAINER)hProv;

    DebugOut("CPReleaseContext");

    if (pCont == NULL)
    {
        SetLastError(ERROR_INVALID_PARAMETER);
        return FALSE;
    }

    // make sure this handle is at least in the proper heap
    if (HeapValidate(GetProcessHeap(), 0, pCont) == FALSE)
    {
 		return FALSE;
    }

    //release the crypto context
    if (pCont->hProv)
    {
        CryptReleaseContext(pCont->hProv, dwFlags);
    }

    // release the cert context
    if (pCont->pCertContext)
    {
        CertFreeCertificateContext(pCont->pCertContext);
    }

    // release the cert string
    if (pCont->pszEncodedCert)
    {
        HeapFree(GetProcessHeap(), 0, pCont->pszEncodedCert);
    }

    ZeroMemory(pCont, sizeof(CONTAINER));

    HeapFree(GetProcessHeap(), 0, pCont);
	
    return TRUE;
}

BOOL WINAPI CPGenRandom( HCRYPTPROV hProv, DWORD dwLen, unsigned char *pbBuffer )
{
	DebugOut("CPGenRandom");
	SetLastError(ERROR_NOT_CAPABLE);
    return FALSE;
}

BOOL WINAPI CPCreateHash( HCRYPTPROV hProv, ALG_ID alg_id, HCRYPTKEY hKey,
			     DWORD dwFlags, HCRYPTHASH *phHash )
{
	BOOL rc = FALSE;
	PCONTAINER pCont = (PCONTAINER)hProv;
	PHASH_HANDLE pHash;
	DebugOut("CPCreateHash");

    if (pCont == NULL)
    {
        SetLastError(ERROR_INVALID_PARAMETER);
        return FALSE;
    }
	
    pHash = (PHASH_HANDLE)HeapAlloc(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, sizeof(HASH_HANDLE));
	if( (rc = CryptCreateHash(pCont->hProv, alg_id, hKey, dwFlags, &pHash->hHash)))
	{
		*phHash = (HCRYPTHASH)pHash;
	}
	return rc;
}

BOOL WINAPI CPDuplicateHash( HCRYPTPROV hProv, HCRYPTHASH hHash,
			       DWORD *pdwReserved, DWORD dwFlags, HCRYPTHASH *phHash )
{
	PCONTAINER pCont = (PCONTAINER)hProv;
	PHASH_HANDLE pHash = (PHASH_HANDLE)hHash;

    DebugOut("CPDuplicateHash");

    if (pCont == NULL)
    {
        SetLastError(ERROR_INVALID_PARAMETER);
        return FALSE;
    }

	return CryptDuplicateHash(pHash->hHash, pdwReserved, dwFlags, phHash);
}

BOOL WINAPI CPGetHashParam( HCRYPTPROV hProv,
			       HCRYPTHASH hHash,
			       DWORD dwParam,
			       unsigned char *pbData,
			       DWORD *pdwDataLen,
			       DWORD dwFlags )
{
	PHASH_HANDLE pHash = (PHASH_HANDLE)hHash;
	DebugOut("CPGetHashParam dwParam=%ld", dwParam);

    if (pHash == NULL)
    {
        SetLastError(ERROR_INVALID_PARAMETER);
        return FALSE;
    }
	return CryptGetHashParam(pHash->hHash, dwParam, pbData, pdwDataLen, dwFlags);	
}

BOOL WINAPI CPSetHashParam( HCRYPTPROV hProv,
			       HCRYPTHASH hHash,
			       DWORD dwParam,
			       const BYTE *pbData,
			       DWORD dwFlags )
{
	PHASH_HANDLE pHash = (PHASH_HANDLE)hHash;
	DebugOut("CPSetHashParam");
    if (pHash == NULL)
    {
        SetLastError(ERROR_INVALID_PARAMETER);
        return FALSE;
    }
	return CryptSetHashParam(pHash->hHash, dwParam, pbData, dwFlags);	
}

BOOL WINAPI CPHashData( HCRYPTPROV hProv,
			   HCRYPTHASH hHash,
			   const BYTE *pbData,
			   DWORD dwDataLen,
			   DWORD dwFlags )
{
	PHASH_HANDLE pHash = (PHASH_HANDLE)hHash;
	DebugOut("CPHashData");
    if (pHash == NULL)
    {
        SetLastError(ERROR_INVALID_PARAMETER);
        return FALSE;
    }
    return CryptHashData(pHash->hHash, pbData, dwDataLen, dwFlags);
}

BOOL WINAPI CPHashSessionKey( HCRYPTPROV hProv,
				 HCRYPTHASH hHash,
				 HCRYPTKEY hKey,
				 DWORD dwFlags )
{
	DebugOut("CPHashSessionKey");
	SetLastError(ERROR_NOT_CAPABLE);
    return FALSE;
}

BOOL WINAPI CPDestroyHash( HCRYPTPROV hProv, HCRYPTHASH hHash )
{
	PHASH_HANDLE pHash = (PHASH_HANDLE)hHash;
	BOOL rc = FALSE;

    if (pHash == NULL)
    {
        SetLastError(ERROR_INVALID_PARAMETER);
        return FALSE;
    }

    // make sure this handle is at least in the proper heap
    if (HeapValidate(GetProcessHeap(), 0, pHash) == FALSE)
    {
		return FALSE;
    }

	DebugOut("CPDestroyHash");
    if (pHash->hHash != 0) 
    {
        rc = CryptDestroyHash(pHash->hHash);
    }
        
    HeapFree(GetProcessHeap(), 0, pHash);
    pHash = NULL;

	return rc;
}

BOOL WINAPI CPSignHash( HCRYPTPROV hProv,
			   HCRYPTHASH hHash,
			   DWORD dwKeySpec,
			   LPCWSTR sDescription,
			   DWORD	dwFlags,
			   unsigned char *pbSignature,
			   DWORD *pdwSigLen )
{
	PCONTAINER pCont = (PCONTAINER)hProv;
	PHASH_HANDLE pHash = (PHASH_HANDLE)hHash;
	BYTE  pbHash[512];
	DWORD dwHashLen = sizeof(pbHash);
	BYTE * pbSig = NULL;
	BOOL rc = FALSE;
	unsigned int algId;
	DWORD algIdLen = sizeof(algId);

	DebugOut("CPSignHash");

    if (pCont == NULL || pHash == NULL)
    {
        SetLastError(ERROR_INVALID_PARAMETER);
        return FALSE;
    }

	if (!CryptGetHashParam(pHash->hHash, HP_ALGID,(BYTE *) &algId, &algIdLen, 0))
	{
		// we are going to rely on the SetLastError call inside of CryptGetHashParam
		DebugOut("CryptGetHashParam failed when retreiving the hash algorithm.");
		return FALSE;
	}

    if( CryptGetHashParam( pHash->hHash, HP_HASHVAL, pbHash, &dwHashLen, 0 ) )
	{
        rc = ThalesCSPSign(pCont, pbHash, dwHashLen, algId, pbSignature, pdwSigLen);
	}

	return rc;
}

BOOL WINAPI CPSignHashValue( HCRYPTPROV hProv,
			   BYTE  *pbHash,
			   DWORD dwHashLen,
			   LPCWSTR sDescription,
			   DWORD	dwFlags,
			   unsigned char *pbSignature,
			   DWORD *pdwSigLen )
{
	PCONTAINER pCont = (PCONTAINER)hProv;
	BYTE * pbSig = NULL;
	BOOL rc = FALSE;
	int hashAlg;

	DebugOut("CPSignHash");
    if (pCont == NULL)
    {
        SetLastError(ERROR_INVALID_PARAMETER);
        return FALSE;
    }

	switch (dwHashLen)
	{
	case 20:
		hashAlg = CALG_SHA1;
		break;
	case 32:
		hashAlg = CALG_SHA_256;
		break;
	case 48:
		hashAlg = CALG_SHA_384;
		break;
	case 64:
		hashAlg = CALG_SHA_512;
		break;
	default:
		// unknown hash alg, error
		DebugOut("CSSKSPSignHash failed, unknown hash algorithm used: %d", dwHashLen);
		SetLastError(ERROR_INVALID_PARAMETER);
		return FALSE;
		break;
	}

    rc = ThalesCSPSign(pCont, pbHash, dwHashLen, hashAlg, pbSignature, pdwSigLen);

    return rc;
}

BOOL WINAPI CPDeriveKey( HCRYPTPROV hProv, ALG_ID Algid, HCRYPTHASH hBaseData, DWORD dwFlags,
                            HCRYPTKEY *phKey )
{
	DebugOut("CPDeriveKey");
	SetLastError(ERROR_NOT_CAPABLE);
    return FALSE;
}

BOOL WINAPI CPGenKey( HCRYPTPROV hProv, ALG_ID Algid, DWORD dwFlags, HCRYPTKEY *phKey )
{
	DebugOut("CPGenKey");
	SetLastError(ERROR_NOT_CAPABLE);
    return FALSE;
}

BOOL WINAPI CPDuplicateKey( HCRYPTPROV hProv, HCRYPTKEY hKey,
			      DWORD *pdwReserved, DWORD dwFlags, HCRYPTKEY *phKey )
{
	DebugOut("CPDuplicateKey");
	SetLastError(ERROR_NOT_CAPABLE);
    *phKey = 0x45;
    return TRUE;
}

BOOL WINAPI CPGetKeyParam( HCRYPTPROV hProv, HCRYPTKEY hKey,
			      DWORD dwParam, unsigned char *pbData, DWORD *pdwDataLen,
			      DWORD dwFlags )
{
    BOOL bRetVal = FALSE;
	DebugOut("CPGetKeyParam");
    switch (dwParam) {
    case KP_ALGID:
        *pdwDataLen = (DWORD)sizeof(DWORD);
        if (pbData != NULL)
        {
            *pbData = (unsigned char)CALG_RSA_KEYX;
        }
        bRetVal = TRUE;
        break;
    default:
    	SetLastError(ERROR_NOT_CAPABLE);
        break;
    }

    return bRetVal;
}

BOOL WINAPI CPSetKeyParam( HCRYPTPROV hProv, HCRYPTKEY hKey,
			      DWORD dwParam, const BYTE *pbData, DWORD dwFlags )
{
	DebugOut("CPSetKeyParam");
	SetLastError(ERROR_NOT_CAPABLE);
    return FALSE;
}

BOOL WINAPI CPEncrypt( HCRYPTPROV hProv,
			  HCRYPTKEY hKey,
			  HCRYPTHASH hHash,
			  BOOL final,
			  DWORD dwflags,
			  unsigned char *pbdata,
			  DWORD *pdwDataLen,
			  DWORD dwBufLen )
{
	PCONTAINER pCont = (PCONTAINER)hProv;
	PHASH_HANDLE pHash = (PHASH_HANDLE)hHash;
	BYTE  pbHash[512];
	DWORD dwHashLen = sizeof(pbHash);
	BYTE * pbSig = NULL;
    BOOL rc = FALSE;

    DebugOut("CPEncrypt");
	SetLastError(ERROR_NOT_CAPABLE);
    return FALSE;
}

BOOL WINAPI CPDecrypt( HCRYPTPROV hProv,
			  HCRYPTKEY hKey,
			  HCRYPTHASH hHash,
			  BOOL final,
			  DWORD dwflags,
			  unsigned char *pbdata,
			  DWORD *pdwDataLen )
{
	DebugOut("CPDecrypt");
	SetLastError(ERROR_NOT_CAPABLE);
    return FALSE;
}

BOOL WINAPI CPDestroyKey( HCRYPTPROV hProv, HCRYPTKEY hKey )
{
	DebugOut("CPDestroyKey");
//	SetLastError(ERROR_NOT_CAPABLE);
    return TRUE;
}

BOOL WINAPI CPImportKey( HCRYPTPROV hProv,
			    const BYTE *pbData,
			    DWORD dwDataLen,
			    HCRYPTKEY hImportKey,
			    DWORD dwFlags,
			    HCRYPTKEY *phKey_out )
{
	PCONTAINER pCont = (PCONTAINER)hProv;
	BOOL rc = FALSE;

	DebugOut("CPImportKey");

    if (pCont == NULL)
    {
        SetLastError(ERROR_INVALID_PARAMETER);
        return FALSE;
    }

    rc = CryptImportKey(pCont->hProv, pbData, dwDataLen, hImportKey, dwFlags, phKey_out);
    return rc;
}

BOOL WINAPI CPExportKey( HCRYPTPROV hProv,
			    HCRYPTKEY hKey,
			    HCRYPTKEY hExportKey,
			    DWORD dwBlobType,
			    DWORD dwFlags,
			    unsigned char *pbData,
			    DWORD *pdwDataLen )
{
	PCONTAINER pCont = (PCONTAINER)hProv;
	HCRYPTKEY hPubKey;
	BOOL rc = FALSE;
		
	DebugOut("CPExportKey");

    if (pCont == NULL)
    {
        SetLastError(ERROR_INVALID_PARAMETER);
        return FALSE;
    }

	if( dwBlobType != PUBLICKEYBLOB )
	{
		DebugOut("CPExportKey: PUBLICKEYBLOB only");
		return FALSE;
	}

	if( pCont->pCertContext && CryptImportPublicKeyInfo(pCont->hProv, MY_ENCODING_TYPE, &pCont->pCertContext->pCertInfo->SubjectPublicKeyInfo, &hPubKey) )
	{
		rc = CryptExportKey(hPubKey, 0, dwBlobType, dwFlags, pbData, pdwDataLen);
		CryptDestroyKey(hPubKey);
	}

	return rc;
}

BOOL WINAPI CPGetUserKey( HCRYPTPROV hProv, DWORD dwKeySpec, HCRYPTKEY *phUserKey )
{
	PCONTAINER pCont = (PCONTAINER)hProv;

	DebugOut("CPGetUserKey dwKeySpec=%s", dwKeySpec == AT_KEYEXCHANGE ? "AT_KEYEXCHANGE" : "AT_SIGNATURE");

    if (pCont == NULL)
    {
        SetLastError(ERROR_INVALID_PARAMETER);
        return FALSE;
    }

	// Give back a meaningless pointer to keep the caller happy.
	// We ignore CPDestroyKey calls.
	*phUserKey = (HCRYPTKEY)pCont->pCertContext;
    return TRUE;
}

BOOL WINAPI CPVerifySignature( HCRYPTPROV hProv,
				  HCRYPTHASH hHash,
				  const BYTE *pbSignature,
				  DWORD dwSigLen,
				  HCRYPTKEY hPubKeyIgnored,
				  LPCWSTR sDescription,
				  DWORD dwFlags )
{
	PHASH_HANDLE pHash = (PHASH_HANDLE)hHash;
	PCONTAINER pCont = (PCONTAINER)hProv;
    BOOL rc = FALSE;
    HCRYPTKEY hPubKey;

	DebugOut("CPVerifySignature");

    if (pCont == NULL || pHash == NULL)
    {
        SetLastError(ERROR_INVALID_PARAMETER);
        return FALSE;
    }

    if( pCont->pCertContext && 
        CryptImportPublicKeyInfo(pCont->hProv, MY_ENCODING_TYPE, &pCont->pCertContext->pCertInfo->SubjectPublicKeyInfo, &hPubKey) )
	{
        rc = CryptVerifySignature(pHash->hHash, pbSignature, dwSigLen, hPubKey, NULL, dwFlags);
        if (rc == FALSE)
        {
            LogError("Failed to verify signature: 0x%08x", GetLastError());
        }
		CryptDestroyKey(hPubKey);
	}

    return rc;
}

BOOL WINAPI CPGetProvParam( HCRYPTPROV hProv, DWORD dwParam,
			       unsigned char *pbData, DWORD *pdwDataLen, DWORD dwFlags )
{
	DWORD len = 0;
	unsigned char *data;
	DWORD dwData;
	PCONTAINER pCont = (PCONTAINER)hProv;
    PROV_ENUMALGS *pEnumAlgs = NULL;
    static DWORD dwCount;

	DebugOut("CPGetProvParam dwParam=%ld", dwParam);

    if (pCont == NULL)
    {
        SetLastError(ERROR_INVALID_PARAMETER);
        return FALSE;
    }

	switch(dwParam)
	{
	case PP_ENUMALGS:

        // this is a bit of a hack since we return mid function...  
        // We return two algs to support verification that certutil
        // performs
        pEnumAlgs = (PROV_ENUMALGS *)pbData;
        if (pEnumAlgs && (dwFlags & CRYPT_FIRST))
        {
            dwCount = 1;
            pEnumAlgs->aiAlgid = ALG_TYPE_RSA|ALG_SID_RSA_ANY;
            pEnumAlgs->dwBitLen = 2048;
            pEnumAlgs->dwNameLen = 3;
            pEnumAlgs->szName[0] = 'R'; 
            pEnumAlgs->szName[1] = 'S'; 
            pEnumAlgs->szName[2] = 'A'; 
            pEnumAlgs->szName[3] = 0; 
            return TRUE;
        }
        else if (dwCount == 1)
        {
            dwCount++;
            pEnumAlgs->aiAlgid = CALG_SHA1;
            pEnumAlgs->dwBitLen = 20;
            pEnumAlgs->dwNameLen = 0;
            pEnumAlgs->szName[0] = 0; 
            return TRUE;
        }
        else
        {
            SetLastError(ERROR_NO_MORE_ITEMS);
            return FALSE;
        }
        break;
	case PP_ENUMCONTAINERS:
		break;
	case PP_IMPTYPE:
		len = sizeof(DWORD);
		dwData = CRYPT_IMPL_MIXED;
		data = (unsigned char*)&dwData;
		break;	
	case PP_NAME:
		if ( pCont->secondaryFlag )
		{
			len = (int)strlen(CSSCSP_SECONDARY_NAME)+1;
			data = (unsigned char*)CSSCSP_SECONDARY_NAME;
		}
		else
		{
			len = (int)strlen(CSSCSP_NAME)+1;
			data = (unsigned char*)CSSCSP_NAME;
		}
		break;
	case PP_VERSION:
		len = sizeof(DWORD);
		dwData = 0x00000100;
		data = (unsigned char*)&dwData;
		break;
	case PP_CONTAINER:
        len = (DWORD)strlen(pCont->szContainer) + 1;
        data = (unsigned char *)pCont->szContainer;
        break;
	case PP_CHANGE_PASSWORD:
        break;
	case PP_KEYSET_SEC_DESCR:
        break;
	case PP_CERTCHAIN:
        break;
	case PP_KEY_TYPE_SUBTYPE:
        break;
	case PP_PROVTYPE:        
		len = sizeof(DWORD);
		dwData = PROV_RSA_FULL;
		data = (unsigned char*)&dwData;
        break;
	case PP_KEYSTORAGE:
        break;
	case PP_APPLI_CERT:
        break;
	case PP_SYM_KEYSIZE:
        break;
	case PP_SESSION_KEYSIZE:
        break;
	case PP_UI_PROMPT:
        break;
	case PP_ENUMALGS_EX:
		if (dwFlags == CRYPT_FIRST) {
			len = sizeof(myAlgs);
			data = (unsigned char*)myAlgs;
		}
		break;	case PP_ENUMMANDROOTS:
	case PP_ENUMELECTROOTS:
        break;
	case PP_KEYSET_TYPE:
		len = sizeof(DWORD);
		dwData = 0;
		data = (unsigned char*)&dwData;
		break;
	case PP_ADMIN_PIN:
        break;
	case PP_KEYEXCHANGE_PIN:
        break;
	case PP_SIGNATURE_PIN:
        break;
	case PP_SIG_KEYSIZE_INC:
        break;
	case PP_KEYX_KEYSIZE_INC:
        break;
	case PP_UNIQUE_CONTAINER:
        break;
	case PP_SGC_INFO:
        break;
	case PP_USE_HARDWARE_RNG:
        break;
	case PP_KEYSPEC:		
		len = sizeof(DWORD);
		dwData = AT_SIGNATURE;
		data = (unsigned char*)&dwData;
		break;
	case PP_ENUMEX_SIGNING_PROT:
#if (NTDDI_VERSION >= NTDDI_WS03)
	case PP_CRYPT_COUNT_KEY_USE:
#endif //(NTDDI_VERSION >= NTDDI_WS03)
#if (NTDDI_VERSION >= NTDDI_WINLH)
	case PP_USER_CERTSTORE:
        break;
	case PP_SMARTCARD_READER:
        break;
	case PP_SMARTCARD_GUID:
        break;
	case PP_ROOT_CERTSTORE:
#endif //(NTDDI_VERSION >= NTDDI_WINLH)
		break;
	}
	if( len > 0 )
	{
		if( pbData )
		{
			if( *pdwDataLen >= len )
				memcpy( (char*)pbData, data, len );
		}
		*pdwDataLen = len;
		return TRUE;
	}
	return FALSE;
}

BOOL WINAPI CPSetProvParam( HCRYPTPROV hProv, DWORD dwParam,
			       const BYTE *pbData, DWORD dwFlags )
{
	DebugOut("CPSetProvParam");
	SetLastError(ERROR_NOT_CAPABLE);
    return FALSE;
}


BOOL WINAPI DllMain(HINSTANCE hinstDLL, DWORD fdwReason, LPVOID reserved)
{
    switch(fdwReason)
    {
    case DLL_PROCESS_ATTACH:
		DebugOut("CSSCSP.DLL was loaded");
		InitGlobals();
        break;

    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

