#pragma once

#include "cssutil.h"

#ifdef __cplusplus
extern "C" {
#endif

// High level public functions
BOOL ThalesCSPGetCertList(char ** ppszCerts);
BOOL ThalesCSPSign(PCONTAINER pCont, BYTE *pbData, DWORD cbData, int hashAlgId, BYTE *pbSignature, DWORD *pcbSignature);

#ifdef __cplusplus
}
#endif
