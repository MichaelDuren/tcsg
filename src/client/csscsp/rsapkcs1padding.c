#include "stdafx.h"

#include <stdio.h>
#include <stdio.h>
#include <conio.h>
#include <tchar.h>
#include <string.h>

#include "rsapkcs1padding.h"

static const unsigned char md5sigheader[] =
{
   0x30, // SEQUENCE
   0x20, // LENGTH
     0x30, // SEQUENCE
     0x0C, // LENGTH
     0x06, 0x08, 0x2A, 0x86, 0x48, 0x86, 0xF7, 0x0D, 0x02, 0x05, 
     0x05, 0x00, // OPTIONAL ANY algorithm params (NULL)
     0x04, 0x10 // OCTET STRING (16 bytes)
};

static const unsigned char sha1sigheader[] =
{
  0x30, // SEQUENCE
  0x21, // LENGTH
    0x30, // SEQUENCE
    0x09, // LENGTH
    0x06, 0x05, 0x2B, 0x0E, 0x03, 0x02, 0x1a, // SHA1 OID (1 4 14 3 2 26)
      0x05, 0x00, // OPTIONAL ANY algorithm params (NULL)
    0x04, 0x14 // OCTET STRING (20 bytes)
};

static const unsigned char sha256sigheader[] =
{
  0x30, // SEQUENCE
  0x31, // LENGTH
    0x30, // SEQUENCE
    0x0d, // LENGTH
    0x06, 0x09, 0x60, 0x86, 0x48, 0x01, 0x65, 0x03, 0x04, 0x02, 0x01, // SHA1 OID (2 16 840 1 101 3 4 2 1)
      0x05, 0x00, // OPTIONAL ANY algorithm params (NULL)
    0x04, 0x20 // OCTET STRING (32 bytes)
};

static const unsigned char sha384sigheader[] =
{
  0x30, // SEQUENCE
  0x41, // LENGTH
    0x30, // SEQUENCE
    0x0d, // LENGTH
    0x06, 0x09, 0x60, 0x86, 0x48, 0x01, 0x65, 0x03, 0x04, 0x02, 0x02, // SHA1 OID (2 16 840 1 101 3 4 2 2)
      0x05, 0x00, // OPTIONAL ANY algorithm params (NULL)
    0x04, 0x30 // OCTET STRING (48 bytes)
};

static const unsigned char sha512sigheader[] =
{
  0x30, // SEQUENCE
  0x51, // LENGTH
    0x30, // SEQUENCE
    0x0d, // LENGTH
    0x06, 0x09, 0x60, 0x86, 0x48, 0x01, 0x65, 0x03, 0x04, 0x02, 0x03, // SHA1 OID (2 16 840 1 101 3 4 2 3)
      0x05, 0x00, // OPTIONAL ANY algorithm params (NULL)
    0x04, 0x40 // OCTET STRING (64 bytes)
};

static const unsigned char sha224sigheader[] =
{
  0x30, // SEQUENCE
  0x2d, // LENGTH
    0x30, // SEQUENCE
    0x0d, // LENGTH
    0x06, 0x09, 0x60, 0x86, 0x48, 0x01, 0x65, 0x03, 0x04, 0x02, 0x04, // SHA1 OID (2 16 840 1 101 3 4 2 4)
      0x05, 0x00, // OPTIONAL ANY algorithm params (NULL)
    0x04, 0x1c // OCTET STRING (28 bytes)
};

int encode_hash(unsigned char* out, DWORD *pcbout, const unsigned char* hash, DWORD cbhash)
{
	const unsigned char* prefix = NULL;
	size_t cbprefix = 0;
	size_t cbpadd;
	switch (cbhash) {
	case 16: prefix = md5sigheader; cbprefix = sizeof md5sigheader; break;
	case 20: prefix = sha1sigheader; cbprefix = sizeof sha1sigheader; break;
	case 32: prefix = sha256sigheader; cbprefix = sizeof sha256sigheader; break;
	case 48: prefix = sha384sigheader; cbprefix = sizeof sha384sigheader; break;
	case 64: prefix = sha512sigheader; cbprefix = sizeof sha512sigheader; break;
	case 28: prefix = sha224sigheader; cbprefix = sizeof sha224sigheader; break;
	default:
		return -1; // unrecognized hash size
	}

	memcpy(out, prefix, cbprefix);
	out += cbprefix;
	memcpy(out, hash, cbhash);
	*pcbout = cbprefix + cbhash + 1;

	return 0;
}


int emsa_pkcs1_v1_5_encode_hash(unsigned char* out, size_t cbout, const unsigned char* hash, size_t cbhash)
{
  const unsigned char* prefix = NULL;
  size_t cbprefix = 0;
  size_t cbpadd;
  switch (cbhash) {
    case 16: prefix = md5sigheader; cbprefix = sizeof md5sigheader; break;
    case 20: prefix = sha1sigheader; cbprefix = sizeof sha1sigheader; break;
    case 32: prefix = sha256sigheader; cbprefix = sizeof sha256sigheader; break;
    case 48: prefix = sha384sigheader; cbprefix = sizeof sha384sigheader; break;
    case 64: prefix = sha512sigheader; cbprefix = sizeof sha512sigheader; break;
    case 28: prefix = sha224sigheader; cbprefix = sizeof sha224sigheader; break;
    default:
      return -1; // unrecognized hash size
  }

  if (cbout < cbhash + cbprefix + 11)
    return -1; // message too big for key

  *(out++) = 0;
  *(out++) = 1;
  cbpadd = cbout - 3 - cbhash - cbprefix;
  memset(out, 0xff, cbpadd);
  out += cbpadd;
  *(out++) = 0;
  memcpy(out, prefix, cbprefix);
  out += cbprefix;
  memcpy(out, hash, cbhash);

  return 0;
}

void pkcs1_test()
{
  int i;
  unsigned char sha1[20] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0};
  unsigned char sha256[32] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2};
  unsigned char sha384[48] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8};
  unsigned char sha512[64] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8};
  unsigned char sha224[28] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8};

  unsigned char buf[256];

  printf("test 1: %d\n", emsa_pkcs1_v1_5_encode_hash(buf, sizeof(buf), sha1, sizeof(sha1)));
  for (i = 0; i < sizeof(buf); ++i) {printf("%02x ", buf[i]);if ((i+1)%16 == 0) printf("\n");}
  printf("test 1: %d\n", emsa_pkcs1_v1_5_encode_hash(buf, sizeof(buf), sha256, sizeof(sha256)));
  for (i = 0; i < sizeof(buf); ++i) {printf("%02x ", buf[i]);if ((i+1)%16 == 0) printf("\n");}
  printf("test 1: %d\n", emsa_pkcs1_v1_5_encode_hash(buf, sizeof(buf), sha384, sizeof(sha384)));
  for (i = 0; i < sizeof(buf); ++i) {printf("%02x ", buf[i]);if ((i+1)%16 == 0) printf("\n");}
  printf("test 1: %d\n", emsa_pkcs1_v1_5_encode_hash(buf, sizeof(buf), sha512, sizeof(sha512)));
  for (i = 0; i < sizeof(buf); ++i) {printf("%02x ", buf[i]);if ((i+1)%16 == 0) printf("\n");}
  printf("test 1: %d\n", emsa_pkcs1_v1_5_encode_hash(buf, sizeof(buf), sha224, sizeof(sha224)));
  for (i = 0; i < sizeof(buf); ++i) {printf("%02x ", buf[i]);if ((i+1)%16 == 0) printf("\n");}
}
