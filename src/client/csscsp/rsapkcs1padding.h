#pragma once

#ifdef __cplusplus
extern "C" {
#endif

int emsa_pkcs1_v1_5_encode_hash(unsigned char* out, size_t cbout, const unsigned char* hash, size_t cbhash);

#ifdef __cplusplus
}
#endif

