#include "stdafx.h"
#include <stdio.h>

#include "certstore.h"
#include "cssutil.h"
#include "csspipe.h"
// Globals

// Request the list of signing certificates assigned to our IP address and add
// them to the local machine personal cert store.
//
BOOL GetAuthorizedSigningCertificates(PCSS_CERT *ppCssCerts, DWORD *pnCssCerts, BOOL secondaryFlag )
{
	PCSS_CERT pCssCerts = NULL;
	char *pszCerts = NULL;
	BOOL rc = FALSE;

	// On success, pszCerts will contain: one line of ';' delimited aliases ,
	// and one line for each corresponding certificate delimited with lines of "--------"
	if( ThalesCSPGetCertList( &pszCerts ) )
	{
		char * pszCertContext;
		char * pszAliasContext;
		char * pszToken;
		char * pszAlias;
		char * pszCert;
		int    cAliases = 0;

		// Now that we got something back, clear out the store
		PurgeCertStore( secondaryFlag );

		// make sure we have at least enough data in pszCerts to cover the empty list string of "{}"
		if ((strlen(pszCerts) < 2) || (pszCerts[0] != '{'))
		{
			// error in formatting
		}

		// start from pszCerts[1]
		pszToken = strtok_s(pszCerts + 1, "}", &pszCertContext);
		while (pszToken != NULL)
		{
			// move past the "{" or the ",{"
			while (*pszToken != 0 && *pszToken == ',' && *pszToken != '{') ++pszToken;

			// each entry in the list is:  <alias>, <base64 cert>
			if (strlen(pszToken) < 2)
			{
				// there is nothing here to decode
				break;
			}
			else
			{
				pszAlias = strtok_s(pszToken + 1, ",", &pszAliasContext);
				// we have a pointer to the alias
				pszCert = strtok_s(NULL, ",", &pszAliasContext);

				if (strlen(pszAlias) && strlen(pszCert))
				{
					// looks like we have a keeper
					cAliases++;
					if (pCssCerts == NULL)
					{
						pCssCerts = (PCSS_CERT)HeapAlloc(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, sizeof(CSS_CERT));
					}
					else
					{
						pCssCerts = (PCSS_CERT)HeapReAlloc(GetProcessHeap(), HEAP_GENERATE_EXCEPTIONS, pCssCerts, cAliases * sizeof(CSS_CERT));
					}

					// copy in the alias to the cert record
					strcpy_s(pCssCerts[cAliases - 1].szAlias, sizeof(pCssCerts[cAliases - 1].szAlias), pszAlias);

					// then the certificate 
					PCCERT_CONTEXT pCert = NULL;
					if (DecodeCertificate(pszCert, &pCert))
					{
						AddCertContextToStore(pszAlias, pCert, secondaryFlag);
						pCssCerts[cAliases - 1].pszEncodedCert = _strdup(pszCert);
						pCssCerts[cAliases - 1].pCertContext = pCert;
					}
					else
					{
						// error decoding a certificate
						break;
					}
				}
			}
			pszToken = strtok_s(NULL, "}", &pszCertContext);
		}

		// Set return values
        if (ppCssCerts) 
        {
		    *ppCssCerts = pCssCerts;
		    *pnCssCerts = cAliases;
        }
        else 
        {
            HeapFree(GetProcessHeap(), 0, pCssCerts);
            pCssCerts = NULL;
        }

		rc = TRUE;
        HeapFree(GetProcessHeap(), 0, pszCerts);
        pszCerts = NULL;
	}
	return rc;
}
