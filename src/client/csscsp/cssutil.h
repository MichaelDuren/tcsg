#pragma once

#include <wincrypt.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _container {
	HCRYPTPROV hProv;
	const char szContainer[256];
	char *pszEncodedCert;
	PCCERT_CONTEXT pCertContext;
	BOOL secondaryFlag;
} CONTAINER, *PCONTAINER;

typedef struct _hash {
	HCRYPTHASH hHash;
} HASH_HANDLE, *PHASH_HANDLE;

typedef struct CSS_Cert 
{
	char szAlias[256];
	char *pszEncodedCert;
	PCCERT_CONTEXT pCertContext;
} CSS_CERT, *PCSS_CERT;

BOOL GetAuthorizedSigningCertificates(PCSS_CERT *ppCssCerts, DWORD *pnCssCerts, BOOL secondaryFlag );

void DebugOut(LPCSTR format, ...);
void LogError(LPCSTR format, ...);
void InitGlobals();

#ifdef __cplusplus
}
#endif
