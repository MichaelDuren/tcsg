using System;
using System.ServiceProcess;
using CSG.AppServer;
using CSG.AppServer.Managers;
using CSExecutive.Tasks;
using System.Threading;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using CSG.AppServer.Db;
using System.IO;

namespace CSExecutive
{
    public partial class CSExecutiveService : ServiceBase
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static bool initialized = false;

        // ********************************************************************************
        #region Standard Initialize / OnStart / OnStop logic
        // ********************************************************************************

        public CSExecutiveService()
        {
            InitializeComponent();

            if (!initialized)
            {
                // AppServer Initialization
                try
                {
                    AppServerInitializer.InitializeLoggingEnvironmentVariables();
                }
                catch (Exception ex)
                {
                    // nothing much we can do here if the logging environment fails.
                    throw ex;
                }

                log.Info("--> Running Application_Start");
            }
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                log.Info("--> Registering Tasks");
                RegisterTasks();
                log.Info("--> Starting Tasks");
                TaskManager.TaskManager.Start();
            }
            catch (Exception e)
            {
                string message = string.Format("Exception: {0} occurred attempting to start TaskManager.", e.Message);

                throw new Exception(message, e);
            }
        }

        protected override void OnStop()
        {

            try
            {
                ActivityLog.LogExecutiveActivity("CSExecutiveService", ActivityLog.LogEventId_CSEXECUTIVE_SHUTDOWN,
                                                 "OnStop", text1: "Stopping CSExecutiveService");

                TaskManager.TaskManager.UnregisterAllTasks();
            }
            catch (Exception e)
            {
                string message = string.Format("Exception: {0} occurred attempting to stop TaskManager (Unregistering Tasks).  Ignored.", e.Message);
                ActivityLog.LogException(null, "CS EXECUTIVE", ActivityLog.LogEventId_CSEXECUTIVE_EXCEPTION, 
                                         "CSExecutiveService", "OnStop", message, exception: e);

                // Ignore
            }
            finally
            {
                TaskManager.TaskManager.Stop();
            }

            ActivityLog.LogExecutiveActivity("CSExecutiveService", ActivityLog.LogEventId_CSEXECUTIVE_SHUTDOWN, 
                                             "OnStop", text1: "CSExecutiveService Stopped");
        }

        #endregion

        // ********************************************************************************
        #region Register Tasks Logic
        // ********************************************************************************

        private void RegisterTasks()
        {
            TaskManager.TaskManager.Register(new InitializeSystemTask(this));
        }

        #endregion

    }
}
