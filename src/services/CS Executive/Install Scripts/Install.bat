
SET ExecutableDir=%windir%\Microsoft.NET\Framework\v4.0.30319

SET ServiceName=CSExecutive
SET DisplayName=Code Signing Executive
SET Description=This service monitors the ESP system for new code signing requests and submits the request to the appropriate servers.
SET ServiceExe=..\CSExecutive.exe

"%ExecutableDir%\InstallUtil" /name=%ServiceName% /displayname="%DisplayName%" /description="%Description%" "%ServiceExe%"


