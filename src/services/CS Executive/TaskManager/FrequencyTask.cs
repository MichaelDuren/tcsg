using System;
using Util;

namespace CSExecutive.TaskManager
{
    /// <summary>
    /// Frequency Task - allows a task to be run every specified TimeSpan Frequency
    /// </summary>
    public abstract class FrequencyTask : Task
    {
        /// <summary>
        /// The time interval between each run of the task
        /// </summary>
        public TimeSpan Interval { get; internal set; }

        // ********************************************************************************
        #region Constructors
        // ********************************************************************************

        public FrequencyTask(string name) : base(name) { }

        public FrequencyTask(string name, TimeSpan interval)
            : base(name)
        {
            this.Interval = interval;
        }

        #endregion

        // ********************************************************************************
        #region Overrides of Core Methods
        // ********************************************************************************

        /// <summary>
        /// Validates that the task config is valid.  If not, it throws and exception.
        /// </summary>
        public override void Validate()
        {
            base.Validate();
            Guard.FailIfTrue(this.Interval <= TimeSpan.Zero, "Interval must be set for Task: " + Name);
        }

        public override bool IsFired()
        {
            DateTime nextRunDateTime = this.LastRunStarted + Interval;
            Time nextRunTime = nextRunDateTime.ToTime();

            // Check Start Time
            if (TaskManager.CurrentCycleStartTime > nextRunDateTime)
            {
                return true;
            }

            return false;
        }

        #endregion
    }
}
