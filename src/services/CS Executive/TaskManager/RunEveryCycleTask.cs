using System;
using Util;

namespace CSExecutive.TaskManager
{
    /// <summary>
    /// Task to allows a task to be run every time that the Task Manager cycles (eg. every minute)
    /// </summary>
    public abstract class RunEveryCycleTask : Task
    {
        // ********************************************************************************
        #region Constructors
        // ********************************************************************************

        public RunEveryCycleTask(string name) : base(name) { }

        #endregion

        // ********************************************************************************
        #region Overrides of Core Methods
        // ********************************************************************************

        /// <summary>
        /// Validates that the task config is valid.  If not, it throws and exception.
        /// </summary>
        public override void Validate()
        {
            base.Validate();
        }

        public override bool IsFired()
        {
            // Always return true - we want to run all the time!
            return true;
        }

        #endregion
    }
}
