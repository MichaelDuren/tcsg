using System;
using Util;

namespace CSExecutive.TaskManager
{
    /// <summary>
    /// Daily Task - allows a task to be run every day of the week (or specified days) at a specified time.
    /// </summary>
    public abstract class DailyTask : Task
    {
        public bool Sunday { get; set; }
        public bool Monday { get; set; }
        public bool Tuesday { get; set; }
        public bool Wednesday { get; set; }
        public bool Thursday { get; set; }
        public bool Friday { get; set; }
        public bool Saturday { get; set; }

        public Time StartTime { get; set; }

        // ********************************************************************************
        #region Constructors
        // ********************************************************************************

        public DailyTask() : base() { }

        public DailyTask(string name, bool everyDay)
            : base(name)
        {
            Sunday = true;
            Monday = true;
            Tuesday = true;
            Wednesday = true;
            Thursday = true;
            Friday = true;
            Saturday = true;
        }

        public DailyTask(string name, Time startTime, bool everyDay)
            : this(name, everyDay)
        {
            this.StartTime = StartTime;
        }

        #endregion

        // ********************************************************************************
        #region Overrides of Core Methods
        // ********************************************************************************

        /// <summary>
        /// Validates that the task config is valid.  If not, it throws and exception.
        /// </summary>
        public override void Validate()
        {
            base.Validate();

            if (!Sunday && !Monday && !Tuesday && !Wednesday && !Thursday && !Friday && !Saturday)
            {
                throw new ArgumentOutOfRangeException("Must specify at least one day of the week to run for Task: " + Name);
            }

            Guard.ValueNotNull(StartTime, "Must specify a StartTime for Task: " + Name);
        }

        public override bool IsFired()
        {
            if (!IsTodayAGoodDay())
            {
                return false;
            }

            // Check Start Time
            if (StartTime > TaskManager.LastCycleStartTime.ToTime() && StartTime < TaskManager.CurrentCycleStartTime.ToTime())
            {
                return true;
            }

            return false;
        }

        private bool IsTodayAGoodDay()
        {
            DayOfWeek dayOfWeek = DateTime.Now.DayOfWeek;

            switch (dayOfWeek)
            {
                case DayOfWeek.Monday: if (Monday) { return true; } break;
                case DayOfWeek.Tuesday: if (Tuesday) { return true; } break;
                case DayOfWeek.Wednesday: if (Wednesday) { return true; } break;
                case DayOfWeek.Thursday: if (Thursday) { return true; } break;
                case DayOfWeek.Friday: if (Friday) { return true; } break;
                case DayOfWeek.Saturday: if (Saturday) { return true; } break;
                case DayOfWeek.Sunday: if (Sunday) { return true; } break;

                default:
                    throw new Exception("Should never get here");
            }

            return false;
        }

        #endregion
    }
}
