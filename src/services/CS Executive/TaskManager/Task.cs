using System;
using CSG.AppServer.Managers;
using Util;

namespace CSExecutive.TaskManager
{
    /// <summary>
    /// The current status of a Task
    /// </summary>
    public enum TaskStatus
    {
        NotRunning, Running, Disabled
    }

    public abstract class Task
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //// ******************************************************************************
        //// Basic Properties
        //// ******************************************************************************

        /// <summary>
        /// Name of the Task (for human viewing)
        /// </summary>
        public string Name { get; internal set; }

        internal volatile TaskStatus _status;
        /// <summary>
        /// Current Status of the Task
        /// </summary>
        public TaskStatus Status
        {
            get { return _status; }
        }

        /// <summary>
        /// When did the task run last
        /// </summary>
        public DateTime LastRunStarted { get; internal set; }

        /// <summary>
        /// If true - then the standard status messages are logged to the activity log.
        /// For very frequently running tasks - this is better set to false
        /// </summary>
        public bool LogNormalStatusToActivityLog { get; set; }

        //// ******************************************************************************
        //// Constructors
        //// ******************************************************************************

        public Task()
        {
            this._status = TaskStatus.NotRunning;

            // Set LastRunStarted to some reasonable value
            this.LastRunStarted = DateTime.Now.AddSeconds(30);
        }

        public Task(string name)
            : this()
        {
            this.Name = name;
        }

        // ********************************************************************************
        #region Core Methods
        // ********************************************************************************

        /// <summary>
        /// Validates that the task config is valid.  If not, it throws and exception.
        /// </summary>
        public virtual void Validate()
        {
            Guard.ValueNotBlankOrNull(Name, "Name is required for Task: " + Name);
        }

        /// <summary>
        /// Wrapper method for Task Implementations of IsFired() - to provide the basic functionality, status, exception handling, etc.
        /// </summary>
        internal bool AttemptIsFired()
        {
            // Make sure that the task status isn't Running or Disabled
            if (this.Status == TaskStatus.Running || this.Status == TaskStatus.Disabled)
            {
                return false;
            }

            try
            {
                return IsFired();
            }
            catch (Exception e)
            {
                string message = string.Format("Unexpected {0} occurred running IsFired() for Task: {1}.  Message: {2}", e.GetType().ToString(), this.Name, e.Message);
                ActivityLog.LogException(null, "CS EXECUTIVE", ActivityLog.LogEventId_CSEXECUTIVE_EXCEPTION, 
                                         "Task", "AttemptIsFired", message, exception: e);

                return false;
                // DO NOT THROW!!!
            }
        }

        /// <summary>
        /// Wrapper method for Task Implementations of Run() - to provide the basic functionality, status, exception handling, etc.
        /// </summary>
        internal void AttemptRun()
        {
            LogMessage(string.Format("Started Running Task: {0}  LastRunStarted was: {1}", this.Name, this.LastRunStarted));

            // *******************************************************
            // Update the task - running
            // *******************************************************

            this._status = TaskStatus.Running;
            this.LastRunStarted = DateTime.Now;

            // *******************************************************
            // Run the task
            // *******************************************************

            try
            {
                Run();
            }
            catch (Exception e)
            {
                string message = string.Format("Unexpected {0} occurred running Task: {1}.  Message: {2}", e.GetType().ToString(), this.Name, e.Message);
                ActivityLog.LogException(null, "CS EXECUTIVE", ActivityLog.LogEventId_CSEXECUTIVE_EXCEPTION, 
                                         "Task", "AttemptRun", message, exception: e);

                // DO NOT THROW!!!

                this._status = TaskStatus.NotRunning;
                return;
            }

            // *******************************************************
            // Update the task - run complete
            // *******************************************************

            this._status = TaskStatus.NotRunning;

            TimeSpan elapsedTime = DateTime.Now - LastRunStarted;

            LogMessage(string.Format("Completed Running Task: {0}  LastRunStarted was: {1}", this.Name, this.LastRunStarted), elapsedMilliseconds: (int)elapsedTime.TotalMilliseconds);
        }

        private void LogMessage(string message, int? elapsedMilliseconds = null)
        {
            if (LogNormalStatusToActivityLog)
            {
//                ActivityLog.LogExecutiveActivity("Task", ActivityLog.LogEventId_LOG_ENTRY, 
//                                                 this.Name, text1: message, elapsedMilliseconds: elapsedMilliseconds);
            }
            else
            {
                log.Info(message);
            }
        }

        #endregion

        // ********************************************************************************
        #region Abstract Methods
        // ********************************************************************************

        /// <summary>
        /// Determines if the task should run now
        /// </summary>
        public abstract bool IsFired();

        /// <summary>
        /// This is the actual logic that the task runs - to do whatever work is needed by the task
        /// </summary>
        public abstract void Run();

        #endregion
    }
}
