using System;
using Util;

namespace CSExecutive.TaskManager
{
    /// <summary>
    /// Task that runs one time
    /// </summary>
    public abstract class RunOnceTask : Task
    {
        // ********************************************************************************
        #region Constructors
        // ********************************************************************************
        private bool ranOnce = false;

        public RunOnceTask(string name) : base(name) { }
        #endregion

        // ********************************************************************************
        #region Overrides of Core Methods
        // ********************************************************************************

        /// <summary>
        /// Validates that the task config is valid.  If not, it throws and exception.
        /// </summary>
        public override void Validate()
        {
            base.Validate();
        }

        public override bool IsFired()
        {
            if (ranOnce == false)
            {
                ranOnce = true;
                return true;
            }
            // we have already run, so return false from here on out
            return false;
        }

        #endregion
    }
}
