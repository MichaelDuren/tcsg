using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using CSG.AppServer.Managers;
using Util;

namespace CSExecutive.TaskManager
{
    public static class TaskManager
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static int TASK_MANAGER_RUN_FREQUENCY;

        // ********************************************************************************
        #region Properties
        // ********************************************************************************

        /// <summary>
        /// Shows the status of the TaskManager - true if it is started and processing Tasks
        /// </summary>
        public static bool Running { get; private set; }

        private static List<Task> registeredTasks = new List<Task>();

        public static DateTime LastCycleStartTime { get; private set; }
        public static DateTime CurrentCycleStartTime { get; private set; }

        #endregion

        // ********************************************************************************
        #region Constructors
        // ********************************************************************************

        static TaskManager()
        {
            int taskManagerRunFrequency = (int)SystemSettingsManager.Current.GetBySettingName(CSG.AppServer.Constants.SettingNames.TaskManagerRunFrequency).IntegerValue;
            if (taskManagerRunFrequency <= 0)
            {
                taskManagerRunFrequency = 60;
            }

            TASK_MANAGER_RUN_FREQUENCY = 1000 * taskManagerRunFrequency;
        }

        #endregion

        // ********************************************************************************
        #region Public facing Methods
        // ********************************************************************************

        /// <summary>
        /// Starts the TaskManager
        /// </summary>
        public static void Start()
        {

            LastCycleStartTime = DateTime.Now;
            CurrentCycleStartTime = LastCycleStartTime;

            Running = true;

            // Start Thread to manage the tasks (and makes it a background task)
            ThreadStart ts = new ThreadStart(ManageTasks);
            Thread t = new Thread(ts);
            t.IsBackground = true;
            t.Start();

            log.Info("Main WorkLoop thread started.");
        }

        /// <summary>
        /// Stops the Task manager (does NOT unregister tasks)
        /// </summary>
        public static void Stop()
        {
            Running = false;
        }

        /// <summary>
        /// Registers a task with the task manager
        /// </summary>
        public static void Register(Task task)
        {
            Guard.ArgumentNotNull(task, "task");

//            ActivityLog.LogExecutiveActivity("TaskManager", "Register", text1: string.Format("Registering Task: {0}", task.Name));

            task.Validate();

            lock (registeredTasks)
            {
                registeredTasks.Add(task);
            }
        }

        /// <summary>
        /// UnRegisters a task from the task manager
        /// </summary>
        public static void Unregister(Task task)
        {
            Guard.ArgumentNotNull(task, "task");

            ActivityLog.LogExecutiveActivity("TaskManager", ActivityLog.LogEventId_LOG_ENTRY, 
                                             "Unregister", text1: string.Format("Unregistering Task: {0}", task.Name));

            lock (registeredTasks)
            {
                registeredTasks.Remove(task);
            }
        }

        public static void UnregisterAllTasks()
        {
            lock (registeredTasks)
            {
                foreach (Task task in registeredTasks.ToList())
                {
                    Unregister(task);
                }
            }
        }

        #endregion

        // ********************************************************************************
        #region Core Methods
        // ********************************************************************************

        /// <summary>
        /// Checks to see if any Registered task needs to be started
        /// </summary>
        private static void ManageTasks()
        {
            List<Task> registeredTasks_Copy;

            Thread.Sleep(15000);
            // Endless loop - to continuously attempt to run the tasks (while the task manager is Running)
            while (Running)
            {
                // Track the Last and Current Cycle Start Times - for tasks to use for determining if they should run
                LastCycleStartTime = CurrentCycleStartTime; // set "Last" cycle start time to the value from the LAST time through the loop
                CurrentCycleStartTime = DateTime.Now;

                lock (registeredTasks)
                {
                    registeredTasks_Copy = registeredTasks.ToList();
                }

                foreach (Task task in registeredTasks_Copy)
                {
                    try
                    {
                        if (task.AttemptIsFired())
                        {
                            log.DebugFormat("Task fired: {0}", task.Name);

                            task.AttemptRun();
                        }
                    }
                    catch
                    {
                        // We shouldn't get any exceptions here - they should be handled and logged by the task.

                        // Ignore
                    }
                }

                // Wait a while - then check again
                Thread.Sleep(TASK_MANAGER_RUN_FREQUENCY);
            }
        }

        #endregion
    }
}
