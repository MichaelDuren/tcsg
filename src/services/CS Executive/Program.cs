using System.ServiceProcess;

namespace CSExecutive
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[] 
            { 
                new CSExecutiveService() 
            };
            ServiceBase.Run(ServicesToRun);
        }
    }
}
