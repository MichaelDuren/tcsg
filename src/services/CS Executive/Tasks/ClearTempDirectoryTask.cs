﻿using System;
using System.IO;
using CSG.AppServer;
using CSG.AppServer.Entities;
using CSG.AppServer.Managers;
using CSExecutive.TaskManager;

namespace CSExecutive.Tasks
{
    /// <summary>
    /// This task is responsible for clearing out any files copied to the temp directory as a result of a user downloading a file
    /// </summary>
    public class ClearTempDirectoryTask : RunEveryCycleTask
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public int MaxMinutesToKeepTempFiles { get; set; }

        public ClearTempDirectoryTask()
            : base("Clear Temp Directory Task")
        {
            try
            {
                SystemSetting setting = SystemSettingsManager.Current.GetBySettingName(CSG.AppServer.Constants.SettingNames.MaxMinutesToKeepTempFiles, true);
                MaxMinutesToKeepTempFiles = (int)setting.IntegerValue;
            }
            catch
            {
                //Defaulting to 15 minutes if setting not found
                MaxMinutesToKeepTempFiles = 15;
            }
        }

        public override void Run()
        {
            log.Debug("ClearTempDirectoryTask.Run()");

            try
            {
                ProcessDirectory(AppServerContext.TempStorageLocation);
            }
            catch
            {
                //Temporarily ignoring all errors
            }
        }

        /// <summary>
        /// This method will process all files found in the directory provided as well as any subdirectories found in the directory
        /// </summary>
        /// <param name="startingDirectory"></param>
        private void ProcessDirectory(string startingDirectory)
        {
            string[] files = Directory.GetFiles(startingDirectory);

            foreach (string file in files)
            {
                FileInfo fi = new FileInfo(file);

                if (fi.CreationTime.AddMinutes(15) < DateTime.Now)
                {
                    try
                    {
                        File.Delete(file);
                    }
                    catch (Exception e)
                    {
                        //If it fails to delete, just log it and move on to the next file
                        string message =  String.Format("A file could not be deleted from the temp location: {0}", file);
                        ActivityLog.LogException(null, "CS EXECUTIVE", ActivityLog.LogEventId_CSEXECUTIVE_EXCEPTION,
                                                 "ClearTempDirectoryTask", "ProcessDirectory", message, exception: e);
                    }
                }
            }

            string[] subDirectories = Directory.GetDirectories(startingDirectory);

            foreach (string subDirectory in subDirectories)
            {
                ProcessDirectory(subDirectory);
            }
        }
    }
}
