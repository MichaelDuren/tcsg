﻿using System;
using CSG.AppServer.Managers;
using CSG.AppServer.Security;
using CSExecutive.TaskManager;
using CSG.AppServer.Db;
using CSG.AppServer;
using System.IO;
using System.Threading;
using System.ServiceProcess;

namespace CSExecutive.Tasks
{
    public class InitializeSystemTask : RunOnceTask
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        CSExecutiveService thisService;

        public InitializeSystemTask(CSExecutiveService service)
            : base("Initialize System Task")
        {
            thisService = service;
        }

        public override void Run()
        {

            log.Info("InitializeSystemTask.Run()");

            // Verify the database connection before initializing
            try
            {
                ESPDbDomainService svc = new ESPDbDomainService();
            }
            catch (Exception ex)
            {
                ActivityLog.LogException(null, "CS EXECUTIVE", ActivityLog.LogEventId_DATABASE_EXCEPTION,
                                         "CSExecutiveService", "InitializeSystemTask", "Verifying database connectivity", exception: ex);
                // Log exception
                thisService.ExitCode = -1;
                thisService.Stop();
                throw;
            }

            try
            { 

                AppServerInitializer.Initialize();
            }
            catch (Exception ex)
            {
                ActivityLog.LogException(null, "CS EXECUTIVE", ActivityLog.LogEventId_DATABASE_EXCEPTION,
                                         "CSExecutiveService", "InitializeSystemTask", "Verifying database connectivity", exception: ex);
                // Log exception
                thisService.ExitCode = -1;
                thisService.Stop();
                throw;
            }

            // Verify presents of primary and secondary storage locations
            if (!Directory.Exists(AppServerContext.PrimaryStorageLocation))
            {
                var ex = new Exception("Primary Storage Location not found");
                ActivityLog.LogException(null, "CS EXECUTIVE", ActivityLog.LogEventId_STORAGE_LOCATION_ERROR,
                                         "CSExecutiveService", "InitializeSystemTask", "Verifying Primary Storage", exception: ex);

                thisService.ExitCode = -1;
                thisService.Stop();
                throw ex; 
            }

            // Verify presents of primary and secondary storage locations
            if (!Directory.Exists(AppServerContext.SecondaryStorageLocation))
            {
                var ex = new Exception("Secondary Storage Location not found");
                ActivityLog.LogException(null, "CS EXECUTIVE", ActivityLog.LogEventId_STORAGE_LOCATION_ERROR,
                                         "CSExecutiveService", "InitializeSystemTask", "Verifying Secondary Storage", exception: ex);

                thisService.ExitCode = -1;
                thisService.Stop();
                throw ex;
            }

            UserManagerBase userManager = UserManagerFactory.CreateUserManager();
            userManager.SyncAdminGroupsFromAppConfig();

            // register the remaining tasks
            TaskManager.TaskManager.Register(new SyncWithDirectoryTask());
            TaskManager.TaskManager.Register(new ProcessTemporalApprovals());
            TaskManager.TaskManager.Register(new ProcessSubmittedRequestsTask());
            TaskManager.TaskManager.Register(new ProcessApprovedRequestsTask());
            TaskManager.TaskManager.Register(new ClearTempDirectoryTask());
            TaskManager.TaskManager.Register(new CheckCertificatesTask());
            TaskManager.TaskManager.Register(new ClearStorageDirectoryTask());

            log.Info("AppServerInitializer.Initialize() Complete");

        }
    }
}
