﻿using CSG.AppServer.Managers;
using CSExecutive.TaskManager;

namespace CSExecutive.Tasks
{
    public class ProcessTemporalApprovals : RunEveryCycleTask
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ProcessTemporalApprovals()
            : base("Process Temporal Approvals Task") { }

        public override void Run()
        {
            log.Debug("ProcessTemporalApprovals.Run()");

            CodeSigningRequestManager manager = new CodeSigningRequestManager();
            manager.ProcessTemporalApprovals();
        }
    }
}
