﻿using System;
using System.IO;
using CSG.AppServer;
using CSG.AppServer.Entities;
using CSG.AppServer.Managers;
using CSExecutive.TaskManager;

namespace CSExecutive.Tasks
{
    /// <summary>
    /// This task is responsible for clearing out any files copied to the temp directory as a result of a user downloading a file
    /// </summary>
    public class ClearStorageDirectoryTask : FrequencyTask
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public int MaxStorageToKeepUnsignedFiles { get; set; }

        public ClearStorageDirectoryTask()
            : base("Clear Storage Directory Task")
        {
            try
            {
                MaxStorageToKeepUnsignedFiles = (int)SystemSettingsManager.Current.GetBySettingName(CSG.AppServer.Constants.SettingNames.Task_CheckCertificatesTask_Interval).IntegerValue;
                this.Interval = new TimeSpan(MaxStorageToKeepUnsignedFiles, 0, 0, 0);
            }
            catch
            {
                //Defaulting to 30 days if setting not found
                MaxStorageToKeepUnsignedFiles = 30;
                this.Interval = new TimeSpan(MaxStorageToKeepUnsignedFiles, 0, 0, 0);
            }
        }

        public override void Run()
        {
            log.Debug("ClearStorageDirectoryTask.Run()");

            try
            {
                MaxStorageToKeepUnsignedFiles = (int)SystemSettingsManager.Current.GetBySettingName(CSG.AppServer.Constants.SettingNames.Task_CheckCertificatesTask_Interval).IntegerValue;
                if (MaxStorageToKeepUnsignedFiles > 0)
                {
                    ProcessDirectory(AppServerContext.PrimaryStorageLocation);
                    ProcessDirectory(AppServerContext.SecondaryStorageLocation);
                }
            }
            catch
            {
                //Temporarily ignoring all errors
            }
        }

        /// <summary>
        /// This method will process all files found in the directory provided as well as any subdirectories found in the directory
        /// </summary>
        /// <param name="startingDirectory"></param>
        private void ProcessDirectory(string startingDirectory)
        {
            string[] files = Directory.GetFiles(startingDirectory);

            foreach (string file in files)
            {
                FileInfo fi = new FileInfo(file);

                if (file.Contains("Unsigned") &&
                    fi.CreationTime.AddDays(MaxStorageToKeepUnsignedFiles) < DateTime.Now)
                {
                    try
                    {
                        File.Delete(file);
                    }
                    catch (Exception e)
                    {
                        //If it fails to delete, just log it and move on to the next file
                        string message =  String.Format("A file could not be deleted from the storage location: {0}", file);
                        ActivityLog.LogException(null, "CS EXECUTIVE", ActivityLog.LogEventId_CSEXECUTIVE_EXCEPTION,
                                                 "ClearStorageDirectoryTask", "ProcessDirectory", message, exception: e);
                    }
                }
            }

            string[] subDirectories = Directory.GetDirectories(startingDirectory);

            foreach (string subDirectory in subDirectories)
            {
                ProcessDirectory(subDirectory);
            }
        }
    }
}
