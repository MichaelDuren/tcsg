﻿using System;
using CSG.AppServer.Managers;
using CSG.AppServer.Security;
using CSExecutive.TaskManager;
using CSG.AppServer.Db;

namespace CSExecutive.Tasks
{
    public class SyncWithDirectoryTask : FrequencyTask
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public SyncWithDirectoryTask()
            : base("Sync with Directory Task")
        {
            int intervalMinutes = (int)SystemSettingsManager.Current.GetBySettingName(CSG.AppServer.Constants.SettingNames.Task_SyncWithActiveDirectory_Interval).IntegerValue;
            this.Interval = new TimeSpan(0, intervalMinutes, 0);
            log.Info("SNYC Interval: " + intervalMinutes);
        }

        public override void Run()
        {
            log.Debug("SyncWithDirectoryTask.Run()");

            using (UserManagerBase userManager = UserManagerFactory.CreateUserManager())
            {
                userManager.SyncAllWithActiveDirectory();

                ActivityLog.LogExecutiveActivity("SyncWithDirectoryTask", ActivityLog.LogEventId_SYNC_DIRECTORY);
            }
        }
    }
}
