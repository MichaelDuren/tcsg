﻿using CSG.AppServer.Managers;
using CSExecutive.TaskManager;

namespace CSExecutive.Tasks
{
    public class ProcessApprovedRequestsTask : RunEveryCycleTask
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ProcessApprovedRequestsTask()
            : base("Process Approved Requests Task") { }

        public override void Run()
        {
            log.Debug("ProcessApprovedRequestsTask.Run()");

            CodeSigningRequestManager manager = new CodeSigningRequestManager();
            manager.ProcessApprovedRequests();
        }
    }
}
