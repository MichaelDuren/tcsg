﻿using System;
using CSG.AppServer.Managers;
using CSExecutive.TaskManager;

namespace CSExecutive.Tasks
{
    public class CheckCertificatesTask : FrequencyTask
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public CheckCertificatesTask()
            : base("Process Submitted Out Requests Task")
        {
            int intervalDays = (int)SystemSettingsManager.Current.GetBySettingName(CSG.AppServer.Constants.SettingNames.Task_CheckCertificatesTask_Interval).IntegerValue;
            this.Interval = new TimeSpan(intervalDays, 0, 0, 0);
        }

        public override void Run()
        {
            log.Debug("CheckCertificatesTask.Run()");

            CertificateManager manager = new CertificateManager();
            manager.CheckCertificates(this.Interval);
        }
    }
}
