﻿using System;
using CSG.AppServer.Managers;
using CSExecutive.TaskManager;

namespace CSExecutive.Tasks
{
    public class ProcessSubmittedRequestsTask : FrequencyTask
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public ProcessSubmittedRequestsTask()
            : base("Process Submitted Out Requests Task")
        {
            int intervalMinutes = (int)SystemSettingsManager.Current.GetBySettingName(CSG.AppServer.Constants.SettingNames.Task_ProcessSubmittedRequestsTask_Interval).IntegerValue;
            this.Interval = new TimeSpan(0, intervalMinutes, 0);
        }

        public override void Run()
        {
            log.Debug("ProcessSubmittedRequestsTask.Run()");

            CodeSigningRequestManager manager = new CodeSigningRequestManager();
            manager.ProcessSubmittedRequests();
        }
    }
}
