﻿using System;
using System.Web.Http;
using System.Linq;
using Microsoft.AspNet.Identity;
using ESP.AppServer.APITypes;
using ESP.AppServer.Security;
using ESP.AppServer.Managers;
using System.Collections.Specialized;
using System.Web;
using ESP.AppServer.Entities;
using System.Net.Http;
using System.IO;
using ESP.AppServer;
using System.Threading.Tasks;
using System.Net;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using ESP.AppServer.Exceptions;

namespace ESP.espDirect.Controllers
{

    [RoutePrefix("api/espDirect")]
    public class espDirectController : ApiController
    {
        private const string qsSUBJECT = "SUBJECT";
        private const string qsRequestId = "requestId";

        // ******************************************************************************
        #region Properties / Attributes
        // ******************************************************************************
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        #endregion

        public espDirectController()
        {
        }


        // ******************************************************************************
        #region API Methods
        // ******************************************************************************
        [Route("espListSigningTypes")]
        [HttpGet]
        public IHttpActionResult espListSigningTypes()
        {
            var serializer = new DataContractJsonSerializer(typeof(APIListSigningTypes));
            Gatekeeper gatekeeper;
            // we need to access the query string, use name value pairs here
            NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);

            try
            {
                // Gatekeeper handles enumeration of the user's identity
                gatekeeper = new Gatekeeper(nvc["SUBJECT"]);
                var userProfile = gatekeeper.GetCurrentUserProfile();

                ActivityLog.Log(userProfile.DomainAndLoginId.ToString(), userProfile.DomainAndLoginId.LoginId, "espDirectAPI", "espListSigningTypes");
                gatekeeper.LogUserActivity("User called espListSigningTypes");

            }
            catch (Exception ex)
            {
                log.Error("User does not have access to this resource");
                return Unauthorized();
            }

            try
            {
                APIManager apiMgr = new APIManager(gatekeeper);
                return Ok(apiMgr.espListSigningTypes());
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                throw ex;
            }

        }

        [Route("espCodesignRequestCreate")]
        [HttpPost]
        public IHttpActionResult espCodesignRequestCreate([FromBody]espCodesignRequestCreateParams requestParams)
        {
            // we need to access the query string, use name value pairs here
            NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);
            Gatekeeper gatekeeper;

            try
            {
                // Gatekeeper handles enumeration of the user's identity
                gatekeeper = new Gatekeeper(nvc["SUBJECT"]);
                var userProfile = gatekeeper.GetCurrentUserProfile();

                // Use gatekeeper to validate access to this type
                gatekeeper.ValidateUserAuthorizedForType(requestParams.signingTypeId);

                ActivityLog.Log(userProfile.DomainAndLoginId.ToString(), userProfile.DomainAndLoginId.LoginId, "espDirectAPI", "espCodesignRequestCreate");
                gatekeeper.LogUserActivity("User called espCodesignRequestCreate for signing type: " + requestParams.signingTypeId);

                APIManager apiMgr = new APIManager(gatekeeper);

                return Ok(apiMgr.espCodesignRequestCreate(requestParams));
            }
            catch (LoginException lex)
            {
                log.Error("User is not logged in or has not been authenticated. " + lex.Message);
                return Unauthorized();
            }
            catch (UserRightsException urex)
            {
                log.Error("User does not have access to this resource. " + urex.Message);
                return Unauthorized();
            }
            catch (Exception ex)
            {
                log.Error("User does not have access to this resource");
                return null;
            }
        }

        [HostAuthentication(DefaultAuthenticationTypes.ExternalBearer)]
        [Route("espCodesignRequestStatus")]
        [HttpGet]
        public IHttpActionResult espCodesignRequestStatus()
        {
            // we need to access the query string, use name value pairs here
            NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);
            try
            {
                int requestId = Convert.ToInt32(nvc["requestId"]);

                // Gatekeeper handles enumeration of the user's identity
                Gatekeeper gatekeeper = new Gatekeeper(nvc["SUBJECT"]);

                gatekeeper.ValidateUserViewRequest(requestId);

                var userProfile = gatekeeper.GetCurrentUserProfile();
                ActivityLog.Log(userProfile.DomainAndLoginId.ToString(), userProfile.DomainAndLoginId.LoginId, "espDirectAPI", "espCodesignRequestStatus");
                gatekeeper.LogUserActivity("User called espCodesignRequestStatus for request: " + requestId);

                APIManager apiMgr = new APIManager(gatekeeper);
                return Ok(apiMgr.espCodesignRequestStatus(requestId));
            }
            catch (LoginException lex)
            {
                log.Error("User is not logged in or has not been authenticated. " + lex.Message);
                return Unauthorized();
            }
            catch (UserRightsException urex)
            {
                log.Error("User does not have access to this resource. " + urex.Message);
                return Unauthorized();
            }
            catch (Exception ex)
            {
                log.Error("User does not have access to this resource");
                return null;
            }
        }

        [Route("espCodesignRequestSubmitFile")]
        [HttpPost]
        public async Task<HttpResponseMessage> espCodesignRequestSubmitFile()
        {
            APIManager apiMgr=null;
            Gatekeeper gatekeeper;

            // we need to access the query string, use name value pairs here
            NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);

            int requestId = Convert.ToInt32(nvc["requestId"]);

            try
            {
                // Gatekeeper handles enumeration of the user's identity
                gatekeeper = new Gatekeeper(nvc["SUBJECT"]);
                var userProfile = gatekeeper.GetCurrentUserProfile();

                gatekeeper.ValidateUserSubmittedRequest(requestId);

                ActivityLog.Log(userProfile.DomainAndLoginId.ToString(), userProfile.DomainAndLoginId.LoginId, "espDirectAPI", "espCodesignRequestSubmitFile");
                gatekeeper.LogUserActivity("User called espCodesignRequestSubmitFile for request: " + requestId);


            }
            catch (LoginException lex)
            {
                log.Error("User is not logged in or has not been authenticated. " + lex.Message);
                return Request.CreateResponse(HttpStatusCode.Unauthorized);
            }
            catch (UserRightsException urex)
            {
                log.Error("User does not have access to this resource. " + urex.Message);
                return Request.CreateResponse(HttpStatusCode.Unauthorized);
            }
            catch (Exception ex)
            {
                log.Error("Error occured: " + ex.Message);
                return null;
            }

            try
            {
                apiMgr = new APIManager(gatekeeper);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
                throw ex;
            }

            // Verify that this is a legitimate Id before accepting the file, will throw an exception if invalid
            apiMgr.espValidateRequestId(requestId);


            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new UnsupportedMediaTypeException("This API call requires a mime multipart message", null);
            }

            string tempPath = AppServerContext.LocalUploadFolder;// Path.Combine(AppServerContext.PrimaryStorageLocation, "temp");
            if (!Directory.Exists(tempPath))
            {
                Directory.CreateDirectory(tempPath);
            }

            // generate a temporary filename until for the upload, this will be renamed in the API manager based on the 
            var uniqueFileName = string.Format(@"{0}.txt", Guid.NewGuid());

            var streamProvider = new MultipartFormDataStreamProvider(tempPath);
            await Request.Content.ReadAsMultipartAsync(streamProvider);
            foreach (MultipartFileData fileData in streamProvider.FileData)
            {
                if (string.IsNullOrEmpty(fileData.Headers.ContentDisposition.FileName))
                {
                    return Request.CreateResponse(HttpStatusCode.NotAcceptable, "This request is not properly formatted");
                }

                string fileName = fileData.Headers.ContentDisposition.FileName;
                if (fileName.StartsWith("\"") && fileName.EndsWith("\""))
                {
                    fileName = fileName.Trim('"');
                }
                if (fileName.Contains(@"/") || fileName.Contains(@"\"))
                {
                    fileName = Path.GetFileName(fileName);
                }
                File.Move(fileData.LocalFileName, Path.Combine(tempPath, uniqueFileName));

                // process the request with the api manager
                apiMgr.espCodesignRequestSubmitFile(requestId, Path.Combine(tempPath, uniqueFileName), fileName);
            }
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [Route("espCodesignRequestDownloadFile")]
        [HttpGet]
        public HttpResponseMessage espCodesignRequestDownloadFile()
        {
            APIManager apiMgr = null;
            string file;

            // we need to access the query string, use name value pairs here
            NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);

            int requestId = Convert.ToInt32(nvc["requestId"]);

            // Gatekeeper handles enumeration of the user's identity
            Gatekeeper gatekeeper = new Gatekeeper(nvc["SUBJECT"]);
            var userProfile = gatekeeper.GetCurrentUserProfile();

            ActivityLog.Log(userProfile.DomainAndLoginId.ToString(), userProfile.DomainAndLoginId.LoginId, "espDirectAPI", "espCodesignRequestDownloadFile");
            gatekeeper.LogUserActivity("User called espCodesignRequestDownloadFile for request: " + requestId);

            apiMgr = new APIManager(gatekeeper);

            // Verify that this is a legitimate Id before accepting the file, will throw an exception if invalid
            file = apiMgr.espCodesignRequestDownloadFile(requestId);

            HttpResponseMessage result = new HttpResponseMessage(HttpStatusCode.OK);
            var stream = new FileStream(file, FileMode.Open);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentDisposition.FileName = Path.GetFileName(file);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
            result.Content.Headers.ContentLength = stream.Length;
            return result;
        }

        [Route("espSignHash")]
        [HttpPost]
        public APISignHashResult espSignHash([FromBody]espSignHashRequest request)
        {
            // we need to access the query string, use name value pairs here
            NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);

            // Gatekeeper handles enumeration of the user's identity
            Gatekeeper gatekeeper = new Gatekeeper(nvc["SUBJECT"]);

            var userProfile = gatekeeper.GetCurrentUserProfile();

            ActivityLog.Log(userProfile.DomainAndLoginId.ToString(), userProfile.DomainAndLoginId.LoginId, "espDirectAPI", "espSignHash");
            gatekeeper.LogUserActivity("User called espSignHash");

            try
            {
                APIManager apiMgr = new APIManager(gatekeeper);
                return apiMgr.espSignHash(request);
            }
            catch (Exception ex)
            {
                log.Error(ex.Message);
            }
            return null;
        }

        #endregion

    }
}
