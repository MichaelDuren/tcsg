﻿//
//
//  WARNING: This is generated code.  Do NOT modify this file.  Instead, create another partial class and modify that file instead.
//
//
using System.Data.Entity;
using CSG.AppServer.Entities;

namespace CSG.AppServer.Db
{
    public partial class ESPDbContext : DbContext
    {
        // DbSets
        internal DbSet<ActivityLogEntry> ActivityLogEntries { get; set; }
        internal DbSet<SystemSetting> SystemSettings { get; set; }
        internal DbSet<UserProfileCodeSigningType> UserProfileCodeSigningTypes { get; set; }
        internal DbSet<UserProfileRole> UserProfileRoles { get; set; }
        internal DbSet<UserProfileGroup> UserProfileGroups { get; set; }
        internal DbSet<CodeSigningTypeGroup> CodeSigningTypeGroups { get; set; }
        internal DbSet<CodeSigningTemporalApproval> CodeSigningTemporalApprovals { get; set; }
        internal DbSet<UserProfile> UserProfiles { get; set; }
        internal DbSet<CodeSigningRequest> CodeSigningRequests { get; set; }
        internal DbSet<CodeSigningTool> CodeSigningTools { get; set; }
        internal DbSet<Certificate> Certificates { get; set; }
        internal DbSet<Role> Roles { get; set; }
        internal DbSet<CodeSigningType> CodeSigningTypes { get; set; }
        internal DbSet<Group> Groups { get; set; }
    }
}
