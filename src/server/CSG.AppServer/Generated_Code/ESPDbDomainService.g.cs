﻿//
//
//  WARNING: This is generated code.  Do NOT modify this file.  Instead, create another partial class and modify that file instead.
//
//
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel.DomainServices.EntityFramework;
using CSG.AppServer.Entities;
using CSG.AppServer.Managers;

namespace CSG.AppServer.Db
{
    public partial class ESPDbDomainService : DbDomainService<ESPDbContext>
    {
        // *************************************************************************
        #region ActivityLogEntries Methods
        // *************************************************************************

        public virtual IQueryable<ActivityLogEntry> GetActivityLogEntries(string includes = null)
        {
            log.DebugFormat("GetActivityLogEntries called with Includes: {0}", includes);
            return this.DbContext.Set<ActivityLogEntry>(includes);
        }

        public virtual ActivityLogEntry GetActivityLogEntryById(int id, string includes = null)
        {
            log.DebugFormat("GetActivityLogEntryById called for ActivityLogEntry.Id: {0} with Includes: {1}", id, includes);
            var entity = this.DbContext.Set<ActivityLogEntry>(includes).Where(e => e.Id == id).FirstOrDefault();
            return entity;
        }

        public virtual void InsertActivityLogEntry(ActivityLogEntry entity)
        {
            log.DebugFormat("InsertActivityLogEntry called");
            UpdateEntityStats(entity, CudOperation.Insert);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Insert);
            }
            this.DbContext.Insert(entity);
        }

        public virtual void UpdateActivityLogEntry(ActivityLogEntry entity)
        {
            log.DebugFormat("UpdateActivityLogEntry called for ActivityLogEntry.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Update);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Update);
            }
            this.DbContext.Update(entity);
        }

        public virtual void DeleteActivityLogEntry(ActivityLogEntry entity)
        {
            log.DebugFormat("DeleteActivityLogEntry called for ActivityLogEntry.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Delete);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Delete);
            }
            this.DbContext.Delete(entity);
        }

        #endregion

        // *************************************************************************
        #region SystemSettings Methods
        // *************************************************************************

        public virtual IQueryable<SystemSetting> GetSystemSettings(string includes = null)
        {
            log.DebugFormat("GetSystemSettings called with Includes: {0}", includes);
            return this.DbContext.Set<SystemSetting>(includes);
        }

        public virtual SystemSetting GetSystemSettingById(int id, string includes = null)
        {
            log.DebugFormat("GetSystemSettingById called for SystemSetting.Id: {0} with Includes: {1}", id, includes);
            var entity = this.DbContext.Set<SystemSetting>(includes).Where(e => e.Id == id).FirstOrDefault();
            return entity;
        }

        public virtual void InsertSystemSetting(SystemSetting entity)
        {
            log.DebugFormat("InsertSystemSetting called");
            UpdateEntityStats(entity, CudOperation.Insert);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Insert);
            }
            this.DbContext.Insert(entity);
        }

        public virtual void UpdateSystemSetting(SystemSetting entity)
        {
            log.DebugFormat("UpdateSystemSetting called for SystemSetting.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Update);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Update);
            }
            this.DbContext.Update(entity);
        }

        public virtual void DeleteSystemSetting(SystemSetting entity)
        {
            log.DebugFormat("DeleteSystemSetting called for SystemSetting.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Delete);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Delete);
            }
            this.DbContext.Delete(entity);
        }

        #endregion

        // *************************************************************************
        #region UserProfileCodeSigningTypes Methods
        // *************************************************************************

        public virtual IQueryable<UserProfileCodeSigningType> GetUserProfileCodeSigningTypes(string includes = null)
        {
            log.DebugFormat("GetUserProfileCodeSigningTypes called with Includes: {0}", includes);
            return this.DbContext.Set<UserProfileCodeSigningType>(includes);
        }

        public virtual UserProfileCodeSigningType GetUserProfileCodeSigningTypeById(int id, string includes = null)
        {
            log.DebugFormat("GetUserProfileCodeSigningTypeById called for UserProfileCodeSigningType.Id: {0} with Includes: {1}", id, includes);
            var entity = this.DbContext.Set<UserProfileCodeSigningType>(includes).Where(e => e.Id == id).FirstOrDefault();
            return entity;
        }

        public virtual void InsertUserProfileCodeSigningType(UserProfileCodeSigningType entity)
        {
            log.DebugFormat("InsertUserProfileCodeSigningType called");
            UpdateEntityStats(entity, CudOperation.Insert);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Insert);
            }
            this.DbContext.Insert(entity);
        }

        public virtual void UpdateUserProfileCodeSigningType(UserProfileCodeSigningType entity)
        {
            log.DebugFormat("UpdateUserProfileCodeSigningType called for UserProfileCodeSigningType.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Update);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Update);
            }
            this.DbContext.Update(entity);
        }

        public virtual void DeleteUserProfileCodeSigningType(UserProfileCodeSigningType entity)
        {
            log.DebugFormat("DeleteUserProfileCodeSigningType called for UserProfileCodeSigningType.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Delete);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Delete);
            }
            this.DbContext.Delete(entity);
        }

        #endregion

        // *************************************************************************
        #region UserProfileRoles Methods
        // *************************************************************************

        public virtual IQueryable<UserProfileRole> GetUserProfileRoles(string includes = null)
        {
            log.DebugFormat("GetUserProfileRoles called with Includes: {0}", includes);
            return this.DbContext.Set<UserProfileRole>(includes);
        }

        public virtual UserProfileRole GetUserProfileRoleById(int id, string includes = null)
        {
            log.DebugFormat("GetUserProfileRoleById called for UserProfileRole.Id: {0} with Includes: {1}", id, includes);
            var entity = this.DbContext.Set<UserProfileRole>(includes).Where(e => e.Id == id).FirstOrDefault();
            return entity;
        }

        public virtual void InsertUserProfileRole(UserProfileRole entity)
        {
            log.DebugFormat("InsertUserProfileRole called");
            UpdateEntityStats(entity, CudOperation.Insert);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Insert);
            }
            this.DbContext.Insert(entity);
        }

        public virtual void UpdateUserProfileRole(UserProfileRole entity)
        {
            log.DebugFormat("UpdateUserProfileRole called for UserProfileRole.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Update);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Update);
            }
            this.DbContext.Update(entity);
        }

        public virtual void DeleteUserProfileRole(UserProfileRole entity)
        {
            log.DebugFormat("DeleteUserProfileRole called for UserProfileRole.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Delete);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Delete);
            }
            this.DbContext.Delete(entity);
        }

        #endregion

        // *************************************************************************
        #region UserProfileGroups Methods
        // *************************************************************************

        public virtual IQueryable<UserProfileGroup> GetUserProfileGroups(string includes = null)
        {
            log.DebugFormat("GetUserProfileGroups called with Includes: {0}", includes);
            return this.DbContext.Set<UserProfileGroup>(includes);
        }

        public virtual UserProfileGroup GetUserProfileGroupById(int id, string includes = null)
        {
            log.DebugFormat("GetUserProfileGroupById called for UserProfileGroup.Id: {0} with Includes: {1}", id, includes);
            var entity = this.DbContext.Set<UserProfileGroup>(includes).Where(e => e.Id == id).FirstOrDefault();
            return entity;
        }

        public virtual void InsertUserProfileGroup(UserProfileGroup entity)
        {
            log.DebugFormat("InsertUserProfileGroup called");
            UpdateEntityStats(entity, CudOperation.Insert);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Insert);
            }
            this.DbContext.Insert(entity);
        }

        public virtual void UpdateUserProfileGroup(UserProfileGroup entity)
        {
            log.DebugFormat("UpdateUserProfileGroup called for UserProfileGroup.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Update);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Update);
            }
            this.DbContext.Update(entity);
        }

        public virtual void DeleteUserProfileGroup(UserProfileGroup entity)
        {
            log.DebugFormat("DeleteUserProfileGroup called for UserProfileGroup.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Delete);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Delete);
            }
            this.DbContext.Delete(entity);
        }

        #endregion

        // *************************************************************************
        #region CodeSigningTypeGroups Methods
        // *************************************************************************

        public virtual IQueryable<CodeSigningTypeGroup> GetCodeSigningTypeGroups(string includes = null)
        {
            log.DebugFormat("GetCodeSigningTypeGroups called with Includes: {0}", includes);
            return this.DbContext.Set<CodeSigningTypeGroup>(includes);
        }

        public virtual CodeSigningTypeGroup GetCodeSigningTypeGroupById(int id, string includes = null)
        {
            log.DebugFormat("GetCodeSigningTypeGroupById called for CodeSigningTypeGroup.Id: {0} with Includes: {1}", id, includes);
            var entity = this.DbContext.Set<CodeSigningTypeGroup>(includes).Where(e => e.Id == id).FirstOrDefault();
            return entity;
        }

        public virtual void InsertCodeSigningTypeGroup(CodeSigningTypeGroup entity)
        {
            log.DebugFormat("InsertCodeSigningTypeGroup called");
            UpdateEntityStats(entity, CudOperation.Insert);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Insert);
            }
            this.DbContext.Insert(entity);
        }

        public virtual void UpdateCodeSigningTypeGroup(CodeSigningTypeGroup entity)
        {
            log.DebugFormat("UpdateCodeSigningTypeGroup called for CodeSigningTypeGroup.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Update);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Update);
            }
            this.DbContext.Update(entity);
        }

        public virtual void DeleteCodeSigningTypeGroup(CodeSigningTypeGroup entity)
        {
            log.DebugFormat("DeleteCodeSigningTypeGroup called for CodeSigningTypeGroup.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Delete);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Delete);
            }
            this.DbContext.Delete(entity);
        }

        #endregion

        // *************************************************************************
        #region CodeSigningTemporalApprovals Methods
        // *************************************************************************

        public virtual IQueryable<CodeSigningTemporalApproval> GetCodeSigningTemporalApprovals(string includes = null)
        {
            log.DebugFormat("GetCodeSigningTemporalApprovals called with Includes: {0}", includes);
            return this.DbContext.Set<CodeSigningTemporalApproval>(includes);
        }

        public virtual CodeSigningTemporalApproval GetCodeSigningTemporalApprovalById(int id, string includes = null)
        {
            log.DebugFormat("GetCodeSigningTemporalApprovalById called for CodeSigningTemporalApproval.Id: {0} with Includes: {1}", id, includes);
            var entity = this.DbContext.Set<CodeSigningTemporalApproval>(includes).Where(e => e.Id == id).FirstOrDefault();
            return entity;
        }

        public virtual void InsertCodeSigningTemporalApproval(CodeSigningTemporalApproval entity)
        {
            log.DebugFormat("InsertCodeSigningTemporalApproval called");
            UpdateEntityStats(entity, CudOperation.Insert);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Insert);
            }
            this.DbContext.Insert(entity);
        }

        public virtual void UpdateCodeSigningTemporalApproval(CodeSigningTemporalApproval entity)
        {
            log.DebugFormat("UpdateCodeSigningTemporalApproval called for CodeSigningTemporalApproval.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Update);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Update);
            }
            this.DbContext.Update(entity);
        }

        public virtual void DeleteCodeSigningTemporalApproval(CodeSigningTemporalApproval entity)
        {
            log.DebugFormat("DeleteCodeSigningTemporalApproval called for CodeSigningTemporalApproval.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Delete);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Delete);
            }
            this.DbContext.Delete(entity);
        }

        #endregion

        // *************************************************************************
        #region UserProfiles Methods
        // *************************************************************************

        public virtual IQueryable<UserProfile> GetUserProfiles(string includes = null)
        {
            log.DebugFormat("GetUserProfiles called with Includes: {0}", includes);
            return this.DbContext.Set<UserProfile>(includes);
        }

        public virtual UserProfile GetUserProfileById(int id, string includes = null)
        {
            log.DebugFormat("GetUserProfileById called for UserProfile.Id: {0} with Includes: {1}", id, includes);
            var entity = this.DbContext.Set<UserProfile>(includes).Where(e => e.Id == id).FirstOrDefault();
            return entity;
        }

        public virtual void InsertUserProfile(UserProfile entity)
        {
            log.DebugFormat("InsertUserProfile called");
            UpdateEntityStats(entity, CudOperation.Insert);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Insert);
            }
            this.DbContext.Insert(entity);
        }

        public virtual void UpdateUserProfile(UserProfile entity)
        {
            log.DebugFormat("UpdateUserProfile called for UserProfile.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Update);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Update);
            }
            this.DbContext.Update(entity);
        }

        public virtual void DeleteUserProfile(UserProfile entity)
        {
            log.DebugFormat("DeleteUserProfile called for UserProfile.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Delete);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Delete);
            }
            this.DbContext.Delete(entity);
        }

        #endregion

        // *************************************************************************
        #region CodeSigningRequests Methods
        // *************************************************************************

        public virtual IQueryable<CodeSigningRequest> GetCodeSigningRequests(string includes = null)
        {
            log.DebugFormat("GetCodeSigningRequests called with Includes: {0}", includes);
            return this.DbContext.Set<CodeSigningRequest>(includes);
        }

        public virtual CodeSigningRequest GetCodeSigningRequestById(int id, string includes = null)
        {
            log.DebugFormat("GetCodeSigningRequestById called for CodeSigningRequest.Id: {0} with Includes: {1}", id, includes);
            var entity = this.DbContext.Set<CodeSigningRequest>(includes).Where(e => e.Id == id).FirstOrDefault();
            return entity;
        }

        public virtual void InsertCodeSigningRequest(CodeSigningRequest entity)
        {
            log.DebugFormat("InsertCodeSigningRequest called");
            UpdateEntityStats(entity, CudOperation.Insert);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Insert);
            }
            this.DbContext.Insert(entity);
        }

        public virtual void UpdateCodeSigningRequest(CodeSigningRequest entity)
        {
            log.DebugFormat("UpdateCodeSigningRequest called for CodeSigningRequest.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Update);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Update);
            }
            this.DbContext.Update(entity);
        }

        public virtual void DeleteCodeSigningRequest(CodeSigningRequest entity)
        {
            log.DebugFormat("DeleteCodeSigningRequest called for CodeSigningRequest.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Delete);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Delete);
            }
            this.DbContext.Delete(entity);
        }

        #endregion

        // *************************************************************************
        #region CodeSigningTools Methods
        // *************************************************************************

        public virtual IQueryable<CodeSigningTool> GetCodeSigningTools(string includes = null)
        {
            log.DebugFormat("GetCodeSigningTools called with Includes: {0}", includes);
            return this.DbContext.Set<CodeSigningTool>(includes);
        }

        public virtual IEnumerable<CodeSigningTool> GetAllCodeSigningToolsFromCache (string includes = null)
        {
            log.DebugFormat("GetAllCodeSigningToolsFromCache called with Includes: {0}", includes);
            return GetAllFromCache<CodeSigningTool>(includes);
        }

        public virtual void InvalidateCodeSigningToolsCache ()
        {
            log.DebugFormat("InvalidateCodeSigningToolsCache called");
            InvalidateCache<CodeSigningTool>();
        }

        public virtual CodeSigningTool GetCodeSigningToolById(int id, string includes = null)
        {
            log.DebugFormat("GetCodeSigningToolById called for CodeSigningTool.Id: {0} with Includes: {1}", id, includes);
            var entity = this.DbContext.Set<CodeSigningTool>(includes).Where(e => e.Id == id).FirstOrDefault();
            return entity;
        }

        public virtual void InsertCodeSigningTool(CodeSigningTool entity)
        {
            log.DebugFormat("InsertCodeSigningTool called");
            UpdateEntityStats(entity, CudOperation.Insert);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Insert);
            }
            this.DbContext.Insert(entity);
        }

        public virtual void UpdateCodeSigningTool(CodeSigningTool entity)
        {
            log.DebugFormat("UpdateCodeSigningTool called for CodeSigningTool.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Update);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Update);
            }
            this.DbContext.Update(entity);
        }

        public virtual void DeleteCodeSigningTool(CodeSigningTool entity)
        {
            log.DebugFormat("DeleteCodeSigningTool called for CodeSigningTool.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Delete);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Delete);
            }
            this.DbContext.Delete(entity);
        }

        #endregion

        // *************************************************************************
        #region Certificates Methods
        // *************************************************************************

        public virtual IQueryable<Certificate> GetCertificates(string includes = null)
        {
            log.DebugFormat("GetCertificates called with Includes: {0}", includes);
            return this.DbContext.Set<Certificate>(includes);
        }

        public virtual IEnumerable<Certificate> GetAllCertificatesFromCache (string includes = null)
        {
            log.DebugFormat("GetAllCertificatesFromCache called with Includes: {0}", includes);
            return GetAllFromCache<Certificate>(includes);
        }

        public virtual void InvalidateCertificatesCache ()
        {
            log.DebugFormat("InvalidateCertificatesCache called");
            InvalidateCache<Certificate>();
        }

        public virtual Certificate GetCertificateById(int id, string includes = null)
        {
            log.DebugFormat("GetCertificateById called for Certificate.Id: {0} with Includes: {1}", id, includes);
            var entity = this.DbContext.Set<Certificate>(includes).Where(e => e.Id == id).FirstOrDefault();
            return entity;
        }

        public virtual void InsertCertificate(Certificate entity)
        {
            log.DebugFormat("InsertCertificate called");
            UpdateEntityStats(entity, CudOperation.Insert);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Insert);
            }
            this.DbContext.Insert(entity);
        }

        public virtual void UpdateCertificate(Certificate entity)
        {
            log.DebugFormat("UpdateCertificate called for Certificate.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Update);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Update);
            }
            this.DbContext.Update(entity);
        }

        public virtual void DeleteCertificate(Certificate entity)
        {
            log.DebugFormat("DeleteCertificate called for Certificate.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Delete);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Delete);
            }
            this.DbContext.Delete(entity);
        }

        #endregion

        // *************************************************************************
        #region Roles Methods
        // *************************************************************************

        public virtual IQueryable<Role> GetRoles(string includes = null)
        {
            log.DebugFormat("GetRoles called with Includes: {0}", includes);
            return this.DbContext.Set<Role>(includes);
        }

        public virtual IEnumerable<Role> GetAllRolesFromCache (string includes = null)
        {
            log.DebugFormat("GetAllRolesFromCache called with Includes: {0}", includes);
            return GetAllFromCache<Role>(includes);
        }

        public virtual void InvalidateRolesCache ()
        {
            log.DebugFormat("InvalidateRolesCache called");
            InvalidateCache<Role>();
        }

        public virtual Role GetRoleById(int id, string includes = null)
        {
            log.DebugFormat("GetRoleById called for Role.Id: {0} with Includes: {1}", id, includes);
            var entity = this.DbContext.Set<Role>(includes).Where(e => e.Id == id).FirstOrDefault();
            return entity;
        }

        public virtual void InsertRole(Role entity)
        {
            log.DebugFormat("InsertRole called");
            UpdateEntityStats(entity, CudOperation.Insert);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Insert);
            }
            this.DbContext.Insert(entity);
        }

        public virtual void UpdateRole(Role entity)
        {
            log.DebugFormat("UpdateRole called for Role.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Update);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Update);
            }
            this.DbContext.Update(entity);
        }

        public virtual void DeleteRole(Role entity)
        {
            log.DebugFormat("DeleteRole called for Role.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Delete);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Delete);
            }
            this.DbContext.Delete(entity);
        }

        #endregion

        // *************************************************************************
        #region CodeSigningTypes Methods
        // *************************************************************************

        public virtual IQueryable<CodeSigningType> GetCodeSigningTypes(string includes = null)
        {
            log.DebugFormat("GetCodeSigningTypes called with Includes: {0}", includes);
            return this.DbContext.Set<CodeSigningType>(includes);
        }

        public virtual IEnumerable<CodeSigningType> GetAllCodeSigningTypesFromCache (string includes = null)
        {
            log.DebugFormat("GetAllCodeSigningTypesFromCache called with Includes: {0}", includes);
            return GetAllFromCache<CodeSigningType>(includes);
        }

        public virtual void InvalidateCodeSigningTypesCache ()
        {
            log.DebugFormat("InvalidateCodeSigningTypesCache called");
            InvalidateCache<CodeSigningType>();
        }

        public virtual CodeSigningType GetCodeSigningTypeById(int id, string includes = null)
        {
            log.DebugFormat("GetCodeSigningTypeById called for CodeSigningType.Id: {0} with Includes: {1}", id, includes);
            var entity = this.DbContext.Set<CodeSigningType>(includes).Where(e => e.Id == id).FirstOrDefault();
            return entity;
        }

        public virtual void InsertCodeSigningType(CodeSigningType entity)
        {
            log.DebugFormat("InsertCodeSigningType called");
            UpdateEntityStats(entity, CudOperation.Insert);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Insert);
            }
            this.DbContext.Insert(entity);
        }

        public virtual void UpdateCodeSigningType(CodeSigningType entity)
        {
            log.DebugFormat("UpdateCodeSigningType called for CodeSigningType.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Update);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Update);
            }
            this.DbContext.Update(entity);
        }

        public virtual void DeleteCodeSigningType(CodeSigningType entity)
        {
            log.DebugFormat("DeleteCodeSigningType called for CodeSigningType.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Delete);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Delete);
            }
            this.DbContext.Delete(entity);
        }

        #endregion

        // *************************************************************************
        #region Groups Methods
        // *************************************************************************

        public virtual IQueryable<Group> GetGroups(string includes = null)
        {
            log.DebugFormat("GetGroups called with Includes: {0}", includes);
            return this.DbContext.Set<Group>(includes);
        }

        public virtual IEnumerable<Group> GetAllGroupsFromCache (string includes = null)
        {
            log.DebugFormat("GetAllGroupsFromCache called with Includes: {0}", includes);
            return GetAllFromCache<Group>(includes);
        }

        public virtual void InvalidateGroupsCache ()
        {
            log.DebugFormat("InvalidateGroupsCache called");
            InvalidateCache<Group>();
        }

        public virtual Group GetGroupById(int id, string includes = null)
        {
            log.DebugFormat("GetGroupById called for Group.Id: {0} with Includes: {1}", id, includes);
            var entity = this.DbContext.Set<Group>(includes).Where(e => e.Id == id).FirstOrDefault();
            return entity;
        }

        public virtual void InsertGroup(Group entity)
        {
            log.DebugFormat("InsertGroup called");
            UpdateEntityStats(entity, CudOperation.Insert);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Insert);
            }
            this.DbContext.Insert(entity);
        }

        public virtual void UpdateGroup(Group entity)
        {
            log.DebugFormat("UpdateGroup called for Group.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Update);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Update);
            }
            this.DbContext.Update(entity);
        }

        public virtual void DeleteGroup(Group entity)
        {
            log.DebugFormat("DeleteGroup called for Group.Id: {0}", entity.Id);
            UpdateEntityStats(entity, CudOperation.Delete);
            if (LogCRUDActivity)
            {
                ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.Delete);
            }
            this.DbContext.Delete(entity);
        }

        #endregion

    }
}

