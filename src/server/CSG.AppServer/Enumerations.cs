using System.ComponentModel;

namespace CSG.AppServer
{
    public enum ApplicationOnlineStatus
    {
        Online,
        AppAdminOnly,
        Offline
    }

    public enum CudOperation
    {
        NotACudOperation,
        Insert,
        Update,
        Delete
    }
    public enum UserActions
    {
        [Description("NoAction")]
        NoAction = 0,
        [Description("Submit Requests")]
        SubmitRequests = 1,
        [Description("Approve Requests Group 1")]
        Approve1Requests = 2,
        [Description("Approve Requests Group 2")]
        Approve2Requests = 3,
        [Description("Offline Sign")]
        OfflineSignRequests = 4,
        [Description("Manager Approval Requests")]
        ManagerApproveRequests = 5,
        [Description("Administrate")]
        Administrate = 6,
        [Description("Approve Requests - Administrator")]
        AdministratorApproveRequests = 7,
        [Description("Unknown")]
        Reserved = 8,

        [Description("Invalid Action")]
        Invalid = -1 // used to mark actions that are invalid, see temporal approvals that are applied after a profile change
    }

    public enum CodeSigningRequestStatus
    {
        /// <summary>
        /// This is the state that the request is initially create in.
        /// </summary>
        [Description("Unconfirmed")]
        Unconfirmed = 0,

        /// <summary>
        /// This is the state that the request is in after the user that created the request Confirms it.
        /// This is done as part of the original request creation process.
        /// In this state, no one has yet approved the request
        /// </summary>
        [Description("Submitted")]
        Submitted,

        /// <summary>
        /// This is the state that a request can be in if the original user that created the request "denies" their own request
        /// </summary>
        [Description("Cancelled")]
        Cancelled,

        /// <summary>
        /// This is the state that a request can be in if a user OTHER than the original user that created the request denies the request
        /// </summary>
        [Description("Denied")]
        Denied,

        /// <summary>
        /// This is the state that the request is in after it is approved.
        /// Once it is in this state - the executive should pick it up and start processing it.
        /// </summary>
        [Description("Approved")]
        Approved,

        /// <summary>
        /// This is the state that the request is in after it is approved by at least one responder, but not yet
        /// fully approved.
        /// </summary>
        [Description("Partially Approved")]
        PartiallyApproved,

        /// <summary>
        /// This is the state that the request is in if 72 hours (actually a configurable amount of hours) passes after it was submitted if it is not approved
        /// </summary>
        [Description("Timed Out")]
        TimedOut,

        /// <summary>
        /// This is the state that the request is in once the files have been moved to the staging directory.
        /// </summary>
        [Description("In Process")]
        InProcess,

        /// <summary>
        /// Request has been created via the WebAPI.  Awaiting the upload of the target file.
        /// </summary>
        [Description("AwaitingFile")]
        AwaitingFile,

        /// <summary>
        /// Request is approved, but needs to be manually signed.
        /// </summary>
        [Description("Awaiting Offline Signing")]
        AwaitingOfflineSign,

        [Description("Completed - Success")]
        CompletedSuccess,

        [Description("Completed - Failed")]
        CompletedFailed
    }

    public enum SystemSettingDataType
    {
        Undefined = 0,
        String,
        Integer,
        Double,
        DateTime,
        Boolean,
        Password
    }

}
