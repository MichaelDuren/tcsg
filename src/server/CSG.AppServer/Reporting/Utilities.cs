﻿using System.Collections.Generic;
using System.Linq;
using CSG.AppServer.Db;
using CSG.AppServer.Entities;

namespace CSG.AppServer.Reporting
{
    public class Utilities
    {
        /// <summary>
        /// This method will return an IEnumerable for getting a list of users from the system
        /// If the user is not an admin, they will be limited to people that are part of the groups
        /// they are a part of.  If they are an admin, there will be no restrictions.
        /// </summary>
        public static IEnumerable<UserProfile> GetUsers(UserProfile currentUser, string searchCriteria)
        {
            ESPDbDomainService svc = new ESPDbDomainService();

            if (currentUser.IsAppAdmin)
            {
                var users = svc.GetUserProfiles(null).Where(m => m.DisplayName.Contains(searchCriteria));
                return users;
            }
            else
            {

                // TODO
                return null;
            }
        }

        /// <summary>
        /// This method will return an IEnumerable for getting a list of groups from the system
        /// If the user is not an admin, they will be limited to groups that 
        /// they are a part of.  If they are an admin, there will be no restrictions.
        /// </summary>
        public static IEnumerable<Group> GetGroups(UserProfile currentUser)
        {
            ESPDbDomainService svc = new ESPDbDomainService();

            if (currentUser.IsAppAdmin)
            {
                var groups = svc.GetGroups(null);
                return groups;
            }
            else
            {
                var groups = currentUser.Groups
                                .Select(upg => upg.Group)
                                .Where(g => g.IsDeleted == false);

                return groups;
            }
        }

        /// <summary>
        /// This method will return an IEnumerable for getting a list of tools from the system
        /// </summary>
        public static IEnumerable<CodeSigningTool> GetCodeSigningTools(UserProfile currentUser)
        {
            ESPDbDomainService svc = new ESPDbDomainService();

            if (currentUser.IsAppAdmin)
            {
                var tools = svc.GetCodeSigningTools(null);
                return tools;
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// This method will return an IEnumerable for getting a list of code signing types from the system
        /// If the user is not an admin, they will be limited to code signing types that 
        /// they have access to.  If they are an admin, there will be no restrictions.
        /// </summary>
        public static IEnumerable<CodeSigningType> GetCodeSigningTypes(UserProfile currentUser)
        {
            ESPDbDomainService svc = new ESPDbDomainService();

            if (currentUser.IsAppAdmin)
            {
                var types = svc.GetCodeSigningTypes(null);
                return types;
            }
            else
            {
                var types = svc.GetUserProfileCodeSigningTypes().Where(m => m.UserProfileId == currentUser.Id).Select(upcst => upcst.CodeSigningType);
                return types;
            }
        }
    }
}
