﻿using System;
using System.Collections.Generic;
using System.Linq;
using Util.EF;
using Util.Reports;

namespace CSG.AppServer.Reporting
{
    public class DashboardReport : ESPAdHocReport
    {
        // ********************************************************************************
        #region Constructor
        // ********************************************************************************

        public DashboardReport()
        {
            //Have to default to -1 so that the UI specific value <ANY> is chosen by default intead
            //of Unconfirmed (0) since ints get initialized to 0
            StatusId = -1;
            SelectedSaveType = "HTML";
        }

        #endregion

        // ********************************************************************************
        #region Properties
        // ********************************************************************************

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int StatusId { get; set; }
        public int UserId { get; set; }
        public string User { get; set; }
        public int RoleId { get; set; }
        public int GroupId { get; set; }
        public int CodeSigningTypeId { get; set; }

        #endregion

        // ********************************************************************************
        #region Methods
        // ********************************************************************************

        public override StandardValidators ValidateCriteria()
        {
            StandardValidators validators = new StandardValidators();

            validators.ValidateFailIfTrue(StartDate > EndDate, propertyName: "Start Date", errorMessage: "The Start Date must be before the End Date.");
            validators.ValidateFailIfTrue(RoleId > 0 && UserId < 1, propertyName: "User", errorMessage: "If you are searching by role, you must identify the user.");
            validators.ValidateFailIfTrue(RoleId == 0 && UserId > 0, propertyName: "Role", errorMessage: "If you are searching by user, you must identify the user's role.");

            return validators;
        }

        protected override void Initialize()
        {
            base.Initialize();

            ReportDefinition = new AdHocReportDefinition("Dashboard Report");

            ReportDefinition.ReportColumnDefinitions.Add(new AdHocReportColumnDefinition("Id"));
            ReportDefinition.ReportColumnDefinitions.Add(new AdHocReportColumnDefinition("SigningType"));
            ReportDefinition.ReportColumnDefinitions.Add(new AdHocReportColumnDefinition("Group"));
            //CR01
            ReportDefinition.ReportColumnDefinitions.Add(new AdHocReportColumnDefinition("FileName"));
            ReportDefinition.ReportColumnDefinitions.Add(new AdHocReportColumnDefinition("SubmittedBy"));
            ReportDefinition.ReportColumnDefinitions.Add(new AdHocReportColumnDefinition("SubmitDate"));
            ReportDefinition.ReportColumnDefinitions.Add(new AdHocReportColumnDefinition("Responder"));
            ReportDefinition.ReportColumnDefinitions.Add(new AdHocReportColumnDefinition("ResponseDate"));
            ReportDefinition.ReportColumnDefinitions.Add(new AdHocReportColumnDefinition("CompletionDate"));
            ReportDefinition.ReportColumnDefinitions.Add(new AdHocReportColumnDefinition("UnsignedHash"));
            ReportDefinition.ReportColumnDefinitions.Add(new AdHocReportColumnDefinition("SignedHash"));

            //CR01
            var results =
              from csr in DomainService.GetCodeSigningRequests("CodeSigningType, CreatedByUser, Responder")
              select new RequestInfo
              {
                  Id = csr.Id,
                  StatusId = csr.StatusId,
                  SigningType = csr.CodeSigningType.Name,
                  SigningTypeId = csr.CodeSigningTypeId,
                  FileName = csr.UnsignedFileName,
                  SubmittedBy = csr.CreatedByUser.DisplayName,
                  SubmittedById = csr.CreatedByUser.Id,
                  SubmitDate = csr.CreatedDate,
                  ManagerResponderId = csr.ManagerResponderId,
                  ManagerResponder = (csr.ManagerResponder != null ? csr.ManagerResponder.DisplayName : ""),
                  ManagerResponseDate = csr.ManagerResponseDate,
                  Approver1ResponderId = csr.Approver1ResponderId,
                  Approver1Responder = (csr.Approver1Responder != null ? csr.Approver1Responder.DisplayName : ""),
                  Approver1ResponseDate = csr.Approver1ResponseDate,
                  Approver2ResponderId = csr.Approver2ResponderId,
                  Approver2Responder = (csr.Approver2Responder != null ? csr.Approver2Responder.DisplayName : ""),
                  Approver2ResponseDate = csr.Approver2ResponseDate,
                  CompletionDate = csr.CompletionDate,
                  UnsignedHash = csr.UnsignedFileHash,
                  SignedHash = csr.SignedFileHash
              };

            if (StartDate != null)
            {
                //CR05
                //Set to start of day so that anything created during the day is returned
                StartDate = new DateTime(StartDate.Value.Year, StartDate.Value.Month, StartDate.Value.Day, 0, 0, 0);

                results = results.Where(csr => csr.SubmitDate > StartDate);

                if (EndDate != null)
                {
                    //CR05
                    //Set to end of day so that anything created during the day is returned
                    EndDate = new DateTime(EndDate.Value.Year, EndDate.Value.Month, EndDate.Value.Day, 23, 59, 59);

                    results = results.Where(csr => csr.SubmitDate < EndDate);
                }
            }
            else
            {
                if (EndDate != null)
                {
                    //CR05
                    //Set to end of day so that anything created during the day is returned
                    EndDate = new DateTime(EndDate.Value.Year, EndDate.Value.Month, EndDate.Value.Day, 23, 59, 59);

                    results = results.Where(csr => csr.SubmitDate < EndDate);
                }
            }

            if (this.StatusId != -1)
            {
                //If they are searching for the user that denied the request, the status must be Denied which will be handled below
                if (this.RoleId != 3)
                {
                    results = results.Where(csr => csr.StatusId == this.StatusId);
                }
            }

            if (this.RoleId > 0)
            {
                switch (this.RoleId)
                {
                    case 1:
                        results = results.Where(csr => csr.SubmittedById == this.UserId);
                        break;
                    case 2:
                        results = results.Where(csr => (csr.Approver1ResponderId == this.UserId ||
                                                        csr.Approver2ResponderId == this.UserId ||
                                                        csr.ManagerResponderId == this.UserId) && csr.StatusId != (int)CodeSigningRequestStatus.Denied);
                        break;
                    case 3:
                        results = results.Where(csr => (csr.Approver1ResponderId == this.UserId ||
                                                        csr.Approver2ResponderId == this.UserId ||
                                                        csr.ManagerResponderId == this.UserId) && csr.StatusId == (int)CodeSigningRequestStatus.Denied);
                        break;
                }
            }

            if (this.GroupId != 0)
            {
                results = results.Where(csr => csr.GroupId == this.GroupId);
            }

            if (this.CodeSigningTypeId != 0)
            {
                results = results.Where(csr => csr.SigningTypeId == this.CodeSigningTypeId);
            }

            results = results.OrderBy(csr => csr.SubmitDate);

            List<RequestInfo> requests = results.ToList<RequestInfo>();

            foreach (RequestInfo request in requests)
            {
                request.Status = ((CodeSigningRequestStatus)request.StatusId).ToString();
            }

            //CR01
            var results2 =
                  from csr in requests
                  select new
                  {
                      Id = csr.Id,
                      Status = csr.Status,
                      SigningType = csr.SigningType,
                      FileName = csr.FileName,
                      SubmittedBy = csr.SubmittedBy,
                      SubmitDate = csr.SubmitDate,
                      ManagerResponder = csr.ManagerResponder,
                      ManagerResponseDate = csr.ManagerResponseDate,
                      Approver1Responder = csr.Approver1Responder,
                      Approver1ResponseDate = csr.Approver1ResponseDate,
                      Approver2Responder = csr.Approver2Responder,
                      Approver2ResponseDate = csr.Approver2ResponseDate,
                      CompletionDate = csr.CompletionDate,
                      UnsignedHash = csr.UnsignedHash,
                      SignedHash = csr.SignedHash
                  };

            SetDataSource(results2.AsDataReader());
        }

        #endregion
    }
}
