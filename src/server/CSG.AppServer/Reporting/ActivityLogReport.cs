﻿using System;
using System.Linq;
using Util.EF;
using Util.Reports;

namespace CSG.AppServer.Reporting
{
    public class ActivityLogReport : ESPAdHocReport
    {
        // ********************************************************************************
        #region Constructor
        // ********************************************************************************

        public ActivityLogReport() { }

        #endregion

        // ********************************************************************************
        #region Properties
        // ********************************************************************************

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string LogEntryLevel { get; set; }

        #endregion

        // ********************************************************************************
        #region Methods
        // ********************************************************************************

        public override StandardValidators ValidateCriteria()
        {
            StandardValidators validators = new StandardValidators();

            validators.ValidateFailIfTrue(StartDate > EndDate, propertyName: "Start Date", errorMessage: "The Start Date must be before the End Date.");

            return validators;
        }

        protected override void Initialize()
        {
            base.Initialize();

            ReportDefinition = new AdHocReportDefinition("Activity Log Report");

            var results =
              from al in DomainService.GetActivityLogEntries()
              select new
              {
                  UserName = al.UserName,
                  LogEntryLevel = al.LogEntryLevel,
                  LogTime = al.LogTime,
                  ActivityType = al.ActivityType,
                  ActivitySubType = al.ActivitySubType,
                  Details = al.Details,
                  Server = al.Server
              };

            if (StartDate != null)
            {
                //CR05
                //Set to start of day so that anything created during the day is returned
                StartDate = new DateTime(StartDate.Value.Year, StartDate.Value.Month, StartDate.Value.Day, 0, 0, 0);

                results = results.Where(al => al.LogTime > StartDate);

                if (EndDate != null)
                {
                    //CR05
                    //Set to end of day so that anything created during the day is returned
                    EndDate = new DateTime(EndDate.Value.Year, EndDate.Value.Month, EndDate.Value.Day, 23, 59, 59);

                    results = results.Where(al => al.LogTime < EndDate);
                }
            }
            else
            {
                if (EndDate != null)
                {
                    //CR05
                    //Set to end of day so that anything created during the day is returned
                    EndDate = new DateTime(EndDate.Value.Year, EndDate.Value.Month, EndDate.Value.Day, 23, 59, 59);

                    results = results.Where(al => al.LogTime < EndDate);
                }
            }

            if (!String.IsNullOrWhiteSpace(LogEntryLevel))
            {
                results = results.Where(al => al.LogEntryLevel == LogEntryLevel);
            }

            results = results.OrderBy(al => al.LogTime);

            SetDataSource(results.AsDataReader());
        }

        #endregion
    }
}
