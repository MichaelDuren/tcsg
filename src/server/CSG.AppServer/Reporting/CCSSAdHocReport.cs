﻿using CSG.AppServer.Db;
using Util.Reports;

namespace CSG.AppServer.Reporting
{
    public class ESPAdHocReport : AdHocReport
    {
        public ESPDbDomainService DomainService { get; set; }

        public ESPAdHocReport()
        {
            DomainService = new ESPDbDomainService();
        }

        public ESPAdHocReport(AdHocReportDefinition reportDefinition)
            : base(reportDefinition) { }

        public ESPAdHocReport(ReportWriterType reportWriterType, AdHocReportDefinition reportDefinition)
            : base(reportDefinition, reportWriterType) { }

        protected override void CreateReportHeader()
        {
            // Add any logic here that you want - to modify the standard report header

            // Or - just use the standard report header
            base.CreateReportHeader();
        }

        public string SelectedSaveType { get; set; }
    }
}
