﻿using System;

namespace CSG.AppServer.Reporting
{
    public class RequestInfo
    {
        public int? Id { get; set; }
        public int? StatusId { get; set; }
        public string Status { get; set; }
        public string SigningType { get; set; }
        public int? SigningTypeId { get; set; }
        public string Group { get; set; }
        public int? GroupId { get; set; }
        public string SubmittedBy { get; set; }
        public int? SubmittedById { get; set; }
        public DateTime? SubmitDate { get; set; }
        public string ManagerResponder { get; set; }
        public int? ManagerResponderId { get; set; }
        public DateTime? ManagerResponseDate { get; set; }
        public string Approver1Responder { get; set; }
        public int? Approver1ResponderId { get; set; }
        public DateTime? Approver1ResponseDate { get; set; }
        public string Approver2Responder { get; set; }
        public int? Approver2ResponderId { get; set; }
        public DateTime? Approver2ResponseDate { get; set; }

        public DateTime? CompletionDate { get; set; }
        public string UnsignedHash { get; set; }
        public string SignedHash { get; set; }
        public string FileName { get; set; }
    }
}
