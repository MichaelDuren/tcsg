﻿using System;
using System.Collections.Generic;
using System.Linq;
using CSG.AppServer.Entities;
using Util.Collections.Generic.RC;
using Util.Reports;

namespace CSG.AppServer.Reporting
{
    public enum UsageReportType
    {
        Group,
        User,
        CodeSigningType
    }

    public class UsageReport : ESPAdHocReport
    {
        class GroupedResultDto
        {
            public string Name { get; set; }
            public int Year { get; set; }
            public int Month { get; set; }
            public int Count { get; set; }

            public string YearMonth
            {
                get { return UsageReport.CreateReadableMonth(Year, Month); }
            }
        }

        // ********************************************************************************
        #region Constructor
        // ********************************************************************************

        public UsageReport()
        {
            UserId = 0;
            UserIsAppAdmin = false;
            SelectedSaveType = "HTML";
        }

        #endregion

        // ********************************************************************************
        #region Properties
        // ********************************************************************************

        //private RowColumnCollection<string, string> accumulator;

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public UsageReportType UsageReportTypeId { get; set; }
        public int UserId { get; set; }
        public bool UserIsAppAdmin { get; set; }
        public int GroupId { get; set; }
        public int CodeSigningTypeId { get; set; }

        // Needed for Report Criteria UI ???
        public string User { get; set; }

        #endregion

        // ********************************************************************************
        #region Methods
        // ********************************************************************************

        public override StandardValidators ValidateCriteria()
        {
            StandardValidators validators = new StandardValidators();

            validators.ValidateFailIfTrue(StartDate > EndDate, propertyName: "Start Date", errorMessage: "The Start Date must be before the End Date.");

            return validators;
        }

        protected override void Initialize()
        {
            base.Initialize();

            ReportDefinition = new AdHocReportDefinition("Usage Report");

            // If no dates provided, default to past year
            if (StartDate == null && EndDate == null)
            {
                EndDate = DateTime.Now;
                StartDate = EndDate.Value.AddYears(-1);
            }
            else
            {
                if (StartDate != null)
                {
                    //CR05
                    //Set to start of day so that anything created during the day is returned
                    StartDate = new DateTime(StartDate.Value.Year, StartDate.Value.Month, StartDate.Value.Day, 0, 0, 0);
                    if (EndDate == null)
                    {
                        if (StartDate > DateTime.Now)
                        {
                            // default to the same day
                            EndDate = new DateTime(StartDate.Value.Year, StartDate.Value.Month, StartDate.Value.Day, 23, 59, 59);
                        }
                        else
                        {
                            // default to the same day
                            EndDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
                        }
                    }
                }
                if (EndDate != null)
                {
                    //CR05
                    //Set to end of day so that anything created during the day is returned
                    EndDate = new DateTime(EndDate.Value.Year, EndDate.Value.Month, EndDate.Value.Day, 23, 59, 59);
                    if (StartDate == null)
                    {
                        if (EndDate > DateTime.Now)
                        {
                            // default to the same day
                            StartDate = new DateTime(StartDate.Value.Year, StartDate.Value.Month, StartDate.Value.Day, 00, 00, 00);
                        }
                        else
                        {
                            // default to a day in the past
                            StartDate = new DateTime(1980, 0, 0);
                        }
                    }
                }
            }

            IQueryable<CodeSigningRequest> filteredData = GetDataFilteredByReportCriteria();
            List<GroupedResultDto> groupedData = GetGroupedData(filteredData);
            StandardRowColumnCollection<string> accumulator = PivotData(groupedData);

            SetDataSource(accumulator);
        }

        /// <summary>
        /// Do initial filtering based on the report criteria
        /// </summary>
        private IQueryable<CodeSigningRequest> GetDataFilteredByReportCriteria()
        {
            var results = DomainService.GetCodeSigningRequests();

            results = results.Where(r => r.CreatedDate > StartDate);
            results = results.Where(r => r.CreatedDate < EndDate);

            // constrain the report if this is not an administrative user
            if (!UserIsAppAdmin)
            {
                results = results.Where(r => r.CreatedByUserId == UserId || r.Approver1ResponderId == UserId ||
                                             r.ManagerResponderId == UserId);
            }

            if (this.UsageReportTypeId == UsageReportType.Group && this.GroupId > 0)
            {
//                results = results.Where(r => r.GroupId == GroupId);
            }
            else if (this.UsageReportTypeId == UsageReportType.CodeSigningType && this.CodeSigningTypeId > 0)
            {                
                results = results.Where(r => r.CodeSigningTypeId == CodeSigningTypeId);
            }
            else if (this.UsageReportTypeId == UsageReportType.User && this.UserId > 0)
            {
                results = results.Where(r => r.CreatedByUserId == UserId || r.Approver1ResponderId == UserId ||
                                             r.ManagerResponderId == UserId);
            }

            return results;
        }

        private List<GroupedResultDto> GetGroupedData(IQueryable<CodeSigningRequest> requests)
        {
            IQueryable<GroupedResultDto> results;

            if (this.UsageReportTypeId == UsageReportType.Group)
            {
                results = from r in requests
                          group r by new
                          {
                              Year = r.CreatedDate.Value.Year,
                              Month = r.CreatedDate.Value.Month
                          } into grouping
                          select new GroupedResultDto
                          {
                              Year = grouping.Key.Year,
                              Month = grouping.Key.Month,
                              Count = grouping.Count()
                          }
                    ;
            }
            else if (this.UsageReportTypeId == UsageReportType.CodeSigningType)
            {
                results = from r in requests
                          group r by new
                          {
                              Name = r.CodeSigningType.Name,
                              Year = r.CreatedDate.Value.Year,
                              Month = r.CreatedDate.Value.Month
                          } into grouping
                          select new GroupedResultDto
                          {
                              Name = grouping.Key.Name,
                              Year = grouping.Key.Year,
                              Month = grouping.Key.Month,
                              Count = grouping.Count()
                          }
                    ;
            }
            else if (this.UsageReportTypeId == UsageReportType.User)
            {
                results = from r in requests
                          group r by new
                          {
                              Name = r.CreatedByUser.DisplayName,
                              Year = r.CreatedDate.Value.Year,
                              Month = r.CreatedDate.Value.Month
                          } into grouping
                          select new GroupedResultDto
                          {
                              Name = grouping.Key.Name,
                              Year = grouping.Key.Year,
                              Month = grouping.Key.Month,
                              Count = grouping.Count()
                          }
                    ;
            }
            else
            {
                throw new ArgumentOutOfRangeException("Invalid UsageReportType: " + this.UsageReportTypeId);
            }

            return results.ToList();
        }

        private StandardRowColumnCollection<string> PivotData(List<GroupedResultDto> groupedData)
        {
            StandardRowColumnCollection<string> accumulator = new StandardRowColumnCollection<string>();

            // Create Column Names
            accumulator.Columns.Add("Name");
            accumulator.Columns.Add(GetMonthNames());

            // Create the Rows - and populate the Name column with the row name
            List<string> rowNames = (from o in groupedData
                                     orderby o.Name
                                     select o.Name
                ).Distinct().ToList();

            foreach (var rowName in rowNames)
            {
                accumulator.Rows[rowName].Cells["Name"].StringValue = rowName;
            }

            // Populate the Accumulator
            foreach (var gd in groupedData)
            {
                accumulator.Rows[gd.Name].Cells[gd.YearMonth].IntValue = gd.Count;
            }

            return accumulator;
        }

        private List<string> GetMonthNames()
        {
            List<string> monthNames = new List<string>();

            DateTime d = (DateTime)StartDate;

            while (true)
            {
                monthNames.Add(CreateReadableMonth(d.Year, d.Month));

                if (d.Year >= ((DateTime)EndDate).Year && d.Month >= ((DateTime)EndDate).Month)
                {
                    break;
                }

                d = d.AddMonths(1);
            }

            return monthNames;
        }

        #endregion

        // ********************************************************************************
        #region Support Methods
        // ********************************************************************************

        public static string CreateReadableMonth(int year, int month)
        {
            string monthName;

            switch (month)
            {
                case 1: monthName = "January"; break;
                case 2: monthName = "February"; break;
                case 3: monthName = "March"; break;
                case 4: monthName = "April"; break;
                case 5: monthName = "May"; break;
                case 6: monthName = "June"; break;
                case 7: monthName = "July"; break;
                case 8: monthName = "August"; break;
                case 9: monthName = "September"; break;
                case 10: monthName = "October"; break;
                case 11: monthName = "November"; break;
                case 12: monthName = "December"; break;
                default: throw new ArgumentOutOfRangeException("Invalid Month Value");
            }

            return string.Format("{0} - {1}", year, monthName);
        }

        #endregion
    }
}
