﻿using System;
using System.Text;
using Util.Reports;

namespace CSG.AppServer.Reporting
{
    //public enum UsageReportType
    //{
    //    Group,
    //    User,
    //    CodeSigningType
    //}

    public class OLD_UsageReport : ESPAdHocReport
    {
        // ********************************************************************************
        #region Constructor
        // ********************************************************************************

        public OLD_UsageReport()
        {
            SelectedSaveType = "HTML";
        }

        #endregion

        // ********************************************************************************
        #region Properties
        // ********************************************************************************

        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public UsageReportType UsageReportTypeId { get; set; }
        public int UserId { get; set; }
        public string User { get; set; }
        public int GroupId { get; set; }
        public int CodeSigningTypeId { get; set; }

        #endregion

        // ********************************************************************************
        #region Methods
        // ********************************************************************************

        public override StandardValidators ValidateCriteria()
        {
            StandardValidators validators = new StandardValidators();

            validators.ValidateFailIfTrue(StartDate > EndDate, propertyName: "Start Date", errorMessage: "The Start Date must be before the End Date.");

            return validators;
        }

        protected override void Initialize()
        {
            base.Initialize();

            ReportDefinition = new AdHocReportDefinition("Usage Report");

            //If no dates provided, default to past year
            if (StartDate == null && EndDate == null)
            {
                EndDate = DateTime.Now;
                StartDate = EndDate.Value.AddYears(-1);
            }

            ReportDefinition.DataParameters.Add(new Util.Sql.DataParameter { ParameterName = "@StartDate", Value = this.StartDate });
            ReportDefinition.DataParameters.Add(new Util.Sql.DataParameter { ParameterName = "@EndDate", Value = this.EndDate });

            TimeSpan? ts = EndDate - StartDate;
            int monthCount = (ts.Value.Days / 30) + 1;

            DateTime tempDate;

            StringBuilder sql = new StringBuilder(SqlSnippets.SelectClause1);

            for (int i = 0; i < monthCount; i++)
            {
                tempDate = StartDate.Value.AddMonths(i);
                sql.Append(String.Format(" SUM([{0}-{1}]) AS [{0}-{1}]", tempDate.Year, tempDate.ToString("MMMM")));
                if (i < monthCount - 1)
                {
                    sql.Append(",");
                }
            }

            sql.Append(SqlSnippets.SelectClause2);

            for (int i = 0; i < monthCount; i++)
            {
                tempDate = StartDate.Value.AddMonths(i);
                sql.Append(String.Format(SqlSnippets.MonthCase, i.ToString(), tempDate.Year, tempDate.ToString("MMMM")));
                if (i < monthCount - 1)
                {
                    sql.Append(",");
                }
            }

            sql.Append(SqlSnippets.FromClause1);

            if (this.UsageReportTypeId == UsageReportType.Group)
            {
                sql.Append(SqlSnippets.FromNameClauseGroup);
            }
            else if (this.UsageReportTypeId == UsageReportType.CodeSigningType)
            {
                sql.Append(SqlSnippets.FromNameClauseCodeSigningType);
            }
            else if (this.UsageReportTypeId == UsageReportType.User)
            {
                sql.Append(SqlSnippets.FromNameClauseUser);
            }

            sql.Append(SqlSnippets.FromClause2);

            if (this.UsageReportTypeId == UsageReportType.Group)
            {
                if (this.GroupId > 0)
                {
                    sql.Append(" AND GroupId = @GroupId");
                    ReportDefinition.DataParameters.Add(new Util.Sql.DataParameter { ParameterName = "@GroupId", Value = this.GroupId });
                }
            }
            else if (this.UsageReportTypeId == UsageReportType.CodeSigningType)
            {
                if (this.CodeSigningTypeId > 0)
                {
                    sql.Append(" AND CodeSigningTypeId = @CodeSigningTypeId");
                    ReportDefinition.DataParameters.Add(new Util.Sql.DataParameter { ParameterName = "@CodeSigningTypeId", Value = this.CodeSigningTypeId });
                }
            }
            else if (this.UsageReportTypeId == UsageReportType.User)
            {
                if (this.UserId > 0)
                {
                    sql.Append(" AND CreatedByUserId = @CreatedByUserId");
                    ReportDefinition.DataParameters.Add(new Util.Sql.DataParameter { ParameterName = "@CreatedByUserId", Value = this.UserId });
                }
            }

            sql.Append(SqlSnippets.FromClause3);

            ReportDefinition.Sql = sql.ToString();

            SetDataSource(DomainService.DbContext.Database.Connection);
        }

        #endregion

        // ********************************************************************************
        #region Internal Classes, etc.
        // ********************************************************************************

        //public class UsageMetric
        //{
        //    public int Year { get; set; }
        //    public int Month { get; set; }
        //    public int RequestCount { get; set; }
        //    public int SearchId { get; set; }
        //    public string Name { get; set; }
        //}

        class SqlSnippets
        {
            public static string SelectClause1 = "SELECT [Name], ";
            public static string SelectClause2 = "FROM " +
                                                 "( " +
                                                  "SELECT [Name], ";
            public static string MonthCase = " 	   CASE WHEN YearId = datepart(year, dateadd(month, {0}, @StartDate))" +
                                                       " AND MonthId = datepart(month, dateadd(month, {0}, @StartDate)) " +
                                                      " THEN RequestCount " +
                                                      " ELSE 0 " +
                                                 " END AS [{1}-{2}]";

            public static string FromClause1 = "FROM ( " +
                                                        "SELECT [Name], YearId, MonthId, Count(Id) AS RequestCount " +
                                                        "FROM (SELECT	Id, " +
                                                        "		datepart(month, CreatedDate) as MonthId, " +
                                                        "		datepart(year, CreatedDate) as YearId, " +
                                                        "		CodeSigningTypeId, " +
                                                        "		GroupId, ";

            public static string FromClause2 = "		CreatedByUserId " +
                                                "  FROM App_CodeSigningRequests " +
                                                "  WHERE CreatedDate >= @StartDate " +
                                                "    AND CreatedDate <= @EndDate ";

            public static string FromClause3 = ") as d1 " +
                                            " GROUP BY [Name], YearId, MonthId" +
                                            ") as d2 " +
                                            ") as d3 " +
                                            " GROUP BY [Name]";

            public static string FromNameClauseGroup = "		(SELECT Name FROM Lookup_Groups WHERE Id = GroupId) as [Name],";
            public static string FromNameClauseCodeSigningType = "		(SELECT Name FROM Lookup_CodeSigningTypes WHERE Id = CodeSigningTypeId) as [Name],";
            public static string FromNameClauseUser = "		(SELECT DisplayName FROM App_UserProfiles WHERE Id = CreatedByUserId) as [Name], ";
        }

        #endregion
    }
}
