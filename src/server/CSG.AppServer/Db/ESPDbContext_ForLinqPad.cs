using System.Data.Entity;
using CSG.AppServer.Entities;

namespace CSG.AppServer.Db
{
    public partial class ESPDbContext_ForLinqPad : DbContext
    {
        // *********************************************************************************
        #region DbSets
        // *********************************************************************************

        // Generally these are in the Generated DbContext
        public DbSet<ActivityLogEntry> ActivityLogEntries { get; set; }
        public DbSet<CodeSigningTypeGroup> CodeSigningTypeGroups { get; set; }
        public DbSet<SystemSetting> SystemSettings { get; set; }
        public DbSet<UserProfileCodeSigningType> UserProfileCodeSigningTypes { get; set; }
        public DbSet<UserProfileGroup> UserProfileGroups { get; set; }
        public DbSet<UserProfileRole> UserProfileRoles { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<CodeSigningRequest> CodeSigningRequests { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<CodeSigningType> CodeSigningTypes { get; set; }
        public DbSet<Group> Groups { get; set; }

        #endregion

        // *********************************************************************************
        #region Constructors
        // *********************************************************************************

        public ESPDbContext_ForLinqPad(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
            System.Data.Entity.Database.SetInitializer<ESPDbContext>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            // Removing cascade deletes
            modelBuilder.Conventions.Remove<System.Data.Entity.ModelConfiguration.Conventions.ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<System.Data.Entity.ModelConfiguration.Conventions.OneToManyCascadeDeleteConvention>();
        }

        #endregion
    }
}
