using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using CSG.AppServer.Entities;
using Util;
using Util.Config;

namespace CSG.AppServer.Db
{
    public class ESPDbInitializer
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private ESPDbContext DbContext;

        private bool ForceRecreateDb = StringTools.GetBoolean(DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.Testing_ForceRecreateDb]);

        private int indexCreationErrors;


        // *********************************************************************************
        #region Public and Core Methods
        // *********************************************************************************

        public bool CheckIfInitializeRequired()
        {
            if (ForceRecreateDb)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public virtual void InitializeDb()
        {
            if (AppServerContext.IsProductionEnvironment)
            {
                throw new Exception("Cannot initialize the DB in Production.");
            }

            DbContext = new ESPDbContext();

            if (ForceRecreateDb)
            {
                RecreateDb();
                SeedDb();
            }

            log.Info("TestEnvironmentInitializer.Initialize() complete");
        }

        private void RecreateDb()
        {
            if (DbContext.Database.Exists())
            {
                log.Info("Deleting Database");
                DbContext.Database.Delete();
            }

            if (!DbContext.Database.Exists())
            {
                DbContext.Database.Create();
                log.Info("Database recreated");
            }

            DbContext = new ESPDbContext();

            // Uncomment this if this script is needed.
            //string script = ((IObjectContextAdapter)DbContext).ObjectContext.CreateDatabaseScript();

            OnAfterDbCreation();

            CreateIndexes();
        }

        #endregion

        // *********************************************************************************
        #region Editable Methods - for App Specific Logic
        // *********************************************************************************

        private void OnAfterDbCreation()
        {
            // Can execute additional scripts if needed
            //ExecuteSqlScriptFile(@"PUT FILE PATH HERE");
        }

        private void CreateIndexes()
        {
            log.Info("Creating Indexes");

            // *******************************************
            // App Tables
            // *******************************************

            //Create for all foreign keys
            CreateNonClusteredIndex("dbo.App_CodeSigningRequests", "App_CodeSigningRequests_GroupId", "GroupId");
            CreateNonClusteredIndex("dbo.App_CodeSigningRequests", "App_CodeSigningRequests_CodeSigningTypeId", "CodeSigningTypeId");
            CreateNonClusteredIndex("dbo.App_CodeSigningRequests", "App_CodeSigningRequests_StatusId", "StatusId");
            CreateNonClusteredIndex("dbo.App_CodeSigningRequests", "App_CodeSigningRequests_ResponderId", "ResponderId");
            CreateNonClusteredIndex("dbo.App_CodeSigningRequests", "App_CodeSigningRequests_CreatedByUserId", "CreatedByUserId");

            CreateNonClusteredIndex("dbo.App_UserProfileRoles", "App_UserProfileRoles_RoleId", "RoleId");
            CreateNonClusteredIndex("dbo.App_UserProfileRoles", "App_UserProfileRoles_UserProfileId", "UserProfileId");

            CreateNonClusteredIndex("dbo.App_UserProfileGroups", "App_UserProfileGroups_GroupId", "GroupId");
            CreateNonClusteredIndex("dbo.App_UserProfileGroups", "App_UserProfileGroups_UserProfileId", "UserProfileId");

            CreateNonClusteredIndex("dbo.App_UserProfileCodeSigningTypes", "App_UserProfileCodesigningTypes_CodeSigningTypeId", "CodeSigningTypeId");
            CreateNonClusteredIndex("dbo.App_UserProfileCodeSigningTypes", "App_UserProfileCodeSigningTypes_UserProfileId", "UserProfileId");

            CreateNonClusteredIndex("dbo.App_CodeSigningTypeGroups", "App_CodeSigningTypeGroups_GroupId", "GroupId");
            CreateNonClusteredIndex("dbo.App_CodeSigningTypeGroups", "App_CodeSigningTypeGroups_CodeSigningTypeId", "CodeSigningTypeId");

            //Create Unique Indexes
            CreateUniqueIndex("dbo.Lookup_Groups", "Lookup_Groups_DomainGroupName", "Domain, Name");
            CreateUniqueIndex("dbo.Lookup_Groups", "Lookup_Groups_ActiveDirectorySId", "ActiveDirectorySId");

            CreateUniqueIndex("dbo.Lookup_Roles", "Lookup_Roles_Name", "Name");

            CreateUniqueIndex("dbo.Lookup_CodeSigningTypes", "Lookup_CodeSigningTypes_Name", "Name");

            CreateUniqueIndex("dbo.App_UserProfiles", "App_UserProfiles_DomainLoginId", "Domain, LoginId");
            CreateUniqueIndex("dbo.App_UserProfiles", "App_UserProfiles_ActiveDirectorySId", "ActiveDirectorySId");

            CreateUniqueIndex("dbo.App_CodeSigningTypeGroups", "App_CodeSigningTypeGroups_TypeGroup", "CodeSigningTypeId, GroupId");

            if (indexCreationErrors > 0)
            {
                throw new Exception("Error were encountered creating Indexes - see log for details.");
            }
        }

        private void SeedDb()
        {
            log.Info("Seeding database");

            PopulateSecurityTables();
            PopulateDefaultDataTables();

            //Save the changes
            DbContext.SaveChanges();
        }

        private void PopulateSecurityTables()
        {
        }

        private void PopulateDefaultDataTables()
        {
            Group testTeam1 = DbContext.Set<Group>().Where(g => g.Name == "ESP Dev Team 1").FirstOrDefault();
            Group testTeam2 = DbContext.Set<Group>().Where(g => g.Name == "ESP Dev Team 2").FirstOrDefault();

            CodeSigningType csType = DbContext.Insert<CodeSigningType>(new CodeSigningType { Name = "Adobe", IsDeleted = false, SignedDirectory = @"C:\temp\signed", StagingDirectory = @"C:\temp\staging" });
            DbContext.Insert<CodeSigningTypeGroup>(new CodeSigningTypeGroup { Group = testTeam1, CodeSigningType = csType });

            csType = DbContext.Insert<CodeSigningType>(new CodeSigningType { Name = "Authenticode", IsDeleted = false, SignedDirectory = @"C:\temp\signed", StagingDirectory = @"C:\temp\staging" });
            DbContext.Insert<CodeSigningTypeGroup>(new CodeSigningTypeGroup { Group = testTeam1, CodeSigningType = csType });
            DbContext.Insert<CodeSigningTypeGroup>(new CodeSigningTypeGroup { Group = testTeam2, CodeSigningType = csType });

            csType = DbContext.Insert<CodeSigningType>(new CodeSigningType { Name = "DOCSIS STB (T2W)", IsDeleted = false, SignedDirectory = @"C:\temp\signed", StagingDirectory = @"C:\temp\staging" });
            DbContext.Insert<CodeSigningTypeGroup>(new CodeSigningTypeGroup { Group = testTeam1, CodeSigningType = csType });

            csType = DbContext.Insert<CodeSigningType>(new CodeSigningType { Name = "DOCSIS Certwave (modems)", IsDeleted = false, SignedDirectory = @"C:\temp\signed", StagingDirectory = @"C:\temp\staging" });
            DbContext.Insert<CodeSigningTypeGroup>(new CodeSigningTypeGroup { Group = testTeam1, CodeSigningType = csType });

            csType = DbContext.Insert<CodeSigningType>(new CodeSigningType { Name = "Firefox", IsDeleted = false, SignedDirectory = @"C:\temp\signed", StagingDirectory = @"C:\temp\staging" });
            DbContext.Insert<CodeSigningTypeGroup>(new CodeSigningTypeGroup { Group = testTeam2, CodeSigningType = csType });

            csType = DbContext.Insert<CodeSigningType>(new CodeSigningType { Name = "Mac", IsDeleted = false, SignedDirectory = @"C:\temp\signed", StagingDirectory = @"C:\temp\staging" });
            DbContext.Insert<CodeSigningTypeGroup>(new CodeSigningTypeGroup { Group = testTeam2, CodeSigningType = csType });

            csType = DbContext.Insert<CodeSigningType>(new CodeSigningType { Name = "Whitesky", IsDeleted = false, SignedDirectory = @"C:\temp\signed", StagingDirectory = @"C:\temp\staging" });
            DbContext.Insert<CodeSigningTypeGroup>(new CodeSigningTypeGroup { Group = testTeam2, CodeSigningType = csType });

            DbContext.SaveChanges();
        }

        #endregion

        // *********************************************************************************
        #region Support Methods
        // *********************************************************************************

        private void CreateNonClusteredIndex(string tableName, string indexName, string indexColumns)
        {
            CreateIndex(tableName, "NONCLUSTERED", indexName, indexColumns);
        }

        private void CreateUniqueIndex(string tableName, string indexName, string indexColumns)
        {
            CreateIndex(tableName, "UNIQUE", indexName, indexColumns);
        }

        private void CreateIndex(string tableName, string indexType, string indexName, string indexColumns)
        {
            string sql = null;

            try
            {
                sql = string.Format("CREATE {0} INDEX {1} ON {2} ({3});", indexType, indexName, tableName, indexColumns);
                ExecuteSql(sql);
            }
            catch (Exception e)
            {
                indexCreationErrors++;

                log.ErrorFormat("Exception occurred attempting to create index.  SQL: {0}\n  Exception Message: {1}", sql, e.Message);
            }
        }

        private void ExecuteSql(string sql)
        {
            // NOTE: Not sure why we need to close the connection first...  Actually - it is NOT like this in other places...
            if (this.DbContext.Database.Connection.State == ConnectionState.Open)
            {
                this.DbContext.Database.Connection.Close();
            }
            this.DbContext.Database.ExecuteSqlCommand(sql);
        }

        private void ExecuteSqlScriptFile(string filePath)
        {
            log.DebugFormat("ExecuteSqlScript called for SQL Script: {0}", filePath);

            try
            {
                string sqlScript = LoadStringFromFile(filePath);
                ExecuteSql(sqlScript);
            }
            catch (Exception e)
            {
                string message = string.Format("Exception occurred executing SQL Script: {0}", filePath);
                log.Error(message);

                throw new Exception(message, e);
            }
        }

        private static string LoadStringFromFile(string fileNameAndPath)
        {
            StringBuilder sb = new StringBuilder();

            FileInfo file = new FileInfo(fileNameAndPath);

            if (!File.Exists(file.FullName))
            {
                return null;
            }

            string fileContents = null;
            try
            {
                using (StreamReader rdr = File.OpenText(file.FullName))
                {
                    fileContents = rdr.ReadToEnd();
                }
            }
            catch (IOException e)
            {
                throw new IOException("Exception while loading file: " + fileNameAndPath, e);
            }

            return fileContents;
        }

        #endregion
    }
}
