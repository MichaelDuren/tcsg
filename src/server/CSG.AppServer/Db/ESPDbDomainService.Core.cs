using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.ServiceModel.DomainServices.EntityFramework;
using CSG.AppServer.Entities;
using CSG.AppServer.Managers;
using CSG.AppServer.Security;
using Util.Collections.Generic;
using System;

namespace CSG.AppServer.Db
{
    /// <summary>
    /// This is the Core Components of the DbDomainService.
    /// 
    /// It contains the reusable / common components.
    /// </summary>
    public partial class ESPDbDomainService : DbDomainService<ESPDbContext> //  CommonDbDomainService<ESPDbContext>
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private static Cache<string, IEnumerable> cacheForGetAll = new Cache<string, IEnumerable>(CacheType.AbsoluteExpiration);
        private static double GET_ALL_CACHE_MINUTES = AppServerContext.CacheMinutes;

        private Gatekeeper _gatekeeper;

        /// <summary>
        /// If true (default), then all Create, Update, Delete activity on entities is logged to the Activity Log
        /// </summary>
        public bool LogCRUDActivity { get; set; }

        // ********************************************************************************
        #region Constructors
        // ********************************************************************************

        public ESPDbDomainService(bool logCRUDActivity = true)
            : base()
        {
            LogCRUDActivity = logCRUDActivity;
        }

        // This version of the domain service is used for gatekeepers that already have a user profile
        // established.
        public ESPDbDomainService(Gatekeeper gatekeeper, bool logCRUDActivity = true) : this(logCRUDActivity) 
        {
            _gatekeeper = gatekeeper;
        }

        #endregion

        // ********************************************************************************
        #region DbDomainService Overrides
        // ********************************************************************************

        /// <summary>
        /// This can be useful for debugging SubmitOperation errors.  If the HasErrors property of the SubmitOperation is true
        /// but the EntitiesInError is emptpy, examining the ChangeSet.ChangeSetEntries can provide info on what is failing
        /// </summary>
        protected override bool ValidateChangeSet()
        {
            log.Info("Running ValidateChangeSet");
            bool returnValue = base.ValidateChangeSet();
            return returnValue;
        }

        protected override void OnError(System.ServiceModel.DomainServices.Server.DomainServiceErrorInfo errorInfo)
        {
            base.OnError(errorInfo);
        }

        #endregion

        // ********************************************************************************
        #region Accessors to Commonly needed Objects
        // ********************************************************************************

        public new ESPDbContext DbContext
        {
            get { return base.DbContext; }
        }

        protected Gatekeeper Gatekeeper
        {
            get
            {
                if (_gatekeeper == null)
                {
                    _gatekeeper = new Gatekeeper();
                }

                return _gatekeeper;
            }
        }

        protected UserProfile CurrentUserProfile
        {
            get
            {
                // NOTE:  For future consideration
                //We might want test for a null Http context and then either
                //1.  Create a UserProfile for the current principal which could include accounts like NetworkService, LocalService or LocalSystem
                //or
                //2.  Create a generic profile named something like 'Executive'
                //
                //In either case, we need to consider whether that profile needs to be in at least one group and what effect it might have on
                //the AD update process
                //
                //A third option would be to just return null.

                //Added this try/catch because the current user profile will not exist
                //when running from the executive
                try
                {
                    UserProfile userProfile = Gatekeeper.GetCurrentUserProfile(false);
                    return userProfile;
                }
                catch
                {
                    //Using the first service account found in the list (really the only one in the list
                    //should be the account running the executive)  If not found, null will be returned
//                    ESPDbContext context = new ESPDbContext();
//                    if (context.UserProfiles != null)
//                    return context.UserProfiles.Where(u => u.IsServiceAccount).FirstOrDefault();
                    return null;
                }
            }
        }

        #endregion

        // ********************************************************************************
        #region Entity Support Methods
        // ********************************************************************************

        private void UpdateEntityStats(IEntity entity, CudOperation cudOperation)
        {
            if (entity is ITrackedEntity)
            {
                ITrackedEntity trackedEntity = (ITrackedEntity)entity;
                if (cudOperation == CudOperation.Insert)
                {
                    trackedEntity.UpdateCreatedByStats(this.CurrentUserProfile);
                    trackedEntity.UpdateLastUpdatedByStats(this.CurrentUserProfile);
                }
                else
                {
                    trackedEntity.UpdateLastUpdatedByStats(this.CurrentUserProfile);
                }
            }
        }

        public int SaveChanges()
        {
            log.Debug("SaveChanges called");

            try
            {
                return base.DbContext.SaveChanges();
            }
            catch (DbUpdateConcurrencyException e)
            {
                if (CurrentUserProfile == null)
                {
                    ActivityLog.LogException(null, null, ActivityLog.LogEventId_DATABASE_EXCEPTION,
                                             "DbDomainService", "SaveChanges", 
                                             "DbUpdateConcurrencyException occurred saving changes.", exception: e);
                }
                else
                {
                    ActivityLog.LogException(CurrentUserProfile.DomainAndLoginId, CurrentUserProfile.FullName, 
                                             ActivityLog.LogEventId_DATABASE_EXCEPTION,
                                             "DbDomainService", "SaveChanges", 
                                             "DbUpdateConcurrencyException occurred saving changes.", exception: e);
                }

                throw;
            }
            catch (Exception ex)
            {
                Exception nextEx = ex;

                // inner exceptions often contain important details pertaining to database updates
                while (nextEx != null)
                {
                    ActivityLog.LogException(CurrentUserProfile.DomainAndLoginId, CurrentUserProfile.FullName,
                                             ActivityLog.LogEventId_DATABASE_EXCEPTION,
                                             "DbDomainService", "SaveChanges",
                                             "DbUpdateException occurred saving changes.", exception: ex);
                    nextEx = nextEx.InnerException;
                }

                IEnumerable results = this.DbContext.GetValidationErrors();

                if (results == null)
                {
                    // If nothing is returned - then just throw the original exception
                    throw;
                }

                foreach (DbEntityValidationResult result in results)
                {
                    foreach (DbValidationError error in result.ValidationErrors)
                    {
                        log.ErrorFormat("Entity: ({0}) with Id: {1} - Property: {2} Error Message: {3}", result.Entry.Entity.GetType().FullName, ((EntityBase)result.Entry.Entity).Id, error.PropertyName, error.ErrorMessage);
                    }
                }

                throw;
            }
        }

        #endregion

        // ********************************************************************************
        #region Core Cache Methods
        // ********************************************************************************

        private IEnumerable<TEntity> GetAllFromCache<TEntity>(string includes)
            where TEntity : class, IEntity
        {
            string cacheKey = string.Format("{0}/{1}", typeof(TEntity).FullName, includes);

            if (!cacheForGetAll.ContainsKey(cacheKey))
            {
                List<TEntity> results = this.DbContext.Set<TEntity>(includes).ToList();

                lock (this)
                {
                    if (!cacheForGetAll.ContainsKey(cacheKey))
                    {
                        cacheForGetAll.Add(cacheKey, results, GET_ALL_CACHE_MINUTES);
                    }
                }

                log.DebugFormat("Retrieved IEnumerable<{0}> from Db", typeof(TEntity).Name);
            }
            else
            {
                log.DebugFormat("Retrieved IEnumerable<{0}> from Cache", typeof(TEntity).Name);
            }
            return (IEnumerable<TEntity>)cacheForGetAll[cacheKey];
        }

        public void InvalidateCache<TEntity>()
            where TEntity : class, IEntity
        {
            lock (this)
            {
                var cacheKeys = cacheForGetAll.Where(kv => kv.Key.StartsWith(typeof(TEntity).FullName + "/")).ToList();

                foreach (KeyValuePair<string, IEnumerable> kv in cacheKeys)
                {
                    if (cacheForGetAll.ContainsKey(kv.Key))
                    {
                        cacheForGetAll.Remove(kv.Key);
                    }
                }
            }

            log.DebugFormat("Invalidate cache for IEnumerable<{0}> from Db", typeof(TEntity).Name);
        }

        #endregion

        //// ********************************************************************************
        #region Additional Methods
        //// ********************************************************************************
        #endregion
    }
}

