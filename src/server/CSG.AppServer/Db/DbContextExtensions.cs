//2012.08.25 - Complete

using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.ServiceModel.DomainServices.Server;
using Util;
using CSG.AppServer.Entities;

namespace CSG.AppServer.Db
{
    /// <summary>
    /// This class provides extension methods to the DbContext.
    /// 
    /// The original logic was taken from: http://social.msdn.microsoft.com/Forums/en-US/adonetefx/thread/57793bec-abc6-4520-ac1d-a63e40239aed/
    ///  with extra logic from: http://blogs.msdn.com/b/adonet/archive/2011/01/29/using-dbcontext-in-ef-feature-ctp5-part-4-add-attach-and-entity-states.aspx
    ///
    /// </summary>
    public static class DbContextExtensions
    {
        public static TEntity Insert<TEntity>(this DbContext context, TEntity entity) where TEntity : class
        {
            Guard.ArgumentNotNull(context, "context");
            Guard.ArgumentNotNull(entity, "entity");

            context.Entry(entity).State = System.Data.Entity.EntityState.Added;

            return entity;
        }

        public static void Update<TEntity>(this DbContext context, TEntity entity) where TEntity : class
        {
            Guard.ArgumentNotNull(context, "context");
            Guard.ArgumentNotNull(entity, "entity");

            context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public static void Update<TEntity>(this DbContext context, TEntity current, TEntity original) where TEntity : class
        {
            Guard.ArgumentNotNull(context, "context");
            Guard.ArgumentNotNull(current, "current");
            Guard.ArgumentNotNull(original, "original");

            var entry = context.Entry(current);
            entry.State = System.Data.Entity.EntityState.Unchanged;
            entry.OriginalValues.SetValues(original);

            var properties = TypeDescriptor.GetProperties(typeof(TEntity));
            var attributes = TypeDescriptor.GetAttributes(typeof(TEntity));

            foreach (var propertyName in entry.CurrentValues.PropertyNames)
            {
                var descriptor = properties[propertyName];
                if (descriptor != null &&
                    descriptor.Attributes[typeof(RoundtripOriginalAttribute)] == null &&
                    attributes[typeof(RoundtripOriginalAttribute)] == null &&
                    descriptor.Attributes[typeof(ExcludeAttribute)] == null)
                {
                    entry.Property(propertyName).IsModified = true;
                }
            }

            if (entry.State != System.Data.Entity.EntityState.Modified)
            {
                entry.State = System.Data.Entity.EntityState.Modified;
            }
        }

        /// <summary>
        /// Does either an Insert or Update - based on the value of the Id property.
        /// If Id = 0 then it will do an Insert, otherwise, it will do an update
        /// </summary>
        public static TEntity InsertOrUpdate<TEntity>(this DbContext context, TEntity entity) where TEntity : EntityBase
        {
            Guard.ArgumentNotNull(context, "context");
            Guard.ArgumentNotNull(entity, "entity");

            if (entity.Id == 0)
            {
                context.Entry(entity).State = System.Data.Entity.EntityState.Added;
            }
            else
            {
                context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
            }

            return entity;
        }

        public static void Delete<TEntity>(this DbContext context, TEntity entity) where TEntity : class
        {
            Guard.ArgumentNotNull(context, "context");
            Guard.ArgumentNotNull(entity, "entity");

            context.Entry(entity).State = System.Data.Entity.EntityState.Deleted;
        }


        public static void AttachUnmodified<TEntity>(this DbContext context, TEntity entity) where TEntity : class
        {
            Guard.ArgumentNotNull(context, "context");
            Guard.ArgumentNotNull(entity, "entity");

            context.Entry(entity).State = System.Data.Entity.EntityState.Unchanged;
        }

        public static void AttachModified<TEntity>(this DbContext context, TEntity entity) where TEntity : class
        {
            Guard.ArgumentNotNull(context, "context");
            Guard.ArgumentNotNull(entity, "entity");

            context.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        public static IQueryable<TEntity> Set<TEntity>(this DbContext context, string includes) where TEntity : class
        {
            Guard.ArgumentNotNull(context, "context");

            var queryable = context.Set<TEntity>().AsQueryable<TEntity>();
            if (!StringTools.IsBlankOrNull(includes))
            {
                if (includes.Contains(","))
                {
                    string[] tempIncludes = includes.Split(',');
                    foreach (string tempInclude in tempIncludes)
                    {
                        if (tempInclude.Trim() != String.Empty)
                        {
                            queryable = queryable.Include(tempInclude);
                        }
                    }
                }
                else
                {
                    queryable = queryable.Include(includes);
                }
            }

            return queryable;
        }

    }
}
