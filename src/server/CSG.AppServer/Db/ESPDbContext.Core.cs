using System;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Web;
using System.Data.Entity.Core.Objects;
using Util;
using Util.Config;

namespace CSG.AppServer.Db
{
    public partial class ESPDbContext : DbContext
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // *********************************************************************************
        #region Properties
        // *********************************************************************************

        private bool initialized;

        public ObjectContext ObjectContext
        {
            get
            {
                return (this as IObjectContextAdapter).ObjectContext;
            }
        }

        #endregion

        // *********************************************************************************
        #region DbSets
        // *********************************************************************************

        // Generally these are in the Generated DbContext

        #endregion

        // *********************************************************************************
        #region Constructors
        // *********************************************************************************

        public ESPDbContext() :
            this(DynamicConfigurationManager.GetConnectionStringName()) { }

        public ESPDbContext(DbConnection connection, bool contextOwnsConnection)
            : base(connection, contextOwnsConnection) { }

        public ESPDbContext(string nameOrConnectionString)
            : base(DynamicConfigurationManager.GetConnectionStringName(nameOrConnectionString))
        {
            // Set Database Initializer to use existing database
            SetDbInitializer();

            if (HttpContext.Current == null)
            {
                System.Data.Entity.Database.SetInitializer<ESPDbContext>(null);
            }
        }

        //public ESPDbContext() :
        //    this(DynamicConfigurationManager.GetConnectionStringName(), DUMMY_ARGUMENT: true) { }

        //public ESPDbContext(DbConnection connection, bool contextOwnsConnection)
        //    : base(connection, contextOwnsConnection) { }

        //public ESPDbContext(string nameOrConnectionString, bool DUMMY_ARGUMENT)
        //    : base(DynamicConfigurationManager.GetConnectionStringName(nameOrConnectionString))
        //{
        //    // Set Database Initializer to use existing database
        //    SetDbInitializer();

        //    if (HttpContext.Current == null)
        //    {
        //        System.Data.Entity.Database.SetInitializer<ESPDbContext>(null);
        //    }
        //}

        ///// <summary>
        ///// NOTE: This constructor is ONLY here to be able to support using this DB Context from LinqPad.
        ///// 
        ///// Note: It NEEDS to have this method signature to work correctly
        ///// </summary>
        //public ESPDbContext(string nameOrConnectionString)
        //    : base(nameOrConnectionString)
        //{
        //    if (HttpContext.Current == null)
        //    {
        //        Database.SetInitializer<ESPDbContext>(null);
        //    }

        //    this.initialized = true;
        //}

        #endregion

        // *********************************************************************************
        #region Methods
        // *********************************************************************************

        protected virtual void SetDbInitializer()
        {
            this.initialized = true;

            string efCodeFirstDbInitializeStrategy = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.EFCodeFirstDbInitializeStrategy];

            if (StringTools.IsBlankOrNull(efCodeFirstDbInitializeStrategy))
            {
                efCodeFirstDbInitializeStrategy = "UseExistingDatabase";
            }

            switch (efCodeFirstDbInitializeStrategy)
            {
                case "CreateDatabaseIfNotExists":
                    Database.SetInitializer(new CreateDatabaseIfNotExists<ESPDbContext>());
                    break;
                case "DropCreateDatabaseIfModelChanges":
                    Database.SetInitializer(new DropCreateDatabaseIfModelChanges<ESPDbContext>());
                    break;
                case "DropCreateDatabaseAlways":
                    Database.SetInitializer(new DropCreateDatabaseAlways<ESPDbContext>());
                    break;
                case "UseExistingDatabase":
                    Database.SetInitializer<ESPDbContext>(null);
                    break;

                default:
                    throw new InvalidOperationException("Invalid EFCodeFirstDbInitializeStrategy: " + efCodeFirstDbInitializeStrategy);
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            if (!initialized)
            {
                throw new Exception("ESPDbContext has not been initialized.  You must invoke SetDbInitializer<>() from your DbContext Constructor.");
            }

            // Removing cascade deletes
            modelBuilder.Conventions.Remove<System.Data.Entity.ModelConfiguration.Conventions.ManyToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<System.Data.Entity.ModelConfiguration.Conventions.OneToManyCascadeDeleteConvention>();

            log.Debug("OnModelCreating called for ESPDbContext");
        }

        public override int SaveChanges()
        {
            this.ChangeTracker.DetectChanges();
            return base.SaveChanges();
        }

        #endregion
    }

}
