﻿using System.Data;

namespace CSG.AppServer.Db
{
    public static class DbConnectionFactory
    {
        public static IDbConnection CreateConnection()
        {
            ESPDbDomainService domainService = new ESPDbDomainService();
            return domainService.DbContext.Database.Connection;
        }

        public static IDbConnection CreateOpenConnection()
        {
            IDbConnection connection = CreateConnection();
            connection.Open();
            return connection;
        }

    }
}
