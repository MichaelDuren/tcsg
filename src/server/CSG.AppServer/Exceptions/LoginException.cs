﻿using System;

namespace CSG.AppServer.Exceptions
{
    public enum LoginExceptionSubType
    {
        NotInValidActiveDirectoryGroup,
        UserHasNoRoles,
        UserNotInActiveDirectory,

        /// <summary>
        /// This can happen if they have timed out
        /// </summary>
        UserIsNotLoggedIn,
        UserOAuthTokenNotValid
    }

    public class LoginException : Exception
    {
        public LoginExceptionSubType LoginExceptionSubType { get; set; }

        public LoginException(string message, LoginExceptionSubType loginExceptionSubType)
            : base(message)
        {
            this.LoginExceptionSubType = loginExceptionSubType;
        }
    }
}
