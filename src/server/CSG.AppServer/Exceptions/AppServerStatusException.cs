﻿using System;

namespace CSG.AppServer.Exceptions
{
    public class AppServerStatusException : Exception
    {
        public string StatusMessage { get; set; }

        public AppServerStatusException(string message, string statusMessage)
            : base(message)
        {
            this.StatusMessage = statusMessage;
        }
    }
}
