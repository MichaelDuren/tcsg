﻿using System;

namespace CSG.AppServer.Exceptions
{
    public class UserRightsException : Exception
    {
        public UserRightsException(string message) : base(message) { }
    }
}
