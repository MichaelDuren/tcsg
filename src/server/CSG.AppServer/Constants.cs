
namespace CSG.AppServer
{
    public static class Constants
    {
        public static readonly string DomainAndLoginId = "DomainAndLoginId";
        public static readonly string UserProfile      = "UserProfile";
        public static readonly string LoginId          = "LoginId";

        public static class SettingNames
        {
            // ****************************************
            // Used by App Server Context
            // ****************************************

            public static readonly string ApplicationOnlineStatus = "ApplicationOnlineStatus";
            public static readonly string CacheMinutes = "CacheMinutes";
            public static readonly string UserTimeout = "UserTimeout";
            public static readonly string ServerName = "ServerName";

            // ****************************************
            // 
            // ****************************************

            public static readonly string EmailServer = "EmailServer";
            public static readonly string EmailServerPort = "EmailServerPort";
            public static readonly string EmailServerUserId = "EmailServerUserId";
            public static readonly string EmailServerPassword = "EmailServerPassword";
            public static readonly string EmailServerSSL = "EmailServerSSL";
            public static readonly string EmailSendNotifications = "EmailSendNotifications";
            public static readonly string SupportUrl = "SupportUrl";
            public static readonly string TeamEmailAddress = "TeamEmailAddress";
            public static readonly string AlertMessage = "AlertMessage";
            public static readonly string Copyright = "Copyright";
            public static readonly string OutageMessage = "OutageMessage";
            public static readonly string DaysOfRecentRequests = "DaysOfRecentRequests";
            public static readonly string SystemEventLogSourceName = "SystemEventLogSourceName";
            public static readonly string MaxMinutesToKeepTempFiles = "MaxMinutesToKeepTempFiles";
            public static readonly string PortalUrl = "PortalUrl";
            public static readonly string OAuthServerURL = "OAuthServerURL";
            public static readonly string OAuthUIDHeaderValue = "OAuthUIDHeaderValue";
            public static readonly string GESProfileRoleList = "GESProfileRoleList";
            public static readonly string ProjectList = "ProjectList";
            public static readonly string ReleaseList = "ReleaseList";


            // ****************************************
            // Request Processing
            // ****************************************

            public static readonly string IPAddressRestrictions = "IPAddressRestrictions";
            public static readonly string StorageLocation = "StorageLocation";
            public static readonly string TempStorageLocation = "TempStorageLocation";
            public static readonly string ReminderNotificationFrequencyInHours = "ReminderNotificationFrequencyInHours";
            public static readonly string RequestTimeoutInHours = "RequestTimeoutInHours";
            // Timeout for external signing process
            public static readonly string CodeSigningTimeoutMs = "CodeSigningTimeoutMs";


            // ****************************************
            // Used by Task Manager
            // ****************************************

            public static readonly string TaskManagerRunFrequency = "TaskManagerRunFrequency";
            public static readonly string Task_SyncWithActiveDirectory_Interval = "Task_SyncWithActiveDirectory_Interval";
            public static readonly string Task_ProcessSubmittedRequestsTask_Interval = "Task_ProcessSubmittedRequestsTask_Interval";
            public static readonly string Task_CheckCertificatesTask_Interval = "Task_CheckCertificatesTask_Interval";
            public static readonly string Task_ClearStorageTask_Interval = "Task_ClearStorageTask_Interval";
        }

        public static class SettingCategories
        {
            public static readonly string AppConfig = "AppConfig";
            public static readonly string ClientConfig = "ClientConfig";
        }

        public static class AppConfigNames
        {
            public static readonly string ApplicationName = "ApplicationName";
            public static readonly string ApplicationAbbreviation = "ApplicationAbbreviation";
            public static readonly string AppEnvironment = "AppEnvironment";
            public static readonly string AdminADGroups = "AdminADGroups";            
            public static readonly string BaseDatabaseConnectionStringName = "BaseDatabaseConnectionStringName";
            public static readonly string Testing_ForceRecreateDb = "Testing.ForceRecreateDb";
            public static readonly string Testing_AlternateIdentityMethod = "Testing.AlternateIdentityMethod";
            public static readonly string Administrators = "Administrators";
            public static readonly string EFCodeFirstDbInitializeStrategy = "EFCodeFirstDbInitializeStrategy";
            public static readonly string UseTestUserManager = "UseTestUserManager";
            public static readonly string UsePrimaryStorage = "UsePrimaryStorage";
            public static readonly string GESPortalResourceName = "GESPortalResourceName";
            public static readonly string GESActionStringAdministrator = "GESActionStringAdministrator";
            public static readonly string GESActionStringSignRequest = "GESActionStringSignRequest";
            public static readonly string GESActionStringSignHashRequest = "GESActionStringSignHashRequest";
            public static readonly string GESActionStringOfflineSigner = "GESActionStringOfflineSigner";            
            public static readonly string GESActionStringManagerApprover = "GESActionStringManagerApprover";
            public static readonly string GESActionStringApprover = "GESActionStringApprover";
            public static readonly string GESActionStringEnterpriseApprover = "GESActionStringEnterpriseApprover";
            public static readonly string DefaultDomain = "DefaultDomain";
            public static readonly string OAuthUIDFieldName = "OAuthUIDFieldName";
            public static readonly string OAuthVerifyClientCertificate = "OAuthVerifyClientCertificate";
            public static readonly string OAuthClientUseSystemStore = "OAuthClientUseSystemStore";
            public static readonly string OAuthAuthenticateToServer = "OAuthAuthenticateToServer";
            public static readonly string OAuthAuthenticateIgnoreNameMismatch = "OAuthAuthenticateIgnoreNameMismatch";
            public static readonly string OAuthAuthenticateIgnoreCertChainErrors = "OAuthAuthenticateIgnoreCertChainErrors";
            public static readonly string OAuthClientId = "OAuthClientId";
            public static readonly string DisablePortalUI = "DisablePortalUI";
            public static readonly string DisableESPDirectAPI = "DisableESPDirectAPI";
            public static readonly string SigningExeHashValues = "SigningExeHashValues";
            public static readonly string AllowedDomains = "AllowedDomains";
            public static readonly string SigntoolConfiguration = "SigntoolConfiguration";
            public static readonly string JarsignerConfiguration = "JarsignerConfiguration";
            public static readonly string CodesignConfiguration = "CodeSignConfiguration";
            public static readonly string CodesignSConfiguration = "CodeSignSConfiguration";  //share directory between macOS and Windows
            public static readonly string HABcstConfiguration = "HABcstConfiguration";
            public static readonly string EvmctlConfiguration = "EvmctlConfiguration";
            public static readonly string CertreqConfiguration = "CertreqConfiguration";       // local
            public static readonly string CertreqRConfiguration = "CertreqRConfiguration";    // running on remote machine
            public static readonly string SigntoolRConfiguration = "SigntoolRConfiguration";
            public static readonly string SignBinaryConfiguration = "SignBinaryConfiguration";
            public static readonly string SnConfiguration = "SnConfiguration";
            public static readonly string LinuxScriptConfiguration = "LinuxScriptConfiguration";
            public static readonly string SnRConfiguration = "SnRConfiguration";
            public static readonly string GenericConfiguration = "GenericConfiguration";
            public static readonly string ScanWinNumber = "ScanWinNumber";
            public static readonly string ScanLinuxNumber = "ScanLinuxNumber";
            public static readonly string ScanConfiguration = "ScanConfiguration";
            public static readonly string ScanLinuxConfiguration = "ScanLinuxConfiguration";
            public static readonly string CodeSigningWorkingFolder = "CodeSigningWorkingFolder";
            public static readonly string SSOHeaderUID = "SSOHeaderUID";
            public static readonly string SSOHeaderEMail = "SSOHeaderEMail";
            public static readonly string SSOHeaderFirstName = "SSOHeaderFirstName";
            public static readonly string SSOHeaderLastName = "SSOHeaderLastName";
            public static readonly string ADDomainOrServer = "ADDomainOrServer";
            public static readonly string ADContainer = "ADContainer";
            public static readonly string PrimaryStorageLocation = "PrimaryStorageLocation";
            public static readonly string SecondaryStorageLocation = "SecondaryStorageLocation";
            public static readonly string LocalUploadFolder = "LocalUploadFolder";
            public static readonly string WaitForUnlockTimer = "WaitForUnlockTimer";
            public static readonly string LogToEventLogDb = "LogToEventLogDb";
            public static readonly string WebCertManagement = "WebCertManagement";

        }

        public static class Roles
        {
            public static readonly string AppAdminRole = "AppAdmin";
            public static readonly string DeveloperRole = "Developer";
            public static readonly string ApproverRole = "Approver";
        }
    }
}
