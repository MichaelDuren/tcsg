using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CSG.AppServer.Entities
{
    [Table("App_SystemSettings")]
    public class SystemSetting : EntityBase
    {
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(100)]
        public string Category { get; set; }

        [MaxLength(100)]
        public string SubCategory { get; set; }

        [Required]
        [MaxLength(2000)]
        public string Description { get; set; }

        [NotMapped]
        public SystemSettingDataType DataType { get; set; }
        public int DataTypeId
        {
            get { return (int)DataType; }
            set { DataType = (SystemSettingDataType)value; }
        }
        public bool? BooleanValue { get; set; } = false;

        [NotMapped]
        public bool NotNullableBooleanValue { get { return BooleanValue == true; } set { BooleanValue = value; } }

        public DateTime? DateValue { get; set; }

        public int? IntegerValue { get; set; }

        public double? DoubleValue { get; set; }

        [MaxLength(80000)] //nvarchar(max)
        public string StringValue { get; set; }

        [MaxLength(1000)]
        public string PasswordValue { get; set; }

        public bool ExcludeFromSettingsUI { get; set; }

        [MaxLength(100)]
        public string ExtraString1 { get; set; }

        [MaxLength(100)]
        public string ExtraString2 { get; set; }

        [MaxLength(100)]
        public string ExtraString3 { get; set; }

        public int? ExtraInteger1 { get; set; }
        public int? ExtraInteger2 { get; set; }
        public int? ExtraInteger3 { get; set; }

        [NotMapped]
        public string Value {
            get
            {
                string val = string.Empty;
                switch (DataTypeId)
                {
                    case (int)SystemSettingDataType.String:
                        val = StringValue;
                        break;
                    case (int)SystemSettingDataType.Integer:
                        val = IntegerValue.ToString();
                        break;
                    case (int)SystemSettingDataType.Double:
                        val = DoubleValue.ToString();
                        break;
                    case (int)SystemSettingDataType.DateTime:
                        val = DateValue.ToString();
                        break;
                    case (int)SystemSettingDataType.Boolean:
                        val = BooleanValue.ToString();
                        break;
                    case (int)SystemSettingDataType.Password:
                        val = PasswordValue.ToString();
                        break;
                    case (int)SystemSettingDataType.Undefined:
                    default:
                        val = "unknown datatype";
                        break;
                }
                return val;
            }
        }
    }
}
