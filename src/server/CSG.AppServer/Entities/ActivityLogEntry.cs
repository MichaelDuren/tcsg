using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CSG.AppServer.Entities
{
    [Table("App_ActivityLog")]
    public class ActivityLogEntry : EntityBase
    {
        [MaxLength(50)]
        public string UserLogin { get; set; }

        [MaxLength(100)]
        public string UserName { get; set; }

        /// <summary>
        /// Valid values: Info, Warn, Error
        /// </summary>
        [MaxLength(5)]
        public string LogEntryLevel { get; set; }

        public DateTime? LogTime { get; set; }

        [MaxLength(100)]
        public string ActivityType { get; set; }

        [MaxLength(300)]
        public string ActivitySubType { get; set; }

        [MaxLength(2000)]
        public string Text1 { get; set; }

        [MaxLength(2000)]
        public string Text2 { get; set; }

        [MaxLength(2000)]
        public string Text3 { get; set; }

        [MaxLength(80000)] // Note: this is translated into nvarchar(max)
        public string Details { get; set; }

        public int? ElapsedMilliseconds { get; set; }

        [MaxLength(30)]
        public string ApplicationName { get; set; }

        [MaxLength(50)]
        public string Server { get; set; }

        [MaxLength(50)]
        public string ActiveDirectorySId { get; set; }

        public ActivityLogEntry()
        {
            LogTime = DateTime.Now;
        }
    }
}
