using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using CSG.AppServer.Db;
using System.ComponentModel.DataAnnotations.Schema;

namespace CSG.AppServer.Entities
{
    /// <summary>
    /// This class represents an Active Directory group (also referred to as Development Teams within the spec)
    /// </summary>
    [Table("Lookup_Groups")]
    public class Group : LookupEntityBase
    {
        [Required]
        [MaxLength(125)]
        public override string Name { get; set; }

        [MaxLength(50)]
        public string Domain { get; set; }

        /// <summary>
        /// http://msdn.microsoft.com/en-us/library/windows/desktop/ms679768(v=vs.85).aspx
        /// </summary>
        [MaxLength(50)]
        public string ActiveDirectorySId { get; set; }

        public bool IsAppAdminGroup { get; set; }
        public bool IsDeveloperGroup { get; set; }
        public bool IsApproverGroup { get; set; }

        /// <summary>
        /// Returns the list of CodeSigningTypes the group has rights to access
        /// </summary>
        public static IEnumerable<CodeSigningType> GetCodeSigningTypes(int groupId)
        {
            ESPDbDomainService domainService = new ESPDbDomainService();

            return domainService.GetCodeSigningTypeGroups("CodeSigningType")
                                                .Where(cstg => cstg.GroupId == groupId)
                                                .Select(cstg => cstg.CodeSigningType)
                                                .Distinct()
                                                .OrderBy(cstg => cstg.Order)
                                                .ThenBy(cstg => cstg.Name)
                                                .ToList();
        }
    }
}
