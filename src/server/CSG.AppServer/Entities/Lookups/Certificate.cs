using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Security.Cryptography.X509Certificates;

namespace CSG.AppServer.Entities
{
    [Table("Lookup_Certificates")]
    public class Certificate : LookupEntityBase
    {
        [MaxLength(256)]
        public override string Name { get; set; }

        [MaxLength(1000)]
        public string IssuedTo { get; set; }

        [MaxLength(1000)]
        public string IssuedBy { get; set; }

        [MaxLength(1024)]
        public string DistinguishedName { get; set; }

        [MaxLength(256)]
        public string Thumbprint { get; set; }

        [Required]
        public bool InStore { get; set; }

        [Required]
        public bool InSecurityWorld { get; set; }

        [Required]
        public string MimeEncoded { get; set; }

        [Required]
        public DateTime? NotBefore { get; set; }

        [Required]
        public DateTime? NotAfter { get; set; }

        [NotMapped]
        public bool InValidityPeriod {
            get
            {
                if (NotBefore == null || NotAfter == null)
                {
                    X509Certificate2 decodedCert = new X509Certificate2(Convert.FromBase64String(MimeEncoded));

                    NotBefore = decodedCert.NotBefore;
                    NotAfter = decodedCert.NotAfter;
                }

                if (DateTime.Now > NotAfter || DateTime.Now < NotBefore)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

    }
}
