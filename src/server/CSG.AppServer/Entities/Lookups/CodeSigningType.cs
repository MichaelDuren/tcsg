using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CSG.AppServer.Entities
{
    [Table("Lookup_CodeSigningTypes")]
    public class CodeSigningType : LookupEntityBase
    {
        [Required]
        [Display(Name = "Signing Profile")]
        [MaxLength(125)]
        public override string Name { get; set; }

        [MaxLength(2000)]
        public string StagingDirectory { get; set; }

        [MaxLength(2000)]
        public string SignedDirectory { get; set; }

        [MaxLength(2000)]
        public string AdditionalArguments { get; set; }

        [Required]
        public bool DoVerification { get; set; }

        [Required]
        public bool RequireSecondaryApproval { get; set; }

        [MaxLength(1000)]
        public string ValidFileExtensions { get; set; }

//        public string SigningCertificate { get; set; }

        [MaxLength(100)]
        public string MetadataField1Label { get; set; }

        [MaxLength(100)]
        public string MetadataField2Label { get; set; }

        public bool RequestTimestamp { get; set; }

        public bool ApplyToAll { get; set; }        

        [Required]
        [MaxLength(100)]
        public string Guid { get; set; }

        public bool RawSign { get; set; }

        [Association("CodeSigningTypeSigningTool_To_SigningTool", "SigningToolId", "Id", IsForeignKey = true)]
        [Display(Name = "Code Signing Tool")]
        public virtual CodeSigningTool SigningTool { get; set; }

        public int SigningToolId { get; set; }

        [Association("CodeSigningTypeCertificate_To_Certificate", "SigningCertificateId", "Id", IsForeignKey = true)]
        [Display(Name = "Code Signing Certificate")]
        public virtual Certificate SigningCertificate { get; set; }

        [Required]
        public int SigningCertificateId { get; set; }
        [Required]
        public bool RequireManagerApproval { get; set; }
        [Required]
        public bool AllowManagerApproval { get; set; }
        [Required]
        public bool RequireApprover1Approval { get; set; }
        [Required]
        public bool AllowApprover1Approval { get; set; }
        [Required]
        public bool RequireApprover2Approval { get; set; }
        [Required]
        public bool AllowApprover2Approval { get; set; }
        [Required]
        public bool IsAutoSign { get; set; }
    }
}
