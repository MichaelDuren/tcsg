using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CSG.AppServer.Entities
{
    [Table("Lookup_CodeSigningTools")]
    public class CodeSigningTool : LookupEntityBase
    {
        [Required]
        [MaxLength(125)]
        public override string Name { get; set; }

        public bool Offline { get; set; }

        [MaxLength(2000)]
        public string SigningExecutable { get; set; }

        [MaxLength(2000)]
        public string CommonSigningArguments { get; set; }

        public int SigningArgumentFormat { get; set; }

        [MaxLength(2000)]
        public string VerificationExecutable { get; set; }

        [MaxLength(2000)]
        public string CommonVerificationArguments { get; set; }

        [MaxLength(1000)]
        public string TimestampServer { get; set; }


    }
}
