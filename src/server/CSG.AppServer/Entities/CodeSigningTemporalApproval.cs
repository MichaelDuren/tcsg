using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace CSG.AppServer.Entities
{
    [Table("App_CodeSigningTemporalApprovals")]
    public class CodeSigningTemporalApproval : EntityBase
    {
        [Association("CodeSigningTemporalApproval_To_CodeSigningType", "CodeSigningTypeId", "Id", IsForeignKey = true)]
        [Display(Name = "Code Signing Type")]
        public virtual CodeSigningType CodeSigningType { get; set; }
        public int CodeSigningTypeId { get; set; }

        [Association("CodeSigningTemporalApproval_To_ApprovingUserProfile", "ApprovingUserProfileId", "Id", IsForeignKey = true)]
        public virtual UserProfile ApprovingUserProfile { get; set; }
        public int ApprovingUserProfileId { get; set; }

        [Association("CodeSigningTemporalApproval_To_UserProfile", "UserProfileId", "Id", IsForeignKey = true)]
        public virtual UserProfile UserProfile { get; set; }
        public int UserProfileId { get; set; }

        [NotMapped]
        public AppServer.UserActions ApprovingAction { get; set; }

        public int ApprovingUserAction
        {
            get { return (int)ApprovingAction; }
            set { ApprovingAction = (AppServer.UserActions)value; }
        }

        public DateTime StartDate { get; set; }

        public int Hours { get; set; }

        public string FileName { get; set; }

        public bool EnforceSameFileName { get; set; }

        [NotMapped]
        public int windowInDays 
        {
            get { return Hours/24;}
        }

        [NotMapped]
        public DateTime expiration
        {
            get { return StartDate.AddHours(Hours); }
        }

    }
}
