using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace CSG.AppServer.Entities
{
    [Table("App_CodeSigningRequests")]
    public class CodeSigningRequest : TrackedEntityBase
    {
        [Association("CodeSigningRequestSigningType_To_CodeSigningType", "CodeSigningTypeId", "Id", IsForeignKey = true)]
        [Display(Name = "Code Signing Type")]
        public virtual CodeSigningType CodeSigningType { get; set; }

        [Display(Name = "Request Id")]
        public override int Id { get; set; }

        [Required(ErrorMessage = "The development team is required.")]
        public int CodeSigningTypeId { get; set; }

        [Association("CodeSigningRequestGroup_To_Group", "GroupId", "Id", IsForeignKey = true)]
        public virtual Group Group { get; set; }

        [Required(ErrorMessage = "The code signing type is required.")]
        public int GroupId { get; set; }

        [Association("CodeSigningRequestManagerResponder_To_UserProfile", "ManagerResponderId", "Id", IsForeignKey = true)]
        public virtual UserProfile ManagerResponder { get; set; }
        public int? ManagerResponderId { get; set; }

        [Display(Name = "Manager Response Date")]
        public DateTime? ManagerResponseDate { get; set; }

        [Association("CodeSigningRequestApprover1Responder_To_UserProfile", "Approver1ResponderId", "Id", IsForeignKey = true)]
        public virtual UserProfile Approver1Responder { get; set; }
        public int? Approver1ResponderId { get; set; }

        [Display(Name = "Approver1 Response Date")]
        public DateTime? Approver1ResponseDate { get; set; }

        [Association("CodeSigningRequestApprover2Responder_To_UserProfile", "Approver2ResponderId", "Id", IsForeignKey = true)]
        public virtual UserProfile Approver2Responder { get; set; }
        public int? Approver2ResponderId { get; set; }

        [Display(Name = "Approver2 Response Date")]
        public DateTime? Approver2ResponseDate { get; set; }

        public DateTime? LastNotificationSentDate { get; set; }

        [Display(Name = "Completion Date")]
        public DateTime? CompletionDate { get; set; }

        [NotMapped]
        public CodeSigningRequestStatus Status { get; set; }
        public int StatusId
        {
            get { return (int)Status; }
            set { Status = (CodeSigningRequestStatus)value; }
        }

        //File name only, no path
        [Required(ErrorMessage = "The file to be signed is required.")]
        [MaxLength(4000)]
        [Display(Name = "Unsigned File Name")]
        public string UnsignedFileName { get; set; }

        /// <summary>
        /// An MD5 hash of the unsigned file
        /// </summary>
        [MaxLength(4000)]
        [Display(Name = "Unsigned File Hash")]
        public string UnsignedFileHash { get; set; }

        //File name only, no path
        [MaxLength(4000)]
        [Display(Name = "Signed File Name")]
        public string SignedFileName { get; set; }
        /// <summary>
        /// An MD5 hash of the signed file
        /// </summary>
        [MaxLength(4000)]
        [Display(Name = "Signed File Hash")]
        public string SignedFileHash { get; set; }

        /// <summary>
        /// number of files submitted in this request
        /// </summary>
        public int? FileCount { get; set; }

        /// <summary>
        /// An MD5 hash of the signed file
        /// </summary>
        [MaxLength(int.MaxValue)]
        [Display(Name = "Signing Result")]
        public string SigningResult { get; set; }

        //File name only, no path
        [MaxLength(100)]
        [Display(Name = "Certificate File Name")]
        public string CertFileName { get; set; }

        //File name only, no path
        [MaxLength(100)]
        [Display(Name = "Results File Name")]
        public string ResultsFileName { get; set; }

        [MaxLength(1000)]
        [Display(Name = "Responder Note")]
        public string ResponderNote { get; set; }

        //Property added as a result of feedback during a call.  Not in the spec.
        [MaxLength(100)]
        [Display(Name = "Project Name")]
        public string ProjectName { get; set; }

        [MaxLength(100)]
        [Display(Name = "Release Name")]
        public string ReleaseName { get; set; }

        public int? EscalationCount { get; set; }

        public DateTime? Archived { get; set; }

        public int? ScanningCommandResultCode { get; set; }

        public int? SigningCommandResultCode { get; set; }

        public static string GetRequestDirectory(int csRequestId, bool primary = true)
        {
            string storageLocaction;
            if (primary)
            {
                storageLocaction = AppServerContext.PrimaryStorageLocation;
            }
            else
            {
                storageLocaction = AppServerContext.SecondaryStorageLocation;
            }
            return Path.Combine(storageLocaction, string.Format("Request-{0}", csRequestId));
        }

        public static string GetUnsignedDirectory(int csRequestId, bool primary = true)
        {
            return Path.Combine(GetRequestDirectory(csRequestId, primary), "Unsigned");
        }

        public static string GetSignedDirectory(int csRequestId, bool primary = true)
        {
            return Path.Combine(GetRequestDirectory(csRequestId, primary), "Signed");
        }
    }
}
