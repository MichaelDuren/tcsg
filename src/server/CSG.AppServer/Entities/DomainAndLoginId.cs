﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Util;

namespace CSG.AppServer.Entities
{
    public class DomainAndLoginId : IEquatable<DomainAndLoginId>   //, IComparer<DomainAndLoginId>
    {
        public string Domain { get; set; }
        public string LoginId { get; set; }
        public string ActiveDirectorySId { get; set; }
        public DomainAndLoginId()
        {

        }


        public DomainAndLoginId(string domain, string loginId, string activeDirectorySId = null)
        {
            Guard.ArgumentNotNull(loginId, "loginId");
            Guard.ArgumentNotNull(domain, "domain");

            this.Domain = domain;
            this.LoginId = loginId;
            this.ActiveDirectorySId = activeDirectorySId;
        }

        public override string ToString()
        {
            return string.Format("{0}\\{1}", Domain, LoginId);
        }

        // Needed for ContainsKey in Dictionary based on DomainAndLoginId as the key (both GetHashCode AND Equals)
        public override int GetHashCode()
        {
            return ToString().ToUpper().GetHashCode();
        }

        #region IEquatable<DomainAndLoginId> Members

        public bool Equals(DomainAndLoginId other)
        {
            if (other == null) { return false; }
            if (other.ToString().ToUpper() == this.ToString().ToUpper()) { return true; }
            else { return false; }
        }

        #endregion
    }

    // Custom comparer for the Product class 
    public class DomainAndLoginIdComparer : IEqualityComparer<DomainAndLoginId>
    {
        // Products are equal if their names and product numbers are equal. 
        public bool Equals(DomainAndLoginId x, DomainAndLoginId y)
        {
            // Check whether the compared objects reference the same data. 
            if (Object.ReferenceEquals(x, y)) { return true; }

            // Check whether any of the compared objects is null. 
            if (Object.ReferenceEquals(x, null) || Object.ReferenceEquals(y, null)) { return false; }

            // Check whether the products' properties are equal. 
            return x.Domain.ToUpper() == y.Domain.ToUpper()
                && x.LoginId.ToUpper() == y.LoginId.ToUpper()
                && x.ActiveDirectorySId == y.ActiveDirectorySId;
        }

        // If Equals() returns true for a pair of objects  // then GetHashCode() must return the same value for these objects. 
        public int GetHashCode(DomainAndLoginId domainAndLoginId)
        {
            // Check whether the object is null 
            if (Object.ReferenceEquals(domainAndLoginId, null)) return 0;

            // Get hash code for the Domain field. 
            int hashDomain = domainAndLoginId.Domain.ToUpper().GetHashCode();

            // Get hash code for the LoginId field. 
            int hashProductLoginId = domainAndLoginId.LoginId.ToUpper().GetHashCode();

            // Get hash code for the ActiveDirectorySId field. 
            int hashProductSId = domainAndLoginId.ActiveDirectorySId.ToUpper().GetHashCode();

            // Calculate the hash code for the product. 
            return hashDomain ^ hashProductLoginId ^ hashProductSId;
        }
    }
}
