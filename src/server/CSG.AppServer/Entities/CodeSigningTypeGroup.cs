using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CSG.AppServer.Entities
{
    [Table("App_CodeSigningTypeGroups")]
    public class CodeSigningTypeGroup : EntityBase
    {
        [Association("CodeSigningTypeGroup_To_CodeSigningType", "CodeSigningTypeId", "Id", IsForeignKey = true)]
        public virtual CodeSigningType CodeSigningType { get; set; }
        public int CodeSigningTypeId { get; set; }

        [Association("CodeSigningTypeGroup_To_Group", "GroupId", "Id", IsForeignKey = true)]
        public virtual Group Group { get; set; }
        public int GroupId { get; set; }

        [Required]
        public bool IsApprover { get; set; }
    }
}
