using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ServiceModel.DomainServices.Server;
using System.Text;

namespace CSG.AppServer.Entities
{
    [Table("App_UserProfiles")]
    public class UserProfile : VersionedEntityBase
    {
        /// <summary>
        /// http://msdn.microsoft.com/en-us/library/windows/desktop/ms679768(v=vs.85).aspx
        /// </summary>
        [MaxLength(50)]
        public string ActiveDirectorySId { get; set; }

        [MaxLength(50)]
        public string Domain { get; set; }

        [MaxLength(50)]
        public string LoginId { get; set; }

        [Exclude]
        public DomainAndLoginId DomainAndLoginId
        {
            get { return new DomainAndLoginId(this.Domain, this.LoginId, this.ActiveDirectorySId); }
        }

        [MaxLength(30)]
        public string FirstName { get; set; }

        [MaxLength(20)]
        public string MiddleName { get; set; }

        [MaxLength(50)]
        public string LastName { get; set; }

        [MaxLength(100)]
        public string DisplayName { get; set; }

        [MaxLength(100)]
        public string Email { get; set; }

        [MaxLength(100)]
        public string ProxyEmail { get; set; }

        public DateTime? CreatedDate { get; set; }
        public DateTime? LastAccessedDate { get; set; }

        public int LoginCount { get; set; }

        public bool IsDeleted { get; set; }

        public bool IsServiceAccount { get; set; }


        private List<UserProfileCodeSigningType> _codeSigningTypes;
        /// <summary>
        ///
        /// </summary>
        [Association("UserProfileCodeSigningType_To_UserProfile", "Id", "UserProfileId")]
        public List<UserProfileCodeSigningType> CodeSigningTypes
        {
            get { if (_codeSigningTypes == null) { _codeSigningTypes = new List<UserProfileCodeSigningType>(); } return _codeSigningTypes; }
            set { _codeSigningTypes = value; }
        }

        private List<UserProfileGroup> _groups;
        /// <summary>
        /// Active Directory groups the user belongs to
        /// </summary>
        [Association("UserProfileGroup_To_UserProfile", "Id", "UserProfileId")]
        public List<UserProfileGroup> Groups
        {
            get { if (_groups == null) { _groups = new List<UserProfileGroup>(); } return _groups; }
            set { _groups = value; }
        }

        private List<UserProfileRole> _roles;
        /// <summary>
        /// Roles within the system the user has
        /// </summary>
        [Association("UserProfileRole_To_UserProfile", "Id", "UserProfileId")]
        public List<UserProfileRole> Roles
        {
            get { if (_roles == null) { _roles = new List<UserProfileRole>(); } return _roles; }
            set { _roles = value; }
        }

        public virtual string FullName
        {
            get
            {
                if (!String.IsNullOrEmpty(LastName) || !String.IsNullOrEmpty(FirstName))
                {
                    StringBuilder sb = new StringBuilder();

                    if (!String.IsNullOrEmpty(LastName))
                    {
                        sb.Append(LastName);
                    }

                    if (!String.IsNullOrEmpty(FirstName))
                    {
                        if (sb.Length > 0)
                        {
                            sb.Append(", ");
                        }
                        sb.Append(FirstName);
                    }

                    return sb.ToString();
                }
                else if (!String.IsNullOrEmpty(DisplayName))
                {
                    return DisplayName;
                }
                else
                {
                    return DomainAndLoginId.ToString();
                }
            }
        }

        public bool IsAppAdmin
        {
            get
            {
                 var mapping = Roles.Find(m => m.Role.Name == Constants.Roles.AppAdminRole);
                 if (mapping != null) { return true; } else { return false; }
            }
        }

        public UserProfile()
        {
            CreatedDate = DateTime.Now;
            LastAccessedDate = DateTime.Now;
        }

        public override string ToString()
        {
            string response = null;

            response =      "UserProfile: \r\n";
            response +=     " LoginId:     " + LoginId;

            return response;
        }
    }
}
