
using System.ComponentModel.DataAnnotations;

namespace CSG.AppServer.Entities
{
    public interface ILookupEntity : ITrackedEntity
    {
        string Name { get; set; }
        string Description { get; set; }
        string Abbreviation { get; set; }
        int? Order { get; set; }
        bool IsDeleted { get; set; }
    }
    public abstract class LookupEntityBase : TrackedEntityBase, ILookupEntity
    {
        [MaxLength(125)]
        public virtual string Name { get; set; }

        [MaxLength(1000)]
        public virtual string Description { get; set; }

        [MaxLength(15)]
        public virtual string Abbreviation { get; set; }

        public virtual int? Order { get; set; }
        public virtual bool IsDeleted { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
