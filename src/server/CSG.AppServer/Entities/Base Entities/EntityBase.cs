using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Util;

namespace CSG.AppServer.Entities
{
    public interface IEntity
    {
        int Id { get; set; }
    }

    public abstract class EntityBase : IEntity
    {
        [Key]
        [Display(Order = 0, Name = "Id:")]
        [Bindable(true, BindingDirection.OneWay)]
        public virtual int Id { get; set; }

        public static bool AreIdsEqual(EntityBase x, EntityBase y)
        {
            return EntityBaseComparer.Current.Equals(x, y);
        }
    }

    public class EntityBaseComparer : IEqualityComparer<IEntity>
    {
        private EntityBaseComparer() { }

        private static EntityBaseComparer _current;

        public static EntityBaseComparer Current
        {
            get
            {
                if (_current == null)
                {
                    _current = new EntityBaseComparer();
                }
                return _current;
            }
        }

        public bool Equals(IEntity x, IEntity y)
        {
            if ((x == null) || (y == null))
            {
                return false;
            }

            //New entities default id to 0 and cannot be compared.
            if (x.Id == 0 || y.Id == 0)
            {
                return false;
            }

            return x.Id == y.Id;
        }

        public int GetHashCode(IEntity x)
        {
            Guard.ArgumentNotNull(x, "x");

            return x.Id;
        }
    }
}

