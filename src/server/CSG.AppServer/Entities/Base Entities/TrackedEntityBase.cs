using System;

namespace CSG.AppServer.Entities
{
    public interface ITrackedEntity : IVersionedEntity
    {
        int? CreatedByUserId { get; set; }
        DateTime? CreatedDate { get; set; }

        int? LastUpdatedByUserId { get; set; }
        DateTime? LastUpdatedDate { get; set; }

        void UpdateCreatedByStats(UserProfile user);
        void UpdateLastUpdatedByStats(UserProfile user);
    }

    public abstract class TrackedEntityBase : VersionedEntityBase, ITrackedEntity
    {
        //Intentionally not creating the CodeFirst association here
        public UserProfile CreatedByUser { get; set; }
        public int? CreatedByUserId { get; set; }
        public virtual DateTime? CreatedDate { get; set; }

        //Intentionally not creating the CodeFirst association here
        public UserProfile LastUpdatedByUser { get; set; }
        public int? LastUpdatedByUserId { get; set; }
        public virtual DateTime? LastUpdatedDate { get; set; }

        public void UpdateCreatedByStats(UserProfile user)
        {
            if (user != null)
            {
                CreatedByUserId = user.Id;
            }
            CreatedDate = DateTime.Now;
        }

        public void UpdateLastUpdatedByStats(UserProfile user)
        {
            if (user != null)
            {
                LastUpdatedByUserId = user.Id;
            }
            LastUpdatedDate = DateTime.Now;
        }

    }
}
