using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace CSG.AppServer.Entities
{
    public interface IVersionedEntity : IEntity
    {
        byte[] RowVersion { get; set; }
    }

    public abstract class VersionedEntityBase : EntityBase, IVersionedEntity
    {
        [Timestamp]
        [Bindable(false)]
        [Display(Order = 300, AutoGenerateField = false)]
        public virtual byte[] RowVersion { get; set; }
    }
}
