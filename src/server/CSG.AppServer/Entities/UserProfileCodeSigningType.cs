using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CSG.AppServer.Entities
{
    [Table("App_UserProfileCodeSigningTypes")]
    public class UserProfileCodeSigningType : EntityBase
    {
        [Association("UserProfileCodeSigningType_To_UserProfile", "UserProfileId", "Id", IsForeignKey = true)]
        public virtual UserProfile UserProfile { get; set; }
        public int UserProfileId { get; set; }

        [Association("UserProfileCodeSigningType_To_CodeSigningType", "CodeSigningTypeId", "Id", IsForeignKey = true)]
        public virtual CodeSigningType CodeSigningType { get; set; }
        public int CodeSigningTypeId { get; set; }

        [NotMapped]
        public AppServer.UserActions AuthorizedUserAction { get; set; }

        public int UserSigningTypeRole 
        {
            get { return (int)AuthorizedUserAction; }
            set { AuthorizedUserAction = (AppServer.UserActions)value; }
        }
    }
}
