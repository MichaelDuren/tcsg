using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CSG.AppServer.Entities
{
    [Table("App_UserProfileRoles")]
    public class UserProfileRole : EntityBase
    {
        [Association("UserProfileRole_To_UserProfile", "UserProfileId", "Id", IsForeignKey = true)]
        public virtual UserProfile UserProfile { get; set; }
        public int UserProfileId { get; set; }

        [Association("UserProfileRole_To_Role", "RoleId", "Id", IsForeignKey = true)]
        public virtual Role Role { get; set; }
        public int RoleId { get; set; }
    }
}
