using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CSG.AppServer.Entities
{
    [Table("App_CodeSigningTypeTestFiles")]
    public class CodeSigningTypeTestFile : TrackedEntityBase
    {
        [Required]
        public byte[] File { get; set; }

        [Required]
        public string UnsignedHash { get; set; }

        [Required]
        public string SignedHash { get; set; }

        //public void Temp()
        //{
        //    System.IO.Stream fileStream = OpenFileDialog.File.OpenRead();

        //    byte[] data = new byte[fileStream.Length];

        //    long bytesRead = fileStream.Read(data, 0, (int)fileStream.Length);
        //    fileStream.Close();

        //}


    }
}
