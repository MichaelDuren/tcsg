using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace CSG.AppServer.Entities
{
    [Table("App_UserProfileGroups")]
    public class UserProfileGroup : EntityBase
    {
        [Association("UserProfileGroup_To_UserProfile", "UserProfileId", "Id", IsForeignKey = true)]
        public virtual UserProfile UserProfile { get; set; }
        public int UserProfileId { get; set; }

        [Association("UserProfileGroup_To_Group", "GroupId", "Id", IsForeignKey = true)]
        public virtual Group Group { get; set; }
        public int GroupId { get; set; }

        public bool IsDesignatedApprover { get; set; }

    }
}
