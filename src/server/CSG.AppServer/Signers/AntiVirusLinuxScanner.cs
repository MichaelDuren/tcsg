﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using CSG.AppServer.Entities;
using CSG.AppServer.Managers;
using Microsoft.Practices.Unity;
using Util.Config;
using Util;
using System.Xml.Serialization;
using System.Xml;

namespace CSG.AppServer.Signers
{

    /// <summary>
    /// Scans files.
    /// </summary>
    internal class AntivirusLinuxScanner
    {
        private static readonly ESPLogWrapper.ILog Log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly Type ThisDeclaringType = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;

        ScanConfig scanConfig = null;

        public AntivirusLinuxScanner()
        {
            SigningTimeoutMs = (int)SystemSettingsManager.Current.GetBySettingName(Constants.SettingNames.CodeSigningTimeoutMs).IntegerValue;
        }

        public int SigningTimeoutMs { get; set; }

        [Dependency]
        public IProcessRunner ProcRunner { get; set; }

        public ScanConfig LoadSigningToolConfig(string configFile)
        {
             scanConfig = null;

            // get the signing tool config from the app.config file
            FileStream fs = new FileStream(configFile, FileMode.Open);
            XmlReader reader = XmlReader.Create(fs);
            using (fs)
            using (reader)
            {
                try
                {

                    // Create an instance of the XmlSerializer specifying type and namespace.
                    XmlSerializer serializer = new
                        XmlSerializer(typeof(ScanConfig), new XmlRootAttribute() { ElementName = "ScanConfig" });

                    scanConfig = (ScanConfig)serializer.Deserialize(reader);

                }
                finally
                {
                    fs.Close();
                }
            }
            return scanConfig;
        }

        /// <summary>
        /// Builds the copying command line string
        /// </summary>
        /// <param name="fileToScan">The file to scan.</param>
        /// <param name="scanConfig">Scan Config file.</param>
        /// <returns></returns>
        public string BuildCopyCommandLine(string fileToScan, string remoteDir)
        {
            if (scanConfig == null) return null;

            string commandLine = "";

            commandLine += scanConfig.copyArg+" \""+fileToScan+"\""+ " \""+scanConfig.userHost+":"+remoteDir+"\"";
 
            return commandLine;
        }

        /// <summary>
        /// Builds the scanning command line string
        /// </summary>
        /// <param name="fileToScan">The file to scan.</param>
        /// <param name="scanConfig">Scan Config file.</param>
        /// <returns></returns>
        public string BuildScanCommandLine(string fileToScan, string remoteDir)
        {
            if (scanConfig == null) return null;

            string commandLine = "";

            commandLine = scanConfig.userHost+" " + scanConfig.scanArg + " " + remoteDir +"/"+ Path.GetFileName(fileToScan);

            return commandLine;
        }

        public string BuildScanDirCommandLine(string remoteDir)
        {
            if (scanConfig == null) return null;

            string commandLine = "";

            commandLine = scanConfig.userHost + " " + scanConfig.scanArg + " " + remoteDir + "/";

            return commandLine;
        }

        /// <summary>
        /// Signs the specified file using signingType.
        /// </summary>
        /// <param name="fileToScan">The file to sign.</param>
        /// <returns></returns>
        public ProcessRunResult Scan(string fileToScan, string fileDir, string fileHash)
        {
            
            Log.Info($"scanning file:{fileToScan}");
            string commandLine;
            ProcessRunResult retVal = null;
            var rcode = 1;
            
            var remoteDir = scanConfig.copyDir+"/"+fileDir;
            
            commandLine = BuildCopyCommandLine( fileToScan, remoteDir);

            Guard.ValueNotBlankOrNull(commandLine, "Copying tool is not properly configured. A command line stringcould not built.");

            // recreate a working directory
            var result = ProcRunner.Run("\"" + scanConfig.sshExe + "\"", "\""+scanConfig.userHost+"\" \"rm -rf "+remoteDir +" && mkdir "+ remoteDir+"\"" , SigningTimeoutMs);
            Log.Info($"recreate:{remoteDir} output:{result.Output} is:{result.ResultCode}");

            // copy file
            result = ProcRunner.Run("\""+scanConfig.scpExe+"\"", commandLine, SigningTimeoutMs);
            retVal = new ProcessRunResult(result.ResultCode,
                $"scp {commandLine} Command Output:{result.Output}{Environment.NewLine}scp Exit Code: {result.ResultCode}",
                result.Errors);
            Log.Info($"Result of copying file:{fileToScan} is:{result.ResultCode}");

            if (result.ResultCode == 0)
            {
                // create sha256 hash file 
                result = ProcRunner.Run("\"" + scanConfig.sshExe + "\"", "\"" + scanConfig.userHost + "\" \"cd " + remoteDir + " && sha256sum " + Path.GetFileName(fileToScan) + " > sha256.txt && cat ./sha256.txt \"", SigningTimeoutMs);
                retVal = new ProcessRunResult(result.ResultCode,
                $"{retVal.Output}{Environment.NewLine}sha256sum create hash file command output:{result.Output}{Environment.NewLine}sha256sum Exit Code: {result.ResultCode}",
                result.Errors);

                // scanning command
                commandLine = BuildScanCommandLine(fileToScan, remoteDir);
                Guard.ValueNotBlankOrNull(commandLine, "Scanning tool is not properly configured. A command line string could not be built .");
                // scan
                result = ProcRunner.Run("\"" + scanConfig.sshExe + "\"", commandLine, SigningTimeoutMs);
                retVal = new ProcessRunResult(result.ResultCode,
                $"{retVal.Output}{Environment.NewLine}(ssh) {commandLine} command output:{result.Output}{Environment.NewLine}Scanning Exit Code: {result.ResultCode}",
                result.Errors);
                Log.Info($"Result of scanning file:{fileToScan} is:{result.ResultCode}");

                //  check file hash
                result = ProcRunner.Run("\"" + scanConfig.sshExe + "\"", "\"" + scanConfig.userHost + "\" \"cd " + remoteDir + " && sha256sum -c sha256.txt\"", SigningTimeoutMs);
                if (result.Output.Contains("OK"))
                    rcode = 0;

                retVal = new ProcessRunResult(rcode,
                $"{retVal.Output}{Environment.NewLine}sha256sum -c sha256.txt    hash check output:{result.Output}{Environment.NewLine}hash check Exit Code: {rcode}",
                result.Errors);

                if (result.ResultCode != 0 || rcode == 1)
                {
                    Log.Error($"Error messages: {result.Errors} or expected pattern not matching, Output: {result.Output}");
                    return retVal;
                }

                commandLine = scanConfig.userHost+" "+scanConfig.delArg + " " + remoteDir;

                var resultDel = ProcRunner.Run(scanConfig.sshExe, commandLine, SigningTimeoutMs);

                retVal = new ProcessRunResult(resultDel.ResultCode,
                    $"{retVal.Output}{Environment.NewLine}(ssh) {commandLine} command output:{resultDel.Output}{Environment.NewLine}delete Exit Code: {resultDel.ResultCode}",
                    resultDel.Errors);

                if (resultDel.ResultCode != 0 )
                {
                    Log.Error($"Error messages: {result.Errors} or expected pattern not matching, Output: {result.Output}");
                }

            }
            else
            {
                Log.Error($"Error messages: {result.Errors}, Output: {result.Output}");
            }
            
            return retVal;
        }

        public ProcessRunResult Scan(List<string> files, int requestid, string[] fileHashes)
        {
            string dirToScan = Path.GetDirectoryName(files[0]);
            Log.Info($"scanning dir:{dirToScan}");
            string commandLine;
            ProcessRunResult retVal = null;
            var rcode = 1;

            var remoteDir = scanConfig.copyDir + "/" + requestid;

            commandLine = BuildCopyCommandLine(dirToScan, remoteDir);

            Guard.ValueNotBlankOrNull(commandLine, "Copying tool is not properly configured. A command line stringcould not built.");

            // recreate a working directory
            var result = ProcRunner.Run("\"" + scanConfig.sshExe + "\"", "\"" + scanConfig.userHost + "\" \"rm -rf " + remoteDir + " && mkdir " + remoteDir + "\"", SigningTimeoutMs);
            Log.Info($"recreate:{remoteDir} output:{result.Output} is:{result.ResultCode}");

            // copy file
            result = ProcRunner.Run("\"" + scanConfig.scpExe + "\"", commandLine, SigningTimeoutMs);
            retVal = new ProcessRunResult(result.ResultCode,
                $"scp {commandLine} Command Output:{result.Output}{Environment.NewLine}scp Exit Code: {result.ResultCode}",
                result.Errors);
            Log.Info($"Result of copying dir:{dirToScan} is:{result.ResultCode}");

            if (result.ResultCode == 0)
            {
                // create sha256 hash file 
                foreach(var file in files)
                {
                    string filename = Path.GetFileName(file);
                    result = ProcRunner.Run("\"" + scanConfig.sshExe + "\"", "\"" + scanConfig.userHost + "\" \"cd " + remoteDir + " && sha256sum " + filename + " > sha256_"+filename+".txt && cat ./sha256_"+filename+".txt \"", SigningTimeoutMs);
                    /* too much output 
                    retVal = new ProcessRunResult(result.ResultCode,
                    $"{retVal.Output}{Environment.NewLine}sha256sum create hash file command output:{result.Output}{Environment.NewLine}sha256sum Exit Code: {result.ResultCode}",
                    result.Errors);
                    */
                }
                // scanning command
                commandLine = BuildScanDirCommandLine(remoteDir);
                Guard.ValueNotBlankOrNull(commandLine, "Scanning tool is not properly configured. A command line string could not be built .");
                // scan
                result = ProcRunner.Run("\"" + scanConfig.sshExe + "\"", commandLine, SigningTimeoutMs);
                retVal = new ProcessRunResult(result.ResultCode,
                $"{retVal.Output}{Environment.NewLine}(ssh) {commandLine} command output:{result.Output}{Environment.NewLine}Scanning Exit Code: {result.ResultCode}",
                result.Errors);
                Log.Info($"Result of scanning dir:{dirToScan} is:{result.ResultCode}");

                //  check file hash
                foreach (var file in files)
                {
                    string filename = Path.GetFileName(file);
                    result = ProcRunner.Run("\"" + scanConfig.sshExe + "\"", "\"" + scanConfig.userHost + "\" \"cd " + remoteDir + " && sha256sum -c sha256_"+filename+".txt\"", SigningTimeoutMs);
                    if (result.Output.Contains("OK"))
                        rcode = 0;
                    /* 
                    retVal = new ProcessRunResult(rcode,
                    $"{retVal.Output}{Environment.NewLine}sha256sum -c sha256.txt    hash check output:{result.Output}{Environment.NewLine}hash check Exit Code: {rcode}",
                    result.Errors);
                    */
                }

                if (result.ResultCode != 0 || rcode == 1)
                {
                    Log.Error($"Error messages: {result.Errors} or expected pattern not matching, Output: {result.Output}");
                    return retVal;
                }

                commandLine = scanConfig.userHost + " " + scanConfig.delArg + " " + remoteDir;

                var resultDel = ProcRunner.Run(scanConfig.sshExe, commandLine, SigningTimeoutMs);

                retVal = new ProcessRunResult(resultDel.ResultCode,
                    $"{retVal.Output}{Environment.NewLine}(ssh) {commandLine} command output:{resultDel.Output}{Environment.NewLine}delete Exit Code: {resultDel.ResultCode}",
                    resultDel.Errors);

                if (resultDel.ResultCode != 0)
                {
                    Log.Error($"Error messages: {result.Errors} or expected pattern not matching, Output: {result.Output}");
                }

            }
            else
            {
                Log.Error($"Error messages: {result.Errors}, Output: {result.Output}");
            }

            return retVal;
        }


    }


}
