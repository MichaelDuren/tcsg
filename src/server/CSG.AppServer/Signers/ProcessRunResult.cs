﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CSG.AppServer.Signers
{
    internal class ProcessRunResult
    {
        public int ResultCode { get; }

        public string Output { get; }
        public string Errors { get; }

        public ProcessRunResult(int resultCode, string output, string errors)
        {
            ResultCode = resultCode;
            Output = output;
            Errors = errors;
        }
    }
}
