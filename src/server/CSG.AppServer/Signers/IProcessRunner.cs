﻿namespace CSG.AppServer.Signers
{
    internal interface IProcessRunner
    {
        /// <summary>
        /// Runs the specified application.
        /// </summary>
        /// <param name="fileName">Application filename.</param>
        /// <param name="arguments">The arguments.</param>
        /// <param name="timeoutMs">The timeout ms.</param>
        ProcessRunResult Run(string fileName, string arguments, int timeoutMs);
    }
}