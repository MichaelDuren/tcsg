﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using CSG.AppServer.Managers;

namespace CSG.AppServer.Signers
{
    /// <summary>
    /// Class to run process.
    /// </summary>
    internal class ProcessRunner : IProcessRunner
    {
        private static readonly ESPLogWrapper.ILog Log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Runs the specified application.
        /// </summary>
        /// <param name="fileName">Application filename.</param>
        /// <param name="arguments">The arguments.</param>
        /// <param name="timeoutMs">The timeout ms.</param>
        public ProcessRunResult Run(string fileName, string arguments, int timeoutMs)
        {
            var info = GetProcessStartInfo(fileName, arguments);

            var errorsCollector = new StringBuilder();
            var outCollector = new StringBuilder();
            var exitCode = int.MaxValue;

            try
            {
                using (var process = new Process())
                {
                    process.StartInfo = info;

                    process.StartInfo.UseShellExecute = false;
                    process.StartInfo.RedirectStandardOutput = true;
                    process.StartInfo.RedirectStandardError = true;

                    process.ErrorDataReceived += (sender, args) => errorsCollector.AppendLine(args.Data);
                    process.OutputDataReceived += (sender, args) => outCollector.AppendLine(args.Data);


                    process.Start();

                    process.BeginErrorReadLine();
                    process.BeginOutputReadLine();

                    process.WaitForExit(timeoutMs);

                    if (!process.HasExited)
                    {
                        process.Kill();
                    }
                    else
                    {
                        exitCode = process.ExitCode;
                    }                   
                }
            }
            catch (Exception ex)
            {
                Log.Error($@"Failed to execute process:{fileName} with arguments:{arguments}", ex);
                errorsCollector.AppendLine(ex.Message);
            }

            return new ProcessRunResult(exitCode, outCollector.ToString(), errorsCollector.ToString());
        }

        private static ProcessStartInfo GetProcessStartInfo(string fileName, string arguments)
        {
            var info = new ProcessStartInfo(fileName, arguments)
            {
                UseShellExecute = false,
                RedirectStandardError = true,
                RedirectStandardInput = true,
                RedirectStandardOutput = true,
                CreateNoWindow = true,
                ErrorDialog = false,
                WindowStyle = ProcessWindowStyle.Hidden
            };

            return info;
        }
    }
}
