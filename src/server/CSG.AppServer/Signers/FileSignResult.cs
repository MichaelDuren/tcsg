﻿namespace CSG.AppServer.Signers
{
    internal class FileSignResult
    {
        public bool IsSuccess { get; }

        public string Output { get; }
        public string Errors { get; }

        public FileSignResult(bool isSuccess, string output, string errors)
        {
            IsSuccess = isSuccess;
            Output = output;
            Errors = errors;
        }
    }
}
