﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using CSG.AppServer.Entities;
using CSG.AppServer.Managers;
using Microsoft.Practices.Unity;
using Util.Config;
using Util;
using System.Xml.Serialization;
using System.Xml;

namespace CSG.AppServer.Signers
{

    /// <summary>
    /// Signs files.
    /// </summary>
    internal class CodeSigner
    {
        private readonly object bLock = new object();
        private readonly object xmlLock = new object();
        private static readonly ESPLogWrapper.ILog Log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly Type ThisDeclaringType = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;

        private List<string> _hashValuesFromConfig;

        public CodeSigner()
        {
            SigningTimeoutMs = (int)SystemSettingsManager.Current.GetBySettingName(Constants.SettingNames.CodeSigningTimeoutMs).IntegerValue;
            _hashValuesFromConfig = GetExeHashValuesFromConfig();
        }

        public int SigningTimeoutMs { get; set; }

        [Dependency]
        public IProcessRunner ProcRunner { get; set; }

        public SignerConfig LoadSigningToolConfig(string configFile)
        {
            SignerConfig signerConfig = null;

            // get the signing tool config from the app.config file
            FileStream fs = new FileStream(configFile, FileMode.Open);
            XmlReader reader = XmlReader.Create(fs);
            using (fs)
            using (reader)
            {
                try
                {

                    // Create an instance of the XmlSerializer specifying type and namespace.
                    XmlSerializer serializer = new
                        XmlSerializer(typeof(SignerConfig), new XmlRootAttribute() { ElementName = "SigningToolConfig" });

                    signerConfig = (SignerConfig)serializer.Deserialize(reader);

                }
                finally
                {
                    fs.Close();
                }
            }
            return signerConfig;
        }

        /// <summary>
        /// Builds the signing tool command line string
        /// </summary>
        /// <param name="signingType">Signing type record.</param>
        /// <param name="fileToSign">The file to sign.</param>
        /// <returns></returns>
        public string BuildSigntoolCommandLine(CodeSigningType signingType, string fileToSign, SignerConfig signerConfig)
        {
            if (signingType.SigningTool.SigningArgumentFormat != 0) return null;

            string commandLine = "";
            lock (bLock)
            {

                // start with the certificate
                commandLine += "sign /sha1 " + signingType.SigningCertificate.Thumbprint;

                // is Timestamping selected?
                if (signingType.RequestTimestamp &&
                    signingType.SigningTool.TimestampServer != null &&
                    signingType.SigningTool.TimestampServer.Length > 0)
                {
                    commandLine += " /tr \"" + signingType.SigningTool.TimestampServer.Trim() + "\"";
                }

                // add additional argments
                if (signingType.SigningTool.CommonSigningArguments != null &&
                    signingType.SigningTool.CommonSigningArguments.Length > 0)
                {
                    commandLine += " " + signingType.SigningTool.CommonSigningArguments;
                }
                if (signingType.AdditionalArguments != null &&
                    signingType.AdditionalArguments.Length > 0)
                {
                    commandLine += " " + signingType.AdditionalArguments;
                }

                // add the file to sign in quotes.
                commandLine += " \"" + fileToSign+"\"";

            }
            return commandLine;
        }

        public string BuildJarsignerCommandLine(CodeSigningType signingType, string fileToSign, SignerConfig signerConfig)
        {
            if (signingType.SigningTool.SigningArgumentFormat != 1) return null;

            string commandLine = "";
            lock (bLock)
            {
                var certsKeyStores = signerConfig.jarsignerConfig.certsAndKeystores;
                var certAndKeyStore = certsKeyStores.Find(f => f.certThumbprint.ToUpper() == signingType.SigningCertificate.Thumbprint.ToUpper());
                Guard.ValueNotNull(certAndKeyStore, "Error locating certificate entry in jarsigner configuration file.");

                // start with the certificate
                commandLine += "-keystore " + certAndKeyStore.keystoreFile + " " +
                                "-storepass " + certAndKeyStore.keystorePass + " " +
                                "-keypass " + certAndKeyStore.keyPass;

                // is Timestamping selected?
                if (signingType.RequestTimestamp &&
                    signingType.SigningTool.TimestampServer != null &&
                    signingType.SigningTool.TimestampServer.Length > 0)
                {
                    commandLine += " -tsa \"" + signingType.SigningTool.TimestampServer.Trim() + "\"";
                }

                // add additional argments
                if (signingType.SigningTool.CommonSigningArguments != null &&
                    signingType.SigningTool.CommonSigningArguments.Length > 0)
                {
                    commandLine += " " + signingType.SigningTool.CommonSigningArguments;
                }
                if (signingType.AdditionalArguments != null &&
                    signingType.AdditionalArguments.Length > 0)
                {
                    commandLine += " " + signingType.AdditionalArguments;
                }
                // add the file to sign.
                commandLine += " \"" + fileToSign + "\"";
                // add the key alias
                commandLine += " " + certAndKeyStore.alias;
            }
            return commandLine;
        }

        public string BuildCodesignCommandLine(CodeSigningType signingType, string fileToSign, SignerConfig signerConfig)
        {
            if (signingType.SigningTool.SigningArgumentFormat != 2) return null;

            string commandLine = "";
            lock (bLock)
            {
                var certsKeyStores = signerConfig.otherSignerConfig.certsAndKeystores;
                var certAndKeyStore = certsKeyStores.Find(f => f.certThumbprint.ToUpper() == signingType.SigningCertificate.Thumbprint.ToUpper());
                Guard.ValueNotNull(certAndKeyStore, "Error locating certificate entry in Codesign configuration file.");

                // start with the certificate
                commandLine += signerConfig.codesignCMD + " -f -s " + signingType.SigningCertificate.Thumbprint;

                // is Timestamping selected?
                if (signingType.RequestTimestamp &&
                    signingType.SigningTool.TimestampServer != null &&
                    signingType.SigningTool.TimestampServer.Length > 0)
                {
                    commandLine += " --timestamp= \"" + signingType.SigningTool.TimestampServer.Trim() + "\"";
                }

                // add additional argments
                if (signingType.SigningTool.CommonSigningArguments != null &&
                    signingType.SigningTool.CommonSigningArguments.Length > 0)
                {
                    commandLine += " " + signingType.SigningTool.CommonSigningArguments;
                }
                if (signingType.AdditionalArguments != null &&
                    signingType.AdditionalArguments.Length > 0)
                {
                    commandLine += " " + signingType.AdditionalArguments;
                }

                string fparent = Directory.GetParent(fileToSign).Name;
                string fname = Path.GetFileName(fileToSign);
                string fnoext = fname.Replace(".tar", "");
                commandLine = signerConfig.userHost + " \"cd " + signerConfig.codesignDir + " && "
                        + signerConfig.tarCMD + " -xvf " + fname + " && "
                        + commandLine + " " + fnoext + " && "
                        + signerConfig.tarCMD + " -cvf " + fname + " " + fnoext + " \"";

                // scp from Windows to MacOS. Since signExe is starting cmd, use scp instead.
                commandLine = fileToSign + " " + signerConfig.userHost + ":"
                    + signerConfig.codesignDir + "," + commandLine;

                // scp from MacOS to Windows
                commandLine += "," + signerConfig.userHost + ":"
                    + signerConfig.codesignDir + "/" + fname + " " + fileToSign;


            }

            return commandLine;
        }

        public string BuildCodesignSCommandLine(CodeSigningType signingType, string fileToSign, SignerConfig signerConfig)
        {
            if (signingType.SigningTool.SigningArgumentFormat != 3) return null;

            string commandLine = "";
            lock (bLock)
            {
                var certsKeyStores = signerConfig.otherSignerConfig.certsAndKeystores;
                var certAndKeyStore = certsKeyStores.Find(f => f.certThumbprint.ToUpper() == signingType.SigningCertificate.Thumbprint.ToUpper());
                Guard.ValueNotNull(certAndKeyStore, "Error locating certificate entry in CodesignS configuration file.");

                // start with the certificate
                commandLine += signerConfig.codesignCMD + " -f -s " + signingType.SigningCertificate.Thumbprint;

                // is Timestamping selected?
                if (signingType.RequestTimestamp &&
                    signingType.SigningTool.TimestampServer != null &&
                    signingType.SigningTool.TimestampServer.Length > 0)
                {
                    commandLine += " --timestamp= \"" + signingType.SigningTool.TimestampServer.Trim() + "\"";
                }

                // add additional argments
                if (signingType.SigningTool.CommonSigningArguments != null &&
                    signingType.SigningTool.CommonSigningArguments.Length > 0)
                {
                    commandLine += " " + signingType.SigningTool.CommonSigningArguments;
                }
                if (signingType.AdditionalArguments != null &&
                    signingType.AdditionalArguments.Length > 0)
                {
                    commandLine += " " + signingType.AdditionalArguments;
                }

                string fparent = Directory.GetParent(fileToSign).Name;
                string fname = Path.GetFileName(fileToSign);
                string fnoext = fname.Replace(".tar", "");

                // user shared drive
                // cd to dir && untar the file && commandline && tar the result
                // The assumption is the the working folder in csexecutive.exe.config, such as C:\npsCSG\CSTest, is shared 
                commandLine = signerConfig.userHost + " \"cd " + signerConfig.codesignDir + "/unsigned/" + fparent + " && "
                    + signerConfig.tarCMD + " -xvf " + fname + " && "
                    + commandLine + " " + fnoext + " &&  "
                    + signerConfig.tarCMD + " -cvf " + fname + " " + fnoext + " \"";


                // add the file to sign in quotes.
                commandLine += " " + signerConfig.codesignDir + "/" + fnoext;

            }

            return commandLine;
        }

        public string BuildHABcstCommandLine(CodeSigningType signingType, string fileToSign, SignerConfig signerConfig)
        {
            if (signingType.SigningTool.SigningArgumentFormat != 4) return null;
            string commandLine = "";
            lock (bLock)
            {
                var certsKeyStores = signerConfig.otherSignerConfig.certsAndKeystores;
                var certAndKeyStore = certsKeyStores.Find(f => f.certThumbprint.ToUpper() == signingType.SigningCertificate.Thumbprint.ToUpper());
                Guard.ValueNotNull(certAndKeyStore, "Error locating certificate entry in HABcst configuration file.");

                // start with the paramFile
                if (signerConfig.paramFile != null)
                    commandLine += signerConfig.codesignCMD + " $(cat " + signerConfig.paramFile + ")";


                // add additional argments
                if (signingType.SigningTool.CommonSigningArguments != null &&
                    signingType.SigningTool.CommonSigningArguments.Length > 0)
                {
                    commandLine += " " + signingType.SigningTool.CommonSigningArguments;
                }
                if (signingType.AdditionalArguments != null &&
                    signingType.AdditionalArguments.Length > 0)
                {
                    commandLine += " " + signingType.AdditionalArguments;
                }

                string fparent = Directory.GetParent(fileToSign).Name;
                string fname = Path.GetFileName(fileToSign);
                string fnoext = fname.Replace(".tar", "");

                // cd dir && tar -xvf fname && cd fnoext && codesign $(cat paramFile) && cd ../ && tar -cvf
                commandLine = signerConfig.userHost + " \"cd " + signerConfig.codesignDir + " && "
                        + signerConfig.tarCMD + " -xvf " + fname + " &&  cd " + fnoext + " && "
                        + commandLine + " && " + " cd ../  && "
                        + signerConfig.tarCMD + " -cvf " + fname + " " + fnoext + " \"";

                // scp from Windows to Linux. Since signExe is starting cmd, use scp instead.
                commandLine = fileToSign + " " + signerConfig.userHost + ":"
                    + signerConfig.codesignDir + "," + commandLine;

                // scp from Linux to Windows
                commandLine += "," + signerConfig.userHost + ":"
                    + signerConfig.codesignDir + "/" + fname + " " + fileToSign;
            }

            return commandLine;
        }

        public string BuildEvmctlCommandLine(CodeSigningType signingType, string fileToSign, SignerConfig signerConfig)
        {
            if (signingType.SigningTool.SigningArgumentFormat == 5) return null;
            string commandLine = "";
            lock (bLock)
            {
                var certsKeyStores = signerConfig.otherSignerConfig.certsAndKeystores;
                var certAndKeyStore = certsKeyStores.Find(f => f.certThumbprint.ToUpper() == signingType.SigningCertificate.Thumbprint.ToUpper());
                Guard.ValueNotNull(certAndKeyStore, "Error locating certificate entry in Evmctl configuration file.");

                // maybe add the precommand argurement for server access such as ip, userID/login

                // start with evmctl
                commandLine += signerConfig.codesignCMD;
                // add additional argments
                if (signingType.SigningTool.CommonSigningArguments != null &&
                    signingType.SigningTool.CommonSigningArguments.Length > 0)
                {
                    commandLine += " " + signingType.SigningTool.CommonSigningArguments;
                }
                if (signingType.AdditionalArguments != null &&
                    signingType.AdditionalArguments.Length > 0)
                {
                    commandLine += " " + signingType.AdditionalArguments;
                }

                // add the file to sign in quotes.
                commandLine += " \"" + fileToSign + "\"";

                string fparent = Directory.GetParent(fileToSign).Name;
                string fname = Path.GetFileName(fileToSign);
                string fnoext = fname.Replace(".tar", "");


                // cd dir  && evmctl arg fname  
                commandLine = signerConfig.userHost + " \"cd " + signerConfig.codesignDir + " && "
                        + commandLine + " " + fname + " && " + " cd ../  && "
                        + " \""; ;

                // scp from Windows to Linux
                commandLine = fileToSign + " " + signerConfig.userHost + ":"
                    + signerConfig.codesignDir + "," + commandLine;

                // scp from Linux to Windows
                commandLine += "," + signerConfig.userHost + ":"
                    + signerConfig.codesignDir + "/" + fname + " " + fileToSign;
                
            }

            return commandLine;
        }

        public string BuildLinuxCommandLine(CodeSigningType signingType, string fileToSign, SignerConfig signerConfig)
        {
            if (signingType.SigningTool.SigningArgumentFormat == 11) return null;
            string commandLine = "";
            lock (bLock)
            {
                var certsKeyStores = signerConfig.otherSignerConfig.certsAndKeystores;
                var certAndKeyStore = certsKeyStores.Find(f => f.certThumbprint.ToUpper() == signingType.SigningCertificate.Thumbprint.ToUpper());
                Guard.ValueNotNull(certAndKeyStore, "Error locating certificate entry in Linux configuration file.");

                // start with the paramFile
                if (signerConfig.paramFile != null)
                    commandLine += "./" + signerConfig.codesignCMD + " $(cat " + signerConfig.paramFile + ")";
                else
                    commandLine += signerConfig.codesignCMD;


                // add additional argments
                if (signingType.SigningTool.CommonSigningArguments != null &&
                    signingType.SigningTool.CommonSigningArguments.Length > 0)
                {
                    commandLine += " " + signingType.SigningTool.CommonSigningArguments;
                }
                if (signingType.AdditionalArguments != null &&
                    signingType.AdditionalArguments.Length > 0)
                {
                    commandLine += " " + signingType.AdditionalArguments;
                }

                string fparent = Directory.GetParent(fileToSign).Name;
                string fname = Path.GetFileName(fileToSign);
                string fnoext = fname.Replace(".tar", "");

                // The difference between 4 & 11 is  that codesignCMD in 4 is absoluate path and
                // codesignCMD is relative path and using the script in tar file
                // cd dir && tar -xvf fname && cd fnoext && codesign $(cat paramFile) && cd ../ && tar -cvf
                commandLine = signerConfig.userHost + " \"cd " + signerConfig.codesignDir + " && "
                        + signerConfig.tarCMD + " -xvf " + fname + " &&  cd " + fnoext + " && "
                        + commandLine + " && " + " cd ../  && "
                        + signerConfig.tarCMD + " -cvf " + fname + " " + fnoext + " \"";

                // scp from Windows to Linux
                commandLine = fileToSign + " " + signerConfig.userHost + ":"
                    + signerConfig.codesignDir + "," + commandLine;

                // scp from Linux to Windows
                commandLine += "," + signerConfig.userHost + ":"
                    + signerConfig.codesignDir + "/" + fname + " " + fileToSign;
            }

            return commandLine;
        }

        public string BuildCertreqCommandLine(CodeSigningType signingType, string fileToSign, SignerConfig signerConfig)
        {
            if (signingType.SigningTool.SigningArgumentFormat != 6) return null;
            string commandLine = "";
            lock (bLock)
            {
                var certsKeyStores = signerConfig.otherSignerConfig.certsAndKeystores;
                var certAndKeyStore = certsKeyStores.Find(f => f.certThumbprint.ToUpper() == signingType.SigningCertificate.Thumbprint.ToUpper());
                Guard.ValueNotNull(certAndKeyStore, "Error locating certificate entry in Certreq configuration file.");

                string fname = Path.GetFileName(fileToSign);           
                    // add additional argments
                    if (signingType.SigningTool.CommonSigningArguments != null &&
                        signingType.SigningTool.CommonSigningArguments.Length > 0)
                    {
                        commandLine += " " + signingType.SigningTool.CommonSigningArguments;
                    }
                    if (signingType.AdditionalArguments != null &&
                        signingType.AdditionalArguments.Length > 0)
                    {
                        commandLine += " " + signingType.AdditionalArguments;
                    }

                    // add the file to sign in quotes.
                    commandLine +=  " \""+fileToSign +"\"  \""+fileToSign.Replace(Path.GetExtension(fname), ".cer")+"\"";
            }

            return commandLine;
        }

        public string BuildCertreqRCommandLine(CodeSigningType signingType, string fileToSign, SignerConfig signerConfig)
        {
            if (signingType.SigningTool.SigningArgumentFormat != 7) return null;
            string commandLine = "";
            lock (bLock)
            {
                var certsKeyStores = signerConfig.otherSignerConfig.certsAndKeystores;
                var certAndKeyStore = certsKeyStores.Find(f => f.certThumbprint.ToUpper() == signingType.SigningCertificate.Thumbprint.ToUpper());
                Guard.ValueNotNull(certAndKeyStore, "Error locating certificate entry in CertreqR configuration file.");


                string fparent = Directory.GetParent(fileToSign).Name;
                string fname = Path.GetFileName(fileToSign);
                string fnoext = fname.Replace(Path.GetExtension(fname), "");

                // add additional argments
                if (signingType.SigningTool.CommonSigningArguments != null &&
                    signingType.SigningTool.CommonSigningArguments.Length > 0)
                {
                    commandLine += " " + signingType.SigningTool.CommonSigningArguments;
                }
                if (signingType.AdditionalArguments != null &&
                    signingType.AdditionalArguments.Length > 0)
                {
                    commandLine += " " + signingType.AdditionalArguments;
                }

                // assume that the working dir is the shard dir C:\npsCSG\CSTest\" 
                // sharedDir\Request-NN\file 
                commandLine += signerConfig.sharedDir +"\\unsigned\\"+fparent+"\\"+fname+" "+signerConfig.sharedDir+"\\unsigned\\"+fparent +"\\"+fnoext+".cer";
                commandLine = signerConfig.winrsArg + " " + signerConfig.certreqExe + " " + commandLine;
            }

            return commandLine;
        }

        public string BuildSigntoolRCommandLine(CodeSigningType signingType, string fileToSign, SignerConfig signerConfig)
        {
            if (signingType.SigningTool.SigningArgumentFormat != 8) return null;

            string commandLine = "";
            lock (bLock)
            {
                string fparent = Directory.GetParent(fileToSign).Name;
                string fname = Path.GetFileName(fileToSign);
                string fnoext = fname.Replace(Path.GetExtension(fname), "");
                // start with the certificate
                commandLine += "sign /sha1 " + signingType.SigningCertificate.Thumbprint;

                // is Timestamping selected?
                if (signingType.RequestTimestamp &&
                    signingType.SigningTool.TimestampServer != null &&
                    signingType.SigningTool.TimestampServer.Length > 0)
                {
                    commandLine += " /tr \"" + signingType.SigningTool.TimestampServer.Trim() + "\"";
                }

                // add additional argments
                if (signingType.SigningTool.CommonSigningArguments != null &&
                    signingType.SigningTool.CommonSigningArguments.Length > 0)
                {
                    commandLine += " " + signingType.SigningTool.CommonSigningArguments;
                }
                if (signingType.AdditionalArguments != null &&
                    signingType.AdditionalArguments.Length > 0)
                {
                    commandLine += " " + signingType.AdditionalArguments;
                }
                // assume that the working dir is the shard dir C:\npsCSG\CSTest\" 
                // sharedDir\Request-NN\file 
                commandLine += " \""+signerConfig.sharedDir + "\\unsigned\\" + fparent + "\\" + fname+"\"";
                commandLine = signerConfig.winrsArg + " " + signerConfig.signExe + " " + commandLine;
            }
                return commandLine;
        }

        public string BuildSignBinaryCommandLine(CodeSigningType signingType, string fileToSign, SignerConfig signerConfig)
        {
            if (signingType.SigningTool.SigningArgumentFormat != 9) return null;
            string commandLine = "";
            lock (bLock)
            {
                var certsKeyStores = signerConfig.otherSignerConfig.certsAndKeystores;
                var certAndKeyStore = certsKeyStores.Find(f => f.certThumbprint.ToUpper() == signingType.SigningCertificate.Thumbprint.ToUpper());
                Guard.ValueNotNull(certAndKeyStore, "Error locating certificate entry in SignBinary configuration file.");

                string fparent = Directory.GetParent(fileToSign).Name;
                string fname = Path.GetFileName(fileToSign);
                string fnoext = fname.Replace(Path.GetExtension(fname), "");

                // add additional argments
                if (signingType.SigningTool.CommonSigningArguments != null &&
                    signingType.SigningTool.CommonSigningArguments.Length > 0)
                {
                    commandLine += " " + signingType.SigningTool.CommonSigningArguments;
                }
                if (signingType.AdditionalArguments != null &&
                    signingType.AdditionalArguments.Length > 0)
                {
                    commandLine += " " + signingType.AdditionalArguments;
                }

                // add the file to sign in quotes.
                commandLine += " -k "+ certAndKeyStore.keyName +" -i \"" + fileToSign + "\" -o \"" + fileToSign+".sig\"";
            }

            return commandLine;
        }


        public string BuildSnCommandLine(CodeSigningType signingType, string fileToSign, SignerConfig signerConfig)
        {
            if (signingType.SigningTool.SigningArgumentFormat != 10) return null;
            string commandLine = "";
            lock (bLock)
            {
                var certsKeyStores = signerConfig.otherSignerConfig.certsAndKeystores;
                var certAndKeyStore = certsKeyStores.Find(f => f.certThumbprint.ToUpper() == signingType.SigningCertificate.Thumbprint.ToUpper());
                Guard.ValueNotNull(certAndKeyStore, "Error locating certificate entry in Sn configuration file.");

                string fparent = Directory.GetParent(fileToSign).Name;
                string fname = Path.GetFileName(fileToSign);
                string fnoext = fname.Replace(Path.GetExtension(fname), "");

                commandLine += " -Rc \"" + fileToSign + "\" " + certAndKeyStore.keyName;
                // add additional argments
                if (signingType.SigningTool.CommonSigningArguments != null &&
                    signingType.SigningTool.CommonSigningArguments.Length > 0)
                {
                    commandLine += " " + signingType.SigningTool.CommonSigningArguments;
                }
                if (signingType.AdditionalArguments != null &&
                    signingType.AdditionalArguments.Length > 0)
                {
                    commandLine += " " + signingType.AdditionalArguments;
                }

            }

            return commandLine;
        }

        public string BuildSnRCommandLine(CodeSigningType signingType, string fileToSign, SignerConfig signerConfig)
        {
            if (signingType.SigningTool.SigningArgumentFormat != 12) return null;
            string commandLine = "";
            lock (bLock)
            {
                var certsKeyStores = signerConfig.otherSignerConfig.certsAndKeystores;
                var certAndKeyStore = certsKeyStores.Find(f => f.certThumbprint.ToUpper() == signingType.SigningCertificate.Thumbprint.ToUpper());
                Guard.ValueNotNull(certAndKeyStore, "Error locating certificate entry in SnR configuration file.");

                string fparent = Directory.GetParent(fileToSign).Name;
                string fname = Path.GetFileName(fileToSign);
                string fnoext = fname.Replace(Path.GetExtension(fname), "");

                commandLine += " -Rc \"" + signerConfig.sharedDir + "\\unsigned\\" + fparent + "\\" + fname + "\" " + certAndKeyStore.keyName;
                // add additional argments
                if (signingType.SigningTool.CommonSigningArguments != null &&
                    signingType.SigningTool.CommonSigningArguments.Length > 0)
                {
                    //sn -Rc file container
                    commandLine += " " + signingType.SigningTool.CommonSigningArguments;
                }
                if (signingType.AdditionalArguments != null &&
                    signingType.AdditionalArguments.Length > 0)
                {
                    commandLine += " " + signingType.AdditionalArguments;
                }

                // assume that the working dir is the shard dir C:\npsCSG\CSTest\" 
                // sharedDir\Request-NN\file 
                commandLine = signerConfig.winrsArg + " " + signerConfig.signExe + " " + commandLine;
            }

            return commandLine;
        }

        /// <summary>
        /// Builds the signing tool command line string
        /// </summary>
        /// <param name="signingType">Signing type record.</param>
        /// <param name="fileToVerify">The file to sign.</param>
        /// <returns></returns>
        public string BuildVerifyCommandLine(CodeSigningType signingType, string fileToVerify, SignerConfig signerConfig)
        {
            string commandLine = "";

            if (signingType.SigningTool.SigningArgumentFormat == 0)
            {
                // start with the certificate
                commandLine += "verify";

                // add additional argments
                if (signingType.SigningTool.CommonVerificationArguments != null &&
                    signingType.SigningTool.CommonVerificationArguments.Length > 0)
                {
                    commandLine += " " + signingType.SigningTool.CommonVerificationArguments;
                }
                // add the file to sign.
                commandLine += " \"" + fileToVerify + "\"";
            }
            else if (signingType.SigningTool.SigningArgumentFormat == 1)
            {
                // start with the certificate
                commandLine += "-verify ";

                // add additional argments
                if (signingType.SigningTool.CommonVerificationArguments != null &&
                    signingType.SigningTool.CommonVerificationArguments.Length > 0)
                {
                    commandLine += " " + signingType.SigningTool.CommonVerificationArguments;
                }

                // add the file to sign.
                commandLine += " \"" + fileToVerify + "\"";
            }
                    // codesign
            else if ((signingType.SigningTool.SigningArgumentFormat == 2)||
                (signingType.SigningTool.SigningArgumentFormat == 3))
            {
                // start ssh 
                commandLine += signerConfig.userHost+" \""+signerConfig.codesignCMD+" -v ";

                string fname = Path.GetFileName(fileToVerify);
                string fnoexit = fname.Replace(".tar", "");

                commandLine += signerConfig.codesignDir + "/" + fnoexit;

            }
                // signbinary
            else if (signingType.SigningTool.SigningArgumentFormat == 9)
            {
                var certsKeyStores = signerConfig.otherSignerConfig.certsAndKeystores;
                var certAndKeyStore = certsKeyStores.Find(f => f.certThumbprint.ToUpper() == signingType.SigningCertificate.Thumbprint.ToUpper());
                Guard.ValueNotNull(certAndKeyStore, "Error locating certificate entry in SignBinary configuration file.");

                string fname = Path.GetFileName(fileToVerify);
                
                // add additional argments
                if (signingType.SigningTool.CommonVerificationArguments != null &&
                    signingType.SigningTool.CommonVerificationArguments.Length > 0)
                {
                    commandLine += " " + signingType.SigningTool.CommonVerificationArguments;
                }

                // add the file to sign.
                commandLine += " -v \""+ certAndKeyStore.pubKeyFile +"\" -i \"" + fileToVerify + "\" -o \"" + fileToVerify+".sig\"";
            }
            else
            {

            }

            return commandLine;
        }

        /// <summary>
        /// Signs the specified file using signingType.
        /// </summary>
        /// <param name="signingType">Signing type e.g. Authenticode.</param>
        /// <param name="fileToSign">The file to sign.</param>
        /// <returns></returns>
        public ProcessRunResult Sign(CodeSigningType signingType, string fileToSign)
        {
            Log.Info($"Signing file:{fileToSign} with:{signingType.Name}");
            ProcessRunResult retVal = null;

            switch(signingType.SigningTool.SigningArgumentFormat)
            { 
                case 0:
                    retVal=SignSigntool(signingType, fileToSign);
                    break;
                case 1:
                    retVal=SignJarsigner(signingType, fileToSign);
                    break;
                case 2:
                    retVal = SignCodesign(signingType, fileToSign);
                    break;
                case 3:
                    retVal=SignCodesignS(signingType, fileToSign);
                    break;
                case 4:
                    retVal = SignHABcst(signingType, fileToSign);
                    break;
                case 5:
                    retVal = SignEvmctl(signingType, fileToSign);
                    break;
                case 6:
                    retVal = SignCertreq(signingType, fileToSign);
                    break;
                case 7:
                    retVal=SignCertreqR(signingType, fileToSign);
                    break;
                case 8:
                    retVal = SignSigntoolR(signingType, fileToSign);
                    break;
                case 9:
                    retVal = SignBinary(signingType, fileToSign);
                    break;
                case 10:
                    retVal= SignSn(signingType, fileToSign);
                    break;
                case 11:
                    retVal = SignLinux(signingType, fileToSign);
                    break;
                case 12:
                    retVal = SignSnR(signingType, fileToSign);
                    break;
                case 13:
                default:
                    retVal=new ProcessRunResult(1, $"Generic(13) and default not implemented yet", null);
                    break;
            }

            return retVal;
        }

        public ProcessRunResult SignSigntool(CodeSigningType signingType, string fileToSign)
        {
            if (signingType.SigningTool.SigningArgumentFormat != 0)
            {
                Log.Error("In SignSignTool (0), but the code is " + signingType.SigningTool.SigningArgumentFormat);
                throw new Exception("In SignSignTool (0), but the code is " + signingType.SigningTool.SigningArgumentFormat);
            }
            string commandLine;
            ProcessRunResult retVal = null;
            SignerConfig signingConfig = null;

            // get the signing tool configuration
            lock (xmlLock)
            {
                if (DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.SigntoolConfiguration] == null)
                {
                    Log.Error("Signtool configuration is invalid: There is no entry in the config file for the signtool configuration data.");
                    throw new Exception("Signtool Configuration Error");
                }

                signingConfig = LoadSigningToolConfig(DynamicConfigurationManager.
                                                            AppSettings[Constants.AppConfigNames.SigntoolConfiguration]);
            }
            Guard.ValueNotNull(signingConfig, "Signing tool is not properly configured, the signing tool config file was not loaded.");

            // validate tool configuration, could go in a dedicated function
            if (signingType.RequestTimestamp &&
                (signingType.SigningTool.TimestampServer == null ||
                 signingType.SigningTool.TimestampServer.Length == 0))
            {
                string msg = "Signing profile requires timestamping, but the selected tool is not configured with a timestamp server.";
                Log.Error(msg);
                throw new Exception(msg);
            }
            else
            {
                // make sure the signing exe file exists for local signing
                if (!File.Exists(signingConfig.signExe) && signingType.SigningTool.SigningArgumentFormat == 0)
                {
                    string errorMsg = $"Configuration Error: Cannot find EXE configured for code signnig.";
                    Log.Error(errorMsg);
                    throw new Exception(errorMsg);
                }
                else
                {
                    /*
                     * The VerifyExeHash function needs to be revisited.
                    if (VerifyExeHash(signingConfig.signExe) == false)
                    {
                        string errorMsg = $"Invalid EXE configured for code signing.";
                        Log.Error(errorMsg);
                        throw new Exception(errorMsg);
                    }
                    else
                    */
                    commandLine = BuildSigntoolCommandLine(signingType, fileToSign, signingConfig);
                    Guard.ValueNotBlankOrNull(commandLine, "Signing tool is not properly configured, could not build a command line string.");
                    Log.Debug("Invoking signing command: " + commandLine);
                    var result = ProcRunner.Run(signingConfig.signExe, commandLine, SigningTimeoutMs);
                    retVal = new ProcessRunResult(result.ResultCode,
                    $"signtool {commandLine}   output:{Environment.NewLine}{result.Output}{Environment.NewLine}Signing Exit Code: {result.ResultCode}",
                    result.Errors);
                    Log.Info($"Result of signing file:{fileToSign} with:{signingType.Name} is:{retVal.ResultCode}");
                    if ((retVal.ResultCode == 0))       
                    {
                        result = VerifySignature(signingType, signingConfig, fileToSign);
                        retVal = new ProcessRunResult(result.ResultCode,
                        $"{retVal.Output}{Environment.NewLine}{Environment.NewLine}verify  output:{result.Output}{Environment.NewLine}verify Exit Code: {result.ResultCode}",
                        result.Errors);
                    }
                    else
                    {
                        Log.Error($"Error messages: {result.Errors}, Output: {result.Output}");
                    }

                    
                }
            }

            return retVal;
        }

        public ProcessRunResult SignSigntoolR(CodeSigningType signingType, string fileToSign)
        {
            if (signingType.SigningTool.SigningArgumentFormat != 8)
            {
                Log.Error("In SignSignToolR (8), but the code is " + signingType.SigningTool.SigningArgumentFormat);
                throw new Exception("In SignSignToolR (8), but the code is " + signingType.SigningTool.SigningArgumentFormat);
            }
            string commandLine;
            ProcessRunResult retVal = null;
            SignerConfig signingConfig = null;

            // get the signing tool configuration
            lock (xmlLock)
            {
                if (DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.SigntoolRConfiguration] == null)
                {
                    Log.Error("SigntoolRemote configuration is invalid: There is no entry in the config file for the SigntoolRemote configuration data.");
                    throw new Exception("SingtoolRemote Configuration Error");
                }

                signingConfig = LoadSigningToolConfig(DynamicConfigurationManager.
                                                            AppSettings[Constants.AppConfigNames.SigntoolRConfiguration]);
            }
            Guard.ValueNotNull(signingConfig, "SigningtoolRemote is not properly configured, the signing tool remote config file was not loaded.");

            // validate tool configuration, could go in a dedicated function
            if (signingType.RequestTimestamp &&
                (signingType.SigningTool.TimestampServer == null ||
                 signingType.SigningTool.TimestampServer.Length == 0))
            {
                string msg = "Signing profile requires timestamping, but the selected tool is not configured with a timestamp server.";
                Log.Error(msg);
                throw new Exception(msg);
            }
            else
            {
                commandLine = BuildSigntoolRCommandLine(signingType, fileToSign, signingConfig);
                Guard.ValueNotBlankOrNull(commandLine, "Signing tool is not properly configured, could not build a command line string.");
                Log.Debug("Invoking signing command: " + commandLine);
                var result = ProcRunner.Run(signingConfig.winrsExe, commandLine, SigningTimeoutMs);
                retVal = new ProcessRunResult(result.ResultCode,
                $"winrs {commandLine}   output:{Environment.NewLine}{result.Output}{Environment.NewLine}Signing Exit Code: {result.ResultCode}",
                result.Errors);

                Log.Info($"Result of signing file:{fileToSign} with:{signingType.Name} is:{retVal.ResultCode}");

                if ((retVal.ResultCode == 0))
                {
                    result = VerifySignature(signingType, signingConfig, fileToSign);
                    retVal = new ProcessRunResult(result.ResultCode,
                    $"{retVal.Output}{Environment.NewLine}{Environment.NewLine}verify  output:{result.Output}{Environment.NewLine}verify Exit Code: {result.ResultCode}",
                    result.Errors);
                }
                else
                {
                    Log.Error($"Error messages: {result.Errors}, Output: {result.Output}");
                }   
            }
            return retVal;
        }

        public ProcessRunResult SignJarsigner(CodeSigningType signingType, string fileToSign)
        {
            if (signingType.SigningTool.SigningArgumentFormat != 1)
            {
                Log.Error("In SignJarsigner (1), but the code is " + signingType.SigningTool.SigningArgumentFormat);
                throw new Exception("In SignJarsigner (1), but the code is " + signingType.SigningTool.SigningArgumentFormat);
            }
            Log.Info($"Signing file:{fileToSign} with:{signingType.Name}");
            string commandLine;
            ProcessRunResult retVal = null;
            SignerConfig signingConfig = null;

            // get the signing tool configuration
            lock (xmlLock)
            {
                if (DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.JarsignerConfiguration] == null)
                {
                    Log.Error("Jarsigner configuration is invalid: There is no entry in the config file for the jarsigner configuration data.");
                    throw new Exception("Jarsigner Configuration Error");
                }
                signingConfig = LoadSigningToolConfig(DynamicConfigurationManager.
                                                            AppSettings[Constants.AppConfigNames.JarsignerConfiguration]);
            }
            Guard.ValueNotNull(signingConfig, "Jarsigner is not properly configured, the jarsigner config file was not loaded.");

            // validate tool configuration, could go in a dedicated function
            if (signingType.RequestTimestamp &&
                (signingType.SigningTool.TimestampServer == null ||
                 signingType.SigningTool.TimestampServer.Length == 0))
            {
                string msg = "Signing profile requires timestamping, but the selected tool is not configured with a timestamp server.";
                Log.Error(msg);
                throw new Exception(msg);
            }
            else
            {
                commandLine = BuildJarsignerCommandLine(signingType, fileToSign, signingConfig);

                Guard.ValueNotBlankOrNull(commandLine, "Signing tool is not properly configured, could not build a command line string.");

                Log.Debug("Invoking signing command: " + commandLine);

                // make sure the signing exe file exists
                if (!File.Exists(signingConfig.signExe))
                {
                    string errorMsg = $"Configuration Error: Cannot find EXE configured for code signnig.";
                    Log.Error(errorMsg);
                    throw new Exception(errorMsg);
                }
                else
                {
                    var result = ProcRunner.Run(signingConfig.signExe, commandLine, SigningTimeoutMs);
                    retVal = new ProcessRunResult(result.ResultCode,
                    $"jarsigner {commandLine}   output:{Environment.NewLine}{result.Output}{Environment.NewLine}Signing Exit Code: {result.ResultCode}",
                    result.Errors);
                    Log.Info($"Result of signing file:{fileToSign} with:{signingType.Name} is:{retVal.ResultCode}");
                    Log.Info($"Result of signing file:{fileToSign} with:{signingType.Name} is:{retVal.ResultCode}");
                    if ((retVal.ResultCode == 0))
                    {
                        result = VerifySignature(signingType, signingConfig, fileToSign);
                        retVal = new ProcessRunResult(result.ResultCode,
                        $"{retVal.Output}{Environment.NewLine}{Environment.NewLine}verify  output:{result.Output}{Environment.NewLine}verify Exit Code: {result.ResultCode}",
                        result.Errors);
                    }
                    else
                    {
                        Log.Error($"Error messages: {result.Errors}, Output: {result.Output}");
                    }               
                }
            }

            return retVal;
        }

        ProcessRunResult GetCMDResult(string commandLine, SignerConfig signingConfig)
        {
            ProcessRunResult retVal = null;
            ProcessRunResult result = new ProcessRunResult(1, null, null);
            Log.Debug("Invoking signing command: " + commandLine);
            Log.Info($"commandLine:{commandLine}");
            string[] cmdlines = commandLine.Split(',');
            if (cmdlines.Length != 3) return result;
            result = ProcRunner.Run(signingConfig.scpExe, cmdlines[0], SigningTimeoutMs);
            retVal = new ProcessRunResult(result.ResultCode,
            $"scp {cmdlines[0]}   output:{Environment.NewLine}{result.Output}{Environment.NewLine}scp Exit Code: {result.ResultCode}",
            result.Errors);
            if (result.ResultCode != 0)
            {
                Log.Error($"Error messages: {result.Errors} or expected pattern not matching, Output: {result.Output}");
                return retVal;
            }
            result = ProcRunner.Run(signingConfig.sshExe, cmdlines[1], SigningTimeoutMs);
            retVal = new ProcessRunResult(result.ResultCode,
            $"{retVal.Output}{Environment.NewLine}ssh {cmdlines[1]}   output:{Environment.NewLine}{result.Output}{Environment.NewLine}Signing Exit Code: {result.ResultCode}",
            result.Errors);
            if (result.ResultCode != 0)
            {
                Log.Error($"Error messages: {result.Errors} or expected pattern not matching, Output: {result.Output}");
                return retVal;
            }
            result = ProcRunner.Run(signingConfig.scpExe, cmdlines[2], SigningTimeoutMs);
            retVal = new ProcessRunResult(result.ResultCode,
            $"{retVal.Output}{Environment.NewLine}scp {cmdlines[2]}   output:{Environment.NewLine}{result.Output}{Environment.NewLine}scp Exit Code: {result.ResultCode}",
            result.Errors);
            return retVal;
        }

        public ProcessRunResult SignCodesign(CodeSigningType signingType, string fileToSign)
        {
            if (signingType.SigningTool.SigningArgumentFormat != 2)
            {
                Log.Error("In SignCodesign (2), but the code is " + signingType.SigningTool.SigningArgumentFormat);
                throw new Exception("In SignCodesign (2), but the code is " + signingType.SigningTool.SigningArgumentFormat);
            }

            string commandLine;
            ProcessRunResult retVal = null;
            SignerConfig signingConfig = null;

            // get the signing tool configuration
            lock (xmlLock)
            {
                    if (DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.CodesignConfiguration] == null)
                    {
                        Log.Error("Codesign configuration is invalid: There is no entry in the config file for the Codesign configuration data.");
                        throw new Exception("Codesign Configuration Error");
                    }

                    signingConfig = LoadSigningToolConfig(DynamicConfigurationManager.
                                                              AppSettings[Constants.AppConfigNames.CodesignConfiguration]);
            }
            Guard.ValueNotNull(signingConfig, "Codesign is not properly configured, the codesign config file was not loaded.");

            // validate tool configuration, could go in a dedicated function
            if (signingType.RequestTimestamp &&
                (signingType.SigningTool.TimestampServer == null ||
                 signingType.SigningTool.TimestampServer.Length == 0))
            {
                string msg = "Signing profile requires timestamping, but the selected tool is not configured with a timestamp server.";
                Log.Error(msg);
                throw new Exception(msg);
            }
            else
            {
                commandLine = BuildCodesignCommandLine(signingType, fileToSign, signingConfig);
                Guard.ValueNotBlankOrNull(commandLine, "Codesign is not properly configured, could not build a command line string.");
                var result = GetCMDResult(commandLine, signingConfig);
                retVal = new ProcessRunResult(result.ResultCode,
                        $"output:{Environment.NewLine}{result.Output}{Environment.NewLine}Signing Exit Code: {result.ResultCode}",
                        result.Errors);
                if (retVal.ResultCode != 0)
                {
                    Log.Error($"Error messages: {retVal.Errors} or expected pattern not matching, Output: {retVal.Output}");
                    return retVal;
                }

                Log.Info($"Result of signing file:{fileToSign} with:{signingType.Name} is:{retVal.ResultCode}");
                result = VerifySignature(signingType, signingConfig, fileToSign);
                retVal = new ProcessRunResult(result.ResultCode,
                $"{retVal.Output}{Environment.NewLine}{Environment.NewLine}verify  output:{result.Output}{Environment.NewLine}verify Exit Code: {result.ResultCode}",
                result.Errors);
                
            }

            return retVal;
        }

        public ProcessRunResult SignCodesignS(CodeSigningType signingType, string fileToSign)
        {
            if (signingType.SigningTool.SigningArgumentFormat != 3)
            {
                Log.Error("In SignCodesignS (3), but the code is " + signingType.SigningTool.SigningArgumentFormat);
                throw new Exception("In SignCodesignS (3), but the code is " + signingType.SigningTool.SigningArgumentFormat);
            }
            Log.Info($"Signing file:{fileToSign} with:{signingType.Name}");
            string commandLine;
            ProcessRunResult retVal = null;
            SignerConfig signingConfig = null;

            // get the signing tool configuration
            lock (xmlLock)
            {
                if (DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.CodesignSConfiguration] == null)
                {
                    Log.Error("CodesignShare configuration is invalid: There is no entry in the config file for the CodesignShare configuration data.");
                    throw new Exception("Code Configuration Error");
                }

                signingConfig = LoadSigningToolConfig(DynamicConfigurationManager.
                                                            AppSettings[Constants.AppConfigNames.CodesignSConfiguration]);

            }
            Guard.ValueNotNull(signingConfig, "Signing tool is not properly configured, the signing tool config file was not loaded.");

            // validate tool configuration, could go in a dedicated function
            if (signingType.RequestTimestamp &&
                (signingType.SigningTool.TimestampServer == null ||
                 signingType.SigningTool.TimestampServer.Length == 0))
            {
                string msg = "Signing profile requires timestamping, but the selected tool is not configured with a timestamp server.";
                Log.Error(msg);
                throw new Exception(msg);
            }
            else
            {
                commandLine = BuildCodesignSCommandLine(signingType, fileToSign, signingConfig);
                Guard.ValueNotBlankOrNull(commandLine, "Signing tool is not properly configured, could not build a command line string.");
                Log.Debug("Invoking signing command: " + commandLine);
                var result = ProcRunner.Run(signingConfig.sshExe, commandLine, SigningTimeoutMs);
                retVal = new ProcessRunResult(result.ResultCode,
                $"output:{Environment.NewLine}{result.Output}{Environment.NewLine}Signing Exit Code: {result.ResultCode}",
                result.Errors);
                if (retVal.ResultCode != 0)
                {
                    Log.Error($"Error messages: {retVal.Errors} or expected pattern not matching, Output: {retVal.Output}");
                    return retVal;
                }
                result = VerifySignature(signingType, signingConfig, fileToSign);
                retVal = new ProcessRunResult(result.ResultCode,
                $"{retVal.Output}{Environment.NewLine}{Environment.NewLine}verify  output:{result.Output}{Environment.NewLine}verify Exit Code: {result.ResultCode}",
                result.Errors);
            }

            return retVal;
        }

        public ProcessRunResult SignHABcst(CodeSigningType signingType, string fileToSign)
        {
            if (signingType.SigningTool.SigningArgumentFormat != 4)
            {
                Log.Error("In HABcst (4), but the code is " + signingType.SigningTool.SigningArgumentFormat);
                throw new Exception("In HABcst (4), but the code is " + signingType.SigningTool.SigningArgumentFormat);
            }
            Log.Info($"Signing file:{fileToSign} with:{signingType.Name}");
            string commandLine;
            SignerConfig signingConfig = null;

            // get the signing tool configuration
            lock (xmlLock)
            {
                if (DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.HABcstConfiguration] == null)
                {
                    Log.Error("HABcst configuration is invalid: There is no entry in the config file for the HABcst configuration data.");
                    throw new Exception("Codesign Configuration Error");
                }

                signingConfig = LoadSigningToolConfig(DynamicConfigurationManager.
                                                            AppSettings[Constants.AppConfigNames.HABcstConfiguration]);
            }
            Guard.ValueNotNull(signingConfig, "HABcst is not properly configured, the HABcst config file was not loaded.");
            commandLine = BuildHABcstCommandLine(signingType, fileToSign, signingConfig);
            Guard.ValueNotBlankOrNull(commandLine, "HABcst is not properly configured and could not build a command line string.");
            Log.Debug("Invoking signing command: " + commandLine);
            return GetCMDResult(commandLine, signingConfig);      
        }

        public ProcessRunResult SignEvmctl(CodeSigningType signingType, string fileToSign)
        {
            if (signingType.SigningTool.SigningArgumentFormat != 5)
            {
                Log.Error("In evmctl (5), but the code is " + signingType.SigningTool.SigningArgumentFormat);
                throw new Exception("In evmctl (5), but the code is " + signingType.SigningTool.SigningArgumentFormat);
            }
            Log.Info($"Signing file:{fileToSign} with:{signingType.Name}");
            string commandLine;
            SignerConfig signingConfig = null;

            // get the signing tool configuration
            lock (xmlLock)
            {
                if (DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.EvmctlConfiguration] == null)
                {
                    Log.Error("Evmctl configuration is invalid: There is no entry in the config file for the evmctl configuration data.");
                    throw new Exception("Evmctl Configuration Error");
                }

                signingConfig = LoadSigningToolConfig(DynamicConfigurationManager.
                                                            AppSettings[Constants.AppConfigNames.HABcstConfiguration]);
            }
            Guard.ValueNotNull(signingConfig, "evmctl is not properly configured, the evmctl config file was not loaded.");
            commandLine = BuildEvmctlCommandLine(signingType, fileToSign, signingConfig);
            Guard.ValueNotBlankOrNull(commandLine, "Evmctl is not properly configured and could not build a command line string.");
            return GetCMDResult(commandLine, signingConfig);
        }

        public ProcessRunResult SignLinux(CodeSigningType signingType, string fileToSign)
        {
            if (signingType.SigningTool.SigningArgumentFormat != 11)
            {
                Log.Error("In SignLinux (11), but the code is " + signingType.SigningTool.SigningArgumentFormat);
                throw new Exception("In SignLinux (11), but the code is " + signingType.SigningTool.SigningArgumentFormat);
            }
            Log.Info($"Signing file:{fileToSign} with:{signingType.Name}");
            string commandLine;
            SignerConfig signingConfig = null;

            // get the signing tool configuration
            lock (xmlLock)
            {
                    if (DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.LinuxScriptConfiguration] == null)
                    {
                        Log.Error("LinuxScript configuration is invalid: There is no entry in the config file for the LinuxScript configuration data.");
                        throw new Exception("LinuxScript Configuration Error");
                    }

                    signingConfig = LoadSigningToolConfig(DynamicConfigurationManager.
                                                              AppSettings[Constants.AppConfigNames.LinuxScriptConfiguration]);
            }
            Guard.ValueNotNull(signingConfig, "LinuxScript is not properly configured and its config file was not loaded.");
            commandLine = BuildLinuxCommandLine(signingType, fileToSign, signingConfig);
            Guard.ValueNotBlankOrNull(commandLine, "Signing tool is not properly configured and could not build a command line string.");
            return GetCMDResult(commandLine, signingConfig);
        }

        public ProcessRunResult SignCertreq(CodeSigningType signingType, string fileToSign)
        {
            if (signingType.SigningTool.SigningArgumentFormat != 6)
            {
                Log.Error("In SignCertreq (6), but the code is " + signingType.SigningTool.SigningArgumentFormat);
                throw new Exception("In SignCertreq (6), but the code is " + signingType.SigningTool.SigningArgumentFormat);
            }
            Log.Info($"Signing file:{fileToSign} with:{signingType.Name}");
            string commandLine;
            SignerConfig signingConfig = null;

            // get the signing tool configuration
            lock (xmlLock)
            {

                    if (DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.CertreqConfiguration] == null)
                    {
                        Log.Error("Certreq configuration is invalid: There is no entry in the config file for the Certreq configuration data.");
                        throw new Exception("Codesign Configuration Error");
                    }

                    signingConfig = LoadSigningToolConfig(DynamicConfigurationManager.
                                                              AppSettings[Constants.AppConfigNames.CertreqConfiguration]);
               
            }
            Guard.ValueNotNull(signingConfig, "Certreq is not properly configured and its config file was not loaded.");
            commandLine = BuildCertreqCommandLine(signingType, fileToSign, signingConfig);
            Guard.ValueNotBlankOrNull(commandLine, "Signing tool is not properly configured, could not build a command line string.");
            Log.Debug("Invoking signing command: " + commandLine);                  
            return ProcRunner.Run(signingConfig.certreqExe, commandLine, SigningTimeoutMs);
        }

        public ProcessRunResult SignCertreqR(CodeSigningType signingType, string fileToSign)
        {
            if (signingType.SigningTool.SigningArgumentFormat != 7)
            {
                Log.Error("In SignCertreqR (7), but the code is " + signingType.SigningTool.SigningArgumentFormat);
                throw new Exception("In SignCertreq (7), but the code is " + signingType.SigningTool.SigningArgumentFormat);
            }
            Log.Info($"Signing file:{fileToSign} with:{signingType.Name}");
            string commandLine;
            SignerConfig signingConfig = null;

            // get the signing tool configuration
            lock (xmlLock)
            {
                if (DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.CertreqRConfiguration] == null)
                {
                    Log.Error("CertreqRemote configuration is invalid: There is no entry in the config file for the CertreqRemote configuration data.");
                    throw new Exception("CertreqRemote Configuration Error");
                }

                signingConfig = LoadSigningToolConfig(DynamicConfigurationManager.
                                                            AppSettings[Constants.AppConfigNames.CertreqRConfiguration]);
            }
            Guard.ValueNotNull(signingConfig, "CertreqR is not properly configured and config file was not loaded.");

            commandLine = BuildCertreqRCommandLine(signingType, fileToSign, signingConfig);

            Guard.ValueNotBlankOrNull(commandLine, "Signing tool is not properly configured, could not build a command line string.");

            Log.Debug("Invoking signing command: " + commandLine);

            return ProcRunner.Run(signingConfig.winrsExe, commandLine, SigningTimeoutMs);


        }

        public ProcessRunResult SignBinary(CodeSigningType signingType, string fileToSign)
        {
            if (signingType.SigningTool.SigningArgumentFormat != 9)
            {
                Log.Error("In SignClient (9), but the code is " + signingType.SigningTool.SigningArgumentFormat);
                throw new Exception("In SignClient (9), but the code is " + signingType.SigningTool.SigningArgumentFormat);
            }
            Log.Info($"Signing file:{fileToSign} with:{signingType.Name}");
            string commandLine;
            SignerConfig signingConfig = null;

            // get the signing tool configuration
            lock (xmlLock)
            {
                if (DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.SignBinaryConfiguration] == null)
                {
                    Log.Error("SignBinary configuration is invalid: There is no entry in the config file for the SignBinary configuration data.");
                    throw new Exception("SignBinary Configuration Error");
                }

                signingConfig = LoadSigningToolConfig(DynamicConfigurationManager.
                                                            AppSettings[Constants.AppConfigNames.SignBinaryConfiguration]);

            }
            Guard.ValueNotNull(signingConfig, "SignBinary is not properly configured and its config file was not loaded.");


            commandLine = BuildSignBinaryCommandLine(signingType, fileToSign, signingConfig);

            Guard.ValueNotBlankOrNull(commandLine, "SignBinary is not properly configured and could not build a command line string.");

            Log.Debug("Invoking signing command: " + commandLine);
            // exe to be changed
            var result = ProcRunner.Run(signingConfig.signExe, commandLine, SigningTimeoutMs);
            ProcessRunResult retVal = new ProcessRunResult(result.ResultCode,
            $"SignBinary {commandLine}   output:{Environment.NewLine}{result.Output}{Environment.NewLine}Signing Exit Code: {result.ResultCode}",
            result.Errors);


            if ((retVal.ResultCode == 0))
            {
                result = VerifySignature(signingType, signingConfig, fileToSign);
                retVal = new ProcessRunResult(result.ResultCode,
                $"{retVal.Output}{Environment.NewLine}{Environment.NewLine}verify  output:{result.Output}{Environment.NewLine}verify Exit Code: {result.ResultCode}",
                result.Errors);
            }
            else
            {
                Log.Error($"Error messages: {result.Errors}, Output: {result.Output}");
            }

            return retVal;

        }
        public ProcessRunResult SignSn(CodeSigningType signingType, string fileToSign)
        {
            if (signingType.SigningTool.SigningArgumentFormat != 10)
            {
                Log.Error("In SignSn (10), but the code is " + signingType.SigningTool.SigningArgumentFormat);
                throw new Exception("In SignSn (10), but the code is " + signingType.SigningTool.SigningArgumentFormat);
            }
            Log.Info($"Signing file:{fileToSign} with:{signingType.Name}");
            string commandLine;
            SignerConfig signingConfig = null;

            // get the signing tool configuration
            lock (xmlLock)
            {

                    if (DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.SnConfiguration] == null)
                    {
                        Log.Error("Sn configuration is invalid: There is no entry in the config file for the Sn configuration data.");
                        throw new Exception("Sn Configuration Error");
                    }

                    signingConfig = LoadSigningToolConfig(DynamicConfigurationManager.
                                                              AppSettings[Constants.AppConfigNames.SnConfiguration]);

            }
            Guard.ValueNotNull(signingConfig, "Sn is not properly configured and its config file was not loaded.");
            commandLine = BuildSnCommandLine(signingType, fileToSign, signingConfig);

            Guard.ValueNotBlankOrNull(commandLine, "Sn is not properly configured and could not build a command line string.");

            Log.Debug("Invoking sn signing command: " + commandLine);

            // exe to be changed
            return ProcRunner.Run(signingConfig.signExe, commandLine, SigningTimeoutMs);
        }


        public ProcessRunResult SignSnR(CodeSigningType signingType, string fileToSign)
        {
            if (signingType.SigningTool.SigningArgumentFormat != 12)
            {
                Log.Error("In SnR (12), but the code is " + signingType.SigningTool.SigningArgumentFormat);
                throw new Exception("In SignSnR (12), but the code is " + signingType.SigningTool.SigningArgumentFormat);
            }
            Log.Info($"Signing file:{fileToSign} with:{signingType.Name}");
            string commandLine;
            SignerConfig signingConfig = null;

            // get the signing tool configuration
            lock (xmlLock)
            {
                if (DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.SnRConfiguration] == null)
                {
                    Log.Error("SnR configuration is invalid: There is no entry in the config file for the CertreqRemote configuration data.");
                    throw new Exception("SnR Configuration Error");
                }

                signingConfig = LoadSigningToolConfig(DynamicConfigurationManager.
                                                            AppSettings[Constants.AppConfigNames.SnRConfiguration]);
            }
            Guard.ValueNotNull(signingConfig, "SnR is not properly configured and config file was not loaded.");

            commandLine = BuildSnRCommandLine(signingType, fileToSign, signingConfig);

            Guard.ValueNotBlankOrNull(commandLine, "Signing tool is not properly configured, could not build a command line string.");

            Log.Debug("Invoking signing command: " + commandLine);

            return ProcRunner.Run(signingConfig.winrsExe, commandLine, SigningTimeoutMs);


        }

        private ProcessRunResult VerifySignature(CodeSigningType signingType, SignerConfig signingConfig,
                                               string fileToVerify)
        {
            ProcessRunResult retVal =new ProcessRunResult(0, "Verification is NOT requested", null);
            string commandLine;

            if (!signingType.DoVerification)
            {
                Log.Info($"Verification is NOT requested.");
                return  retVal;
            }

            Log.Info($"Verifying file:{fileToVerify} with:{signingType.Name}");

            // TODO:
            if (VerifyExeHash("") == false)
            {
                string errorMsg = $"Invalid EXE configured for code signing.";
                Log.Error(errorMsg);
                throw new Exception(errorMsg);
            }
            else
            {
                commandLine = BuildVerifyCommandLine(signingType, fileToVerify, signingConfig);
                var result = new ProcessRunResult(1, null, null);

                if (signingType.SigningTool.SigningArgumentFormat < 2 || signingType.SigningTool.SigningArgumentFormat == 9)
                {
                    result = ProcRunner.Run(signingConfig.verifyExe, commandLine, SigningTimeoutMs);
                    Log.Info($"Result of verifying file:{fileToVerify} with:{signingType.Name} is:{result.ResultCode}");
                }
                else if ( (signingType.SigningTool.SigningArgumentFormat == 2)||
                    (signingType.SigningTool.SigningArgumentFormat == 3) )
                {
                    result = ProcRunner.Run(signingConfig.codesignCMD, commandLine, SigningTimeoutMs);
                    Log.Info($"Result of verifying file:{fileToVerify} with:{signingType.Name} is:{result.ResultCode}");
                }
                else
                {
                    Log.Info($"No Signature verification");
                }

                // Append verification to signing results
                retVal = new ProcessRunResult(result.ResultCode,
                $"{Environment.NewLine}Verification Exit Code: {result.ResultCode}", result.Errors + Environment.NewLine + result.Errors);
            }

            return retVal;
        }

        /// <summary>
        /// Checks the hash of the passed in file against list of approved hashes if 
        /// hash values are configured.
        /// </summary>
        /// <param name="exePath">The file to verify.</param>
        /// <returns>true if verified or no hashes configured, false otherwise</returns>

        private bool VerifyExeHash(string exePath)
        {
            bool retVal = true;
            // determine whether or not the signing exe is acceptable
            if (_hashValuesFromConfig.Count > 0)
            {
                string exeHash = null;
                using (FileStream fs = new FileStream(exePath, FileMode.Open))
                using (BufferedStream bs = new BufferedStream(fs))
                {
                    using (SHA1Managed sha1 = new SHA1Managed())
                    {
                        byte[] hash = sha1.ComputeHash(bs);
                        StringBuilder formatted = new StringBuilder(2 * hash.Length);
                        foreach (byte b in hash)
                        {
                            formatted.AppendFormat("{0:X2}", b);
                        }
                        exeHash = formatted.ToString();
                    }
                }

                if (!_hashValuesFromConfig.Contains(exeHash))
                {
                    // reject the signign request, something is wrong in the configuration
                    retVal = false;
                }

            }

            return retVal;
        }

        private static List<string> GetExeHashValuesFromConfig()
        {
            List<string> hashValuesFromConfig = new List<string>();
            string temp = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.SigningExeHashValues];

            if (!String.IsNullOrWhiteSpace(temp))
            {
                string[] hashes = temp.Split(',');

                foreach (string hash in hashes)
                {
                    hashValuesFromConfig.Add(hash.Trim());
                }
            }
            return hashValuesFromConfig;
        }
    }
    [XmlType]
    public class SignerConfig
    {
        [XmlElement]
        public string signExe;
        [XmlElement]
        public string verifyExe;
        [XmlElement]
        public JarsignerConfig jarsignerConfig;
        [XmlElement]
        public string codesignCMD;
        [XmlElement]
        public string paramFile;
        [XmlElement]
        public string codesignDir;
        [XmlElement]
        public string tarCMD;
        [XmlElement]
        public string userHost;
        [XmlElement]
        public string scpExe;
        [XmlElement]
        public string sshExe;
        [XmlElement]
        public string certreqExe;
        [XmlElement]
        public string copyExe;
        [XmlElement]
        public string delExe;
        [XmlElement]
        public string sharedDir;
        [XmlElement]
        public string winrsExe;
        [XmlElement]
        public string winrsArg;
        // for auditing purpose, the keys are tracked by the certificates
        // OtherSignerConfig for those signtools, none jarsigner and signtool 
        [XmlElement]
        public OtherSignerConfig otherSignerConfig;

    }

    [XmlType]
    public class JarsignerConfig
    {
        [XmlArray(ElementName = "certsAndKeystores")]
        [XmlArrayItem(ElementName = "certAndKeystore")]
        public List<CertAndKeystore> certsAndKeystores;
    }

    [XmlType]
    public class OtherSignerConfig
    {
        [XmlArray(ElementName = "certsAndKeystores")]
        [XmlArrayItem(ElementName = "certAndKeystore")]
        public List<CertAndKeystore> certsAndKeystores;
    }


    [XmlType]
    public class CertAndKeystore
    {
        [XmlElement]
        public string certThumbprint;
        [XmlElement]
        public string keystoreFile;
        [XmlElement]
        public string keystorePass;
        [XmlElement]
        public string alias;
        [XmlElement]
        public string keyPass;
        [XmlElement]
        public string keyName;
        [XmlElement]
        public string pubKeyFile;
    }

}
