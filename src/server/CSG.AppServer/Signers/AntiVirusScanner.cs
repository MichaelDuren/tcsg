﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using CSG.AppServer.Entities;
using CSG.AppServer.Managers;
using Microsoft.Practices.Unity;
using Util.Config;
using Util;
using System.Xml.Serialization;
using System.Xml;

namespace CSG.AppServer.Signers
{

    /// <summary>
    /// Scans files.
    /// </summary>
    internal class AntivirusScanner
    {
        private static readonly ESPLogWrapper.ILog Log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly Type ThisDeclaringType = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
        private ScanConfig scanConfig = null;

        public AntivirusScanner()
        {
            SigningTimeoutMs = (int)SystemSettingsManager.Current.GetBySettingName(Constants.SettingNames.CodeSigningTimeoutMs).IntegerValue;


        }


        public int SigningTimeoutMs { get; set; }

        [Dependency]
        public IProcessRunner ProcRunner { get; set; }

        public ScanConfig LoadSigningToolConfig(string configFile)
        {
            scanConfig = null;
            // get the signing tool config from the app.config file
            FileStream fs = new FileStream(configFile, FileMode.Open);
            XmlReader reader = XmlReader.Create(fs);
            using (fs)
            using (reader)
            {
                try
                {

                    // Create an instance of the XmlSerializer specifying type and namespace.
                    XmlSerializer serializer = new
                        XmlSerializer(typeof(ScanConfig), new XmlRootAttribute() { ElementName = "ScanConfig" });

                    scanConfig = (ScanConfig)serializer.Deserialize(reader);

                }
                finally
                {
                    fs.Close();
                }
            }
            return scanConfig;
        }

        /// <summary>
        /// Builds the copying command line string
        /// </summary>
        /// <param name="fileToScan">The file to scan.</param>
        /// <param name="scanConfig">Scan Config file.</param>
        /// <returns></returns>
        public string BuildCopyCommandLine(string fileToScan, string remoteDir)
        {
            if (scanConfig == null) return null;

            string commandLine = "";

            commandLine += scanConfig.copyArg+" \""+fileToScan+"\" \""+remoteDir+"\"";
 
            return commandLine;
        }

        /// <summary>
        /// Builds the scanning command line string
        /// </summary>
        /// <param name="fileToScan">The file to scan.</param>
        /// <param name="scanConfig">Scan Config file.</param>
        /// <returns></returns>
        public string BuildScanCommandLine(string fileToScan, string remoteDir)
        {
            string commandLine = "";

            commandLine = scanConfig.winrsArg +" \"\""+scanConfig.scanExe+"\" "+scanConfig.scanArg+" \"" + remoteDir +"\\"+ Path.GetFileName(fileToScan)+"\"\"";

            return commandLine;
        }

        public string BuildScanDirCommandLine(string remoteDir)
        {
            string commandLine = "";

            commandLine = scanConfig.winrsArg + " \"\"" + scanConfig.scanExe + "\" " + scanConfig.scanArg + " \"" + remoteDir + "\"\"";

            return commandLine;
        }

        /// <summary>
        /// Signs the specified file using signingType.
        /// </summary>
        /// <param name="fileToScan">The file to sign.</param>
        /// <returns></returns>
        public ProcessRunResult Scan(string fileToScan, string fileDir, string fileHash)
        {
            
            Log.Info($"scanning file:{fileToScan}");
            string commandLine;
            ProcessRunResult retVal = null, copyVal=null;
            bool vflag = false;
            
            var remoteDir = Path.Combine(scanConfig.copyDir, fileDir);
            RecreateDirectory(remoteDir);
            
            commandLine = BuildCopyCommandLine( fileToScan, remoteDir);

            Guard.ValueNotBlankOrNull(commandLine, "Signing tool (copy) is not properly configured, could not build a command line string.");
            

            var result = ProcRunner.Run("\""+scanConfig.copyExe+"\"", commandLine, SigningTimeoutMs);

            Log.Info($"Result of copying file:{fileToScan} is:{result.ResultCode}");


            copyVal = new ProcessRunResult(result.ResultCode,
                $"{result.Output}{Environment.NewLine}Copying Exit Code: {result.ResultCode}",
                result.Errors);
            
            if (result.ResultCode == 0)
            {
                // scanning result.
                commandLine = BuildScanCommandLine(fileToScan, remoteDir);

                Guard.ValueNotBlankOrNull(commandLine, "Signing tool (scan) is not properly configured, could not build a command line string.");

                //string scanexe = "C:\\Windows\\System32\\winrs.exe";
                //commandLine = "-r:win10 -u:myorg\\cwang -p:Welcome1 \"C:\\Program Files\\Windows Defender\\MpCmdRun.exe\" -Scan -ScanType 3 -File C:\\tmp\\"+Path.GetFileName(fileToScan);

                result = ProcRunner.Run("\"" + scanConfig.winrsExe + "\"", commandLine, SigningTimeoutMs);

                Log.Info($"Result of scanning file:{fileToScan} is:{result.ResultCode}");

                if (!VerifyScannedFileHash(remoteDir, fileToScan, fileHash))
                {
                    vflag = true;
                    Log.Info($"Result of scanning file:{fileToScan} is remediated");
                }

                var rcode = vflag ? 1024 : result.ResultCode;
                retVal = new ProcessRunResult(rcode,
                $"{copyVal.Output}{Environment.NewLine}{result.Output}{Environment.NewLine}Scanning Exit Code: {rcode}",
                result.Errors);

                //commandLine = "/F /Q " + scanConfig.copyDir + "\\" + Path.GetFileName(fileToScan);
                //string scanexe = "C:\\Windows\\System32\\winrs.exe";



                if (result.ResultCode != 0 || vflag)
                {
                    Log.Error($"Error messages: {result.Errors} or expected pattern not matching, Output: {result.Output}");
                    return retVal;
                }

                commandLine = scanConfig.winrsArg+" \""+scanConfig.delExe+" "+scanConfig.delArg + " \""+ scanConfig.scanDir +"\\"+ fileDir
                    +"\" && rmdir \""+ scanConfig.scanDir + "\\" + fileDir + "\"\"";


                var resultDel = ProcRunner.Run("\"" + scanConfig.winrsExe + "\"", commandLine, SigningTimeoutMs);

                retVal = new ProcessRunResult(resultDel.ResultCode,
                    $"{retVal.Output}{Environment.NewLine}{resultDel.Output}{Environment.NewLine}rd Exit Code: {resultDel.ResultCode}",
                    resultDel.Errors);

                if (resultDel.ResultCode != 0 )
                {
                    Log.Error($"Error messages: {result.Errors} or expected pattern not matching, Output: {result.Output}");
                }

            }
            else
            {
                Log.Error($"Error messages: {result.Errors}, Output: {result.Output}");
                return copyVal;
            }
            

            return retVal;
        }

        public ProcessRunResult Scan(List<string> files, int requestid, string[] fileHashes)
        {
            string dirToScan = Path.GetDirectoryName( files[0]); 
            Log.Info($"scanning dir:{dirToScan}");
            string commandLine;
            ProcessRunResult retVal = null, copyVal = null;
            bool vflag = false;

            var remoteDir = Path.Combine(scanConfig.copyDir, requestid.ToString());
            RecreateDirectory(remoteDir);

            commandLine = BuildCopyCommandLine(dirToScan, remoteDir);

            Guard.ValueNotBlankOrNull(commandLine, "Signing tool (copy) is not properly configured, could not build a command line string.");


            var result = ProcRunner.Run("\"" + scanConfig.copyExe + "\"", commandLine, SigningTimeoutMs);

            Log.Info($"Result of copying dir:{dirToScan} is:{result.ResultCode}");


            copyVal = new ProcessRunResult(result.ResultCode,
                $"{result.Output}{Environment.NewLine}Copying Exit Code: {result.ResultCode}",
                result.Errors);

            if (result.ResultCode == 0)
            {
                // scanning result.
                commandLine = BuildScanDirCommandLine(remoteDir);

                Guard.ValueNotBlankOrNull(commandLine, "Signing tool (scan) is not properly configured, could not build a command line string.");

                //string scanexe = "C:\\Windows\\System32\\winrs.exe";
                //commandLine = "-r:win10 -u:myorg\\cwang -p:Welcome1 \"C:\\Program Files\\Windows Defender\\MpCmdRun.exe\" -Scan -ScanType 3 -File C:\\tmp\\"+Path.GetFileName(fileToScan);

                result = ProcRunner.Run("\"" + scanConfig.winrsExe + "\"", commandLine, SigningTimeoutMs);

                Log.Info($"Result of scanning Dir:{dirToScan} is:{result.ResultCode}");

                if (!VerifyScannedFileHashes(remoteDir, files, fileHashes))
                {
                    vflag = true;
                    Log.Info($"Result of scanning Dir:{dirToScan} is remediated");
                }

                var rcode = vflag ? 1024 : result.ResultCode;
                retVal = new ProcessRunResult(rcode,
                $"{copyVal.Output}{Environment.NewLine}{result.Output}{Environment.NewLine}Scanning Exit Code: {rcode}",
                result.Errors);

                //commandLine = "/F /Q " + scanConfig.copyDir + "\\" + Path.GetFileName(fileToScan);
                //string scanexe = "C:\\Windows\\System32\\winrs.exe";



                if (result.ResultCode != 0 || vflag)
                {
                    Log.Error($"Error messages: {result.Errors} or expected pattern not matching, Output: {result.Output}");
                    return retVal;
                }

                commandLine = scanConfig.winrsArg + " \"" + scanConfig.delExe + " " + scanConfig.delArg + " \"" + scanConfig.scanDir + "\\" + requestid
                    + "\" && rmdir \"" + scanConfig.scanDir + "\\" + requestid + "\"\"";


                var resultDel = ProcRunner.Run("\"" + scanConfig.winrsExe + "\"", commandLine, SigningTimeoutMs);

                retVal = new ProcessRunResult(resultDel.ResultCode,
                    $"{retVal.Output}{Environment.NewLine}{resultDel.Output}{Environment.NewLine}rd Exit Code: {resultDel.ResultCode}",
                    resultDel.Errors);

                if (resultDel.ResultCode != 0)
                {
                    Log.Error($"Error messages: {result.Errors} or expected pattern not matching, Output: {result.Output}");
                }

            }
            else
            {
                Log.Error($"Error messages: {result.Errors}, Output: {result.Output}");
                return copyVal;
            }


            return retVal;
        }


        /// <summary>
        /// Recreates the directory by recursively deleting it if it exists, and then creating it again.
        /// </summary>
        /// <param name="dirName">Name of the dir.</param>
        private static void RecreateDirectory(string dirName)
        {
            if (Directory.Exists(dirName))
            {
                Directory.Delete(dirName, true);
            }

            Directory.CreateDirectory(dirName);
        }

        /// <summary>
        /// Verify the scanned file hash
        /// 
        /// </summary>
        private bool VerifyScannedFileHash(string remoteDir, string fileToScan, string fileHash )
        {
            return (CodeSigningRequestManager.GetFileHash(remoteDir + "\\" + Path.GetFileName(fileToScan)) == fileHash);
        }

        private bool VerifyScannedFileHashes(string remoteDir, List<string> files, string[] fileHashes)
        {
            int i = 0;
            foreach (var file in files)
            {
                if (CodeSigningRequestManager.GetFileHash(remoteDir + "\\" + Path.GetFileName(file)) != fileHashes[i++])
                    return false;
            }
            return true;
        }

    }


    [XmlType]
    public class ScanConfig
    {
        [XmlElement]
        public string copyExe;
        [XmlElement]
        public string copyArg;
        [XmlElement]
        public string copyDir;
        [XmlElement]
        public string scanExe;
        [XmlElement]
        public string scanArg;
        [XmlElement]
        public string scanDir;
        [XmlElement]
        public string delExe;
        [XmlElement]
        public string delArg;
        [XmlElement]
        public string userHost;
        [XmlElement]
        public string sshExe;
        [XmlElement]
        public string scpExe;
        [XmlElement]
        public string winrsExe;
        [XmlElement]
        public string winrsArg;

    }

  



}
