using System;
using CSG.AppServer.Db;
using CSG.AppServer.Managers;
using CSG.AppServer.Security;
using Util.Config;
using Microsoft.Practices.EnterpriseLibrary.Logging;

namespace CSG.AppServer
{
    public static class AppServerInitializer
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // *********************************************************************************
        #region Public / Core Methods
        // *********************************************************************************

        public static void InitializeLoggingEnvironmentVariables()
        {
            string applicationName = DynamicConfigurationManager.AppSettings["ApplicationName"];
            string appEnvironment = DynamicConfigurationManager.Environment;

            Environment.SetEnvironmentVariable("ApplicationName", applicationName);
            Environment.SetEnvironmentVariable("AppEnvironment", appEnvironment);

            Logger.SetLogWriter(new LogWriterFactory().Create(), false);

            log.Info("ApplicationName: " + applicationName);
            log.Info("AppEnvironment: " + appEnvironment);
        }

        public static void Initialize()
        {
            try
            {
                AppServerContext.Initialize();

                log.InfoFormat("Running AppServerInitializer.Initialize().  Server: {0}, Environment: {1}", AppServerContext.AppServerMachineName, AppServerContext.AppEnvironment);

                ESPDbInitializer dbInitializer = new ESPDbInitializer();
                if (dbInitializer.CheckIfInitializeRequired())
                {
                    dbInitializer.InitializeDb();
                }

                InitializeCachedData();
                PopulateSystemSettings();

                UserManagerBase userManager = UserManagerFactory.CreateUserManager();

                userManager.SyncAdminGroupsFromAppConfig();

                log.Info("AppServerInitializer.Initialize() Complete");

            }
            catch (Exception e)
            {
                string message = "An exception occurred during AppServerInitializer.Initialize().";
                log.Fatal(message, e);
                throw new Exception(message, e);
            }
        }

        public static void InitializeAPI()
        {
            try
            {
                AppServerContext.Initialize();

                log.InfoFormat("Running AppServerInitializer.Initialize().  Server: {0}, Environment: {1}", AppServerContext.AppServerMachineName, AppServerContext.AppEnvironment);

                ESPDbInitializer dbInitializer = new ESPDbInitializer();
                if (dbInitializer.CheckIfInitializeRequired())
                {
                    dbInitializer.InitializeDb();
                }

                InitializeCachedData();
                PopulateSystemSettings();

                UserManagerBase userManager = UserManagerFactory.CreateUserManager();

                log.Info("AppServerInitializer.InitializeAPI() Complete");

            }
            catch (Exception e)
            {
                string message = "An exception occurred during AppServerInitializer.Initialize().";
                log.Fatal(message, e);
                throw new Exception(message, e);
            }
        }
        #endregion

        // *********************************************************************************
        #region Editable Methods - for App Specific Logic
        // *********************************************************************************

        public static void InitializeCachedData()
        {
            log.Info("   Initializing Cached Data");
            //SecuredAssetManager.ClearCache();
        }

        private static void PopulateSystemSettings()
        {
            log.Info("Populate System Settings");

            SystemSettingsManager ssm = SystemSettingsManager.Current;

            ssm.GetOrCreateSystemSetting(Constants.SettingNames.EmailServer,
                                            description: "The mail server to use when sending emails.",
                                            dataType: SystemSettingDataType.String,
                                            category: "Email Settings",
                                            stringValue: "smtp.gmail.com");

            ssm.GetOrCreateSystemSetting(Constants.SettingNames.EmailServerPort,
                                            description: "The mail server port to use when sending emails.",
                                            dataType: SystemSettingDataType.Integer,
                                            category: "Email Settings",
                                            integerValue: 587);

            ssm.GetOrCreateSystemSetting(Constants.SettingNames.EmailServerUserId,
                                            description: "The user id to use when when sending emails.",
                                            dataType: SystemSettingDataType.String,
                                            category: "Email Settings",
                                            stringValue: "praxtest@gmail.com");

            ssm.GetOrCreateSystemSetting(Constants.SettingNames.EmailServerPassword,
                                            description: "The password to use when when sending emails.",
                                            dataType: SystemSettingDataType.Password,
                                            category: "Email Settings",
                                            passwordValue: "CodesignTest01");

            ssm.GetOrCreateSystemSetting(Constants.SettingNames.EmailServerSSL,
                                            description: "Use SSL when sending emails.",
                                            dataType: SystemSettingDataType.Boolean,
                                            category: "Email Settings",
                                            booleanValue: true);

            ssm.GetOrCreateSystemSetting(Constants.SettingNames.TeamEmailAddress,
                                            description: "The primary contact address for the team support the application.",
                                            dataType: SystemSettingDataType.String,
                                            stringValue: "ESP@nCipher.com");

            ssm.GetOrCreateSystemSetting(Constants.SettingNames.Copyright,
                                            description: "The copyright message to display at the bottom of the page.",
                                            dataType: SystemSettingDataType.String);

            ssm.GetOrCreateSystemSetting(Constants.SettingNames.CacheMinutes,
                                            description: "CacheMinutes",
                                            dataType: SystemSettingDataType.Integer,
                                            integerValue: 2);

            ssm.GetOrCreateSystemSetting(Constants.SettingNames.UserTimeout,
                                            description: "UserTimeout",
                                            dataType: SystemSettingDataType.Integer,
                                            integerValue: 15);

            ssm.GetOrCreateSystemSetting(Constants.SettingNames.DaysOfRecentRequests,
                                            description: "The number of days of requests to be displayed in the Dashboard under the 'Recent Requests' section.",
                                            dataType: SystemSettingDataType.Integer,
                                            integerValue: 30);

            ssm.GetOrCreateSystemSetting(Constants.SettingNames.ApplicationOnlineStatus,
                                            description: "ApplicationOnlineStatus",
                                            dataType: SystemSettingDataType.String,
                                            stringValue: "Online");

            ssm.GetOrCreateSystemSetting(Constants.SettingNames.SystemEventLogSourceName,
                                            description: "The name of the System Event Log to be written to by this system.",
                                            dataType: SystemSettingDataType.String,
                                            stringValue: "nCipher Code Signing System");

            ssm.GetOrCreateSystemSetting(Constants.SettingNames.OAuthServerURL,
                                            description: "The URL of the OAuth validation server.",
                                            dataType: SystemSettingDataType.String,
                                            stringValue: "https://<replace with value>");
            ssm.GetOrCreateSystemSetting(Constants.SettingNames.OAuthUIDHeaderValue,
                                            description: "The URL of the OAuth validation server.",
                                            dataType: SystemSettingDataType.String,
                                            stringValue: "client_id");
            ssm.GetOrCreateSystemSetting(Constants.SettingNames.GESProfileRoleList,
                                            description: "The GES Roles used to defined Signing Profiles.",
                                            dataType: SystemSettingDataType.String,
                                            stringValue: "");


        // ***********************************
        // Request Processing Settings
        // ***********************************
        ssm.GetOrCreateSystemSetting(Constants.SettingNames.IPAddressRestrictions,
                                category: "Request Processing",
                                description: "Restrict API access to certain IP addresses.  Disabled if blank.",
                                dataType: SystemSettingDataType.String,
                                stringValue: @"");

            ssm.GetOrCreateSystemSetting(Constants.SettingNames.TempStorageLocation,
                                category: "Request Processing",
                                description: "The temporary location to store files for download.  The files in this location will be deleted periodically.  The account the application is running under must have full read / write access to the location.",
                                dataType: SystemSettingDataType.String,
                                stringValue: @"C:\Development\vs\Projects\nCipherESP\Development\Dev\src\server\CSG.WebSite\temp");

            ssm.GetOrCreateSystemSetting(Constants.SettingNames.ReminderNotificationFrequencyInHours,
                                category: "Request Processing",
                                description: "The number of hours after a request is submitted and the interval after that for notifications to be sent that the request needs to be approved.",
                                dataType: SystemSettingDataType.Integer,
                                integerValue: 24);

            ssm.GetOrCreateSystemSetting(Constants.SettingNames.RequestTimeoutInHours,
                    category: "Request Processing",
                    description: "The number of hours after a request is submitted that the request should be marked as having timed out.",
                    dataType: SystemSettingDataType.Integer,
                    integerValue: 72);

            ssm.GetOrCreateSystemSetting(Constants.SettingNames.MaxMinutesToKeepTempFiles,
                    description: "The number of minutes to wait before deleting files moved to the temporary location for download.",
                    dataType: SystemSettingDataType.Integer,
                    integerValue: 15);

            ssm.GetOrCreateSystemSetting(Constants.SettingNames.CodeSigningTimeoutMs,
                   category: "Request Processing",
                   description: "Timeout for external signing process.",
                   dataType: SystemSettingDataType.Integer,
                   integerValue: 10000);


            // ***********************************
            // Task related settings
            // ***********************************

            ssm.GetOrCreateSystemSetting(Constants.SettingNames.TaskManagerRunFrequency,
                                category: "Task Manager",
                                description: "The frequency in seconds of how often the task manager should run to check for new and completed code signing requests.",
                                dataType: SystemSettingDataType.Integer,
                                integerValue: 60);

            ssm.GetOrCreateSystemSetting(Constants.SettingNames.Task_SyncWithActiveDirectory_Interval,
                                category: "Task Manager",
                                description: "The frequency in minutes of how often the task manager should run the SyncWithActiveDirectory Task.",
                                dataType: SystemSettingDataType.Integer,
                                integerValue: 12 * 60);

            ssm.GetOrCreateSystemSetting(Constants.SettingNames.Task_ProcessSubmittedRequestsTask_Interval,
                                category: "Task Manager",
                                description: "The frequency in minutes of how often the task manager should run the ProcessSubmittedRequestsTask Task.",
                                dataType: SystemSettingDataType.Integer,
                                integerValue: 60);

        }

        #endregion
    }
}
