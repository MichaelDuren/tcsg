﻿using System;
using System.IO;
using System.Linq;
using CSG.AppServer.Db;
using CSG.AppServer.Entities;
using CSG.AppServer.Security;
using Util;
using System.Collections.Generic;

namespace CSG.AppServer.Managers
{
    public class CodeSigningTypeManager
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly Type ThisDeclaringType = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;

        private ESPDbDomainService domainService = new ESPDbDomainService();
        private Gatekeeper gatekeeper = new Gatekeeper();

        // *********************************************************************************
        #region Constructor and related
        // *********************************************************************************

        public CodeSigningTypeManager() { }

        #endregion

        // *********************************************************************************
        #region Methods
        // *********************************************************************************


        public CodeSigningType SaveCodeSigningType(CodeSigningType type, string developerGroupIds, string approverGroupIds)
        {
            CodeSigningType savedType;
            if (type.Id < 1)
            {
                savedType = InsertCodeSigningType(type, developerGroupIds, approverGroupIds);
            }
            else
            {
                savedType = UpdateCodeSigningType(type, developerGroupIds, approverGroupIds);
            }

            return savedType;

        }

        /// <summary>
        /// </summary>
        private void UpdateGroupsForUpdateCodeSigningType(int codeSignTypeId, List<String> developerGroupList, List<String> approverGroupList)
        {

            // TODO: Verify the group type here

            //Add new approver groups
            var groupsForType = domainService.GetCodeSigningTypeGroups(null).Where(m => m.CodeSigningTypeId == codeSignTypeId).Select(m => m.GroupId).ToList();


            foreach (var groupId in developerGroupList)
            {
                if (!String.IsNullOrWhiteSpace(groupId))
                {
                    int groupIdInt = System.Convert.ToInt32(groupId);

                    if (groupsForType.Contains(groupIdInt))
                    {
                        continue;
                    }
                    else
                    {
                        CodeSigningTypeGroup cstg = new CodeSigningTypeGroup { GroupId = groupIdInt, CodeSigningTypeId = codeSignTypeId, IsApprover = false};
                        domainService.InsertCodeSigningTypeGroup(cstg);
                    }
                }
            }
            foreach (var groupId in approverGroupList)
            {
                if (!String.IsNullOrWhiteSpace(groupId))
                {
                    int groupIdInt = System.Convert.ToInt32(groupId);

                    if (groupsForType.Contains(groupIdInt))
                    {
                        continue;
                    }
                    else
                    {

                        CodeSigningTypeGroup cstg = new CodeSigningTypeGroup { GroupId = groupIdInt, CodeSigningTypeId = codeSignTypeId, IsApprover = true };
                        domainService.InsertCodeSigningTypeGroup(cstg);
                    }
                }
            }

            //Remove missing groups
            foreach (var groupId in groupsForType)
            {
                if (developerGroupList.Contains(groupId.ToString()) || approverGroupList.Contains(groupId.ToString()))
                {
                    continue;
                }
                else
                {
                    CodeSigningTypeGroup cstg = domainService.GetCodeSigningTypeGroups(null)
                                                        .Where(m => m.GroupId == groupId && m.CodeSigningTypeId == codeSignTypeId)
                                                        .FirstOrDefault();
                    if (cstg != null)
                    {
                        domainService.DeleteCodeSigningTypeGroup(cstg);
                    }
                }
            }
            domainService.SaveChanges();
        }

        /// <summary>
        /// Verifies that the server can access the staging and signed directories and if so, saves the changes
        /// </summary>
        private CodeSigningType UpdateCodeSigningType(CodeSigningType updatedType, string developerGroupIds, string approverGroupIds)
        {
            // remove any signing entitlements for this siging type.
            var currentTypeSigningEntitlements = domainService.GetUserProfileCodeSigningTypes().
                                                               Where(upcst => upcst.UserSigningTypeRole == (int)UserActions.SubmitRequests && 
                                                                              upcst.CodeSigningTypeId == updatedType.Id).
                                                               ToList();

            foreach (var entry in currentTypeSigningEntitlements)
            {
                domainService.DeleteUserProfileCodeSigningType(entry);
            }
             
            // Need to get the full object and then update just the properties that could have changed.
            CodeSigningType type = domainService.GetCodeSigningTypeById(updatedType.Id);
            type.Name = updatedType.Name;
            type.StagingDirectory = updatedType.StagingDirectory;
            type.SignedDirectory = updatedType.SignedDirectory;

            // save approval requirements
            type.AllowManagerApproval = updatedType.AllowManagerApproval;
            type.RequireManagerApproval = updatedType.RequireManagerApproval;
            type.AllowApprover1Approval = updatedType.AllowApprover1Approval;
            type.RequireApprover1Approval = updatedType.RequireApprover1Approval;
            type.AllowApprover2Approval = updatedType.AllowApprover2Approval;
            type.RequireApprover2Approval = updatedType.RequireApprover2Approval;
            type.RequireSecondaryApproval = updatedType.RequireSecondaryApproval;

            type.RequestTimestamp = updatedType.RequestTimestamp;

            type.MetadataField1Label = updatedType.MetadataField1Label;
            type.MetadataField2Label = updatedType.MetadataField2Label;

            type.AdditionalArguments = updatedType.AdditionalArguments;

            type.Description = updatedType.Description;

            type.SigningToolId = updatedType.SigningToolId;

            type.RawSign = updatedType.RawSign;

            type.ApplyToAll = updatedType.ApplyToAll;
            type.IsAutoSign = updatedType.IsAutoSign;

            // Signing/Validation executables and if validation is enabled
            type.DoVerification = updatedType.DoVerification;

            type.IsDeleted = updatedType.IsDeleted;
            type.RequireSecondaryApproval = updatedType.RequireSecondaryApproval;
            type.ValidFileExtensions = updatedType.ValidFileExtensions;

            type.SigningCertificateId = updatedType.SigningCertificateId;

            PerformValidations(type);

            domainService.UpdateCodeSigningType(type);
            domainService.SaveChanges();

            string[] developerGroupList = new string[0];

            if (!String.IsNullOrWhiteSpace(developerGroupIds))
            {
                developerGroupList = developerGroupIds.Split(',');
            }

            string[] approverGroupList = new string[0];

            if (!String.IsNullOrWhiteSpace(approverGroupIds))
            {
                approverGroupList = approverGroupIds.Split(',');
            }

            //Add/remove groups
            UpdateGroupsForUpdateCodeSigningType(type.Id, developerGroupList.ToList(), approverGroupList.ToList());

            // Save the changes
            domainService.SaveChanges();

            return type;
        }

        /// <summary>
        /// Verifies that the server can access the staging and signed directories and if so, creates the new type
        /// </summary>
        private CodeSigningType InsertCodeSigningType(CodeSigningType type, string developerGroupIds, string approverGroupIds)
        {
            type.Guid = Guid.NewGuid().ToString();
            PerformValidations(type);

            domainService.InsertCodeSigningType(type);
            domainService.SaveChanges();

            //Add groups
            if (!String.IsNullOrWhiteSpace(developerGroupIds))
            {
                string[] groupListReceived = developerGroupIds.Split(',');

                foreach (var groupId in groupListReceived)
                {
                    if (!String.IsNullOrWhiteSpace(groupId))
                    {
                        CodeSigningTypeGroup cstg = new CodeSigningTypeGroup { GroupId = System.Convert.ToInt32(groupId), CodeSigningTypeId = type.Id, IsApprover = false };
                        domainService.InsertCodeSigningTypeGroup(cstg);
                    }
                }
                domainService.SaveChanges();
            }
            if (!String.IsNullOrWhiteSpace(approverGroupIds))
            {
                string[] groupListReceived = approverGroupIds.Split(',');

                foreach (var groupId in groupListReceived)
                {
                    if (!String.IsNullOrWhiteSpace(groupId))
                    {
                        CodeSigningTypeGroup cstg = new CodeSigningTypeGroup { GroupId = System.Convert.ToInt32(groupId), CodeSigningTypeId = type.Id, IsApprover = true };
                        domainService.InsertCodeSigningTypeGroup(cstg);
                    }
                }
                domainService.SaveChanges();
            }

            return type;
        }

        private void PerformValidations(CodeSigningType type)
        {
            Guard.ValueNotBlankOrNull(type.Name, "The name cannot be blank.");

            CodeSigningType existingType = domainService.GetCodeSigningTypes(null).Where(t => t.Name == type.Name).FirstOrDefault();

            if (existingType != null)
            {
                if (type.Id == 0)
                {
                    throw new TypeExistsException(String.Format("There is already a code signing type with the same name.  Please update the existing type."), existingType.Id);
                }
            }

        }

        /// <summary>
        /// If filePath is not null or empty, it will verfiy if file exists. 
        /// If failOnEmpty is true and filePath is null or empty, validation will fail.
        /// Throws FileDoesNotExist exception in case if it does not.
        /// </summary>
        /// <param name="filePath"></param>
        private void VerifyFileExists(string filePath, bool failOnEmpty = true)
        {
            var isNullOrEmpty = string.IsNullOrEmpty(filePath);

            if ((failOnEmpty && isNullOrEmpty) || (!isNullOrEmpty && !File.Exists(filePath)) )
            {
                throw new FileDoesNotExist($"File {filePath} does not exist.");
            }
        }

        #endregion
    }

    public class DirectoryNotFoundException : Exception
    {
        public DirectoryNotFoundException(string message) : base(message) { }
    }

    public class AccessDeniedException : Exception
    {
        public AccessDeniedException(string message) : base(message) { }
    }

    public class DirectoryNotAccessibleException : Exception
    {
        public DirectoryNotAccessibleException(string message) : base(message) { }
    }

    public class FileDoesNotExist : Exception
    {
        public FileDoesNotExist(string message) : base(message) { }
    }

    public class TypeExistsException : Exception
    {
        public int ExistingTypeId { get; set; }
        public TypeExistsException(string message, int existingTypeId)
            : base(message)
        {
            ExistingTypeId = existingTypeId;
        }

        public TypeExistsException(string message) : base(message) { }
    }
}
