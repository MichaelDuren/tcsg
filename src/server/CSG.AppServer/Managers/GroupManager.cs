﻿using System;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using CSG.AppServer.Db;
using CSG.AppServer.Entities;
using CSG.AppServer.Security;
using Util;

namespace CSG.AppServer.Managers
{
    public class GroupManager
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private ESPDbDomainService domainService = new ESPDbDomainService();
        private Gatekeeper gatekeeper = new Gatekeeper();

        // *********************************************************************************
        #region Constructor and related
        // *********************************************************************************

        public GroupManager() { }

        #endregion

        // *********************************************************************************
        #region Methods
        // *********************************************************************************

        public Group GetGroup(int groupId)
        {
            return domainService.GetGroupById(groupId);
        }

        /// <summary>
        /// Inserts/updates the group depending on whether it is a new or existing group.
        /// </summary>
        public Group SaveGroup(int groupId, string domain, string name, bool isDeveloperGroup, bool isAppAdminGroup, string selectedCodeSigningTypeIds, bool isDeleted)
        {
            if (groupId < 1)
            {
                Group group = new Group();
                group.Domain = domain;
                group.Name = name;
                group.IsDeveloperGroup = isDeveloperGroup;
                group.IsAppAdminGroup = isAppAdminGroup;
                group.IsDeleted = false;

                return InsertGroup(group, selectedCodeSigningTypeIds);
            }
            else
            {
                Group group = domainService.GetGroupById(groupId);
                group.Domain = domain;
                group.Name = name;
                group.IsDeveloperGroup = isDeveloperGroup;
                group.IsAppAdminGroup = isAppAdminGroup;
                group.IsDeleted = isDeleted;

                return UpdateGroup(group, selectedCodeSigningTypeIds);
            }
        }

        /// <summary>
        /// Inserts/updates the group depending on whether it is a new or existing group.
        /// </summary>
        public Group SaveGroup(Group group, string selectedCodeSigningTypeIds)
        {
            if (group.Id < 1)
            {
                return InsertGroup(group, selectedCodeSigningTypeIds);
            }
            else
            {
                return UpdateGroup(group, selectedCodeSigningTypeIds);
            }
        }

        /// <summary>
        /// Ensures that the group is valid in Active Directory and if so updates the group and the users for that group.
        /// </summary>
        private Group UpdateGroup(Group updatedGroup, string codeSigningTypeIds)
        {
            // Need to get the full object and then update just the properties that could have changed.
            Group group = domainService.GetGroupById(updatedGroup.Id);
            group.Domain = updatedGroup.Domain;
            group.Name = updatedGroup.Name;
            group.IsDeveloperGroup = updatedGroup.IsDeveloperGroup;
            group.IsAppAdminGroup = updatedGroup.IsAppAdminGroup;
            group.IsApproverGroup = updatedGroup.IsApproverGroup;
            group.IsDeleted = updatedGroup.IsDeleted;

            PerformValidations(group);

            domainService.UpdateGroup(group);
            domainService.SaveChanges();

            //Add new groups
            var codeSingingTypesForGroup = domainService.GetCodeSigningTypeGroups(null).Where(m => m.GroupId == group.Id).Select(m => m.CodeSigningTypeId).ToList();
            string[] codeSigningTypeListReceived = codeSigningTypeIds.Split(',');

            foreach (var typeId in codeSigningTypeListReceived)
            {
                if (!String.IsNullOrWhiteSpace(typeId))
                {
                    int typeIdInt = System.Convert.ToInt32(typeId);

                    if (codeSingingTypesForGroup.Contains(typeIdInt))
                    {
                        continue;
                    }
                    else
                    {
                        CodeSigningTypeGroup cstg = new CodeSigningTypeGroup { GroupId = group.Id, CodeSigningTypeId = typeIdInt };
                        domainService.InsertCodeSigningTypeGroup(cstg);
                    }
                }
            }
            domainService.SaveChanges();

            //Remove missing groups
            foreach (var typeId in codeSingingTypesForGroup)
            {
                if (codeSigningTypeListReceived.Contains(typeId.ToString()))
                {
                    continue;
                }
                else
                {
                    CodeSigningTypeGroup cstg = domainService.GetCodeSigningTypeGroups(null)
                                                        .Where(m => m.GroupId == group.Id && m.CodeSigningTypeId == typeId)
                                                        .FirstOrDefault();
                    if (cstg != null)
                    {
                        domainService.DeleteCodeSigningTypeGroup(cstg);
                    }
                }
            }
            domainService.SaveChanges();

            return group;
        }

        /// <summary>
        /// Ensures that the group is valid in Active Directory and if so inserts the group and the users for that group.
        /// </summary>
        private Group InsertGroup(Group group, string codeSigningTypeIds)
        {
            PerformValidations(group);

            domainService.InsertGroup(group);
            domainService.SaveChanges();

            //Add groups
            string[] codeSigingTypeListReceived = codeSigningTypeIds.Split(',');

            foreach (var codeSigningTypeId in codeSigingTypeListReceived)
            {
                if (!String.IsNullOrWhiteSpace(codeSigningTypeId))
                {
                    CodeSigningTypeGroup cstg = new CodeSigningTypeGroup { GroupId = group.Id, CodeSigningTypeId = System.Convert.ToInt32(codeSigningTypeId) };
                    domainService.InsertCodeSigningTypeGroup(cstg);
                }
            }
            domainService.SaveChanges();

            return group;
        }

        private void PerformValidations(Group group)
        {
            Guard.ValueNotBlankOrNull(group.Name, "The name cannot be blank.");
            Guard.ValueNotBlankOrNull(group.Domain, "The domain cannot be blank.");

            Group existingGroup = domainService.GetGroups(null).Where(g => g.Domain == group.Domain && g.Name == group.Name).FirstOrDefault();

            if (existingGroup != null)
            {
                if (group.Id == 0)
                {
                    throw new GroupExistsException(String.Format("There is already a group with the same domain and name.  Please update the existing group."), existingGroup.Id);
                }
            }

            if (!AppServerContext.UseTestUserManager)
            {
                using (ActiveDirectoryManager adManager = new ActiveDirectoryManager())
                {
                    GroupPrincipal gp = adManager.GetGroupPrincipal(group.Domain, group.Name);

                    if (gp == null)
                    {
                        throw new GroupNotFoundException(String.Format("The group {0} could not be found in the domain {1}.", group.Name, group.Domain));
                    }

                    group.ActiveDirectorySId = gp.Sid.Value;
                    group.Description = gp.Description;
                }
            }
        }

        #endregion
    }

    public class GroupNotFoundException : Exception
    {
        public GroupNotFoundException(string message) : base(message) { }
    }

    public class GroupExistsException : Exception
    {
        public int ExistingGroupId { get; set; }
        public GroupExistsException(string message, int existingGroupId)
            : base(message)
        {
            ExistingGroupId = existingGroupId;
        }
    }

}
