﻿using System;
using System.IO;
using System.Linq;
using CSG.AppServer.Db;
using CSG.AppServer.Entities;
using CSG.AppServer.Security;
using Util;

namespace CSG.AppServer.Managers
{
    public class SigningToolManager
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly Type ThisDeclaringType = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;

        private ESPDbDomainService domainService = new ESPDbDomainService();
        private Gatekeeper gatekeeper = new Gatekeeper();

        // *********************************************************************************
        #region Constructor and related
        // *********************************************************************************

        public SigningToolManager() { }

        #endregion

        // *********************************************************************************
        #region Methods
        // *********************************************************************************

        public CodeSigningTool SaveCodeSigningTool(CodeSigningTool tool)
        {
            if (tool.Id < 1)
            {
                return InsertCodeSigningTool(tool);
            }
            else
            {
                return UpdateCodeSigningTool(tool);
            }
        }

        /// <summary>
        /// </summary>
        private CodeSigningTool UpdateCodeSigningTool(CodeSigningTool updatedTool)
        {

            // Need to get the full object and then update just the properties that could have changed.
            CodeSigningTool tool = domainService.GetCodeSigningToolById(updatedTool.Id);
            tool.Name = updatedTool.Name;
            tool.SigningArgumentFormat = updatedTool.SigningArgumentFormat;
            tool.CommonSigningArguments = updatedTool.CommonSigningArguments;
            tool.CommonVerificationArguments = updatedTool.CommonVerificationArguments;
            tool.TimestampServer = updatedTool.TimestampServer;
            tool.Offline = updatedTool.Offline;

            domainService.UpdateCodeSigningTool(tool);
            domainService.SaveChanges();

            return tool;
        }

        /// <summary>
        /// Verifies the tool input and inserts record in the database
        /// </summary>
        private CodeSigningTool InsertCodeSigningTool(CodeSigningTool tool)
        {
            PerformValidations(tool);

            domainService.InsertCodeSigningTool(tool);
            domainService.SaveChanges();

            return tool;
        }

        private void PerformValidations(CodeSigningTool tool)
        {
            Guard.ValueNotBlankOrNull(tool.Name, "The name cannot be blank.");

            CodeSigningTool existingType = domainService.GetCodeSigningTools(null).Where(t => t.Name == tool.Name && t.IsDeleted == false).FirstOrDefault();

            if (existingType != null)
            {
                throw new ToolExistsException(String.Format("There is already a signing tool with the same name.  Please update the existing tool."), 
                                              tool.Name);
            }
        }

        /// <summary>
        /// If filePath is not null or empty, it will verfiy if file exists. 
        /// If failOnEmpty is true and filePath is null or empty, validation will fail.
        /// Throws FileDoesNotExist exception in case if it does not.
        /// </summary>
        /// <param name="filePath"></param>
        private void VerifyFileExists(string filePath, bool failOnEmpty = true)
        {
            var isNullOrEmpty = string.IsNullOrEmpty(filePath);

            if ((failOnEmpty && isNullOrEmpty) || (!isNullOrEmpty && !File.Exists(filePath)) )
            {
                throw new FileDoesNotExist($"File {filePath} does not exist.");
            }
        }

        #endregion
    }

    public class ToolExistsException : Exception
    {
        public string ExistingToolName { get; set; }
        public ToolExistsException(string message, string existingToolName)
            : base(message)
        {
            ExistingToolName = existingToolName;
        }

        public ToolExistsException(string message) : base(message) { }
    }
}
