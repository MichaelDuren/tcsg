﻿using System;
using System.IO;
using System.Linq;
using CSG.AppServer.Db;
using CSG.AppServer.Entities;
using CSG.AppServer.Security;
using Util;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;

namespace CSG.AppServer.Managers
{
    public class CertificateManager
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private ESPDbDomainService domainService = new ESPDbDomainService();
        private Gatekeeper gatekeeper = new Gatekeeper();


        // *********************************************************************************
        #region Constructor and related
        // *********************************************************************************

        public CertificateManager() { }

        #endregion

        public static List<KeyValuePair<string, string>> ParseDistinguishedName(string input)
        {
            int i = 0;
            int a = 0;
            int v = 0;
            var attribute = new char[50];
            var value = new char[200];
            var inAttribute = true;
            string attributeString, valueString;
            var names = new List<KeyValuePair<string, string>>();
            char[] trimChars = { ' ' };

            while (i < input.Length)
            {
                char ch = input[i++];
                switch (ch)
                {
                    case '\\':
                        value[v++] = ch;
                        value[v++] = input[i++];
                        break;
                    case '=':
                        inAttribute = false;
                        break;
                    case ',':
                    case ';':
                        inAttribute = true;
                        attributeString = new string(attribute).Substring(0, a);
                        valueString = new string(value).Substring(0, v);

                        // remove leading and trailing white spaces from the strings
                        attributeString = attributeString.Trim(trimChars);
                        valueString = valueString.Trim(trimChars);
                        names.Add(new KeyValuePair<string, string>(attributeString, valueString));
                        a = v = 0;
                        break;
                    default:
                        if (inAttribute)
                        {
                            attribute[a++] = ch;
                        }
                        else
                        {
                            value[v++] = ch;
                        }
                        break;
                }
            }

            attributeString = new string(attribute).Substring(0, a);
            valueString = new string(value).Substring(0, v);

            // remove leading and trailing white spaces from the strings
            attributeString = attributeString.Trim(trimChars);
            valueString = valueString.Trim(trimChars);
            names.Add(new KeyValuePair<string, string>(attributeString, valueString));
            return names;
        }
        // *********************************************************************************
        #region Methods
        // *********************************************************************************

        /// <summary>
        /// </summary>
        public Certificate SaveCertificate(Certificate certToSave)
        {
            if (certToSave.Id < 1)
            {
                return InsertCertificate(certToSave);
            }
            else
            {
                return UpdateCertificate(certToSave);
            }

        }

        public List<Certificate> GetCertificates(bool includeExpired = false)
        {
            List<Certificate> certs = new List<Certificate>();

            // TODO: TEMPORARY CODE TO UPDATE TEST DATABASE
            foreach (var certInDb in domainService.GetCertificates())
            {
                if (certInDb.NotAfter == null || certInDb.NotBefore == null)
                {
                    var decodedCertInfDb = new X509Certificate2(Convert.FromBase64String(certInDb.MimeEncoded));
                    certInDb.NotBefore = decodedCertInfDb.NotBefore;
                    certInDb.NotAfter = decodedCertInfDb.NotAfter;
                    domainService.UpdateCertificate(certInDb);
                }
            }
            domainService.SaveChanges();

            if (includeExpired)
            {
                certs = domainService.GetCertificates().ToList();
            }
            else
            {
                certs = domainService.GetCertificates().Where(crt => DateTime.Now < crt.NotAfter && DateTime.Now > crt.NotBefore)
                                                       .ToList();
            }

            return certs;
        }

        public Certificate GetCertificateById(int certId)
        {
            return domainService.GetCertificateById(certId);
        }

        private void PerformValidations(Certificate cert)
        {
            Guard.ValueNotBlankOrNull(cert.Thumbprint, "The certificate thumbprint cannot be blank.");
            Certificate existingCert = domainService.GetCertificates(null).Where(c => c.Thumbprint == cert.Thumbprint).FirstOrDefault();

            if (existingCert != null)
            {
                throw new CertificateExistsException(String.Format("There is already a certificate with this thumbprint registered."), existingCert.Thumbprint);
            }
        }

        /// <summary>
        /// </summary>
        private Certificate InsertCertificate(Certificate certToSave)
        {
            PerformValidations(certToSave);

            domainService.InsertCertificate(certToSave);
            domainService.SaveChanges();

            return certToSave;
        }

        /// <summary>
        /// </summary>
        private Certificate UpdateCertificate(Certificate certToSave)
        {

            domainService.UpdateCertificate(certToSave);
            domainService.SaveChanges();

            return certToSave;
        }


        public void CheckCertificates(TimeSpan internval)
        {
            EmailManager emailManager = null;
            try
            {
                emailManager = new EmailManager();
            }
            catch (Exception e)
            {
                log.Error("An error occurred sending notification email.", e);
                // DO NOT throw e;
            }

            List<Certificate> certs=GetCertificates();


            foreach (Certificate cert in certs)
            {
                // if cert within internval(15) days expires, the emaill will be sent
                if ( ((System.DateTime)cert.NotAfter).Subtract(System.DateTime.Now).CompareTo(internval) < 0 )
                {
                    emailManager.SendCertificateNotification(cert);
                }

                // Otherwise do nothing
            }

        }

        #endregion
    }

    public class CertificateExistsException : Exception
    {


        public String ExistingThumbprint { get; set; }
        public CertificateExistsException(string message, string existingThumbprint)
            : base(message)
        {
            ExistingThumbprint = existingThumbprint;
        }

        public CertificateExistsException(string message) : base(message) { }
    }
}
