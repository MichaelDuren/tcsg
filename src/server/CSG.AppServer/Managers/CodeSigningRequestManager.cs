﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using CSG.AppServer.Db;
using CSG.AppServer.Entities;
using CSG.AppServer.Security;
using CSG.AppServer.Signers;
using Util;
using Util.Config;
using System.Security.Cryptography;
using System.Threading;
using System.Threading.Tasks;

namespace CSG.AppServer.Managers
{
    public class CodeSigningRequestManager
    {
        private readonly object pLock = new object();
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly Type ThisDeclaringType = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;

        private ESPDbDomainService _domainService = null;
        private Gatekeeper _gatekeeper = null;

        private readonly CodeSigner codeSigner = new CodeSigner() {ProcRunner = new ProcessRunner()};
//        private readonly AntivirusScanner scanner = new AntivirusScanner() { ProcRunner = new ProcessRunner() };
//        private readonly AntivirusScanner scanner2 = new AntivirusScanner(true) { ProcRunner = new ProcessRunner() };
//        private readonly AntivirusLinuxScanner linuxScanner = new AntivirusLinuxScanner() { ProcRunner = new ProcessRunner() };
 //       private readonly AntivirusLinuxScanner linuxScanner2 = new AntivirusLinuxScanner(true) { ProcRunner = new ProcessRunner() };

        // *********************************************************************************
        #region Constructor and related
        // *********************************************************************************

        // Constructor for session managed connections
        public CodeSigningRequestManager()
        {
            _gatekeeper = new Gatekeeper();
            _domainService = new ESPDbDomainService();
        }
        // Constructor for API connections
        public CodeSigningRequestManager(Gatekeeper gatekeeper)
        {
            _gatekeeper = gatekeeper;
            _domainService = new ESPDbDomainService(_gatekeeper);
        }
        #endregion

        // ********************************************************************************
        #region Methods to support the UI
        // ********************************************************************************

        /// <summary>
        /// Generates an MD5 hash for the file received
        /// </summary>
        public static string GetFileHash(string fileName)
        {
            using (FileStream stream = File.OpenRead(fileName))
            {
                SHA256Managed sha = new SHA256Managed();
                byte[] hash = sha.ComputeHash(stream);
                return BitConverter.ToString(hash).Replace("-", String.Empty);
            }
        }
        /// <summary>
        /// Generates an SHA256 hash for the file received
        /// </summary>
        public static string GetFileHashSha256(string fileName)
        {
            using (FileStream stream = File.OpenRead(fileName))
            {
                SHA256Managed sha = new SHA256Managed();
                byte[] hash = sha.ComputeHash(stream);
                return BitConverter.ToString(hash).Replace("-", String.Empty);
            }
        }


        /// <summary>
        /// Returns an IQueryable for the list of code signing requests that are pending for the development teams that the 
        /// user is member of or for the approver groups with pending requests
        /// </summary>
        public IQueryable<CodeSigningRequest> GetPendingCodeSigningRequestsForUser(UserProfile user)
        {
            CodeSigningRequestManager mgr = new CodeSigningRequestManager();

            var userGroupIds = user.Groups.Select(g => g.GroupId).ToList();

            // get the requests for which I am an approver
            var userCodeSigningTypeGroups = _domainService.GetCodeSigningTypeGroups().Where(cstg => userGroupIds.Contains(cstg.GroupId) && 
                                                                                            cstg.IsApprover && 
                                                                                            cstg.CodeSigningType.AllowApprover2Approval);

            var userCodeSigningTypeGroupIds = userCodeSigningTypeGroups.Select(cstg => cstg.CodeSigningTypeId).ToList();



            var pendingRequests = _domainService.GetCodeSigningRequests("CodeSigningType, Group, CreatedByUser")
                                    .Where(r => (userCodeSigningTypeGroupIds.Contains(r.CodeSigningTypeId) || userGroupIds.Contains(r.GroupId)) 
                                            && (r.StatusId == (int)CodeSigningRequestStatus.Submitted ||
                                                r.StatusId == (int)CodeSigningRequestStatus.Approved ||
                                                r.StatusId == (int)CodeSigningRequestStatus.PartiallyApproved ||
                                                r.StatusId == (int)CodeSigningRequestStatus.InProcess))
                                  .OrderByDescending(r => r.CreatedDate);
  
           return pendingRequests;
        }

        /// <summary>
        /// Returns an IQueryable for the list of code signing requests that are pending for the development teams that the 
        /// user is member of
        /// </summary>
        public List<int> GetApproverGroupIds(CodeSigningRequest req)
        {
            List<int> groupIds = new List<int>();

            // if this code signing type allows for secondary approvers, then include these groups in the list.
            if (req.CodeSigningType.AllowApprover1Approval)
            {
                groupIds.Add(req.GroupId);
            }

            // if this code signing type allows for secondary approvers, then include these groups in the list.
            if (req.CodeSigningType.AllowApprover2Approval)
            {
                groupIds = groupIds.Concat(_domainService.GetCodeSigningTypeGroups().Where(g => g.CodeSigningTypeId == req.CodeSigningTypeId && g.IsApprover == true).Select(cstg => cstg.GroupId)).ToList();
            }

            return groupIds;
        }

        /// <summary>
        /// Returns an IQueryable for the list of code signing requests that have been recently completed for the development teams that the 
        /// user is member of.  Display any requests completed in the last 30 days (the number of days in configurable in the System Settings).
        /// </summary>
        public IQueryable<CodeSigningRequest> GetRecentlyCompletedCodeSigningRequestsForUser(UserProfile user)
        {
            CodeSigningRequestManager mgr = new CodeSigningRequestManager();

            var userGroupIds = user.Groups.Select(g => g.GroupId).ToList();

            // get the requests for which I am an approver
            var userCodeSigningTypeGroups = _domainService.GetCodeSigningTypeGroups().Where(cstg => userGroupIds.Contains(cstg.GroupId) && cstg.IsApprover);

            var userCodeSigningTypeGroupIds = userCodeSigningTypeGroups.Select(cstg => cstg.CodeSigningTypeId).ToList();

            int daysOfRequestsToReturn = (int)SystemSettingsManager.Current.GetBySettingName(Constants.SettingNames.DaysOfRecentRequests).IntegerValue;
            DateTime boundaryDate = DateTime.Now.AddDays(daysOfRequestsToReturn * -1);

            var requests = _domainService.GetCodeSigningRequests("CodeSigningType, Group, CreatedByUser")
                                    .Where(r => (userCodeSigningTypeGroupIds.Contains(r.CodeSigningTypeId) || userGroupIds.Contains(r.GroupId))
                                            && (r.StatusId == (int)CodeSigningRequestStatus.CompletedSuccess ||
                                                r.StatusId == (int)CodeSigningRequestStatus.CompletedFailed || r.StatusId == (int)CodeSigningRequestStatus.Cancelled ||
                                                r.StatusId == (int)CodeSigningRequestStatus.Denied ||
                                                r.StatusId == (int)CodeSigningRequestStatus.TimedOut)
                                                && r.CreatedDate >= boundaryDate)
                                  .OrderByDescending(r => r.CreatedDate);

            return requests;
        }

        /// <summary>
        /// Returns an IQueryable for the list of code signing requests that a user may view.
        /// </summary>
        public IQueryable<CodeSigningRequest> GetRequestsForUser(UserProfile user)
        {
            IQueryable<CodeSigningRequest> requests;
            if (user.IsAppAdmin)
            {
                requests = _domainService.GetCodeSigningRequests("CodeSigningType, CreatedByUser,ManagerResponder,Approver1Responder,Approver2Responder").Where(m => m.StatusId != (int)CodeSigningRequestStatus.Unconfirmed)
                                          .OrderByDescending(r => r.CreatedDate); 
            }
            else
            {
                // get the code signing types that this user as permissions to access
                var cstIds = user.CodeSigningTypes.Where(cst => cst.UserSigningTypeRole != (int)AppServer.UserActions.SubmitRequests)
                                                  .Select(col => col.CodeSigningTypeId).ToList();
                requests = _domainService.GetCodeSigningRequests("CodeSigningType, CreatedByUser,ManagerResponder,Approver1Responder,Approver2Responder")
                                    .Where(r => (user.Id == r.CreatedByUserId || cstIds.Contains(r.CodeSigningTypeId)))
                                    .OrderByDescending(r => r.CreatedDate);

            }

            return requests;
        }

        /// <summary>
        /// Gets the details of file names and file hashes 
        /// </summary>
        public static Dictionary<string, string> GetNameAndHashValues(string fileNames, string fileHashes)
        {
            int i = 0;
            Dictionary<string, string> nameAndHash = new Dictionary<string, string>();
            if (fileNames != null && fileHashes != null )
            {
                // extract multiple files into individual names and hashes
                string[] individualFilenames = fileNames.Split(';');
                string[] individualHashValues = fileHashes.Split(';');

                Guard.FailIfFalse(individualFilenames.Count() == individualHashValues.Count(),
                                  "Invalid request configuration, file and hash mismatch.");

                foreach (string file in individualFilenames)
                {
                    if (file.Length > 0 && individualHashValues.Length > 0)
                    {
                        nameAndHash.Add(file.TrimEnd(';'), individualHashValues[i++].TrimEnd(';'));
                    }
                }

            }
            return nameAndHash;
        }
        /// <summary>
        /// Gets the full path names 
        /// </summary>
        public static List<string> GetFullPathNames(string folderName, string filenames)
        {
            List<string> fullPathNames = new List<string>();
            if (folderName != null && filenames != null)
            {
                // extract multiple files into individual names and hashes
                string[] individualFilenames = filenames.Split(';');

                foreach (string file in individualFilenames)
                {
                    fullPathNames.Add(Path.Combine((folderName), file));
                }

            }
            return fullPathNames;
        }

        /// <summary>
        /// Creates a code signing request in unconfirmed status
        /// </summary>
        public CodeSigningRequest CreateCodeSigningRequest(int codeSigningTypeId,int groupId, string fullFilePath, string fileName,                                                            
                                                           bool apiRequest, string releaseName = null, string projectName = null)
        {
            List<string> multiFilenames = new List<string>();
            string folderName = Path.GetDirectoryName(fullFilePath);  // get the name of the temp folder where we are working

            Guard.ArgumentGreaterThan(groupId, 0, "Group Id");
            Guard.ArgumentGreaterThan(codeSigningTypeId, 0, "Code Signing Type Id");
            Guard.ArgumentNotBlankOrNull(fullFilePath, "Full File Path");
            if (!File.Exists(fullFilePath))
            {
                throw new FileNotFoundException("The file to be signed was not found at: {0}", fullFilePath);
            }

            CodeSigningRequest request = new CodeSigningRequest();

            if (Path.GetExtension(fileName) != null &&
                Path.GetExtension(fileName).ToUpper() == ".ZIP")
            {
                string combinedFilenames = string.Empty;
                string combinedUnsignedHashValues = string.Empty;

                // this is an archive file
                using (ZipArchive archive = ZipFile.OpenRead(fullFilePath))
                {
                    request.FileCount = archive.Entries.Count;
                    foreach (ZipArchiveEntry entry in archive.Entries)
                    {
                        var extractedFilename = Path.Combine(folderName, entry.Name);

                        if (!IsValidFileExtension(codeSigningTypeId, extractedFilename))
                        {
                            throw new InvalidFileTypeException();
                        }

                        // extract and overwrite if there is already a file there of this name
                        entry.ExtractToFile(extractedFilename, true);

                        multiFilenames.Add(entry.Name);

                        // get the file hash value
                        combinedUnsignedHashValues += GetFileHash(extractedFilename) + ";";

                        combinedFilenames += entry.Name + ";";
                    }
                }
                // remove the extra ; from the end of the string
                combinedFilenames.TrimEnd(new char[] { ';' });
                combinedUnsignedHashValues.TrimEnd(new char[] { ';' });
                
                fileName = combinedFilenames;

                request.UnsignedFileName = combinedFilenames;
                request.UnsignedFileHash = combinedUnsignedHashValues;
            }
            else
            {
                request.UnsignedFileName = fileName;
                request.UnsignedFileHash = GetFileHash(fullFilePath);
                request.FileCount = 1;
                if (!IsValidFileExtension(codeSigningTypeId, fileName))
                {
                    throw new InvalidFileTypeException();
                }
            }

            var cst = _domainService.GetCodeSigningTypeById(codeSigningTypeId);

            request.CodeSigningTypeId = codeSigningTypeId;
            request.ProjectName = projectName;
            request.ReleaseName = releaseName;
            request.GroupId = groupId;
            request.Status = CodeSigningRequestStatus.Unconfirmed;
            if (apiRequest)
            {
                if (cst == null)
                {
                    throw new RequestProcessingError();
                }

                if (cst.IsAutoSign)
                {
                    request.Status = CodeSigningRequestStatus.Approved;
                }
                else
                {
                    request.Status = CodeSigningRequestStatus.Submitted;
                }
            }

            _domainService.InsertCodeSigningRequest(request);
            _domainService.SaveChanges();

            //Move the file to proper location and rename 
            string path = CodeSigningRequest.GetUnsignedDirectory(request.Id, true);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            if (multiFilenames.Count > 0)
            {
                // need to move individual files if this is an archive
                foreach (var requestFile in multiFilenames)
                {
                    //Move the file(s) from the temp location to the final location
                    System.IO.File.Move(Path.Combine(folderName, requestFile),
                                        Path.Combine(path, requestFile));
                }
            }
            else
            {
                // just move the file over is this is not an archive
                System.IO.File.Move(fullFilePath, Path.Combine(path, request.UnsignedFileName));
            }

            return request;
        }

        /// <summary>
        /// Creates a code signing request in unconfirmed status, unless otherwise specified
        /// </summary>
        public CodeSigningRequest CreateCodeSigningRequest(CodeSigningRequest csRequest, string fullFilePath)
        {
            Guard.ArgumentGreaterThan(csRequest.GroupId, 0, "Group Id");
            Guard.ArgumentGreaterThan(csRequest.CodeSigningTypeId, 0, "Code Signing Type Id");
            Guard.ArgumentNotBlankOrNull(fullFilePath, "Full File Path");
            if (!File.Exists(fullFilePath))
            {
                throw new FileNotFoundException("The file to be signed was not found at: {0}", fullFilePath);
            }

            if (!IsValidFileExtension(csRequest.CodeSigningTypeId, csRequest.UnsignedFileName))
            {
                throw new InvalidFileTypeException();
            }

            var cst = _domainService.GetCodeSigningTypeById(csRequest.CodeSigningTypeId);
            if (cst == null)
            {
                throw new RequestProcessingError();
            }

            if (cst.IsAutoSign)
            {
                csRequest.Status = CodeSigningRequestStatus.Approved;
            }
            else
            {
                csRequest.Status = CodeSigningRequestStatus.Submitted;
            }
            csRequest.UnsignedFileHash = CodeSigningRequestManager.GetFileHashSha256(fullFilePath);

            _domainService.InsertCodeSigningRequest(csRequest);
            _domainService.SaveChanges();

            //Move the file to proper location and rename 
            string path = CodeSigningRequest.GetUnsignedDirectory(csRequest.Id);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            //Move the file from the temp location to the final location
            System.IO.File.Move(fullFilePath, Path.Combine(path, csRequest.UnsignedFileName));

            return csRequest;
        }

        /// <summary>
        /// </summary>
        public bool ApplyTemporalApprovals(int csRequestId)
        {
            bool bSuccess = true;
            try
            {
                CodeSigningRequest request = _domainService.GetCodeSigningRequestById(csRequestId, "CreatedByUser, ManagerResponder, Approver1Responder, Approver2Responder");

                var temporalApprovals = _domainService.GetCodeSigningTemporalApprovals().Where(ta => ta.UserProfileId == request.CreatedByUserId &&
                                                                                                 ta.CodeSigningTypeId == request.CodeSigningTypeId).ToList();
                foreach (var temporalApproval in temporalApprovals)
                {

                    // verify the time
                    if (DateTime.Now > (temporalApproval.StartDate + TimeSpan.FromHours(temporalApproval.Hours)))
                    {
                        // no longer valid, lets delete this one.
                        _domainService.DeleteCodeSigningTemporalApproval(temporalApproval);
                        ActivityLog.LogExecutiveActivity("ApplyTemporalApprovals", 0, "Removing expired temporal approval " + temporalApproval.Id);
                        _domainService.SaveChanges();
                    }
                    else
                    {
                        UserActions action = UserActions.NoAction;
                        var responder = _domainService.GetUserProfileById(temporalApproval.ApprovingUserProfileId, "CodeSigningTypes.CodeSigningType");
                        if (temporalApproval.EnforceSameFileName)
                        {
                            // only approve future requests with the same filename
                            if (request.CreatedDate >= temporalApproval.StartDate &&
                                temporalApproval.FileName.ToLower() == request.UnsignedFileName.ToLower())
                            {
                                ActivityLog.LogExecutiveActivity("ApplyTemporalApprovals", 0, "Request approved through temporal approval by " + temporalApproval.ApprovingUserProfile.FullName);
                                // let the approval function decide how this applies to the request
                                action = ApproveCodeSigningRequest(request, responder);
                            }
                        }
                        else
                        {
                            ActivityLog.LogExecutiveActivity("ApplyTemporalApprovals", 0, "Request approved through temporal approval by " + temporalApproval.ApprovingUserProfile.FullName);
                            // let the approval function decide how this applies to the request
                            action = ApproveCodeSigningRequest(request, responder);
                        }

                        if (action == UserActions.Invalid)
                        {
                            // this temporal approval is not valid anymore.
                            _domainService.DeleteCodeSigningTemporalApproval(temporalApproval);
                            ActivityLog.LogExecutiveActivity("ApplyTemporalApprovals", 0, "Removing invalid temporal approval: " + temporalApproval.Id);
                            _domainService.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception e)
            {
                log.Error("An error occurred confirming the request.", e);
                bSuccess = false;
            }
            return bSuccess;
        }
    


        /// <summary>
        /// Creates a code signing request in unconfirmed status
        /// </summary>
        public CodeSigningRequest CreateHashSigningRequest(CodeSigningRequest request)
        {

            request.Status = CodeSigningRequestStatus.CompletedSuccess;

            try
            {
                _domainService.InsertCodeSigningRequest(request);
                _domainService.SaveChanges();
            }
            catch (Exception ex)
            {
                string errorMessage;

                errorMessage = "Error saving new request.";
                if (ex.InnerException != null)
                {
                    errorMessage += " Inner Exception: " + ex.InnerException.Message;
                }
                log.Error(errorMessage, ex);
            }

            return request;
        }

        /// <summary>
        /// Updates the code siging request with the uploaded file and changes the state to submitted
        /// </summary>
        public CodeSigningRequest UpdateCodeSigningRequest(CodeSigningRequest request, string fullFilePath, string fileName, string projectName = null)
        {
            Guard.ArgumentNotBlankOrNull(fullFilePath, "Full File Path");
            if (!File.Exists(fullFilePath))
            {
                throw new FileNotFoundException("The file to be signed was not found at: {0}", fullFilePath);
            }

            if (!IsValidFileExtension(request.CodeSigningTypeId, fileName))
            {
                throw new InvalidFileTypeException();
            }

            if (request.Status != CodeSigningRequestStatus.AwaitingFile)
            {
                log.Error("User attempted to update code signing request, but the request is not in the proper state.");
                throw new InvalidRequestState(); 
            }

            request.UnsignedFileName = fileName;
            request.UnsignedFileHash = GetFileHash(fullFilePath);

            //Move the file to proper location and rename 
            string path = CodeSigningRequest.GetUnsignedDirectory(request.Id, true);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            //Move the file from the temp location to the final location
            System.IO.File.Move(fullFilePath, Path.Combine(path, request.UnsignedFileName));

            // Update the database status
            request.Status = CodeSigningRequestStatus.Submitted;

            _domainService.UpdateCodeSigningRequest(request);
            _domainService.SaveChanges();

            return request;
        }

        /// <summary>
        /// Creates a code signing request in awaiting status, prior to uploading of the file to sign
        /// </summary>
        public CodeSigningRequest CreateCodeSigningRequest(CodeSigningRequest request)
        {
            request.Status = CodeSigningRequestStatus.AwaitingFile;

            _domainService.InsertCodeSigningRequest(request);
            _domainService.SaveChanges();

            return request;
        }

        /// <summary>
        /// Cancels a code signing request (as distinct from Denying a code signing request).  A cancel only occurs if the 
        /// original submitter decides to cancel the request rather than confirm it.
        /// </summary>
        public void CancelCodeSigningRequest(int csRequestId)
        {
            CodeSigningRequest request = _domainService.GetCodeSigningRequestById(csRequestId);

            UserProfile responder = _gatekeeper.GetCurrentUserProfile();
            if (responder.Id != request.CreatedByUserId)
            {
                log.Error("User attempted to cancel a request that they did not submit.");
                throw new LowLevelUserRightsViolation();
            }

            if (request.Status != CodeSigningRequestStatus.Unconfirmed)
            {
                log.Error("User attempted to cancel a request that is not in the proper state.");
                throw new InvalidRequestState();
            }

            if (request == null)
            {
                //The request was not found.  No need to do anything.
                log.WarnFormat("A request to cancel request id {0} was received but the request was not found.", csRequestId);
                return;
            }
            else
            {
                //Delete the directory
                try
                {
                    Directory.Delete(CodeSigningRequest.GetRequestDirectory(csRequestId), true);
                }
                catch
                {
                    //Failing to delete the directory, while problematic, shouldn't throw an error.  Log the issue and return
                    log.WarnFormat("A request to cancel request id {0} was received but the directory {1} could not be deleted.", csRequestId, CodeSigningRequest.GetRequestDirectory(csRequestId));
                }

                //Delete the request
                _domainService.DeleteCodeSigningRequest(request);
                _domainService.SaveChanges();
            }
        }

        /// <summary>
        /// Cancels an in progress offline request filfillment.
        /// </summary>
        public void CancelOfflineFulfill(int csRequestId)
        {
            CodeSigningRequest request = _domainService.GetCodeSigningRequestById(csRequestId);

            // only allow cancellation of unconfirmed requests
            if (request.Status != CodeSigningRequestStatus.Unconfirmed)
            {
                log.Error("User attempted to cancel a request that is not in the proper state.");
                throw new InvalidRequestState();
            }

            if (request == null)
            {
                //The request was not found.  No need to do anything.
                log.WarnFormat("A request to cancel offline filfillment for request id {0} was received but the request was not found.", csRequestId);
                return;
            }
            else
            {
                //Delete the uploaded file
                try
                {
                    string path = CodeSigningRequest.GetSignedDirectory(request.Id, true);
                    File.Delete(Path.Combine(path, request.SignedFileName));
                }
                catch
                {
                    //Failing to delete the directory, while problematic, shouldn't throw an error.  Log the issue and return
                    log.WarnFormat("A request to cancel offline filfillment for request id {0} was received but the directory {1} could not be deleted.", csRequestId, CodeSigningRequest.GetRequestDirectory(csRequestId));
                }

                request.SignedFileName = null;
                request.SignedFileHash = null;
                request.Status = CodeSigningRequestStatus.AwaitingOfflineSign;
                _domainService.SaveChanges();
            }
        }

        /// <summary>
        /// Entity returned can be changed and updated through this object
        /// </summary>
        public CodeSigningRequest GetCodeSigningRequestById(int csRequestId)
        {
            try
            {
                CodeSigningRequest request = _domainService.GetCodeSigningRequestById(csRequestId, "CreatedByUser,ManagerResponder,Approver1Responder,Approver2Responder");
                return request;
            }
            catch (Exception e)
            {
                log.Error("An error occurred trying to locate request by ID.", e);
                throw e;
            }
        }

        /// <summary>
        /// Confirms a code signing request (as distinct from approving a code signing request).  A confirm occurs when the 
        /// original submitter confirms the request after the initial submittal.
        /// </summary>
        public void ConfirmCodeSigningRequest(int csRequestId)
        {
            try
            {
                CodeSigningRequest request = _domainService.GetCodeSigningRequestById(csRequestId, "CreatedByUser,ManagerResponder,Approver1Responder,Approver2Responder");

                Guard.ValueNotNull(request, string.Format("A call to confirm request id {0} was received but the request was not found.", csRequestId));


                if (request.Status != CodeSigningRequestStatus.Unconfirmed)
                {
                    log.Error("User attempted to confirm a request that is not in the proper state.");
                    throw new InvalidRequestState();
                }

                // If this is auto approving type, mark it as approved immediately
                // request.Status = !(request.CodeSigningType.RequireSecondaryApproval) ? CodeSigningRequestStatus.Approved : CodeSigningRequestStatus.Submitted;
                if (request.CodeSigningType.IsAutoSign)
                {
                    if (request.CodeSigningType.SigningTool.Offline)
                    {
                        request.Status = CodeSigningRequestStatus.AwaitingOfflineSign;
                    }
                    else
                    {
                        request.Status = CodeSigningRequestStatus.Approved;
                    }
                }
                else
                {
                    request.Status = CodeSigningRequestStatus.Submitted;
                }

                _domainService.UpdateCodeSigningRequest(request);
                _domainService.SaveChanges();

                //Need to notify stakeholders
                try
                {
                    EmailManager emailManager = new EmailManager();
                    emailManager.SendNewRequest(request);
                }
                catch (Exception e)
                {
                    log.Error("An error occurred sending notification email.", e);
                    // DO NOT throw e;
                }
            }
            catch (Exception e)
            {
                log.Error("An error occurred confirming the request.", e);
                throw e;
            }
        }

        /// <summary>
        /// Denies a code signing request (as distinct from cancelling a code signing request).
        /// </summary>
        public CodeSigningRequest DenyCodeSigningRequest(int csRequestId, string responderNote)
        {
            try
            {
                CodeSigningRequest request = _domainService.GetCodeSigningRequestById(csRequestId, "CreatedByUser,ManagerResponder,Approver1Responder,Approver2Responder");

                if (request == null)
                {
                    //The request was not found.  No need to do anything.
                    log.ErrorFormat("A request to deny request id {0} was received but the request was not found.", csRequestId);
                }
                else
                {
                    UserProfile responder = _gatekeeper.GetCurrentUserProfile();
                    if (request.CreatedByUserId != responder.Id)
                    {
                        // this should only be denied if the request is not yet approved 
                        if (request.Status != CodeSigningRequestStatus.Submitted &&
                            request.Status != CodeSigningRequestStatus.PartiallyApproved)
                        {
                            log.Error("User attempted to deny a request that is not in the proper state.");
                            throw new InvalidRequestState();
                        }
                    }
                    else
                    {
                        // the owning user can cancel requests is various states
                        if (request.Status != CodeSigningRequestStatus.Submitted &&
                            request.Status != CodeSigningRequestStatus.PartiallyApproved && 
                            request.Status != CodeSigningRequestStatus.AwaitingOfflineSign &&
                            request.Status != CodeSigningRequestStatus.AwaitingFile)
                        {
                            log.Error("User attempted to deny a request that is not in the proper state.");
                            throw new InvalidRequestState();
                        }
                    }

                    //Deny the request
                    if (responder.IsAppAdmin)
                    {
                        request.Approver2ResponderId = responder.Id;
                        request.Approver2ResponseDate = DateTime.Now;
                    }
                    else if (request.CreatedByUserId != responder.Id)
                    {
                        // start with approver1, then assume manager
                        if (responder.CodeSigningTypes.Select(m => m.CodeSigningTypeId == request.CodeSigningTypeId &&
                                                                    m.AuthorizedUserAction == UserActions.Approve1Requests).Count() > 0)
                        {
                            request.Approver1ResponderId = responder.Id;
                            request.Approver1ResponseDate = DateTime.Now;
                        }
                        else
                        {
                            request.ManagerResponderId = responder.Id;
                            request.ManagerResponseDate = DateTime.Now;
                        }
                    }
                    request.ResponderNote = responderNote;

                    if (request.CreatedByUserId == responder.Id)
                    {
                        request.Status = CodeSigningRequestStatus.Cancelled;
                    }
                    else
                    {
                        request.Status = CodeSigningRequestStatus.Denied;
                    }

                    _domainService.UpdateCodeSigningRequest(request);
                    _domainService.SaveChanges();

                    // Need to notify team members
                    try
                    {
                        EmailManager emailManager = new EmailManager();
                        if (request.Status == CodeSigningRequestStatus.Cancelled)
                        {
                            emailManager.SendCancelledRequest(request);
                        }
                        else
                        {
                            emailManager.SendDeniedRequest(request);
                        }
                    }
                    catch (Exception e)
                    {
                        log.Error("An error occurred sending notification email.", e);
                        // DO NOT throw e;
                    }
                }
                return request;
            }
            catch (Exception e)
            {
                log.Error("An error occurred denying the request.", e);
                throw e;
            }
        }

        /// <summary>
        /// sends notifications to administrators and enterprise approvals that there is a request
        /// for them to approve 
        /// </summary>
        public void SendEscalationNotifications(CodeSigningRequest request)
        {
            if (request.EscalationCount != null && request.EscalationCount > 0)
            {
                log.Error("Escalation attmped for request that has already been escalated.");
                return;
            }

            try
            {
                EmailManager emailManager = new EmailManager();

                emailManager.SendEscalactionNotification(request);
            }
            catch (Exception e)
            {
                log.Error("An error occurred sending notification email.", e);
                // DO NOT throw e;
            }
            request.EscalationCount = (request.EscalationCount == null) ? 1 : request.EscalationCount + 1;
            _domainService.UpdateCodeSigningRequest(request);
            _domainService.SaveChanges();
        }

        /// <summary>
        /// Approve a code signing request (as distinct from confirming a code signing request).  An approval occurs when the 
        /// a member of the development team (other than the original submitter) confirms the request after the request submittal.
        /// </summary>
        public UserActions ApproveCodeSigningRequest(int csRequestId)
        {
            CodeSigningRequest request = _domainService.GetCodeSigningRequestById(csRequestId, "CreatedByUser,ManagerResponder,Approver1Responder,Approver2Responder");
            Guard.ValueNotNull(request, string.Format("A call to approve request id {0} was received but the request was not found.", csRequestId));

            CodeSigningType csType = _domainService.GetCodeSigningTypeById(request.CodeSigningTypeId);
            Guard.ValueNotNull(csType, "The code signing type for this request could not be found.  Please contact an administrator.");

            //Update the request to set the status to approved
            UserProfile responder = _gatekeeper.GetCurrentUserProfile();

            return ApproveCodeSigningRequest(request, responder);
        }


        /// <summary>
        /// Approve a code signing request (as distinct from confirming a code signing request).  An approval occurs when the 
        /// a member of the development team (other than the original submitter) confirms the request after the request submittal.
        /// </summary>
        public UserActions ApproveCodeSigningRequest(CodeSigningRequest request, UserProfile responder)
        {         
            UserActions approvalAction = UserActions.NoAction;  // keep track of the action that is applied to this request.
            var beginningStatus = request.Status;
            CodeSigningType csType = _domainService.GetCodeSigningTypeById(request.CodeSigningTypeId);
            Guard.ValueNotNull(csType, "The code signing type for this request could not be found.  Please contact an administrator.");

            // this should only be denied if the request is not yet approved 
            if (request.Status != CodeSigningRequestStatus.Submitted &&
                request.Status != CodeSigningRequestStatus.PartiallyApproved)
            {
                log.Error("User attempted to approve a request that is not in the proper state.");
                throw new InvalidRequestState();
            }

            DateTime responseDate = DateTime.Now;
            // this should never happen because of the gatekeeper checks, but this is not a bad way to 
            // show that we are serious about users not being able to approve their own requests.
            if (request.CreatedByUserId == responder.Id)
            {
                log.Error("Users cannot approve their own requests.");
                return approvalAction;
            }
            else
            {
                bool bDeveloperResponder = false;
                bool bApprovingResponder = false;

                // if this user is part of the developer group of the request, then they are a developer responder
                if (responder.Groups.Select(r => r.GroupId).Contains(request.GroupId))
                {
                    bDeveloperResponder = true;
                }
                else
                {
                    // if the user is an approver for the request, then they are an approving responder
                    // get the list of approvder groups for this code signing type
                    IEnumerable<int> cstgs = _domainService.GetCodeSigningTypeGroups(null)
                                                                            .Where(m => m.CodeSigningTypeId == request.CodeSigningTypeId && m.IsApprover == true)
                                                                            .Select(m => m.GroupId);
                    // if the user is in one of these groups, then they are an approver.
                    if (responder.Groups.Where(grp => cstgs.Contains(grp.GroupId)).Count() > 0)
                    {
                        bApprovingResponder = true;
                    }
                }

                if (!bApprovingResponder && !bDeveloperResponder)
                {
                    // this is an error since neither user can approve the request
                    log.Error("User is not authorized to approve requests for this profile: " + request.CodeSigningTypeId);
                    return UserActions.NoAction;
                }

                if (bDeveloperResponder && request.Approver1ResponseDate == null && request.CodeSigningType.AllowApprover1Approval)
                {
                    request.Approver1ResponderId = responder.Id;
                    request.Approver1ResponseDate = responseDate;
                    approvalAction = UserActions.Approve1Requests;
                }
                else if (bApprovingResponder && request.Approver2ResponseDate == null && request.CodeSigningType.AllowApprover2Approval)
                {
                    request.Approver2ResponderId = responder.Id;
                    request.Approver2ResponseDate = responseDate;
                    approvalAction = UserActions.Approve2Requests;
                }

                if (approvalAction != UserActions.NoAction)
                {
                    // Developer or Approver
                    if (request.CodeSigningType.AllowApprover1Approval && request.CodeSigningType.AllowApprover2Approval &&
                        !request.CodeSigningType.RequireApprover1Approval && !request.CodeSigningType.RequireApprover2Approval)
                    {
                        // if either approved, then request is approved
                        if (request.Approver1ResponderId > 0 || request.Approver2ResponderId > 0)
                        {
                            request.Status = CodeSigningRequestStatus.Approved;
                        }
                    }
                    else if (request.CodeSigningType.RequireApprover1Approval && request.Approver1ResponderId > 0)
                    {
                        // Developer and Approver
                        if (!request.CodeSigningType.RequireApprover2Approval ||
                            (request.CodeSigningType.RequireApprover2Approval && request.Approver2ResponderId > 0))
                        {
                            request.Status = CodeSigningRequestStatus.Approved;
                        }
                    }
                    else if (request.CodeSigningType.RequireApprover2Approval && request.Approver2ResponderId > 0)
                    {
                        if (!request.CodeSigningType.RequireApprover1Approval ||
                            (request.CodeSigningType.RequireApprover1Approval && request.Approver1ResponderId > 0))
                        {

                            request.Status = CodeSigningRequestStatus.Approved;
                        }
                    }
                    else
                    {
                        // request.Status = CodeSigningRequestStatus.CompletedFailed;
                        approvalAction = UserActions.Invalid;

                        log.Error("Irrelevant approval applied to request: " + request.CodeSigningType.Id);
                    }

                    // if there was an approval action, but no approval, then indicate partial approval
                    if (request.Status != CodeSigningRequestStatus.Approved &&
                        approvalAction != UserActions.NoAction &&
                        approvalAction != UserActions.Invalid)
                    {
                        request.Status = CodeSigningRequestStatus.PartiallyApproved;
                    }
                    _domainService.UpdateCodeSigningRequest(request);
                    _domainService.SaveChanges();
                }


                try
                {
                    if (approvalAction != UserActions.NoAction && 
                        approvalAction != UserActions.Invalid)
                    {
                        EmailManager emailManager = new EmailManager();
                        if (request.Status == CodeSigningRequestStatus.PartiallyApproved)
                        {
                            emailManager.SendPartiallyApprovedRequest(request, responder, responseDate);
                        }
                        else if (request.Status == CodeSigningRequestStatus.Approved)
                        {
                            emailManager.SendApprovedRequest(request);
                        }
                    }
                }
                catch (Exception e)
                {
                    log.Error("An error occurred sending notification email.", e);
                }
            }
            return approvalAction;
        }

        /// <summary>
        /// Approve a code signing request (as distinct from confirming a code signing request).  An approval occurs when the 
        /// a member of the development team (other than the original submitter) confirms the request after the request submittal.
        /// </summary>
        public bool HashRequestApproved(CodeSigningType profile, UserProfile user)
        {
            bool bApproved = false;
            bool bApproverApproved = false;
            bool bManagerApproved = false;


            if (profile.IsAutoSign)
            {
                // no approvals are needed.
                return true;
            }

            var temporalApprovals = _domainService.GetCodeSigningTemporalApprovals()
                                                  .Where(m => m.CodeSigningTypeId == profile.Id &&
                                                              m.UserProfileId == user.Id).ToList();

            if (temporalApprovals != null || temporalApprovals.Count() > 0)
            {
                if (temporalApprovals.Where(m => (m.ApprovingUserAction == (int)AppServer.UserActions.AdministratorApproveRequests ||
                                                 m.ApprovingUserAction == (int)AppServer.UserActions.Approve2Requests) &&
                                                 m.expiration > DateTime.Now).Count() > 0)
                {
                    return true;
                }

                if (temporalApprovals.Where(m => m.ApprovingUserAction == (int)AppServer.UserActions.Approve1Requests &&
                                                  m.expiration > DateTime.Now).Count() > 0)
                {
                    bApproverApproved = true;
                }
                if (temporalApprovals.Where(m => m.ApprovingUserAction == (int)AppServer.UserActions.ManagerApproveRequests &&
                                                  m.expiration > DateTime.Now).Count() > 0)
                {
                    bManagerApproved = true;
                }

                if (profile.RequireApprover1Approval && profile.RequireManagerApproval)
                {
                    if (bApproverApproved && bManagerApproved)
                    {
                        bApproved = true;
                    }
                }
                else if (profile.RequireManagerApproval)
                {
                    if (bManagerApproved)
                    {
                        bApproved = true;
                    }
                }
                else if (profile.RequireApprover1Approval)
                {
                    if (bApproverApproved)
                    {
                        bApproved = true;
                    }
                }
                else
                {
                    if (profile.AllowManagerApproval)
                    {
                        if (bManagerApproved)
                        {
                            bApproved = true;
                        }
                    }

                    if (!bApproved && profile.AllowApprover1Approval)
                    {
                        if (bApproverApproved)
                        {
                            bApproved = true;
                        }
                    }
                }
            }

            return bApproved;
        }
        /// <summary>
        /// Fulfill the offline code signing request with the uploaded file
        /// </summary>
        public CodeSigningRequest FulfillOfflineCodeSigningRequest(CodeSigningRequest request, string fullFilePath, 
                                                                   string fileName)
        {
            List<string> multiFilenames = new List<string>();
            string folderName = Path.GetDirectoryName(fullFilePath);  // get the name of the temp folder where we are working

            if (request.Status != CodeSigningRequestStatus.AwaitingOfflineSign)
            {
                log.Error("User attempted to fulfill an offline signing request that is not in the proper state.");
                throw new InvalidRequestState();
            }

            Guard.ArgumentNotBlankOrNull(fullFilePath, "Full File Path");
            if (!File.Exists(fullFilePath))
            {
                throw new FileNotFoundException("The file to be signed was not found at: {0}", fullFilePath);
            }

            if (Path.GetExtension(fileName) != null &&
                Path.GetExtension(fileName).ToUpper() == ".ZIP")
            {
                string combinedFilenames = string.Empty;
                string combinedSignedHashValues = string.Empty;

                // this is an archive file
                using (ZipArchive archive = ZipFile.OpenRead(fullFilePath))
                {
                    request.FileCount = archive.Entries.Count;
                    foreach (ZipArchiveEntry entry in archive.Entries)
                    {
                        var extractedFilename = Path.Combine(folderName, entry.Name);

                        if (!IsValidFileExtension(request.CodeSigningTypeId, extractedFilename))
                        {
                            throw new InvalidFileTypeException();
                        }

                        // extract and overwrite if there is already a file there of this name
                        entry.ExtractToFile(extractedFilename, true);

                        multiFilenames.Add(entry.Name);

                        // get the file hash value
                        combinedSignedHashValues += GetFileHash(extractedFilename) + ";";

                        combinedFilenames += entry.Name + ";";
                    }
                }
                // remove the extra ; from the end of the string
                combinedFilenames.TrimEnd(new char[] { ';' });
                combinedSignedHashValues.TrimEnd(new char[] { ';' });

                fileName = combinedFilenames;

                request.SignedFileName = combinedFilenames;
                request.SignedFileHash = combinedSignedHashValues;
            }
            else
            {
                request.SignedFileName = fileName;
                request.SignedFileHash = GetFileHash(fullFilePath);
                request.FileCount = 1;
                if (!IsValidFileExtension(request.CodeSigningTypeId, fileName))
                {
                    throw new InvalidFileTypeException();
                }
                // verify that this is not the same file as the unsigned file.  Per discussions with
                // Sri and Rene on 7/24/17
                if (request.SignedFileHash == request.UnsignedFileHash)
                {
                    throw new FileHashMismatchException();
                }
            }

            //Move the file to proper location and rename 
            string path = CodeSigningRequest.GetSignedDirectory(request.Id, true);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            //Move the file from the temp location to the final location
            System.IO.File.Move(fullFilePath, Path.Combine(path, request.SignedFileName));

            // Update the database status
            request.Status = CodeSigningRequestStatus.Unconfirmed;

            _domainService.UpdateCodeSigningRequest(request);
            _domainService.SaveChanges();

            return request;
        }

        /// <summary>
        /// Reject the offline code signing request and mark failed
        /// </summary>
        public CodeSigningRequest RejectOfflineCodeSigningRequest(CodeSigningRequest request)
        {
            if (request.Status != CodeSigningRequestStatus.AwaitingOfflineSign)
            {
                log.Error("User attempted to reject an offline signing request that is not in the proper state.");
                throw new InvalidRequestState();
            }

            request.CompletionDate = DateTime.Now;
            // Update the database status
            request.Status = CodeSigningRequestStatus.CompletedFailed;

            _domainService.UpdateCodeSigningRequest(request);
            _domainService.SaveChanges();

            return request;
        }

        /// <summary>
        /// Called when an offline request fulfillment is confirmed.
        /// </summary>
        public void ConfirmFulfillOfflineCodeSigningRequest(int csRequestId)
        {
            try
            {

                CodeSigningRequest request = _domainService.GetCodeSigningRequestById(csRequestId, "CreatedByUser,ManagerResponder,Approver1Responder,Approver2Responder");

                Guard.ValueNotNull(request, string.Format("A call to fulfill request id {0} was received but the request was not found.", csRequestId));

                if (request.Status != CodeSigningRequestStatus.Unconfirmed)
                {
                    log.Error("User attempted to confirm an offline signing request that is not in the proper state.");
                    throw new InvalidRequestState();
                }

                request.Status = CodeSigningRequestStatus.CompletedSuccess;
                request.CompletionDate = DateTime.Now;

                _domainService.UpdateCodeSigningRequest(request);
                _domainService.SaveChanges();

                try
                {
                    //Need to notify team members
                    EmailManager emailManager = new EmailManager();
                    emailManager.SendCompletedSuccess(request, true);
                }
                catch (Exception e)
                {
                    log.Error("An error occurred sending notification email.", e);
                    // DO NOT throw e;
                }
            }
            catch (Exception e)
            {
                log.Error("An error occurred confirming the request.", e);
                throw e;
            }
        }


        public FileInfo GetFile(int requestId, string type, string fileToGet = null)
        {
            CodeSigningRequest request = _domainService.GetCodeSigningRequestById(requestId);

            Guard.ValueNotNull(request, "Request not found");

            List<string> fileList;
            string fileName = string.Empty;
            string filePath = string.Empty;
            string tempPath = Path.Combine(AppServerContext.TempStorageLocation, "download", string.Format("Request-{0}", requestId));

            if (!Directory.Exists(tempPath))
            {
                Directory.CreateDirectory(tempPath);
            }

            switch (type.ToUpper().Trim())
            {
                case "C":
                    // TODO get the certificate from the request entry.
                    fileName = request.CertFileName;
                    filePath = Path.Combine(CodeSigningRequest.GetSignedDirectory(requestId, true), fileName);
                    if (!File.Exists(filePath))
                    {
                        filePath = Path.Combine(CodeSigningRequest.GetSignedDirectory(requestId, false), fileName);
                    }
                    tempPath = Path.Combine(tempPath, fileName);
                    File.Copy(filePath, tempPath, true);
                    _gatekeeper.LogUserActivity("Certificate File Downloaded: " + filePath);
                    break;

                case "R":
                    fileName = request.ResultsFileName;
                    filePath = Path.Combine(CodeSigningRequest.GetSignedDirectory(requestId, true), fileName);
                    if (!File.Exists(filePath))
                    {
                        filePath = Path.Combine(CodeSigningRequest.GetSignedDirectory(requestId, false), fileName);
                        if (!File.Exists(filePath))
                        {
                            throw new InvalidOperationException("The results file for this request cannot be found or is valid.");
                        }
                    }
                    tempPath = Path.Combine(tempPath, fileName);
                    File.Copy(filePath, tempPath, true);
                    _gatekeeper.LogUserActivity("Results File Downloaded: " + filePath);
                    break;

                case "U":
                case "S":
                    fileList = GetValidatedFiles(request, type.ToUpper().Trim() == "S" ? true : false);
                    if (fileList != null)
                    {
                        int i = 0;
                        string[] requestHashValues;
                        if (type.ToUpper().Trim() == "S")
                        {
                            // get the signed hash values
                            requestHashValues = request.SignedFileHash.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                        }
                        else
                        {
                            // get the signed hash values
                            requestHashValues = request.UnsignedFileHash.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);

                        }

                        if (!String.IsNullOrEmpty(fileToGet))
                        {
                            filePath = null;
                            // only one file is requested, make sure it's in the validated file list.
                            foreach (var fileEntry in fileList)
                            {
                                if (Path.GetFileName(fileEntry) == fileToGet)
                                {
                                    filePath = fileEntry;
                                    break;
                                }
                            }
                            if (filePath != null)
                            {
                                tempPath = Path.Combine(tempPath, fileToGet);
                                File.Copy(filePath, tempPath, true);

                                // verify the file hash
                                var tempHash = GetFileHash(tempPath);
                                if (tempHash != requestHashValues[i])
                                {
                                    log.Error("File hash mismatch.  The file download is aborted.");
                                    throw new FileHashMismatchException();
                                }
                                _gatekeeper.LogUserActivity("File Downloaded: " + fileToGet);
                            }
                            else
                            {
                                throw new InvalidOperationException("The files for this request cannot be found or are invalid.");
                            }
                        }
                        else
                        {
                            if (request.FileCount > 1)
                            {
                                tempPath = Path.Combine(tempPath, Path.GetRandomFileName());
                                if (!Directory.Exists(tempPath))
                                {
                                    Directory.CreateDirectory(tempPath);
                                }
                                tempPath = Path.Combine(tempPath, String.Format("request_files_{0}.zip", request.Id));
                                var archive = ZipFile.Open(tempPath, ZipArchiveMode.Create);
                                foreach (var fileEntry in fileList)
                                {
                                    // verify the file hash
                                    var tempHash = GetFileHash(fileEntry);
                                    if (tempHash != requestHashValues[i++])
                                    {
                                        log.Error("File hash mismatch.  The file download is aborted.");
                                        throw new FileHashMismatchException();
                                    }

                                    archive.CreateEntryFromFile(fileEntry, Path.GetFileName(fileEntry));
                                }
                                archive.Dispose();
                            }
                            else
                            {
                                fileName = request.UnsignedFileName;
                                if (request.CodeSigningType.SigningTool.SigningArgumentFormat == 6 ||     // certreq & certreqR
                                    request.CodeSigningType.SigningTool.SigningArgumentFormat == 7)
                                    tempPath = Path.Combine(tempPath, fileName.Replace(Path.GetExtension(fileName), ".cer"));
                                else if (request.CodeSigningType.SigningTool.SigningArgumentFormat == 9)
                                    tempPath = Path.Combine(tempPath, fileName+".sig");
                                else
                                    tempPath = Path.Combine(tempPath, fileName);
                                File.Copy(fileList[0], tempPath, true);
                            }
                        }
                    }
                    else
                    {
                        throw new InvalidOperationException("The files for this request cannot be found or are invalid.");
                    }
                    _gatekeeper.LogUserActivity("Unsigned File Downloaded: " + tempPath);
                    break;
                default:
                    throw new InvalidOperationException(String.Format("The type {0} is not valid.", type));
            }

            FileInfo fileInfo = new FileInfo(tempPath);

            return fileInfo;
        }

        public bool IsValidFileExtension(int codeSigningTypeId, string fileName)
        {
            CodeSigningType csType = _domainService.GetCodeSigningTypeById(codeSigningTypeId);

            //If not extensions are specified, then consider the file provided to be valid
            if (String.IsNullOrWhiteSpace(csType.ValidFileExtensions))
            {
                return true;
            }

            string[] extensions = csType.ValidFileExtensions.Split(',');
            string temp;

            foreach (string extension in extensions)
            {
                temp = extension;
                if (extension.Substring(0, 1) != ".")
                {
                    temp = "." + extension;
                }

                if (fileName.EndsWith(temp))
                {
                    return true;
                }
            }

            return false;
        }

        #endregion

        // ********************************************************************************
        #region Methods to support the Executive
        // ********************************************************************************
        public Boolean IsTimedOut(CodeSigningRequest request, EmailManager emailManager)
        {
            Boolean result = false;
            int requestTimeoutInHours = (int)SystemSettingsManager.Current.GetBySettingName(Constants.SettingNames.RequestTimeoutInHours).IntegerValue;

            // ***************************************************
            // Check to see if the request has timed out
            // ***************************************************
            if (DateTime.Now > ((DateTime)request.CreatedDate).AddHours(requestTimeoutInHours))
            {
                result = true;

                try
                {
                    // Time Out the Request
                    emailManager.SendExpiredRequest(request);
                }
                catch
                {
                    log.WarnFormat("Failed to send email for timed out request {0}", request.Id);
                }

                request.Status = CodeSigningRequestStatus.TimedOut;
                request.CompletionDate = DateTime.Now;
                _domainService.UpdateCodeSigningRequest(request);
                _domainService.SaveChanges();


            }
            return result;
        }

        public void ProcessSubmittedRequests()
        {
            EmailManager emailManager = null;
            try
            {
                emailManager = new EmailManager();
            }
            catch (Exception e)
            {
                log.Error("An error occurred sending notification email.", e);
                // DO NOT throw e;
            }

            List<CodeSigningRequest> submittedRequests = _domainService.GetCodeSigningRequests("CreatedByUser,ManagerResponder,Approver1Responder,Approver2Responder")
                                 .Where(r => r.StatusId == (int)CodeSigningRequestStatus.Submitted ||
                                             r.StatusId == (int)CodeSigningRequestStatus.PartiallyApproved ||
                                             r.StatusId == (int)CodeSigningRequestStatus.AwaitingFile ||
                                             r.StatusId == (int)CodeSigningRequestStatus.InProcess ||
                                             r.StatusId == (int)CodeSigningRequestStatus.AwaitingOfflineSign)
                                 .OrderBy(r => r.CreatedDate)
                                 .ToList();

            // Make sure that there is work to do
            if (submittedRequests.Count == 0) { return; }

            // Get settings
            int reminderNotificationFrequencyInHours = (int)SystemSettingsManager.Current.GetBySettingName(Constants.SettingNames.ReminderNotificationFrequencyInHours).IntegerValue;
            int requestTimeoutInHours = (int)SystemSettingsManager.Current.GetBySettingName(Constants.SettingNames.RequestTimeoutInHours).IntegerValue;

            foreach (CodeSigningRequest request in submittedRequests)
            {
                DateTime dateToCheck;

                if (IsTimedOut(request, emailManager)) {
                    // go to the next request
                    continue;
                }

                // ***************************************************
                // Check to see if we need to send a notification
                // ***************************************************

                if (request.LastNotificationSentDate == null)
                {
                    dateToCheck = (DateTime)request.CreatedDate;
                }
                else
                {
                    dateToCheck = (DateTime)request.LastNotificationSentDate;
                }

                if (dateToCheck.AddHours(reminderNotificationFrequencyInHours) < DateTime.Now)
                {
                    // Send Reminder Notification
                    try
                    {
                        if (emailManager != null)
                        {
                            emailManager.SendRequestReminder(request);
                        }
                    }
                    catch (Exception e)
                    {
                        log.Error("An error occurred sending notification email.", e);
                        // DO NOT throw e;
                    }

                    request.LastNotificationSentDate = DateTime.Now;
                    _domainService.UpdateCodeSigningRequest(request);
                    _domainService.SaveChanges();
                }

            }
        }

        /// <summary>
        /// This method enumerate temporal approvals in the system and apply them to the appropriate pending requests
        /// </summary>
        public void ProcessTemporalApprovals()
        {

            // first expire any expired temporal requests
            var temporalApprovals = _domainService.GetCodeSigningTemporalApprovals();
            if (temporalApprovals != null)
            {
                foreach (var tpa in temporalApprovals)
                {
                    if (DateTime.Now >= tpa.expiration)
                    {
                        _domainService.DeleteCodeSigningTemporalApproval(tpa);
                    }
                }
                _domainService.SaveChanges();
            }

            var pendingRequests = _domainService.GetCodeSigningRequests("CodeSigningType")
                                                                .Where(m => m.StatusId == (int)CodeSigningRequestStatus.Submitted ||
                                                                            m.StatusId == (int)CodeSigningRequestStatus.PartiallyApproved )
                                                                .ToList();
            var errorOccurred = pendingRequests.Aggregate(false, (current, request) =>
                current | ApplyTemporalApprovals(request.Id)
            );

        }

        /// <summary>
        /// This method will identify requests that are approved and ready to be signed and move them
        /// to the appropriate location based on the code signing type
        /// </summary>
        public void ProcessApprovedRequests()
        {
            var approvedRequests = _domainService.GetCodeSigningRequests("CreatedByUser,ManagerResponder,Approver1Responder,Approver2Responder")
                                                                .Where(m => m.StatusId == (int)CodeSigningRequestStatus.Approved)
                                                                .ToList();

            Parallel.ForEach(approvedRequests, request =>ProcessApprovedRequest(request));
            /*
            var errorOccurred = approvedRequests.AsParallel().Aggregate(false, (current, request) => 
                current | ProcessApprovedRequest(request)
            );

            //Not in reqs but seems like contacting admins when errors occur while processing would be a good idea
            if (errorOccurred)
            {
                try
                {
                    EmailManager emailManager = new EmailManager();
                    //emailManager.SendExecutiveError();
                }
                catch (Exception e)
                {
                    log.Error("An error occurred sending notification email.", e);
                    // DO NOT throw e;
                }
            }
            */
        }

        private bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;
            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        /// <summary>
        /// This method checks the storage folder for files and verifies they are ready to be copied for processing.
        /// </summary>
        private List<string> ExamineStoreFiles(string[] files, string storageFolder)
        {
            List<string> validatedFileList = new List<string>();
            int i = 0;

            // examine the store files
            foreach (var requestFile in files)
            {
                FileInfo fileInfo;
                // build the path and make sure the file is present in the files list
                var requestFilePath = Path.Combine(storageFolder, requestFile);
                if (File.Exists(requestFilePath))
                {
                    // verify that we can access the file
                    fileInfo = new FileInfo(requestFilePath);
                    if (IsFileLocked(fileInfo))
                    {
                        // this file is not available in this location at this time
                        log.Info("File not availble for copy:" + requestFilePath);
                        validatedFileList = null;
                        break;
                    }
                    else
                    {
                        // add the file to the list of validated files
                        validatedFileList.Add(requestFilePath);
                        ++i;
                    }
                }
                else
                {
                    log.Info(String.Format("File not found {0}", requestFilePath));
                    validatedFileList = null;
                    break;
                }
            }

            return validatedFileList;
        }

        /// <summary>
        /// This method will find the repository for the files in this request and make sure
        /// the hash values of the submitted files match the repository.
        /// </summary>
        private List<string> GetValidatedFiles(CodeSigningRequest request, bool signed)
        {
            List<string> validatedFileList = null;
            string[] requestHashValues;
            string[] requestFiles;

            if (signed)
            {
                // get the signed hash values
                requestHashValues = request.SignedFileHash.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                // get the unsigned filenames
                requestFiles = request.SignedFileName.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            }
            else
            { 
                // get the unsigned hash values
                requestHashValues = request.UnsignedFileHash.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
                // get the unsigned filenames
                requestFiles = request.UnsignedFileName.Split(new char[] { ';' }, StringSplitOptions.RemoveEmptyEntries);
            }

            // make sure the hash count and the file count match
            if (requestFiles.Count() != requestHashValues.Count())
            {
                log.Error("There is a mismatch in files and hash values for this request: " + request.Id);
            }
            else
            {
                string fileStorage;

                if (signed)
                {
                    fileStorage = CodeSigningRequest.GetSignedDirectory(request.Id, true);
                }
                else
                {
                    fileStorage = CodeSigningRequest.GetUnsignedDirectory(request.Id, true);
                }

                validatedFileList = ExamineStoreFiles(requestFiles, fileStorage);
                if (validatedFileList == null)
                {
                    // note the ! operator to negate the flag for primary vs secondary
                    if (signed)
                    {
                        fileStorage = CodeSigningRequest.GetSignedDirectory(request.Id, false);
                    }
                    else
                    {
                        fileStorage = CodeSigningRequest.GetUnsignedDirectory(request.Id, false);
                    }

                    validatedFileList = ExamineStoreFiles(requestFiles, fileStorage);
                }
            }
            return validatedFileList;
        }

        ProcessRunResult ScanWinVirus(string fileToScan, string fileDir, string fileHash)
        {
            ProcessRunResult scanningResult = new ProcessRunResult(0, "Windows Scan skipped.", null);
            try
            {               
                string scanNumber = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.ScanWinNumber];
                if (scanNumber == null || Int32.Parse(scanNumber) == 0) return scanningResult;  // if no scanNumber set, scan is skipped.
                scanningResult = new ProcessRunResult(1, null, null);
                AntivirusScanner scanner = new AntivirusScanner() { ProcRunner = new ProcessRunner() };
                for (int i = 0; i < Int32.Parse(scanNumber); i++)
                {                 
                    string scanConfigFile = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.ScanConfiguration] + i + ".xml";
                    ScanConfig scanConfig = scanner.LoadSigningToolConfig(scanConfigFile);
                    if (scanConfig == null)
                    {
                        scanningResult = new ProcessRunResult(1, $"{scanningResult.Output}{Environment.NewLine }ScanWinVirus scan config file is null.  " + scanConfigFile, null);
                        log.Error( scanningResult.Output);
                        return scanningResult;
                    }
                    ProcessRunResult scanningResult2 = scanner.Scan(fileToScan, fileDir, fileHash);
                    scanningResult=new ProcessRunResult(scanningResult2.ResultCode, $"{scanningResult.Output}{Environment.NewLine}{scanningResult2.Output}", scanningResult2.Errors);
                    if (scanningResult.ResultCode != 0) return scanningResult;
                }
            }
            catch( Exception e)
            {
                log.Error("ScanWinVirus Exeption:" + e.Message);
            }
            return scanningResult;        
        }

        ProcessRunResult ScanWinZipVirus(List<string> files, int requestid, string[] fileHashes)
        {
            ProcessRunResult scanningResult = new ProcessRunResult(0, "Windows Scan skipped.", null);
            try
            {
                string scanNumber = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.ScanWinNumber];
                if (scanNumber == null || Int32.Parse(scanNumber) == 0) return scanningResult;  // if no scanNumber set, scan is skipped.
                scanningResult = new ProcessRunResult(1, null, null);
                AntivirusScanner scanner = new AntivirusScanner() { ProcRunner = new ProcessRunner() };
                for (int i = 0; i < Int32.Parse(scanNumber); i++)
                {
                    string scanConfigFile = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.ScanConfiguration] + i + ".xml";
                    ScanConfig scanConfig = scanner.LoadSigningToolConfig(scanConfigFile);
                    if (scanConfig == null)
                    {
                        scanningResult = new ProcessRunResult(1, $"{scanningResult.Output}{Environment.NewLine }ScanWinVirus scan config file is null.  " + scanConfigFile, null);
                        log.Error(scanningResult.Output);
                        return scanningResult;
                    }
                    ProcessRunResult scanningResult2 = scanner.Scan(files, requestid, fileHashes);
                    scanningResult = new ProcessRunResult(scanningResult2.ResultCode, $"{scanningResult.Output}{Environment.NewLine}{scanningResult2.Output}", scanningResult2.Errors);
                    if (scanningResult.ResultCode != 0) return scanningResult;
                }
            }
            catch (Exception e)
            {
                log.Error("ScanWinVirus Exeption:" + e.Message);
            }
            return scanningResult;
        }

        ProcessRunResult ScanLinuxVirus(string fileToScan, string fileDir, string fileHash)
        {
            ProcessRunResult scanningResult = new ProcessRunResult(0, "Linux Scan Skipped.", null);
            try
            {
                string scanNumber = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.ScanLinuxNumber];
                int snum = Int32.Parse(scanNumber);
                if (scanNumber == null || snum == 0) return scanningResult;  // if no scanNumber set, scan is skipped.
                scanningResult = new ProcessRunResult(1, null, null);
                AntivirusLinuxScanner scanner = new AntivirusLinuxScanner() { ProcRunner = new ProcessRunner() };
                for (int i = 0; i < snum; i++)
                {
                    string scanConfigFile = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.ScanLinuxConfiguration] + i + ".xml";
                    ScanConfig scanConfig = scanner.LoadSigningToolConfig(scanConfigFile);
                    if (scanConfig == null)
                    {
                        scanningResult = new ProcessRunResult(1, $"{scanningResult.Output}{Environment.NewLine }ScanLinuxVirus scan config file is null.  " + scanConfigFile, null);
                        log.Error(scanningResult.Output);
                        return scanningResult;
                    }
                    ProcessRunResult scanningResult2 = scanner.Scan(fileToScan, fileDir, fileHash);
                    scanningResult = new ProcessRunResult(scanningResult2.ResultCode, $"{scanningResult.Output}{Environment.NewLine}{scanningResult2.Output}", scanningResult2.Errors);
                    if (scanningResult.ResultCode != 0) return scanningResult;
                }
            }
            catch (Exception e)
            {
                log.Error("ScanLinuxVirus Exeption:" + e.Message);
            }
            return scanningResult;
        }

        ProcessRunResult ScanLinuxZipVirus(List<string> files, int requestid, string[] fileHashes)
        {
            ProcessRunResult scanningResult = new ProcessRunResult(0, "Linux Scan Skipped.", null);
            try
            {
                string scanNumber = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.ScanLinuxNumber];
                int snum = Int32.Parse(scanNumber);
                if (scanNumber == null || snum == 0) return scanningResult;  // if no scanNumber set, scan is skipped.
                scanningResult = new ProcessRunResult(1, null, null);
                AntivirusLinuxScanner scanner = new AntivirusLinuxScanner() { ProcRunner = new ProcessRunner() };
                for (int i = 0; i < snum; i++)
                {
                    string scanConfigFile = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.ScanLinuxConfiguration] + i + ".xml";
                    ScanConfig scanConfig = scanner.LoadSigningToolConfig(scanConfigFile);
                    if (scanConfig == null)
                    {
                        scanningResult = new ProcessRunResult(1, $"{scanningResult.Output}{Environment.NewLine }ScanLinuxVirus scan config file is null.  " + scanConfigFile, null);
                        log.Error(scanningResult.Output);
                        return scanningResult;
                    }
                    ProcessRunResult scanningResult2 = scanner.Scan(files, requestid, fileHashes);
                    scanningResult = new ProcessRunResult(scanningResult2.ResultCode, $"{scanningResult.Output}{Environment.NewLine}{scanningResult2.Output}", scanningResult2.Errors);
                    if (scanningResult.ResultCode != 0) return scanningResult;
                }
            }
            catch (Exception e)
            {
                log.Error("ScanLinuxVirus Exeption:" + e.Message);
            }
            return scanningResult;
        }

        public bool ProcessApprovedRequest(CodeSigningRequest request)
        {
            var processedRequest = false;
            var isSuccess = false;
            var signingResultOutput = new StringBuilder();
            var requestDirectory = Path.GetFileName(CodeSigningRequest.GetRequestDirectory(request.Id));
            var workingFolder =  DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.CodeSigningWorkingFolder];
            var unsignedRequestDirectory = Path.Combine(workingFolder, Path.Combine("unsigned", requestDirectory));            

            try
            {
                // get the signing tool information for this signing type
                bool flag = true;
                lock (pLock)
                {
                    var signingTool = _domainService.GetCodeSigningToolById(request.CodeSigningType.SigningToolId);
                    Guard.ValueNotNull(signingTool, "The signing tool configured for this code signing type could not be found."
                                                     + request.CodeSigningType.SigningToolId.ToString());
                    flag = signingTool.Offline;
                }
                if (!flag)
                {

                    request.SigningResult = "";

                    log.InfoFormat("Processing approved request {0}:", request.Id);

                    // check to see if the working "staging" folder exists, if not, then create it
                    if (!Directory.Exists(workingFolder))
                    {
                        Directory.CreateDirectory(workingFolder);
                    }

                    RecreateDirectory(unsignedRequestDirectory);

                    var filesToSign = GetValidatedFiles(request, false);

                    if (filesToSign == null)
                    {
                        // cannot find valid files for signing, wait for the next round and try again.
                        log.Info("Cannot find valid files for signing.");
                    }
                    else
                    {
                        // Update the status on the request now that we have files to process
                        lock (pLock)
                        {
                            SetRequestStatus(request, CodeSigningRequestStatus.InProcess);
                        }
                        processedRequest = true;

                        // get the unsigned hash values
                        var requestHashValues = request.UnsignedFileHash.Split(new char[] { ';' }, 
                                                                               StringSplitOptions.RemoveEmptyEntries);
                        bool isZip = false;
                        if (filesToSign.Count > 1) isZip = true;
                        ProcessRunResult scanningResult;

                        int i = 0;

                        var signedFiles = new List<string>();
                        var resultsFiles = new List<string>();


                        if (isZip)
                        {
                            // windows scan
                            scanningResult = ScanWinZipVirus(filesToSign, request.Id, requestHashValues);
                            signingResultOutput.AppendLine(scanningResult.Output);
                            signingResultOutput.AppendLine(scanningResult.Errors);
                            request.ScanningCommandResultCode = scanningResult.ResultCode;

                            if (scanningResult.ResultCode != 0)
                            {   // file is deleted in finally section
                                var resultsFile2 = Path.GetDirectoryName(filesToSign[0])+"\\" +request.Id + ".results";
                                System.IO.File.WriteAllText(resultsFile2, signingResultOutput.ToString());
                                resultsFiles.Add(resultsFile2);
                                isSuccess = false;
                            }

                            // Linux scan
                            scanningResult = ScanLinuxZipVirus(filesToSign, request.Id, requestHashValues);
                            signingResultOutput.AppendLine(scanningResult.Output);
                            signingResultOutput.AppendLine(scanningResult.Errors);
                            request.ScanningCommandResultCode = scanningResult.ResultCode;

                            if (scanningResult.ResultCode != 0)
                            {   // file is deleted in finally section
                                var resultsFile2 = Path.GetDirectoryName(filesToSign[0]) + "\\" + request.Id + ".results";
                                System.IO.File.WriteAllText(resultsFile2, signingResultOutput.ToString());
                                resultsFiles.Add(resultsFile2);
                                isSuccess = false;
                            }

                        }


                        foreach (var file in filesToSign)
                        {
                            var fileName = Path.GetFileName(file);
                            var stagingFile = Path.Combine(unsignedRequestDirectory, fileName);                            

                            File.Copy(file, stagingFile);

                            // provide time for the server's security scanning system
                            // to scan the file
                            if (AppServerContext.WaitForUnlockTimer > 0)
                            {
                                DateTime startWaitForLock = DateTime.Now;
                                while (IsFileLocked(new FileInfo(stagingFile)) && DateTime.Now > startWaitForLock.AddSeconds(AppServerContext.WaitForUnlockTimer))
                                {
                                    Thread.Sleep(100);
                                }
                            }

                            // verify that the hash of the file has not changed
                            var stagingFileHash = GetFileHash(stagingFile);
                            if (requestHashValues[i++] != stagingFileHash)
                            {
                                log.Error(String.Format("File hash values do not match for {0}.  Expected {1}, but found {2}",
                                                        stagingFile, requestHashValues[i], stagingFileHash));

                                // this breaks the loop
                                isSuccess = false;
                            }
                            else
                            {
                                if (!isZip)  // not zip file
                                {
                                    // windows scan
                                    scanningResult = ScanWinVirus(stagingFile, request.Id + "_" + i, stagingFileHash);
                                    signingResultOutput.AppendLine(scanningResult.Output);
                                    signingResultOutput.AppendLine(scanningResult.Errors);
                                    request.ScanningCommandResultCode = scanningResult.ResultCode;

                                    if (scanningResult.ResultCode != 0)
                                    {   // file is deleted in finally section
                                        var resultsFile2 = stagingFile + ".results";
                                        System.IO.File.WriteAllText(resultsFile2, signingResultOutput.ToString());
                                        resultsFiles.Add(resultsFile2);
                                        isSuccess = false;
                                        break;
                                    }

                                    // Linux scan
                                    scanningResult = ScanLinuxVirus(stagingFile, request.Id + "_" + i, stagingFileHash);
                                    signingResultOutput.AppendLine(scanningResult.Output);
                                    signingResultOutput.AppendLine(scanningResult.Errors);
                                    request.ScanningCommandResultCode = scanningResult.ResultCode;

                                    if (scanningResult.ResultCode != 0)
                                    {   // file is deleted in finally section
                                        var resultsFile2 = stagingFile + ".results";
                                        System.IO.File.WriteAllText(resultsFile2, signingResultOutput.ToString());
                                        resultsFiles.Add(resultsFile2);
                                        isSuccess = false;
                                        break;
                                    }
                                }

                                // Once copied to staging, sign the file there 
                                var signingResult = codeSigner.Sign(request.CodeSigningType, stagingFile);
                                log.Info("sign result:" + signingResult.Output);

                                // save the most recent signing command result code
                                request.SigningCommandResultCode = signingResult.ResultCode;

                                signingResultOutput.AppendLine(signingResult.Output);
                                signingResultOutput.AppendLine(signingResult.Errors);
                                // save the output to the results file
                                var resultsFile = stagingFile + ".results";
                                System.IO.File.WriteAllText(resultsFile, signingResultOutput.ToString());

                                isSuccess = signingResult.ResultCode == 0;

                                if(request.CodeSigningType.SigningTool.SigningArgumentFormat ==6 ||     // certreq & certreqR
                                   request.CodeSigningType.SigningTool.SigningArgumentFormat == 7 )
                                    signedFiles.Add(stagingFile.Replace(Path.GetExtension(stagingFile), ".cer"));
                                else if (request.CodeSigningType.SigningTool.SigningArgumentFormat == 9 )
                                    signedFiles.Add(stagingFile+".sig");
                                else
                                    signedFiles.Add(stagingFile);
                                
                                resultsFiles.Add(resultsFile);
                                if (!isSuccess)
                                {
                                    break;
                                }
                            }
                        }

                        // make sure the target folder exists before we copy files
                        RecreateDirectory(CodeSigningRequest.GetSignedDirectory(request.Id, true));
                        if (isSuccess)
                        {
                            MoveSignedFiles(request, signedFiles);
                        }
                        MoveResultsFiles(request, resultsFiles);
                    }
                }
            }
            catch (Exception e)
            {
                log.Error(e.Message, e);
                isSuccess = false;
            }
            finally
            {
                if (processedRequest)
                {
                    // this function saves the current state and status of the request
                    ApprovedRequestProcessed(request, signingResultOutput, unsignedRequestDirectory, isSuccess);
                }
            }

            return isSuccess;
        }

        private void ApprovedRequestProcessed(CodeSigningRequest request, StringBuilder signingResultOutput, 
            string stagingRequestDirectory, bool isSuccess)
        {
            try
            {
                request.SigningResult = signingResultOutput.ToString();
                request.CompletionDate = DateTime.Now;
                lock (pLock)
                {
                    SetRequestStatus(request, isSuccess ? CodeSigningRequestStatus.CompletedSuccess : CodeSigningRequestStatus.CompletedFailed);
                }
            }
            catch (Exception ex)
            {
                log.Error($"Failed to set signing status on request: {request.Id}. Success: {isSuccess}. Signing process output: {signingResultOutput}", ex);
            }

            try
            {
                try
                {
                    var emailManager = new EmailManager();

                    if (isSuccess)
                    {
                        emailManager.SendCompletedSuccess(request);
                    }
                    else
                    {
                        emailManager.SendCompletedFailed(request);
                    }
                }
                catch (Exception e)
                {
                    log.Error("An error occurred sending notification email.", e);
                    // DO NOT throw e;
                }
            }
            catch (Exception ex)
            {
                log.Error($"Failed to send email that approved request {request.Id} was processed. Request processign was successful: {isSuccess}", ex);
            }
            

            //CR06
            try
            {
                //Attempt to delete the directory
                Directory.Delete(stagingRequestDirectory, true);
            }
            catch
            {
                //Ignore any exception this might throw
            }
        }

        /// <summary>
        /// Deletes files associated with a request and marks the request as archived.
        /// </summary>
        public int ArchiveRequests(int daysOld)
        {
            int recordsDeleted = 0;
            if (daysOld >= 0)
            {
                var compareDate = DateTime.Now.AddDays(-daysOld);
                var oldRequests = _domainService.GetCodeSigningRequests("CodeSigningType")
                                                                    .Where(m => m.CreatedDate < compareDate && m.Archived == null)
                                                                    .ToList();
                recordsDeleted = oldRequests.Count;
                foreach (var oldRequest in oldRequests)
                {
                    var requestDir = CodeSigningRequest.GetRequestDirectory(oldRequest.Id, true);

                    try
                    {
                        Directory.Delete(requestDir, true);
                        log.InfoFormat("Removed request directory in Archive operation: {0}", requestDir);
                    }
                    catch (Exception ex)
                    {
                        log.Error("An error occurred removing request folder from primary storage: " + requestDir, ex);
                    }

                    // do the same for the secondary storage
                    requestDir = CodeSigningRequest.GetRequestDirectory(oldRequest.Id, false);
                    try
                    {
                        Directory.Delete(requestDir, true);
                        log.InfoFormat("Removed request directory in Archive operation: {0}", requestDir);
                    }
                    catch (Exception ex)
                    {
                        log.Error("An error occurred removing request folder from secondary storage: " + requestDir, ex);
                    }

                    oldRequest.Archived = DateTime.Now;
                    _domainService.UpdateCodeSigningRequest(oldRequest);
                }
                _domainService.SaveChanges();
            }
            else
            {
                log.Error("Invalid day range value: " + daysOld);
            }
            return recordsDeleted;
        }

        private void SetRequestStatus(CodeSigningRequest request, CodeSigningRequestStatus newStatus)
        {
            request.Status = newStatus;

            _domainService.UpdateCodeSigningRequest(request);
            _domainService.SaveChanges();
        }

        /// <summary>
        /// Method to move signedFiles to 'signed' directory and update request with 
        /// signed file name and hash.  Assumes GetSignedDirectory folder exists.
        /// </summary>
        private void MoveSignedFiles(CodeSigningRequest request, IEnumerable<string> signedFiles)
        {
            request.SignedFileName = string.Empty;
            request.SignedFileHash = string.Empty;

            // Move all files to signed directory
            foreach (var signedFile in signedFiles)
            {
                var fileName = Path.GetFileName(signedFile);

                var filePatInSignedDir = Path.Combine(CodeSigningRequest.GetSignedDirectory(request.Id, true), fileName);

                MoveFile(signedFile, filePatInSignedDir);

                request.SignedFileName += fileName + ";";
                request.SignedFileHash += GetFileHash(filePatInSignedDir) + ";";
            }

            // trim the ";" at the end of the signed filename string
            char[] charsToTrim = { ';' };
            request.SignedFileName = request.SignedFileName.TrimEnd(charsToTrim);
            request.SignedFileHash = request.SignedFileHash.TrimEnd(charsToTrim);
        }

        /// <summary>
        /// Method to move output of the signing to the storage directory.  Assumes GetSignedDirectory exists.
        /// </summary>
        private void MoveResultsFiles(CodeSigningRequest request, IEnumerable<string> resultsFiles)
        {
            // Move all files to signed directory
            foreach (var resultsFile in resultsFiles)
            {
                var fileName = Path.GetFileName(resultsFile);

                var filePatInSignedDir = Path.Combine(CodeSigningRequest.GetSignedDirectory(request.Id, true), fileName);

                MoveFile(resultsFile, filePatInSignedDir);

                request.ResultsFileName = fileName;
            }
        }
        /// <summary>
        /// Recreates the directory by recursively deleting it if it exists, and then creating it again.
        /// </summary>
        /// <param name="dirName">Name of the dir.</param>
        private static void RecreateDirectory(string dirName)
        {
            if (Directory.Exists(dirName))
            {
                Directory.Delete(dirName, true);
            }

            Directory.CreateDirectory(dirName);
        }

        /// <summary>
        /// Moves the file by copying first and then trying to delete the original.
        /// If deletion of original fails, exception will be swallowed.
        /// If target directory does not exist, it will be created.
        /// </summary>
        /// <param name="fromFile">From file.</param>
        /// <param name="toFile">To file.</param>
        private static void MoveFile(string fromFile, string toFile)
        {
            if (File.Exists(toFile))
            {
                File.Delete(toFile);
            }

            var toDirectory = Directory.GetParent(toFile);

            if (!Directory.Exists(toDirectory.ToString()))
            {
                Directory.CreateDirectory(toDirectory.ToString());
            }

            File.Copy(fromFile, toFile);

            //Delete the source file, intentionally not doing a MOVE above in case account has read but not write
            try
            {
                File.Delete(fromFile);
            }
            catch
            {
                //If the source file cannot be deleted, ignore the error
            }
        }

        #endregion
    }
    public class LowLevelUserRightsViolation : Exception { }

    public class InvalidRequestState : Exception { }

    public class InvalidFileTypeException : Exception { }

    public class FileHashMismatchException : Exception { }

    public class RequestProcessingError : Exception { }
}
