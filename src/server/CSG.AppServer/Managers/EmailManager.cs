﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading;
using CSG.AppServer.Db;
using CSG.AppServer.Entities;
using Util;

namespace CSG.AppServer.Managers
{
    public class EmailManager
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly Type ThisDeclaringType = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;

        // *********************************************************************************
        #region Properties
        // *********************************************************************************

        private string MailServer { get; set; }
        private int Port { get; set; }
        private string UserId { get; set; }
        private string Password { get; set; }
        private bool? UseSSL { get; set; }
        private bool? SendNotifications { get; set; }

        private bool RunThreaded { get; set; }

        private string FromEmail { get; set; }
        private string PortalUrl { get; set; }
        private string ToEmail { get; set; }
        private string Subject { get; set; }
        private string Body { get; set; }

        private string ServerName { get; set; }

        #endregion

        // *********************************************************************************
        #region Constructor
        // *********************************************************************************

        public EmailManager(bool runThreaded = true)
        {

            MailServer = SystemSettingsManager.Current.GetBySettingName(Constants.SettingNames.EmailServer, true).StringValue;
            try
            {
                Port = (int)SystemSettingsManager.Current.GetBySettingName(Constants.SettingNames.EmailServerPort, true).IntegerValue;
            }
            catch
            {
                Port = 25;
            }

            ServerName = SystemSettingsManager.Current.GetBySettingName(Constants.SettingNames.ServerName, true).StringValue;

            FromEmail = SystemSettingsManager.Current.GetBySettingName(Constants.SettingNames.TeamEmailAddress, true).StringValue;

            PortalUrl = SystemSettingsManager.Current.GetBySettingName(Constants.SettingNames.PortalUrl, true).StringValue;

            UserId = SystemSettingsManager.Current.GetBySettingName(Constants.SettingNames.EmailServerUserId, true).StringValue;

            Password = SystemSettingsManager.Current.GetBySettingName(Constants.SettingNames.EmailServerPassword, true).PasswordValue;
            if (!string.IsNullOrWhiteSpace(Password))
            {
                Password = CryptographyTools.Decrypt(Password);
            }

            UseSSL = SystemSettingsManager.Current.GetBySettingName(Constants.SettingNames.EmailServerSSL, true).BooleanValue;
            if (UseSSL == null)
            {
                UseSSL = false;
            }

            SendNotifications = SystemSettingsManager.Current.GetBySettingName(Constants.SettingNames.EmailSendNotifications, true).BooleanValue;
            if (SendNotifications == null)
            {
                SendNotifications = false;
            }
            
            RunThreaded = runThreaded;
        }

        #endregion

        // *********************************************************************************
        #region Methods
        // *********************************************************************************

        /// <summary>
        /// Sends an email to all team members for the request provided notifying them of a new code signing request.
        /// </summary>
        public void SendNewRequest(CodeSigningRequest request)
        {
            if ((bool)SendNotifications)
            {
                StringBuilder message = new StringBuilder("A new request for code signing has been submitted to the Code Signing System, with following details.");
                string subject;
                bool offline;

                message.AppendLine(string.Empty);
                message.AppendLine(string.Empty);
                message.AppendLine(String.Format("ServerName:   {0}", ServerName));
                message.AppendLine(String.Format("Filename:   {0}", request.UnsignedFileName));
                message.AppendLine(String.Format("File Hash:  {0}", request.UnsignedFileHash));
                message.AppendLine(String.Format("Request Id: {0}", request.Id));
                message.AppendLine(string.Empty);
                if (request.CodeSigningType.IsAutoSign)
                {
                    message.AppendLine(String.Format("This request was submitted by {0} and will be auto-signed.", request.CreatedByUser.FullName));
                }
                else
                {
                    message.AppendLine(String.Format("This request was submitted by {0} and requires approval before it will be processed.", request.CreatedByUser.FullName));
                    message.AppendLine(String.Format("Please login to approve or deny the signing request by {0}, after which request is auto denied.", request.CreatedDate.Value.AddHours(AppServerContext.RequestTimeout)));
                }
                message.AppendLine(string.Empty);
                message.AppendLine(String.Format("You may login to the Portal ({0}) for additional information regarding this request.", this.PortalUrl));
                message.AppendLine(string.Empty);
                message.AppendLine(string.Empty);
                message.AppendLine("Thank you,");
                message.AppendLine(string.Empty);
                message.AppendLine("CSG Administration ");

                log.Debug("Notification Message prepared: " + message);

                offline = request.CodeSigningType.SigningTool.Offline;
                if (request.CodeSigningType.IsAutoSign)
                {
                    subject = String.Format("Code Signing Request Submitted and Approved: Request ID - {0}", request.Id);
                }
                else
                {
                    subject = String.Format("Code Signing Request Submitted: Request ID - {0}", request.Id);
                }
                Send(this.FromEmail, GetRecipientList(request, offline), subject, message.ToString());
            }
        }

        /// <summary>
        /// Sends an email to all team members for the request provided notifying them that there is a request waiting
        /// for approval.
        /// </summary>
        public void SendRequestReminder(CodeSigningRequest request)
        {
            if ((bool)SendNotifications)
            {
                StringBuilder message = new StringBuilder("A request for code signing was submitted to the Code Signing System.  The following code was submitted: ");
                string subject;
                bool offline;

                message.AppendLine(string.Empty);
                message.AppendLine(string.Empty);
                message.AppendLine(String.Format("ServerName:   {0}", ServerName));
                message.AppendLine(String.Format("Filename:   {0}", request.UnsignedFileName));
                message.AppendLine(String.Format("File Hash:  {0}", request.UnsignedFileHash));
                message.AppendLine(String.Format("Request Id: {0}", request.Id));
                message.AppendLine(string.Empty);
                if (request.CodeSigningType.IsAutoSign)
                {
                    message.AppendLine(String.Format("This request was submitted by {0} and will be auto-signed.", request.CreatedByUser.FullName));
                }
                else
                {
                    message.AppendLine(String.Format("This request was submitted by {0} and requires approval before it will be processed. Please login to approve or deny the signing request by {1}, at which time the request will be automatically denied.", request.CreatedByUser.FullName, request.CreatedDate.Value.AddHours(AppServerContext.RequestTimeout)));
                }
                message.AppendLine(string.Empty);
                message.AppendLine(String.Format("You may login to the Portal ({0}) for additional information regarding this request.", this.PortalUrl));
                message.AppendLine(string.Empty);
                message.AppendLine(string.Empty);
                message.AppendLine("Thank you,");
                message.AppendLine(string.Empty);
                message.AppendLine("CSG Administration ");

                log.Debug("Notification Message prepared: " + message);

                subject = String.Format("REMINDER: Code Signing Request Submitted: Request Id {0}", request.Id);

                offline = request.CodeSigningType.SigningTool.Offline;
                Send(this.FromEmail, GetRecipientList(request, offline), subject, message.ToString());
            }
        }

        /// <summary>
        /// Sends an email to all team members for the request provided notifying them of the denial of the code signing request.
        /// </summary>
        public void SendDeniedRequest(CodeSigningRequest request)
        {
            if ((bool)SendNotifications)
            {

                StringBuilder message = new StringBuilder("A request for code signing has been denied for the following request:");
                message.AppendLine(string.Empty);
                message.AppendLine(string.Empty);
                message.AppendLine(String.Format("ServerName:   {0}", ServerName));
                message.AppendLine(String.Format("Filename:   {0}", request.UnsignedFileName));
                message.AppendLine(String.Format("File Hash:  {0}", request.UnsignedFileHash));
                message.AppendLine(String.Format("Request Id: {0}", request.Id));
                message.AppendLine(string.Empty);
                message.AppendLine(string.Empty);
                message.AppendLine(String.Format("Please login to the Portal ({0}) for additional information regarding this request.", this.PortalUrl));
                message.AppendLine(string.Empty);
                message.AppendLine("Thank you,");
                message.AppendLine(string.Empty);
                message.AppendLine("CSG Administration ");

                Send(this.FromEmail, 
                     GetRecipientList(request), 
                     string.Format("Code Signing Request Denied: Request ID - {0}", request.Id), 
                     message.ToString());
            }
        }

        /// <summary>
        /// Sends an email to admins and enterprise approvers indicating the have been asked to approve a request.
        /// </summary>
        public void SendEscalactionNotification(CodeSigningRequest request)
        {
            if ((bool)SendNotifications)
            {
                StringBuilder message = new StringBuilder("A user has escalated the approval process for a request in the Code Signing System.  ");
                message.AppendLine("The following code was submitted: ");

                message.AppendLine(string.Empty);
                message.AppendLine(string.Empty);
                message.AppendLine(String.Format("ServerName:   {0}", ServerName));
                message.AppendLine(String.Format("Filename:   {0}", request.UnsignedFileName));
                message.AppendLine(String.Format("File Hash:  {0}", request.UnsignedFileHash));
                message.AppendLine(String.Format("Request Id: {0}", request.Id));
                message.AppendLine(string.Empty);

                message.AppendLine(String.Format("This request was submitted by {0}.  Please login to approve or deny the signing request by {1}, after which the request will be auto denied.", request.CreatedByUser.FullName, request.CreatedDate.Value.AddHours(AppServerContext.RequestTimeout)));
                message.AppendLine(string.Empty);
                message.AppendLine(String.Format("You may login to the Portal ({0}) for additional information regarding this request.", this.PortalUrl));
                message.AppendLine(string.Empty);
                message.AppendLine("Thank you,");
                message.AppendLine(string.Empty);
                message.AppendLine("CSG Administration ");

                Send(this.FromEmail, 
                     GetAdmins(request), 
                     String.Format("Code Signing Request Urgent: Request ID - {0}", request.Id), 
                     message.ToString());
            }
        }

        /// <summary>
        /// Sends an email to all team members for the request provided notifying them of the cancellation of the code signing request.
        /// </summary>
        public void SendCancelledRequest(CodeSigningRequest request)
        {
            if ((bool)SendNotifications)
            {

                StringBuilder message = new StringBuilder("The request for code signing has been cancelled for the following code: ");
                message.AppendLine(string.Empty);
                message.AppendLine(string.Empty);
                message.AppendLine(String.Format("ServerName:   {0}", ServerName));
                message.AppendLine(String.Format("Filename:   {0}", request.UnsignedFileName));
                message.AppendLine(String.Format("File Hash:  {0}", request.UnsignedFileHash));
                message.AppendLine(String.Format("Request Id: {0}", request.Id));
                message.AppendLine(string.Empty);
                message.AppendLine(string.Empty);
                message.AppendLine(String.Format("Please login to the Portal ({0}) for additional information regarding this request.", this.PortalUrl));
                message.AppendLine(string.Empty);
                message.AppendLine("Thank you,");
                message.AppendLine(string.Empty);
                message.AppendLine("CSG Administration ");

                Send(this.FromEmail, GetRecipientList(request), "Code Signing Request - Cancelled", message.ToString());
            }
        }

        /// <summary>
        /// Sends an email to all team members for the request provided notifying them of the partial approval of the code signing request.
        /// </summary>
        public void SendPartiallyApprovedRequest(CodeSigningRequest request, UserProfile responder, DateTime responseDate)
        {
            if ((bool)SendNotifications)
            {
                string subject;
                StringBuilder message = new StringBuilder("A request for code signing has been approved for the following code: ");
                message.AppendLine(string.Empty);
                message.AppendLine(string.Empty);
                message.AppendLine(String.Format("ServerName:   {0}", ServerName));
                message.AppendLine(String.Format("Filename:   {0}", request.UnsignedFileName));
                message.AppendLine(String.Format("File Hash:  {0}", request.UnsignedFileHash));
                message.AppendLine(String.Format("Request Id: {0}", request.Id));
                message.AppendLine(string.Empty);
                message.AppendLine(String.Format("This request was submitted by {0} on {1}.  This request requires additional approval before it is processed. " +
                                                 "You will receive an additional email notification on completion of the process when the file is available for download.",
                                                 (request.CreatedByUser == null) ? "<>" : request.CreatedByUser.FullName, request.CreatedDate.Value));
                if (responder != null)
                {
                    message.AppendLine(String.Format("This request was approved by {0} on {1}.", responder.FullName, responseDate));
                }

                message.AppendLine(string.Empty);
                message.AppendLine(String.Format("Please login to the Portal ({0}) for additional information regarding this request.", this.PortalUrl));
                message.AppendLine(string.Empty);
                if (request.CodeSigningType.SigningTool.Offline)
                {
                    message.AppendLine(String.Format("This is an offline signing request and requires an offline signer to manually complete the signing process."));
                    message.AppendLine(string.Empty);
                }

                message.AppendLine("Thank you,");
                message.AppendLine(string.Empty);
                message.AppendLine("CSG Administration ");

                subject = String.Format("Code Signing Request Partially Approved: Request Id - {0}", request.Id);
                Send(this.FromEmail, GetRecipientList(request, request.CodeSigningType.SigningTool.Offline),
                    subject, message.ToString());
            }
        }

        /// <summary>
        /// Sends an email to all team members for the request provided notifying them of the approval of the code signing request.
        /// </summary>
        public void SendApprovedRequest(CodeSigningRequest request)
        {
            if ((bool)SendNotifications)
            {
                string subject;
                StringBuilder message = new StringBuilder("A request for code signing has been approved for the following code: ");
                message.AppendLine(string.Empty);
                message.AppendLine(string.Empty);
                message.AppendLine(String.Format("ServerName:   {0}", ServerName));
                message.AppendLine(String.Format("Filename:   {0}", request.UnsignedFileName));
                message.AppendLine(String.Format("File Hash:  {0}", request.UnsignedFileHash));
                message.AppendLine(String.Format("Request Id: {0}", request.Id));
                message.AppendLine(string.Empty);
                message.AppendLine(String.Format("This request was submitted by {0} on {1}.  You will receive an additional email notification on completion of the process when file is available for download.", 
                                                 (request.CreatedByUser == null) ? "<>" : request.CreatedByUser.FullName, request.CreatedDate.Value));
                if (request.ManagerResponder != null)
                {
                    message.AppendLine(String.Format("This request was approved by {0} on {1}.", request.ManagerResponder.FullName, request.ManagerResponseDate.Value));
                }
                if (request.Approver1Responder != null)
                {
                    message.AppendLine(String.Format("This request was approved by {0} on {1}.", request.Approver1Responder.FullName, request.Approver1ResponseDate.Value));
                }
                if (request.Approver2Responder != null)
                {
                    message.AppendLine(String.Format("This request was approved by {0} on {1}.", request.Approver2Responder.FullName, request.Approver2ResponseDate.Value));
                }

                message.AppendLine(string.Empty);
                message.AppendLine(String.Format("Please login to the Portal ({0}) for additional information regarding this request.", this.PortalUrl));
                message.AppendLine(string.Empty);
                if (request.CodeSigningType.SigningTool.Offline)
                {
                    message.AppendLine(String.Format("This is an offline signing request and requires an offline signer to manually complete the signing process."));
                    message.AppendLine(string.Empty);
                }

                message.AppendLine("Thank you,");
                message.AppendLine(string.Empty);
                message.AppendLine("CSG Administration ");

                subject = String.Format("Code Signing Request Approved: Request Id - {0}", request.Id);
                Send(this.FromEmail, GetRecipientList(request, request.CodeSigningType.SigningTool.Offline),
                    subject, message.ToString());
            }
        }

        /// <summary>
        /// Sends an email to all team members for the request provided notifying them of the expiration of the code signing request.
        /// </summary>
        public void SendExpiredRequest(CodeSigningRequest request)
        {
            if ((bool)SendNotifications)
            {
                StringBuilder message = new StringBuilder();
                message.AppendLine(String.Format("A request for code signing has expired.  The request was originally submitted by {0} on {1} and was for the following code:", request.CreatedByUser.FullName, request.CreatedDate.Value));
                message.AppendLine(string.Empty);
                message.AppendLine(string.Empty);
                message.AppendLine(String.Format("ServerName:   {0}", ServerName));
                message.AppendLine(String.Format("Filename:   {0}", request.UnsignedFileName));
                message.AppendLine(String.Format("File Hash:  {0}", request.UnsignedFileHash));
                message.AppendLine(String.Format("Request Id: {0}", request.Id));
                message.AppendLine(string.Empty);
                message.AppendLine(String.Format("This code sign request has been auto cancelled."));
                message.AppendLine(string.Empty);
                message.AppendLine(String.Format("Please login to the Portal ({0}) for additional information regarding this request.", this.PortalUrl));
                message.AppendLine(string.Empty);
                message.AppendLine("Thank you,");
                message.AppendLine(string.Empty);
                message.AppendLine("CSG Administration ");

                Send(this.FromEmail,
                     GetRecipientList(request), 
                     string.Format("Code Signing Request Expired: Request ID - {0}", request.Id), 
                     message.ToString());
            }
        }

        /// <summary>
        /// Sends an email to all team members for the request provided notifying them of the successful completion of the code signing request.
        /// </summary>
        public void SendCompletedSuccess(CodeSigningRequest request, bool offlineSign = false)
        {
            if ((bool)SendNotifications)
            {
                StringBuilder message = new StringBuilder("A request for code signing has been successfully completed for the following code: ");
                message.AppendLine(string.Empty);
                message.AppendLine(string.Empty);
                message.AppendLine(String.Format("ServerName:   {0}", ServerName));
                message.AppendLine(String.Format("Filename:   {0}", request.UnsignedFileName));
                message.AppendLine(String.Format("File Hash:  {0}", request.UnsignedFileHash));
                message.AppendLine(String.Format("Request Id: {0}", request.Id));
                message.AppendLine(string.Empty);
                message.AppendLine(String.Format("This request was submitted by {0} on {1}.  The signed file is now available for download.", request.CreatedByUser.FullName, request.CreatedDate.Value));
                if (request.ManagerResponder != null)
                {
                    message.AppendLine(String.Format("This request was approved by {0} on {1}.", request.ManagerResponder.FullName, request.ManagerResponseDate.Value));
                }
                if (request.Approver1Responder != null)
                {
                    message.AppendLine(String.Format("This request was approved by {0} on {1}.", request.Approver1Responder.FullName, request.Approver1ResponseDate.Value));
                }
                if (request.Approver2Responder != null)
                {
                    message.AppendLine(String.Format("This request was approved by {0} on {1}.", request.Approver2Responder.FullName, request.Approver2ResponseDate.Value));
                }
                message.AppendLine(string.Empty);
                message.AppendLine(String.Format("Please login to the Portal ({0}) to download the signed code and to view additional information regarding this request.", this.PortalUrl));
                message.AppendLine(string.Empty);
                message.AppendLine(string.Empty);
                message.AppendLine("Thank you,");
                message.AppendLine(string.Empty);
                message.AppendLine("CSG Administration ");

                var toMail = GetRecipientList(request, true);
                Send(this.FromEmail, toMail, "Code Signing Request - Completed", message.ToString());
            }
        }

        /// <summary>
        /// Sends an email to all team members for the request provided notifying them of the failed completion of the code signing request.
        /// </summary>
        public void SendCompletedFailed(CodeSigningRequest request)
        {
            if ((bool)SendNotifications)
            {
                StringBuilder message = new StringBuilder("A request for code signing has failed for the following request: ");
                message.AppendLine(string.Empty);
                message.AppendLine(string.Empty);
                message.AppendLine(String.Format("ServerName:   {0}", ServerName));
                message.AppendLine(String.Format("Filename:   {0}", request.UnsignedFileName));
                message.AppendLine(String.Format("File Hash:  {0}", request.UnsignedFileHash));
                message.AppendLine(String.Format("Request Id: {0}", request.Id));
                message.AppendLine(string.Empty);
                message.AppendLine(String.Format("Please login to the Portal ({0}) for additional information regarding this request.", this.PortalUrl));
                message.AppendLine(string.Empty);
                message.AppendLine("Thank you,");
                message.AppendLine(string.Empty);
                message.AppendLine("CSG Administration ");

                Send(this.FromEmail, 
                     GetRecipientList(request), 
                     string.Format("Code Signing Request Failed: Request Id - {0}", request.Id), 
                     message.ToString());
            }
        }

        public void SendCertificateNotification(Certificate cert)
        {
            if ((bool)SendNotifications)
            {
                StringBuilder message = new StringBuilder("A certificate will be expired soon.  ");
                message.AppendLine("The following code was submitted: ");

                message.AppendLine(string.Empty);
                message.AppendLine(string.Empty);
                message.AppendLine(String.Format("ServerName:   {0}", ServerName));
                message.AppendLine(String.Format("Name:   {0}", cert.Name));
                message.AppendLine(String.Format("Issued To:  {0}", cert.IssuedTo));
                message.AppendLine(String.Format("Issued By: {0}", cert.IssuedBy));
                message.AppendLine(String.Format("Expired at: {0}", cert.NotAfter.ToString()));
                message.AppendLine(string.Empty);

                message.AppendLine(string.Empty);
                message.AppendLine(String.Format("You may login to the Portal ({0}) for additional information regarding this certificate.", this.PortalUrl));
                message.AppendLine(string.Empty);
                message.AppendLine("Thank you,");
                message.AppendLine(string.Empty);
                message.AppendLine("CSG Administration ");

                Send(this.FromEmail,
                     this.FromEmail,
                     String.Format("A certificate ({0}) will be expired soon at {1}.  ", cert.Name, ServerName),
                     message.ToString());
            }
        }


        /// <summary>
        /// Send email
        /// </summary>
        private void Send(string fromEmail, string toEmail, string subjectText, string bodyText)
        {
            this.FromEmail = fromEmail;
            this.ToEmail = toEmail;
            this.Subject = subjectText;
            this.Body = bodyText;

            if (RunThreaded)
            {
                ThreadStart ts = new ThreadStart(DoSend);
                Thread t = new Thread(ts);
                t.IsBackground = true;
                t.Start();
            }
            else
            {
                DoSend();
            }
        }

        private void DoSend()
        {
            try
            {
                lock (this)
                {
                    SmtpClient smtpClient = GetSmtpClient();

                    log.InfoFormat("Sending Email {0} from ", FromEmail);
                    // Create and send the Mail Message
                    using (MailMessage mailMessage = new MailMessage())
                    {
                        mailMessage.From = new MailAddress(FromEmail);
                        mailMessage.Subject = Subject;
                        mailMessage.Body = Body;

                        var toAddresses = ToEmail.Split(';').ToList();
                        log.InfoFormat("Sending Email {0}, {1}, {2}", mailMessage.From, mailMessage.Subject, ToEmail);

                        // remove duplicates frmo the to address list.
                        toAddresses = toAddresses.Distinct().ToList();

                        foreach (string toAddress in toAddresses)
                        {
                            if (!String.IsNullOrWhiteSpace(toAddress))
                            {
                                mailMessage.To.Add(new MailAddress(toAddress));
                            }
                        }

                        smtpClient.Send(mailMessage);
                    }
                }
            }
            catch (Exception e)
            {
                //Just logging the error.  Do not re-throw
                log.Error("Exception occurred sending email message.", e);
                log.ErrorFormat("Email Address Settings: {0}, {1}", FromEmail, ToEmail);
            }
        }

        private SmtpClient GetSmtpClient()
        {
            // Create an SMTP Client
            SmtpClient smtpClient = new SmtpClient();
            smtpClient.Host = MailServer;
            smtpClient.Port = Port;
            smtpClient.EnableSsl = (bool)UseSSL;

            if (!String.IsNullOrWhiteSpace(this.UserId))
            {
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                //string unencryptedPassword = Util.
                smtpClient.Credentials = new NetworkCredential(this.UserId, this.Password);
            }

            return smtpClient;
        }

        private string GetAdmins(CodeSigningRequest request)
        {
            string toList = string.Empty;
            ESPDbDomainService svc = new ESPDbDomainService();

            // include the user as a recipient.
            if (request.CreatedByUser != null &&
                request.CreatedByUser.Email != null &&
                request.CreatedByUser.Email.Length > 0)
            {
                toList = request.CreatedByUser.Email + ";";
                if (request.CreatedByUser.ProxyEmail != null)
                {
                    toList = toList + request.CreatedByUser.ProxyEmail + ";";
                }
            }

            // get the enterprise approvers for the request type
            List<UserProfileCodeSigningType> approvers = svc.GetUserProfileCodeSigningTypes("UserProfile").
                                                               Where(m => m.CodeSigningTypeId == request.CodeSigningTypeId &&
                                                                          m.UserSigningTypeRole == (int)UserActions.Approve2Requests).ToList<UserProfileCodeSigningType>();

            foreach (UserProfileCodeSigningType approver in approvers)
            {
                if (approver.UserProfile.Email != null && approver.UserProfile.Email.Length > 0)
                {
                    toList = toList + approver.UserProfile.Email + ";";
                    if (approver.UserProfile.ProxyEmail != null)
                    {
                        toList = toList + approver.UserProfile.ProxyEmail + ";";
                    }
                }
            }

            // add the admins
            var adminRoleId = svc.GetRoles().Where(r => r.Name == Constants.Roles.AppAdminRole).Select(r => r.Id).FirstOrDefault();

            List<UserProfileRole> admins = svc.GetUserProfileRoles("UserProfile").Where(upr => upr.RoleId == adminRoleId).ToList<UserProfileRole>();
            foreach (UserProfileRole admin in admins)
            {
                if (admin.UserProfile.Email != null && admin.UserProfile.Email.Length > 0)
                {
                    toList = toList + admin.UserProfile.Email + ";";
                    if (admin.UserProfile.ProxyEmail != null)
                    {
                        toList = toList + admin.UserProfile.ProxyEmail + ";";
                    }
                }
            }

            return toList;
        }
        private string GetRecipientList(CodeSigningRequest request, bool includeOfflineSigners=false )
        {

            ESPDbDomainService svc = new ESPDbDomainService();
            log.Debug("Getting recipient list for email notification.");

            /* 
             List<UserProfileCodeSigningType> usersToNotify = null;
             if (includeOfflineSigners)
             {
                 // emails go to requesting user and approvers for this type
                 usersToNotify = svc.GetUserProfileCodeSigningTypes().Where(upcst => upcst.CodeSigningTypeId == request.CodeSigningTypeId &&
                                                                     (upcst.UserSigningTypeRole == (int)UserActions.ManagerApproveRequests ||
                                                                      upcst.UserSigningTypeRole == (int)UserActions.Approve1Requests ||
                                                                      upcst.UserSigningTypeRole == (int)UserActions.OfflineSignRequests)).ToList();
             }
             else
             {
                 // emails go to requesting user and approvers for this type
                 usersToNotify = svc.GetUserProfileCodeSigningTypes().Where(upcst => upcst.CodeSigningTypeId == request.CodeSigningTypeId &&
                                                                     (upcst.UserSigningTypeRole == (int)UserActions.ManagerApproveRequests ||
                                                                      upcst.UserSigningTypeRole == (int)UserActions.Approve1Requests)).ToList();
             }
             */

            List<int> profileIds;
            List<UserProfile> approvingUsers = new List<UserProfile>();

            if (request.CodeSigningType.AllowApprover1Approval && request.Approver1ResponderId == null)
            {
                // get the names of the other users in the group
                profileIds = svc.GetUserProfileGroups().Where(m => m.GroupId == request.GroupId).Select(g => g.UserProfileId).ToList();
                if (request.CreatedByUserId.HasValue)
                {
                    profileIds.Remove(request.CreatedByUserId.Value);
                }
                approvingUsers = svc.GetUserProfiles().Where(m => profileIds.Contains(m.Id)).ToList();
            }

            if (request.CodeSigningType.AllowApprover2Approval && request.Approver2ResponderId == null)
            {
                // get the approver names
                profileIds = svc.GetCodeSigningTypeGroups().Where(m => m.CodeSigningTypeId == request.CodeSigningTypeId &&
                                                                m.IsApprover == true).
                                                              Select(m => m.GroupId).ToList();
                profileIds = svc.GetUserProfileGroups().Where(m => profileIds.Contains(m.GroupId)).Select(g => g.UserProfileId).ToList();

                approvingUsers.AddRange(svc.GetUserProfiles().Where(m => profileIds.Contains(m.Id)));
            }


            string toList = string.Empty;
            if (request.CreatedByUser != null && 
                request.CreatedByUser.Email != null && 
                request.CreatedByUser.Email.Length > 0)
            {
                toList = request.CreatedByUser.Email + ";";
                if ( request.CreatedByUser.ProxyEmail != null)
                {
                    toList = toList + request.CreatedByUser.ProxyEmail + ";";
                }

            }

            /*
            foreach (var approverToNotify in usersToNotify)
            {
                if (approverToNotify.UserProfile == null)
                {
                    log.Debug("Error retrieving user profile information for approvers");
                }
                else
                {
                    if (approverToNotify.UserProfile.Email != null && approverToNotify.UserProfile.Email.Length > 0)
                    {
                        toList = toList + approverToNotify.UserProfile.Email + ";";
                    }
                }
            }
            */
            foreach ( var uprofile in approvingUsers)
            {
                if (uprofile.Email != null && uprofile.Email.Length > 0)
                {
                    toList = toList + uprofile.Email + ";";
                }

            }

            char[] charsToTrim = { ';' };
            toList = toList.TrimEnd(charsToTrim).Replace("SMTP:", "").Replace("smtp:","");
            log.Debug("Recipient list for email notification includes: " + toList);

            return toList;
        }

        #endregion
    }
}
