﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using CSG.AppServer.Entities;
using CSG.AppServer.Db;
using CSG.AppServer.Security;
using CSG.AppServer.APITypes;
using CSG.AppServer.Reporting;
using CSG.AppServer.Signers;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Security.Cryptography.Pkcs;
using CSG.AppServer.Exceptions;

namespace CSG.AppServer.Managers
{
    // NOTE:  For future consideration
    // Move this to Util.Logging.ActivityLog

    public class APIManager
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly Type ThisDeclaringType = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;

        private ESPDbDomainService domainService = null;
        private Gatekeeper _gatekeeper = null;

        // static int ESPERROR_SUCCESS = 0;
        public static int WEBAPIERROR_INVALID_PROFILE = -1;
        public static int WEBAPIERROR_INVALID_CERTIFICATE = -2;
        public static int WEBAPIERROR_SIGNATURE_ERROR = -3;
        public static int WEBAPIERROR_INVALID_REQUEST = -4;
        public static int WEBAPIERROR_HASH_MISMATCH = -5;
        
        public static int WEBAPIERROR_INTERNAL_ERROR = -44;

        static APIManager()
        { 
        }

        /// <summary>
        /// Sessionless constructor, id is passed in through API/SAML token
        /// </summary>
        public APIManager(Gatekeeper gatekeeper)
        {
            _gatekeeper = gatekeeper;
            domainService = new ESPDbDomainService(_gatekeeper);
        }

        // ********************************************************************************
        #region API Methods
        // ********************************************************************************

        /// <summary>
        /// Fulfills the espListSigningTypes espDirect API function.  
        /// </summary>
        public APIListSigningTypes listSigningTypes()
        {

            UserProfile authenticatedUser;
            List<espDirectSigningType> signingTypeList = new List<espDirectSigningType>();
            csgAPIErrorInfo apiErrorInfo = new csgAPIErrorInfo
            {
                errorCode = 0,
                errorString = ""
            };

            try
            {
                authenticatedUser = _gatekeeper.GetCurrentUserProfile();

                // retreive the details for each signing type that is authorized, 
                // only return rawSign types.
                var groups = authenticatedUser.Groups
                            .Select(upg => upg.Group)
                            .Where(g => g.IsDeleted == false)
                            .ToList();
                foreach (var group in groups)
                {
                    var authorizedSigningTypes = domainService.GetCodeSigningTypeGroups()
                                                              .Where(cstg => cstg.GroupId == group.Id)
                                                              .Select(s => s.CodeSigningType).ToList();
                    if (authorizedSigningTypes.Count() > 0)
                    {
                        foreach (CodeSigningType cst in authorizedSigningTypes)
                        {
                            var signingType = new espDirectSigningType
                            {
                                signingProfileName = cst.Name,
                                signingProfileId = cst.Id,
                                certificate = cst.SigningCertificate.MimeEncoded,
                                groupId = group.Id,
                                groupName = group.Name
                               
                            };
                            signingTypeList.Add(signingType);
                        }
                    }
                }
                var groupIds = groups.Select(g => g.Id).ToList();

            }
            catch (Exception ex)
            {
                // log exeception 
                apiErrorInfo.errorCode = WEBAPIERROR_INTERNAL_ERROR;  
                apiErrorInfo.errorString = "Internal server error";
                log.Error("listSigningTypes", ex);

            }

            APIListSigningTypes signingTypesToReturn = new APIListSigningTypes
            {
                returnCode = apiErrorInfo,
                signingProfiles = signingTypeList.ToArray()
            };
            return signingTypesToReturn;
        }

        /// <summary>
        /// 
        /// codesignRequestCreateWithFile
        /// 
        /// Fulfills the ccssCodesignRequestSubmitFile ccssDirect API function.  
        /// 
        /// </summary>
        public APICodesignRequestStatus codesignRequestCreateWithFile(codesignRequestCreateParams createRequest, string fullPath, string uploadedFilename)
        {
            csgAPIErrorInfo apiErrorInfo = new csgAPIErrorInfo
            {
                errorCode = 0,
                errorString = ""
            };
            CodeSigningRequestManager manager = new CodeSigningRequestManager(_gatekeeper);
            UserProfile authenticatedUser;

            authenticatedUser = _gatekeeper.GetCurrentUserProfile();

            // get the signing type from the DB
            CodeSigningType cst = domainService.GetCodeSigningTypeById(createRequest.signingProfileId, "CreatedByUser");

            try
            {
                _gatekeeper.ValidateUserDeveloperInGroup(createRequest.groupId);
                _gatekeeper.ValidateGroupAuthorizedForType(createRequest.groupId, createRequest.signingProfileId);
            }
            catch (Exception ex)
            {
                ActivityLog.LogException(authenticatedUser.DomainAndLoginId, "",0,  "codesignRequestCreate", "CreateCodeSigningRequest", "Invalid code signing type ID", "", ex);
                throw new InvalidRequestException();
            }

            CodeSigningRequest csRequest; /* = new CodeSigningRequest();
            
            csRequest.CreatedByUserId = authenticatedUser.Id;
            csRequest.CodeSigningTypeId = createRequest.signingProfileId;
            csRequest.UnsignedFileName = uploadedFilename;
            csRequest.GroupId = createRequest.groupId;
            csRequest.ReleaseName = createRequest.releaseName;
            csRequest.ProjectName = createRequest.projectName;
            */

            var uploadedFileHash = CodeSigningRequestManager.GetFileHashSha256(fullPath);

            if ( createRequest.fileHashSha256 == null) throw new FileHashMismatchException();
            if (uploadedFileHash.ToLower() != createRequest.fileHashSha256.ToLower())
            {
                ActivityLog.LogException(authenticatedUser.DomainAndLoginId, "", 0, "codesignRequestCreate", "CreateCodeSigningRequest", "Uploaded file hash mismatch");
                throw new FileHashMismatchException();
            }

            try
            {
                //csRequest = manager.CreateCodeSigningRequest(csRequest, fullPath);
                csRequest = manager.CreateCodeSigningRequest(createRequest.signingProfileId, createRequest.groupId, fullPath, uploadedFilename, true, createRequest.releaseName, createRequest.projectName);
            }
            catch (Exception ex)
            {
                ActivityLog.LogException(authenticatedUser.DomainAndLoginId, "", 0, "codesignRequestCreate", "CreateCodeSigningRequest", "", "", ex);
                throw;
            }

            return codesignRequestStatus(csRequest.Id);
        }

        /// <summary>
        /// Fulfills the espCodesignRequestCreate espDirect API function.  
        /// </summary>
        public APICodesignRequestStatus espCodesignRequestCreate(codesignRequestCreateParams createRequest)
        {
            UserProfile authenticatedUser;
            csgAPIErrorInfo apiErrorInfo = new csgAPIErrorInfo
            {
                errorCode = 0,
                errorString = ""
            };

            authenticatedUser = _gatekeeper.GetCurrentUserProfile();

            // get the signing type from the DB
            CodeSigningType cst = domainService.GetCodeSigningTypeById(createRequest.signingProfileId, "CreatedByUser");

            CodeSigningRequest csRequest = new CodeSigningRequest();


            csRequest.CreatedByUserId = authenticatedUser.Id;
            csRequest.Status = CodeSigningRequestStatus.Submitted;
            csRequest.CodeSigningTypeId = createRequest.signingProfileId;
            csRequest.UnsignedFileName = "";
            csRequest.ReleaseName = createRequest.releaseName;
            csRequest.ProjectName = createRequest.projectName;

            // create the entry in the database
            CodeSigningRequestManager manager = new CodeSigningRequestManager();
            csRequest = manager.CreateCodeSigningRequest(csRequest);

            // pull the request status from the Db
            return codesignRequestStatus(csRequest.Id);
        }

        /// <summary>
        /// Fulfills the espRequestStatus espDirect API function.  
        /// </summary>
        public APICodesignRequestStatus codesignRequestStatus(int requestId)
        {
            csgAPIErrorInfo apiErrorInfo = new csgAPIErrorInfo
            {
                errorCode = 0,
                errorString = ""
            };
            espCodesignRequest apiRequest = null;


            // retreive the request from the database
            CodeSigningRequest csRequest = domainService.GetCodeSigningRequestById(requestId);

            // fill in the JSON response
            apiRequest = new espCodesignRequest {
                requestStatus = csRequest.Status.ToString(),
                requestId = csRequest.Id,
                signedFileHashSha256 = csRequest.SignedFileHash,
                signingProfileId = csRequest.CodeSigningTypeId,
                projectName = csRequest.ProjectName,
                releaseName = csRequest.ReleaseName,
                scanningCommandResultCode = csRequest.ScanningCommandResultCode,
                signingCommandResultCode = csRequest.SigningCommandResultCode
            };

            APICodesignRequestStatus statusResponse = new APICodesignRequestStatus
            {
                returnCode = apiErrorInfo,
                request = apiRequest
            };

            return statusResponse;
        }

        /// <summary>
        /// Verifies that the requestId is valid and that the user may access this request
        /// </summary>
        public void espValidateRequestId(int requestId)
        {
            // retreive the request from the database
            CodeSigningRequest csRequest = domainService.GetCodeSigningRequestById(requestId);
        }

        /// <summary>
        /// 
        /// espCodesignRequestSubmitFile
        /// 
        /// Fulfills the espCodesignRequestSubmitFile espDirect API function.  
        /// 
        /// </summary>
        public APICodesignRequestStatus espCodesignRequestSubmitFile(int requestId, string fullPath, string uploadedFilename)
        {
            csgAPIErrorInfo apiErrorInfo = new csgAPIErrorInfo
            {
                errorCode = 0,
                errorString = ""
            };
            espCodesignRequest apiRequest = null;
            CodeSigningRequestManager manager = new CodeSigningRequestManager(_gatekeeper);

            // retreive the request from the database
            CodeSigningRequest csRequest = manager.GetCodeSigningRequestById(requestId);

            // TODO: verify the status of the request

            try
            {
                csRequest = manager.UpdateCodeSigningRequest(csRequest, fullPath, uploadedFilename);
            } 
            catch (Exception ex)
            {
                apiErrorInfo.errorCode = -44;
                apiErrorInfo.errorString = ex.Message;
            }

            // fill in the JSON response
            apiRequest = new espCodesignRequest { requestStatus = csRequest.Status.ToString(), requestId = csRequest.Id };

            APICodesignRequestStatus statusResponse = new APICodesignRequestStatus
            {
                returnCode = apiErrorInfo,
                request = apiRequest
            };
            return statusResponse;
        }

        /// <summary>
        /// 
        /// espCodesignRequestDownloadFile
        /// 
        /// </summary>
        public string codesignRequestDownloadFile(int requestId)
        {
            FileInfo file;

            CodeSigningRequestManager manager = new CodeSigningRequestManager(_gatekeeper);

            // retreive the request from the database
            try
            {
                CodeSigningRequest csRequest = manager.GetCodeSigningRequestById(requestId);
                // only return the file if the request is completed
                if (csRequest.Status != CodeSigningRequestStatus.CompletedSuccess)
                {
                    // the request is not valid
                    log.Info("The request does not have a signed file to download: " + requestId);
                    return null;
                }
                else
                {
                    file = manager.GetFile(requestId, "S");
                }
            }
            catch (Exception)
            {
                // the request is not valid
                log.Info("The request could not be retreived from the database using ID: " + requestId);
                return null;
            }


            return file.FullName;
        }

        /// <summary>
        /// 
        /// espSignHash
        /// 
        /// </summary>
        public APISignHashResult espSignHash(espSignHashRequest request)
        {
            int signingTypeId=0;
            csgAPIErrorInfo apiErrorInfo = new csgAPIErrorInfo
            {
                errorCode = 0,
                errorString = ""
            };
            espSignHashResponse signedHash = new espSignHashResponse();

            CodeSigningRequestManager manager = new CodeSigningRequestManager(_gatekeeper);

            var authenticatedUser = _gatekeeper.GetCurrentUserProfile();
            if (authenticatedUser == null)
            {
                throw new UserRightsException("User profile not found.");
            }

            var signingTypeSearch = domainService.GetCodeSigningTypes(null)
                    .Where(cst => request.signingTypeGuid.Equals(cst.Guid))
                    .Select(cst => cst);

            if (signingTypeSearch.Count() <= 0)
            {
                apiErrorInfo.errorCode = WEBAPIERROR_INVALID_PROFILE;
                apiErrorInfo.errorString = "Code signing profile is invalid.";
                log.Error("User requested signature with an invalid signing profile.");
                // throw unauthorized to the user
                throw new UserRightsException("User signing request is not valid.");
            }

            var signingType = signingTypeSearch.Cast<CodeSigningType>().First();

            // verify the approvals for this signing profile and user are in place
            if (!manager.HashRequestApproved(signingType, authenticatedUser))
            {
                log.Info("User sign hash request is not approved.");
                throw new UserRightsException("Request is not approved.");
            }

            try
            {
                // create a signing request for signing
                signedHash.signatureValue = "";
                X509Store my = new X509Store(StoreName.My, StoreLocation.LocalMachine);
                my.Open(OpenFlags.ReadOnly);

                var signingCert = my.Certificates.Find(X509FindType.FindByThumbprint, signingType.SigningCertificate.Thumbprint, false);

                if (signingCert.Count == 0)
                {
                    apiErrorInfo.errorCode = WEBAPIERROR_INVALID_CERTIFICATE;
                    apiErrorInfo.errorString = "Signing certificate not configured.";
                    log.Error("Signing certificate was not found in the certificate store or is not configured properly.");
                }
                else
                {
                    signingTypeId = signingType.Id;
                    var hashBin = Convert.FromBase64String(request.hashValue);
                    RSACryptoServiceProvider rsaCSP = new RSACryptoServiceProvider();

                    // GetRSAPrivateKey works with both CSPs and KSPs
                    var key = signingCert[0].GetRSAPrivateKey();
                    HashAlgorithmName algName;
                    switch (request.hashAlg.ToUpper())
                    {
                        case "MD5":
                            algName = HashAlgorithmName.MD5;
                            break;
                        case "SHA1":
                            algName = HashAlgorithmName.SHA1;
                            break;
                        case "SHA256":
                            algName = HashAlgorithmName.SHA256;
                            break;
                        case "SHA384":
                            algName = HashAlgorithmName.SHA384;
                            break;
                        case "SHA512":
                            algName = HashAlgorithmName.SHA512;
                            break;
                        default:
                            algName = HashAlgorithmName.SHA256;
                            break;
                    }
                    byte[] sigVal = key.SignHash(hashBin, algName, RSASignaturePadding.Pkcs1);

                    signedHash.signatureValue = Convert.ToBase64String(sigVal);

                    CodeSigningRequest hashCsr = new CodeSigningRequest() {
                                   ReleaseName = request.releaseName,
//TBD                                   ProjectName = request.ProjectName,
                                   CodeSigningTypeId = signingTypeId,
                                   SignedFileHash = signedHash.signatureValue,
                                   UnsignedFileName = "HASH_SIGNING",
                                   UnsignedFileHash = request.hashValue
                                   };
                    CodeSigningRequest csr = manager.CreateHashSigningRequest(hashCsr);

                    signedHash.requestId = csr.Id;
                }
            }
            catch (Exception ex)
            {
                apiErrorInfo.errorCode = WEBAPIERROR_SIGNATURE_ERROR;
                apiErrorInfo.errorString = "Exception occured when signing hash value: " + ex.Message;
                log.Error("An error occured when attempting fulling a sign hash request.", ex);
            }


            APISignHashResult result = new APISignHashResult
            {
                returnCode = apiErrorInfo,
                response = signedHash
            };

            return result;
        }
        #endregion
        public class InvalidRequestException : Exception { }

    }
}
