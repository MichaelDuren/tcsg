using System;
using System.Linq;
using CSG.AppServer.Db;
using CSG.AppServer.Entities;
using Util;
using Util.Collections.Generic;


namespace CSG.AppServer.Managers
{
    public class SystemSettingsManager
    {
        // *********************************************************************************
        #region Properties
        // *********************************************************************************

        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly Type ThisDeclaringType = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;

        protected static Cache<string, SystemSetting> settingCache = new Cache<string, SystemSetting>(CacheType.AbsoluteExpiration);

        private static SystemSettingsManager _current;
        public static SystemSettingsManager Current
        {
            get
            {
                if (_current == null)
                {
                    _current = new SystemSettingsManager();
                }
                return _current;
            }
        }

        #endregion

        // *********************************************************************************
        #region Constructor and related
        // *********************************************************************************

        private SystemSettingsManager() { }

        #endregion

        // *********************************************************************************
        #region GetByName methods
        // *********************************************************************************

        public SystemSetting GetBySettingName(string name, bool failIfNotFound = false)
        {
            lock (settingCache)
            {
                if (settingCache.ContainsKey(name))
                {
                    return settingCache[name];
                }
                else
                {
                    SystemSetting systemSetting = GetBySettingNameFromDb(name, failIfNotFound);

                    UpdateCacheItem(name, systemSetting);

                    return systemSetting;
                }
            }
        }

        private SystemSetting GetBySettingNameFromDb(string name, bool failIfNotFound)
        {
            ESPDbDomainService domainService = new ESPDbDomainService();

            var systemSettings = domainService.GetSystemSettings()
                .Where(s => s.Name == name).ToList();

            if (systemSettings.Count == 0)
            {
                if (failIfNotFound)
                {
                    throw new InvalidOperationException(String.Format("SystemSetting with name: {0} does not exist in the database.", name));
                }
                else
                {
                    return null;
                }
            }
            else if (systemSettings.Count == 1)
            {
                return systemSettings[0];
            }
            else
            {
                log.ErrorFormat("Encountered more than one System Setting row with the same name ({0}).  Deleted the second one, kept the first.", name);

                domainService.DeleteSystemSetting(systemSettings[1]);
                domainService.SaveChanges();

                return systemSettings[0];
            }
        }

        #endregion

        // *********************************************************************************
        #region Insert Update methods
        // *********************************************************************************

        public SystemSetting GetOrCreateSystemSetting(string settingName, SystemSettingDataType dataType, string stringValue = null, int? integerValue = null, bool? booleanValue = null, double? doubleValue = null, DateTime? dateTimeValue = null, bool excludeFromSettingsUI = false, string category = null, string description = null, string passwordValue = null)
        {
            SystemSetting systemSetting;

            systemSetting = GetBySettingName(settingName);

            if (systemSetting == null)
            {
                systemSetting = CreateSystemSetting(settingName, dataType, stringValue, integerValue, booleanValue, doubleValue, dateTimeValue, excludeFromSettingsUI, category, description, passwordValue);
            }

            return systemSetting;
        }

        public SystemSetting CreateSystemSetting(string settingName, SystemSettingDataType dataType, string stringValue = null, int? integerValue = null, bool? booleanValue = null, double? doubleValue = null, DateTime? dateTimeValue = null, bool excludeFromSettingsUI = false, string category = null, string description = null, string passwordValue = null)
        {
            Guard.ArgumentNotBlankOrNull(settingName, "settingName");

            SystemSetting systemSetting = new SystemSetting
            {
                Name = settingName,
                Description = description,
                Category = category,
                DataType = dataType,
                StringValue = stringValue,
                IntegerValue = integerValue,
                BooleanValue = booleanValue,
                DateValue = dateTimeValue,
                DoubleValue = doubleValue,
                PasswordValue = passwordValue,
                ExcludeFromSettingsUI = excludeFromSettingsUI
            };

            return CreateSystemSetting(systemSetting);
        }

        public SystemSetting CreateSystemSetting(SystemSetting systemSetting)
        {
            Guard.ArgumentNotBlankOrNull(systemSetting.Name, "systemSetting.Name");

            // Set logCRUDActivity to false - to avoid issues with initial bootstrap of the app... chicken and egg with settings, etc.
            ESPDbDomainService domainService = new ESPDbDomainService(logCRUDActivity: false);

            if (systemSetting.DataType == SystemSettingDataType.Password)
            {
                if (!String.IsNullOrWhiteSpace(systemSetting.PasswordValue))
                {
                    systemSetting.PasswordValue = Util.CryptographyTools.Encrypt(systemSetting.PasswordValue);
                }
            }

            domainService.InsertSystemSetting(systemSetting);
            domainService.SaveChanges();

            UpdateCacheItem(systemSetting.Name, systemSetting);

            return systemSetting;
        }

        public void UpdateSystemSetting(SystemSetting systemSetting)
        {
            Guard.ArgumentNotBlankOrNull(systemSetting.Name, "systemSetting.Name");

            ESPDbDomainService domainService = new ESPDbDomainService();

            if (systemSetting.DataType == SystemSettingDataType.Password)
            {
                SystemSetting originalSetting = domainService.GetSystemSettingById(systemSetting.Id);
                string originalPassword = originalSetting.PasswordValue;
                domainService.DbContext.ObjectContext.Detach(originalSetting);

                if (systemSetting.PasswordValue != originalPassword)
                {
                    if (!String.IsNullOrWhiteSpace(systemSetting.PasswordValue))
                    {
                        systemSetting.PasswordValue = CryptographyTools.Encrypt(systemSetting.PasswordValue);
                    }
                    else
                    {
                        systemSetting.PasswordValue = null;
                    }
                }
            }
            else
            {
                if ((systemSetting.DataType == SystemSettingDataType.Integer && systemSetting.IntegerValue == null) ||
                    (systemSetting.DataType == SystemSettingDataType.Double && systemSetting.DoubleValue == null) ||
                    (systemSetting.DataType == SystemSettingDataType.DateTime && systemSetting.DateValue == null))
                {
                    throw new InvalidSettingValue();
                }

                if (systemSetting.DataType == SystemSettingDataType.Integer && systemSetting.IntegerValue == 0)
                {
                    throw new InvalidSettingValue();
                }
            }

            domainService.UpdateSystemSetting(systemSetting);
            domainService.SaveChanges();

            UpdateCacheItem(systemSetting.Name, systemSetting);
        }

        #endregion

        // *********************************************************************************
        #region Cache methods
        // *********************************************************************************

        public static void ClearCache()
        {
            lock (settingCache)
            {
                settingCache.Clear();
            }
        }

        protected void UpdateCacheItem(string name, SystemSetting systemSetting)
        {
            lock (settingCache)
            {
                if (settingCache.ContainsKey(name))
                {
                    settingCache.Remove(name);
                    settingCache.Add(name, systemSetting, AppServerContext.CacheMinutes);
                }
                else
                {
                    //If the DB is first being built, this setting won't exist so defaulting
                    if (name == Constants.SettingNames.CacheMinutes)
                    {
                        settingCache.Add(name, systemSetting, 5);
                    }
                    else
                    {
                        settingCache.Add(name, systemSetting, AppServerContext.CacheMinutes);
                    }
                }
            }
        }

        #endregion
    }

    public class InvalidSettingValue : Exception { }

}

