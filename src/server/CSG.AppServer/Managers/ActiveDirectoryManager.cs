//http://msdn.microsoft.com/en-us/magazine/cc135979.aspx
//http://msdn.microsoft.com/en-us/library/system.directoryservices.accountmanagement.aspx

using CSG.AppServer.Entities;
using System;
using System.Collections.Generic;
using System.Configuration.Provider;
using System.DirectoryServices;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using Util;

namespace CSG.AppServer.Managers
{
    public enum SearchTargetType
    {
        User,
        Group
    }

    public class ActiveDirectoryManager : IDisposable
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private const string QUERY_ERROR_MESSAGE = "Unable to query Active Directory";

        private Dictionary<string, PrincipalContext> principalContextsByDomain = new Dictionary<string, PrincipalContext>();

        // Used for testing - to allows testing against the local machine context
        private bool useMachineContext = false;

        PrincipalContext principalContext = null;
        // ********************************************************************************
        #region Constructor, etc.
        // ********************************************************************************

        public ActiveDirectoryManager()
        {
            principalContext = new PrincipalContext(ContextType.Domain,
                                                            AppServerContext.ADDomainOrServer,
                                                            AppServerContext.ADContainer);
            if (principalContext == null)
            { 
                throw new Exception("Cannot obtain the principal context for AD searches.");
            }
        }

        public ActiveDirectoryManager(bool useMachineContext)
        {
            this.useMachineContext = useMachineContext;
        }

        public void Dispose()
        {
            List<string> domainNames = principalContextsByDomain.Keys.ToList();
            foreach (string domainName in domainNames)
            {
                PrincipalContext pc = principalContextsByDomain[domainName];
                principalContextsByDomain.Remove(domainName);
                pc.Dispose();
            }
            if (principalContext != null)
            {
                principalContext.Dispose();
            }
        }

        #endregion

        // ********************************************************************************
        #region Login
        // ********************************************************************************

        /// <summary>
        /// Login against the domain provided with the username and password provided
        /// </summary>
        public bool Login(string domain, string username, string password)
        {
            PrincipalContext principalContext = GetPrincipalContext(domain);
            bool isValid = principalContext.ValidateCredentials(username, password);
            return isValid;
        }

        #endregion

        // ********************************************************************************
        #region User Methods
        // ********************************************************************************

        /// <summary>
        /// Gets a User Principal for the specified Identity Value.
        ///     By default - the identity value is LoginId - because IdentityType identityType = IdentityType.SamAccountName.
        ///     However - you can also lookup by Sid by setting identityType to Sid
        ///     If user does not exist - it returns null
        /// </summary>
        public UserPrincipal GetUserPrincipal(string domain, string identityValue, IdentityType identityType = IdentityType.SamAccountName)
        {
            try
            {
                log.DebugFormat("GetUserPrincipal: Domain - {0}, ident - {1}", domain, identityValue);
                if (String.IsNullOrWhiteSpace(identityValue))
                {
                    return null;
                }
                else
                {
                    PrincipalContext principalContext = GetPrincipalContext(domain);
                    UserPrincipal userPrincipal = UserPrincipal.FindByIdentity(principalContext, identityType, identityValue);
                    return userPrincipal;
                }
            }
            catch (Exception ex)
            {
                log.Error(QUERY_ERROR_MESSAGE, ex);
                throw new ProviderException(QUERY_ERROR_MESSAGE, ex);
            }
        }

        /// <summary>
        /// Gets a User Principal for the specified Identity Value.
        ///     By default - the identity value is LoginId - because IdentityType identityType = IdentityType.SamAccountName.
        ///     However - you can also lookup by Sid by setting identityType to Sid
        ///     If user does not exist - it returns null
        /// </summary>
        public UserProfile GetUserPrincipalData(UserProfile userProfile, string identityValue)
        {
            try
            {
                log.DebugFormat("GetUserPrincipal: ident - {0}", identityValue);


                if (String.IsNullOrWhiteSpace(identityValue))
                {
                    return null;
                }
                else
                {
                    try
                    {
                        
                        PrincipalContext context = new PrincipalContext(ContextType.Domain, 
                                                                        AppServerContext.ADDomainOrServer, 
                                                                        AppServerContext.ADContainer);
                        if (context == null)
                        {
                            log.Error("Unable to obtain a context for this domain or server: " + AppServerContext.ADDomainOrServer);
                            userProfile = null;
                        }
                        else
                        {
                            using (context)
                            {
                                UserPrincipal userPrincipal;
                                userPrincipal = UserPrincipal.FindByIdentity(context, identityValue);
                                if (userPrincipal != null)
                                {
                                    userProfile.Email = userPrincipal.EmailAddress;
                                    userProfile.FirstName = userPrincipal.GivenName;
                                    userProfile.LastName = userPrincipal.Surname;
                                    userProfile.DisplayName = userProfile.FullName;
                                    var contextDn = CertificateManager.ParseDistinguishedName(userPrincipal.DistinguishedName);
                                    string domainName = string.Empty;
                                    foreach (var name in contextDn)
                                    {
                                        if (name.Key.ToUpper() == "DC")
                                        {
                                            if (domainName.Length > 0)
                                            {
                                                domainName += ".";
                                            }
                                            domainName += name.Value;
                                        }
                                    }
                                    userProfile.Domain = domainName; 

                                    PropertyCollection properties = ((DirectoryEntry)userPrincipal.GetUnderlyingObject()).Properties;
                                    foreach (object property in properties["proxyaddresses"])
                                    {
                                        if (userProfile.ProxyEmail == null)
                                        { userProfile.ProxyEmail = property.ToString(); }
                                        else
                                        {
                                            userProfile.ProxyEmail = userProfile.ProxyEmail + ";" + property.ToString();
                                        }
                                    }

                                }
                                else
                                {
                                    log.Error("Unable to locate user in Active Directory: " + identityValue);
                                    userProfile = null;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("ERROR Exception: " + ex.Message);
                    }
                    return userProfile;
                }
            }
            catch (Exception ex)
            {
                log.Error(QUERY_ERROR_MESSAGE, ex);
                throw new ProviderException(QUERY_ERROR_MESSAGE, ex);
            }
        }

        /// <summary>
        /// Gets a Group Principal for the specified Identity Value.
        ///     By default - the identity value is Group Name - because IdentityType identityType = IdentityType.SamAccountName.
        ///     However - you can also lookup by Sid by setting identityType to Sid
        ///     If group does not exist - it returns null
        /// </summary>
        public GroupPrincipal GetGroupPrincipal(string domain, string identityValue, IdentityType identityType = IdentityType.SamAccountName)
        {
            try
            {
                PrincipalContext principalContext = GetPrincipalContext(domain);
                GroupPrincipal groupPrincipal = GroupPrincipal.FindByIdentity(principalContext, identityType, identityValue);
                return groupPrincipal;
            }
            catch (Exception ex)
            {
                log.Error(QUERY_ERROR_MESSAGE, ex);
                throw new ProviderException(QUERY_ERROR_MESSAGE, ex);
            }
        }

        public static List<GroupPrincipal> GetGroupPrincipalsForUserPrincipal(UserPrincipal userPrincipal, bool recurse = false)
        {
            List<GroupPrincipal> groupPrincipals = userPrincipal
                    .GetGroups()
                    .Cast<GroupPrincipal>()
                    .ToList();

            if (recurse)
            {
                // Add each groups sub groups into the groupList
                foreach (var groupPrincipal in groupPrincipals.ToList())
                {
                    RecurseGroup(groupPrincipal, groupPrincipals);
                }
            }

            return groupPrincipals;
        }

        private static void RecurseGroup(GroupPrincipal groupPrincipal, List<GroupPrincipal> groupPrincipals)
        {
            List<GroupPrincipal> recursiveGroups = groupPrincipal
                                    .GetGroups()
                                    .Cast<GroupPrincipal>()
                                    .ToList();

            groupPrincipals.AddRange(recursiveGroups.Except(groupPrincipals));

            foreach (var _groupPrincipal in recursiveGroups)
            {
                RecurseGroup(_groupPrincipal, groupPrincipals);
            }
        }
        #endregion

        // ********************************************************************************
        #region Support Methods
        // ********************************************************************************

        private PrincipalContext GetPrincipalContext(string domain)
        {
            lock (principalContextsByDomain)
            {
                Guard.ArgumentNotNull(domain, "domain");

                if (principalContextsByDomain.ContainsKey(domain.ToUpper()))
                {
                    return principalContextsByDomain[domain.ToUpper()];
                }
                else
                {
                    PrincipalContext principalContext;
                    if (useMachineContext)
                    {
                        principalContext = new PrincipalContext(ContextType.Machine, domain);
                    }
                    else
                    {
                        principalContext = new PrincipalContext(ContextType.Domain, domain);
                    }

                    principalContextsByDomain.Add(domain.ToUpper(), principalContext);
                    return principalContext;
                }
            }
        }

        #endregion

    }


}
