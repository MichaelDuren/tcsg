﻿using System;
using System.Diagnostics;
using CSG.AppServer.Db;
using CSG.AppServer.Entities;
using Util;

namespace CSG.AppServer.Managers
{
    // NOTE:  For future consideration
    // Move this to Util.Logging.ActivityLog

    public static class ActivityLog
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // Used for Logging - to tell ESPLogWrapper wrapper to use THIS class as the boundary between what is being logged and the internals - so that all logging statements show the class / method / line number of whatever Invoked this class
        private static readonly Type ThisDeclaringType = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;

        private const string LogEntryLevel_DEBUG = "DEBUG";
        private const string LogEntryLevel_INFO = "INFO";
        private const string LogEntryLevel_WARN = "WARN";
        private const string LogEntryLevel_ERROR = "ERROR";
        private const string LogEntryLevel_FATAL = "FATAL";

        public const int LogEventId_LOG_ENTRY                     = 0;
        public const int LogEventId_LOGIN                         = 1001;
        public const int LogEventId_CHANGE_SYSTEM_SETTING         = 1002;
        public const int LogEventId_CHANGE_TOOL_CONFIG            = 1003;
        public const int LogEventId_SAVE_PROFILE                  = 1004;
        public const int LogEventId_SAVE_CERTIFICATE              = 1005;
        public const int LogEventId_SYNC_DIRECTORY                = 1006;
        public const int LogEventId_RIGHTS_EXCEPTION              = 1007;
        public const int LogEventId_DATABASE_EXCEPTION            = 1008;
        public const int LogEventId_MAINTENANCE                   = 1009;
        public const int LogEventId_STORAGE_LOCATION_ERROR        = 1010;

        public const int LogEventId_CSEXECUTIVE_STARTUP           = 2001;
        public const int LogEventId_CSEXECUTIVE_STARTUP_EXCEPTION = 2002;
        public const int LogEventId_CSEXECUTIVE_SHUTDOWN          = 2003;
        public const int LogEventId_CSEXECUTIVE_EXCEPTION         = 2004;

        static ActivityLog()
        {
        }

        // ********************************************************************************
        #region Log Entries
        // ********************************************************************************

        /// <summary>
        /// This method will return the name of the entity.  Some entities are dynamically generated proxies that inherit from the true entity.  This method's
        /// purpose is to get the actual name of the entity regardless of whether it is a dynamically generated proxy or not
        /// </summary>
        private static string GetEntityName(IEntity entity)
        {
            Type type = entity.GetType();

            if (type.Namespace != null && type.Namespace.Contains("DynamicProxies"))
            {
                if (type.BaseType != null)
                {
                    return type.BaseType.Name;
                }
            }

            return type.Name;
        }

        public static void LogEntityActivity(UserProfile userProfile, IEntity entity, CudOperation cudOperation)
        {
            Guard.ArgumentNotNull(entity, "entity");

            Log(userProfile: userProfile, 
                eventId: 0,
                activityType: string.Format("Entity Activity ({0})", GetEntityName(entity)),
                activitySubType: cudOperation.ToString(),
                logEntryLevel: LogEntryLevel_DEBUG,
                text1: string.Format("Id ({0})", entity.Id)
                );
        }

        public static void LogExecutiveActivity(string logSource, int eventId, string activitySubType = null, string text1 = null, int? elapsedMilliseconds = null)
        {
            Log(userLogin: null,
                userName: "CS EXECUTIVE",
                activityType: logSource,
                activitySubType: activitySubType,
                text1: text1,
                elapsedMilliseconds: elapsedMilliseconds,
                eventId: eventId
            );
        }

        public static void Log(UserProfile userProfile, int eventId, string activityType, string activitySubType, string text1, string text2 = null, string text3 = null, string details = null, int? elapsedMilliseconds = null, Type callerStackBoundaryDeclaringType = null, string logEntryLevel = LogEntryLevel_INFO)
        {
            string userLogin = null;
            string userName = null;
            string activeDirectorySId = null;

            if (userProfile != null)
            {
                if (userProfile.DomainAndLoginId != null)
                {
                    userLogin = userProfile.DomainAndLoginId.ToString();
                }
                userName = userProfile.FullName;
                activeDirectorySId = userProfile.ActiveDirectorySId;
            }

            Log(userLogin: userLogin,
                userName: userName,
                eventId: eventId,
                activityType: activityType,
                activitySubType: activitySubType,
                text1: text1,
                text2: text2,
                text3: text3,
                details: details,
                elapsedMilliseconds: elapsedMilliseconds,
                callerStackBoundaryDeclaringType: callerStackBoundaryDeclaringType,
                activeDirectorySId: activeDirectorySId,
                logEntryLevel: logEntryLevel
                );
        }

        public static void Log(DomainAndLoginId domainAndLoginId, int eventId, string userName, string activityType, string activitySubType, string text1, string details = null, Type callerStackBoundaryDeclaringType = null)
        {
            string userLogin = null;
            string activeDirectorySId = null;
            if (domainAndLoginId != null)
            {
                userLogin = domainAndLoginId.ToString();
                activeDirectorySId = domainAndLoginId.ActiveDirectorySId;
            }

            Log(userLogin: userLogin,
                userName: userName,
                activityType: activityType,
                activitySubType: activitySubType,
                text1: text1,
                details: details,
                callerStackBoundaryDeclaringType: callerStackBoundaryDeclaringType,
                activeDirectorySId: activeDirectorySId
                );
        }

        public static void LogException(DomainAndLoginId domainAndLoginId, string userName, int eventId, 
                                        string activityType, string activitySubType, string text1, 
                                        string text2 = null, Exception exception = null, 
                                        Type callerStackBoundaryDeclaringType = null)
        {
            string userLogin = null;
            if (domainAndLoginId != null)
            {
                userLogin = string.Format(@"{0}\{1}", domainAndLoginId.Domain, domainAndLoginId.LoginId);
            }

            string details = null;
            if (exception != null)
            {
                details = exception.ToString();
            }

            if (activityType == null)
            {
                activityType = "EXCEPTION";
            }

            // activity log exceptions may not have DB access, so
            // log to file and event logs only.
            LogNoDb(userLogin: userLogin,
                userName: userName,
                eventId: eventId,
                activityType: activityType,
                activitySubType: activitySubType,
                text1: text1,
                details: details,
                logEntryLevel: LogEntryLevel_ERROR,
                exception: exception,
                callerStackBoundaryDeclaringType: callerStackBoundaryDeclaringType
                );
        }

        public static void LogNoDb(string userLogin, string userName, string activityType,
                              string activitySubType = null, string text1 = null, string text2 = null,
                              string text3 = null, string details = null, int? elapsedMilliseconds = null,
                              string logEntryLevel = LogEntryLevel_INFO, Exception exception = null,
                              Type callerStackBoundaryDeclaringType = null, string activeDirectorySId = null,
                              int eventId = 0)
        {
            ActivityLogEntry logEntry = new ActivityLogEntry
            {
                UserLogin = StringTools.Truncate(userLogin, 50),
                UserName = StringTools.Truncate(userName, 100),
                ActivityType = StringTools.Truncate(activityType, 100),
                ActivitySubType = StringTools.Truncate(activitySubType, 300),
                Text1 = StringTools.Truncate(text1, 2000),
                Text2 = StringTools.Truncate(text2, 2000),
                Text3 = StringTools.Truncate(text3, 2000),
                Details = details,
                ElapsedMilliseconds = elapsedMilliseconds,
                ApplicationName = StringTools.Truncate(AppServerContext.ApplicationName, 30),
                Server = StringTools.Truncate(AppServerContext.AppServerMachineName, 50),
                LogEntryLevel = logEntryLevel,
                ActiveDirectorySId = activeDirectorySId
            };
            WriteToLog4NetLog(logEntryLevel, logEntry, exception, callerStackBoundaryDeclaringType, eventId);
        }

        public static void Log(string userLogin, string userName, string activityType,
                               string activitySubType = null, string text1 = null, string text2 = null,
                               string text3 = null, string details = null, int? elapsedMilliseconds = null,
                               string logEntryLevel = LogEntryLevel_INFO, Exception exception = null,
                               Type callerStackBoundaryDeclaringType = null, string activeDirectorySId = null,
                               int eventId = 0)
        {
            ActivityLogEntry logEntry = new ActivityLogEntry
            {
                UserLogin = StringTools.Truncate(userLogin, 50),
                UserName = StringTools.Truncate(userName, 100),
                ActivityType = StringTools.Truncate(activityType, 100),
                ActivitySubType = StringTools.Truncate(activitySubType, 300),
                Text1 = StringTools.Truncate(text1, 2000),
                Text2 = StringTools.Truncate(text2, 2000),
                Text3 = StringTools.Truncate(text3, 2000),
                Details = details,
                ElapsedMilliseconds = elapsedMilliseconds,
                ApplicationName = StringTools.Truncate(AppServerContext.ApplicationName, 30),
                Server = StringTools.Truncate(AppServerContext.AppServerMachineName, 50),
                LogEntryLevel = logEntryLevel,
                ActiveDirectorySId = activeDirectorySId
            };

            WriteToLog4NetLog(logEntryLevel, logEntry, exception, callerStackBoundaryDeclaringType, eventId);

            // Write it to the DB if we are configured to do so
            if (AppServerContext.LogToEventLogDb)
            {
                try
                {
                    ESPDbDomainService domainService = new ESPDbDomainService(logCRUDActivity: false);
                    domainService.InsertActivityLogEntry(logEntry);
                    domainService.SaveChanges();
                }
                catch (Exception)
                {
                    // Is ther anything else we can do here?  Error is already written to the log
                }
            }
        }

        private static void WriteToLog4NetLog(string logEntryLevel, ActivityLogEntry logEntry, Exception exception, Type callerStackBoundaryDeclaringType, int eventId)
        {
            StringDelimiter sd = new StringDelimiter(" - ");
            AddValueToStringDelimiterIfNotNull(sd, "UserLogin", logEntry.UserLogin);
            AddValueToStringDelimiterIfNotNull(sd, "UserName", logEntry.UserName);
            AddValueToStringDelimiterIfNotNull(sd, "ActiveDirectorySId", logEntry.ActiveDirectorySId);
            AddValueToStringDelimiterIfNotNull(sd, "ActivityType", logEntry.ActivityType);
            AddValueToStringDelimiterIfNotNull(sd, "ActivitySubType", logEntry.ActivitySubType);
            AddValueToStringDelimiterIfNotNull(sd, "Text1", logEntry.Text1);
            AddValueToStringDelimiterIfNotNull(sd, "Text2", logEntry.Text2);
            AddValueToStringDelimiterIfNotNull(sd, "Text3", logEntry.Text3);
            AddValueToStringDelimiterIfNotNull(sd, "Details", logEntry.Details);
            AddValueToStringDelimiterIfNotNull(sd, "ElapsedMilliseconds", logEntry.ElapsedMilliseconds);

            string message = sd.ToString();

            Type callerStackBoundary = callerStackBoundaryDeclaringType;
            if (callerStackBoundary == null)
            {
                callerStackBoundary = ThisDeclaringType;
            }

            if (logEntryLevel == LogEntryLevel_INFO)
            {
                log.Info(callerStackBoundary, message, eventId);
            }
            else if (logEntryLevel == LogEntryLevel_WARN)
            {
                log.Warn(callerStackBoundary, message, eventId);
            }
            else if (logEntryLevel == LogEntryLevel_ERROR)
            {
                log.Error(callerStackBoundary, message, exception, eventId);
            }
            else if (logEntryLevel == LogEntryLevel_DEBUG)
            {
                log.Debug(callerStackBoundary, message, eventId);
            }
            else if (logEntryLevel == LogEntryLevel_FATAL)
            {
                log.Fatal(callerStackBoundary, message, eventId);
            }
            else
            {
                throw new ArgumentOutOfRangeException("Invalid LogEntryLevel: " + logEntryLevel);
            }
        }


        #endregion

        // ********************************************************************************
        #region Support Methods
        // ********************************************************************************

        private static void AddValueToStringDelimiterIfNotNull(StringDelimiter sd, string label, object value)
        {
            if (value != null)
            {
                sd.Add("{0}: {1}", label, value);
            }
        }

        #endregion
    }
}
