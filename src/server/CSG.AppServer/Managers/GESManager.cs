﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using com.baml.gmrt.ges.client.api;
using com.baml.gmrt.ges.client.thrift;
using System.Xml.Serialization;
using System.Xml;

namespace CSG.AppServer.Managers
{
    public class GESManager 
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly Type ThisDeclaringType = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;

        private static XmlSerializer serializer = new
                                     XmlSerializer(typeof(GESManager.GESActionItemList[]), new XmlRootAttribute() { ElementName = "GESActionItemList" });

        private List<String> allGroups = new List<string>();
        private List<Tuple<String, Dictionary<string, List<string>>>> usersActionsAndResources = new List<Tuple<String, Dictionary<string, List<string>>>>();
        private List<Tuple<String, List<string>>> usersResourcesAndGroups = new List<Tuple<string, List<string>>>();
        private const string QUERY_ERROR_MESSAGE = "Unable to query GES";
        private string gesTestDataFilename = null;
        private bool _testMode;

        [XmlType]
        public class GESAllRoles
        {
            [XmlElement]
            public GESSubjectAndRoles[] allSubjectsResourcesAndActions;
        }

        [XmlType]
        public class GESSubjectAndRoles
        {
            [XmlElement]
            public string subject;
            [XmlElement]
            public List<string> subjectRoles;
        }

        [XmlType]
        public class GESAllEntitlements
        {
            [XmlElement]
            public GESActionItemList[] allSubjectsResourcesAndActions;
        }

        [XmlType]
        public class GESActionItemList
        {
            [XmlElement]
            public string subject;
            [XmlElement]
            public GESActionItem[] subjectResourcesAndActions;
            [XmlElement]
            public List<string> subjectRoles;
        }

        [XmlType]
        public class GESActionItem
        {
            [XmlAttribute]
            public string userAction;
            [XmlAttribute]
            public List<string> resourceIds;
        }

        class GESSubjectItem
        {
            [XmlAttribute]
            public string subject = string.Empty;
        }

        IEntitlementsServiceFactory entFactory = null;

        // ********************************************************************************
        #region Constructor, etc. 

        public GESManager()
        {
            if (gesTestDataFilename == null)
            {
                SystemSettingsManager ssm = SystemSettingsManager.Current;
                var xmlFile = ssm.GetBySettingName("TEST_GES_DATA");
                if (xmlFile != null)
                {
                    allGroups.Clear();
                    usersActionsAndResources.Clear();
                    usersResourcesAndGroups.Clear();
                    gesTestDataFilename = xmlFile.StringValue;
                    TESTgesLoadFromXML(gesTestDataFilename);
                    _testMode = true;
                    log.Info("Using test mode for GES data: " + xmlFile.StringValue);
                }
            }

            if (!_testMode)
            { 
                entFactory = new ThriftEntitlementsServiceFactory();
            }
        }
        
        #endregion
        // ********************************************************************************

        // ********************************************************************************
        #region User Methods
        /// <summary>
        /// Obtains all of the subjects in ges for the configured namespace
        /// TODO: Candidate for removal or comment for debug builds only
        /// </summary>
        public void TESTgesLoadFromXML(string filename)
        {                     
            using (FileStream fs = new FileStream(filename, FileMode.Open))
            using (XmlReader reader = XmlReader.Create(fs))
            {
                try
                {

                    // Create an instance of the XmlSerializer specifying type and namespace.

                    var deserializedGESData = (GESManager.GESActionItemList[])serializer.Deserialize(reader);

                    foreach (var gesEntry in deserializedGESData)
                    {

                        Dictionary<string, List<string>> resourcesActions = new Dictionary<string, List<string>>();

                        if (gesEntry.subjectResourcesAndActions != null)
                        {
                            foreach (var ra in gesEntry.subjectResourcesAndActions)
                            {
                                resourcesActions.Add(ra.userAction, ra.resourceIds);
                            }
                            Tuple<string, Dictionary<string, List<string>>> subjectResourcesAction =
                                new Tuple<string, Dictionary<string, List<string>>>(gesEntry.subject, resourcesActions);
                                
                            usersActionsAndResources.Add(subjectResourcesAction);
                        }
                        usersResourcesAndGroups.Add(new Tuple<string, List<string>>(gesEntry.subject, gesEntry.subjectRoles));
                        allGroups = allGroups.Concat(gesEntry.subjectRoles).ToList();
                    }
                    // remove duplicated from the allGroups list
                    allGroups = allGroups.Distinct().ToList();
                }
                finally
                {
                    fs.Close();
                    reader.Close();
                }
			}
        }


        /// <summary>
        /// Obtains all of the subjects in ges for the configured namespace
        /// </summary>
        public List<string> gesGetAllSubjects()
        {
            List<String> results = null;
            var startTime = DateTime.Now;
            if (_testMode)
            {
                // return item1 from the tuple, which is the list of users in our test data
                results = usersActionsAndResources.Select(users => users.Item1).ToList();
            }
            else
            {
                using (var dataServices = entFactory.GetDataService())
                {
                    results = dataServices.GetSubjects();
                }
            }

            var stopTime = DateTime.Now;

            if (results != null)
            {
                log.Debug(String.Format("gesGetAllSubjects completed in {0}ms and returned {1} subjects.",
                                    stopTime.Subtract(startTime).TotalMilliseconds, results.Count()));
            }
            return results;
        }

        /// <summary>
        /// Obtains all of the entitlements in ges for the specified user
        /// </summary>
        public Dictionary<String, List<String>> gesGetPermittedResources(String subject)
        {
            Dictionary<String, List<String>> results = null;
            var startTime = DateTime.Now;
            if (_testMode)
            {
                Tuple<String, Dictionary<string, List<string>>> subjectsResourcesAndActions;
                subjectsResourcesAndActions = usersActionsAndResources.Where(user => user.Item1.ToLower() == subject.ToLower()).FirstOrDefault();
                if (subjectsResourcesAndActions != null)
                {
                    results = subjectsResourcesAndActions.Item2;
                }
            }
            else
            {
                using (var service = entFactory.GetService())
                {
                    results = service.GetPermittedResources(subject);
                }
            }

            var stopTime = DateTime.Now;
            if (results != null)
            {
                log.Debug(String.Format("gesGetPermittedResources completed in {0}ms and returned {1} objects.",
                                    stopTime.Subtract(startTime).TotalMilliseconds, results.Count()));
            }
            return results;
        }

        /// <summary>
        /// Obtains all of the roles in ges that are defined for the configured namespace
        /// </summary>
        public List<string> gesGetAllRoles()
        {
            List<String> results = null;
            var startTime = DateTime.Now;

            if (_testMode)
            {
                // return item1 from the tuple, which is the list of users in our test data
                results = allGroups;
            }
            else
            {
                using (var dataServices = entFactory.GetDataService())
                {
                    results = dataServices.GetRoles();
                }

            }

            var stopTime = DateTime.Now;
            if (results != null)
            {
                log.Debug(String.Format("gesGetAllRoles completed in {0}ms and returned {1} objects.",
                                    stopTime.Subtract(startTime).TotalMilliseconds, results.Count()));
            }
            return results;
        }
        /// <summary>
        /// Obtains all of the roles in ges for a specified user
        /// </summary>
        public List<string> gesGetSubjectRoles(String subject)
        {
            List<String> results = null;
            var startTime = DateTime.Now;

            if (_testMode)
            {
                // return item1 from the tuple, which is the list of users in our test data
                results = usersResourcesAndGroups.Where(users => users.Item1 == subject).Select(users => users.Item2).FirstOrDefault();
            }
            else
            {
                using (var dataServices = entFactory.GetDataService())
                {
                    results = dataServices.GetRolesForSubject(subject);
                }
            }

            var stopTime = DateTime.Now;
            if (results != null)
            {
                log.Debug(String.Format("gesGetSubjectRoles completed in {0}ms and returned {1} objects.",
                                        stopTime.Subtract(startTime).TotalMilliseconds, results.Count()));
            }

            return results;
        }

        #endregion
        // ********************************************************************************

    }
}
