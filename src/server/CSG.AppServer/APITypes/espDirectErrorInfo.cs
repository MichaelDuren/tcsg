﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CSG.AppServer.APITypes
{
    public class csgAPIErrorInfo
    {
        public int errorCode { get; set; }
        public string errorString { get; set; }
    }
}