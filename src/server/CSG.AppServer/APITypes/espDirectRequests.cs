﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CSG.AppServer.APITypes
{
    public class espCodesignRequest
    {
        public int requestId { get; set; }
        public int signingProfileId { get; set; }
        public string requestStatus { get; set; }
        public string signedFileHashSha256 { get; set; }

        public string projectName { get; set; }
        public string releaseName { get; set; }

        public int? scanningCommandResultCode { get; set; }

        public int? signingCommandResultCode { get; set; }

    }

    public class codesignRequestCreateParams
    {
        public int signingProfileId { get; set; }
        public int groupId { get; set; }
        public string fileType { get; set; }
        public string fileHashSha256 { get; set; }
        public string projectName { get; set; }
        public string releaseName { get; set; }
    }

    public class APICodesignRequestStatus
    { 
        public csgAPIErrorInfo returnCode { get; set;}

        public espCodesignRequest request { get; set; }
    }
}