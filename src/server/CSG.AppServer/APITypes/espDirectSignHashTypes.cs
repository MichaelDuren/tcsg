﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CSG.AppServer.APITypes
{
    public class espSignHashRequest
    {
        public string hashAlg { get; set; }
        public string hashValue { get; set; }
        public int signingTypeId { get; set; }
        public String signingTypeGuid { get; set; }
        public String projectName { get; set; }
        public string releaseName { get; set; }
    }

    public class espSignHashResponse
    {
        public string signatureValue { get; set; }
        public int requestId { get; set; }
    }

    public class APISignHashResult
    { 
        public csgAPIErrorInfo returnCode { get; set;}

        public espSignHashResponse response { get; set; }
    }
}