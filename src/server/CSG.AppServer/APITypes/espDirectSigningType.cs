﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CSG.AppServer.APITypes
{
    public class espDirectSigningType
    {
        public int signingProfileId { get; set; }
        public string signingProfileName { get; set; }
        public string certificate { get; set; }
        public int groupId { get; set; }
        public string groupName { get; set; }
    }

    public class APIListSigningTypes
    { 
        public csgAPIErrorInfo returnCode { get; set;}
        public espDirectSigningType[] signingProfiles;
    }
}