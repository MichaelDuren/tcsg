using System;
using System.IO;
using CSG.AppServer.Entities;
using CSG.AppServer.Managers;
using Util;
using Util.Config;

namespace CSG.AppServer
{
    public static class AppServerContext
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // *********************************************************************************
        #region Properties
        // *********************************************************************************

        public static string BaseDatabaseConnectionStringName { get; private set; }
        public static string ApplicationName { get; private set; }
        public static string AppEnvironment { get; private set; }
        public static string AppServerMachineName { get; private set; }
        public static string ApplicationBaseDirectory { get; private set; }
        public static string DefaultDomain { get; private set; }
        public static string GESPortalResourceName { get; private set; }
        public static string GESActionManagerApprover { get; private set; }
        public static string GESActionApprover { get; private set; }
        public static string GESActionEnterpriseApprover { get; private set; }
        public static string GESActionSignRequest { get; private set; }
        public static string GESActionSignHashRequest { get; private set; }
        public static string GESActionOfflineSigner { get; private set; }
        public static string GESActionAdministrator { get; private set; }
        public static string ADDomainOrServer { get; private set; }        
        public static string ADContainer { get; private set; }
        public static string PrimaryStorageLocation { get; private set; }
        public static string SecondaryStorageLocation { get; private set; }
        public static string LocalUploadFolder { get; private set; }        
        public static string SSOHeaderUID { get; private set; }
        public static string SSOHeaderEMail { get; private set; }
        public static string SSOHeaderFirstName { get; private set; }
        public static string SSOHeaderLastName { get; private set; }
        public static string OAuthUIDFieldName { get; private set; }
        public static string OAuthClientId { get; private set; }
        
        public static string OAuthVerifyClientCertificate { get; private set; }
        public static bool OAuthClientUseSystemStore { get; private set; }
        public static bool OAuthAuthenticateToServer { get; private set; }
        public static bool OAuthAuthenticateIgnoreNameMismatch { get; private set; }
        public static bool OAuthAuthenticateIgnoreCertChainErrors { get; private set; }
        public static bool DisablePortalUI { get; private set; }
        public static bool DisableWebAPI { get; private set; }
        public static int WaitForUnlockTimer { get; private set; }
        public static bool LogToEventLogDb { get; private set; }

        public static string ProjectList
        {
            get
            {
                SystemSetting systemSetting = SystemSettingsManager.Current.GetBySettingName(Constants.SettingNames.ProjectList);
                return systemSetting.StringValue;
            }
        }
        public static string ReleaseList
        {
            get
            {
                SystemSetting systemSetting = SystemSettingsManager.Current.GetBySettingName(Constants.SettingNames.ReleaseList);
                return systemSetting.StringValue;
            }
        }

        public static bool UseTestUserManager
        {
            get
            {
                string _useTestUserManager_string = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.UseTestUserManager];
                return StringTools.GetBoolean(_useTestUserManager_string);
            }
        }

        public static bool IsProductionEnvironment
        {
            get
            {
                if (AppEnvironment != null &&
                    (AppEnvironment.ToUpper() == "PRD" || AppEnvironment.ToUpper().StartsWith("PROD")))
                {
                    return true;
                }
                else { return false; }
            }
        }

        public static bool UsePrimaryStorageFirst
        {
            get
            {
                string _boolString = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.UsePrimaryStorage];
                return StringTools.GetBoolean(_boolString);
            }
        }
        // Values from System Settings

        public static ApplicationOnlineStatus ApplicationOnlineStatus
        {
            get
            {
                SystemSetting systemSetting = SystemSettingsManager.Current.GetBySettingName(Constants.SettingNames.ApplicationOnlineStatus);
                return EnumerationTools.Parse<ApplicationOnlineStatus>(systemSetting.StringValue);
            }
            set
            {
                SystemSetting systemSetting = SystemSettingsManager.Current.GetBySettingName(Constants.SettingNames.ApplicationOnlineStatus);
                systemSetting.StringValue = value.ToString();
                SystemSettingsManager.Current.UpdateSystemSetting(systemSetting);
            }
        }

        private static int? _cacheMinutes;
        public static int CacheMinutes
        {
            get
            {
                if (_cacheMinutes == null)
                {
                    SystemSetting systemSetting = SystemSettingsManager.Current.GetBySettingName(Constants.SettingNames.CacheMinutes);

                    // Must create a default for this setting because it is used by the System Settings Manager
                    if (systemSetting == null)
                    {
                        // Set default to 2 minutes if not in the DB
                        _cacheMinutes = 2;
                    }
                    else
                    {
                        _cacheMinutes = (int)systemSetting.IntegerValue;
                    }
                }

                return (int)_cacheMinutes;
            }
        }

        private static int? _userTimeout;
        public static int UserTimeout
        {
            get
            {
                if (_userTimeout == null)
                {
                    SystemSetting systemSetting = SystemSettingsManager.Current.GetBySettingName(Constants.SettingNames.UserTimeout);
                    _userTimeout = (int)systemSetting.IntegerValue;
                }
                return (int)_userTimeout;
            }
        }

        public static string IPAddressRestrictions
        {
            get
            {
                SystemSetting IPAddressRestrictions = SystemSettingsManager.Current.GetBySettingName(Constants.SettingNames.IPAddressRestrictions);
                return IPAddressRestrictions.StringValue;
            }
        }

        public static string TempStorageLocation
        {
            get
            {
                SystemSetting systemSetting = SystemSettingsManager.Current.GetBySettingName(Constants.SettingNames.TempStorageLocation);
                return systemSetting.StringValue;
            }
        }

        public static int RequestTimeout
        {
            get
            {
                SystemSetting systemSetting = SystemSettingsManager.Current.GetBySettingName(Constants.SettingNames.RequestTimeoutInHours);
                return (int)systemSetting.IntegerValue;
            }
        }

        public static string TeamEmailAddress
        {
            get
            {
                SystemSetting systemSetting = SystemSettingsManager.Current.GetBySettingName(Constants.SettingNames.TeamEmailAddress);
                return systemSetting.StringValue;
            }
        }


        #endregion

        // *********************************************************************************
        #region Initialize Methods
        // *********************************************************************************

        public static void Initialize(string baseDatabaseConnectionStringName)
        {
            BaseDatabaseConnectionStringName = baseDatabaseConnectionStringName;

            Initialize();
        }

        public static void Initialize()
        {
            log.Info("Running CommonAppServerContext.Initialize()");

            LoadConfigValues();

            Guard.ValueNotBlankOrNull(ApplicationName, "ApplicationName is null. ApplicationName must be set in the App/Web config.");
            log.Info("   ApplicationName: " + ApplicationName);

            Guard.ValueNotBlankOrNull(AppEnvironment, "AppEnvironment is null. Is the ServerEnvironment for the current machine specified in the dynamicConfig section of the app/web config file?");
            log.Info("   AppEnvironment: " + AppEnvironment);

            AppServerMachineName = Environment.MachineName;
            log.Info("   AppServerMachineName: " + AppServerMachineName);

            Guard.ValueNotBlankOrNull(BaseDatabaseConnectionStringName, "BaseDatabaseConnectionStringName must be set by calling Initialize() or set in the App/Web config.");
            log.Info("   BaseDatabaseConnectionStringName: " + BaseDatabaseConnectionStringName);

            log.Info("   Actual DatabaseConnectionStringName: " + DynamicConfigurationManager.GetConnectionStringName());

            Guard.ValueNotBlankOrNull(GESPortalResourceName, "GESPortalResourceName must be set by calling Initialize() or set in the App/Web config.");
            log.Info("   GESPortalResourceName: " + GESPortalResourceName);

            ApplicationBaseDirectory = System.AppDomain.CurrentDomain.BaseDirectory;
        }

        private static void LoadConfigValues()
        {
            try
            {
                if (BaseDatabaseConnectionStringName == null)
                {
                    BaseDatabaseConnectionStringName = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.BaseDatabaseConnectionStringName];
                }

                ApplicationName = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.ApplicationName];
                AppEnvironment = DynamicConfigurationManager.Environment;

                GESPortalResourceName = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.GESPortalResourceName];
                GESActionManagerApprover = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.GESActionStringManagerApprover];
                GESActionApprover = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.GESActionStringApprover];
                GESActionEnterpriseApprover = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.GESActionStringEnterpriseApprover];
                GESActionSignRequest = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.GESActionStringSignRequest];
                GESActionSignHashRequest = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.GESActionStringSignHashRequest];
                GESActionOfflineSigner = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.GESActionStringOfflineSigner];
                GESActionAdministrator = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.GESActionStringAdministrator];

                DefaultDomain = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.DefaultDomain];

                SSOHeaderUID = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.SSOHeaderUID];
                SSOHeaderEMail = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.SSOHeaderEMail];
                SSOHeaderFirstName = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.SSOHeaderFirstName];
                SSOHeaderLastName = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.SSOHeaderLastName];

                OAuthUIDFieldName = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.OAuthUIDFieldName];
                OAuthClientId = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.OAuthClientId];
                
                OAuthVerifyClientCertificate = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.OAuthVerifyClientCertificate];
                OAuthClientUseSystemStore = true;
                if (Convert.ToBoolean(DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.OAuthClientUseSystemStore] != null))
                {
                    OAuthClientUseSystemStore = Convert.ToBoolean(DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.OAuthClientUseSystemStore]);
                }

                OAuthAuthenticateToServer = true;
                if (Convert.ToBoolean(DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.OAuthAuthenticateToServer] != null))
                {
                    OAuthAuthenticateToServer = Convert.ToBoolean(DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.OAuthAuthenticateToServer]);
                }
                OAuthAuthenticateIgnoreNameMismatch = true;
                if (Convert.ToBoolean(DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.OAuthAuthenticateIgnoreNameMismatch] != null))
                {
                    OAuthAuthenticateIgnoreNameMismatch = Convert.ToBoolean(DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.OAuthAuthenticateIgnoreNameMismatch]);
                }
                OAuthAuthenticateIgnoreCertChainErrors = true;
                if (Convert.ToBoolean(DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.OAuthAuthenticateIgnoreCertChainErrors] != null))
                {
                    OAuthAuthenticateIgnoreCertChainErrors = Convert.ToBoolean(DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.OAuthAuthenticateIgnoreCertChainErrors]);
                }

                ADDomainOrServer = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.ADDomainOrServer];
                ADContainer = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.ADContainer];

                WaitForUnlockTimer = 0;
                if (DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.WaitForUnlockTimer] != null)
                {
                    WaitForUnlockTimer = Convert.ToInt32(DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.WaitForUnlockTimer]);
                }

                LogToEventLogDb = false;
                if (Convert.ToBoolean(DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.LogToEventLogDb] != null))
                {
                    LogToEventLogDb = Convert.ToBoolean(DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.LogToEventLogDb]);
                }
 
                PrimaryStorageLocation = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.PrimaryStorageLocation];
                if (PrimaryStorageLocation == null)
                {
                    var msg = "The configuration value for PrimaryStorageLocation is empty";
                    log.Error(msg);
                    throw new Exception(msg);
                }
                SecondaryStorageLocation = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.SecondaryStorageLocation];
                if (SecondaryStorageLocation == null)
                {
                    var msg = "The configuration value for SecondaryStorageLocation is empty";
                    log.Error(msg);
                    throw new Exception(msg);
                }

                LocalUploadFolder = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.LocalUploadFolder];
                if (LocalUploadFolder == null)
                {
                    var msg = "The configuration value for LocalUploadFolder is empty";
                    log.Error(msg);
                    throw new Exception(msg);
                }

                DisablePortalUI = DisableWebAPI = false;
                if (Convert.ToBoolean(DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.DisableESPDirectAPI] != null))
                {
                    DisableWebAPI = Convert.ToBoolean(DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.DisableESPDirectAPI]);
                }
                if (Convert.ToBoolean(DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.DisablePortalUI] != null))
                {
                    DisablePortalUI = Convert.ToBoolean(DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.DisablePortalUI]);
                }

            }
            catch (Exception e)
            {
                log.Error("An exception has occurred loading values from the web.config / app.config file.", e);
                throw;
            }
        }

        #endregion
    }
}
