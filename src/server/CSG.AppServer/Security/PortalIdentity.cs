﻿
using CSG.AppServer.Entities;
using System.Security.Principal;

namespace CSG.AppServer.Security
{
    public class PortalIdentity : GenericIdentity
    {
        private UserProfile _userProfile = null;
        public UserProfile userProfile
        {
            get { return _userProfile; }
            set { _userProfile = value; }
        }

        public PortalIdentity(string name, UserProfile userProfile) : base(name)
        {
            _userProfile = userProfile;
        }
    }
}
