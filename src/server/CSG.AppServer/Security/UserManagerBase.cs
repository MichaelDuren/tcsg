﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using CSG.AppServer.Db;
using CSG.AppServer.Entities;
using CSG.AppServer.Exceptions;
using CSG.AppServer.Managers;
using Util;
using Util.Config;
using System.Data.Entity.Infrastructure;

namespace CSG.AppServer.Security
{
    public abstract class UserManagerBase : IDisposable
    {
        // *********************************************************************************
        #region Properties
        // *********************************************************************************
        bool disposed = false;
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly Type ThisDeclaringType = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;

        protected ESPDbDomainService domainService = new ESPDbDomainService();
        private static List<string> _domainValuesFromConfig;
        protected static UserCache UserCache { get; private set; }

        protected DomainAndLoginId _domainAndLoginId = null;

        protected UserProfile _userProfile=null;

        private readonly object umLock = new object();
        #endregion

        // *********************************************************************************
        #region Constructors
        // *********************************************************************************

        public UserManagerBase(ESPDbDomainService sharedDomainService = null)
        {
            if (sharedDomainService == null)
            {
                domainService = new ESPDbDomainService();
            }
            else
            {
                domainService = sharedDomainService;
            }
            _domainValuesFromConfig = GetDomainValuesFromConfig();
            UserCache = new UserCache();
        }
        public void Dispose()
        {
            Dispose(true);
        }

        public void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {
                   // domainService.Dispose();
                }
                disposed = true;
            }
        }

        #endregion

        // *********************************************************************************
        #region Core Methods
        // *********************************************************************************

      
        /// <summary>
        /// Does Login for a user.
        /// For normal conditions - it returns true or false - based on if the user is authenticated.
        /// For anticipated unusual conditions - it throws a LoginException - indicating what happened
        /// </summary>
        public bool Login(string domain, string loginId, string password, bool saveToSession = true)
        {
            if (StringTools.IsBlankOrNull(domain) || StringTools.IsBlankOrNull(loginId) || StringTools.IsBlankOrNull(password))
            {
                return false;
            }

            // make sure the domain is in the list of allowed domains
            if (!_domainValuesFromConfig.Contains(domain.ToLower()))
            {
                return false;
            }

            LoginProfile authenticatedWithSid;
            authenticatedWithSid = IsAuthenticated(domain, loginId, password);
            DomainAndLoginId domainAndLoginId = new DomainAndLoginId(domain, loginId, authenticatedWithSid.SId);

            try
            {

                // Make sure that the user isn't in the cache
                UserCache.Remove(domainAndLoginId);
                if (authenticatedWithSid.Authenticated)
                {

                    // NOTE: It is possible that this method will throw a LoginException
                    UserProfile userProfile = GetUserProfileForLogin(domainAndLoginId, authenticatedWithSid.SId);
                    Guard.ArgumentNotNull(userProfile, "userProfile");

                    string message = string.Format("Login() completed successfully for LoginId: {0} Name: {1}", userProfile.DomainAndLoginId, userProfile.FullName);
                    if (userProfile.Groups != null)
                    {
                        List<string> groupNames = userProfile.Groups.Select(r => r.Group.Name).ToList();
                        message += string.Format(" with Groups: {0}", StringDelimiter.Concatenate(", ", groupNames));
                    }

                    ActivityLog.Log(userProfile, 0, "UserManagerBase", "Login", "Login-Success", message);
                    log.Debug(message);

                    // Add the user profile to the cache.  Do a Remove first - just in case it was added between the time that we checked above and here.h
                    if (saveToSession)
                    {
                        FormsAuthentication.SetAuthCookie(domainAndLoginId.ToString(), true);
                        UserCache.Remove(userProfile);
                        UserCache.Add(userProfile);
                        // Save the Domain and LoginId to Session
                        SaveDomainAndLoginIdToSession(domainAndLoginId);
                    }
                    else
                    {
                        // in this case we keep the user information local, API calls take this route rather than
                        // keep user info in an http session.
                        _domainAndLoginId = domainAndLoginId;
                        // cache the user profile locally
                        _userProfile = userProfile;
                    }
                }
                else
                {
                    ActivityLog.Log(domainAndLoginId, 0, domainAndLoginId.ToString(), "UserManagerBase", "Login", "Login-Failed", "User Failed Authentication");
                }

                return authenticatedWithSid.Authenticated;
            }
            catch (LoginException e)
            {
                ActivityLog.LogException(domainAndLoginId, domainAndLoginId.ToString(), 0, "UserManagerBase", "Login", "Login-Failed", e.Message, e);
                throw;
            }
            catch (Exception e)
            {
                ActivityLog.LogException(domainAndLoginId, domainAndLoginId.ToString(), 0, "UserManagerBase", "Login", "Login-Failed - Unexpected Exception", e.Message, e);
                throw;
            }
        }

        /// <summary>
        /// Authenticates user for API method access.
        /// </summary>
        public bool AuthenticateUser(string domain, string loginId, string password)
        {
            if (StringTools.IsBlankOrNull(domain) || StringTools.IsBlankOrNull(loginId) || StringTools.IsBlankOrNull(password))
            {
                return false;
            }

            // make sure the domain is in the list of allowed domains
            if (!_domainValuesFromConfig.Contains(domain.ToLower()))
            {
                return false;
            }

            LoginProfile authenticatedWithSid;
            authenticatedWithSid = IsAuthenticated(domain, loginId, password);
            DomainAndLoginId domainAndLoginId = new DomainAndLoginId(domain, loginId, authenticatedWithSid.SId);

            try
            {
                if (authenticatedWithSid.Authenticated)
                {
                    // NOTE: It is possible that this method will throw a LoginException
                    UserProfile userProfile = GetUserProfileForLogin(domainAndLoginId, authenticatedWithSid.SId);
                    Guard.ArgumentNotNull(userProfile, "userProfile");

                    string message = string.Format("Login() completed successfully for LoginId: {0} Name: {1}", userProfile.DomainAndLoginId, userProfile.FullName);
                    if (userProfile.Groups != null)
                    {
                        List<string> groupNames = userProfile.Groups.Select(r => r.Group.Name).ToList();
                        message += string.Format(" with Groups: {0}", StringDelimiter.Concatenate(", ", groupNames));
                    }

                    ActivityLog.Log(userProfile, 0, "UserManagerBase", "Login", "Login-Success", message);
                    log.Debug(message);

                    // in this case we keep the user information local, API calls take this route rather than
                    // keep user info in an http session.
                    _domainAndLoginId = domainAndLoginId;
                    // cache the user profile locally
                    _userProfile = userProfile;
                }
                else
                {
                    ActivityLog.Log(domainAndLoginId, 0, domainAndLoginId.ToString(), "UserManagerBase", "Login", "Login-Failed", "User Failed Authentication");
                }

                return authenticatedWithSid.Authenticated;
            }
            catch (LoginException e)
            {
                ActivityLog.LogException(domainAndLoginId, domainAndLoginId.ToString(), 0, "UserManagerBase", "Login", "Login-Failed", e.Message, e);
                throw;
            }
            catch (Exception e)
            {
                ActivityLog.LogException(domainAndLoginId, domainAndLoginId.ToString(), 0, "UserManagerBase", "Login", "Login-Failed - Unexpected Exception", e.Message, e);
                throw;
            }
        }

        /// <summary>
        /// Does Login for a user.  The user identity is already autheticated via SSO solution
        /// when this point is reached.
        /// </summary>
        public bool Login(UserProfile userProfileData)
        {
            if (StringTools.IsBlankOrNull(userProfileData.LoginId))
            {
                return false;
            }

            try
            {
                UserProfile userProfile;
                UserManagerBase userManager = UserManagerFactory.CreateUserManager();

                userProfile = userManager.GetUserProfile(userProfileData.LoginId);
                if (userProfile != null && !userProfile.IsDeleted)
                {
                    // user profile information comes through the login process, lets update it if needed
                    if (userProfile.Email != userProfileData.Email ||
                        userProfile.FirstName != userProfileData.FirstName ||
                        userProfile.LastName != userProfileData.LastName)
                    {
                        userProfile.Email = userProfileData.Email;
                        userProfile.FirstName = userProfileData.FirstName;
                        userProfile.LastName = userProfileData.LastName;
                        userManager.UpdateUserProfile(userProfile);
                    }

                    // Add the user profile to the cache.  
                    HttpContext.Current.Session.Add(Constants.UserProfile, userProfile);
                 //   FormsAuthentication.SetAuthCookie(HttpContext.Current.User.Identity.Name, true);
                    ActivityLog.Log(userProfile, ActivityLog.LogEventId_LOGIN, "UserManagerBase", "Login", "Login-Success");

                }

                return true;
            }
            catch (LoginException e)
            {
                ActivityLog.Log(userProfileData, ActivityLog.LogEventId_LOGIN, "UserManagerBase", "Login", "Login-Failure", e.Message);
                throw;
            }
            catch (Exception e)
            {
                ActivityLog.Log(userProfileData, ActivityLog.LogEventId_LOGIN, "UserManagerBase", "Login", "Login-Failure", e.Message);
                throw;
            }
        }

        /// <summary>
        /// Returns the user profile after a login.  This function updates the last access date in the user's profile.
        /// </summary>

        private UserProfile GetUserProfileForLogin(DomainAndLoginId domainAndLoginId, string sid)
        {
            // Determine if there is a User for this userName
            UserProfile userProfile = GetFullUserProfileByDomainAndLoginId(domainAndLoginId, sid);

            // If no user exists, attempt to create one
            if (userProfile == null)
            {
                try
                {
                    // Create new user object and populate based on Active Directory data.
                    //  NOTE: It throws and exception if they aren't in one of our groups
                    userProfile = CreateUserProfile(domainAndLoginId);
                    Guard.ArgumentNotNull(userProfile, "userProfile");
                }
                // Note: this try/catch is not needed - it is just added for code clarity - showing that a LoginException is expected from AttemptCreateUserProfile
                catch (LoginException)
                {
                    throw;
                }
            }
            else
            {
                // Need to confirm if the user DOES actually have rights to the system (ie. if they have any roles)
                if (userProfile.Groups.Count() == 0)
                {
                    throw new LoginException("User does not have any Rights (roles) in the system", LoginExceptionSubType.UserHasNoRoles);
                }

                if (userProfile.IsDeleted)
                {
                    // This code is based on the assumption that we Re-Activate the user if they were deleted - but currently DO have rights
                    ActivityLog.Log(userProfile, 0, "UserManagerBase", "Login-Reactivate User", string.Format("Reactivating user - Login: {0} Name: {1}", userProfile.DomainAndLoginId, userProfile.FullName));

                    userProfile.IsDeleted = false;
                }

                userProfile.LoginCount++;
                userProfile.LastAccessedDate = DateTime.Now;
                UpdateUserProfile(userProfile);
            }

            return userProfile;
        }

        public void LogOut()
        {
            FormsAuthentication.SignOut();

            UserProfile userProfile = GetCurrentUserProfile();

            // Remove user from Session
            RemoveDomainAndLoginIdFromSession();

            // Remove user from Cache
            try
            {
                if (userProfile != null)
                {
                    log.Info("Removing (logout) User: " + userProfile.DomainAndLoginId.ToString() + " from User Cache");
                 //   UserCache.Remove(userProfile);

                    ActivityLog.Log(userProfile, ActivityLog.LogEventId_LOGIN, "UserManagerBase", "Logout", "Logout-Success");
                }
            }
            catch
            {
                // Ignore
            }
        }

        public UserProfile GetUserProfile(String loginId)
        {
            UserProfile userProfile = null;

            // retrieve profile from the database
            userProfile = GetFullUserProfileByUserId(loginId);

            return userProfile;
        }

        public UserProfile GetCurrentUserProfile()
        {
            DomainAndLoginId domainAndLoginId = GetDomainAndLoginIdFromSession();

            // return the local copy if we have one
            if (_userProfile != null)
            {
                return _userProfile;
            }

            // return the entry in the user cache
            if (domainAndLoginId != null)
            {
                return UserCache.Get(domainAndLoginId);
            }

            return null;
        }

        public void UpdateCurrentUserProfile(UserProfile userProfile)
        {

            if (HttpContext.Current.Session != null)
            {
                HttpContext.Current.Session[Constants.UserProfile] = userProfile;
            }
        }

        #endregion

        // ********************************************************************************
        #region Abstract Methods
        // ********************************************************************************

        protected abstract LoginProfile IsAuthenticated(string domain, string loginId, string password);

        protected abstract UserProfile CreateUserProfile(DomainAndLoginId domainAndLoginId, bool updateGroupsAndRoles = true);

        protected abstract UserProfile CreateUserProfile(string loginId, bool saveChanges = true);

        public abstract void SyncAllWithActiveDirectory();

        public abstract void SyncAdminGroupsFromAppConfig();
        #endregion

        // ********************************************************************************
        #region Session related methods - for storing, etc. Domain / LoginId
        // ********************************************************************************

        protected void SaveDomainAndLoginIdToSession(DomainAndLoginId domainAndLoginId)
        {
            // Note: confirmd that Add does not fail if it already exists in the session
            HttpContext.Current.Session.Add(Constants.DomainAndLoginId, domainAndLoginId);
        }

        protected DomainAndLoginId GetDomainAndLoginIdFromSession()
        {
            if (HttpContext.Current.Session == null)
            {
                return null;
            }

            // NOTE: confirmed that if it is not in the session - it returns null
            DomainAndLoginId domainAndLoginId = HttpContext.Current.Session[Constants.DomainAndLoginId] as DomainAndLoginId;
            return domainAndLoginId;
        }

        protected void RemoveDomainAndLoginIdFromSession()
        {
            // NOTE: confirmed that if it is not in the session - it does NOT fail
            HttpContext.Current.Session.Remove(Constants.DomainAndLoginId);
        }

        #endregion

        // *********************************************************************************
        #region User DAL Methods
        // *********************************************************************************

        protected UserProfile GetFullUserProfileByDomainAndLoginId(DomainAndLoginId domainAndLoginId, String sid)
        {
            return GetUserProfiles("Roles.Role, Groups.Group, CodeSigningTypes.CodeSigningType").FirstOrDefault(u => u.Domain == domainAndLoginId.Domain && u.LoginId == domainAndLoginId.LoginId);
        }

        protected UserProfile GetFullUserProfileByUserId(String userId)
        {
            return GetUserProfiles("Roles.Role, Groups.Group, CodeSigningTypes.CodeSigningType").FirstOrDefault(u => u.LoginId == userId);
        }

        protected IQueryable<UserProfile> GetUserProfiles(string includes = null)
        {
            return domainService.GetUserProfiles(includes);
        }

        protected void InsertUserProfile(UserProfile userProfile)
        {
            lock (umLock)
            {
                log.InfoFormat("InsertUserProfile called for User - Login/Name: {0}/{1}", userProfile.LoginId, userProfile.FullName);
                domainService.InsertUserProfile(userProfile);
                bool saveFailed;
                do
                {
                    saveFailed = false;

                    try
                    {
                        domainService.DbContext.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException ex)
                    {
                        saveFailed = true;

                        // Update the values of the entity that failed to save from the store
                        ex.Entries.Single().Reload();
                    }

                } while (saveFailed);
            }
        }

        protected void UpdateUserProfile(UserProfile userProfile)
        {
            lock (umLock)
            {
                log.InfoFormat("UpdateUserProfile called for User Id: {0} - Login/Name: {1}/{2}", userProfile.Id, userProfile.LoginId, userProfile.FullName);
                domainService.UpdateUserProfile(userProfile);

                bool saveFailed;
                do
                {
                    saveFailed = false;

                    try
                    {
                        domainService.DbContext.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException ex)
                    {
                        saveFailed = true;

                        // Update the values of the entity that failed to save from the store
                        ex.Entries.Single().Reload();
                    }

                } while (saveFailed);

            }
        }

        protected void DeleteUserProfile(UserProfile userProfile)
        {
            lock (umLock)
            {
                log.InfoFormat("DeleteUserProfile called for User Id: {0} - Login/Name: {1}/{2}", userProfile.Id, userProfile.LoginId, userProfile.FullName);
                domainService.DeleteUserProfile(userProfile);
                bool saveFailed;
                do
                {
                    saveFailed = false;

                    try
                    {
                        domainService.DbContext.SaveChanges();
                    }
                    catch (DbUpdateConcurrencyException ex)
                    {
                        saveFailed = true;

                        // Update the values of the entity that failed to save from the store
                        ex.Entries.Single().Reload();
                    }

                } while (saveFailed);
            }
        }

        #endregion

        // *********************************************************************************
        #region Protected Methods
        // *********************************************************************************
        private static List<string> GetDomainValuesFromConfig()
        {
            List<string> domainsFromConfig = new List<string>();
            string temp = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.AllowedDomains];

            if (!String.IsNullOrWhiteSpace(temp))
            {
                string[] domains = temp.Split(',');

                foreach (string domain in domains)
                {
                    domainsFromConfig.Add(domain.Trim().ToLower());
                }
            }
            return domainsFromConfig;
        }
        #endregion

    }
}