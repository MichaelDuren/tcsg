using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using CCSS.AppServer.Entities;
using CCSS.AppServer.Exceptions;
using CCSS.AppServer.Managers;
using Util;
using Util.Config;

namespace CCSS.AppServer.Security
{
    public class CCSSUserManager : UserManagerBase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // ********************************************************************************
        #region Properties and Variables
        // ********************************************************************************

        private List<Group> _allGroups;
        public List<Group> AllGroups
        {
            get
            {
                if (_allGroups == null)
                {
                    _allGroups = domainService.GetGroups().Where(g => g.IsDeleted == false).ToList();
                }
                return _allGroups;
            }
        }

        private List<Role> _allRoles;
        public List<Role> AllRoles
        {
            get
            {
                if (_allRoles == null)
                {
                    _allRoles = domainService.GetRoles().ToList();
                }
                return _allRoles;
            }
        }

        #endregion

        // Internal Constructor
        internal CCSSUserManager() { }

        // *********************************************************************************
        #region Core Method Overrides
        // *********************************************************************************

        protected override bool IsAuthenticated(string domain, string loginId, string password)
        {
            Guard.ValueNotBlankOrNull(domain, "The Domain must be set before attempting authentication");
            using (ActiveDirectoryManager activeDirectoryManager = new ActiveDirectoryManager())
            {
                return activeDirectoryManager.Login(domain, loginId, password);
            }
        }

        /// <summary>
        /// Create a new User Profile for the login id provided and populate the properties with data from Active Directory
        /// </summary>
        protected override UserProfile CreateUserProfile(DomainAndLoginId domainAndLoginId, bool updateGroupsAndRoles = true)
        {
            using (ActiveDirectoryManager activeDirectoryManager = new ActiveDirectoryManager())
            {
                UserPrincipal userPrincipal = activeDirectoryManager.GetUserPrincipal(domainAndLoginId.Domain, domainAndLoginId.LoginId);

                // Note: this should not happen - because the user should be authenticated before we get here.
                if (userPrincipal == null)
                {
                    throw new LoginException("User does not exist in Active Directory", LoginExceptionSubType.UserNotInActiveDirectory);
                }

                UserProfile userProfile = CreateUserProfile(domainAndLoginId, userPrincipal, updateGroupsAndRoles);

                domainService.InsertUserProfile(userProfile);
                domainService.SaveChanges();

                // Just to make sure that we've got exactly what is in the DB - get it fresh from the DB after creating it.
                userProfile = GetFullUserProfileByDomainAndLoginId(domainAndLoginId);

                return userProfile;
            }
        }

        private UserProfile CreateUserProfile(DomainAndLoginId domainAndLoginId, UserPrincipal userPrincipal, bool updateGroupsAndRoles = true)
        {
            Guard.ArgumentNotNull(userPrincipal, "userPrincipal");

            UserProfile userProfile = new UserProfile
            {
                Domain = domainAndLoginId.Domain,
                LoginId = domainAndLoginId.LoginId,
                LoginCount = 1,
            };

            domainService.InsertUserProfile(userProfile);

            UpdateUserProfileFromUserPrincipal(userProfile, userPrincipal);

            if (updateGroupsAndRoles)
            {
                UpdateUserProfileGroups(userProfile, userPrincipal);
                SyncUserRolesFromGroups(userProfile);

                // Need to confirm if the user DOES actually have rights to the system (ie. if they have any roles)
                if (userProfile.Roles.Count() == 0)
                {
                    throw new LoginException("User does not have any Rights (roles) in the system", LoginExceptionSubType.UserHasNoRoles);
                }
            }

            domainService.SaveChanges();

            ActivityLog.Log(domainAndLoginId, userProfile.FullName, "CCSSUserManager", "Created User Profile", string.Format("Creating user profile for user - Login/Name: {0}/{1}", userProfile.LoginId, userProfile.DisplayName));

            return userProfile;
        }

        #endregion

        // ********************************************************************************
        #region Update Groups and Roles Methods
        // ********************************************************************************

        /// <summary>
        /// Update the list of groups the user belongs to from Active Directory
        /// 
        /// NOTE: It does NOT update the database - it JUST updates the User Profile.  
        ///         Need to call InsertUserProfile or UpdateUserProfile after this method is called to persist the changes
        /// </summary>
        private void UpdateUserProfileGroups(UserProfile userProfile, UserPrincipal userPrincipal)
        {
            Guard.ArgumentNotNull(userProfile, "userProfile");
            Guard.ArgumentNotNull(userPrincipal, "userPrincipal");

            List<GroupPrincipal> groupPrincipals = ActiveDirectoryManager.GetGroupPrincipalsForUserPrincipal(userPrincipal);
            List<string> adGroupSids = groupPrincipals.Select(gp => gp.Sid.Value).ToList();

            // Remove any groups for the user that they no longer have and any that we no longer care about
            foreach (UserProfileGroup upg in userProfile.Groups.ToList())
            {
                // See if they still have rights to this group
                if (!adGroupSids.Contains(upg.Group.ActiveDirectorySId))
                {
                    //MT - AD Sync Fix
                    domainService.DeleteUserProfileGroup(upg);
                }
                // If it is not one of the groups that we care about - then remove it 
                else if (AllGroups.Count(g => g.Name == upg.Group.Name) == 0)
                {
                    //MT - AD Sync Fix
                    domainService.DeleteUserProfileGroup(upg);
                }
            }

            // Add any new groups for the user.
            foreach (string adGroupSid in adGroupSids)
            {
                // Ignore any group that is not one of the groups that we care about
                Group group = AllGroups.Where(g => g.ActiveDirectorySId == adGroupSid).FirstOrDefault();

                if (group == null)
                {
                    continue;
                }

                // The the user doesn't already have this group - then add it
                if (userProfile.Groups.Count(upg => upg.Group.ActiveDirectorySId == adGroupSid) == 0)
                {
                    userProfile.Groups.Add(new UserProfileGroup { UserProfile = userProfile, Group = group });
                }
            }

            domainService.SaveChanges();
        }

        /// <summary>
        /// Sync the users roles based on the groups that they are in.  Also, updates IsDeleted - based on if they have any roles
        /// </summary>
        private void SyncUserRolesFromGroups(UserProfile userProfile)
        {
            // ************************
            // App Admin
            // ************************

            // Check if the user is in a Group that is an App Admin Group
            if (userProfile.Groups.Count(upg => upg.Group.IsAppAdminGroup) > 0)
            {
                // User SHOULD be an app admin - make sure that they have the role
                if (userProfile.Roles.Count(r => r.Role.Name == Constants.Roles.AppAdminRole) == 0)
                {
                    Role role = AllRoles.Where(r => r.Name == Constants.Roles.AppAdminRole).FirstOrDefault();
                    if (role != null)
                    {
                        userProfile.Roles.Add(new UserProfileRole { UserProfile = userProfile, Role = role });
                    }
                }
            }
            else
            {
                //MT - AD Sync Fix
                // User SHOULD NOT be an app admin - make sure that they do not have the role
                UserProfileRole userProfileRole = userProfile.Roles.Where(upr => upr.Role.Name == Constants.Roles.AppAdminRole
                                                                              && upr.UserProfileId == userProfile.Id)
                                                                    .FirstOrDefault();

                if (userProfileRole != null)
                {
                    domainService.DeleteUserProfileRole(userProfileRole);
                }
            }

            // ************************
            // Developer
            // ************************

            // Check if the user is in a Group that is a Developer Group
            if (userProfile.Groups.Count(upg => upg.Group.IsDeveloperGroup) > 0)
            {
                // User SHOULD be a Developer - make sure that they have the role
                if (userProfile.Roles.Count(r => r.Role.Name == Constants.Roles.DeveloperRole) == 0)
                {
                    Role role = AllRoles.Where(r => r.Name == Constants.Roles.DeveloperRole).FirstOrDefault();
                    if (role != null)
                    {
                        userProfile.Roles.Add(new UserProfileRole { UserProfile = userProfile, Role = role });
                    }
                }
            }
            else
            {
                //MT - AD Sync Fix
                // User SHOULD NOT be a Developer - make sure that they do not have the role
                UserProfileRole userProfileRole = userProfile.Roles.Where(upr => upr.Role.Name == Constants.Roles.DeveloperRole
                                                                              && upr.UserProfileId == userProfile.Id)
                                                                    .FirstOrDefault();

                if (userProfileRole != null)
                {
                    //userProfile.Roles.Remove(userProfileRole);
                    domainService.DeleteUserProfileRole(userProfileRole);
                }
            }

            // ************************
            // IsDeleted
            // ************************

            if (userProfile.Roles.Count() == 0)
            {
                userProfile.IsDeleted = true;
            }
            else
            {
                userProfile.IsDeleted = false;
            }

            domainService.SaveChanges();
        }

        #endregion

        // ********************************************************************************
        #region Methods used by AppServerInitializer
        // ********************************************************************************

        /// <summary>
        /// Attempts to add Admin groups from the config file to the Groups table.
        /// Fails if the group name doesn't include the domain (ex. domainName\GroupName) or if the Group is not in Active Directory.
        /// If the group is already in the DB - but is deleted - it reactivates it.
        /// </summary>
        public override void SyncAdminGroupsFromAppConfig()
        {
            try
            {
                List<string> adminGroupsFromConfig = GetAdminGroupsFromConfig();

                List<Group> groups = domainService.GetGroups().ToList(); // NOTE: Get Deleted groups too

                foreach (string domainAndGroupName in adminGroupsFromConfig)
                {
                    string[] domainAndGroupNameParts = domainAndGroupName.Split(new char[] { '/', '\\' });

                    //MT - The groups aren't always in domain/group format.  They can also be in
                    //     domain/ou1/ou2/ouX/group format also

                    if (domainAndGroupNameParts.Count() < 2)
                    {
                        ActivityLog.LogException(null, null, "CCSSUserManager", "SyncAdminGroupsFromAppConfig", string.Format(@"Invalid Admin Group Name: {0} in the Web/App Config file.  Must contain Domain\GroupName.", domainAndGroupName));
                        continue;
                    }

                    int lastIndex = domainAndGroupName.LastIndexOf(@"\");
                    if (lastIndex < 0)
                    {
                        lastIndex = domainAndGroupName.LastIndexOf(@"/");
                        if (lastIndex < 0)
                        {
                            ActivityLog.LogException(null, null, "CCSSUserManager", "SyncAdminGroupsFromAppConfig", string.Format(@"Invalid Admin Group Name: {0} in the Web/App Config file.  Must contain Domain\GroupName.", domainAndGroupName));
                        }
                    }

                    string domain = domainAndGroupName.Substring(0, lastIndex);
                    string groupName = domainAndGroupNameParts[domainAndGroupNameParts.Length - 1];


                    // If it is in the DB - but is deleted then re-activate it
                    Group deletedGroup = groups.FirstOrDefault(g => g.Domain.ToUpper() == domain.ToUpper() && g.Name.ToUpper() == groupName.ToUpper() && g.IsDeleted == true);
                    if (deletedGroup != null)
                    {
                        deletedGroup.IsDeleted = false;
                        deletedGroup.IsAppAdminGroup = true;

                        domainService.UpdateGroup(deletedGroup);
                        domainService.SaveChanges();

                        return;
                    }

                    Group group = groups.FirstOrDefault(g => g.Domain.ToUpper() == domain.ToUpper() && g.Name.ToUpper() == groupName.ToUpper());

                    using (ActiveDirectoryManager activeDirectoryManager = new ActiveDirectoryManager())
                    {
                        // If the group is NOT in the DB then create it
                        if (group == null)
                        {
                            GroupPrincipal groupPrincipal = activeDirectoryManager.GetGroupPrincipal(domain, groupName, IdentityType.SamAccountName);

                            if (groupPrincipal == null)
                            {
                                ActivityLog.LogException(null, null, "CCSSUserManager", "SyncAdminGroupsFromAppConfig", string.Format(@"Invalid Admin Group Name: {0} in the Web/App Config file.  Not in Active Directory.", domainAndGroupName));
                                continue;
                            }

                            group = new Group
                            {
                                Name = groupPrincipal.SamAccountName,
                                Domain = domain,
                                ActiveDirectorySId = groupPrincipal.Sid.Value,
                                Description = groupPrincipal.Description,
                                IsAppAdminGroup = true
                            };

                            domainService.InsertGroup(group);
                            domainService.SaveChanges();

                        }

                        AddUsersForGroup(activeDirectoryManager, group, true);

                    }
                }
            }
            catch (Exception e)
            {
                log.Error("An error occurred while syncing the Admin groups from the config.", e);
                throw e;
            }
        }

        private void AddUsersForGroup(ActiveDirectoryManager activeDirectoryManager, Group group, bool updateGroupsAndRoles = false)
        {
            // *******************************************
            // Create a list of all users in all groups
            // *******************************************
            Dictionary<DomainAndLoginId, UserPrincipal> allUserPrincipalsInGroup = new Dictionary<DomainAndLoginId, UserPrincipal>();

            GetUsersForGroup(activeDirectoryManager, allUserPrincipalsInGroup, group);

            domainService.SaveChanges();

            // *******************************************
            // Make sure all of those users are in the system
            // *******************************************

            int count = AddUsers(allUserPrincipalsInGroup, updateGroupsAndRoles);

            domainService.SaveChanges();

            ActivityLog.LogExecutiveActivity("CCSSUserManager", "Added new users from AD Groups", text1: string.Format("Added new users from AD Groups.  Count: {0}", count));
        }

        private static List<string> GetAdminGroupsFromConfig()
        {
            List<string> adminGroupsFromConfig = new List<string>();
            string temp = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.AdminADGroups];

            if (!String.IsNullOrWhiteSpace(temp))
            {
                string[] adGroups = temp.Split(',');

                foreach (string group in adGroups)
                {
                    adminGroupsFromConfig.Add(group.Trim());
                }
            }
            return adminGroupsFromConfig;
        }

        #endregion

        // ********************************************************************************
        #region Methods used by the Executive
        // ********************************************************************************

        /// <summary>
        /// This is the "Master" method called by the executive - to do all sync operations with Active Directory
        /// </summary>
        public override void SyncAllWithActiveDirectory()
        {
            SyncAdminGroupsFromAppConfig();
            domainService.SaveChanges();
            UpdateUserProfilesFromActiveDirectory();
            domainService.SaveChanges();
            UpdateGroupsFromActiveDirectory();
            domainService.SaveChanges();
        }

        /// <summary>
        /// Updates All User Profiles in the system - from Active Directory
        /// </summary>
        private void UpdateUserProfilesFromActiveDirectory()
        {
            List<UserProfile> userProfiles = GetUserProfiles("Roles.Role, Groups.Group").ToList();

            int count = 0;

            using (ActiveDirectoryManager activeDirectoryManager = new ActiveDirectoryManager())
            {
                foreach (UserProfile userProfile in userProfiles)
                {
                    UserPrincipal userPrincipal = activeDirectoryManager.GetUserPrincipal(userProfile.Domain, userProfile.ActiveDirectorySId, IdentityType.Sid);

                    if (userPrincipal == null)
                    {
                        log.InfoFormat("Deleting user");

                        // If the userPrincipal is null (ie. if the user is no longer in AD?  Remove their rights and mark them as deleted.
                        userProfile.IsDeleted = true;
                        userProfile.Groups.Clear();
                        userProfile.Roles.Clear();
                    }
                    else
                    {
                        UpdateUserProfileFromUserPrincipal(userProfile, userPrincipal);
                        UpdateUserProfileGroups(userProfile, userPrincipal);
                        SyncUserRolesFromGroups(userProfile);
                    }

                    count++;
                }
            }

            domainService.SaveChanges();

            ActivityLog.LogExecutiveActivity("CCSSUserManager", "Updated all Users from Active Directory", text1: string.Format("Updated all Users from Active Directory.  Count: {0}", count));
        }

        /// <summary>
        /// NOTE: WE just need to see if there are any users that we don't already have.  
        ///     We aren't concerned with which groups they are in.
        ///     The UpdateUserProfileFromActiveDirectory logic will do that.
        /// </summary>
        private void UpdateGroupsFromActiveDirectory()
        {
            using (ActiveDirectoryManager activeDirectoryManager = new ActiveDirectoryManager())
            {
                // *******************************************
                // Create a list of all users in all groups
                // *******************************************

                Dictionary<DomainAndLoginId, UserPrincipal> allUserPrincipalsInAllGroups = new Dictionary<DomainAndLoginId, UserPrincipal>();

                List<Group> groups = domainService.GetGroups().Where(g => g.IsDeleted == false).ToList();

                foreach (Group group in groups)
                {
                    AddUsersForGroup(activeDirectoryManager, group);
                }

                domainService.SaveChanges();
            }
        }

        private int AddUsers(Dictionary<DomainAndLoginId, UserPrincipal> usersToAdd, bool updateGroupsAndRoles = false)
        {
            int count = 0;
            List<UserProfile> allUserProfiles = GetUserProfiles("Roles.Role, Groups.Group").ToList();

            foreach (DomainAndLoginId domainAndLoginId in usersToAdd.Keys)
            {
                UserPrincipal userPrincipal = usersToAdd[domainAndLoginId];

                // See if they are already in the system - if not, then add them
                UserProfile userProfile = allUserProfiles.Where(up => up.ActiveDirectorySId == userPrincipal.Sid.Value).FirstOrDefault();
                if (userProfile == null)
                {
                    CreateUserProfile(domainAndLoginId, userPrincipal, updateGroupsAndRoles);
                    count++;
                }
                else
                {
                    //The user already exists but may not have the right roles
                    if (updateGroupsAndRoles)
                    {
                        UpdateUserProfileGroups(userProfile, userPrincipal);
                        SyncUserRolesFromGroups(userProfile);
                    }
                }

                domainService.SaveChanges();
            }
            return count;
        }

        private void GetUsersForGroup(ActiveDirectoryManager activeDirectoryManager, Dictionary<DomainAndLoginId, UserPrincipal> users, Group group)
        {
            GroupPrincipal groupPrincipal = activeDirectoryManager.GetGroupPrincipal(group.Domain, group.ActiveDirectorySId, IdentityType.Sid);

            if (groupPrincipal == null)
            {
                group.IsDeleted = true;

                // NOTE:  For future consideration
                // Might need to address groups that have been deleted.  What do we do?  Probably need to remove the associated UserProfile Groups
                //  Probably also need to ignore it everywhere... pretty sure that we are NOT doing this currently

                foreach (UserProfileGroup upg in domainService.GetUserProfileGroups().Where(upg => upg.GroupId == group.Id).ToList())
                {
                    log.InfoFormat("Delete UserProfileGroup {0} - {1}", upg.UserProfileId, upg.GroupId);
                    domainService.DeleteUserProfileGroup(upg);
                }

                // Go on to the next group - we're done with this one.
                return;
            }

            group.Name = groupPrincipal.SamAccountName;
            group.Description = groupPrincipal.Description;

            List<UserPrincipal> userPrincipals = ActiveDirectoryManager.GetUserPrincipalsInGroupPrincipal(groupPrincipal);

            foreach (UserPrincipal userPrincipal in userPrincipals)
            {
                DomainAndLoginId domainAndLoginId = new DomainAndLoginId(group.Domain, userPrincipal.SamAccountName);

                if (!users.ContainsKey(domainAndLoginId))
                {
                    log.InfoFormat("Add User {0} - {1}", domainAndLoginId, userPrincipal.Name);

                    users.Add(domainAndLoginId, userPrincipal);
                }
            }
        }

        #endregion

        // ********************************************************************************
        #region Support Methods
        // ********************************************************************************

        private static void UpdateUserProfileFromUserPrincipal(UserProfile userProfile, UserPrincipal userPrincipal)
        {
            userProfile.FirstName = userPrincipal.GivenName;
            userProfile.LastName = userPrincipal.Surname;
            userProfile.Email = userPrincipal.EmailAddress;
            userProfile.DisplayName = userPrincipal.DisplayName;
            userProfile.ActiveDirectorySId = userPrincipal.Sid.Value;
        }

        #endregion

    }
}
