using System;
using System.Linq;
using System.Web.Security;
using CSG.AppServer.Entities;

namespace CSG.AppServer.Security
{
    public class CustomMembershipProvider : MembershipProvider
    {
        public string Domain { get; set; }

        public CustomMembershipProvider() { }

        /// <summary>
        /// Verifies that the specified user name and password exist in the data source.
        /// </summary>
        public override bool ValidateUser(string loginId, string password)
        {
            UserManagerBase userManager = UserManagerFactory.CreateUserManager();
            return userManager.Login(Domain, loginId, password);
        }

        public void LogOff()
        {
            UserManagerBase userManager = UserManagerFactory.CreateUserManager();
            userManager.LogOut();
        }

        // ********************************************************************************
        #region Not Implemented MembershipProvider Methods
        // ********************************************************************************

        // *******************************
        // Properties
        // *******************************

        public override string ApplicationName
        {
            get { throw new NotImplementedException(); }
            set { throw new NotImplementedException(); }
        }

        public override int MaxInvalidPasswordAttempts { get { throw new NotImplementedException(); } }
        public override int MinRequiredNonAlphanumericCharacters { get { throw new NotImplementedException(); } }
        public override int MinRequiredPasswordLength { get { throw new NotImplementedException(); } }
        public override int PasswordAttemptWindow { get { throw new NotImplementedException(); } }
        public override MembershipPasswordFormat PasswordFormat { get { throw new NotImplementedException(); } }
        public override string PasswordStrengthRegularExpression { get { throw new NotImplementedException(); } }
        public override bool RequiresQuestionAndAnswer { get { throw new NotImplementedException(); } }
        public override bool RequiresUniqueEmail { get { throw new NotImplementedException(); } }
        public override bool EnablePasswordReset { get { throw new NotImplementedException(); } }
        public override bool EnablePasswordRetrieval { get { throw new NotImplementedException(); } }

        // *******************************
        // Methods
        // *******************************

        public void CreateUser(string username, string fullName, string password, string email, string roleName)
        {
            throw new InvalidOperationException("Users must be added through Active Directory.");
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            throw new InvalidOperationException("Passwords are managed through Active Directory.  Please change your password in Active Directory.");
        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer) { throw new NotImplementedException(); }
        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status) { throw new NotImplementedException(); }
        public override bool DeleteUser(string username, bool deleteAllRelatedData) { throw new NotImplementedException(); }
        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords) { throw new NotImplementedException(); }
        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords) { throw new NotImplementedException(); }
        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords) { throw new NotImplementedException(); }
        public override int GetNumberOfUsersOnline() { throw new NotImplementedException(); }
        public override string GetPassword(string username, string answer) { throw new NotImplementedException(); }
        public override MembershipUser GetUser(string username, bool userIsOnline) { throw new NotImplementedException(); }
        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline) { throw new NotImplementedException(); }
        public override string GetUserNameByEmail(string email) { throw new NotImplementedException(); }
        public override string ResetPassword(string username, string answer) { throw new NotImplementedException(); }
        public override bool UnlockUser(string userName) { throw new NotImplementedException(); }
        public override void UpdateUser(MembershipUser user) { throw new NotImplementedException(); }

        #endregion
    }
}

