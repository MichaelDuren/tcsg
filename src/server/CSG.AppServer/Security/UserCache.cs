﻿using System.Collections.Generic;
using CSG.AppServer.Entities;
using Util;
using Util.Collections.Generic;

namespace CSG.AppServer.Security
{
    /// <summary>
    /// User Cache - based on DomainAndLoginId.
    /// </summary>
    public class UserCache
    {
        private static Cache<string, UserProfile> internalCache = new Cache<string, UserProfile>(CacheType.SlidingExpiration);

        public List<UserProfile> Values
        {
            get { return new List<UserProfile>(internalCache.Values); }
        }

        public virtual void Add(UserProfile userProfile)
        {
            Guard.ArgumentNotNull(userProfile, "userProfile");
            internalCache.Add(GetCacheKey(userProfile.DomainAndLoginId), userProfile, (double)AppServerContext.UserTimeout);
        }

        public bool ContainsKey(DomainAndLoginId domainAndLoginId)
        {
            return internalCache.ContainsKey(GetCacheKey(domainAndLoginId));
        }

        public UserProfile Get(DomainAndLoginId domainAndLoginId)
        {
            return internalCache.Get(GetCacheKey(domainAndLoginId));
        }

        public virtual bool Remove(UserProfile userProfile)
        {
            return this.Remove(userProfile.DomainAndLoginId);
        }

        public virtual bool Remove(DomainAndLoginId domainAndLoginId)
        {
            return internalCache.Remove(GetCacheKey(domainAndLoginId));
        }

        public void Touch(UserProfile userProfile)
        {
            this.Touch(userProfile.DomainAndLoginId);
        }

        public void Touch(DomainAndLoginId domainAndLoginId)
        {
            internalCache.Touch(GetCacheKey(domainAndLoginId));
        }

        public void Clear()
        {
            internalCache.Clear();
        }

        private string GetCacheKey(DomainAndLoginId domainAndLoginId)
        {
            Guard.ArgumentNotNull(domainAndLoginId.LoginId, "LoginId");
            Guard.ArgumentNotNull(domainAndLoginId.Domain, "Domain");
            return string.Format("{0}/{1}", domainAndLoginId.Domain.ToUpper(), domainAndLoginId.LoginId.ToUpper());
        }

    }
}
