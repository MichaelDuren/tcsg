using Util.Config;
using Util;
using System;

namespace CSG.AppServer.Security
{
    public class UserManagerFactory
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly Type ThisDeclaringType = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;

        private static bool initialized;
        private static bool useTestUserManager;

        public static UserManagerBase CreateUserManager()
        {
            if (!initialized)
            {
                Initialize();
            }

            return new PortalUserManagerAD();
        }

        private static void Initialize()
        {
            string _useTestUserManager_string = null;
            try
            {
                _useTestUserManager_string = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.UseTestUserManager];
                useTestUserManager = StringTools.GetBoolean(_useTestUserManager_string);
            }
            catch
            {
                throw new Exception(string.Format("Invalid value ({0}) for UseTestUserManager", _useTestUserManager_string));
            }

            initialized = true;
        }

    }
}
