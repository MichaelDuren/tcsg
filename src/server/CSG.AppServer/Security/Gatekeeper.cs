using System;
using System.Linq;
using CSG.AppServer.Db;
using CSG.AppServer.Entities;
using CSG.AppServer.Exceptions;
using CSG.AppServer.Managers;
using Util;
using System.Collections.Generic;
using ESPLogWrapper;

namespace CSG.AppServer.Security
{
    public class Gatekeeper
    {
        // ******************************************************************************
        #region Properties / Attributes
        // ******************************************************************************
        class JwtTokenCacheEntry
        {
            public string token { get; set; }
            public DateTime expriation { get; set; }
            public string loginId { get; set; }
        }

        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // Used for Logging - to tell log4net wrapper to use THIS class as the boundary between what is being logged and the internals - so that all logging statements show the class / method / line number of whatever Invoked this class
        private static readonly Type ThisDeclaringType = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;

        private static int logCount = 0;

        /// <summary>
        /// Class name that is requesting the query, etc. to be executed (for Activity Logging). Discovered automatically when the Gatekeeper is instantiated.
        /// </summary>
        public string CallingClassName { get; private set; }

        /// <summary>
        /// Method name that is requesting the query, etc. to be executed (for Activity Logging). Discovered automatically when the Gatekeeper is instantiated.
        /// </summary>
        public string CallingMethodName { get; private set; }

        private UserProfile _userProfile;
        private string _oauth_access_token = string.Empty;

        private DateTime startTime = DateTime.Now;

        #endregion

        // ******************************************************************************
        #region Constructors
        // ******************************************************************************

        public Gatekeeper()
        {
            LocationInfo locationInfo = new LocationInfo(ThisDeclaringType);
            this.CallingClassName = CleanupGenericClassNames(locationInfo.ClassName);
            this.CallingMethodName = locationInfo.MethodName;

            logCount++;
        }

        public Gatekeeper(UserProfile userProfile)
        {
            LocationInfo locationInfo = new LocationInfo(ThisDeclaringType);
            this.CallingClassName = CleanupGenericClassNames(locationInfo.ClassName);
            this.CallingMethodName = locationInfo.MethodName;

            logCount++;

            _userProfile = userProfile;
        }
        public Gatekeeper(string token) : this()
        {
            _oauth_access_token = token;
        }

        private string CleanupGenericClassNames(string className)
        {
            int i = className.IndexOf('`');
            if (i > 0)
            {
                className = className.Substring(0, i);
            }

            return className;
        }

        #endregion

        // ******************************************************************************
        #region Core Methods
        // ******************************************************************************

        /// <summary>
        /// Gets the User Profile for the current user.
        /// Note: It is POSSIBLE that this is null if the user is not logged in - or if they have expired
        /// </summary>
        public UserProfile GetCurrentUserProfile(bool failIfNotLoggedIn = true, bool refreshProfile = false)
        {
            // If we don't want to fail if the user is not logged in - then we can't check the online status
            if (failIfNotLoggedIn)
            {
                CheckApplicationOnlineStatus();
            }
            return _getCurrentUserProfile(failIfNotLoggedIn, refreshProfile);
        }

        private UserProfile _getCurrentUserProfile(bool failIfNotLoggedIn = true, bool refreshProfile = false)
        {

            if (_userProfile == null)
            {
                if (_oauth_access_token.Length > 0)
                {
                    _userProfile = GetUserProfileFromToken(_oauth_access_token);
                }
                else
                {
                    _userProfile = UserManagerFactory.CreateUserManager().GetCurrentUserProfile();
                    if (_userProfile == null && failIfNotLoggedIn)
                    {
                        throw new LoginException("User is not currently logged in.", LoginExceptionSubType.UserIsNotLoggedIn);
                    }
                }

            }

            if (refreshProfile)
            { 
                _userProfile = UserManagerFactory.CreateUserManager().GetUserProfile(_userProfile.LoginId);
                if (_userProfile == null && failIfNotLoggedIn)
                {
                    throw new LoginException("User is not currently logged in.", LoginExceptionSubType.UserIsNotLoggedIn);
                }

                // update the local cached user data
                UserManagerFactory.CreateUserManager().UpdateCurrentUserProfile(_userProfile);

            }
            return _userProfile;
        }

        public bool IsUserInAdminRole()
        {
            try
            {
                UserProfile userProfile = GetCurrentUserProfile();
                return userProfile.IsAppAdmin;
            }
            catch
            {
                return false;
            }
        }


        /// <summary>
        /// Validates that the user is an app admin.
        /// Throws an exception if they are not
        /// </summary>
        public void ValidateUserIsAppAdmin()
        {
            UserProfile userProfile = GetCurrentUserProfile();

            if (!userProfile.IsAppAdmin)
            {
                throw new UserRightsException("User is not an app admin");
            }
        }

        public bool IsUserApprover()
        {
            bool bAnswer = false;  // not authorized unless explicitly allowed

            try
            {
                UserProfile userProfile = GetCurrentUserProfile();

                if (userProfile.IsAppAdmin)
                {
                    bAnswer = true;
                }
                else
                {
                    var userCsts = userProfile.CodeSigningTypes.Where(cst => cst.AuthorizedUserAction == UserActions.Approve1Requests ||
                                                                              cst.AuthorizedUserAction == UserActions.Approve2Requests ||
                                                                              cst.AuthorizedUserAction == UserActions.ManagerApproveRequests)
                                                                              .FirstOrDefault();
                    if (userCsts != null)
                    {
                        bAnswer = true;
                    }
                }
                return bAnswer;
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// Validates that the user can update or remove a temporal window
        /// </summary>
        public void ValidateTemporalApprovalModification(CodeSigningTemporalApproval ta)
        {
            UserProfile userProfile = GetCurrentUserProfile();

            if (!userProfile.IsAppAdmin && ta.ApprovingUserProfileId != userProfile.Id)
            {
                throw new UserRightsException("User is not authorized to modify the approval window");
            }
        }

        /// <summary>
        /// Validates that the user is a developer in the specified group.
        /// Throws an exception if they are not
        /// </summary>
        public void ValidateUserDeveloperInGroup(int groupId)
        {
            UserProfile userProfile = GetCurrentUserProfile();

            var groupInQuestion = userProfile.Groups.Where(g => g.GroupId == groupId).FirstOrDefault();
            if (groupInQuestion !=  null)
            {
                if (groupInQuestion.Group.IsDeveloperGroup)
                {
                    return;
                }
            }

            throw new UserRightsException("User is not in group or group is not a developer group, groupId: " + groupId);

        }

        /// <summary>
        /// Validates that the group has access to this signing type.
        /// Throws an exception if they are not
        /// </summary>
        public void ValidateGroupAuthorizedForType(int groupId, int signingTypeId)
        {
            ESPDbDomainService domainService = new ESPDbDomainService();

            try
            {
                var cst = domainService.GetCodeSigningTypeGroups().Where(m => m.GroupId == groupId && m.CodeSigningTypeId == signingTypeId);
            }
            catch
            {
                throw new UserRightsException(String.Format("Group {0} does not have access to signing type {1}.", groupId, signingTypeId));
            }

        }


        /// <summary>
        /// Validates that the user is authorized for a specific signing type
        /// </summary>
        public void ValidateUserAuthorizedForType(int signingTypeId)
        {
            UserProfile userProfile = GetCurrentUserProfile();
            ESPDbDomainService domainService = new ESPDbDomainService();

            // admins may alway view
            if (userProfile.IsAppAdmin)
            {
                return;
            }

            if (domainService.GetUserProfileCodeSigningTypes(null)
                                                .Where(cstu => (cstu.CodeSigningTypeId == signingTypeId) && (cstu.UserProfileId == userProfile.Id))
                                                .Select(cst => cst).Count() == 0)
            {
                throw new UserRightsException("User is not authorized for signing type: " + signingTypeId);
            }
        }

        /// <summary>
        /// Validates that the user is a developer in the group associated with the specified request
        /// Throws an exception if they are not
        /// </summary>
        public void ValidateUserDeveloperInRequest(int requestId)
        {
            ESPDbDomainService domainService = new ESPDbDomainService();
            var request = domainService.GetCodeSigningRequestById(requestId);
            Guard.ValueNotNull(request, "Unable to find CodeSignRequest with Id: " + requestId);

//            ValidateUserDeveloperInGroup(request.GroupId);
        }


        /// <summary>
        /// Users can always view requests they submit.  Otherwise, you need to have approval rights to deny
        /// </summary>
        public void ValidateUserViewRequest(int requestId)
        {
            ESPDbDomainService domainService = new ESPDbDomainService();
            var request = domainService.GetCodeSigningRequestById(requestId);
            Guard.ValueNotNull(request, "Unable to find CodeSignRequest with Id: " + requestId);

            UserProfile userProfile = GetCurrentUserProfile();

            // if the user submitted this request or if they are an appAdmin or if they previously approved a request, then they can view it. 
            if (userProfile.Id != request.CreatedByUserId && userProfile.IsAppAdmin == false &&
                 userProfile.Id != request.Approver1ResponderId &&
                 userProfile.Id != request.Approver2ResponderId)
            {
                // if the user is in this requests group, they can view it
                if (userProfile.Groups.Select(g => g.GroupId == request.GroupId).Count() == 0)
                {
                    // user is not in the developer group, then they must be an approver to see it
                    ValidateUserApproverInRequest(requestId);
                }
            }
        }


        public void ValidateUserForActionOnSigningType(int signingTypeId, UserActions action)
        {
            UserProfile userProfile = GetCurrentUserProfile();

            // admins can do almost anything
            if (!userProfile.IsAppAdmin)
            {
                var userSigningTypes = userProfile.CodeSigningTypes;
                if (userSigningTypes == null)
                {
                    throw new UserRightsException("User does not have any signing rights in this system.");
                }

                ESPDbDomainService domainService = new ESPDbDomainService();
                // get the code signing type details
                CodeSigningType cst = domainService.GetCodeSigningTypeById(signingTypeId);
                Guard.ValueNotNull(cst, "There is no signing type configured for this id: " + signingTypeId);

                // Check to determine if this user has this entitlement
                var matchingEntitlement = userProfile.CodeSigningTypes.Where(entry => entry.CodeSigningTypeId == signingTypeId &&
                                                                                      entry.UserSigningTypeRole == (int)action).ToList();
                if (matchingEntitlement == null || matchingEntitlement.Count() == 0)
                {
                    throw new UserRightsException("User does not have permission to perform this action against signing type: " + signingTypeId);
                }
            }

        }

        public void ValidateUserApproverOnSigningType(int signingTypeId)
        {
            UserProfile userProfile = GetCurrentUserProfile();

            // admins can do almost anything
            if (!userProfile.IsAppAdmin)
            {
                var userSigningTypes = userProfile.CodeSigningTypes;
                if (userSigningTypes == null)
                {
                    throw new UserRightsException("User does not have any signing rights in this system.");
                }

                var privileges = userProfile.CodeSigningTypes.Where(m => m.CodeSigningTypeId == signingTypeId &&
                                                                         (m.AuthorizedUserAction == UserActions.Approve1Requests ||
                                                                          m.AuthorizedUserAction == UserActions.Approve2Requests ||
                                                                          m.AuthorizedUserAction == UserActions.ManagerApproveRequests));
                if (privileges.Count() == 0)
                {
                    throw new UserRightsException("User does not have permission to perform this action against signing type: " + signingTypeId);
                }
            }

        }

        /// <summary>
        /// Verifies that a user has permissions on a specific signing type as specifed by the Guid
        /// </summary>
        public void ValidateUserForActionOnSigningType(string guid, UserActions action)
        {
            UserProfile userProfile = GetCurrentUserProfile();

            ESPDbDomainService domainService = new ESPDbDomainService();
            var signingTypeSearch = domainService.GetCodeSigningTypes(null)
                                                                      .Where(type => guid.Equals(type.Guid) &&
                                                                             type.IsDeleted == false &&
                                                                             type.RawSign == true).First();
            if (signingTypeSearch == null)
            {
                throw new UserRightsException("Signing profile with this guid does not exist.");
            }

            // get the code signing type details
            CodeSigningType cst = domainService.GetCodeSigningTypeById(signingTypeSearch.Id);
            Guard.ValueNotNull(cst, "There is no signing profile configured for this id: " + signingTypeSearch.Id);

            // Check to determine if this user has this entitlement
            var matchingEntitlement = userProfile.CodeSigningTypes.Where(entry => entry.CodeSigningTypeId == signingTypeSearch.Id &&
                                                                                  entry.UserSigningTypeRole == (int)action).FirstOrDefault();
            if (matchingEntitlement == null)
            {
                throw new UserRightsException("User does not have permission to perform this action against signing profile: " + signingTypeSearch.Id);
            }
        }

        /// <summary>
        /// Validates that the user is a developer in the group associated with the specified request
        /// Throws an exception if they are not
        /// </summary>
        public void ValidateUserApproverInRequest(int requestId)
        {
            ESPDbDomainService domainService = new ESPDbDomainService();
            var request = domainService.GetCodeSigningRequestById(requestId);
            Guard.ValueNotNull(request, "Unable to find CodeSignRequest with Id: " + requestId);

            UserProfile userProfile = GetCurrentUserProfile();

            if (request.CreatedByUserId == userProfile.Id)
            {
                throw new UserRightsException("User is not an approved approver and is not a developer in groupId: ");
            }

            if (userProfile.Groups.Where(grp => grp.GroupId == request.GroupId).Count() > 0)
            {
                // user in in the developer group for this request
                return;
            }

            // if the profile allows for designated approvers, then check to see if this is an approver
            if (request.CodeSigningType.AllowApprover2Approval)
            {
                // get the list of approvder groups for this code signing type
                IEnumerable<int> cstgs = domainService.GetCodeSigningTypeGroups(null)
                                                                        .Where(m => m.CodeSigningTypeId == request.CodeSigningTypeId && m.IsApprover == true)
                                                                        .Select(m => m.GroupId);
                // if the user is in one of these groups, then they are an approver.
                if (userProfile.Groups.Where(grp => cstgs.Contains(grp.GroupId)).Count() > 0)
                {
                    return;
                }
            }

            // if we reach this point then the user is no an approver
            throw new UserRightsException("User is not an approved approver and is not a developer in groupId: ");

        }

        /// <summary>
        /// Returns true if the user is an approver to the provided request
        /// </summary>
        public bool IsUserApproverForRequest(CodeSigningRequest request)
        {
            UserProfile userProfile = GetCurrentUserProfile();
            bool result = false;

            if (request.CreatedByUserId != userProfile.Id)
            {
                CodeSigningRequestManager mgr = new CodeSigningRequestManager();
                var groupIds = mgr.GetApproverGroupIds(request);

                if (groupIds.Intersect(userProfile.Groups.Select(g => g.GroupId)).Count() > 0)
                {
                    result = true;
                }                
            }
            return result;
        }

        /// <summary>
        /// Validates that the user is a developer in the group associated with the specified request
        /// Throws an exception if they are not
        /// </summary>
        public void ValidateUserSubmittedRequest(int requestId)
        {
            ESPDbDomainService domainService = new ESPDbDomainService();
            var request = domainService.GetCodeSigningRequestById(requestId);
            Guard.ValueNotNull(request, "Unable to find CodeSignRequest with Id: " + requestId);

            UserProfile userProfile = GetCurrentUserProfile();

            if (request.CreatedByUserId != userProfile.Id)
            {
                throw new UserRightsException("User cannot update this request.");
            }
        }


        /// <summary>
        /// Validates that the user is approved as an offline signer for a request.
        /// Throws an exception if they are not
        /// </summary>
        public void ValidateUserOfflineSignerInRequest(int requestId)
        {
            ESPDbDomainService domainService = new ESPDbDomainService();
            var request = domainService.GetCodeSigningRequestById(requestId);
            Guard.ValueNotNull(request, "Unable to find CodeSignRequest with Id: " + requestId);

            UserProfile userProfile = GetCurrentUserProfile();

            if (request.CreatedByUserId == userProfile.Id)
            {
                throw new UserRightsException("Users cannot sign their own requests.");
            }

            if (request.Approver1ResponderId == userProfile.Id || 
                request.Approver2ResponderId == userProfile.Id ||
                request.ManagerResponderId == userProfile.Id)
            {
                throw new UserRightsException("Users cannot sign and approve requests.");
            }

            ValidateUserForActionOnSigningType(request.CodeSigningTypeId, UserActions.OfflineSignRequests);
        }


        public void CheckApplicationOnlineStatus()
        {
            bool onlineForUser;
            UserProfile userProfile = _getCurrentUserProfile(failIfNotLoggedIn: true);

            switch (AppServerContext.ApplicationOnlineStatus)
            {
                case ApplicationOnlineStatus.Online:
                    onlineForUser = true;
                    break;

                case ApplicationOnlineStatus.AppAdminOnly:
                    if (userProfile.IsAppAdmin) { onlineForUser = true; }
                    else { onlineForUser = false; }
                    break;

                case ApplicationOnlineStatus.Offline:
                    onlineForUser = false;
                    break;

                default:
                    throw new InvalidOperationException("Should never get here.");
            }

            if (onlineForUser != true)
            {
                throw new AppServerStatusException("The Application Server is currently having a server outage.", string.Empty);
            }
        }

        #endregion

        // ******************************************************************************
        #region Identity Methods
        // ******************************************************************************
        /// <summary>
        /// Gets the User Profile for the current user.
        /// Note: It is POSSIBLE that this is null if the user is not logged in - or if they have expired
        /// </summary>
        public UserProfile GetUserProfileFromToken(string authToken)
        {
            OAuth2 oauth = new OAuth2();

            string loginId = oauth.VerifyToken(authToken);

            if (loginId == null || loginId.Length == 0)
            {
                throw new LoginException("User could not be authenticated", LoginExceptionSubType.UserOAuthTokenNotValid);
            }

            ESPDbDomainService domainService = new ESPDbDomainService();

            UserManagerBase userManager = UserManagerFactory.CreateUserManager();

            _userProfile = userManager.GetUserProfile(loginId);
            if (_userProfile == null)
            {
                throw new LoginException("User is not currently logged in.", LoginExceptionSubType.UserIsNotLoggedIn);
            }

            return _userProfile;
        }
        #endregion

        // ********************************************************************************
        #region Logging Methods
        // ********************************************************************************

        public void LogUserActivity(string activityType)
        {
            UserProfile userProfile = GetCurrentUserProfile(failIfNotLoggedIn: false);

            ActivityLog.Log(
                userProfile,
                ActivityLog.LogEventId_LOG_ENTRY,
                activityType,
                string.Format("{0}.{1}()", CallingClassName, CallingMethodName),
                text1: string.Format("log #: {0}", logCount),
                callerStackBoundaryDeclaringType: ThisDeclaringType);
        }

        public void LogUserActivity(string activityType, int requestId)
        {
            UserProfile userProfile = GetCurrentUserProfile(failIfNotLoggedIn: false);

            ActivityLog.Log(
                userProfile,
                ActivityLog.LogEventId_LOG_ENTRY,
                activityType,
                string.Format("{0}.{1}()", CallingClassName, CallingMethodName),
                text1: string.Format("log #: {0}", logCount),
                text2: string.Format("requestId: {0}", requestId),
                callerStackBoundaryDeclaringType: ThisDeclaringType);
        }

        public void LogElapsedTime(string activityType, string details = null)
        {
            UserProfile userProfile = GetCurrentUserProfile(failIfNotLoggedIn: false);

            TimeSpan ts = DateTime.Now - startTime;
            int elapsedMilliseconds = (int)(ts.Ticks / TimeSpan.TicksPerMillisecond);

            ActivityLog.Log(
                userProfile,
                ActivityLog.LogEventId_LOG_ENTRY,
                activityType,
                string.Format("{0}.{1}()", CallingClassName, CallingMethodName),
                text1: string.Format("log #: {0}", logCount),
                details: details,
                elapsedMilliseconds: elapsedMilliseconds,
                callerStackBoundaryDeclaringType: ThisDeclaringType);
        }

        public virtual Exception LogException(string messsage, Exception e, int eventId = 0)
        {
            UserProfile userProfile = GetCurrentUserProfile(failIfNotLoggedIn: false);

            if (userProfile == null)
            {
                ActivityLog.LogException(null, null, eventId, CallingClassName, CallingMethodName,
                                         messsage, exception: e, callerStackBoundaryDeclaringType: ThisDeclaringType);
            }
            else
            {
                ActivityLog.LogException(userProfile.DomainAndLoginId, userProfile.FullName, eventId,
                                         CallingClassName, CallingMethodName, messsage, exception: e, callerStackBoundaryDeclaringType: ThisDeclaringType);
            }

            return e;
        }

        #endregion
    }
}
