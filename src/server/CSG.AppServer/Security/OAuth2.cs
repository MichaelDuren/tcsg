using System;
using System.Linq;
using CSG.AppServer.Db;
using CSG.AppServer.Entities;
using CSG.AppServer.Exceptions;
using CSG.AppServer.Managers;
using System.Net;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Collections.Concurrent;

namespace CSG.AppServer.Security
{

    public class OAuth2
    {
        class JwtTokenCacheEntry
        {
            public string token { get; set; }
            public DateTime expiration { get; set; }
            public string loginId { get; set; }
        }

        public class AccessTokenLoginId
        {
            public string login_id { get; set; }
        }
        public class AccessTokenValidation
        {
            public AccessTokenLoginId access_token { get; set; }
            public string token_type { get; set; }
            public int expires_in { get; set; }
            public string client_id { get; set; }
        }
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        // Used for Logging - to tell log4net wrapper to use THIS class as the boundary between what is being logged and the internals - so that all logging statements show the class / method / line number of whatever Invoked this class
        private static readonly Type ThisDeclaringType = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;

        // stores valid jwt tokens
        private static ConcurrentDictionary<string, JwtTokenCacheEntry> jwtTokenCache = new ConcurrentDictionary<string, JwtTokenCacheEntry>();

        public OAuth2()
        {

        }
        private static bool AcceptServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            bool retVal = false;
            HttpWebRequest pReq = (HttpWebRequest)sender;

            if (SslPolicyErrors.None != (sslPolicyErrors & SslPolicyErrors.RemoteCertificateChainErrors))
            {
                if (AppServerContext.OAuthAuthenticateIgnoreCertChainErrors)
                {
                    retVal = true;
                }
            }

            if (SslPolicyErrors.None != (sslPolicyErrors & SslPolicyErrors.RemoteCertificateNameMismatch))
            {
                if (AppServerContext.OAuthAuthenticateIgnoreNameMismatch)
                {
                    retVal = true;
                }
            }
            return retVal;
        }

        public string GetOAuthServerURL()
        {
            var authServer = SystemSettingsManager.Current.GetBySettingName(Constants.SettingNames.OAuthServerURL, true).StringValue;
            if (String.IsNullOrEmpty(authServer))
            {
                log.Error("Error getting the OAuthServer ULR");
            }
            return authServer;
        }

        private X509Certificate2 GetSSLClientCertificate()
        {
            X509Certificate2 sslCert = null;

            // use the configuration flag to determine which store to look in
            StoreLocation certLocation = (AppServerContext.OAuthClientUseSystemStore) ? StoreLocation.LocalMachine : StoreLocation.CurrentUser;
            X509Store store = new X509Store("MY", certLocation);
            try
            {
                store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
                X509Certificate2Collection certCollection = (X509Certificate2Collection)store.Certificates;
                if (certCollection != null)
                {
                    X509Certificate2Collection foundCollection = (X509Certificate2Collection)certCollection.Find(X509FindType.FindByThumbprint,
                                                                                                                 AppServerContext.OAuthVerifyClientCertificate,
                                                                                                                 false);
                    if (foundCollection.Count < 1)
                    {
                        log.Error("Failed to location OAuth client certificate thumbprint in store.");
                    }
                    else
                    {
                        sslCert = foundCollection[0];

                    }
                }
            }
            catch (Exception ex)
            {
                log.Error("Failed to retreive client certificate for OAuth authentication", ex);
            }
            return sslCert;
        }

        public string VerifyToken(string authToken)
        {
            // lets first look the token cache
            if (jwtTokenCache.ContainsKey(authToken))
            {
                var cachedTokenEntry = jwtTokenCache[authToken];
                if (cachedTokenEntry != null)
                {
                    if (DateTime.Compare(DateTime.Now, cachedTokenEntry.expiration) < 0)
                    {
                        log.Info("Found valid cached token entry for user: " + cachedTokenEntry.loginId);
                        return cachedTokenEntry.loginId;
                    }
                    else
                    {
                        // remove the item from the cache
                        jwtTokenCache.TryRemove(authToken, out cachedTokenEntry);
                    }
                }
            }

            string userId = null;
            string oauthServerURL = GetOAuthServerURL();
            string jsonResponse = string.Empty;
            string expiresIn = String.Empty;
            string contentParameters;
            string oauth_client_id = String.Empty;

            var request = (HttpWebRequest)WebRequest.Create(oauthServerURL);
            if (AppServerContext.OAuthAuthenticateToServer)
            {
                X509Certificate sslCert = GetSSLClientCertificate();
                if (sslCert == null)
                {
                    throw new Exception("Cannot find a valid SSL client certificate for oauth server.");
                }
                request.ClientCertificates.Add(sslCert);
                if (!string.IsNullOrEmpty(AppServerContext.OAuthClientId))
                {
                    oauth_client_id = AppServerContext.OAuthClientId;
                }
                else
                {
                    var subjectDn = CertificateManager.ParseDistinguishedName(sslCert.Subject);
                    if (subjectDn != null)
                    {
                        oauth_client_id = subjectDn.Find(item => item.Key.Equals("CN")).Value;
                    }
                }
            }
            if (oauth_client_id.Length == 0)
            {
                if (!string.IsNullOrEmpty(AppServerContext.OAuthClientId))
                {
                    oauth_client_id = AppServerContext.OAuthClientId;
                }
                else
                {
                    oauth_client_id = Environment.UserName;
                }
            }

            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";
            contentParameters = String.Format("client_id={0}&grant_type=urn:pingidentity.com:oauth2:grant_type:validate_bearer&token={1}",
                          oauth_client_id, authToken);
            request.ContentLength = contentParameters.Length;
            request.Credentials = CredentialCache.DefaultCredentials;
            request.ServerCertificateValidationCallback = AcceptServerCertificate;
            try
            {
                log.Debug("Connecting to OAuth server for validation: " + oauthServerURL);
                log.Debug("Content Parameters: " + contentParameters);
                Stream dataStream = request.GetRequestStream();
                using (StreamWriter writer = new StreamWriter(request.GetRequestStream(), Encoding.ASCII))
                {
                    writer.Write(contentParameters);
                }

                // Get the server respsonse
                var response = (HttpWebResponse)request.GetResponse();

                // this should not be null, but this check shows that we care
                if (response != null)
                {
                    var rc = response.StatusCode;
                    string tokenType = string.Empty;

                    log.Debug(String.Format("Status code return from OAuth server: {0}", rc));

                    // Add headers to response dictionary
                    if (response.ContentLength != 0)
                    {
                        // Read message Content
                        using (Stream responseStream = response.GetResponseStream())
                        {
                            using (StreamReader sr = new StreamReader(responseStream, Encoding.GetEncoding("utf-8")))
                            {
                                jsonResponse = sr.ReadToEnd();
                                log.Debug(String.Format("Response received from OAuth server: {0} bytes", jsonResponse.Length));
                            }
                        }
                    }

                    if (jsonResponse.Length == 0)
                    {
                        log.Error("OAuth server returned a zero length response.");
                    }
                    else
                    {
                        var dict = JsonConvert.DeserializeObject<AccessTokenValidation>(jsonResponse);

                        log.Info(String.Format("Obtained new bearer token for {0} that expires in {1} minutes", dict.client_id, dict.expires_in));

                        var newCacheEntry = new JwtTokenCacheEntry()
                        {
                            loginId = dict.client_id,
                            expiration = DateTime.Now.AddSeconds(dict.expires_in),
                            token = authToken
                        };

                        // add this to the dictionary
                        jwtTokenCache.AddOrUpdate(authToken, newCacheEntry, (key, value) => newCacheEntry);

                        userId = dict.client_id;
                    }
                }
            }
            catch (WebException ex)
            {
                // if the server responded, read and record the response
                if (ex.Response != null)
                {
                    using (var stream = ex.Response.GetResponseStream())
                    using (var reader = new StreamReader(stream))
                    {
                        log.Error("Server returned the following content: " + reader.ReadToEnd());
                    }
                }
                else
                {
                    // this could be a protocol failure (i.e. unauthorized cert)
                    log.Error("Server connection failed with a WebExecption: " + ex.Message);
                }
            }
            catch (Exception ex)
            {
                log.Error("GetBearerToken", ex);
            }

            return userId;
        }

    }
}
