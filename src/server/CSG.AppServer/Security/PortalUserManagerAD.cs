using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using CSG.AppServer.Entities;
using CSG.AppServer.Exceptions;
using CSG.AppServer.Managers;
using Util;
using Util.Config;
using CSG.AppServer.Db;
using System.Data.Entity.Core.Objects;
using System.DirectoryServices;

namespace CSG.AppServer.Security
{
    public class PortalUserManagerAD : UserManagerBase
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly Type ThisDeclaringType = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;

        // ********************************************************************************
        #region Properties and Variables
        // ********************************************************************************

        private List<Group> _allGroups;
        public List<Group> AllGroups
        {
            get
            {
                if (_allGroups == null)
                {
                    _allGroups = domainService.GetGroups().Where(g => g.IsDeleted == false).ToList();
                }
                return _allGroups;
            }
        }

        private List<Role> _allRoles;
        public List<Role> AllRoles
        {
            get
            {
                if (_allRoles == null)
                {
                    _allRoles = domainService.GetRoles().ToList();
                }
                return _allRoles;
            }
        }

        #endregion

        // Internal Constructor
        internal PortalUserManagerAD(ESPDbDomainService sharedDomainService = null) { }

        // *********************************************************************************
        #region Core Method Overrides
        // *********************************************************************************


        /// <summary>
        /// Returns true of the user is authenticated
        /// </summary>
        protected override LoginProfile IsAuthenticated(string domain, string loginId, string password)
        {
            Guard.ValueNotBlankOrNull(domain, "The Domain must be set before attempting authentication");
            using (ActiveDirectoryManager activeDirectoryManager = new ActiveDirectoryManager())
            {
                LoginProfile lp;
                lp = new LoginProfile();

                lp.Authenticated = activeDirectoryManager.Login(domain, loginId, password);
                if (lp.Authenticated)
                {
                    using (UserPrincipal up = activeDirectoryManager.GetUserPrincipal(domain, loginId))
                    {
                        lp.SId = up.Sid.Value;
                    }
                }
                else
                {
                    lp.SId = "";
                }

                return lp;
            }
        }

        /// <summary>
        /// Create a new User Profile for the login id provided and populate the properties with data from Active Directory
        /// </summary>
        protected override UserProfile CreateUserProfile(DomainAndLoginId domainAndLoginId, bool updateGroupsAndRoles = true)
        {
            using (ActiveDirectoryManager activeDirectoryManager = new ActiveDirectoryManager())
            {
                using (UserPrincipal userPrincipal = activeDirectoryManager.GetUserPrincipal(domainAndLoginId.Domain, domainAndLoginId.LoginId))
                {
                    // Note: this should not happen - because the user should be authenticated before we get here.
                    if (userPrincipal == null)
                    {
                        throw new LoginException("User does not exist in Active Directory", LoginExceptionSubType.UserNotInActiveDirectory);
                    }

                    UserProfile userProfile = CreateUserProfile(domainAndLoginId, userPrincipal, updateGroupsAndRoles);

                    // MJD: This is already done in CreateUserProfile.  This causes an exception on first login where
                    //      and AD Sync was not performed in advance.
                    //  domainService.InsertUserProfile(userProfile);
                    //  domainService.SaveChanges();

                    // Just to make sure that we've got exactly what is in the DB - get it fresh from the DB after creating it.
                    userProfile = GetFullUserProfileByDomainAndLoginId(domainAndLoginId, userPrincipal.Sid.Value);

                    return userProfile;
                }
            }
        }
        private UserProfile CreateUserProfile(DomainAndLoginId domainAndLoginId, UserPrincipal userPrincipal, bool updateGroupsAndRoles = true)
        {
            Guard.ArgumentNotNull(userPrincipal, "userPrincipal");

            UserProfile userProfile = new UserProfile
            {
                Domain = domainAndLoginId.Domain,
                LoginId = domainAndLoginId.LoginId,
                LoginCount = 1,
            };

            domainService.InsertUserProfile(userProfile);

            UpdateUserProfileFromUserPrincipal(userProfile, userPrincipal);

            if (updateGroupsAndRoles)
            {
                UpdateUserProfileGroups(userProfile, userPrincipal);
                SyncUserRolesFromGroups(userProfile);

                // Need to confirm if the user DOES actually have rights to the system (ie. if they have any roles)
                if (userProfile.Groups.Count() == 0)
                {
                    throw new LoginException("User does not have any Rights (roles) in the system", LoginExceptionSubType.UserHasNoRoles);
                }
            }

            domainService.SaveChanges();


            ActivityLog.Log(domainAndLoginId, ActivityLog.LogEventId_LOG_ENTRY, userProfile.FullName, "CCSSUserManager", "Created User Profile", string.Format("Creating user profile for user - Login/Name: {0}/{1}", userProfile.LoginId, userProfile.DisplayName));

            return userProfile;
        }

        /// <summary>
        /// Gets the profile from the database, or creates it if it does not exist
        /// </summary>
        protected override UserProfile CreateUserProfile(string loginId, bool saveChanges = true)
        {
            UserProfile subjectProfile;
            string defaultDomain = string.Empty;
            UserProfile updatedProfile;

            subjectProfile = domainService.GetUserProfiles("Roles.Role, Groups.Group, CodeSigningTypes.CodeSigningType").Where(up => up.LoginId == loginId).FirstOrDefault();
            using (var activeDirectoryManager = new ActiveDirectoryManager())
            {
                if (subjectProfile == null)
                {
                    // create the user profile from the directory
                    subjectProfile = new UserProfile() { LoginId = loginId, DisplayName = loginId, ActiveDirectorySId = loginId };

                    if (activeDirectoryManager != null)
                    {
                        updatedProfile = activeDirectoryManager.GetUserPrincipalData(subjectProfile, loginId);
                        if (updatedProfile != null)
                        {
                            subjectProfile = updatedProfile;
                        }
                    }

                    domainService.InsertUserProfile(subjectProfile);
                    if (saveChanges)
                    {
                        domainService.SaveChanges();
                        // make sure we get the ID back from the DB after the entry is created
                        subjectProfile = domainService.GetUserProfiles(null).Where(up => up.LoginId == loginId).FirstOrDefault();
                    }

                }
                else if (subjectProfile.IsDeleted)
                {
                    // if the user is marked deleted, then undelete it :)
                    subjectProfile.IsDeleted = false;
                    domainService.UpdateUserProfile(subjectProfile);
                    if (saveChanges)
                    {
                        domainService.SaveChanges();
                    }
                }
                else
                {
                    updatedProfile = null;
                    if (activeDirectoryManager != null)
                    {
                        updatedProfile = activeDirectoryManager.GetUserPrincipalData(subjectProfile, loginId);
                        if (updatedProfile != null)
                        {
                            subjectProfile = updatedProfile;
                        }
                    }
                    if (updatedProfile != null)
                    {
                        bool bProfileUpdated = false;

                        if (subjectProfile.FirstName != updatedProfile.FirstName)
                        {
                            bProfileUpdated = true;
                            subjectProfile.FirstName = updatedProfile.FirstName;
                        }

                        if (subjectProfile.LastName != updatedProfile.LastName)
                        {
                            bProfileUpdated = true;
                            subjectProfile.LastName = updatedProfile.LastName;
                        }
                        if (subjectProfile.DisplayName != updatedProfile.DisplayName)
                        {
                            bProfileUpdated = true;
                            subjectProfile.DisplayName = updatedProfile.DisplayName;
                        }
                        if (subjectProfile.Email != updatedProfile.Email)
                        {
                            bProfileUpdated = true;
                            subjectProfile.Email = updatedProfile.Email;
                        }
                        if (bProfileUpdated)
                        {
                            log.Info(String.Format("Updating user profile from Active Directory. ({0}\\{1})", defaultDomain, loginId));
                            domainService.UpdateUserProfile(subjectProfile);
                            if (saveChanges)
                            {
                                domainService.SaveChanges();
                            }
                        }
                    }

                }
            }
            ActivityLog.Log(loginId, "", "PortalUserManager", "Created User Profile", string.Format("Creating user profile for user - Login: {0}", loginId));

            return subjectProfile;
        }
        #endregion

        // ********************************************************************************
        #region Update Groups and Roles Methods
        // ********************************************************************************
        /// <summary>
        /// Update the list of groups the user belongs to from Active Directory
        /// 
        /// NOTE: It does NOT update the database - it JUST updates the User Profile.  
        ///         Need to call InsertUserProfile or UpdateUserProfile after this method is called to persist the changes
        /// </summary>
        private void UpdateUserProfileGroups(UserProfile userProfile, UserPrincipal userPrincipal)
        {
            Guard.ArgumentNotNull(userProfile, "userProfile");
            Guard.ArgumentNotNull(userPrincipal, "userPrincipal");

            log.DebugFormat("Updating User Profile for: {0}/{1}", userProfile.DisplayName, userProfile.Id);

            // all we need is the SID values for the groups that this user is a member of
            List<GroupPrincipal> groupPrincipals = ActiveDirectoryManager.GetGroupPrincipalsForUserPrincipal(userPrincipal);
            List<string> adGroupSids = groupPrincipals.Select(gp => gp.Sid.Value).ToList();
            foreach (GroupPrincipal gp in groupPrincipals)
            {
                gp.Dispose();
            }

            // Remove any groups for the user that they no longer have and any that we no longer care about
            foreach (UserProfileGroup upg in userProfile.Groups.ToList())
            {
                // See if they still have rights to this group
                if (!adGroupSids.Contains(upg.Group.ActiveDirectorySId))
                {
                    //MT - AD Sync Fix
                    log.DebugFormat("Group membership changes, removing user from group: {0}/{1}", upg.Group.Name, upg.GroupId);
                    domainService.DeleteUserProfileGroup(upg);
                }
                // If it is not one of the groups that we care about - then remove it 
                else if (AllGroups.Count(g => g.Name == upg.Group.Name) == 0)
                {
                    //MT - AD Sync Fix
                    log.DebugFormat("Group no longer exists, removing user from group: {0}/{1}", upg.Group.Name, upg.GroupId);
                    domainService.DeleteUserProfileGroup(upg);
                }
            }

            // Add any new groups for the user.
            foreach (string adGroupSid in adGroupSids)
            {
                // Ignore any group that is not one of the groups that we care about
                Group group = AllGroups.Where(g => g.ActiveDirectorySId == adGroupSid).FirstOrDefault();

                if (group == null)
                {
                    continue;
                }

                // The the user doesn't already have this group - then add it
                if (userProfile.Groups.Count(upg => upg.Group.ActiveDirectorySId == adGroupSid) == 0)
                {
                    log.DebugFormat("Adding user to group: {0}/{1}", group.Name, group.Id);
                    userProfile.Groups.Add(new UserProfileGroup { UserProfile = userProfile, Group = group });
                }
            }

            log.DebugFormat("Saving Changes");
            domainService.SaveChanges();
        }

        /// <summary>
        /// Sync the users roles based on the groups that they are in.  Also, updates IsDeleted - based on if they have any roles
        /// </summary>
        private void SyncUserRolesFromGroups(UserProfile userProfile)
        {
            // ************************
            // App Admin
            // ************************

            // Check if the user is in a Group that is an App Admin Group
            if (userProfile.Groups.Count(upg => upg.Group.IsAppAdminGroup) > 0)
            {
                // User SHOULD be an app admin - make sure that they have the role
                if (userProfile.Roles.Count(r => r.Role.Name == Constants.Roles.AppAdminRole) == 0)
                {
                    Role role = AllRoles.Where(r => r.Name == Constants.Roles.AppAdminRole).FirstOrDefault();
                    if (role != null)
                    {
                        userProfile.Roles.Add(new UserProfileRole { UserProfile = userProfile, Role = role });
                    }
                }
            }
            else
            {
                //MT - AD Sync Fix
                // User SHOULD NOT be an app admin - make sure that they do not have the role
                UserProfileRole userProfileRole = userProfile.Roles.Where(upr => upr.Role.Name == Constants.Roles.AppAdminRole
                                                                              && upr.UserProfileId == userProfile.Id)
                                                                    .FirstOrDefault();

                if (userProfileRole != null)
                {
                    domainService.DeleteUserProfileRole(userProfileRole);
                }
            }

            // ************************
            // IsDeleted
            // ************************
            if (userProfile.Groups.Count() == 0)
            {
                userProfile.IsDeleted = true;
            }
            else
            {
                userProfile.IsDeleted = false;
            }

            domainService.SaveChanges();
        }
        #endregion


        // ********************************************************************************
        #region Methods used by AppServerInitializer
        // ********************************************************************************

        /// <summary>
        /// Attempts to add Admin groups from the config file to the Groups table.
        /// Fails if the group name doesn't include the domain (ex. domainName\GroupName) or if the Group is not in Active Directory.
        /// If the group is already in the DB - but is deleted - it reactivates it.
        /// </summary>
        public override void SyncAdminGroupsFromAppConfig()
        {
            try
            {
                List<string> adminGroupsFromConfig = GetAdminGroupsFromConfig();

                List<Group> groups = domainService.GetGroups().ToList(); // NOTE: Get Deleted groups too

                foreach (string domainAndGroupName in adminGroupsFromConfig)
                {
                    string[] domainAndGroupNameParts = domainAndGroupName.Split(new char[] { '/', '\\' });

                    //MT - The groups aren't always in domain/group format.  They can also be in
                    //     domain/ou1/ou2/ouX/group format also

                    if (domainAndGroupNameParts.Count() < 2)
                    {
                        ActivityLog.LogException(null, null, ActivityLog.LogEventId_LOG_ENTRY, "CCSSUserManager", "SyncAdminGroupsFromAppConfig", string.Format(@"Invalid Admin Group Name: {0} in the Web/App Config file.  Must contain Domain\GroupName.", domainAndGroupName));
                        continue;
                    }

                    int lastIndex = domainAndGroupName.LastIndexOf(@"\");
                    if (lastIndex < 0)
                    {
                        lastIndex = domainAndGroupName.LastIndexOf(@"/");
                        if (lastIndex < 0)
                        {
                            ActivityLog.LogException(null, null, ActivityLog.LogEventId_LOG_ENTRY, "CCSSUserManager", "SyncAdminGroupsFromAppConfig", string.Format(@"Invalid Admin Group Name: {0} in the Web/App Config file.  Must contain Domain\GroupName.", domainAndGroupName));
                        }
                    }

                    string domain = domainAndGroupName.Substring(0, lastIndex);
                    string groupName = domainAndGroupNameParts[domainAndGroupNameParts.Length - 1];


                    // If it is in the DB - but is deleted then re-activate it
                    Group deletedGroup = groups.FirstOrDefault(g => g.Domain.ToUpper() == domain.ToUpper() && g.Name.ToUpper() == groupName.ToUpper() && g.IsDeleted == true);
                    if (deletedGroup != null)
                    {
                        deletedGroup.IsDeleted = false;
                        deletedGroup.IsAppAdminGroup = true;

                        domainService.UpdateGroup(deletedGroup);
                        domainService.SaveChanges();

                        return;
                    }

                    Group group = groups.FirstOrDefault(g => g.Domain.ToUpper() == domain.ToUpper() && g.Name.ToUpper() == groupName.ToUpper());

                    using (ActiveDirectoryManager activeDirectoryManager = new ActiveDirectoryManager())
                    {
                        // If the group is NOT in the DB then create it
                        if (group == null)
                        {
                            using (GroupPrincipal groupPrincipal = activeDirectoryManager.GetGroupPrincipal(domain, groupName, IdentityType.SamAccountName))
                            {

                                if (groupPrincipal == null)
                                {
                                    ActivityLog.LogException(null, null, ActivityLog.LogEventId_LOG_ENTRY, "CCSSUserManager", "SyncAdminGroupsFromAppConfig", string.Format(@"Invalid Admin Group Name: {0} in the Web/App Config file.  Not in Active Directory.", domainAndGroupName));
                                    continue;
                                }

                                group = new Group
                                {
                                    Name = groupPrincipal.SamAccountName,
                                    Domain = domain,
                                    ActiveDirectorySId = groupPrincipal.Sid.Value,
                                    Description = groupPrincipal.Description,
                                    IsAppAdminGroup = true
                                };

                                domainService.InsertGroup(group);
                                domainService.SaveChanges();
                            }
                        }

                        AddUsersForGroup(activeDirectoryManager, group, true);

                    }
                }
            }
            catch (Exception e)
            {
                log.Error("An error occurred while syncing the Admin groups from the config.", e);
                throw e;
            }
        }

        private void AddUsersForGroup(ActiveDirectoryManager activeDirectoryManager, Group group, bool updateGroupsAndRoles = false)
        {
            // *******************************************
            // Create a list of all users in all groups
            // *******************************************
            Dictionary<DomainAndLoginId, UserPrincipal> allUserPrincipalsInGroup = new Dictionary<DomainAndLoginId, UserPrincipal>();

            GetUsersForGroup(activeDirectoryManager, allUserPrincipalsInGroup, group);

            domainService.SaveChanges();

            // *******************************************
            // Make sure all of those users are in the system
            // *******************************************
            int count = AddUsers(allUserPrincipalsInGroup, updateGroupsAndRoles);

            foreach (KeyValuePair<DomainAndLoginId, UserPrincipal> kpv in allUserPrincipalsInGroup)
            {
                kpv.Value.Dispose();
            }

            domainService.SaveChanges();

            ActivityLog.LogExecutiveActivity("CCSSUserManager", ActivityLog.LogEventId_LOG_ENTRY, "Added new users from AD Groups", text1: string.Format("Added new users from AD Groups.  Group: {0}, Count: {1}", group.Name, count));
        }

        private static List<string> GetAdminGroupsFromConfig()
        {
            List<string> adminGroupsFromConfig = new List<string>();
            string temp = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.AdminADGroups];

            if (!String.IsNullOrWhiteSpace(temp))
            {
                string[] adGroups = temp.Split(',');

                foreach (string group in adGroups)
                {
                    adminGroupsFromConfig.Add(group.Trim());
                }
            }
            return adminGroupsFromConfig;
        }

       
        private void AddAdministratorRole(UserProfile adminUser)
        {
            // no need to add this if the user is already in the database as administrator
            if (!adminUser.IsAppAdmin)
            {
                // user is an administrator, add this to the user_roles table
                var adminRole = domainService.GetRoles().Where(r => r.Name == Constants.Roles.AppAdminRole).FirstOrDefault();
                Guard.ValueNotNull(adminRole, "System is not configured with an Administrator role.");

                // let's not add this if the ID is not filled in, keeping ID 0 reserved TODO in database or initialization
                if (adminUser.Id != 0)
                {
                    UserProfileRole userAdminRole = new UserProfileRole() { Role = adminRole, UserProfileId = adminUser.Id };
                    domainService.InsertUserProfileRole(userAdminRole);
                    ActivityLog.LogExecutiveActivity("PortalUserManager", ActivityLog.LogEventId_LOG_ENTRY,
                                                     "Added new administrative user.",
                                                     text1: string.Format("Added administrative user.  LoginId: {0}", 
                                                     adminUser.LoginId));
                }
            }
        }

        private static List<string> GetAdmininstratorsFromConfig()
        {
            List<string> admins = new List<string>();
            string temp = DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.Administrators];

            if (!String.IsNullOrWhiteSpace(temp))
            {
                string[] adGroups = temp.Split(',');

                foreach (string group in adGroups)
                {
                    admins.Add(group.Trim());
                }
            }
            return admins;
        }

        #endregion

        // ********************************************************************************
        #region Methods used by the Executive
        // ********************************************************************************
        /// <summary>
        /// Gets the latest subjects from AD
        /// </summary>
        private List<DomainAndLoginId> GetAllSubjects()
        {
            List<DomainAndLoginId> subjects = new List<DomainAndLoginId>();
            using (ActiveDirectoryManager activeDirectoryManager = new ActiveDirectoryManager())
            {

                // go through each group and get the accounts names for each user
                var activeGroups = domainService.GetGroups().Where(g => g.IsDeleted == false);
                foreach (var group in activeGroups)
                {
                    var groupPrincipal = activeDirectoryManager.GetGroupPrincipal(group.Domain, group.Name);

                    // get each user from the group
                    foreach (Principal p in groupPrincipal.GetMembers(recursive: true))
                    {
                        if (p.GetType() == typeof(UserPrincipal))
                        {
                            String userDomain = p.Context.Name;

                            DomainAndLoginId domainAndLoginId = new DomainAndLoginId(userDomain, p.SamAccountName, p.Sid.Value);

                            subjects.Add(domainAndLoginId);
                        }
                    }
                    groupPrincipal.Dispose();
                }
            }

            // remove any duplicate entries and return
            return subjects.Distinct().ToList();
        }

        /// <summary>
        /// Gets the latest subjects from AD
        /// </summary>
        private void SyncSubjects(List<DomainAndLoginId> subjects)
        {
            List<DomainAndLoginId> profileList;

            // start with a query of the SIDs in the database.
            var userProfilesSids = GetUserProfiles().Select(up => up.ActiveDirectorySId).ToList();

            // get the SIDs from the list of subjects
            var subjectsSids = subjects.Select(s => s.ActiveDirectorySId);
            List<string> deletedSIDs = new List<string>();

            // get all of the profiles in the system now that are not deleted and that are not in the directory.
            var usersToDelete = GetUserProfiles().Where(profile => profile.IsDeleted == false && !subjectsSids.Contains(profile.ActiveDirectorySId)).
                                                         Select(profile => profile.Id).ToList();

            // mark the removed profiles as deleted
            foreach (var removedUser in usersToDelete)
            {
                var userProfile = domainService.GetUserProfiles().Where(up => up.Id == removedUser).FirstOrDefault();
                if (userProfile != null)
                {
                    // mark the profile as deleted.
                    userProfile.IsDeleted = true;
                    domainService.UpdateUserProfile(userProfile);
                    deletedSIDs.Add(userProfile.ActiveDirectorySId);
                }
            }

            // Add the new profiles
            profileList = subjects.Where(s => !userProfilesSids.Contains(s.ActiveDirectorySId)).ToList();
            foreach (var newUser in profileList)
            {
                CreateUserProfile(newUser, false);
            }

            // remove the profiles that we deleted
            subjectsSids = subjectsSids.Except(deletedSIDs.ToList());
            // Look for any profiles that are in the DB, but marked deleted.
            var profilesToUndelete = GetUserProfiles().Where(up => up.IsDeleted && subjectsSids.Contains(up.ActiveDirectorySId)).ToList();
            foreach (var undeletedUser in profilesToUndelete)
            {
                var userProfile = domainService.GetUserProfiles().Where(up => up.Id == undeletedUser.Id).FirstOrDefault();
                if (userProfile != null)
                {
                    // mark the profile as deleted.
                    userProfile.IsDeleted = false;
                    domainService.UpdateUserProfile(userProfile);
                }
            }
        }

        /// <summary>
        /// This is the "Master" method called by the executive - to do all sync operations with Active Directory
        /// </summary>
        public override void SyncAllWithActiveDirectory()
        {
            // invalidate the cache entity information since there are other processes updating these
            // tables.
            domainService.InvalidateCache<UserProfileCodeSigningType>();
            domainService.InvalidateCache<UserProfile>();
            domainService.InvalidateCache<UserProfileGroup>();
            domainService.InvalidateCache<UserProfileRole>();
            domainService.InvalidateCache<CodeSigningTypeGroup>();
            domainService.InvalidateCache<Group>();


            // Get all of the subjects based on the groups in AD
            var allActiveSubjects = GetAllSubjects();
            // sync the user profile table with the directory
            SyncSubjects(allActiveSubjects);
            // save these changes to the database
            domainService.SaveChanges();
            
            // MJD - and this one updates the group memberships for all users (include new ones).
            UpdateUserProfilesFromActiveDirectory();
            domainService.SaveChanges();


            // finish with adding the admin groups from the config file
            SyncAdminGroupsFromAppConfig();
            domainService.SaveChanges();
        }


        /// <summary>
        /// Updates All User Profiles in the system - from Active Directory
        /// </summary>
        private void UpdateUserProfilesFromActiveDirectory()
        {
            List<UserProfile> userProfiles = GetUserProfiles("Roles.Role, Groups.Group").ToList();

            int count = 0;

            using (ActiveDirectoryManager activeDirectoryManager = new ActiveDirectoryManager())
            {
                foreach (UserProfile userProfile in userProfiles)
                {
                    using (UserPrincipal userPrincipal = activeDirectoryManager.GetUserPrincipal(userProfile.Domain, userProfile.ActiveDirectorySId, IdentityType.Sid))
                    {
                        // delete the user profile if the AD principal is null
                        if (userPrincipal == null)
                        {
                            //
                            // MJD - only delete the user if they are not deleted.
                            if (!userProfile.IsDeleted)
                            {
                                domainService.SaveChanges();
                                log.InfoFormat("Deleting user: {0}/{1}", userProfile.DisplayName, userProfile.Id);

                                //
                                // MJD - Updated to fix AD sync issue
                                UserProfileGroup[] upgToDelete = new UserProfileGroup[userProfile.Groups.Count];
                                userProfile.Groups.CopyTo(upgToDelete, 0);
                                foreach (UserProfileGroup upg in upgToDelete)
                                {
                                    domainService.DeleteUserProfileGroup(upg);
                                }

                                //
                                // MJD - Updated to fix AD sync issue
                                UserProfileRole[] uprToDelete = new UserProfileRole[userProfile.Roles.Count];
                                userProfile.Roles.CopyTo(uprToDelete, 0);
                                foreach (UserProfileRole upr in uprToDelete)
                                {
                                    domainService.DeleteUserProfileRole(upr);
                                }

                                // If the userPrincipal is null (ie. if the user is no longer in AD?  Remove their rights and mark them as deleted.
                                userProfile.IsDeleted = true;
                                domainService.SaveChanges();
                            }
                        }
                        else
                        {
                            UpdateUserProfileFromUserPrincipal(userProfile, userPrincipal);
                            UpdateUserProfileGroups(userProfile, userPrincipal);
                            SyncUserRolesFromGroups(userProfile);
                        }
                        count++;
                    }
                }
            }

            domainService.SaveChanges();

            ActivityLog.LogExecutiveActivity("CCSSUserManager", ActivityLog.LogEventId_LOG_ENTRY, "Updated all Users from Active Directory", 
                                             text1: string.Format("Updated all Users from Active Directory.  Count: {0}", count));

        }

        /// <summary>
        /// NOTE: WE just need to see if there are any users that we don't already have.  
        ///     We aren't concerned with which groups they are in.
        ///     The UpdateUserProfileFromActiveDirectory logic will do that.
        /// </summary>
        private void AddUsersFromActiveDirectory()
        {
            using (ActiveDirectoryManager activeDirectoryManager = new ActiveDirectoryManager())
            {
                // *******************************************
                // Get each registered group from the database
                // *******************************************
                List<Group> groups = domainService.GetGroups().Where(g => g.IsDeleted == false).ToList();

                foreach (Group group in groups)
                {
                    // add the users for each of these groups
                    AddUsersForGroup(activeDirectoryManager, group);
                }

                domainService.SaveChanges();
            }
        }

        private int AddUsers(Dictionary<DomainAndLoginId, UserPrincipal> usersToAdd, bool updateGroupsAndRoles = false)
        {
            int count = 0;
            List<UserProfile> allUserProfiles = GetUserProfiles("Roles.Role, Groups.Group").ToList();

            foreach (DomainAndLoginId domainAndLoginId in usersToAdd.Keys)
            {
                UserPrincipal userPrincipal = usersToAdd[domainAndLoginId];

                // See if they are already in the system - if not, then add them
                UserProfile userProfile = allUserProfiles.Where(up => up.ActiveDirectorySId == userPrincipal.Sid.Value).FirstOrDefault();
                if (userProfile == null)
                {
                    CreateUserProfile(domainAndLoginId, userPrincipal, updateGroupsAndRoles);
                    count++;
                }
                else
                {
                    //The user already exists but may not have the right roles
                    if (updateGroupsAndRoles)
                    {
                        UpdateUserProfileGroups(userProfile, userPrincipal);
                        SyncUserRolesFromGroups(userProfile);
                    }
                }

                domainService.SaveChanges();

            }
            return count;
        }


        //
        // Get the active directory users that are members of this group
        //
        private void GetUsersForGroup(ActiveDirectoryManager activeDirectoryManager, Dictionary<DomainAndLoginId, UserPrincipal> users, Group group)
        {
            GroupPrincipal groupPrincipal = activeDirectoryManager.GetGroupPrincipal(group.Domain, group.ActiveDirectorySId, IdentityType.Sid);
            if (groupPrincipal == null)
            {
                // This group no longer exists in AD, delete the user profile group entries and mark the group deleted.
                group.IsDeleted = true;

                foreach (UserProfileGroup upg in domainService.GetUserProfileGroups().Where(upg => upg.GroupId == group.Id).ToList())
                {
                    log.InfoFormat("Delete UserProfileGroup {0} - {1}", upg.UserProfileId, upg.GroupId);
                    domainService.DeleteUserProfileGroup(upg);
                }

                // Go on to the next group - we're done with this one.
                return;
            }

            group.Name = groupPrincipal.SamAccountName;
            group.Description = groupPrincipal.Description;

            // get each user from the group
            foreach (Principal p in groupPrincipal.GetMembers(recursive: true))
            {
                if (p.GetType() == typeof(UserPrincipal))
                {
                    String userDomain = p.Context.Name;

                    DomainAndLoginId domainAndLoginId = new DomainAndLoginId(userDomain, p.SamAccountName, p.Sid.Value);

                    // if the user is no already in the database, then add them
                    if (!users.ContainsKey(domainAndLoginId))
                    {

                        log.InfoFormat("Add User {0} - {1}", domainAndLoginId, p.Name);

                        users.Add(domainAndLoginId, (UserPrincipal)p);
                    }
                }
                //       p.Dispose();
            }

            groupPrincipal.Dispose();
        }

        //*******************//



        #endregion
        // ********************************************************************************
        #region Support Methods
        // ********************************************************************************
        private static void UpdateUserProfileFromUserPrincipal(UserProfile userProfile, UserPrincipal userPrincipal)
        {
            userProfile.FirstName = userPrincipal.GivenName;
            userProfile.LastName = userPrincipal.Surname;
            userProfile.Email = userPrincipal.EmailAddress;
            userProfile.DisplayName = userPrincipal.DisplayName;
            userProfile.ActiveDirectorySId = userPrincipal.Sid.Value;
            PropertyCollection properties = ((DirectoryEntry)userPrincipal.GetUnderlyingObject()).Properties;
            foreach (object property in properties["proxyaddresses"])
            {
                userProfile.ProxyEmail = property.ToString();
            }
        }

        #endregion

    }
}
