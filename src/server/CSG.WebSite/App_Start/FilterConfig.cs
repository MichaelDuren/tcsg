using System.Web.Mvc;

namespace CSG.WebSite
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            //This will require authorization by default.  In order to not require authorization
            //apply the [AllowAnonymous] attribute to the controller methods
            filters.Add(new System.Web.Mvc.AuthorizeAttribute());
        }
    }
}