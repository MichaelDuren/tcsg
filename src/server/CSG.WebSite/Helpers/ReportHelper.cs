﻿using CSG.AppServer.Reporting;
using Util.Reports;
using System;
using System.Web;

namespace CSG.WebSite.Helpers
{
    public static class ReportHelper
    {
        ///// <summary>
        ///// Convenience method to handle displaying a report.
        ///// </summary>
        //public static ReportRunStatus DisplayReport(ESPAdHocReport report, HttpResponseBase response, string selectedSaveType)
        //{
        //    ReportSaveType saveType = GetReportSaveType(selectedSaveType);

        //    ReportRunStatus runStatus = report.InitializeAndRun();

        //    // Handle "saving" (ie. streaming them to the user's browser) Non-Html files
        //    if (runStatus.RanSuccessfully && saveType != ReportSaveType.Html)
        //    {
        //        report.Save(response, reportSaveType: saveType);
        //        runStatus.ReportCompletionState = ReportCompletionState.SuccessSaved;
        //        return runStatus;
        //    }
        //    else
        //    {
        //        return runStatus;
        //    }
        //}

        public static ReportSaveType GetReportSaveType(string selectedSaveType)
        {
            ReportSaveType saveType = ReportSaveType.Html;
            switch (selectedSaveType.ToUpper())
            {
                case "HTML":
                    saveType = ReportSaveType.Html;
                    break;

                case "WORD":
                    saveType = ReportSaveType.Word;
                    break;

                case "EXCEL":
                    saveType = ReportSaveType.Excel;
                    break;

                case "CSV":
                    saveType = ReportSaveType.Csv;
                    break;

                default:
                    throw new ArgumentOutOfRangeException("Unsupported ReportSaveType: " + saveType);
            }

            return saveType;
        }

    }
}