﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CSG.AppServer;
using CSG.AppServer.Db;
using CSG.AppServer.Entities;
using CSG.AppServer.Exceptions;
using CSG.AppServer.Reporting;
using CSG.WebSite.Controllers;
using Util.Sql.Query;

namespace CSG.WebSite.Helpers
{
    public class UIHelper
    {
        /// <summary>
        /// This method will allow the user to automatically add a '<Select>' entry 
        /// to the list.
        /// </summary>
        public static SelectList GetSelectList(SelectList startingList, bool addDefaultItem = true, object selectedValue = null)
        {
            if (addDefaultItem)
            {
                //Only add the default item if the list is longer than one
                if (startingList.Count() > 1)
                {
                    List<SelectListItem> temp = startingList.ToList<SelectListItem>();
                    SelectListItem newItem = new SelectListItem();
                    newItem.Value = null;
                    newItem.Text = "<SELECT>";
                    temp.Insert(0, newItem);
                    return new SelectList(temp, "Value", "Text", selectedValue);
                }
                else
                {
                    return startingList;
                }
            }
            else
            {
                return startingList;
            }
        }

        public static SelectList GetSelectListForDataTypes(int selectedValue)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            AddValueToList(items, "<SELECT>", ((int)(SystemSettingDataType.Undefined)).ToString(), selectedValue.ToString());
            AddValueToList(items, SystemSettingDataType.String.ToString(), ((int)(SystemSettingDataType.String)).ToString(), selectedValue.ToString());
            AddValueToList(items, SystemSettingDataType.Integer.ToString(), ((int)(SystemSettingDataType.Integer)).ToString(), selectedValue.ToString());
            AddValueToList(items, SystemSettingDataType.Double.ToString(), ((int)(SystemSettingDataType.Double)).ToString(), selectedValue.ToString());
            AddValueToList(items, SystemSettingDataType.DateTime.ToString(), ((int)(SystemSettingDataType.DateTime)).ToString(), selectedValue.ToString());
            AddValueToList(items, SystemSettingDataType.Boolean.ToString(), ((int)(SystemSettingDataType.Boolean)).ToString(), selectedValue.ToString());

            return new SelectList(items, "Value", "Text", selectedValue.ToString());
        }

        public static SelectList GetSelectListForUsageReportTypes(int selectedValue)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            AddValueToList(items, UsageReportType.CodeSigningType.ToString(), ((int)(UsageReportType.CodeSigningType)).ToString(), selectedValue.ToString());
            AddValueToList(items, UsageReportType.User.ToString(), ((int)(UsageReportType.User)).ToString(), selectedValue.ToString());

            return new SelectList(items, "Value", "Text", selectedValue.ToString());
        }

        public static List<string> GetLogEntryLevels()
        {
            List<string> items = new List<string>();

            items.Add("<ALL>");

            QueryDef qd = new QueryDef("SELECT DISTINCT LogEntryLevel FROM App_ActivityLog");
            List<string> itemsFromDb = QueryEngine.GetStringList(DbConnectionFactory.CreateOpenConnection(), qd, "LogEntryLevel");

            items.AddRange(itemsFromDb);

            return items;
        }

        public static SelectList GetSelectListForRoles(string role)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            AddValueToList(items, "<None>", "0", role);
            AddValueToList(items, "Submitted By", "1", role);
            AddValueToList(items, "Approved By", "2", role);
// The database would need to be updated to track denied by user
// in order to support this selection
//            AddValueToList(items, "Denied By", "3", role);

            return new SelectList(items, "Value", "Text", role);
        }

        public static SelectList GetSelectListForStatuses(string status)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            AddValueToList(items, "<ANY>", "-1", status);

//            AddValueToList(items, CodeSigningRequestStatus.Unconfirmed.ToString(), ((int)CodeSigningRequestStatus.Unconfirmed).ToString(), status);
            AddValueToList(items, CodeSigningRequestStatus.Submitted.ToString(), ((int)CodeSigningRequestStatus.Submitted).ToString(), status);
            AddValueToList(items, CodeSigningRequestStatus.Cancelled.ToString(), ((int)CodeSigningRequestStatus.Cancelled).ToString(), status);
            AddValueToList(items, CodeSigningRequestStatus.Denied.ToString(), ((int)CodeSigningRequestStatus.Denied).ToString(), status);
            AddValueToList(items, CodeSigningRequestStatus.TimedOut.ToString(), ((int)CodeSigningRequestStatus.TimedOut).ToString(), status);
            AddValueToList(items, CodeSigningRequestStatus.Approved.ToString(), ((int)CodeSigningRequestStatus.Approved).ToString(), status);
            AddValueToList(items, CodeSigningRequestStatus.PartiallyApproved.ToString(), ((int)CodeSigningRequestStatus.PartiallyApproved).ToString(), status);
            AddValueToList(items, CodeSigningRequestStatus.InProcess.ToString(), ((int)CodeSigningRequestStatus.InProcess).ToString(), status);
            AddValueToList(items, CodeSigningRequestStatus.CompletedFailed.ToString(), ((int)CodeSigningRequestStatus.CompletedFailed).ToString(), status);
            AddValueToList(items, CodeSigningRequestStatus.CompletedSuccess.ToString(), ((int)CodeSigningRequestStatus.CompletedSuccess).ToString(), status);

            return new SelectList(items, "Value", "Text", status);
        }
        public static SelectList GetSelectListForLogEntryLevels(string selectedValue)
        {
            if (selectedValue == null)
            {
                selectedValue = String.Empty;
            }

            List<SelectListItem> items = new List<SelectListItem>();

            AddValueToList(items, "<ALL>", "", selectedValue);
            AddValueToList(items, "INFO", "", selectedValue);
            AddValueToList(items, "ERROR", "", selectedValue);
            AddValueToList(items, "DEBUG", "", selectedValue);

            return new SelectList(items, "Value", "Text", selectedValue.ToString());
        }

        public static List<SelectListItem> GetSelectListForGroups(string selectedGroup, UserProfile currentUser)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            AddValueToList(items, "<ANY>", "0", selectedGroup);

            IEnumerable<Group> groups = Utilities.GetGroups(currentUser);

            foreach (Group group in groups)
            {
                AddValueToList(items, group.Name, group.Id.ToString(), selectedGroup);
            }

            return items;
        }

        public static List<SelectListItem> GetSelectListForCodeSigningTypes(string csType, UserProfile currentUser)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            AddValueToList(items, "<ANY>", "0", csType);

            IEnumerable<CodeSigningType> csTypes = Utilities.GetCodeSigningTypes(currentUser).ToList();

            foreach (CodeSigningType type in csTypes)
            {
                AddValueToList(items, type.Name, type.Id.ToString(), csType);
            }

            return items;
        }


        public static List<SelectListItem> GetSelectListForCodeSigningTools(string selectedTool, UserProfile currentUser)
        {
            List<SelectListItem> items = new List<SelectListItem>();

            IEnumerable<CodeSigningTool> csTools = Utilities.GetCodeSigningTools(currentUser);

            AddValueToList(items, "Signtool test", "1", selectedTool);

            foreach (CodeSigningTool tool in csTools)
            {
                AddValueToList(items, tool.Name, tool.Id.ToString(), selectedTool);
            }

            return items;
        }

        private static void AddValueToList(List<SelectListItem> items, string text, string value, string selectedValue)
        {
            SelectListItem item = new SelectListItem();
            item.Text = text;
            item.Value = value;

            if (selectedValue != null)
            {
                if (item.Value == selectedValue.ToString())
                {
                    item.Selected = true;
                }
            }

            items.Add(item);
        }

        public static ActionResult RedirectToError(Exception e)
        {
            if (e is LoginException)
            {
                LoginException exception = (LoginException)e;
                if (exception.LoginExceptionSubType == LoginExceptionSubType.UserIsNotLoggedIn)
                {
                    //For user not logged in exceptions, redirect to the log in screen
                    AccountController account = new AccountController();
                    return account.Login(string.Empty);
                }
            }

            SharedController shared = new SharedController();
            return shared.Error(e);
        }

        public static bool ConvertCheckBoxValueToBoolean(string value)
        {
            //http://stackoverflow.com/questions/5936048/why-html-checkboxvisible-returns-true-false-in-asp-net-mvc-2
            //MVC returns a hidden field with same name, so when the box is checked, the value on the form is 'true,false'.  When it's not checked, it's just 'false'.
            if (value.ToUpper().Contains("TRUE"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}