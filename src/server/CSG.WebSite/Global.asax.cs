using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using CSG.AppServer;
using System;
using System.Web;
using System.Security.Principal;
using System.Web.Security;
using CSG.AppServer.Security;
using CSG.AppServer.Entities;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using System.Web.Helpers;

namespace CSG.WebSite
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {

        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        protected void Application_Start()
        {

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AuthConfig.RegisterAuth();
            //AppServer Initialization
            AppServerInitializer.InitializeLoggingEnvironmentVariables();

            log.Info("--> Running Application_Start");

            AppServerInitializer.Initialize();

// removed for 0.6.1           AntiForgeryConfig.SuppressXFrameOptionsHeader = true;
        }
        protected void Application_BeginRequest()
        {
            Response.AddHeader("X-Frame-Options", "DENY");
            Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
            Response.Cache.SetNoStore();
        }
// auth functions used for single signon 
#if SINGLE_SIGNON
        protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        {
            // Is the user authenticated?
            if (!HttpContext.Current.Request.IsAuthenticated)
            {
                log.Debug("User is not authenticated");
                string userId = HttpContext.Current.Request.ServerVariables[AppServerContext.SSOHeaderUID];
                if (userId != null)
                {
                    GenericIdentity webIdentity = new GenericIdentity(userId);
                    GenericPrincipal principal = new GenericPrincipal(webIdentity, null);
                    HttpContext.Current.User = principal;
                    FormsAuthentication.SetAuthCookie(userId, true);
                    log.Info("Authentication cookie set for: " + userId);
                }
            }
        }

        protected void Application_Error(Object sender, EventArgs e)
        {
            Exception exception = Server.GetLastError();        
            System.Diagnostics.Debug.WriteLine(exception);
            try
            {
                log.Error("Applicaiton Error Handler", exception);
            }
            catch (Exception)
            {
                // not much we can do here.
            }
            Response.Redirect("/Home/Error");
        }

        protected void Application_PreRequestHandlerExecute(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie == null)
            {
                // This case may be a candidate for removal since SSO prepares this in advance.
                string userId = null;
                if (!HttpContext.Current.Request.IsAuthenticated)
                {
                    userId = HttpContext.Current.Request.ServerVariables[AppServerContext.SSOHeaderUID];

                    if (userId != null)
                    {
                        GenericIdentity webIdentity = new GenericIdentity(userId);
                        GenericPrincipal principal = new GenericPrincipal(webIdentity, null);
                        HttpContext.Current.User = principal;
                    }
                }
                else
                {
                    userId = HttpContext.Current.User.Identity.Name;
                }

                if (userId != null)
                {
                    String firstName = HttpContext.Current.Request.ServerVariables[AppServerContext.SSOHeaderFirstName];
                    String lastName = HttpContext.Current.Request.ServerVariables[AppServerContext.SSOHeaderLastName];
                    String emailAddress = HttpContext.Current.Request.ServerVariables[AppServerContext.SSOHeaderEMail];

                    UserProfile userProfileData = new UserProfile()
                    {
                        LoginId = HttpContext.Current.User.Identity.Name,
                        FirstName = firstName,
                        LastName = lastName,
                        Email = emailAddress
                    };
                    // User is authenticated, but there is no authentication cookie yet, so the user must login
                    UserManagerBase userManager = UserManagerFactory.CreateUserManager();

                    if (userManager.Login(userProfileData) == false)
                    {
                        log.Error("Login failed in Application_PreRequestHandlerExecute.");
                        HttpContext.Current.User = null;
                    }
                    else
                    {
                        log.Info("User Login: " + userProfileData.DisplayName);
                    }
                }
            }
            else
            {
                // determine if we have a user profile yet
                if (HttpContext.Current.Session != null)
                {

                    var up = HttpContext.Current.Session[Constants.DomainAndLoginId];
                    if (up == null)
                    {
                        string userId = HttpContext.Current.Request.ServerVariables[AppServerContext.SSOHeaderUID];
                        string firstName = HttpContext.Current.Request.ServerVariables[AppServerContext.SSOHeaderFirstName];
                        string lastName = HttpContext.Current.Request.ServerVariables[AppServerContext.SSOHeaderLastName];
                        string emailAddress = HttpContext.Current.Request.ServerVariables[AppServerContext.SSOHeaderEMail];

                        UserProfile userProfileData = new UserProfile()
                        {
                            LoginId = userId,
                            FirstName = firstName,
                            LastName = lastName,
                            Email = emailAddress
                        };

                        // User is authenticated, but there is no authentication cookie yet, so the user must login
                        UserManagerBase userManager = UserManagerFactory.CreateUserManager();

                        if (userManager.Login(userProfileData) == false)
                        {
                            log.Debug("Login failed for " + userProfileData.LoginId);
                            HttpContext.Current.User = null;
                        }
                    }
                }
            }
        }
#endif
    }
}