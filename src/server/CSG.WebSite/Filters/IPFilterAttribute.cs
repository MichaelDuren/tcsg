﻿using CSG.AppServer;
using CSG.AppServer.Managers;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace CSG.WebSite.Filters
{

    public class IPFilterAttribute : AuthorizationFilterAttribute
    {
        // ******************************************************************************
        #region Properties / Attributes
        // ******************************************************************************
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        #endregion

        private string GetClientIpAddress(HttpRequestMessage request)
        {
            if (request.Properties.ContainsKey("MS_HttpContext"))
            {
                return IPAddress.Parse(((HttpContextBase)request.Properties["MS_HttpContext"]).Request.UserHostAddress).ToString();
            }
            if (request.Properties.ContainsKey("MS_OwinContext"))
            {
                return IPAddress.Parse(((OwinContext)request.Properties["MS_OwinContext"]).Request.RemoteIpAddress).ToString();
            }
            return null;
        }
        public override Task OnAuthorizationAsync(HttpActionContext actionContext, System.Threading.CancellationToken cancellationToken)
        {
            bool bAuthorized = false;
            SystemSettingsManager ssm = SystemSettingsManager.Current;

            var ipFilter = ssm.GetBySettingName(Constants.SettingNames.IPAddressRestrictions).StringValue;
            if (!string.IsNullOrEmpty(ipFilter))
            {
                var ip = GetClientIpAddress(actionContext.Request);
                var validIPs = ipFilter.Split(';').ToList();
                if (validIPs.Contains(ip))
                {
                    bAuthorized = true;
                }
                else
                {
                    log.Info("Client attempting access from unauthorized IP address: " + ip); 
                }
            }
            else
            {
                // if there is no filter, then disable filter.
                bAuthorized = true;
            }
            if (!bAuthorized)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, "Client is not authorized.");
            }
            return Task.FromResult<object>(null);
        }
    }
}


