using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CCSS.WebSite.Models
{
    public class DashboardModel
    {
        [Display(Name = "Development Team")]
        public SelectList DevelopmentTeams { get; set; }

        [Display(Name = "Code Signing Type")]
        public SelectList CodeSigningTypes { get; set; }
    }
}