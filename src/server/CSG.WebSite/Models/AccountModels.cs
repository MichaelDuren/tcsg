using System.ComponentModel.DataAnnotations;

namespace CSG.WebSite.Models
{
    public class LoginModel
    {
        [Required]
        [Display(Name = "Domain")]
        public string Domain { get; set; }

        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
}
