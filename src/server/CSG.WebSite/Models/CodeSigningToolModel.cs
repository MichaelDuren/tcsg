using CSG.AppServer.Entities;
using System.ComponentModel.DataAnnotations;

namespace CSG.WebSite.Models
{
    public class CodeSigningToolModel
    {
        [Required]
        [MaxLength(125)]
        public string Name { get; set; }

        public bool Offline { get; set; }

        [MaxLength(2000)]
        public string SigningExecutable { get; set; }

        [MaxLength(2000)]
        public string CommonSigningArguments { get; set; }

        public int SigningArgumentFormat { get; set; }

        [MaxLength(2000)]
        public string VerificationExecutable { get; set; }

        [MaxLength(2000)]
        public string CommonVerificationArguments { get; set; }

        [MaxLength(1000)]
        public string TimestampServer { get; set; }

        public void CopyTo(CodeSigningTool entity)
        {
            CommonSigningArguments = entity.CommonSigningArguments;
            CommonVerificationArguments = entity.CommonVerificationArguments;
            Name = entity.Name;
            Offline = entity.Offline;
            SigningArgumentFormat = entity.SigningArgumentFormat;
            SigningExecutable = entity.SigningExecutable;
            TimestampServer = entity.TimestampServer;
            VerificationExecutable = entity.VerificationExecutable;
        }
        public CodeSigningTool CopyFrom()
        {
            CodeSigningTool entity = new CodeSigningTool();

            entity.CommonSigningArguments = CommonSigningArguments;
            entity.CommonVerificationArguments = CommonVerificationArguments;
            entity.Name = Name;
            entity.Offline = Offline;
            entity.SigningArgumentFormat = SigningArgumentFormat;
            entity.SigningExecutable = SigningExecutable;
            entity.TimestampServer = TimestampServer;
            entity.VerificationExecutable = VerificationExecutable;
            return entity;
        }
    }
}
