﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using CSG.AppServer.Entities;
using CSG.AppServer.Security;
using System;

namespace CSG.WebSite.Models
{
    public class TemporalApprovalsModel
    {
        public List<CodeSigningTemporalApproval> temporalApprovals { get; set; }
    }

    // model used for granting approval windows to hash signing profiles
    public class TemporalApprovalModel
    {
        public int Id { get; set; }
        public string displayString { get; set; }
        public string loginId { get; set; }
        public int certificateId { get; set; }
        public string certificateDN { get; set; }
        public bool requireMatchingFilename { get; set; }
    }

}