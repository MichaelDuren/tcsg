using CSG.AppServer.Entities;
using System.ComponentModel.DataAnnotations;

namespace CSG.WebSite.Models
{
    public class CodeSigningTypeModel
    {
        [Required]
        public int Id { get; set; }

        [Required]
        [Display(Name = "Signing Profile")]
        [MaxLength(125)]
        public string Name { get; set; }

        [MaxLength(2000)]
        public string StagingDirectory { get; set; }

        [MaxLength(2000)]
        public string SignedDirectory { get; set; }

        [MaxLength(2000)]
        public string AdditionalArguments { get; set; }

        [Required]
        public bool DoVerification { get; set; }

        [Required]
        public bool RequireSecondaryApproval { get; set; }

        [MaxLength(1000)]
        public string ValidFileExtensions { get; set; }

        [MaxLength(100)]
        public string MetadataField1Label { get; set; }

        [MaxLength(100)]
        public string MetadataField2Label { get; set; }

        [Required]
        [MaxLength(1000)]
        public string GESResourceId { get; set; }

        public bool RequestTimestamp { get; set; }

        public bool ApplyToSelectRoles { get; set; }

        [Required]
        [MaxLength(100)]
        public string Guid { get; set; }

        public bool RawSign { get; set; }

        [Display(Name = "Code Signing Tool")]
        public virtual CodeSigningTool SigningTool { get; set; }

        public int SigningToolId { get; set; }

        [Display(Name = "Code Signing Certificate")]
        public virtual Certificate SigningCertificate { get; set; }

        [Required]
        public int SigningCertificateId { get; set; }
        [Required]
        public bool RequireManagerApproval { get; set; }
        [Required]
        public bool AllowManagerApproval { get; set; }
        [Required]
        public bool RequireApprover1Approval { get; set; }
        [Required]
        public bool AllowApprover1Approval { get; set; }
        [Required]
        public bool RequireApprover2Approval { get; set; }
        [Required]
        public bool AllowApprover2Approval { get; set; }
        [Required]
        public bool IsAutoSign { get; set; }
        [Required]
        public bool IsDeleted { get; set; }

        public void CopyTo(CodeSigningType entity)
        {
            Id = entity.Id;
            Name = entity.Name; 
            StagingDirectory = entity.StagingDirectory;
            SignedDirectory = entity.SignedDirectory;
            AdditionalArguments = entity.AdditionalArguments;
            DoVerification = entity.DoVerification;
            RequireSecondaryApproval = entity.RequireSecondaryApproval;
            ValidFileExtensions = entity.ValidFileExtensions;
            MetadataField1Label = entity.MetadataField1Label;
            MetadataField2Label = entity.MetadataField2Label;
            RequestTimestamp = entity.RequestTimestamp;
            ApplyToSelectRoles = !entity.ApplyToAll;
            Guid = entity.Guid;
            RawSign = entity.RawSign;
            SigningTool = entity.SigningTool;
            SigningToolId = entity.SigningToolId;
            SigningCertificate = entity.SigningCertificate;
            SigningCertificateId = entity.SigningCertificateId;
            RequireManagerApproval = entity.RequireManagerApproval;
            AllowManagerApproval = entity.AllowManagerApproval;
            RequireApprover1Approval = entity.RequireApprover1Approval;
            AllowApprover1Approval = entity.AllowApprover1Approval;
            RequireApprover2Approval = entity.RequireApprover2Approval;
            AllowApprover2Approval = entity.AllowApprover2Approval;
            IsAutoSign = entity.IsAutoSign;
            IsDeleted = entity.IsDeleted;
        }
        public CodeSigningType CopyFrom()
        {
            CodeSigningType entity = new CodeSigningType();
            entity.Id = Id; 
            entity.Name = Name;
            entity.StagingDirectory = StagingDirectory;
            entity.SignedDirectory = SignedDirectory;
            entity.AdditionalArguments = AdditionalArguments;
            entity.DoVerification = DoVerification;
            entity.RequireSecondaryApproval = RequireSecondaryApproval;
            entity.ValidFileExtensions = ValidFileExtensions;
            entity.MetadataField1Label = MetadataField1Label;
            entity.MetadataField2Label = MetadataField2Label;
            entity.RequestTimestamp = RequestTimestamp;
            entity.ApplyToAll = !ApplyToSelectRoles;
            entity.Guid = Guid;
            entity.RawSign = RawSign;
            entity.SigningTool = SigningTool;
            entity.SigningToolId = SigningToolId;
            entity.SigningCertificate = SigningCertificate;
            entity.SigningCertificateId = SigningCertificateId;
            entity.RequireManagerApproval = RequireManagerApproval;
            entity.AllowManagerApproval = AllowManagerApproval;
            entity.RequireApprover1Approval = RequireApprover1Approval;
            entity.AllowApprover1Approval = AllowApprover1Approval;
            entity.RequireApprover2Approval = RequireApprover2Approval;
            entity.AllowApprover2Approval = AllowApprover2Approval;
            entity.IsAutoSign = IsAutoSign;
            entity.IsDeleted = IsDeleted;
            return entity;
        }
    }
}
