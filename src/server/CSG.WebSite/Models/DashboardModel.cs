﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using CSG.AppServer.Entities;
using CSG.AppServer.Security;

namespace CSG.WebSite.Models
{
    public class DashboardModel
    {
        // number of development code signing types the user has access to.
        public int codeSigningTypeCount { get; set; }

        [Required]
        public int CodeSigningTypeId { get; set; }

        // number of development groups that the user belongs to.
        public int groupCount { get; set; }

        [Required]
        public int GroupId { get; set; }

        public List<CodeSigningRequest> PendingRequests { get; set; }

        public List<CodeSigningRequest> RecentlyCompletedRequests { get; set; }

        public Gatekeeper Gatekeeper { get; set; }
    }
}