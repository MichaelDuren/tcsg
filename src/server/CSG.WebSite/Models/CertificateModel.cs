using CSG.AppServer.Entities;
using System;
using System.ComponentModel.DataAnnotations;

namespace CSG.WebSite.Models
{
    public class CertificateModel
    {
        public int Id { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        public string IssuedTo { get; set; }

        public string IssuedBy { get; set; }

        public string DistinguishedName { get; set; }

        public string Thumbprint { get; set; }

        public bool InStore { get; set; }

        public bool InSecurityWorld { get; set; }

        public string MimeEncoded { get; set; }

        public DateTime? NotBefore { get; set; }

        public DateTime? NotAfter { get; set; }

        public bool IsDeleted { get; set; }

        public void CopyTo(Certificate entity)
        {
            Name = entity.Name;
            IssuedTo = entity.IssuedTo;
            IssuedBy = entity.IssuedBy;
            DistinguishedName = entity.DistinguishedName;
            Thumbprint = entity.Thumbprint;
            InStore = entity.InStore;
            InSecurityWorld = entity.InSecurityWorld;
            MimeEncoded = entity.MimeEncoded;
            NotBefore = entity.NotBefore;
            NotAfter = entity.NotAfter;
            IsDeleted = entity.IsDeleted;
        }
        public Certificate CopyFrom()
        {
            Certificate entity = new Certificate();

            entity.Name = Name;
            entity.IssuedTo = IssuedTo;
            entity.IssuedBy = IssuedBy;
            entity.DistinguishedName = DistinguishedName;
            entity.Thumbprint = Thumbprint;
            entity.InStore = InStore;
            entity.InSecurityWorld = InSecurityWorld;
            entity.MimeEncoded = MimeEncoded;
            entity.NotBefore = NotBefore;
            entity.NotAfter = NotAfter;
            entity.IsDeleted = IsDeleted;

            return entity;
        }

    }
}
