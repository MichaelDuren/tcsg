// UNUSED
using CSG.AppServer;
using CSG.AppServer.Entities;
using System;
using System.ComponentModel.DataAnnotations;

namespace CSG.WebSite.Models
{
    public class SystemSettingModel
    {
        public int Id { get; set; }
        [MaxLength(100)]
        public string Name { get; set; }
        [MaxLength(100)]
        public string Category { get; set; }
        [MaxLength(100)]
        public string SubCategory { get; set; }
        [MaxLength(2000)]
        public string Description { get; set; }
        public SystemSettingDataType DataType { get; set; }
        public int DataTypeId
        {
            get { return (int)DataType; }
            set { DataType = (SystemSettingDataType)value; }
        }
        public bool? BooleanValue { get; set; } = false;
        public bool NotNullableBooleanValue { get { return BooleanValue == true; } set { BooleanValue = value; } }
        public DateTime? DateValue { get; set; }
        public int? IntegerValue { get; set; }
        public double? DoubleValue { get; set; }
        [MaxLength(80000)] //nvarchar(max)
        public string StringValue { get; set; }
        [MaxLength(1000)]
        public string PasswordValue { get; set; }
        public bool ExcludeFromSettingsUI { get; set; }
        [MaxLength(100)]
        public string ExtraString1 { get; set; }
        [MaxLength(100)]
        public string ExtraString2 { get; set; }
        [MaxLength(100)]
        public string ExtraString3 { get; set; }
        public int? ExtraInteger1 { get; set; }
        public int? ExtraInteger2 { get; set; }
        public int? ExtraInteger3 { get; set; }
        public string Value
        {
            get
            {
                string val = string.Empty;
                switch (DataTypeId)
                {
                    case (int)SystemSettingDataType.String:
                        val = StringValue;
                        break;
                    case (int)SystemSettingDataType.Integer:
                        val = IntegerValue.ToString();
                        break;
                    case (int)SystemSettingDataType.Double:
                        val = DoubleValue.ToString();
                        break;
                    case (int)SystemSettingDataType.DateTime:
                        val = DateValue.ToString();
                        break;
                    case (int)SystemSettingDataType.Boolean:
                        val = BooleanValue.ToString();
                        break;
                    case (int)SystemSettingDataType.Password:
                        val = PasswordValue.ToString();
                        break;
                    case (int)SystemSettingDataType.Undefined:
                    default:
                        val = "unknown datatype";
                        break;
                }
                return val;
            }
        }

        public string OldValue
        {
            get
            {
                string val = string.Empty;
                switch (DataTypeId)
                {
                    case (int)SystemSettingDataType.String:
                        val = oldSetting.StringValue;
                        break;
                    case (int)SystemSettingDataType.Integer:
                        val = oldSetting.IntegerValue.ToString();
                        break;
                    case (int)SystemSettingDataType.Double:
                        val = oldSetting.DoubleValue.ToString();
                        break;
                    case (int)SystemSettingDataType.DateTime:
                        val = oldSetting.DateValue.ToString();
                        break;
                    case (int)SystemSettingDataType.Boolean:
                        val = oldSetting.BooleanValue.ToString();
                        break;
                    case (int)SystemSettingDataType.Password:
                        val = oldSetting.PasswordValue.ToString();
                        break;
                    case (int)SystemSettingDataType.Undefined:
                    default:
                        val = "unknown datatype";
                        break;
                }
                return val;
            }
        }
        private SystemSetting oldSetting;

        public void CopyTo(SystemSetting entity)
        {
            oldSetting = entity;
            Name = entity.Name;
            DateValue = entity.DateValue;
            IntegerValue = entity.IntegerValue;
            DoubleValue = entity.DoubleValue;
            StringValue = entity.StringValue;
            PasswordValue = entity.PasswordValue;
            Category = entity.Category;
            SubCategory = entity.SubCategory;
            Description = entity.Description;
            DataTypeId = entity.DataTypeId;
            BooleanValue = entity.BooleanValue;
            NotNullableBooleanValue = entity.NotNullableBooleanValue;
            ExcludeFromSettingsUI = entity.ExcludeFromSettingsUI;
        }
        public SystemSetting CopyFrom()
        {
            var entity = oldSetting;
            entity.Name = Name;
            entity.Category = Category;
            entity.SubCategory = SubCategory;
            entity.Description = Description;
            entity.DataTypeId = DataTypeId;
            entity.BooleanValue = BooleanValue;
            entity.NotNullableBooleanValue = NotNullableBooleanValue;
            entity.DateValue = DateValue;
            entity.IntegerValue = IntegerValue;
            entity.DoubleValue = DoubleValue;
            entity.StringValue = StringValue;
            entity.PasswordValue = PasswordValue;
            entity.ExcludeFromSettingsUI = ExcludeFromSettingsUI;
            return entity;
        }
    }
}
