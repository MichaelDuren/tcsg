﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CSG.AppServer.Db;
using CSG.AppServer.Entities;
using CSG.AppServer.Managers;
using CSG.AppServer.Security;
using CSG.WebSite.Helpers;
using System.Security.Cryptography.X509Certificates;
using System.DirectoryServices;
using System.Security.Cryptography;
using System.IO;
using CSG.AppServer;
using CSG.WebSite.Models;

namespace CSG.WebSite.Controllers
{
    public class CertificateController : Controller
    {
        public ActionResult ManageCertificates(string savedMessage = null)
        {
            Gatekeeper gatekeeper = new Gatekeeper();
            try
            {
                gatekeeper.ValidateUserIsAppAdmin();
                bool webcert = true;
                try
                {
                    webcert = bool.Parse(Util.Config.DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.WebCertManagement]);
                }
                catch { } // default is true, which is showing web cert magament
                if (!webcert)
                    throw new Exception("WebCertManagement is off in web.config");
                CertificateManager certMgr = new CertificateManager();
                ViewBag.Certificates = certMgr.GetCertificates(true).Where(m => m.IsDeleted == false).OrderBy(m => m.Name);
                ViewBag.DeletedCertificates = certMgr.GetCertificates(true).Where(m => m.IsDeleted == true).OrderBy(m => m.Name);
                ViewBag.SavedMessage = savedMessage;

                return View();
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }


        public ActionResult ManageCertificate(int certificateId)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                gatekeeper.ValidateUserIsAppAdmin();
                bool webcert = true;
                try
                {
                    webcert = bool.Parse(Util.Config.DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.WebCertManagement]);
                }
                catch { } // default is true, which is showing web cert magament
                if (!webcert)
                    throw new Exception("WebCertManagement is off in web.config");
                CertificateManager certMgr = new CertificateManager();

                if (certificateId > 0)
                {
                    var cert = certMgr.GetCertificates(true).Where(crt => crt.Id == certificateId).FirstOrDefault();

                    if (cert == null)
                    {
                        throw new Exception("Certificate was not found in the database.");
                    }

                    var modelCert = new CertificateModel()
                    {
                        Name = cert.Name,
                        Id = cert.Id,
                        IssuedTo = cert.IssuedTo,
                        IssuedBy = cert.IssuedBy,
                        DistinguishedName = cert.DistinguishedName,
                        Thumbprint = cert.Thumbprint,
                        NotBefore = cert.NotBefore,
                        NotAfter = cert.NotAfter,
                        IsDeleted = cert.IsDeleted
                    };
                    return View("ManageCertificate", modelCert);
                }
                else
                {
                    return RedirectToAction("RegisterCertificate");
                }

            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }
        public ActionResult RegisterCertificate()
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                gatekeeper.ValidateUserIsAppAdmin();
                bool webcert = true;
                try
                {
                    webcert = bool.Parse(Util.Config.DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.WebCertManagement]);
                }
                catch { } // default is true, which is showing web cert magament
                if (!webcert)
                    throw new Exception("WebCertManagement is off in web.config");
                var cert = new CertificateModel();

                return View(cert);
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        /// <summary>
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult VerifyAndConfirmCertificate(FormCollection collection, CertificateModel modelCert)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                gatekeeper.ValidateUserIsAppAdmin();
                bool webcert = true;
                try
                {
                    webcert = bool.Parse(Util.Config.DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.WebCertManagement]);
                }
                catch { } // default is true, which is showing web cert magament
                if (!webcert)
                    throw new Exception("WebCertManagement is off in web.config");
                //Get the file name
                string fileName = string.Empty;
                string tempFileName = string.Empty;
                string combinedFilenames = string.Empty;

                string tempPath = AppServerContext.LocalUploadFolder;// Path.Combine(AppServerContext.PrimaryStorageLocation, "temp");
                if (!Directory.Exists(tempPath))
                {
                    Directory.CreateDirectory(tempPath);
                }

                var count = Request.Files.Count;
                if (count <= 0)
                {
                    throw new Exception("File count is invalid.");
                }
                else
                {

                    fileName = Path.GetFileName(Request.Files[0].FileName);
                    tempFileName = Guid.NewGuid().ToString() + ".tmp";
                    Request.Files[0].SaveAs(Path.Combine(tempPath, tempFileName));

                    var cert = new X509Certificate2(Path.Combine(tempPath, tempFileName));
                    var issuerDn = CertificateManager.ParseDistinguishedName(cert.Issuer);
                    var subjectDn = CertificateManager.ParseDistinguishedName(cert.Subject);
                    var base64Cert = Convert.ToBase64String(cert.Export(X509ContentType.Cert), Base64FormattingOptions.None);

                    // display the certificate for confirmation.
                    Certificate newCert = new Certificate()
                    {
                        IssuedTo = subjectDn.Find(item => item.Key.Equals("CN")).Value,
                        Name = modelCert.Name,
                        DistinguishedName = cert.Subject,
                        InSecurityWorld = false,
                        InStore = true,
                        Thumbprint = cert.Thumbprint,
                        IssuedBy = issuerDn.Find(item => item.Key.Equals("CN")).Value,
                        MimeEncoded = base64Cert,
                        NotAfter = cert.NotAfter,
                        NotBefore = cert.NotBefore
                    };
                    if (!newCert.InValidityPeriod)
                    {
                        var ex = new Exception("Certificate is expired or not yet valid.");
                        return UIHelper.RedirectToError(ex);
                    }
                    modelCert.CopyTo(newCert);

                    return View("ConfirmCertificate", modelCert);
                }
            }
            catch (CryptographicException e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        /// <summary>
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmCertificate(CertificateModel modelCert)
        {
            Gatekeeper gatekeeper = new Gatekeeper();
            String thumbprint = "";

            try
            {
                gatekeeper.ValidateUserIsAppAdmin();
                // false in webcertmanagement throw exception
                bool webcert = true;
                try
                {
                    webcert = bool.Parse(Util.Config.DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.WebCertManagement]);
                }
                catch { } // default is true, which is showing web cert magament
                if (!webcert)
                    throw new Exception("WebCertManagement is off in web.config");
                gatekeeper.LogUserActivity("Certificate Registration");

                var cert = new X509Certificate2(Convert.FromBase64String(modelCert.MimeEncoded));
                var issuerDn = CertificateManager.ParseDistinguishedName(cert.Issuer);
                var subjectDn = CertificateManager.ParseDistinguishedName(cert.Subject);


                // display the certificate for confirmation.
                Certificate certificate = new Certificate()
                {
                    IssuedTo = subjectDn.Find(item => item.Key.Equals("CN")).Value,
                    Name = modelCert.Name,
                    DistinguishedName = cert.Subject,
                    InSecurityWorld = false,
                    InStore = true,
                    Thumbprint = cert.Thumbprint,
                    IssuedBy = issuerDn.Find(item => item.Key.Equals("CN")).Value,
                    MimeEncoded = modelCert.MimeEncoded,
                    NotAfter = cert.NotAfter,
                    NotBefore = cert.NotBefore
                };

                // log the user activity
                ActivityLog.Log(gatekeeper.GetCurrentUserProfile(),
                                ActivityLog.LogEventId_SAVE_CERTIFICATE,
                                "ManageCertificate", "SaveCertificate",
                                certificate.Thumbprint);

                // just saving this to make the exception code simpler 
                thumbprint = certificate.Thumbprint;
                CertificateManager certMgr = new CertificateManager();
                certMgr.SaveCertificate(certificate);

                return RedirectToAction("ManageCertificates", new { @savedMessage = "Certificate Added" });
            }
            catch (CertificateExistsException)
            {
                return CertificateExists(thumbprint);
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }

        }

        /// <summary>
        /// </summary>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveCertificate(CertificateModel certToSave)
        {
            Gatekeeper gatekeeper = new Gatekeeper();
            String thumbprint = "";

            try
            {
                gatekeeper.ValidateUserIsAppAdmin();
                bool webcert = true;
                try
                {
                    webcert = bool.Parse(Util.Config.DynamicConfigurationManager.AppSettings[Constants.AppConfigNames.WebCertManagement]);
                }
                catch { } // default is true, which is showing web cert magament
                if (!webcert)
                    throw new Exception("WebCertManagement is off in web.config");
                gatekeeper.LogUserActivity("Save Certificate");

                // log the user activity
                ActivityLog.Log(gatekeeper.GetCurrentUserProfile(),
                                ActivityLog.LogEventId_SAVE_CERTIFICATE,
                                "ManageCertificate", "SaveCertificate",
                                certToSave.Thumbprint);
                CertificateManager certMgr = new CertificateManager();
                var storedCert = certMgr.GetCertificateById(certToSave.Id);
                storedCert.Name = certToSave.Name;
                storedCert.IsDeleted = certToSave.IsDeleted;
                certMgr.SaveCertificate(storedCert);

                return RedirectToAction("ManageCertificates", new { @savedMessage = "Certificate Saved" });
            }
            catch (CertificateExistsException)
            {
                return CertificateExists(thumbprint);
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }

        }

        public ActionResult CertificateExists(string thumbprint)
        {
            TempData.Add("entity", "Certificate");
            return RedirectToAction("EntityExists", "Admin");
        }

        public ActionResult ConfirmCertificateUpdate()
        {
            return View();
        }
    }
}
