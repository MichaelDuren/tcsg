﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CSG.AppServer.Db;
using CSG.AppServer.Entities;
using CSG.AppServer.Managers;
using CSG.AppServer.Security;
using CSG.WebSite.Helpers;

namespace CSG.WebSite.Controllers
{
    public class GroupController : Controller
    {
        public ActionResult ManageGroups()
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                gatekeeper.ValidateUserIsAppAdmin();

                ESPDbDomainService svc = new ESPDbDomainService();
                //GroupsModel model = new GroupsModel();
                ViewBag.Groups = svc.GetGroups(null);

                return View();
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        public ActionResult ManageGroup(int groupId)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                gatekeeper.ValidateUserIsAppAdmin();

                ESPDbDomainService svc = new ESPDbDomainService();
                Group group = new Group();

                if (groupId > 0)
                {
                    group = svc.GetGroupById(groupId);
                    IEnumerable<int> cstgs = svc.GetCodeSigningTypeGroups(null)
                                                                    .Where(m => m.GroupId == groupId)
                                                                    .Select(m => m.CodeSigningTypeId)
                                                                    .ToList();

                    IQueryable<CodeSigningType> types = svc.GetCodeSigningTypes(null).Where(m => cstgs.Contains(m.Id) && m.IsDeleted == false);

                    ViewBag.SelectedCodeSigningTypes = new SelectList(types, "Id", "Name", null);
                    ViewBag.RemainingCodeSigningTypes = new SelectList(svc.GetCodeSigningTypes(null).Where(m => !cstgs.Contains(m.Id) && m.IsDeleted == false), "Id", "Name", null);
                    foreach (CodeSigningType type in types)
                    {
                        ViewBag.DelimitedCodeSigningTypes = ViewBag.DelimitedCodeSigningTypes + "," + type.Id;
                    }
                }
                else
                {
                    IEnumerable<int> cstgs = svc.GetCodeSigningTypeGroups(null)
                                                .Where(m => m.GroupId == -1)
                                                .Select(m => m.CodeSigningTypeId);

                    ViewBag.SelectedCodeSigningTypes = new SelectList(svc.GetCodeSigningTypes(null).Where(m => cstgs.Contains(m.Id) && m.IsDeleted == false), "Id", "Name", null);
                    ViewBag.RemainingCodeSigningTypes = new SelectList(svc.GetCodeSigningTypes(null).Where(m => m.IsDeleted == false), "Id", "Name", null);
                }

                return View(group);
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        /// <summary>
        /// Save the group
        /// </summary>
        [HttpPost]
        public ActionResult SaveGroup(Group group)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                gatekeeper.ValidateUserIsAppAdmin();
                gatekeeper.LogUserActivity("Group Updated");

                bool sendToADSync = group.Id == 0;
                string selectedCodeSigningTypes = Request.Form["hdnSelectedCodeSigningTypes"];
                selectedCodeSigningTypes = selectedCodeSigningTypes.Replace("undefined", String.Empty);

                GroupManager manager = new GroupManager();
                manager.SaveGroup(group, selectedCodeSigningTypes);

                //If the group is new, display the confirmation page notifying of AD sync, otherwise go back to Groups page
                if (sendToADSync)
                {
                    return RedirectToAction("ConfirmGroupUpdate");
                }
                else
                {
                    return RedirectToAction("ManageGroups");
                }
            }
            catch (GroupExistsException e)
            {
                return GroupExists(e.ExistingGroupId);
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        public ActionResult GroupExists(int groupId)
        {
            TempData.Add("id", groupId);
            TempData.Add("entity", "Group");

            return RedirectToAction("EntityExists", "Admin");
        }

        public ActionResult ConfirmGroupUpdate()
        {
            return View();
        }
    }
}
