﻿using System;
using System.Linq;
using System.Web.Mvc;
using CSG.AppServer.Reporting;
using CSG.AppServer.Security;
using CSG.WebSite.Helpers;
using Util.Reports;
using System.Collections.Generic;
using CSG.AppServer.Entities;
using CSG.AppServer.Db;
using CSG.WebSite.Models;
using CSG.AppServer.Managers;
using Util;
using CSG.AppServer.Exceptions;

namespace CSG.WebSite.Controllers
{
    public class ApproveController : Controller
    {
        public ActionResult ManageApprovalWindows()
        {
            Gatekeeper gatekeeper = new Gatekeeper();
            TemporalApprovalsModel model = new TemporalApprovalsModel();

            try
            {
                //Get the signing types for the user
                UserProfile currentUser = gatekeeper.GetCurrentUserProfile();
                ESPDbDomainService svc = new ESPDbDomainService();
                List<CodeSigningTemporalApproval> temporalApprovals;


                // get the list of temporal approvals that this user can cancel
                if (currentUser.IsAppAdmin)
                {
                    // get them all
                    temporalApprovals = svc.GetCodeSigningTemporalApprovals().ToList();
                }
                else
                {
                    // get only the approvals issued by this user
                    temporalApprovals = svc.GetCodeSigningTemporalApprovals().Where(ta => ta.ApprovingUserProfileId == currentUser.Id).ToList(); ;
                }

                model.temporalApprovals = temporalApprovals;
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }

            return View("ManageApprovalWindows", model);
        }

        public JsonResult RemoveTemporalApproval(int taId)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                ESPDbDomainService svc = new ESPDbDomainService();
                var ta = svc.GetCodeSigningTemporalApprovalById(taId);
                gatekeeper.ValidateTemporalApprovalModification(ta);

                svc.DeleteCodeSigningTemporalApproval(ta);
                svc.SaveChanges();

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ManageApprovalWindow(int windowId)
        {
            Gatekeeper gatekeeper = new Gatekeeper();
            TemporalApprovalModel model = new TemporalApprovalModel();
            List<TemporalApprovalModel> approvalCandidates = new List<TemporalApprovalModel>();

            try
            {
                //Get the signing types for the user
                UserProfile currentUser = gatekeeper.GetCurrentUserProfile();
                ESPDbDomainService svc = new ESPDbDomainService();
                List<CodeSigningType> typesForApproval;


                // get the list of temporal approvals for which this user can specify approval window
                if (currentUser.IsAppAdmin)
                {
                    // get the raw signing types that this user is an approver for
                    typesForApproval = svc.GetCodeSigningTypes().Where(cst => cst.RawSign == true && !cst.IsAutoSign).ToList();
                }
                else
                {
                    // get the raw signing types that this user is an approver for
                    typesForApproval = currentUser.CodeSigningTypes.
                        Where(cst => ((cst.UserSigningTypeRole == (int)AppServer.UserActions.Approve1Requests && cst.CodeSigningType.AllowApprover1Approval)||
                                      (cst.UserSigningTypeRole == (int)AppServer.UserActions.ManagerApproveRequests && cst.CodeSigningType.AllowManagerApproval) ||
                                      (cst.UserSigningTypeRole == (int)AppServer.UserActions.Approve2Requests)) &&
                                     /* cst.CodeSigningType.RawSign == true && */ !cst.CodeSigningType.IsAutoSign).Select(cst => cst.CodeSigningType).ToList();
   
                }

                // put the code signing types into the ViewBag
                var certs = new SelectList(typesForApproval, "Id", "Name");
                ViewBag.Certificates = certs;

                SelectListItem slOneDay = new SelectListItem { Selected = true, Text = "One Day", Value = "1" };
                SelectListItem slThreeDays = new SelectListItem { Selected = true, Text = "Three Days", Value = "3" };
                SelectListItem slSevenDays = new SelectListItem { Selected = true, Text = "Seven Days", Value = "7" };
                SelectListItem slThirtyDays = new SelectListItem { Selected = true, Text = "Thirty Days", Value = "30" };
                SelectListItem slSixMonths = new SelectListItem { Selected = true, Text = "Six Months", Value = "182" };
                List<SelectListItem> selectListItems = new List<SelectListItem>() { slOneDay, slThreeDays, slSevenDays, slThirtyDays, slSixMonths };
                var slApprovalWindows = new SelectList(selectListItems, "Value", "Text", "1");
                ViewData.Add("approvalWindows", slApprovalWindows);

                return View();
            }
            catch (Exception e)
            {
                gatekeeper.LogException("Exception in ManageApprovalWindow", e);
                return UIHelper.RedirectToError(e);
            }

        }
        public class SelectedInfo
        {
            public List<string> users { get; set; }
        }
        public JsonResult GetUserInfo(int typeId)
        {

            Gatekeeper gatekeeper = new Gatekeeper();
            try
            {
                SelectedInfo selectedInfo = new SelectedInfo();
                List<string> users = new List<string>();

                gatekeeper.ValidateUserApproverOnSigningType(typeId);
                var currentUserProfile = gatekeeper.GetCurrentUserProfile();

                ESPDbDomainService svc = new ESPDbDomainService();
                var cst = svc.GetCodeSigningTypeById(typeId);


                var userList = svc.GetUserProfileCodeSigningTypes().Where(upcst => upcst.CodeSigningTypeId == typeId &&
                                                                          upcst.UserSigningTypeRole == (int)AppServer.UserActions.SubmitRequests &&
                                                                          upcst.UserProfileId != currentUserProfile.Id)
                                                                   .Select(s => new { s.UserProfile.LoginId, s.UserProfile.FirstName, s.UserProfile.LastName }).ToList();

                var delimitedUsers = string.Empty;

                foreach (var user in userList)
                {
                    var display = user.LoginId;
                    if (user.LastName != null)
                    {
                        display += ", " + user.LastName;
                    }
                    if (user.FirstName != null)
                    {
                        display += ", " + user.FirstName;
                    }
                    users.Add(display);
                }
                selectedInfo.users = users;

                var response = Json(selectedInfo, JsonRequestBehavior.AllowGet);

                return response;
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SaveApprovalWindow(FormCollection collection, TemporalApprovalModel approvalInfo)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                CodeSigningRequestManager manager = new CodeSigningRequestManager();
                AppServer.UserActions approvingAction;

                List<int> validWindowValues = new List<int>() { 1, 3, 7, 30, 182 };
                // check to see if there is an approval window specified
                string typeIdString = collection["ddlProfile"];
                Guard.ArgumentNotNull(typeIdString, "SaveApprovalWindow: Signing profile value is invalid.");
                var typeId  = Convert.ToInt32(typeIdString);

                // get the users action for this type.  We are picking the first one in the event there is more than one for this type.
                var currentUserProfile = gatekeeper.GetCurrentUserProfile();
                if (currentUserProfile.IsAppAdmin)
                {
                    approvingAction = AppServer.UserActions.AdministratorApproveRequests;
                }
                else
                {
                    try
                    {
                        approvingAction = currentUserProfile.CodeSigningTypes.Where(t => (t.CodeSigningTypeId == typeId) &&
                                                                       (t.UserSigningTypeRole == (int)AppServer.UserActions.Approve1Requests ||
                                                                        t.UserSigningTypeRole == (int)AppServer.UserActions.Approve2Requests ||
                                                                        t.UserSigningTypeRole == (int)AppServer.UserActions.ManagerApproveRequests)).Select(s => s.AuthorizedUserAction).First();
                    }
                    catch (Exception)
                    {
                        throw new UserRightsException("User does not have permission to perform this action.");
                    }
                    gatekeeper.ValidateUserForActionOnSigningType(typeId, approvingAction);
                }

                string userProfileIdString = collection["ddlUser"];
                Guard.ArgumentNotNull(userProfileIdString, "SaveApprovalWindow: User profile value is invalid.");
                string userLoginId = userProfileIdString.Split(',').First();
                Guard.ArgumentNotNull(userLoginId, "SaveApprovalWindow: User profile value is invalid.");
                string approvalWindowText = collection["ddlApprovalWindow"];
                Guard.ArgumentNotNull(approvalWindowText, "SaveApprovalWindow: Approval window value is invalid.");
                // parse the approval target value
                int approvalWindowDays = Convert.ToInt32(approvalWindowText);
                if (approvalWindowDays != 0 && validWindowValues.Contains(approvalWindowDays))
                {
                    ESPDbDomainService svc = new AppServer.Db.ESPDbDomainService();

                    // Now what do we do, all we have is a certificate label .????
                    var cst = svc.GetCodeSigningTypeById(Convert.ToInt32(typeId));
                    Guard.ArgumentNotNull(cst, "SaveApprovalWindow: Signing Profile could not be found.");

                    var userProfile = svc.GetUserProfiles().Where(p => p.LoginId == userLoginId).First();
                    Guard.ArgumentNotNull(userProfile, "SaveApprovalWindow: User profile could not be found.");

                    if (currentUserProfile.Id == userProfile.Id)
                    {
                        throw new UserRightsException("User cannot approve their own profile for requests.");
                    }

                    CodeSigningTemporalApproval approvalWindow = new CodeSigningTemporalApproval()
                                        {
                                            CodeSigningTypeId = cst.Id,
                                            UserProfileId = userProfile.Id,
                                            ApprovingUserProfileId = currentUserProfile.Id,
                                            Hours = approvalWindowDays * 24,
                                            ApprovingAction = approvingAction,
                                            EnforceSameFileName = approvalInfo.requireMatchingFilename,
                                            FileName = "",
                                            StartDate = DateTime.Now
                                        };

                    var existingTAs = svc.GetCodeSigningTemporalApprovals().Where(m => m.ApprovingUserProfileId == currentUserProfile.Id &&
                                                                                       m.UserProfileId == userProfile.Id &&
                                                                                       m.CodeSigningTypeId == cst.Id &&
                                                                                       m.ApprovingUserAction == (int)approvingAction);
                    foreach (var existingTA in existingTAs)
                    {
                        svc.DeleteCodeSigningTemporalApproval(existingTA);
                    }

                    svc.InsertCodeSigningTemporalApproval(approvalWindow);
                    svc.SaveChanges();
                }
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
            return RedirectToAction("ManageApprovalWindows", "Approve");

        }

    }
}
