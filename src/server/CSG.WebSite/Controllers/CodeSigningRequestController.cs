﻿using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using CSG.AppServer;
using CSG.AppServer.Db;
using CSG.AppServer.Entities;
using CSG.AppServer.Managers;
using CSG.AppServer.Security;
using CSG.WebSite.Helpers;
using System.Collections.Generic;

namespace CSG.WebSite.Controllers
{
    public class CodeSigningRequestController : Controller
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// This action will allow the user to view the details of a code signing request provided they are a member the development group that
        /// submitted the request
        /// </summary>
        /// 
        public ActionResult Details(int requestId, string isFromDashboard = "false", string offline = "false")
        {
            Gatekeeper gatekeeper = new Gatekeeper();
            Dictionary<string, string> unsignedFilesAndHashes = new Dictionary<string, string>();
            Dictionary<string, string> signedFilesAndHashes = new Dictionary<string, string>();
            bool offlineSign;

            try
            {
                gatekeeper.ValidateUserViewRequest(requestId);

                CSG.AppServer.Db.ESPDbDomainService svc = new AppServer.Db.ESPDbDomainService();
                CodeSigningRequest request = svc.GetCodeSigningRequestById(requestId, "CreatedByUser, CodeSigningType");

                ViewBag.unsignedFilesAndHashes = CodeSigningRequestManager.GetNameAndHashValues(request.UnsignedFileName, request.UnsignedFileHash);

                ViewBag.IsFromDashboard = Boolean.Parse(isFromDashboard);
                ViewBag.IsEligibleForEscalation = false;
                if (request.EscalationCount == null || request.EscalationCount == 0)
                {
                    if (request.CreatedByUserId == gatekeeper.GetCurrentUserProfile().Id &&
                       (request.Status == CodeSigningRequestStatus.PartiallyApproved ||
                        request.Status == CodeSigningRequestStatus.Submitted))
                    {
                        ViewBag.IsEligibleForEscalation = true;
                    }
                }

                ViewBag.ApprovingUsers = null;
                if (request.Status == CodeSigningRequestStatus.Submitted ||
                    request.Status == CodeSigningRequestStatus.PartiallyApproved)
                {
                    List<int> profileIds;
                    List<UserProfile> approvingUsers = new List<UserProfile>();
                    List<string> approvingUserFormattedList = new List<string>();

                    if (request.CodeSigningType.AllowApprover1Approval && request.Approver1ResponderId == null)
                    {
                        // get the names of the other users in the group
                        profileIds = svc.GetUserProfileGroups().Where(m => m.GroupId == request.GroupId).Select(g => g.UserProfileId).ToList();
                        if (request.CreatedByUserId.HasValue)
                        {
                            profileIds.Remove(request.CreatedByUserId.Value);
                        }
                        approvingUsers = svc.GetUserProfiles().Where(m => profileIds.Contains(m.Id)).ToList();
                    }

                    if (request.CodeSigningType.AllowApprover2Approval && request.Approver2ResponderId == null)
                    {
                        // get the approver names
                        profileIds = svc.GetCodeSigningTypeGroups().Where(m => m.CodeSigningTypeId == request.CodeSigningTypeId &&
                                                                        m.IsApprover == true).
                                                                      Select(m => m.GroupId).ToList();
                        profileIds = svc.GetUserProfileGroups().Where(m => profileIds.Contains(m.GroupId)).Select(g => g.UserProfileId).ToList();

                        approvingUsers.AddRange(svc.GetUserProfiles().Where(m => profileIds.Contains(m.Id)));
                    }

                    foreach (var profile in approvingUsers)
                    {
                        if (!string.IsNullOrEmpty(profile.Email))
                        {
                            approvingUserFormattedList.Add(string.Format("{0}, ({1})", profile.DisplayName, profile.Email));
                        }
                        else
                        {
                            approvingUserFormattedList.Add(string.Format("{0}", profile.DisplayName));
                        }
                    }
                    ViewBag.ApprovingUsers = approvingUserFormattedList;
                }
                offlineSign = Boolean.Parse(offline);

                if (offlineSign)
                {
                    return View("Offline", request);
                }
                else
                {
                    ViewBag.signedFilesAndHashes = CodeSigningRequestManager.GetNameAndHashValues(request.SignedFileName, request.SignedFileHash);
                    return View("Details", request);
                }


            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        public ActionResult Approve(int requestId)
        {
            SelectListItem slNoDays = new SelectListItem { Selected = true, Text = "None", Value = "0" };
            SelectListItem slOneDay = new SelectListItem { Selected = true, Text = "One Day", Value = "1" };
            SelectListItem slThreeDays = new SelectListItem { Selected = true, Text = "Three Days", Value = "3" };
            SelectListItem slSevenDays = new SelectListItem { Selected = true, Text = "Seven Days", Value = "7" };
            SelectListItem slThirtyDays = new SelectListItem { Selected = true, Text = "Thirty Days", Value = "30" };
            SelectListItem slSixMonths = new SelectListItem { Selected = true, Text = "Six Months", Value = "182" };
            List<SelectListItem> selectListItems = new List<SelectListItem>() { slNoDays, slOneDay, slThreeDays, slSevenDays, slThirtyDays, slSixMonths };
            var slApprovalWindows = new SelectList(selectListItems, "Value", "Text", "NoDays");
            ViewData.Add("approvalWindows", slApprovalWindows);

            CSG.AppServer.Db.ESPDbDomainService svc = new AppServer.Db.ESPDbDomainService();
            CodeSigningRequest request = svc.GetCodeSigningRequestById(requestId, "CreatedByUser, CodeSigningType");
            ViewBag.CodeSigningTypeName = request.CodeSigningType.Name;

            return View("Approve", request);
        }

        /// <summary>
        /// This action will allow the user to create a new code signing request provided they are a developer in the group selected.
        /// </summary>
        public ActionResult Create(int groupId, int codeSigningTypeId = -1, string errorMessage = null, string projectName = null)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                gatekeeper.ValidateUserDeveloperInGroup(groupId);

                CodeSigningRequest csRequest = new CodeSigningRequest();
                csRequest.GroupId = groupId;
                csRequest.ProjectName = projectName;
                if (codeSigningTypeId > 0)
                {
                    csRequest.CodeSigningTypeId = codeSigningTypeId;
                }

                ViewBag.Groups = new SelectList(gatekeeper.GetCurrentUserProfile().Groups
                                                                .Select(upg => upg.Group)
                                                                .Where(g => g.IsDeleted == false)
                                                                .AsEnumerable(), "Id", "Name", groupId);

                ViewBag.CodeSigningTypes = UIHelper.GetSelectList(new SelectList(Group.GetCodeSigningTypes(groupId)
                                                                                        .Where(t => t.IsDeleted == false), "Id", "Name", codeSigningTypeId), true, codeSigningTypeId);

                if (!string.IsNullOrWhiteSpace(errorMessage))
                {
                    ModelState.AddModelError("", errorMessage);
                }

                ViewBag.projectList = getProjectList();
                ViewBag.releaseList = getReleaseList();

                return View("Create", csRequest);
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        private List<string> getProjectList()
        {
            List<string> list = (List<string>)System.Web.HttpContext.Current.Cache.Get("plist");
            if ( list == null)
            {
                list= AppServerContext.ProjectList.Split(',').ToList();
                System.Web.HttpContext.Current.Cache.Insert("plist", list, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(5));
            }

            return list;
        }

        private List<string> getReleaseList()
        {
            List<string> list = (List<string>)System.Web.HttpContext.Current.Cache.Get("rlist");
            if (list == null)
            {
                list = AppServerContext.ReleaseList.Split(',').ToList();
                System.Web.HttpContext.Current.Cache.Insert("rlist", list, null, System.Web.Caching.Cache.NoAbsoluteExpiration, TimeSpan.FromMinutes(5));
            }
            return list;
        }


        public class SelectedProfileInfo
        {
            public string MetadataField1Label;
            public string MetadataField2Label;
            public string fileExtensions;
        }

        public JsonResult GetProfileInfo(int typeId)
        {

            Gatekeeper gatekeeper = new Gatekeeper();
            try
            {
                gatekeeper.ValidateUserAuthorizedForType(typeId);

                SelectedProfileInfo selectedInfo = new SelectedProfileInfo();
                List<string> users = new List<string>();

                ESPDbDomainService svc = new ESPDbDomainService();
                var cst = svc.GetCodeSigningTypeById(typeId);

                selectedInfo.MetadataField1Label = cst.MetadataField1Label == null ? "MetadataField1Label" : cst.MetadataField1Label;
                selectedInfo.MetadataField2Label = cst.MetadataField2Label == null ? "MetadataField2Label" : cst.MetadataField2Label;
                if (cst.ValidFileExtensions != null)
                {
                    string[] extensions = cst.ValidFileExtensions.Split(',');
                    if (extensions.Count() > 1)
                    {
                        int i = 0;
                        int count = extensions.Count();
                        selectedInfo.fileExtensions = "All files submitted must be of one of the following types: ";
                        foreach (var extension in extensions)
                        {
                            extension.Trim(' ');
                            selectedInfo.fileExtensions += extension;
                            ++i;
                            if (i < count)
                            {
                                selectedInfo.fileExtensions += ", ";
                            }
                        }
                    }
                    else
                    {
                        selectedInfo.fileExtensions = String.Format("All files submitted must be of type '{0}'", 
                                                                    cst.ValidFileExtensions);
                    }

                }
                else
                {
                    selectedInfo.fileExtensions = null;
                }
                var response = Json(selectedInfo, JsonRequestBehavior.AllowGet);

                return response;
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(CodeSigningRequest csRequest)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                gatekeeper.ValidateUserDeveloperInGroup(csRequest.GroupId);
                gatekeeper.ValidateGroupAuthorizedForType(csRequest.GroupId, csRequest.CodeSigningTypeId);

                //Get the file name
                string fileName = string.Empty;
                string tempFileName = string.Empty;
                string combinedFilenames = string.Empty;

                string tempPath = AppServerContext.LocalUploadFolder;// Path.Combine(AppServerContext.PrimaryStorageLocation, "temp");
                if (!Directory.Exists(tempPath))
                {
                    Directory.CreateDirectory(tempPath);
                }

                //Have to copy the files to a temporary location because we don't have the request Id yet
                //We could create a request first but then we would have to create it without the file hash
                var count = Request.Files.Count;
                foreach (string upload in Request.Files)
                {
                    fileName = Path.GetFileName(Request.Files[upload].FileName);
                    tempFileName = Guid.NewGuid().ToString() + ".tmp";
                    Request.Files[upload].SaveAs(Path.Combine(tempPath, tempFileName));
                    //Only handling one file right now, so break after the first file
                    combinedFilenames += fileName;
                    if (--count != 0)                  
                    {
                        combinedFilenames += ";";
                    }
                    break;
                }

                //Create the code signing request
                CodeSigningRequestManager manager = new CodeSigningRequestManager();
                csRequest = manager.CreateCodeSigningRequest(csRequest.CodeSigningTypeId, csRequest.GroupId, Path.Combine(tempPath, tempFileName), fileName,
                                                             false, csRequest.ReleaseName, csRequest.ProjectName);


                gatekeeper.LogUserActivity("Create Code Signing Request", csRequest.Id);

                //Validations passed and the file has been uploaded.  Display the confirmation page to the user
                return Confirm(csRequest);
            }
            catch (InvalidFileTypeException)
            {
                return Create(csRequest.GroupId, csRequest.CodeSigningTypeId, "The file type provided is not valid for the code signing type selected.");
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        public ActionResult Confirm(CodeSigningRequest request)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                gatekeeper.ValidateUserDeveloperInGroup(request.GroupId);
//                gatekeeper.ValidateUserForActionOnSigningType(request.CodeSigningTypeId, 
//                                                              UserActions.SubmitRequests);

                ESPDbDomainService svc = new ESPDbDomainService();
                request = svc.GetCodeSigningRequestById(request.Id, "CodeSigningType");

                ViewBag.unsignedFilesAndHashes = CodeSigningRequestManager.GetNameAndHashValues(request.UnsignedFileName, request.UnsignedFileHash);

                return View("Confirm", request);
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Escalate(FormCollection collection)
        {
            RouteValueDictionary rvd = new RouteValueDictionary();
            Gatekeeper gatekeeper = new Gatekeeper();
            CodeSigningRequestManager mgr = new CodeSigningRequestManager();

            try
            {
                int csRequestId = System.Convert.ToInt32(collection["hdnCSRequstIdConfirm"]);

                gatekeeper.ValidateUserViewRequest(csRequestId);

                gatekeeper.LogUserActivity("Escalate Code Signing Request", csRequestId);
                var request = mgr.GetCodeSigningRequestById(csRequestId);

                mgr.SendEscalationNotifications(request);

                return Output("Your request for signing has been escalated for Enterprise Approval.  You should receive an email notification when the request has been processed.", 
                               request.Id, false);
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }

        }

        public ActionResult FulfillOffline(int requestId, string errorMessage=null)
        {
            ViewBag.RequestId = requestId.ToString();
            ViewBag.ErrorMessage = errorMessage;
            return View("FulfillOffline", requestId);
        }

        public ActionResult RejectOffline(int requestId)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            gatekeeper.ValidateUserOfflineSignerInRequest(requestId);

            CodeSigningRequestManager manager = new CodeSigningRequestManager();

            var csRequest = manager.GetCodeSigningRequestById(requestId);
            manager.RejectOfflineCodeSigningRequest(csRequest);

            return RedirectToAction("Dashboard", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FulfillOffline(FormCollection collection)
        {
            Gatekeeper gatekeeper = new Gatekeeper();
            RouteValueDictionary rvd = new RouteValueDictionary();
            int csRequestId = 0;
            try
            {

                var requestIdString = collection["hdnRequestId"];
                csRequestId = System.Convert.ToInt32(requestIdString);
                gatekeeper.ValidateUserOfflineSignerInRequest(csRequestId);
                CodeSigningRequestManager manager = new CodeSigningRequestManager();

                gatekeeper.LogUserActivity("Fulfill Offline Signing Request", csRequestId);

                var csRequest = manager.GetCodeSigningRequestById(csRequestId);
                if (csRequest == null)
                {
                    var ex = new Exception("Invalid request, request record could not be found.");
                    return UIHelper.RedirectToError(ex);
                }

                //Get the file name
                string fileName = string.Empty;
                string tempFileName = string.Empty;
                string combinedFilenames = string.Empty;

                string tempPath = AppServerContext.LocalUploadFolder;// Path.Combine(AppServerContext.PrimaryStorageLocation, "temp");
                if (!Directory.Exists(tempPath))
                {
                    Directory.CreateDirectory(tempPath);
                }

                //Have to copy the files to a temporary location because we don't have the request Id yet
                //We could create a request first but then we would have to create it without the file hash
                if (Request.Files.Count > 0)
                {

                    fileName = Path.GetFileName(Request.Files[0].FileName);

                    tempFileName = Guid.NewGuid().ToString() + ".tmp";
                    Request.Files[0].SaveAs(Path.Combine(tempPath, tempFileName));

 
                    csRequest = manager.FulfillOfflineCodeSigningRequest(csRequest, Path.Combine(tempPath, tempFileName),
                                                                         fileName);

                    return ConfirmOffline(csRequest);
                }
                else
                {
                    var ex = new Exception("Invalid request, no files were submitted.");
                    return UIHelper.RedirectToError(ex);

                }
            }
            catch (FileHashMismatchException)
            {
                return RedirectToAction("FulfillOffline", new { @requestId = csRequestId,
                     @errorMessage = "Signed and unsigned files match, requests cannot be fulfilled with the unsigned file."});
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        public ActionResult ConfirmOffline(CodeSigningRequest request)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                gatekeeper.ValidateUserForActionOnSigningType(request.CodeSigningTypeId,
                                                              UserActions.OfflineSignRequests);

                ESPDbDomainService svc = new ESPDbDomainService();
                request = svc.GetCodeSigningRequestById(request.Id, "CodeSigningType");

                ViewBag.signedFilesAndHashes = CodeSigningRequestManager.GetNameAndHashValues(request.SignedFileName, request.SignedFileHash);

                return View("ConfirmOffline", request);
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ConfirmOffline(FormCollection collection)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                int csRequestId = System.Convert.ToInt32(collection["hdnCSRequstIdConfirm"]);

                gatekeeper.ValidateUserOfflineSignerInRequest(csRequestId);
                gatekeeper.LogUserActivity("Confirm Code Signing Request Offline Fulfillment", csRequestId);

                CodeSigningRequestManager manager = new CodeSigningRequestManager();
                manager.ConfirmFulfillOfflineCodeSigningRequest(csRequestId);

                return Output("This offline signing request has been fulfilled.", csRequestId);
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CancelOffline(FormCollection collection)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                int csRequestId = System.Convert.ToInt32(collection["hdnCSRequstIdCancel"]);

                gatekeeper.ValidateUserOfflineSignerInRequest(csRequestId);
                gatekeeper.LogUserActivity("Cancel Offline Fulfillment", csRequestId);

                CodeSigningRequestManager manager = new CodeSigningRequestManager();
                manager.CancelOfflineFulfill(csRequestId);

                return RedirectToAction("Dashboard", "Home");
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Confirm(FormCollection collection)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                int csRequestId = System.Convert.ToInt32(collection["hdnCSRequstIdConfirm"]);

                gatekeeper.ValidateUserDeveloperInRequest(csRequestId);
                gatekeeper.LogUserActivity("Confirm Code Signing Request", csRequestId);

                CodeSigningRequestManager manager = new CodeSigningRequestManager();
                manager.ConfirmCodeSigningRequest(csRequestId);

                ViewBag.Title = String.Format("Completed Code Signing Request ({0})", csRequestId);

                return Output(String.Format("Your request for signing has been submitted.  The Request's Identifer is {0}.  " +
                                            "You should receive an email notification when the request has been processed.  " +
                                            "If this request requires additional approval, then authorized approvers will be notified via email that a request is pending.", csRequestId), csRequestId);
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ApproveOrDeny(FormCollection collection)
        {
            string requestIdString = collection["hdnRequestId"];

            var requestId = Convert.ToInt32(requestIdString);

            string actionString = collection["rbApproveDeny"];
            string denyReason = collection["tbDenyReason"];
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                gatekeeper.ValidateUserApproverInRequest(requestId);
                CodeSigningRequestManager manager = new CodeSigningRequestManager();


                if (actionString == "Approve")
                {
                    gatekeeper.LogUserActivity("Approve Code Signing Request", requestId);
                    var approvalAction = manager.ApproveCodeSigningRequest(requestId);
                    List<int> validWindowValues = new List<int>() { 1, 3, 7, 30, 182 };
                    // check to see if there is an approval window specified
                    string approvalWindowText = collection["ddlApprovalWindow"];
                    int approvalWindowDays = Convert.ToInt32(approvalWindowText);
                    bool requireFileNameMatch = Convert.ToBoolean(collection["cbRequireFilenameMatch"].Split(',')[0]);
                    if (approvalWindowDays != 0 && validWindowValues.Contains(approvalWindowDays))
                    {
                        gatekeeper.LogUserActivity("Add Temporal Approval");
                        ESPDbDomainService svc = new AppServer.Db.ESPDbDomainService();

                        var req = manager.GetCodeSigningRequestById(requestId);
                        CodeSigningTemporalApproval approvalWindow = new CodeSigningTemporalApproval() { 
                                                                         CodeSigningTypeId = req.CodeSigningTypeId,
                                                                         UserProfileId = req.CreatedByUser.Id,
                                                                         ApprovingUserProfileId = gatekeeper.GetCurrentUserProfile().Id,
                                                                         Hours = approvalWindowDays * 24,
                                                                         ApprovingAction = approvalAction,
                                                                         EnforceSameFileName = requireFileNameMatch,
                                                                         FileName = req.UnsignedFileName,
                                                                         StartDate = DateTime.Now
                                                                        };
                        svc.InsertCodeSigningTemporalApproval(approvalWindow);
                        svc.SaveChanges();                            
                    }

                }
                else
                {
                    gatekeeper.LogUserActivity("Deny Code Signing Request", requestId);
                    manager.DenyCodeSigningRequest(requestId, denyReason);
                }
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
            return RedirectToAction("Dashboard", "Home");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Cancel(FormCollection collection)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                int csRequestId = System.Convert.ToInt32(collection["hdnCSRequstIdCancel"]);

                gatekeeper.ValidateUserDeveloperInRequest(csRequestId);
                gatekeeper.LogUserActivity("Abort Code Signing Request", csRequestId);

                CodeSigningRequestManager manager = new CodeSigningRequestManager();
                manager.CancelCodeSigningRequest(csRequestId);

                return RedirectToAction("Dashboard", "Home");
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        public ActionResult Output(string message, int csRequestId, bool includeCreateAnother = true)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                CSG.AppServer.Db.ESPDbDomainService svc = new AppServer.Db.ESPDbDomainService();
                CodeSigningRequest request = svc.GetCodeSigningRequestById(csRequestId);

                ViewBag.Message = message;
                ViewBag.GroupId = request.GroupId;
                ViewBag.CodeSigningTypeId = request.CodeSigningTypeId;
                ViewBag.IncludeCreateAnotherButton = includeCreateAnother;
                return View("Output");
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        [HttpPost]
        public ActionResult Output(FormCollection collection)
        {
            Gatekeeper gatekeeper = new Gatekeeper();
            int typeId = System.Convert.ToInt32(collection["hdnCodeSigningTypeId"]);

            try
            {
                int groupId = System.Convert.ToInt32(collection["hdnGroupId"]);
                RouteValueDictionary rvd = new RouteValueDictionary();
                rvd.Add("groupId", groupId);
                return RedirectToAction("Create", "CodeSigningRequest", rvd);
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        public ActionResult DownloadFile(int requestId, string type, string filename)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                gatekeeper.ValidateUserViewRequest(requestId);

                // prevent the browser from trying to interpret the file
                Response.Headers.Add("X-content-type-options", "nosniff");
                CodeSigningRequestManager manager = new CodeSigningRequestManager();
                ViewBag.FileToDownload = manager.GetFile(requestId, type, filename);

                log.Debug(ViewBag.FileToDownload);

            }
            catch (InvalidOperationException e)
            {
                ViewBag.ErrorMessage = String.Format("An error occurred attempting to copy the file to the download location.  The request id {0} could not be found.", requestId);
                gatekeeper.LogException(String.Empty, e);
            }
            catch (FileHashMismatchException e)
            {
                ViewBag.ErrorMessage = String.Format("The file you are downloading is corrupt. Please contact a system administrator for assistance.");
                gatekeeper.LogException(String.Empty, e);
            }
            catch (UnauthorizedAccessException e)
            {
                ViewBag.ErrorMessage = String.Format("An error occurred attempting to copy the file to the download location.  Please contact a system administrator for assistance.");
                gatekeeper.LogException(String.Empty, e);
            }
            catch (Exception e)
            {
                ViewBag.ErrorMessage = String.Format("It does not appear that you are authorized to download this file.");
                gatekeeper.LogException(String.Empty, e);
            }

            return View();
        }

        public ActionResult RequestArchive()
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                var userProfile = gatekeeper.GetCurrentUserProfile();
                
                CodeSigningRequestManager manager = new CodeSigningRequestManager();

                ViewBag.Statuses = UIHelper.GetSelectListForStatuses("-1");
                ViewBag.Requests = manager.GetRequestsForUser(userProfile).ToList();
                return View();
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        public ActionResult RequestArchiveByStatus(int status)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                var userProfile = gatekeeper.GetCurrentUserProfile();

                CodeSigningRequestManager manager = new CodeSigningRequestManager();
                if (status == -1)
                {
                    ViewBag.Requests = manager.GetRequestsForUser(userProfile).ToList();
                }
                else
                {
                    ViewBag.Requests = manager.GetRequestsForUser(userProfile).Where(m => m.StatusId == status).ToList();
                }
                ViewBag.Statuses = UIHelper.GetSelectListForStatuses(status.ToString());
                return View("RequestArchive");
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        public ActionResult RequestArchiveForGroup(int groupId)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                gatekeeper.ValidateUserDeveloperInGroup(groupId);

                var groups = gatekeeper.GetCurrentUserProfile().Groups.Select(upg => upg.Group).AsEnumerable();

                if (groups.Count() > 1)
                {
                    ViewBag.Groups = new SelectList(gatekeeper.GetCurrentUserProfile().Groups.Select(upg => upg.Group).AsEnumerable(), "Id", "Name", groupId);
                }

                CodeSigningRequestManager manager = new CodeSigningRequestManager();

                return View("RequestArchive");
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

    }
}
