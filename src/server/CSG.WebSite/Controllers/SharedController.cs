﻿using System;
using System.Web.Mvc;
using CSG.AppServer.Exceptions;
using CSG.AppServer.Security;

namespace CSG.WebSite.Controllers
{
    public class SharedController : Controller
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        //
        // GET: /Shared/

        public ActionResult Error(Exception e)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            if (e is LoginException)
            {
                ViewBag.ErrorMessage = "You do not appear to be a member of any groups that have rights to the application.  If you believe you have received this message in error, please contact the system administrators.";
            }
            else if (e is AppServerStatusException)
            {
                ViewBag.ErrorMessage = "The application is not currently available.  Please try again later.";
            }
            else if (e is UserRightsException)
            {
                ViewBag.ErrorMessage = "You are not authorized to perform the requested action.";
            }
            else
            {
                if (gatekeeper.IsUserInAdminRole())
                {
                    ViewBag.ErrorMessage = e.Message;
                }
                else
                {
                    ViewBag.ErrorMessage = "An exception was encountered.  The exception has been logged.  Please try the operation again.  If the problem persists, please contact the system administrators.";
                }
            }

            gatekeeper.LogException(e.Message, e);

            return View("Error");
        }
    }
}
