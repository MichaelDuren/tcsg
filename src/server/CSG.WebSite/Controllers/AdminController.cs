using System;
using System.Web.Mvc;
using CSG.AppServer.Security;
using CSG.WebSite.Helpers;
using CSG.AppServer.Managers;
using CSG.AppServer.Exceptions;
using CSG.AppServer.Entities;

namespace CSG.WebSite.Controllers
{
    public class AdminController : Controller
    {
        //
        // GET: /Admin/
        public ActionResult Index()
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                gatekeeper.ValidateUserIsAppAdmin();

                return View();
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        // ********************************************************************************
        #region Active Directory Sync
        // ********************************************************************************

        public ActionResult StartADSync()
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                gatekeeper.ValidateUserIsAppAdmin();

                // log the user activity
                ActivityLog.Log(gatekeeper.GetCurrentUserProfile(),
                                ActivityLog.LogEventId_SYNC_DIRECTORY,
                                "DirectoryUpdate", "", "");

                UserManagerBase userManager = UserManagerFactory.CreateUserManager();
                userManager.SyncAllWithActiveDirectory();

                return RedirectToAction("ConfirmADSync");
            }
            catch (System.Data.Entity.Infrastructure.DbUpdateConcurrencyException cex)
            {
                gatekeeper.LogException(String.Empty, cex);
                return UIHelper.RedirectToError(new Exception("Database Concurrency Exception. Synchorization is occuring in the background task.  Try again in a few minutes."));
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        public ActionResult ConfirmADSync()
        {
            return View();
        }
        public ActionResult StartADUpdate()
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                gatekeeper.ValidateUserIsAppAdmin();

                // log the user activity
                ActivityLog.Log(gatekeeper.GetCurrentUserProfile(),
                                ActivityLog.LogEventId_SYNC_DIRECTORY,
                                "DirectorySync", "", "");

                UserManagerBase userManager = UserManagerFactory.CreateUserManager();
//                userManager.UpdateUserProfilesFromActiveDirectory();

                return RedirectToAction("ConfirmADSync");
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }


        public ActionResult EntityExists()
        {
            ViewBag.Id = TempData["id"];
            ViewBag.Entity = TempData["entity"];

            return View("EntityExists");
        }

        #endregion
    }
}
