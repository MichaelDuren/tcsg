using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Web.Routing;
using CSG.AppServer.Entities;
using CSG.AppServer.Exceptions;
using CSG.AppServer.Managers;
using CSG.AppServer.Security;
using CSG.WebSite.Helpers;
using CSG.WebSite.Models;
using System.Collections.Generic;
using CSG.AppServer;
using CSG.AppServer.Db;

namespace CSG.WebSite.Controllers
{
    public class HomeController : Controller
    {
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly Type ThisDeclaringType = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;

        [AllowAnonymous]
        public ActionResult Index()
        {
            if (Request.IsAuthenticated)
            {
                return Dashboard();
            }
            return View();
        }

        public ActionResult About()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult Contact()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult FAQ()
        {
            string temp = Path.Combine(Server.MapPath(""));
            temp = temp.Replace("Home", @"Content\faq.htm");

            string faqHtml = System.IO.File.ReadAllText(temp);

            ViewBag.FAQHtml = faqHtml;
            return View();
        }

        public class VisibleIcons
        {
            public bool bShowApproveButton = false;
            public bool bShowViewButton = false;
            public bool bShowCancelButton = false;
            public bool bShowOfflineButton = false;
        }

        public ActionResult Dashboard(string message = null)
        {
            Gatekeeper gatekeeper = new Gatekeeper();
            DashboardModel model = new DashboardModel();
            model.Gatekeeper = gatekeeper;
            List<int> approverGroups = new List<int>();

            try
            {
                //Get the signing types for the user
                UserProfile currentUser = gatekeeper.GetCurrentUserProfile(true, true);

                SelectListItem slViewDetails = new SelectListItem { Selected = true, Text = "View Details", Value = "Details" };
                SelectListItem slApproveDeny = new SelectListItem { Selected = false, Text = "Approve or Deny", Value = "Approve" };
                SelectListItem slOfflineSign = new SelectListItem { Selected = true, Text = "Offline Sign", Value = "Offline" };
                SelectListItem slUserCancel = new SelectListItem { Selected = true, Text = "Cancel", Value = "Cancel" };

                var groups = currentUser.Groups
                                        .Select(upg => upg.Group)
                                        .Where(g => g.IsDeleted == false && g.IsDeveloperGroup  == true)
                                        .ToList();

                ViewBag.Groups = UIHelper.GetSelectList(new SelectList(groups, "Id", "Name"));
                model.codeSigningTypeCount = groups.Count();
                                
                //Get the pending code signing request for the user
                CodeSigningRequestManager manager = new CodeSigningRequestManager();

                var pendingRequests = manager.GetPendingCodeSigningRequestsForUser(currentUser);
                model.PendingRequests = pendingRequests.ToList<CodeSigningRequest>();

                foreach (var req in pendingRequests)
                {
                    SelectList tempActions;
                    List<SelectListItem> selectListItems = new List<SelectListItem>();
                    VisibleIcons visibleIcons = new VisibleIcons();

                    selectListItems.Add(slViewDetails);
                    visibleIcons.bShowViewButton = true;
                    // build the list of action menus based on the user's role and the state of the request
                    if (req.Status != AppServer.CodeSigningRequestStatus.Approved &&
                        req.Status != AppServer.CodeSigningRequestStatus.InProcess)
                    {
                        // if this user submitted the request, then only allow deny
                        // note that submitting users cannot see pending requests unless they were
                        // the submitter
                        if (req.CreatedByUserId == currentUser.Id &&
                           (req.Status == CodeSigningRequestStatus.AwaitingOfflineSign ||
                            req.Status == CodeSigningRequestStatus.PartiallyApproved ||
                            req.Status == CodeSigningRequestStatus.AwaitingFile ||
                            req.Status == CodeSigningRequestStatus.Submitted))
                        {
                            selectListItems.Add(slUserCancel);
                            visibleIcons.bShowCancelButton = true;
                            tempActions = new SelectList(selectListItems, "Value", "Text", 1);
                        }
                        else
                        {
                            if (gatekeeper.IsUserApproverForRequest(req))
                            {
                                if (currentUser.Groups.Select(u => u.GroupId).Contains(req.GroupId))
                                {
                                    if (req.Approver1ResponderId == null)
                                    {
                                        visibleIcons.bShowApproveButton = true;
                                    }
                                }
                                else
                                {
                                    if (req.Approver2ResponderId == null)
                                    {
                                        visibleIcons.bShowApproveButton = true;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {

                    }

                    tempActions = new SelectList(selectListItems, "Value", "Text", 1);
                    ViewData.Add(req.Id.ToString(), visibleIcons);
                }

                var recentRequests = manager.GetRecentlyCompletedCodeSigningRequestsForUser(currentUser);
                if (recentRequests != null)
                {  
                    model.RecentlyCompletedRequests = recentRequests.ToList<CodeSigningRequest>();
                }
                //Set the error message, if any
                if (!String.IsNullOrWhiteSpace(message))
                {
                    ModelState.AddModelError("", message);
                }

                ESPDbDomainService svc = new ESPDbDomainService();
                // System setting ServerName id = 68
                ViewBag.ServerName=svc.GetSystemSettingById(68).StringValue;
            }
            catch (LoginException)
            {
                return RedirectToAction("LogIn", "Account");
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }

            return View("Dashboard", model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ProcessCodeSigningRequest(FormCollection collection)
        {
            string requestIdString = collection["ActionRequestId"];
            string ddlControlIdForRequest = "ddlForRequest" + requestIdString;
            string actionText = collection["hdnActionRequest"];

            int requestId = System.Convert.ToInt32(requestIdString);
            if (requestId > 0)
            {
                RouteValueDictionary rvd = new RouteValueDictionary();
                switch (actionText)
                {
                    case "Approve":
                        rvd.Add("requestId", requestId);
                        // Verify with Gatekeeper and then move to approval page
                        return RedirectToAction("Approve", "CodeSigningRequest", rvd);
                    case "Details":
                        rvd.Add("requestId", requestId);
                        // Verify with Gatekeeper and then move to approval page
                        return RedirectToAction("Details", "CodeSigningRequest", rvd);
                    case "Offline":
                        rvd.Add("requestId", requestId);
                        rvd.Add("isFromDashboard", "true");
                        rvd.Add("offline", "true");
                        // Verify with Gatekeeper and then move to approval page
                        return RedirectToAction("Details", "CodeSigningRequest", rvd);
                }
            }
            return Dashboard();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CreateCodeSigningRequest(FormCollection collection)
        {
            string temp = collection["ddlGroups"];

            if (!String.IsNullOrWhiteSpace(temp))
            {
                int groupId = System.Convert.ToInt32(temp);
                if (groupId > 0)
                {
                    RouteValueDictionary rvd = new RouteValueDictionary();
                    rvd.Add("groupId", groupId);
                    return RedirectToAction("Create", "CodeSigningRequest", rvd);
                }
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError("", "The development group must be selected before proceeding.");

            return Dashboard();
        }

        public JsonResult DenyRequest(int requestId, string responderNote)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                // if you can view the request, you are the submitting user or you are an approver
                gatekeeper.ValidateUserViewRequest(requestId);
                gatekeeper.LogUserActivity("Cancel Code Signing Request");

                CodeSigningRequestManager manager = new CodeSigningRequestManager();
                var request = manager.DenyCodeSigningRequest(requestId, responderNote);


                if (request.CreatedByUserId == gatekeeper.GetCurrentUserProfile().Id)
                {
                    gatekeeper.LogUserActivity("Code Signing Request Denied");
                }
                else
                {
                    gatekeeper.LogUserActivity("Code Signing Request Canceled");
                }

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ApproveRequest(int requestId)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                gatekeeper.ValidateUserApproverInRequest(requestId);
                gatekeeper.LogUserActivity("Code Signing Request Approved");

                CodeSigningRequestManager manager = new CodeSigningRequestManager();
                manager.ApproveCodeSigningRequest(requestId);

                return Json(true, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                gatekeeper.LogException("Exception occurred attempting to approve the request.", e);

                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult Error(string message = null)
        {
            ViewBag.AdminEmail = AppServerContext.TeamEmailAddress;
            return View();
        }
    }
}
