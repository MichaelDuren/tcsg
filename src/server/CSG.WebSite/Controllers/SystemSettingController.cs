﻿using System;
using System.Linq;
using System.Web.Mvc;
using CSG.AppServer.Db;
using CSG.AppServer.Entities;
using CSG.AppServer.Managers;
using CSG.AppServer.Security;
using CSG.WebSite.Helpers;
using CSG.WebSite.Models;

namespace CSG.WebSite.Controllers
{
    public class SystemSettingController : Controller
    {
        public ActionResult DoMaintenance()
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                gatekeeper.ValidateUserIsAppAdmin();

                ESPDbDomainService svc = new ESPDbDomainService();
                ViewBag.Settings = svc.GetSystemSettings(null).Where(s => s.ExcludeFromSettingsUI == false).OrderBy(s => s.Name);

                return View();
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        /// <summary>
        /// Save the group
        /// </summary>
        [HttpPost]
        public ActionResult DoMaintenance(FormCollection collection)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                gatekeeper.ValidateUserIsAppAdmin();

                int ageOfFilesToDelete = System.Convert.ToInt32(collection["txtAgeOfFiles"]);
                ESPDbDomainService svc = new ESPDbDomainService();
                CodeSigningRequestManager mgr = new CodeSigningRequestManager();
                var recordsDeleted = mgr.ArchiveRequests(ageOfFilesToDelete);
                ViewBag.RecordsCount = recordsDeleted.ToString();

                // log the user activity
                ActivityLog.Log(gatekeeper.GetCurrentUserProfile(),
                                ActivityLog.LogEventId_MAINTENANCE,
                                "Maintenance", "FlushedFiles",
                                ageOfFilesToDelete.ToString(), recordsDeleted.ToString());


                return View("ConfirmMaintenance", recordsDeleted);
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        public ActionResult ManageSystemSettings()
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                gatekeeper.ValidateUserIsAppAdmin();

                ESPDbDomainService svc = new ESPDbDomainService();
                ViewBag.Settings = svc.GetSystemSettings(null).Where(s => s.ExcludeFromSettingsUI == false).OrderBy(s => s.Name);

                return View();
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        public ActionResult ManageSystemSetting(int settingId, string errorMessage=null)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                gatekeeper.ValidateUserIsAppAdmin();

                ESPDbDomainService svc = new ESPDbDomainService();
                SystemSetting setting = new SystemSetting();

                if (settingId > 0)
                {
                    setting = svc.GetSystemSettingById(settingId);
                    ViewBag.DataTypes = UIHelper.GetSelectListForDataTypes(setting.DataTypeId);
                }
                ViewBag.ErrorMessage = errorMessage;
                return View(setting);
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        /// <summary>
        /// Save the group
        /// </summary>
        [HttpPost]
        public ActionResult SaveSetting(SystemSetting setting)
        {
            SystemSetting oldSetting = setting;
            Gatekeeper gatekeeper = new Gatekeeper();
            try
            {
                gatekeeper.ValidateUserIsAppAdmin();

                oldSetting = SystemSettingsManager.Current.GetBySettingName(setting.Name);
                if (oldSetting == null)
                {
                    var e = new Exception("Setting cannot be changed.");
                    return UIHelper.RedirectToError(e);
                }

                // log the user activity
                ActivityLog.Log(gatekeeper.GetCurrentUserProfile(),
                                ActivityLog.LogEventId_CHANGE_SYSTEM_SETTING,
                                "ManageSystemSetting", "ChangeSetting",
                                setting.Name, oldSetting.Value, setting.Value);

                //Save the updates
                if (oldSetting.Id < 1)
                {
                    var e = new Exception("Setting cannot be created in this application.");
                    return UIHelper.RedirectToError(e);
                }
                else
                {
                    SystemSettingsManager.Current.UpdateSystemSetting(setting);
                    System.Web.HttpContext.Current.Cache.Remove("plist");
                    System.Web.HttpContext.Current.Cache.Remove("rlist");
                }

                return RedirectToAction("ManageSystemSettings");
            }
            catch (InvalidSettingValue)
            {
                return RedirectToAction("ManageSystemSetting", new { @settingId = oldSetting.Id,
                                                                     @errorMessage = "Setting value is invalid." });
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
            
        }
    }
}
