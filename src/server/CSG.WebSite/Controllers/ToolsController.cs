﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CSG.AppServer.Db;
using CSG.AppServer.Entities;
using CSG.AppServer.Managers;
using CSG.AppServer.Security;
using CSG.WebSite.Helpers;

namespace CSG.WebSite.Controllers
{
    public class ToolsController : Controller
    {
        public ActionResult ManageTools()
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                gatekeeper.ValidateUserIsAppAdmin();

                ESPDbDomainService svc = new ESPDbDomainService();
                //GroupsModel model = new GroupsModel();
                ViewBag.Groups = svc.GetGroups(null);

                return View();
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        public ActionResult ManageTool(int toolId)
        {
            return View();
        }

        /// <summary>
        /// Save the group
        /// </summary>
        [HttpPost]
        public ActionResult SaveTool(CodeSigningTool tool)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {

                gatekeeper.ValidateUserIsAppAdmin();
                gatekeeper.LogUserActivity("Code Signing Tool Update");

                SigningToolManager manager = new SigningToolManager();
                manager.SaveCodeSigningTool(tool);
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }

            return RedirectToAction("ManageSigningTools");
        }

        public ActionResult ToolExists(int toolId)
        {
            return RedirectToAction("EntityExists", "Admin");
        }

        public ActionResult ConfirmToolUpdate()
        {
            return View();
        }
    }
}
