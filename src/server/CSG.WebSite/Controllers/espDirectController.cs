using System;
using System.Web.Http;
using System.Collections.Specialized;
using System.Web;
using System.Net.Http;
using System.Net;
using System.IO;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Net.Http.Headers;
using System.Linq;
using System.Text;

using CSG.AppServer.Exceptions;
using CSG.AppServer;
using CSG.AppServer.Managers;
using CSG.AppServer.APITypes;
using CSG.AppServer.Security;
using CSG.WebSite.Filters;
using CSG.AppServer.Entities;
using Util.Config;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Newtonsoft.Json;
using static CSG.AppServer.Managers.APIManager;

namespace CSG.WebSite.Controllers
{

    public class MultipartFormDataMemoryStreamProvider : MultipartMemoryStreamProvider
    {
        private readonly Collection<bool> _isFormData = new Collection<bool>();
        private readonly NameValueCollection _formData = new NameValueCollection(StringComparer.OrdinalIgnoreCase);
        private readonly List<string> _dataStreams = new List<string>();
        private readonly Dictionary<string, string> _files = new Dictionary<string, string>();

        public List<string> FormData
        {
            get { return _dataStreams; }
        }

        public Dictionary<string, string> Files
        {
            get { return _files; }
        }

        public override Stream GetStream(HttpContent parent, HttpContentHeaders headers)
        {
            if (parent == null)
            {
                throw new ArgumentNullException("parent");
            }

            if (headers == null)
            {
                throw new ArgumentNullException("headers");
            }

            var contentDisposition = headers.ContentDisposition;
            if (contentDisposition == null)
            {
                // treat this as form data
                _isFormData.Add(true);
            }
            else
            {
                _isFormData.Add(String.IsNullOrEmpty(contentDisposition.FileName));
            }

            return base.GetStream(parent, headers);
        }

        public override async Task ExecutePostProcessingAsync()
        {
            for (var index = 0; index < Contents.Count; index++)
            {
                HttpContent formContent = Contents[index];
                if (_isFormData[index])
                {
                    var formData = await formContent.ReadAsStringAsync();
                    FormData.Add(formData);
                }
                else
                {
                    var uniqueFileName = string.Format(@"{0}.txt", Guid.NewGuid());

                    string tempPath = AppServerContext.LocalUploadFolder;
                    if (!Directory.Exists(tempPath))
                    {
                        Directory.CreateDirectory(tempPath);
                    }

                    string fileName = UnquoteToken(formContent.Headers.ContentDisposition.FileName);
                    string tempFile = Path.Combine(tempPath, uniqueFileName);

                    var fileStream = File.Create(tempFile);
                    await formContent.CopyToAsync(fileStream);
                    fileStream.Close();

                    Files.Add(fileName, tempFile);
                }
            }
        }

        private static string UnquoteToken(string token)
        {
            if (string.IsNullOrWhiteSpace(token))
            {
                return token;
            }

            if (token.StartsWith("\"", StringComparison.Ordinal) && token.EndsWith("\"", StringComparison.Ordinal) && token.Length > 1)
            {
                return token.Substring(1, token.Length - 2);
            }

            return token;
        }
    }
    public class csgAPIController : ApiController
    {
        private const string qsSUBJECT = "SUBJECT";
        private const string qsRequestId = "requestId";

        // ******************************************************************************
        #region Properties / Attributes
        // ******************************************************************************
        private static readonly ESPLogWrapper.ILog log = ESPLogWrapper.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private static readonly Type ThisDeclaringType = System.Reflection.MethodBase.GetCurrentMethod().DeclaringType;
        #endregion

        public csgAPIController()
        {
            
        }
        private APIManager preProcessAPIRequest(string apiCall, int requestId = -1)
        {
            AuthenticationHeaderValue authorization = Request.Headers.Authorization;

            if (authorization == null)
            {
                // No authentication was attempted (for this authentication method).
                // Do not set either Principal (which would indicate success) or ErrorResult (indicating an error).
                return null;
            }

            if (authorization.Scheme != "Basic")
            {
                // No authentication was attempted (for this authentication method).
                // Do not set either Principal (which would indicate success) or ErrorResult (indicating an error).
                return null;
            }

            if (String.IsNullOrEmpty(authorization.Parameter))
            {
                // Authentication was attempted but failed. Set ErrorResult to indicate an error.
                return null;
            }

            Tuple<string, string> userNameAndPasword = ExtractUserNameAndPassword(authorization.Parameter);

            if (userNameAndPasword == null)
            {
                // Authentication was attempted but failed. Set ErrorResult to indicate an error.
                return null;
            }

            Gatekeeper gatekeeper;
            string username = userNameAndPasword.Item1;
            string password = userNameAndPasword.Item2;

            // get the domain name from the userName string
            int indexSlash = username.IndexOf("\\");
            String domain = (indexSlash > -1) ? username.Substring(0, indexSlash) : String.Empty;

            if (domain.Length == 0)
            {
                domain = DynamicConfigurationManager.AppSettings[AppServer.Constants.AppConfigNames.DefaultDomain];
            }
            else
            {
                // get the login Id Value
                indexSlash = username.IndexOf("\\");
                username = (indexSlash > -1) ? username.Substring(indexSlash + 1, username.Length - indexSlash - 1) : username;
            }

            // access the query string using name value pairs here
            NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);

            try
            {
                UserManagerBase userManager = UserManagerFactory.CreateUserManager();
                if (userManager.AuthenticateUser(domain, username, password))
                {
                    var userProfile = userManager.GetCurrentUserProfile();
                    if (userProfile == null)
                    {
                        return null;
                    }

                    gatekeeper = new Gatekeeper(userProfile);

                    ActivityLog.Log(userProfile.DomainAndLoginId.ToString(), userProfile.DomainAndLoginId.LoginId, "csgAPI", "apiCall");

                    APIManager apiMgr = new APIManager(gatekeeper);

                    return apiMgr;
                }
                else
                {
                    return null;
                }
            }
            catch (LoginException lex)
            {
                log.Error("User is not logged in or has not been authenticated. " + lex.Message);
                return null;
            }
            catch (UserRightsException urex)
            {
                log.Error("User does not have access to this resource. " + urex.Message);
                return null;
            }
            catch (Exception ex)
            {
                log.Error("User does not have access to this resource. " + ex.Message);
                ActivityLog.Log("CSG API ERROR", "unknown", "csgAPI", "codesignRequestCreate");
                throw;
            }
        }

        private static Tuple<string, string> ExtractUserNameAndPassword(string authorizationParameter)
        {
            byte[] credentialBytes;

            try
            {
                credentialBytes = Convert.FromBase64String(authorizationParameter);
            }
            catch (FormatException)
            {
                return null;
            }

            // The currently approved HTTP 1.1 specification says characters here are ISO-8859-1.
            // However, the current draft updated specification for HTTP 1.1 indicates this encoding is infrequently
            // used in practice and defines behavior only for ASCII.
            Encoding encoding = Encoding.ASCII;
            // Make a writable copy of the encoding to enable setting a decoder fallback.
            encoding = (Encoding)encoding.Clone();
            // Fail on invalid bytes rather than silently replacing and continuing.
            encoding.DecoderFallback = DecoderFallback.ExceptionFallback;
            string decodedCredentials;

            try
            {
                decodedCredentials = encoding.GetString(credentialBytes);
            }
            catch (DecoderFallbackException)
            {
                return null;
            }

            if (String.IsNullOrEmpty(decodedCredentials))
            {
                return null;
            }

            int colonIndex = decodedCredentials.IndexOf(':');

            if (colonIndex == -1)
            {
                return null;
            }

            string userName = decodedCredentials.Substring(0, colonIndex);
            string password = decodedCredentials.Substring(colonIndex + 1);
            return new Tuple<string, string>(userName, password);
        }

        [Route("listSigningProfiles")]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage listSigningProfiles()
        {
            HttpResponseMessage response = new HttpResponseMessage();

            if (AppServerContext.DisableWebAPI)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden);
            }

            try
            {
                APIManager apiMgr = preProcessAPIRequest("listSigningProfiles");
                if (apiMgr == null)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized);
                }
                response.Content = new ObjectContent<APIListSigningTypes>(apiMgr.listSigningTypes(),
                                                                            new JsonMediaTypeFormatter(),
                                                                            "application/json");
                response.StatusCode = HttpStatusCode.OK;
                return response;

            }
            catch (Exception ex)
            {
                ActivityLog.Log("CSG API ERROR", "unknown", "csgAPI", "listSigningProfiles", "", "", "", "", null, "", ex);
                response.Content = new ObjectContent<APICodesignRequestStatus>(new APICodesignRequestStatus()
                { returnCode = new csgAPIErrorInfo() { errorCode = APIManager.WEBAPIERROR_INTERNAL_ERROR, errorString = "Unknown error occurred" }, request = null },
                                                                        new JsonMediaTypeFormatter(),
                                                                        "application/json");
                response.StatusCode = HttpStatusCode.OK;
                return response;
            }
        }

        [HttpPost]
        public async Task<HttpResponseMessage> createRequest()
        {
            codesignRequestCreateParams requestParams = null;
            if (AppServerContext.DisableWebAPI)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden);
            }

            // we need to access the query string, use name value pairs here
            NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);
            HttpResponseMessage response = new HttpResponseMessage();

            try
            {
                APIManager apiMgr = preProcessAPIRequest("codesignRequestCreate");
                if (apiMgr == null)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized);
                }

                if (!Request.Content.IsMimeMultipartContent())
                {
                    return Request.CreateResponse(HttpStatusCode.UnsupportedMediaType);
                }

                string tempPath = AppServerContext.LocalUploadFolder;
                if (!Directory.Exists(tempPath))
                {
                    Directory.CreateDirectory(tempPath);
                }

                // read the content using a custom multipart stream provider that recognizes the content
                MultipartFormDataMemoryStreamProvider provider = new MultipartFormDataMemoryStreamProvider();
                await Request.Content.ReadAsMultipartAsync(provider);

                // assume there is one form data part in the message by taking the first one
                var requestData = provider.FormData.FirstOrDefault();
                if (requestData != null)
                {
                    requestParams = JsonConvert.DeserializeObject<codesignRequestCreateParams>(requestData);
                }

                // if there are no parameters, then return an error
                if (requestParams == null)
                {
                    var invalidRequestError = new csgAPIErrorInfo() { errorCode = APIManager.WEBAPIERROR_INVALID_REQUEST, errorString = "The request is invalid." };
                    response.Content = new ObjectContent<APICodesignRequestStatus>(new APICodesignRequestStatus() { request = null, returnCode = invalidRequestError },
                                                                              new JsonMediaTypeFormatter(),
                                                                              "application/json");
                    response.StatusCode = HttpStatusCode.OK;
                    return response;
                }

                if (provider.Files.Count == 0)
                {
                    log.Error("Request file count is zero.");
                    var invalidRequestError = new csgAPIErrorInfo() { errorCode = APIManager.WEBAPIERROR_INVALID_REQUEST, errorString = "The request is invalid." };
                    response.Content = new ObjectContent<APICodesignRequestStatus>(new APICodesignRequestStatus() { request = null, returnCode = invalidRequestError },
                                                                              new JsonMediaTypeFormatter(),
                                                                              "application/json");
                    response.StatusCode = HttpStatusCode.OK;

                }
                else
                {
                    // generate a temporary filename until for the upload, this will be renamed in the API manager based on the 
                    foreach (KeyValuePair<string, string> file in provider.Files)
                    {
                        APICodesignRequestStatus responseMessage;

                        // process the request with the api manager
                        responseMessage = apiMgr.codesignRequestCreateWithFile(requestParams, file.Value, file.Key);
                        if (responseMessage != null)
                        {
                            response.Content = new ObjectContent<APICodesignRequestStatus>(responseMessage,
                                                                                    new JsonMediaTypeFormatter(),
                                                                                    "application/json");
                            response.StatusCode = HttpStatusCode.OK;
                        }
                        else
                        {
                            response.StatusCode = HttpStatusCode.InternalServerError;
                        }
                    }
                }
                return response;

            }
            catch (InvalidRequestException)
            {
                response.Content = new ObjectContent<APICodesignRequestStatus>(new APICodesignRequestStatus()
                { returnCode = new csgAPIErrorInfo() { errorCode = APIManager.WEBAPIERROR_INVALID_REQUEST, errorString = "Invalid Request" }, request = null },
                                                                        new JsonMediaTypeFormatter(),
                                                                        "application/json");
                response.StatusCode = HttpStatusCode.OK;
                return response;
            }
            catch (FileHashMismatchException)
            {
                response.Content = new ObjectContent<APICodesignRequestStatus>(new APICodesignRequestStatus()
                { returnCode = new csgAPIErrorInfo() { errorCode = APIManager.WEBAPIERROR_HASH_MISMATCH, errorString = "Uploaded file hash does not match file" }, request = null },
                                                                        new JsonMediaTypeFormatter(),
                                                                        "application/json");
                response.StatusCode = HttpStatusCode.OK;
                return response;
            }
            catch (Exception ex)
            {
                ActivityLog.Log("CSG API ERROR", "unknown", "csgAPI", "codesignRequestCreate", ex.ToString());
                response.Content = new ObjectContent<APICodesignRequestStatus>(new APICodesignRequestStatus()
                { returnCode = new csgAPIErrorInfo() { errorCode = APIManager.WEBAPIERROR_INTERNAL_ERROR, errorString = "Unknown error occurred" }, request = null },
                                                                        new JsonMediaTypeFormatter(),
                                                                        "application/json");
                response.StatusCode = HttpStatusCode.OK;
                return response;
            }

        }


        [HttpGet]
        public HttpResponseMessage getRequestStatus()
        {
            HttpResponseMessage response = new HttpResponseMessage();
            if (AppServerContext.DisableWebAPI)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden);
            }

            // we need to access the query string, use name value pairs here
            NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);
            try
            {
                APIManager apiMgr = preProcessAPIRequest("codesignRequestStatus");
                if (apiMgr == null)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized);
                }
                List<APICodesignRequestStatus> statuses = new List<APICodesignRequestStatus>();

                List<int> requestIds = nvc["requestId"].Split(',').Select(int.Parse).ToList();

                foreach (var requestId in requestIds)
                {
                    APICodesignRequestStatus requestStatus;

                    try
                    {
                        requestStatus = apiMgr.codesignRequestStatus(requestId);
                    }
                    catch
                    {
                        requestStatus = new APICodesignRequestStatus() { returnCode = new csgAPIErrorInfo() { errorCode = APIManager.WEBAPIERROR_INVALID_REQUEST, errorString = "Unknown error occured." }, request = null };
                    }
                    statuses.Add(requestStatus);
                }
                response.Content = new ObjectContent<List<APICodesignRequestStatus>>(statuses,
                                                                          new JsonMediaTypeFormatter(),
                                                                          "application/json");
                response.StatusCode = HttpStatusCode.OK;
                return response;

            }
            catch (Exception ex)
            {
                ActivityLog.Log("CSG API ERROR", "unknown", "csgAPI", "codesignRequestStatus", "", "", "", "", null, "", ex);
                response.Content = new ObjectContent<APICodesignRequestStatus>(new APICodesignRequestStatus()
                { returnCode = new csgAPIErrorInfo() { errorCode = APIManager.WEBAPIERROR_INTERNAL_ERROR, errorString = "Unknown error occurred" }, request = null },
                                                                        new JsonMediaTypeFormatter(),
                                                                        "application/json");
                response.StatusCode = HttpStatusCode.OK;
                return response;
            }
        }
        [HttpGet]
        public HttpResponseMessage downloadFile()
        {
            HttpResponseMessage response = new HttpResponseMessage();

            if (AppServerContext.DisableWebAPI)
            {
                return Request.CreateResponse(HttpStatusCode.Forbidden);
            }

            // we need to access the query string, use name value pairs here
            NameValueCollection nvc = HttpUtility.ParseQueryString(Request.RequestUri.Query);
            try
            {
                int requestId = Convert.ToInt32(nvc["requestId"]);
                APIManager apiMgr = preProcessAPIRequest("codesignRequestDownloadFile", requestId);
                if (apiMgr == null)
                {
                    return Request.CreateResponse(HttpStatusCode.Unauthorized);
                }
                // Verify that this is a legitimate Id before accepting the file, will throw an exception if invalid
                var file = apiMgr.codesignRequestDownloadFile(requestId);

                if (file == null)
                {
                    response.StatusCode = HttpStatusCode.BadRequest;
                }
                else
                {
                    var stream = new FileStream(file, FileMode.Open);
                    response.StatusCode = HttpStatusCode.OK;
                    response.Content = new StreamContent(stream);
                    response.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                    response.Content.Headers.ContentDisposition.FileName = Path.GetFileName(file);
                    response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    response.Content.Headers.ContentLength = stream.Length;
                }
                return response;

            }
            catch (Exception ex)
            {
                ActivityLog.Log("CSG API ERROR", "unknown", "csgAPI", "codesignRequestDownloadFile", "", "", "", "", null, "", ex);
                response.Content = new ObjectContent<APICodesignRequestStatus>(new APICodesignRequestStatus()
                { returnCode = new csgAPIErrorInfo() { errorCode = APIManager.WEBAPIERROR_INTERNAL_ERROR, errorString = "Unknown error occurred" }, request = null },
                                                                        new JsonMediaTypeFormatter(),
                                                                        "application/json");
                response.StatusCode = HttpStatusCode.OK;
                return response;
            }
        }        
    }
}
