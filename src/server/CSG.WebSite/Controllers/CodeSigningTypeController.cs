﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CSG.AppServer.Db;
using CSG.AppServer.Entities;
using CSG.AppServer.Managers;
using CSG.AppServer.Security;
using CSG.WebSite.Helpers;
using CSG.WebSite.Models;
using CSG.AppServer;

namespace CSG.WebSite.Controllers
{
    public class CodeSigningTypeController : Controller
    {
        public ActionResult ManageCodeSigningTypes(string savedMessage = null)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                gatekeeper.ValidateUserIsAppAdmin();

                ESPDbDomainService svc = new ESPDbDomainService();

                var csts = svc.GetCodeSigningTypes().Where(cst => cst.IsDeleted == false).OrderBy(m => m.Name).ToList();
                ViewBag.CodeSigningTypes = csts;
                csts = svc.GetCodeSigningTypes().Where(cst => cst.IsDeleted == true).ToList();
                ViewBag.DeletedCodeSigningTypes = csts;
                ViewBag.SavedMessage = savedMessage;
                return View();
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }
        public ActionResult ManageCodeSigningType(int typeId)
        {
            Gatekeeper gatekeeper = new Gatekeeper();
            SelectListItem slNone = new SelectListItem { Selected = false, Text = "None", Value = "N" };
            SelectListItem slManagerOnly = new SelectListItem { Selected = true, Text = "Second Developer", Value = "D" };
            SelectListItem slApproverOnly = new SelectListItem { Selected = false, Text = "Approver Only", Value = "A" };
            SelectListItem slManagerOrApprover = new SelectListItem { Selected = false, Text = "Developer OR Approver", Value = "DorA" };
            SelectListItem slManagerAndApprover = new SelectListItem { Selected = false, Text = "Developer AND Approver", Value = "DandA" };
            List<SelectListItem> selectListItems = new List<SelectListItem>() { slManagerOnly, slApproverOnly, slManagerOrApprover, slManagerAndApprover, slNone };

            try
            {
                gatekeeper.ValidateUserIsAppAdmin();

                ESPDbDomainService svc = new ESPDbDomainService();
                CodeSigningType type = new CodeSigningType();

                // we need the list of code signing tools for the drop box
                var signingTools = svc.GetCodeSigningTools().ToList();
                var tools = new SelectList(signingTools, "Id", "Name");
                if (tools.Count() == 0)
                {
                    throw new Exception("There are no Signing Tools configured in the system.");
                }
                ViewBag.CodeSigningTools = tools;

                // get the list of tools that are macro signing tools
                var offlineMap = signingTools.Where(st => st.Offline).Select(st => new KeyValuePair<int, bool>(st.Id, st.Offline)).ToList();
                ViewBag.OfflineMap = offlineMap;

                var toolStatus = svc.GetCodeSigningTools().Select(m => new { m.Id, m.Offline }).ToDictionary( m => m.Id, m => m.Offline);
                ViewBag.ToolStatus = toolStatus;

                ViewBag.GESMessage = "The identifier used in GES to control access to this signing type.";

                // get the constraining roles (groups) for the profiles
                SystemSettingsManager ssm = SystemSettingsManager.Current;
                List<string> gesRoleList = null;
                var gesRoleListString = ssm.GetBySettingName(Constants.SettingNames.GESProfileRoleList);
                if (!String.IsNullOrEmpty(gesRoleListString.StringValue))
                {
                    gesRoleList = new List<string>(gesRoleListString.StringValue.Split(',').Select(p => p.Trim()).ToList());

                }


                if (typeId > 0)
                {
                    var selected = "D";

                    type = svc.GetCodeSigningTypeById(typeId);

                    IEnumerable<int> cstgs = svc.GetCodeSigningTypeGroups(null)
                                                                    .Where(m => m.CodeSigningTypeId == typeId)
                                                                    .Select(m => m.GroupId);

                    IQueryable<Group> groups = svc.GetGroups(null).Where(m => cstgs.Contains(m.Id) && m.IsDeveloperGroup == true && m.IsDeleted == false)
                                              .OrderBy(m => m.Name);
                    ViewBag.SelectedDeveloperGroups = new SelectList(groups, "Id", "Name", null);
                    foreach (Group group in groups)
                    {
                        ViewBag.DelimitedDeveloperGroups = ViewBag.DelimitedGroups + "," + group.Id;
                    }

                    groups = svc.GetGroups(null).Where(m => cstgs.Contains(m.Id) && m.IsApproverGroup == true && m.IsDeleted == false)
                                              .OrderBy(m => m.Name);
                    ViewBag.SelectedApproverGroups = new SelectList(groups, "Id", "Name", null);
                    foreach (Group group in groups)
                    {
                        ViewBag.DelimitedApproverGroups = ViewBag.DelimitedGroups + "," + group.Id;
                    }

                    IQueryable<Group> unselectedGroups;
                    unselectedGroups = svc.GetGroups(null).Where(m => m.IsDeleted == false && m.IsDeveloperGroup == true && !cstgs.Contains(m.Id)).OrderBy(m => m.Name);
                    ViewBag.RemainingDeveloperGroups = new SelectList(unselectedGroups, "Id", "Name", null);
                    unselectedGroups = svc.GetGroups(null).Where(m => m.IsDeleted == false && m.IsApproverGroup == true && !cstgs.Contains(m.Id)).OrderBy(m => m.Name);
                    ViewBag.RemainingApproverGroups = new SelectList(unselectedGroups, "Id", "Name", null);


                    //Certificates
                    CertificateManager certMgr = new CertificateManager();
                    var certsInDb = certMgr.GetCertificates().Where(m => m.IsDeleted == false || 
                                                                    m.Id == type.SigningCertificateId)
                                                             .OrderBy(m => m.Name)
                                                             .ToList();
                    // if this type has an expired cert, then tell the GUI and add cert
                    if (!type.SigningCertificate.InValidityPeriod)
                    {
                        type.SigningCertificate.Name = "(EXPIRED)" + type.SigningCertificate.Name;
                        certsInDb.Add(type.SigningCertificate);
                    }
                    else
                    {
                    }
                    if (certsInDb.Count() == 0)
                    {
                        throw new Exception("There are no valid Certificates configured in the system, please add one or more certificates before configuring a Profile.");
                    }
                    ViewBag.CodeSigningCertificates = new SelectList(certsInDb, "Id", "Name");

                    if (type.IsAutoSign)
                    {
                        selected = "N";
                    }
                    else if (type.RequireApprover1Approval && type.RequireApprover2Approval)
                    {
                        selected = "DandA";
                    }
                    else if (type.RequireApprover1Approval)
                    {
                    }
                    else if (type.RequireApprover2Approval)
                    {
                        selected = "A"; 
                    }
                    else
                    {
                        selected = "DorA"; 
                    }

                    var slActions = new SelectList(selectListItems, "Value", "Text", selected);
                    ViewData.Add("approvalRequirements", slActions);
                }
                else
                {

                    IEnumerable<int> cstgs = svc.GetCodeSigningTypeGroups(null)
                                                                    .Where(m => m.CodeSigningTypeId == -1)
                                                                    .Select(m => m.GroupId);

                    type = new CodeSigningType();
                    ViewBag.SelectedDeveloperGroups = new SelectList(svc.GetGroups(null).Where(m => cstgs.Contains(m.Id) && m.IsDeleted == false && m.IsDeveloperGroup == true), 
                                                            "Id", "Name", null);
                    ViewBag.SelectedApproverGroups = new SelectList(svc.GetGroups(null).Where(m => cstgs.Contains(m.Id) && m.IsDeleted == false && m.IsDeveloperGroup == true),
                                                            "Id", "Name", null);


                    IQueryable<Group> unselectedGroups;
                    unselectedGroups = svc.GetGroups(null).Where(m => m.IsDeleted == false && m.IsDeveloperGroup == true && !cstgs.Contains(m.Id)).OrderBy(m => m.Name);
                    ViewBag.RemainingDeveloperGroups = new SelectList(unselectedGroups, "Id", "Name", null);
                    unselectedGroups = svc.GetGroups(null).Where(m => m.IsDeleted == false && m.IsApproverGroup == true && !cstgs.Contains(m.Id)).OrderBy(m => m.Name);
                    ViewBag.RemainingApproverGroups = new SelectList(unselectedGroups, "Id", "Name", null);

                    //Certificates
                    CertificateManager certMgr = new CertificateManager();
                    var certsInDb = certMgr.GetCertificates().Where(m => m.IsDeleted == false);
                    if (certsInDb.Count() == 0)
                    {
                        throw new Exception("There are no Certificates configured in the system, please add one or more certificates before configuring a Profile.");
                    }
                    ViewBag.CodeSigningCertificates = new SelectList(certsInDb, "Id", "Name");

                    var slActions = new SelectList(selectListItems, "Value", "Text", "D");
                    ViewData.Add("approvalRequirements", slActions);
                    type.ApplyToAll = true;
                }

                var typeModel = new CodeSigningTypeModel();
                typeModel.CopyTo(type);
                return View(typeModel);
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        public JsonResult IsMacroSigningTool(int toolId)
        {
            Gatekeeper gatekeeper = new Gatekeeper();
            bool bAnswer = false;
            try
            {
                gatekeeper.ValidateUserIsAppAdmin();

                ESPDbDomainService svc = new ESPDbDomainService();
                var cst = svc.GetCodeSigningToolById(toolId);
                if (cst != null && cst.Offline == true)
                {
                    bAnswer = true;
                }

                var response = Json(bAnswer, JsonRequestBehavior.AllowGet);

                return response;
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveCodeSigningType(CodeSigningTypeModel csTypeModel, FormCollection collection)
        {
            Gatekeeper gatekeeper = new Gatekeeper();
            string approverText = collection["ddlApprovalReqs"];
            string gesResourceId = collection["ddlGESResourceId"];

            try
            {
                gatekeeper.ValidateUserIsAppAdmin();
                // log the user activity
                ActivityLog.Log(gatekeeper.GetCurrentUserProfile(),
                                ActivityLog.LogEventId_SAVE_PROFILE,
                                "CodeSigningProfile", "SaveProfile",
                                csTypeModel.Name);

                CodeSigningType csType = csTypeModel.CopyFrom();

                // always allow enterprise approval
                csType.AllowApprover2Approval = true;
                // default to none autosign
                csType.IsAutoSign = false;
                if (approverText == "N")
                {
                    csType.IsAutoSign = true;
                }
                if (approverText == "DandA")
                {
                    csType.RequireApprover1Approval = true;
                    csType.RequireApprover2Approval = true;
                    csType.AllowApprover2Approval = true;
                    csType.AllowApprover1Approval = true;
                }
                else if (approverText == "D")
                {
                    csType.RequireApprover1Approval = true;
                    csType.AllowApprover1Approval = true;

                    csType.RequireApprover2Approval = false;
                    csType.AllowApprover2Approval = false;
                }
                else if (approverText == "A")
                {
                    csType.RequireApprover1Approval = false;
                    csType.AllowApprover1Approval = false;

                    csType.RequireApprover2Approval = true;
                    csType.AllowApprover2Approval = true;
                }
                else 
                {
                    csType.AllowApprover2Approval = true;
                    csType.RequireApprover1Approval = false;

                    csType.AllowApprover1Approval = true;
                    csType.RequireApprover1Approval = false;
                }

                csType.StagingDirectory = "UNUSED";
                csType.SignedDirectory = "UNUSED";

                string selectedDeveloperGroups = Request.Form["hdnSelectedDeveloperGroups"];
                if (selectedDeveloperGroups != null)
                {
                    selectedDeveloperGroups = selectedDeveloperGroups.Replace("undefined", String.Empty);
                }
                else
                {
                    selectedDeveloperGroups = "";
                }
                string selectedApproverGroups = Request.Form["hdnSelectedApproverGroups"];
                if (selectedApproverGroups != null)
                {
                    selectedApproverGroups = selectedApproverGroups.Replace("undefined", String.Empty);
                }
                else
                {
                    selectedApproverGroups = "";
                }

                CodeSigningTypeManager manager = new CodeSigningTypeManager();

                string savedMessage;
                if (csType.Id < 1)
                {
                    savedMessage = "Code Signing Profile Created";
                }
                else
                {
                    savedMessage = "Code Signing Profile Saved";
                }
                manager.SaveCodeSigningType(csType, selectedDeveloperGroups, selectedApproverGroups);


                return RedirectToAction("ManageCodeSigningTypes", new { @savedMessage = savedMessage });
            }
            catch (TypeExistsException e)
            {
                return TypeExists(e.ExistingTypeId);
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        public ActionResult TypeExists(int typeId)
        {
            TempData.Add("id", typeId);
            TempData.Add("entity", "Type");

            return RedirectToAction("EntityExists", "Admin");
        }
    }
}
