using System;
using System.Web.Mvc;
using System.Web.Security;
using System.Linq;
using CSG.AppServer.Security;
using CSG.WebSite.Models;
using System.ServiceModel.DomainServices.Server;
using CSG.AppServer.Db;

namespace CSG.WebSite.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private CustomMembershipProvider MembershipProvider = new CustomMembershipProvider();

        //
        // GET: /Account/Login

        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            if (User == null || User.Identity.IsAuthenticated)
            {
                return LogOff();
            }

            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login

        [HttpPost]
        [AllowAnonymous]
        public ActionResult Login(LoginModel model, string returnUrl)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                gatekeeper.LogUserActivity("Login");

                if (ModelState.IsValid)
                {
                    MembershipProvider.Domain = model.Domain;
                    if (MembershipProvider.ValidateUser(model.UserName, model.Password))
                    {
                        if (!string.IsNullOrWhiteSpace(returnUrl))
                        {
                            if (returnUrl.Contains("LogOff"))
                            {
                                return RedirectToLocal(returnUrl);
                            }
                        }
                        return RedirectToAction("Index", "Home");
                    }
                }

                // If we got this far, something failed, redisplay form
                ModelState.AddModelError("", "The user name or password provided is incorrect for the domain provided.");
                return View(model);
            }
            catch (Exception)
            {
                // If we got this far, something failed, redisplay form
                ModelState.AddModelError("", "The user name or password provided is incorrect for the domain provided.");
                return View(model);
            }
        }

        //
        // POST: /Account/LogOff

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            MembershipProvider.LogOff();

            return RedirectToAction("Index", "Home");
        }


        //
        // GET: /Account/Manage

        public ActionResult Manage()
        {
            ESPDbDomainService _domainService = new ESPDbDomainService();
            IQueryable<AppServer.Entities.ActivityLogEntry> alEntries= _domainService.GetActivityLogEntries();
            ViewBag.ActivitiyEntries=alEntries.Where(e => e.UserLogin == User.Identity.Name).OrderByDescending(e => e.LogTime);

            return View();
        }

        //
        // POST: /Account/Manage

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Manage(string username)
        {
            ESPDbDomainService _domainService = new ESPDbDomainService();
            IQueryable<AppServer.Entities.ActivityLogEntry> alEntries = _domainService.GetActivityLogEntries();
            ViewBag.ActivitiyEntries = alEntries.Where(e => e.UserLogin == User.Identity.Name).OrderByDescending(e => e.LogTime);
            return View();
        }

        #region Helpers
        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        //private static string ErrorCodeToString(MembershipCreateStatus createStatus)
        //{
        //    // See http://go.microsoft.com/fwlink/?LinkID=177550 for
        //    // a full list of status codes.
        //    switch (createStatus)
        //    {
        //        case MembershipCreateStatus.DuplicateUserName:
        //            return "User name already exists. Please enter a different user name.";

        //        case MembershipCreateStatus.DuplicateEmail:
        //            return "A user name for that e-mail address already exists. Please enter a different e-mail address.";

        //        case MembershipCreateStatus.InvalidPassword:
        //            return "The password provided is invalid. Please enter a valid password value.";

        //        case MembershipCreateStatus.InvalidEmail:
        //            return "The e-mail address provided is invalid. Please check the value and try again.";

        //        case MembershipCreateStatus.InvalidAnswer:
        //            return "The password retrieval answer provided is invalid. Please check the value and try again.";

        //        case MembershipCreateStatus.InvalidQuestion:
        //            return "The password retrieval question provided is invalid. Please check the value and try again.";

        //        case MembershipCreateStatus.InvalidUserName:
        //            return "The user name provided is invalid. Please check the value and try again.";

        //        case MembershipCreateStatus.ProviderError:
        //            return "The authentication provider returned an error. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

        //        case MembershipCreateStatus.UserRejected:
        //            return "The user creation request has been canceled. Please verify your entry and try again. If the problem persists, please contact your system administrator.";

        //        default:
        //            return "An unknown error occurred. Please verify your entry and try again. If the problem persists, please contact your system administrator.";
        //    }
        //}
        #endregion
    }
}
