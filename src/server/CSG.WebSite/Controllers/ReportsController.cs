﻿using System;
using System.Linq;
using System.Web.Mvc;
using CSG.AppServer.Reporting;
using CSG.AppServer.Security;
using CSG.WebSite.Helpers;
using Util.Reports;

namespace CSG.WebSite.Controllers
{
    public class ReportsController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult ActivityLogReportDefault()
        {
            ViewBag.LogEntryLevels = UIHelper.GetSelectListForLogEntryLevels("<ALL>");

            return View("ActivityLogReport", new ActivityLogReport());
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ActivityLogReport(FormCollection collection, ActivityLogReport report)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                gatekeeper.ValidateUserIsAppAdmin();

                ReportSaveType saveType = ReportHelper.GetReportSaveType(report.SelectedSaveType);

                ReportRunStatus runStatus = new ReportRunStatus();
                if (report.StartDate == null && !String.IsNullOrEmpty(collection["StartDate"]))
                {
                    runStatus.RanSuccessfully = false;
                    StandardValidators validators = new StandardValidators();
                    validators.AddPropertyError("Start Date", "The field must contain a valid date value.");
                    runStatus.StandardValidators = validators;
                }
                else if (report.EndDate == null && !String.IsNullOrEmpty(collection["EndDate"]))
                {
                    runStatus.RanSuccessfully = false;
                    StandardValidators validators = new StandardValidators();
                    validators.AddPropertyError("End Date", "The field must contain a valid date value.");
                    runStatus.StandardValidators = validators;
                }
                else
                {
                    runStatus = report.InitializeAndRun();
                }

                // Handle "saving" (ie. streaming them to the user's browser) Non-Html files
                if (runStatus.RanSuccessfully)
                {
                    if (saveType != ReportSaveType.Html)
                    {
                        report.Save(Response, reportSaveType: saveType);
                    }
                    else
                    {
                        ViewBag.Html = report.GetHtml();
                    }
                }
                else
                {
                    this.ModelState.AddModelError("validationErrors", runStatus.StandardValidators.GetPropertyErrorMessages());
                }

                ViewBag.LogEntryLevels = UIHelper.GetSelectListForLogEntryLevels(report.LogEntryLevel);

                return View("ActivityLogReport", report);
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        public ActionResult DashboardReportDefault()
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            ViewBag.Statuses = UIHelper.GetSelectListForStatuses("-1");
            ViewBag.Roles = UIHelper.GetSelectListForRoles("0");
            ViewBag.CodeSigningTypes = UIHelper.GetSelectListForCodeSigningTypes("0", gatekeeper.GetCurrentUserProfile());
            var userProfile = gatekeeper.GetCurrentUserProfile();
            ViewBag.IsAppAdmin = userProfile.IsAppAdmin;
            ViewBag.Report = new DashboardReport();

            //ViewBag.SaveTypes = UIHelper.GetSelectListForReportSaveTypes());

            return View("DashboardReport", new DashboardReport());
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult DashboardReport(FormCollection collection, DashboardReport report)
        {
            Gatekeeper gatekeeper = new Gatekeeper();
            try
            {
                var userProfile = gatekeeper.GetCurrentUserProfile();
                ViewBag.IsAppAdmin = userProfile.IsAppAdmin;

                // for non admins, the usage report is always for this user
                if (!userProfile.IsAppAdmin)
                {
                    report.UserId = userProfile.Id;
                    report.RoleId = 1;
                }

                
                ReportSaveType saveType = ReportHelper.GetReportSaveType(report.SelectedSaveType);
                ReportRunStatus runStatus = new ReportRunStatus();
                if (report.StartDate == null && !String.IsNullOrEmpty(collection["StartDate"]))
                {
                    runStatus.RanSuccessfully = false;
                    StandardValidators validators = new StandardValidators();
                    validators.AddPropertyError("Start Date", "The field must contain a valid date value.");
                    runStatus.StandardValidators = validators;
                }
                else if (report.EndDate == null && !String.IsNullOrEmpty(collection["EndDate"]))
                {
                    runStatus.RanSuccessfully = false;
                    StandardValidators validators = new StandardValidators();
                    validators.AddPropertyError("End Date", "The field must contain a valid date value.");
                    runStatus.StandardValidators = validators;
                }
                else
                {
                    runStatus = report.InitializeAndRun();
                }

                // Handle "saving" (ie. streaming them to the user's browser) Non-Html files
                if (runStatus.RanSuccessfully)
                {
                    if (saveType != ReportSaveType.Html)
                    {
                        report.Save(Response, reportSaveType: saveType);
                    }
                    else
                    {
                        ViewBag.Html = report.GetHtml();
                    }
                }
                else
                {
                    this.ModelState.AddModelError("validationErrors", runStatus.StandardValidators.GetPropertyErrorMessages());
                }

                ViewBag.Statuses = UIHelper.GetSelectListForStatuses(report.StatusId.ToString());
                ViewBag.Roles = UIHelper.GetSelectListForRoles(report.RoleId.ToString());
                ViewBag.CodeSigningTypes = UIHelper.GetSelectListForCodeSigningTypes(report.CodeSigningTypeId.ToString(), gatekeeper.GetCurrentUserProfile());
                return View("DashboardReport", report);
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        public ActionResult UsageReportDefault()
        {
            Gatekeeper gatekeeper = new Gatekeeper();
            var userProfile = gatekeeper.GetCurrentUserProfile();
            ViewBag.UsageReportTypes = UIHelper.GetSelectListForUsageReportTypes(0);
            ViewBag.CodeSigningTypes = UIHelper.GetSelectListForCodeSigningTypes("0", userProfile);
            ViewBag.IsAppAdmin = userProfile.IsAppAdmin;
            ViewBag.Report = new UsageReport();

            return View("UsageReport", new UsageReport());
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult UsageReport(FormCollection collection, UsageReport report)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                var userProfile = gatekeeper.GetCurrentUserProfile();

                this.ModelState.Remove("");
                // for non admins, the usage report is always for this user
                report.UserIsAppAdmin = userProfile.IsAppAdmin;
                if (!userProfile.IsAppAdmin)
                {
                    report.UserId = userProfile.Id;
                }

                ReportSaveType saveType = ReportHelper.GetReportSaveType(report.SelectedSaveType);
                ReportRunStatus runStatus = new ReportRunStatus();
                if (report.StartDate == null && !String.IsNullOrEmpty(collection["StartDate"]))
                {
                    runStatus.RanSuccessfully = false;
                    StandardValidators validators = new StandardValidators();
                    validators.AddPropertyError("Start Date", "The field must contain a valid date value.");
                    runStatus.StandardValidators = validators;
                }
                else if (report.EndDate == null && !String.IsNullOrEmpty(collection["EndDate"]))
                {
                    runStatus.RanSuccessfully = false;
                    StandardValidators validators = new StandardValidators();
                    validators.AddPropertyError("End Date", "The field must contain a valid date value.");
                    runStatus.StandardValidators = validators;
                }
                else
                {
                    runStatus = report.InitializeAndRun();
                }

                // Handle "saving" (ie. streaming them to the user's browser) Non-Html files
                if (runStatus.RanSuccessfully)
                {
                    if (saveType != ReportSaveType.Html)
                    {
                        report.Save(Response, reportSaveType: saveType);
                    }
                    else
                    {
                        ViewBag.Html = report.GetHtml();
                    }
                }
                else
                {
                    this.ModelState.AddModelError("validationErrors", runStatus.StandardValidators.GetPropertyErrorMessages());
                }

                ViewBag.UsageReportTypes = UIHelper.GetSelectListForUsageReportTypes((int)report.UsageReportTypeId);
                ViewBag.CodeSigningTypes = UIHelper.GetSelectListForCodeSigningTypes(report.CodeSigningTypeId.ToString(), gatekeeper.GetCurrentUserProfile());
                ViewBag.IsAppAdmin = userProfile.IsAppAdmin;

                return View("UsageReport", report);
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        /// <summary>
        /// This JSON call supports the autocomplete user search
        /// </summary>
        public JsonResult GetUsers(string term)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                var users = Utilities.GetUsers(gatekeeper.GetCurrentUserProfile(), term).Take(10);

                var items = from user in users
                            select new
                            {
                                id = user.Id,
                                value = user.DisplayName
                            };

                return Json(items.ToArray(), JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                gatekeeper.LogException("Exception occurred attempting to search users.", e);

                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
