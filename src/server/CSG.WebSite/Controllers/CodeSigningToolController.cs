﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CSG.AppServer.Db;
using CSG.AppServer.Entities;
using CSG.AppServer.Managers;
using CSG.AppServer.Security;
using CSG.WebSite.Helpers;
using System.Security.Cryptography.X509Certificates;
using System.DirectoryServices;

namespace CSG.WebSite.Controllers
{
    public class CodeSigningToolController : Controller
    {
        public ActionResult ManageSigningTools(string savedMessage = null)
        {
            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                gatekeeper.ValidateUserIsAppAdmin();

                ESPDbDomainService svc = new ESPDbDomainService();
                ViewBag.CodeSigningTools = svc.GetCodeSigningTools();
                ViewBag.SavedMessage = savedMessage;

                return View();
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
        }

        
        public class CodeSigningToolInfo
        {
            public string TimestampServer { get; set; }
            public string VerificationArgs { get; set; }
            public string SigningArgs { get; set; }
            public bool IsMacroSigner { get; set; }
        }

        public JsonResult GetSigningToolInfo(int toolId)
        {

            Gatekeeper gatekeeper = new Gatekeeper();
            try
            {
                // this is only used by administrators
                gatekeeper.ValidateUserIsAppAdmin();

                CodeSigningToolInfo toolInfo = new CodeSigningToolInfo()
                {
                    TimestampServer = string.Empty,
                    VerificationArgs = string.Empty,
                    SigningArgs = string.Empty,
                    IsMacroSigner = false
                };
                ESPDbDomainService svc = new ESPDbDomainService();
                var cst = svc.GetCodeSigningToolById(toolId);

                if (cst != null)
                {
                    toolInfo.TimestampServer = cst.TimestampServer;
                    toolInfo.SigningArgs = cst.CommonSigningArguments;
                    toolInfo.VerificationArgs = cst.CommonVerificationArguments;
                    toolInfo.IsMacroSigner = cst.Offline;
                }

                var response = Json(toolInfo, JsonRequestBehavior.AllowGet);

                return response;
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return Json(false, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ManageSigningTool(int toolId)
        {
            SelectListItem slTypeSigntool = new SelectListItem { Selected = false, Text = "SignTool", Value = "0" };
            SelectListItem slTypeJarsigner = new SelectListItem { Selected = false, Text = "jarsigner", Value = "1" };
            SelectListItem slTypeCodesignCommand = new SelectListItem { Selected = false, Text = "codesign", Value = "2" };
            SelectListItem slTypeCodesignSCommand = new SelectListItem { Selected = false, Text = "codesignShare", Value = "3" };
            SelectListItem slTypeHABcstCommand = new SelectListItem { Selected = false, Text = "HABcst", Value = "4" };
            SelectListItem slTypeEvmctlCommand = new SelectListItem { Selected = false, Text = "evmctl", Value = "5" };
            SelectListItem slTypeCertreqCommand = new SelectListItem { Selected = false, Text = "certreq", Value = "6" };
            SelectListItem slTypeCertreqRCommand = new SelectListItem { Selected = false, Text = "certreqRemote", Value = "7" };
            SelectListItem slTypeSignToolRCommand = new SelectListItem { Selected = false, Text = "SigntoolRemote", Value = "8" };
            SelectListItem slTypeSignBinaryCommand = new SelectListItem { Selected = false, Text = "SignBinary", Value = "9" };
            SelectListItem slTypeSnCommand = new SelectListItem { Selected = false, Text = "sn", Value = "10" };
            SelectListItem slTypeLinuxScriptCommand = new SelectListItem { Selected = false, Text = "LinuxScript", Value = "11" };
            SelectListItem slTypeSnRCommand = new SelectListItem { Selected = false, Text = "snR", Value = "12" };
            SelectListItem slTypeGenericCommand = new SelectListItem { Selected = false, Text = "Generic", Value = "13" };

            // the order of this last matters, it is indexed by the entry in the DB
            List<SelectListItem> selectListItems = new List<SelectListItem>() {
                                slTypeSigntool,                 // 0
                                slTypeJarsigner,                // 1
                                slTypeCodesignCommand,          // 2
                                slTypeCodesignSCommand,         // 3
                                slTypeHABcstCommand,            // 4
                                slTypeEvmctlCommand,           // 5
                                slTypeCertreqCommand,           // 6
                                slTypeCertreqRCommand,          // 7
                                slTypeSignToolRCommand,         // 8
                                slTypeSignBinaryCommand,     // 9
                                slTypeSnCommand,            // 10
                                slTypeLinuxScriptCommand,       // 11
                                slTypeSnRCommand,           // 12
                                slTypeGenericCommand           // 13            
                };

            Gatekeeper gatekeeper = new Gatekeeper();

            try
            {
                gatekeeper.ValidateUserIsAppAdmin();

                ESPDbDomainService svc = new ESPDbDomainService();
                CodeSigningTool tool = null;
                string selectedArgsFormat = null;

                if (toolId > 0)
                {
                    tool = svc.GetCodeSigningToolById(toolId);

                    if (tool.SigningArgumentFormat < selectListItems.Count)
                    {
                        selectedArgsFormat = selectListItems[tool.SigningArgumentFormat].Value;
                    }
                    else
                    {
                        selectedArgsFormat = selectListItems[0].Value;

                    }
                }
                else
                {
                    tool = new CodeSigningTool();
                }

                var slCommandLineTypes = new SelectList(selectListItems, "Value", "Text", selectedArgsFormat);
                ViewBag.CommandLineTypes = slCommandLineTypes;

                return View(tool);
            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }

        }

        [HttpPost]
        public ActionResult SaveCodeSigningTool(FormCollection collection, CodeSigningTool csTool)
        {
            Gatekeeper gatekeeper = new Gatekeeper();
            string slCommandType = collection["ddlCommandLineType"];
            int commandLineType = 0;

            try
            {
                gatekeeper.ValidateUserIsAppAdmin();

                if (csTool.Offline)
                {
                    csTool.SigningExecutable = "offline";
                    csTool.SigningArgumentFormat = 0;
                    csTool.VerificationExecutable = "offline";
                }
                else
                {
                    if (slCommandType != null)
                    {
                        commandLineType = Convert.ToInt32(slCommandType);
                        if (commandLineType < 0 || commandLineType > 13)
                        {
                            commandLineType = 0;
                        }
                    }
                    csTool.SigningArgumentFormat = commandLineType;
                }

                // log the user activity
                ActivityLog.Log(gatekeeper.GetCurrentUserProfile(),
                                ActivityLog.LogEventId_CHANGE_TOOL_CONFIG,
                                "ManageCodeSigningTool", "SaveTool",
                                csTool.Name);

                SigningToolManager manager = new SigningToolManager();
                manager.SaveCodeSigningTool(csTool);               
            }
            catch (ToolExistsException e)
            {
                return ToolExists(e.ExistingToolName);

            }
            catch (Exception e)
            {
                gatekeeper.LogException(String.Empty, e);
                return UIHelper.RedirectToError(e);
            }
            return RedirectToAction("ManageSigningTools", new { @savedMessage = "Signing Tool Saved" });
        }

        public ActionResult ToolExists(string toolName)
        {
            TempData.Add("id", toolName);
            TempData.Add("entity", "Tool");

            return RedirectToAction("EntityExists", "Admin");
        }
    }
}
