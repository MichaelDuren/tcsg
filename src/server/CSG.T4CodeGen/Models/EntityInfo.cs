//  Current as of 08/30/2012

using System.Collections.Generic;

namespace CSG.T4CodeGen.Models
{
    public class EntityInfo
    {
        public string EntityName;

        private string _entityPluralName;
        public string EntityPluralName
        {
            get
            {
                if (_entityPluralName == null) { return EntityName.SimplePluralize(); }
                else { return _entityPluralName; }
            }
            set { _entityPluralName = value; }
        }

        public EntityInterfaceType EntityInterfaceType;
        public static EntityInterfaceType Default_EntityInterfaceType;

        public GatekeeperLoggingOption DomainService_GateKeeperLoggingOption;
        public static GatekeeperLoggingOption Default_DomainService_GatekeeperLoggingOption;

        public GatekeeperValidationOption DomainService_GatekeeperValidationOption;
        public static GatekeeperValidationOption Default_DomainService_GatekeeperValidationOption;

        public List<DomainServiceMethod> DomainService_MethodsToCreate = new List<DomainServiceMethod>();

        public static List<DomainServiceMethod> Default_DomainService_MethodsToCreate = new List<DomainServiceMethod>{
            DomainServiceMethod.Get,
            DomainServiceMethod.GetById,
            DomainServiceMethod.Insert,
            DomainServiceMethod.Update,
            DomainServiceMethod.Delete
        };

        public string DbContext_DbSetAccessModifier;
        public static string Default_DbContext_DbSetAccessModifier = "public";

        public EntityInfo()
        {
            EntityInterfaceType = Default_EntityInterfaceType;
            DomainService_GateKeeperLoggingOption = Default_DomainService_GatekeeperLoggingOption;
            DomainService_GatekeeperValidationOption = Default_DomainService_GatekeeperValidationOption;
            DomainService_MethodsToCreate = Default_DomainService_MethodsToCreate;
            DbContext_DbSetAccessModifier = Default_DbContext_DbSetAccessModifier;
        }
    }
}
