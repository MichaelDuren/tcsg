//  Current as of 08/30/2012

using System;

namespace CSG.T4CodeGen.Models
{
    public static class ExtensionMethods
    {
        public static string InitialToLower(this string thisString)
        {
            if (!String.IsNullOrWhiteSpace(thisString))
            {
                return Char.ToLower(thisString[0]) + thisString.Substring(1);
            }
            else
            {
                return thisString;
            }
        }

        public static string SimplePluralize(this string thisString)
        {
            if (String.IsNullOrWhiteSpace(thisString)) { return string.Empty; }
            return thisString + "s";
        }
    }
}
