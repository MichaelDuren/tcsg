//  Current as of 08/30/2012

using System.Collections.Generic;

namespace CSG.T4CodeGen.Models
{
    public class CodeGenContext
    {
        public string EntitiesNameSpace;
        public string EnumTablesNameSpace;

        public string DbContextName;
        public string DbContextNameSpace;

        public string DbDomainServiceName;
        public string DbDomainServiceNameSpace;

        public string ManagersNameSpace;

        public string AppServerGeneratedDirectory;

        public Dictionary<string, string> ExtraAttributes = new Dictionary<string, string>();

        public List<EntityInfo> Entities = new List<EntityInfo>();

        public virtual void Initialize() { }
    }
}
