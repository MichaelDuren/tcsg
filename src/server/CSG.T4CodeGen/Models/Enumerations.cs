//  Current as of 08/30/2012

namespace CSG.T4CodeGen.Models
{
    public enum DomainServiceMethod
    {
        Get,
        GetById,
        GetAllFromCache,
        Insert,
        Update,
        Delete,
        InvalidateCache
    }

    public enum GatekeeperLoggingOption
    {
        None,
        LogCudMethods,
        LogAllMethods
    }

    public enum GatekeeperValidationOption
    {
        None,
        ClassMethod,
        UserAdminCudMethods,
        UserAdminAllMethods,
        AppAdminCudMethods,
        AppAdminAllMethods,
        SysAdminCudMethods,
        SysAdminAllMethods
    }

    public enum EntityInterfaceType
    {
        None,
        IEnumEntity,
        IEntity,
        IVersionedEntity,
        ITrackedEntity,
        ILookupEntity
    }

}
