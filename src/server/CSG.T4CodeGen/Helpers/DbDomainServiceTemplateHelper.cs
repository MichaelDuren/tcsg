//  Current as of 08/30/2012

using System;
using System.Text;
using CSG.T4CodeGen.Models;

namespace CSG.T4CodeGen.Helpers
{
    public class DbDomainServiceTemplateHelper
    {
        public string CreateMethodBody(EntityInfo entityInfo, DomainServiceMethod domainServiceMethod)
        {
            StringBuilder sb = new StringBuilder();

            sb.Append(CreateCoreMethod(entityInfo, domainServiceMethod));

            return sb.ToString();
        }

        public string CreatePublicMethodBody(EntityInfo entityInfo, DomainServiceMethod domainServiceMethod)
        {
            bool isCudMethod = IsCudMethod(domainServiceMethod);
            bool areLogging = AreLogging(entityInfo, isCudMethod);

            StringBuilder sb = new StringBuilder();

            sb.AppendLine("     Gatekeeper gatekeeper = new Gatekeeper();");
            sb.AppendLine(string.Empty);
            sb.AppendLine("     try");
            sb.AppendLine("     {");

            if (entityInfo.DomainService_GatekeeperValidationOption == GatekeeperValidationOption.ClassMethod)
            {
                sb.AppendLine("         gatekeeper.ValidateClassAndMethodAccess();");
            }
            else if (entityInfo.DomainService_GatekeeperValidationOption == GatekeeperValidationOption.AppAdminAllMethods ||
                    (entityInfo.DomainService_GatekeeperValidationOption == GatekeeperValidationOption.AppAdminCudMethods && isCudMethod))
            {
                sb.AppendLine("         gatekeeper.ValidateUserIsAppAdminAccess();");
            }
            else if (entityInfo.DomainService_GatekeeperValidationOption == GatekeeperValidationOption.UserAdminAllMethods ||
                    (entityInfo.DomainService_GatekeeperValidationOption == GatekeeperValidationOption.UserAdminCudMethods && isCudMethod))
            {
                sb.AppendLine("         gatekeeper.ValidateUserIsUserAdminAccess();");
            }

            else if (entityInfo.DomainService_GatekeeperValidationOption == GatekeeperValidationOption.SysAdminAllMethods ||
                    (entityInfo.DomainService_GatekeeperValidationOption == GatekeeperValidationOption.SysAdminCudMethods && isCudMethod))
            {
                sb.AppendLine("         gatekeeper.ValidateUserIsSysAdminAccess();");
            }

            if (areLogging)
            {
                sb.AppendLine("         gatekeeper.LogUserActivity();");
            }

            sb.AppendLine(CreateCoreMethod(entityInfo, domainServiceMethod));

            sb.AppendLine("         }");
            sb.AppendLine("         catch(Exception e)");
            sb.AppendLine("         {");
            sb.AppendLine("             throw gatekeeper.LogException(e);");
            sb.Append("         }");

            if (areLogging)
            {
                sb.AppendLine(string.Empty);
                sb.AppendLine("         finally");
                sb.AppendLine("         {");
                sb.AppendLine("             gatekeeper.LogElapsedTime();");
                sb.AppendLine("         }");
            }

            return sb.ToString();
        }

        private string CreateCoreMethod(EntityInfo entityInfo, DomainServiceMethod domainServiceMethod)
        {
            StringBuilder sb = new StringBuilder();

            string indentSpaces = Repeat(" ", 12);

            switch (domainServiceMethod)
            {
                case DomainServiceMethod.Get:
                    sb.Append(string.Format("{0}return this.DbContext.Set<{1}>(includes);", indentSpaces, entityInfo.EntityName));
                    break;

                case DomainServiceMethod.GetAllFromCache:
                    sb.Append(string.Format("{0}return GetAllFromCache<{1}>(includes);", indentSpaces, entityInfo.EntityName));
                    break;

                case DomainServiceMethod.InvalidateCache:
                    sb.Append(string.Format("{0}InvalidateCache<{1}>();", indentSpaces, entityInfo.EntityName));
                    break;

                case DomainServiceMethod.GetById:
                    sb.AppendLine(string.Format("{0}var entity = this.DbContext.Set<{1}>(includes).Where(e => e.Id == id).FirstOrDefault();", indentSpaces, entityInfo.EntityName));
                    sb.Append(string.Format("{0}return entity;", indentSpaces));
                    break;

                case DomainServiceMethod.Insert:
                    sb.AppendLine(string.Format("{0}UpdateEntityStats(entity, CudOperation.{1});", indentSpaces, domainServiceMethod.ToString()));
                    CreateLogCRUDActivity(domainServiceMethod, sb, indentSpaces);
                    sb.Append(string.Format("{0}this.DbContext.Insert(entity);", indentSpaces));
                    break;

                case DomainServiceMethod.Update:
                    sb.AppendLine(string.Format("{0}UpdateEntityStats(entity, CudOperation.{1});", indentSpaces, domainServiceMethod.ToString()));
                    CreateLogCRUDActivity(domainServiceMethod, sb, indentSpaces);
                    sb.Append(string.Format("{0}this.DbContext.Update(entity);", indentSpaces));
                    break;

                case DomainServiceMethod.Delete:
                    sb.AppendLine(string.Format("{0}UpdateEntityStats(entity, CudOperation.{1});", indentSpaces, domainServiceMethod.ToString()));
                    CreateLogCRUDActivity(domainServiceMethod, sb, indentSpaces);
                    sb.Append(string.Format("{0}this.DbContext.Delete(entity);", indentSpaces));
                    break;

                default:
                    throw new ArgumentOutOfRangeException("DomainServiceMethod is invalid.");
            }

            return sb.ToString();
        }

        private static void CreateLogCRUDActivity(DomainServiceMethod domainServiceMethod, StringBuilder sb, string indentSpaces)
        {
            sb.AppendLine(string.Format("{0}if (LogCRUDActivity)", indentSpaces));
            sb.AppendLine(string.Format("{0}{{", indentSpaces));
            sb.AppendLine(string.Format("{0}    ActivityLog.LogEntityActivity(CurrentUserProfile, entity, CudOperation.{1});", indentSpaces, domainServiceMethod.ToString()));
            sb.AppendLine(string.Format("{0}}}", indentSpaces));
        }

        private bool IsCudMethod(DomainServiceMethod domainServiceMethod)
        {
            if (domainServiceMethod == DomainServiceMethod.Insert ||
                domainServiceMethod == DomainServiceMethod.Update ||
                domainServiceMethod == DomainServiceMethod.Delete)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private bool AreLogging(EntityInfo entityInfo, bool isCudMethod)
        {
            if (entityInfo.DomainService_GateKeeperLoggingOption == GatekeeperLoggingOption.LogAllMethods ||
                (entityInfo.DomainService_GateKeeperLoggingOption == GatekeeperLoggingOption.LogAllMethods && isCudMethod))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private string Repeat(string value, int repeatTimes)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < repeatTimes; i++)
            {
                sb.Append(value);
            }
            return sb.ToString();
        }
    }
}
