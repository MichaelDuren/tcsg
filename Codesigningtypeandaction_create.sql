USE [espDb_test]
GO

/****** Object:  Table [dbo].[App_CodeSigningTypeAndAction]    Script Date: 4/19/2017 9:25:02 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[App_CodeSigningTypeAndAction](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CodeSigningTypeId] [int] NOT NULL,
	[Action] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[App_CodeSigningTypeAndAction]  WITH CHECK ADD  CONSTRAINT [FK_dbo.App_CodeSigningTypeAndAction_dbo.Lookup_CodeSigningTypes_CodeSigningTypeId] FOREIGN KEY([CodeSigningTypeId])
REFERENCES [dbo].[Lookup_CodeSigningTypes] ([Id])
GO

ALTER TABLE [dbo].[App_CodeSigningTypeAndAction] CHECK CONSTRAINT [FK_dbo.App_CodeSigningTypeAndAction_dbo.Lookup_CodeSigningTypes_CodeSigningTypeId]
GO

