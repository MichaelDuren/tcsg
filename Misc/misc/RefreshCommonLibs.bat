﻿@echo Copy Util
xcopy "C:\Development\vs\Common\Development\Dev\Util\bin\Debug\Util.dll" ..\..\lib\ /Y
xcopy "C:\Development\vs\Common\Development\Dev\Util\bin\Debug\Util.pdb" ..\..\lib\ /Y
xcopy "C:\Development\vs\Common\Development\Dev\Util\bin\Debug\Util.xml" ..\..\lib\ /Y

@echo Copy Log4Net
xcopy "C:\Development\vs\Common\Development\Dev\Util\bin\Debug\Log4NetEnterpriseLibraryWrapper.dll" ..\..\lib\ /Y
xcopy "C:\Development\vs\Common\Development\Dev\Util\bin\Debug\Log4NetEnterpriseLibraryWrapper.pdb" ..\..\lib\ /Y
xcopy "C:\Development\vs\Common\Development\Dev\Util\bin\Debug\Log4NetEnterpriseLibraryWrapper.xml" ..\..\lib\ /Y

pause