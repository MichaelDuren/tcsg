﻿USE [csgDB]
GO

DELETE FROM [dbo].[App_SystemSettings] 
 WHERE [Name] IN ('StorageLocation', 'TempStorageLocation')

GO

--Update [Storage Location] in the statement with the actual location where the uploaded files are to be stored
--This location should not be accessible to the site users
INSERT [dbo].[App_SystemSettings] ([Name], [Category], [Description], [ExcludeFromSettingsUI], [DataTypeId], [StringValue]) 
VALUES (N'StorageLocation', N'Request Processing', N'The location to store uploaded files.  The account the application is running under must have full read / write access to the location.', 0, 1,'C:\npsCSG\storage')

--Update [Temp Storage Location] in the statement with the actual location where downloaded files are stored and temporarily made available for download
--It is recommended that a 'temp' folder under the site itself would be the best location for this folder.  It must be accessible to site users.
INSERT [dbo].[App_SystemSettings] ([Name], [Category], [Description], [ExcludeFromSettingsUI], [DataTypeId], [StringValue]) 
VALUES (N'TempStorageLocation', N'Request Processing', N'The temporary location to store files for download.  The files in this location will be deleted periodically.  The account the application is running under must have full read / write access to the location.', 0, 1, 'C:\npsCSG\temp')

GO
