USE [csgDB]
GO
/****** Object:  Table [dbo].[App_ActivityLog]    Script Date: 2/23/2017 9:28:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[App_ActivityLog](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserLogin] [nvarchar](50) NULL,
	[UserName] [nvarchar](100) NULL,
	[LogEntryLevel] [nvarchar](5) NULL,
	[LogTime] [datetime] NULL,
	[ActivityType] [nvarchar](100) NULL,
	[ActivitySubType] [nvarchar](300) NULL,
	[Text1] [nvarchar](2000) NULL,
	[Text2] [nvarchar](2000) NULL,
	[Text3] [nvarchar](2000) NULL,
	[Details] [nvarchar](max) NULL,
	[ElapsedMilliseconds] [int] NULL,
	[ApplicationName] [nvarchar](30) NULL,
	[Server] [nvarchar](50) NULL,
	[ActiveDirectorySid] [nvarchar](50) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[App_CodeSigningRequests]    Script Date: 2/23/2017 9:28:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[App_CodeSigningRequests](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CodeSigningTypeId] [int] NOT NULL,
	[GroupId] [int] NOT NULL,
	[LastNotificationSentDate] [datetime] NULL,
	[CompletionDate] [datetime] NULL,
	[StatusId] [int] NOT NULL,
	[UnsignedFileName] [nvarchar](4000) NULL,
	[UnsignedFileHash] [nvarchar](4000) NULL,
	[SignedFileName] [nvarchar](4000) NULL,
	[SignedFileHash] [nvarchar](4000) NULL,
	[FileCount] [int] NULL,
	[CertFileName] [nvarchar](100) NULL,
	[ResultsFileName] [nvarchar](100) NULL,
	[ResponderNote] [nvarchar](1000) NULL,
	[CreatedByUserId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdatedByUserId] [int] NULL,
	[LastUpdatedDate] [datetime] NULL,
	[RowVersion] [timestamp] NOT NULL,
	[SigningResult] [nvarchar](max) NULL,
	[ProjectName] [nvarchar](100) NULL,
    [ReleaseName] [nvarchar](100) NULL,
	[ManagerResponderId] [int] NULL,
	[ManagerResponseDate] [datetime] NULL,
	[Approver1ResponderId] [int] NULL,
	[Approver1ResponseDate] [datetime] NULL,
	[Approver2ResponderId] [int] NULL,
	[Approver2ResponseDate] [datetime] NULL,
	[EscalationCount] [int] NULL,
	[Archived] [datetime] NULL,
	[SigningCommandResultCode] [int] NULL,
	[ScanningCommandResultCode] [int] NULL
	
 CONSTRAINT [PK_dbo.App_CodeSigningRequests] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
CREATE NONCLUSTERED INDEX [App_CodeSigningRequests_GroupId] ON [dbo].[App_CodeSigningRequests] 
(
	[GroupId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[App_CodeSigningTypeUsers]    Script Date: 2/23/2017 9:28:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[App_CodeSigningTypeUsers](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CodeSigningTypeId] [int] NOT NULL,
	[UserProfileId] [int] NOT NULL,
	[IsApprover] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[App_SystemSettings]    Script Date: 2/23/2017 9:28:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[App_SystemSettings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Category] [nvarchar](100) NULL,
	[SubCategory] [nvarchar](100) NULL,
	[Description] [nvarchar](2000) NOT NULL,
	[DataTypeId] [int] NOT NULL,
	[BooleanValue] [bit] NULL,
	[DateValue] [datetime] NULL,
	[IntegerValue] [int] NULL,
	[DoubleValue] [float] NULL,
	[StringValue] [nvarchar](max) NULL,
	[PasswordValue] [nvarchar](1000) NULL,
	[ExcludeFromSettingsUI] [bit] NOT NULL,
	[ExtraString1] [nvarchar](100) NULL,
	[ExtraString2] [nvarchar](100) NULL,
	[ExtraString3] [nvarchar](100) NULL,
	[ExtraInteger1] [int] NULL,
	[ExtraInteger2] [int] NULL,
	[ExtraInteger3] [int] NULL,
 CONSTRAINT [PK_dbo.App_SystemSettings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[App_UserProfileGroups]    Script Date: 2/23/2017 9:28:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[App_UserProfileGroups](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserProfileId] [int] NOT NULL,
	[GroupId] [int] NOT NULL,
	[IsDesignatedApprover] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[App_UserProfileCodeSigningTypes]    Script Date: 02/23/2017 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[App_UserProfileCodeSigningTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserProfileId] [int] NOT NULL,
	[CodeSigningTypeId] [int] NOT NULL,
	[UserSigningTypeRole] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [UserProfileCodeSigningTypes_CodeSigningTypeId] ON [dbo].[App_UserProfileCodeSigningTypes] 
(
	[CodeSigningTypeId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [UserProfileCodeSigningTypes_UserProfileId] ON [dbo].[App_UserProfileCodeSigningTypes] 
(
	[UserProfileId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
CREATE UNIQUE NONCLUSTERED INDEX [UserProfileCodeSigningTypes_Unique] ON [dbo].[App_UserProfileCodeSigningTypes] 
(
	[UserProfileId],
	[CodeSigningTypeId],
	[UserSigningTypeRole]
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[App_UserProfileRoles]    Script Date: 2/23/2017 9:28:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[App_UserProfileRoles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserProfileId] [int] NOT NULL,
	[RoleId] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[App_UserProfiles]    Script Date: 2/23/2017 9:28:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[App_UserProfiles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ActiveDirectorySId] [nvarchar](50) NULL,
	[Domain] [varchar](300) NULL,
	[LoginId] [nvarchar](50) NOT NULL,
	[FirstName] [nvarchar](30) NULL,
	[MiddleName] [nvarchar](20) NULL,
	[LastName] [nvarchar](50) NULL,
	[DisplayName] [nvarchar](100) NULL,
	[Email] [nvarchar](100) NULL,
	[CreatedDate] [datetime] NULL,
	[LastAccessedDate] [datetime] NULL,
	[LoginCount] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[IsServiceAccount] [bit] NOT NULL,
	[RowVersion] [timestamp] NOT NULL,
	[ProxyEmail] [nvarchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
CREATE UNIQUE NONCLUSTERED INDEX [UserProfiles_LoginId] ON [dbo].[App_UserProfiles] 
(
	[LoginId] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO


/****** Object:  Table [dbo].[EdmMetadata]    Script Date: 2/23/2017 9:28:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EdmMetadata](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ModelHash] [nvarchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Lookup_CodeSigningTypes]    Script Date: 2/23/2017 9:28:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lookup_CodeSigningTypes](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](125) NOT NULL,
	[StagingDirectory] [nvarchar](2000) NULL,
	[SignedDirectory] [nvarchar](2000) NULL,
	[Guid] [nvarchar](50) NOT NULL,
	[SigningToolId] [int] NOT NULL,
	[RawSign] [bit] NOT NULL,
	[AdditionalArguments] [nvarchar](1000) NULL,
	[SigningCertificateId] [int] NULL,
	[ValidFileExtensions] [nvarchar](1000) NULL,
	[Description] [nvarchar](1000) NULL,
	[Abbreviation] [nvarchar](15) NULL,
	[Order] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedByUserId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdatedByUserId] [int] NULL,
	[LastUpdatedDate] [datetime] NULL,
	[RowVersion] [timestamp] NOT NULL,
	[DoVerification] [bit] NULL,
	[RequireSecondaryApproval] [bit] NULL,
	[RequireManagerApproval] [bit] NULL,
	[AllowManagerApproval] [bit] NULL,
	[RequireApprover1Approval] [bit] NULL,
	[AllowApprover1Approval] [bit] NULL,
	[RequireApprover2Approval] [bit] NULL,
	[AllowApprover2Approval] [bit] NULL,
	[IsAutoSign] [bit] NOT NULL,
	[SigningType] [int] NULL,
	[SigningCertificate] [nvarchar](3000) NULL,
	[MetadataField1Label] [nvarchar](100) NULL,
	[MetadataField2Label] [nvarchar](100) NULL,
	[RequestTimestamp] [bit] NULL,
	[ApplyToAll] [bit] NULL,
	

 CONSTRAINT [PK_dbo.Lookup_CodeSigningTypes] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],

 CONSTRAINT AK_Guid UNIQUE([Guid]) ON [PRIMARY]

) ON [PRIMARY]

GO
CREATE UNIQUE NONCLUSTERED INDEX [CodeSigningTypes_Name] ON [dbo].[Lookup_CodeSigningTypes] 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Lookup_Groups]    Script Date: 2/23/2017 9:28:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lookup_Groups](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](125) NOT NULL,
	[Domain] [varchar](300) NULL,
	[ActiveDirectorySId] [nvarchar](50) NULL,
	[IsAppAdminGroup] [bit] NOT NULL,
	[IsDeveloperGroup] [bit] NOT NULL,
	[IsApproverGroup] [bit] NOT NULL,
	[Description] [nvarchar](1000) NULL,
	[Abbreviation] [nvarchar](15) NULL,
	[Order] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedByUserId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdatedByUserId] [int] NULL,
	[LastUpdatedDate] [datetime] NULL,
	[RowVersion] [timestamp] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

CREATE UNIQUE NONCLUSTERED INDEX [Groups_Name] ON [dbo].[Lookup_Groups] 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO

/****** Object:  Table [dbo].[Lookup_Roles]    Script Date: 2/23/2017 9:28:21 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Lookup_Roles](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](125) NULL,
	[Description] [nvarchar](1000) NULL,
	[Abbreviation] [nvarchar](15) NULL,
	[Order] [int] NULL,
	[IsDeleted] [bit] NOT NULL,
	[CreatedByUserId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdatedByUserId] [int] NULL,
	[LastUpdatedDate] [datetime] NULL,
	[RowVersion] [timestamp] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO

CREATE UNIQUE NONCLUSTERED INDEX [Roles_Name] ON [dbo].[Lookup_Roles] 
(
	[Name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Lookup_Certificates]    Script Date: 5/2/2017 12:06:18 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Lookup_Certificates](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](256) NOT NULL,
	[IssuedTo] [nvarchar](1000) NULL,
	[IssuedBy] [nvarchar](1000) NULL,
	[DistinguishedName] [nvarchar](1024) NOT NULL,
	[Thumbprint] [nvarchar](256) NOT NULL,
	[NotBefore] [datetime] NOT NULL,
	[NotAfter] [datetime] NOT NULL,
	[InStore] [bit] NOT NULL,
	[InSecurityWorld] [bit] NOT NULL,
	[Description] [nvarchar](1000) NULL,
	[Abbreviation] [nvarchar](15) NULL,
	[Order] [int] NULL,
	[CreatedByUserId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdatedByUserId] [int] NULL,
	[LastUpdatedDate] [datetime] NULL,
	[RowVersion] [timestamp] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
	[MimeEncoded] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_dbo.Lookup_Certificates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 
 CONSTRAINT AK_Thumbprint UNIQUE([Thumbprint]) ON [PRIMARY]

) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[Lookup_CodeSigningTools]    Script Date: 5/1/2017 9:09:54 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Lookup_CodeSigningTools](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](125) NOT NULL,
	[Offline] [bit] NOT NULL,
	[SigningExecutable] [nvarchar](2000) NULL,
	[CommonSigningArguments] [nvarchar](2000) NULL,
	[SigningArgumentFormat] [int] NULL,
	[Description] [nvarchar](1000) NULL,
	[Abbreviation] [nvarchar](15) NULL,
	[Order] [int] NULL,
	[CreatedByUserId] [int] NULL,
	[CreatedDate] [datetime] NULL,
	[LastUpdatedByUserId] [int] NULL,
	[LastUpdatedDate] [datetime] NULL,
	[RowVersion] [timestamp] NOT NULL,
	[VerificationExecutable] [nvarchar](2000) NULL,
	[CommonVerificationArguments] [nvarchar](2000) NULL,
	[IsDeleted] [bit] NOT NULL,
	[TimestampServer] [nvarchar](1000) NULL,
	
 CONSTRAINT [PK_dbo.Lookup_CodeSigningTools] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[App_CodeSigningTemporalApprovals]    Script Date: 5/17/2017 8:11:17 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[App_CodeSigningTemporalApprovals](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserProfileId] [int] NOT NULL,
	[ApprovingUserProfileId] [int] NOT NULL,
	[ApprovingUserAction] [int] NOT NULL,
	[CodeSigningTypeId] [int] NOT NULL,
	[StartDate] [datetime] NOT NULL,
	[FileName] [nvarchar](4000) NULL,
	[EnforceSameFileName] [bit] NOT NULL,
	[Hours] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[App_CodeSigningTypeGroups]    Script Date: 6/2/2017 3:18:07 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[App_CodeSigningTypeGroups](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CodeSigningTypeId] [int] NOT NULL,
	[GroupId] [int] NOT NULL,
	[IsApprover] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[App_CodeSigningTypeGroups] ADD  CONSTRAINT [DF_App_CodeSigningTypeGroups_Approver]  DEFAULT ((0)) FOR [IsApprover]
GO

ALTER TABLE [dbo].[App_CodeSigningTypeGroups]  WITH CHECK ADD  CONSTRAINT [FK_dbo.App_CodeSigningTypeGroups_dbo.Lookup_CodeSigningTypes_CodeSigningTypeId] FOREIGN KEY([CodeSigningTypeId])
REFERENCES [dbo].[Lookup_CodeSigningTypes] ([Id])
GO

ALTER TABLE [dbo].[App_CodeSigningTypeGroups] CHECK CONSTRAINT [FK_dbo.App_CodeSigningTypeGroups_dbo.Lookup_CodeSigningTypes_CodeSigningTypeId]
GO

ALTER TABLE [dbo].[App_CodeSigningTypeGroups]  WITH CHECK ADD  CONSTRAINT [FK_dbo.App_CodeSigningTypeGroups_dbo.Lookup_Groups_GroupId] FOREIGN KEY([GroupId])
REFERENCES [dbo].[Lookup_Groups] ([Id])
GO

ALTER TABLE [dbo].[App_CodeSigningTypeGroups] CHECK CONSTRAINT [FK_dbo.App_CodeSigningTypeGroups_dbo.Lookup_Groups_GroupId]
GO


ALTER TABLE [dbo].[Lookup_CodeSigningTools]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Lookup_CodeSigningTools_dbo.App_UserProfiles_CreatedByUserId] FOREIGN KEY([CreatedByUserId])
REFERENCES [dbo].[App_UserProfiles] ([Id])
GO

ALTER TABLE [dbo].[Lookup_CodeSigningTools] CHECK CONSTRAINT [FK_dbo.Lookup_CodeSigningTools_dbo.App_UserProfiles_CreatedByUserId]
GO

ALTER TABLE [dbo].[Lookup_CodeSigningTools]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Lookup_CodeSigningTools_dbo.App_UserProfiles_LastUpdatedByUserId] FOREIGN KEY([LastUpdatedByUserId])
REFERENCES [dbo].[App_UserProfiles] ([Id])
GO

ALTER TABLE [dbo].[Lookup_CodeSigningTools] CHECK CONSTRAINT [FK_dbo.Lookup_CodeSigningTools_dbo.App_UserProfiles_LastUpdatedByUserId]
GO
ALTER TABLE [dbo].[Lookup_CodeSigningTypes]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Lookup_CodeSigningTypes_dbo.Lookup_CodeSigningTools_SigningTool] FOREIGN KEY([SigningToolId])
REFERENCES [dbo].[Lookup_CodeSigningTools] ([Id])
GO
ALTER TABLE [dbo].[Lookup_CodeSigningTypes] CHECK CONSTRAINT [FK_dbo.Lookup_CodeSigningTypes_dbo.Lookup_CodeSigningTools_SigningTool]
GO
ALTER TABLE [dbo].[Lookup_CodeSigningTypes]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Lookup_CodeSigningTypes_dbo.Lookup_Certificates_SigningCertificate] FOREIGN KEY([SigningCertificateId])
REFERENCES [dbo].[Lookup_Certificates] ([Id])
GO
ALTER TABLE [dbo].[Lookup_CodeSigningTypes] CHECK CONSTRAINT [FK_dbo.Lookup_CodeSigningTypes_dbo.Lookup_Certificates_SigningCertificate]
GO
ALTER TABLE [dbo].[Lookup_Certificates]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Lookup_Certificates_dbo.App_UserProfiles_CreatedByUserId] FOREIGN KEY([CreatedByUserId])
REFERENCES [dbo].[App_UserProfiles] ([Id])
GO

ALTER TABLE [dbo].[Lookup_Certificates] CHECK CONSTRAINT [FK_dbo.Lookup_Certificates_dbo.App_UserProfiles_CreatedByUserId]
GO

ALTER TABLE [dbo].[Lookup_Certificates]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Lookup_Certificates_dbo.App_UserProfiles_LastUpdatedByUserId] FOREIGN KEY([LastUpdatedByUserId])
REFERENCES [dbo].[App_UserProfiles] ([Id])
GO

ALTER TABLE [dbo].[Lookup_Certificates] CHECK CONSTRAINT [FK_dbo.Lookup_Certificates_dbo.App_UserProfiles_LastUpdatedByUserId]
GO

ALTER TABLE [dbo].[Lookup_CodeSigningTypes] ADD  CONSTRAINT [DF_Lookup_CodeSigningTypes_DoVerification]  DEFAULT ((0)) FOR [DoVerification]
GO
ALTER TABLE [dbo].[Lookup_CodeSigningTypes] ADD  CONSTRAINT [DF_Lookup_CodeSigningTypes_RequireSecondaryApproval]  DEFAULT ((0)) FOR [RequireSecondaryApproval]
GO
ALTER TABLE [dbo].[Lookup_CodeSigningTypes] ADD  CONSTRAINT [DF_Lookup_CodeSigningTypes_RequireManagerApproval]  DEFAULT ((0)) FOR [RequireManagerApproval]
GO
ALTER TABLE [dbo].[Lookup_CodeSigningTypes] ADD  CONSTRAINT [DF_Lookup_CodeSigningTypes_AllowManagerApproval]  DEFAULT ((1)) FOR [AllowManagerApproval]
GO
ALTER TABLE [dbo].[Lookup_CodeSigningTypes] ADD  CONSTRAINT [DF_Lookup_CodeSigningTypes_RequireApprover1Approval]  DEFAULT ((0)) FOR [RequireApprover1Approval]
GO
ALTER TABLE [dbo].[Lookup_CodeSigningTypes] ADD  CONSTRAINT [DF_Lookup_CodeSigningTypes_AllowApprover1Approval]  DEFAULT ((1)) FOR [AllowApprover1Approval]
GO
ALTER TABLE [dbo].[Lookup_CodeSigningTypes] ADD  CONSTRAINT [DF_Lookup_CodeSigningTypes_RequireApprover2Approval]  DEFAULT ((0)) FOR [RequireApprover2Approval]
GO
ALTER TABLE [dbo].[Lookup_CodeSigningTypes] ADD  CONSTRAINT [DF_Lookup_CodeSigningTypes_AllowApprover2Approval]  DEFAULT ((1)) FOR [AllowApprover2Approval]
GO
ALTER TABLE [dbo].[App_CodeSigningRequests]  WITH CHECK ADD  CONSTRAINT [FK_dbo.App_CodeSigningRequests_dbo.App_UserProfiles_CreatedByUserId] FOREIGN KEY([CreatedByUserId])
REFERENCES [dbo].[App_UserProfiles] ([Id])
GO
ALTER TABLE [dbo].[App_CodeSigningRequests] CHECK CONSTRAINT [FK_dbo.App_CodeSigningRequests_dbo.App_UserProfiles_CreatedByUserId]
GO
ALTER TABLE [dbo].[App_CodeSigningRequests]  WITH CHECK ADD  CONSTRAINT [FK_dbo.App_CodeSigningRequests_dbo.App_UserProfiles_LastUpdatedByUserId] FOREIGN KEY([LastUpdatedByUserId])
REFERENCES [dbo].[App_UserProfiles] ([Id])
GO
ALTER TABLE [dbo].[App_CodeSigningRequests] CHECK CONSTRAINT [FK_dbo.App_CodeSigningRequests_dbo.App_UserProfiles_LastUpdatedByUserId]
GO
ALTER TABLE [dbo].[App_CodeSigningRequests]  WITH CHECK ADD  CONSTRAINT [FK_dbo.App_CodeSigningRequests_dbo.App_UserProfiles_ManagerResponderId] FOREIGN KEY(ManagerResponderId)
REFERENCES [dbo].[App_UserProfiles] ([Id])
GO
ALTER TABLE [dbo].[App_CodeSigningRequests] CHECK CONSTRAINT [FK_dbo.App_CodeSigningRequests_dbo.App_UserProfiles_ManagerResponderId]
GO
ALTER TABLE [dbo].[App_CodeSigningRequests]  WITH CHECK ADD  CONSTRAINT [FK_dbo.App_CodeSigningRequests_dbo.App_UserProfiles_Approver1ResponderId] FOREIGN KEY(Approver1ResponderId)
REFERENCES [dbo].[App_UserProfiles] ([Id])
GO
ALTER TABLE [dbo].[App_CodeSigningRequests] CHECK CONSTRAINT [FK_dbo.App_CodeSigningRequests_dbo.App_UserProfiles_Approver1ResponderId]
GO
ALTER TABLE [dbo].[App_CodeSigningRequests]  WITH CHECK ADD  CONSTRAINT [FK_dbo.App_CodeSigningRequests_dbo.App_UserProfiles_Approver2ResponderId] FOREIGN KEY(Approver2ResponderId)
REFERENCES [dbo].[App_UserProfiles] ([Id])
GO
ALTER TABLE [dbo].[App_CodeSigningRequests] CHECK CONSTRAINT [FK_dbo.App_CodeSigningRequests_dbo.App_UserProfiles_Approver2ResponderId]
GO
ALTER TABLE [dbo].[App_CodeSigningRequests]  WITH CHECK ADD  CONSTRAINT [FK_dbo.App_CodeSigningRequests_dbo.Lookup_CodeSigningTypes_CodeSigningTypeId] FOREIGN KEY([CodeSigningTypeId])
REFERENCES [dbo].[Lookup_CodeSigningTypes] ([Id])
GO
ALTER TABLE [dbo].[App_CodeSigningRequests] CHECK CONSTRAINT [FK_dbo.App_CodeSigningRequests_dbo.Lookup_CodeSigningTypes_CodeSigningTypeId]
GO
ALTER TABLE [dbo].[App_CodeSigningTypeUsers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.App_CodeSigningTypeUsers_dbo.App_UserProfiles_UserProfileId] FOREIGN KEY([UserProfileId])
REFERENCES [dbo].[App_UserProfiles] ([Id])
GO
ALTER TABLE [dbo].[App_CodeSigningTypeUsers] CHECK CONSTRAINT [FK_dbo.App_CodeSigningTypeUsers_dbo.App_UserProfiles_UserProfileId]
GO
ALTER TABLE [dbo].[App_CodeSigningTypeUsers]  WITH CHECK ADD  CONSTRAINT [FK_dbo.App_CodeSigningTypeUsers_dbo.Lookup_CodeSigningTypes_CodeSigningTypeId] FOREIGN KEY([CodeSigningTypeId])
REFERENCES [dbo].[Lookup_CodeSigningTypes] ([Id])
GO
ALTER TABLE [dbo].[App_CodeSigningTypeUsers] CHECK CONSTRAINT [FK_dbo.App_CodeSigningTypeUsers_dbo.Lookup_CodeSigningTypes_CodeSigningTypeId]
GO

/****** Object:  Foreign Keys for [App_UserProfileCodeSigningTypes] ******/
ALTER TABLE [dbo].[App_UserProfileCodeSigningTypes]  WITH CHECK ADD  CONSTRAINT [FK_dbo.App_UserProfileCodeSigningTypes_dbo.App_UserProfiles_UserProfileId] FOREIGN KEY([UserProfileId])
REFERENCES [dbo].[App_UserProfiles] ([Id])
GO
ALTER TABLE [dbo].[App_UserProfileCodeSigningTypes] CHECK CONSTRAINT [FK_dbo.App_UserProfileCodeSigningTypes_dbo.App_UserProfiles_UserProfileId]
GO
ALTER TABLE [dbo].[App_UserProfileCodeSigningTypes]  WITH CHECK ADD  CONSTRAINT [FK_dbo.App_UserProfileCodeSigningTypes_dbo.Lookup_CodeSigningTypeId_CodeSigningTypeId] FOREIGN KEY([CodeSigningTypeId])
REFERENCES [dbo].[Lookup_CodeSigningTypes] ([Id])
GO
ALTER TABLE [dbo].[App_UserProfileCodeSigningTypes] CHECK CONSTRAINT [FK_dbo.App_UserProfileCodeSigningTypes_dbo.Lookup_CodeSigningTypeId_CodeSigningTypeId]
GO

/****** Object:  Foreign Keys for [App_UserProfileGroups] ******/
ALTER TABLE [dbo].[App_UserProfileGroups]  WITH CHECK ADD  CONSTRAINT [FK_dbo.App_UserProfileGroups_dbo.App_UserProfiles_UserProfileId] FOREIGN KEY([UserProfileId])
REFERENCES [dbo].[App_UserProfiles] ([Id])
GO
ALTER TABLE [dbo].[App_UserProfileGroups] CHECK CONSTRAINT [FK_dbo.App_UserProfileGroups_dbo.App_UserProfiles_UserProfileId]
GO
ALTER TABLE [dbo].[App_UserProfileGroups]  WITH CHECK ADD  CONSTRAINT [FK_dbo.App_UserProfileGroups_dbo.Lookup_Groups_GroupId] FOREIGN KEY([GroupId])
REFERENCES [dbo].[Lookup_Groups] ([Id])
GO
ALTER TABLE [dbo].[App_UserProfileGroups] CHECK CONSTRAINT [FK_dbo.App_UserProfileGroups_dbo.Lookup_Groups_GroupId]
GO

/****** Object:  Foreign Keys for [App_UserProfileRoles] ******/
ALTER TABLE [dbo].[App_UserProfileRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.App_UserProfileRoles_dbo.App_UserProfiles_UserProfileId] FOREIGN KEY([UserProfileId])
REFERENCES [dbo].[App_UserProfiles] ([Id])
GO
ALTER TABLE [dbo].[App_UserProfileRoles] CHECK CONSTRAINT [FK_dbo.App_UserProfileRoles_dbo.App_UserProfiles_UserProfileId]
GO
ALTER TABLE [dbo].[App_UserProfileRoles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.App_UserProfileRoles_dbo.Lookup_Roles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[Lookup_Roles] ([Id])
GO
ALTER TABLE [dbo].[App_UserProfileRoles] CHECK CONSTRAINT [FK_dbo.App_UserProfileRoles_dbo.Lookup_Roles_RoleId]
GO

/****** Object:  Foreign Keys for [Lookup_CodeSigningTypes] ******/
ALTER TABLE [dbo].[Lookup_CodeSigningTypes]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Lookup_CodeSigningTypes_dbo.App_UserProfiles_CreatedByUserId] FOREIGN KEY([CreatedByUserId])
REFERENCES [dbo].[App_UserProfiles] ([Id])
GO
ALTER TABLE [dbo].[Lookup_CodeSigningTypes] CHECK CONSTRAINT [FK_dbo.Lookup_CodeSigningTypes_dbo.App_UserProfiles_CreatedByUserId]
GO
ALTER TABLE [dbo].[Lookup_CodeSigningTypes]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Lookup_CodeSigningTypes_dbo.App_UserProfiles_LastUpdatedByUserId] FOREIGN KEY([LastUpdatedByUserId])
REFERENCES [dbo].[App_UserProfiles] ([Id])
GO
ALTER TABLE [dbo].[Lookup_CodeSigningTypes] CHECK CONSTRAINT [FK_dbo.Lookup_CodeSigningTypes_dbo.App_UserProfiles_LastUpdatedByUserId]
GO
ALTER TABLE [dbo].[Lookup_Groups]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Lookup_Groups_dbo.App_UserProfiles_CreatedByUserId] FOREIGN KEY([CreatedByUserId])
REFERENCES [dbo].[App_UserProfiles] ([Id])
GO
ALTER TABLE [dbo].[Lookup_Groups] CHECK CONSTRAINT [FK_dbo.Lookup_Groups_dbo.App_UserProfiles_CreatedByUserId]
GO
ALTER TABLE [dbo].[Lookup_Groups]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Lookup_Groups_dbo.App_UserProfiles_LastUpdatedByUserId] FOREIGN KEY([LastUpdatedByUserId])
REFERENCES [dbo].[App_UserProfiles] ([Id])
GO
ALTER TABLE [dbo].[Lookup_Groups] CHECK CONSTRAINT [FK_dbo.Lookup_Groups_dbo.App_UserProfiles_LastUpdatedByUserId]
GO
ALTER TABLE [dbo].[Lookup_Roles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Lookup_Roles_dbo.App_UserProfiles_CreatedByUserId] FOREIGN KEY([CreatedByUserId])
REFERENCES [dbo].[App_UserProfiles] ([Id])
GO
ALTER TABLE [dbo].[Lookup_Roles] CHECK CONSTRAINT [FK_dbo.Lookup_Roles_dbo.App_UserProfiles_CreatedByUserId]
GO
ALTER TABLE [dbo].[Lookup_Roles]  WITH CHECK ADD  CONSTRAINT [FK_dbo.Lookup_Roles_dbo.App_UserProfiles_LastUpdatedByUserId] FOREIGN KEY([LastUpdatedByUserId])
REFERENCES [dbo].[App_UserProfiles] ([Id])
GO
ALTER TABLE [dbo].[Lookup_Roles] CHECK CONSTRAINT [FK_dbo.Lookup_Roles_dbo.App_UserProfiles_LastUpdatedByUserId]
GO
ALTER TABLE [dbo].[App_CodeSigningTemporalApprovals]  WITH CHECK ADD  CONSTRAINT [FK_dbo.App_CodeSigningTemporalApprovals_dbo.App_UserProfiles_UserProfileId] FOREIGN KEY([UserProfileId])
REFERENCES [dbo].[App_UserProfiles] ([Id])
GO
ALTER TABLE [dbo].[App_CodeSigningTemporalApprovals] CHECK CONSTRAINT [FK_dbo.App_CodeSigningTemporalApprovals_dbo.App_UserProfiles_UserProfileId]
GO
ALTER TABLE [dbo].[App_CodeSigningTemporalApprovals]  WITH CHECK ADD  CONSTRAINT [FK_dbo.App_CodeSigningTemporalApprovals_dbo.App_UserProfiles_ApprovingUserProfileId] FOREIGN KEY([ApprovingUserProfileId])
REFERENCES [dbo].[App_UserProfiles] ([Id])
GO
ALTER TABLE [dbo].[App_CodeSigningTemporalApprovals] CHECK CONSTRAINT [FK_dbo.App_CodeSigningTemporalApprovals_dbo.App_UserProfiles_ApprovingUserProfileId]
GO
ALTER TABLE [dbo].[App_CodeSigningTemporalApprovals]  WITH CHECK ADD  CONSTRAINT [FK_dbo.App_CodeSigningTemporalApprovals_dbo.Lookup_CodeSigningTypeId_CodeSigningTypeId] FOREIGN KEY([CodeSigningTypeId])
REFERENCES [dbo].[Lookup_CodeSigningTypes] ([Id])
GO
ALTER TABLE [dbo].[App_CodeSigningTemporalApprovals] CHECK CONSTRAINT [FK_dbo.App_CodeSigningTemporalApprovals_dbo.Lookup_CodeSigningTypeId_CodeSigningTypeId]
GO

