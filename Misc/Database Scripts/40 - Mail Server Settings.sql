﻿USE [csgDB]
GO

DELETE FROM [dbo].[App_SystemSettings] 
 WHERE [Name] IN ('EmailServer','EmailServerPort','EmailServerUserId','EmailServerPassword','EmailServerSSL')
GO

--Update [MailServer] in the statement with the actual mail server to be used
INSERT [dbo].[App_SystemSettings] ([Name], [Category], [Description], [DataTypeId], [ExcludeFromSettingsUI], [StringValue])  
VALUES (N'EmailServer', N'Email Settings', N'The mail server to use when sending emails.', 1, 0, '192.168.0.2')

--Update [Port] with the actual port to be used to connect to the mail server
INSERT [dbo].[App_SystemSettings] ([Name], [Category], [Description], [DataTypeId], [ExcludeFromSettingsUI], [IntegerValue]) 
VALUES (N'EmailServerPort', N'Email Settings', N'The mail server port to use when sending emails.', 2, 0, 25)

--Update [Email User Id] with the id to be used.  If no id is required, change the value to NULL
INSERT [dbo].[App_SystemSettings] ([Name], [Category], [Description], [DataTypeId], [ExcludeFromSettingsUI], [StringValue])  
VALUES (N'EmailServerUserId', N'Email Settings', N'The user id to use when when sending emails.', 1, 0, 'admin@ncipher.com')

--Update [Email Password] with the password to be used.  If no password is required, change the value to NULL.  Note that the system will store the password
--In an unecrypted format for the time being, the final delivery of the application will encrypt the value when the System setting is managed through the UI
INSERT [dbo].[App_SystemSettings] ([Name], [Category], [Description], [DataTypeId], [ExcludeFromSettingsUI], [PasswordValue])  
VALUES (N'EmailServerPassword', N'Email Settings', N'The password to use when when sending emails.', 6, 0, 'test')

--Update [Use SSL] to either 0 for false or 1 for true
INSERT [dbo].[App_SystemSettings] ([Name], [Category], [Description], [DataTypeId], [ExcludeFromSettingsUI], [BooleanValue]) 
VALUES (N'EmailServerSSL', N'Email Settings', N'Use SSL when sending emails.', 5, 0, 1)

--Set the value to enable/disable notification settings
INSERT [dbo].[App_SystemSettings] ([Name], [Category], [Description], [DataTypeId], [ExcludeFromSettingsUI], [BooleanValue]) 
VALUES (N'EmailSendNotifications', N'Email Settings', N'Disables notifications if false.', 5, 0, 1)
GO
