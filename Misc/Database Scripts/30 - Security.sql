﻿/* TODO: Replace <Domain>\<LoginId> with the actual Domain and Login Id  */

/****** Object:  Login []    Script Date: 06/12/2017 ******/
CREATE LOGIN [<Domain>\<LoginId>] FROM WINDOWS WITH DEFAULT_DATABASE=[csgDbDev2], DEFAULT_LANGUAGE=[us_english]
GO

USE [csgDbDev2]
GO

/****** Object:  User [ESP] ******/
GO

CREATE USER [csgDbDev2] FOR LOGIN [portaltest\portalservice] WITH DEFAULT_SCHEMA=[dbo]
GO


USE [csgDbDev2]
GO

EXEC sp_addrolemember db_datareader, [csgDbDev2]
GO

EXEC sp_addrolemember db_datawriter, [csgDbDev2] 
GO 

